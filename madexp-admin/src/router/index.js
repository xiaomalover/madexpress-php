import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
  path: '/redirect',
  component: Layout,
  hidden: true,
  children: [{
    path: '/redirect/:path(.*)',
    component: () => import('@/views/redirect/index')
  }]
},
{
  path: '/login',
  component: () => import('@/views/login/index'),
  hidden: true
},
{
  path: '/forget-password',
  component: () => import('@/views/forget/index'),
  hidden: true
},
{
  path: '/auth-redirect',
  component: () => import('@/views/login/auth-redirect'),
  hidden: true
},
{
  path: '/404',
  component: () => import('@/views/error-page/404'),
  hidden: true
},
{
  path: '/401',
  component: () => import('@/views/error-page/401'),
  hidden: true
},
{
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  children: [{
    path: 'dashboard',
    component: () => import('@/views/operate/index'),
    name: 'Dashboard',
    meta: {
      title: '运营数据',
      icon: 'icon_yunyingshuju',
      affix: true
    }
  }]
}
  /* {
    path: '/documentation',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/documentation/index'),
      name: 'Documentation',
      meta: {
        title: 'documentation',
        icon: 'documentation',
        affix: true
      }
    }]
  },

  {

    path: '/icon',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/icons/index'),
      name: 'customer',
      meta: {
        title: 'ICONS',
        icon: 'icon_kefu',
        affix: false
      }
    }]

  },*/
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  // 运营
{

    path: '/allocation',
    component: Layout,
    redirect: '/allocation/index',
    name: 'allocation',
    meta: {
      title: '实时概况',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/allocation/index'),
      name: 'allocation',
      meta: {
        title: '实时概况',
        icon: 'icon_fendanguanli',
        affix: false
      }
    },
    {
      path: 'draw',
      component: () => import('@/views/allocation/draw'),
      name: 'draw',
      meta: {
        title: '围栏编辑',
        activeMenu: '/allocation/index'
      },
      hidden: true
    },
    {
      path: 'hotmap',
      component: () => import('@/views/allocation/hotmap'),
      name: 'hotmap',
      meta: {
        title: '热点地图',
        activeMenu: '/allocation/index'
      },
      hidden: true
    }

    ] },

  // 订单管理
  {

    path: '/order',
    component: Layout,
    redirect: '/order/index',
    name: 'order',
    meta: {
      title: '订单管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/order/index'),
      name: 'orderindex',
      meta: {
        title: '订单管理',
        icon: 'icon_dingdanguanli',
        affix: false
      }
    },
    {
      path: 'details/:id(\\d+)',
      component: () => import('@/views/order/details'),
      name: 'orderdetails',
      meta: {
        title: '订单详情',
        activeMenu: '/order/index'
      },
      hidden: true
    },
	{
      path: 'kpi',
      component: () => import('@/views/order/kpi'),
      name: 'kpi',
      meta: {
        title: '订单KPI',
        noCache: true,
        activeMenu: '/order/index'
      },
      hidden: true
    }
    ] },
  // 分单
  
  // 商户管理
  {

    path: '/store',
    component: Layout,
    redirect: '/store/index',
    name: 'store',
    meta: {
      title: '商户管理',
      icon: 'icon_shangjiaguanli'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/store/index'),
      name: 'index',
      meta: {
        title: '商户管理',
        icon: 'icon_shangjiaguanli',
        affix: false
      }
    },
    {
      path: 'cate',
      component: () => import('@/views/store/cate'),
      name: 'cate',
      meta: {
        title: '菜品分类',
        noCache: true,
        activeMenu: '/store/index'
      },
      hidden: true
    },
    {
      path: 'ingr',
      component: () => import('@/views/store/ingr'),
      name: 'ingr',
      meta: {
        title: '菜品元素',
        noCache: true,
        activeMenu: '/store/index'
      },
      hidden: true
    },

    {
      path: 'stripe',
      component: () => import('@/views/store/stripe'),
      name: 'stripe',
      meta: {
        title: 'stripe管理',
        noCache: true,
        activeMenu: '/store/index'
      },
      hidden: true
    },
	{
      path: 'kpi',
      component: () => import('@/views/store/kpi'),
      name: 'kpi',
      meta: {
        title: '商户KPI',
        noCache: true,
        activeMenu: '/store/index'
      },
      hidden: true
    }


    ] },
  // 用户管理
  {

    path: '/user',
    component: Layout,
    redirect: '/user/index',
    name: 'user',
    meta: {
      title: '用户管理',
      icon: 'icon_yonghuguanli'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/user/index'),
      name: 'user',
      meta: {
        title: '用户管理',
        icon: 'icon_yonghuguanli',
        affix: false
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/user/info'),
      name: 'info',
      meta: {
        title: '用户详情',
        noCache: true,
        activeMenu: '/user/index'
      },
      hidden: true
    },
	{
      path: 'kpi',
      component: () => import('@/views/user/kpi'),
      name: 'kpi',
      meta: {
        title: '用户KPI',
        noCache: true,
        activeMenu: '/user/index'
      },
      hidden: true
    }
    ]
  },

  // 配送员
  {

    path: '/delivery',
    component: Layout,
    redirect: '/delivery/index',
    name: 'delivery',
    meta: {
      title: '送餐员管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/delivery/index'),
      name: 'delivery',
      meta: {
        title: '送餐员管理',
        icon: 'icon_songcanyuanguanli'
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/delivery/info'),
      name: 'info',
      meta: {
        title: '送餐员详情',
        noCache: true,
        activeMenu: '/delivery/index'
      },
      hidden: true
    },
	{
      path: 'kpi',
      component: () => import('@/views/delivery/kpi'),
      name: 'kpi',
      meta: {
        title: '送餐员KPI',
        noCache: true,
        activeMenu: '/delivery/index'
      },
      hidden: true
    },{
      path: 'wallet/:id(\\d+)',
      component: () => import('@/views/delivery/wallet'),
      name: 'deliverywallet',
      meta: {
        title: '送餐员财务管理',
        noCache: true,
        activeMenu: '/delivery/index'
      },
      hidden: true
    },{
      path: 'wallet_activities/:id(\\d+)',
      component: () => import('@/views/delivery/wallet_activities'),
      name: 'deliverywalletactivities',
      meta: {
        title: '送餐员钱包流水',
        noCache: true,
        activeMenu: '/delivery/index'
      },
      hidden: true
    }
    ]
  },

  // 财务管理
  {

    path: '/finance',
    component: Layout,
    redirect: '/finance/index',
    name: 'finance',
    meta: {
      title: '财务管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/finance/index'),
      name: 'finance',
      meta: {
        title: '财务管理',
        icon: 'icon_paibanguanli',
        affix: false
      }
    }, {
      path: 'invoice',
      component: () => import('@/views/finance/invoice'),
      name: 'invoice',
      meta: {
        title: '票据管理',
        noCache: true,
        activeMenu: '/finance/index'
      },
      hidden: true
    }, {
      path: 'activities',
      component: () => import('@/views/finance/activities'),
      name: 'activities',
      meta: {
        title: '收支管理',
        noCache: true,
        activeMenu: '/finance/index'
      },
      hidden: true
    }]

  },


  // 排班
  {
    path: '/scheduling',
    component: Layout,
    redirect: '/scheduling/index',
    name: 'scheduling',
    meta: {
      title: '排班管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/scheduling/index'),
      name: 'scheduling',
      meta: {
        title: '排班管理',
        icon: 'icon_paibanguanli',
        affix: false
      }
    }, {
      path: 'setting',
      component: () => import('@/views/scheduling/setting'),
      name: 'type',
      meta: {
        title: '排班设置',
        noCache: true,
        activeMenu: '/scheduling/index'
      },
      hidden: true
    }]
  },

  // 客服
  {
    path: '/customer',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/customer/index'),
      name: 'customer',
      meta: {
        title: '客服管理',
        icon: 'icon_kefu',
        affix: false
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/customer/info'),
      name: 'info',
      meta: {
        title: '详情',
        noCache: true,
        activeMenu: '/customer/index'
      },
      hidden: true
    },
    {
      path: 'help',
      component: () => import('@/views/customer/help'),
      name: 'help',
      meta: {
        title: '常见问题',
        noCache: true,
        activeMenu: '/customer/index'
      },
      hidden: true
    }
    ]

  },
  // 库存
  {

    path: '/depot',
    component: Layout,
    redirect: '/depot/index',
    name: 'depot',
    meta: {
      title: '库存管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/depot/index'),
      name: 'depot',
      meta: {
        title: '库存管理',
        icon: 'icon_canheguanli',
        affix: false
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/depot/info'),
      name: 'info',
      meta: {
        title: '订单详情',
        noCache: true,
        activeMenu: '/depot/index'
      },
      hidden: true
    },
    {
      path: 'log',
      component: () => import('@/views/depot/log'),
      name: 'log',
      meta: {
        title: '仓库日志',
        noCache: true,
        activeMenu: '/depot/index'
      },
      hidden: true
    }
    ] },
  // 车辆
  {

    path: '/vehicle',
    component: Layout,
    redirect: '/vehicle/index',
    name: 'delivery',
    meta: {
      title: '车辆管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/vehicle/index'),
      name: 'vehicle',
      meta: {
        title: '车辆管理',
        icon: 'icon_cheliangguanli',
        affix: false
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/vehicle/info'),
      name: 'info',
      meta: {
        title: '车辆详情',
        noCache: true,
        activeMenu: '/vehicle/index'
      },
      hidden: true
    },
    {
      path: 'rental_add',
      component: () => import('@/views/vehicle/rental_add'),
      name: 'info',
      meta: {
        title: '添加租赁计划',
        noCache: true,
        activeMenu: '/vehicle/index'
      },
      hidden: true
    }, {
      path: 'rental_edit/:id(\\d+)',
      component: () => import('@/views/vehicle/rental_edit'),
      name: 'info',
      meta: {
        title: '编辑租赁计划',
        noCache: true,
        activeMenu: '/vehicle/index'
      },
      hidden: true
    }
    ] },

  // 钱臧畅
  {

    path: '/wallet',
    component: Layout,
    redirect: '/wallet/index',
    name: 'wallet',
    meta: {
      title: '钱葬厂',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/wallet/index'),
      name: 'wallet',
      meta: {
        title: '钱藏厂',
        icon: 'icon_qianzangchang',
        affix: false
      }
    }, {
      path: 'type',
      component: () => import('@/views/wallet/type'),
      name: 'type',
      meta: {
        title: '类型管理',
        noCache: true,
        activeMenu: '/wallet/index'
      },
      hidden: true
    }, {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/wallet/info'),
      name: 'info',
      meta: {
        title: '优惠券详情',
        noCache: true,
        activeMenu: '/wallet/index'
      },
      hidden: true
    }, {
      path: 'send/:id(\\d+)',
      component: () => import('@/views/wallet/send'),
      name: 'send',
      meta: {
        title: '优惠券派发',
        noCache: true,
        activeMenu: '/wallet/index'
      },
      hidden: true
    }]

  },

  // 备忘
  {
    path: '/memo',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/memo/index'),
      name: 'memo',
      meta: {
        title: '备忘录',
        icon: 'icon_beiwanglu',
        affix: false
      }
    }]
  },

  // 界面管理
  {
    path: '/interface',
    component: Layout,
    redirect: '/interface/index',
    name: 'interface',
    meta: {
      title: '界面管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/interface/index'),
      name: 'interface',
      meta: {
        title: '界面管理',
        icon: 'icon_jiemianguanli',
        affix: false
      }
    },
    {
      path: 'manage',
      component: () => import('@/views/interface/manage'),
      name: 'manage',
      meta: {
        title: '新增图片',
        icon: 'icon_beiwanglu',
        affix: false
      },
      hidden: true
    },
    {
      path: 'manage/:adv_id',
      component: () => import('@/views/interface/manage'),
      name: 'manage',
      meta: {
        title: '编辑图片',
        icon: 'icon_beiwanglu',
        affix: false
      },
      hidden: true
    }

    ]
  },

  // 商户专员
  {
    path: '/merchant',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/merchant/index'),
      name: 'merchant',
      meta: {
        title: '商户专员',
        icon: 'icon_mavin_h',
        affix: false
      }
    }]

  },


  {
    path: '/fileBrowse',
    component: Layout,

    children: [{
      path: 'index',
      hidden: true,
      component: () => import('@/views/fileBrowse/index'),
      name: 'fileBrowse',
      meta: {
        title: '文件管理',
        icon: 'documentation',
        affix: false
      }
    }]

  },

  // 消息管理
  {

    path: '/message',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/message/index'),
      name: 'message',
      meta: {
        title: '消息管理',
        icon: 'icon_message_h',
        affix: false
      }
    }, {
      path: 'edit/:code/:type',
      component: () => import('@/views/message/edit'),
      name: 'edit',
      meta: {
        title: '编辑消息',
        noCache: true,
        activeMenu: '/message/index'
      },
      hidden: true
    }, {
      path: 'notice',
      component: () => import('@/views/message/notice'),
      name: 'notice',
      meta: {
        title: '内容管理',
        noCache: true,
        activeMenu: '/message/index'
      },
      hidden: true

    }, {
      path: 'noticeitem/:id',
      component: () => import('@/views/message/noticeitem'),
      name: 'noticeitem',
      meta: {
        title: '编辑素材',
        noCache: true,
        activeMenu: '/message/index'
      },
      hidden: true

    }]

  },

  // 系统设置
  {

    path: '/system',
    component: Layout,
    redirect: '/system/index',
    name: 'system',
    meta: {
      title: '系统设置',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'system',
      component: () => import('@/views/system/index'),
      name: 'system',
      meta: {
        title: '系统设置',
        icon: 'icon_setting',
        affix: false
      }
    }, {
      path: 'log',
      component: () => import('@/views/system/components/LogList'),
      name: 'type',
      meta: {
        title: '操作日志',
        noCache: true,
        activeMenu: '/system/log'
      },
      hidden: true
    }, {
      path: 'task',
      component: () => import('@/views/system/task'),
      name: 'type',
      meta: {
        title: '计划任务',
        noCache: true,
        activeMenu: '/system/index'
      },
      hidden: true
    }, {
      path: 'db',
      component: () => import('@/views/system/db'),
      name: 'db',
      meta: {
        title: '数据库管理',
        noCache: true,
        activeMenu: '/system/index'
      },
      hidden: true
    }]
  },
  // 404 page must be placed at the end !!!
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
