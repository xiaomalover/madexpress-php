import request from '@/utils/request'

export function getProRegionList() {
  return request({
    url: 'index.php?act=region_money&op=get_area_region',
    method: 'post'
  })
}



export function getRegionList() {
  return request({
    url: 'index.php?act=region_money&op=get_region',
    method: 'post'
  })
}

export function  getOldRegionList(data) {
  return request({
    url: 'index.php?act=region_money&op=get_old_region',
    method: 'post'
  })
}
//新增大区
export function  addBigRegion(data) {
  return request({
    url: 'index.php?act=region_money&op=add_big_name',
    method: 'post'	,
	data
  })
}

//新增大区
export function  saveRegionVersion(data) {
  return request({
    url: 'index.php?act=region_money&op=save_region',
    method: 'post'	,
	data
  })
}

//删除大区小区
export function  delBigRegion(data) {
  return request({
    url: 'index.php?act=region_money&op=del_big',
    method: 'post'	,
	data
  })
}

//批量编辑大区名字
export function  editBigRegion(data) {
  return request({
    url: 'index.php?act=region_money&op=edit_big',
    method: 'post'	,
	data
  })
}


//地区版本启用
export function  editVersionEnable(data) {
  return request({
    url: 'index.php?act=region_money&op=region_enable',
    method: 'post'	,
	data
  })
}


//地区后台显示编辑
export function  editRegionShow(data) {
  return request({
    url: 'index.php?act=region_money&op=region_admin_show',
    method: 'post'	,
	data
  })
}


//保存nav修改
export function  saveNav(data) {
  return request({
    url: 'index.php?act=region_money&op=region_nav_save',
    method: 'post'	,
	data
  })
}


//保存扇形修改
export function  saveSector(data) {
  return request({
    url: 'index.php?act=region_money&op=region_sector_save',
    method: 'post'	,
	data
  })
}

//保存地区显示修改
export function  editRegionProShow(data) {
  return request({
    url: 'index.php?act=region_money&op=region_pro_show',
    method: 'post'	,
	data
  })
}
