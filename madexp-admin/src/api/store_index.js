import request from '@/utils/request'


//数据统计
export function fetchStat(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=stat',
    method: 'post',
    data
  })
}

//热销商品&餐盒数据
export function fetchHotFoodbox(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=hotgoods_foodbox',
    method: 'post',
    data
  })
}

//店铺信息
export function fetchStoreInfo(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=store_info',
    method: 'post',
    data
  })
}
