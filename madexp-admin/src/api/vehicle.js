import request from '@/utils/request'


export function info(data) {
  return request({
    url: 'index.php?act=vehicle&op=info',
    method: 'post',
    data
  })
}

//获取车辆分类
export function fetchClassList() {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_class_list',
    method: 'get'
  })
}


///获取车辆列表
export function fetchList(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_list',
    method: 'post',
   data
  })
}



//获取车辆详情
export function fetchDetails(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_info',
    method: 'post',
   data
  })
}


///获取租车列表
export function fetchVehicleLeaseList(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_lease',
    method: 'post',
  data
  })
}

//历史维护列表
export function fetchVehicleMaintainList(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_maintain',
    method: 'post',
  data
  })
}


//文件列表
export function fetchVehicleFileList(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_file',
    method: 'post',
   data
  })
}

//事件列表
export function fetchVehicleEventList(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_event',
    method: 'post',
    data
  })
}





//租赁计划列表
export function fetchRentalList(data) {
  return request({
    url: 'index.php?act=vehicle&op=rental_list',
    method: 'post',
    data
  })
}

//添加计划列表
export function fetchRentalAdd(data) {
  return request({
    url: 'index.php?act=vehicle&op=rental_save',
    method: 'post',
    data
  })
}


//添加车辆

export function fetchVehicleAdd(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_add',
    method: 'post',
    data
  })
}


//分配车辆

export function fetchVehicleAllocation(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_add',
    method: 'post',
    data
  })
}


//召回

export function fetchVehicleRecall(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_add',
    method: 'post',
    data
  })
}




//暂停

export function fetchVehicleSuspend(data) {
  return request({
    url: 'index.php?act=vehicle&op=suspend',
    method: 'post',
    data
  })
}




//回复

export function fetchVehicleRecovery(data) {
  return request({
    url: 'index.php?act=vehicle&op=recovery',
    method: 'post',
    data
  })
}


//区域

export function fetchVehicleRegion(data) {
  return request({
    url: 'index.php?act=vehicle&op=vehicle_region',
    method: 'post',
    data
  })
}





