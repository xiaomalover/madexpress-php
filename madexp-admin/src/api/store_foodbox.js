import request from '@/utils/request'

export function fetchClassList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_foodbox&op=foodbox_list',
    method: 'post',
    data
  })
}

export function fetchOrderList(data) {
  return request({
    url: 'index.php?act=store_foodbox&op=get_order_list',
    method: 'post',
    data
  })
}

