import request from '@/utils/request'

export function info(query) {
  return request({
    url: 'index.php?act=member&op=info',
    method: 'get',
    params: query
  })
}

export function fetchMemberSave(data) {
  return request({
    url: 'index.php?act=member&op=member_save',
    method: 'post',
    data
  })
}

export function fetchList(data) {
  return request({
    url: 'index.php?act=member&op=member_list',
    method: 'post',
    data
  })
}

export function fetchDetails(data) {
  return request({
    url: 'index.php?act=member&op=member_info',
    method: 'post',
    data
  })
}

export function updateMember(data) {
  return request({
    url: '/vue-element-admin/member/update',
    method: 'post',
    data
  })
}

// 优惠券
export function fetchCouponList(data) {
  return request({
    url: 'index.php?act=member&op=member_coupon',
    method: 'post',
    data
  })
}

// 我的收藏
export function fetchLikeList(data) {
  return request({
    url: 'index.php?act=member&op=member_like',
    method: 'post',
    data
  })
}

// 反馈记录
export function fetchFeedbackList(data) {
  return request({
    url: 'index.php?act=member&op=member_feedback',
    method: 'post',
    data
  })
}

// 订单列表
export function fetchOrderList(data) {
  return request({
    url: 'index.php?act=member&op=member_order',
    method: 'post',
    data
  })
}

// 订单统计
export function fetchOrderStat(data) {
  return request({
    url: 'index.php?act=member&op=get_order_stat',
    method: 'post',
    data
  })
}


//获取address
export function fetchAddressInfo(data) {
  return request({
    url: 'index.php?act=member&op=getAddressInfo',
    method: 'post',
    data
  })
}



//编辑lock
export function fetchMemberLock(data) {
  return request({
    url: 'index.php?act=member&op=member_lock',
    method: 'post',
    data
  })
}

//编辑lock
export function fetchMemberBase(data) {
  return request({
    url: 'index.php?act=member&op=member_base',
    method: 'post',
    data
  })
}
