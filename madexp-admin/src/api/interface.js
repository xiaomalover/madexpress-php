import request from '@/utils/request'

export function fetchInterfaceList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=interface_list',
    method: 'post',
    data
  })
}

export function fetchAdvApList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=ap_manage',
    method: 'post',
    data
  })
}

export function fetchAdvAdd(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=adv_save',
    method: 'post',
    data
  })
}

export function fetchAdvInfo(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=getAdvInfo',
    method: 'post',
    data
  })
}

export function editSwitchShow(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=adv_edit',
    method: 'post',
    data
  })
}

export function delAdv(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=adv_del',
    method: 'post',
    data
  })
}

export function fetchAdvInit(data) {
  console.log(data)
  return request({
    url: 'index.php?act=interface&op=adv_init',
    method: 'post',
    data
  })
}


export function fetchSend(data) {
  console.log(data)
  return request({
    url: 'index.php?act=message&op=send_msg',
    method: 'post',
    data
  })
}
