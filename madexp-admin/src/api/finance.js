import request from '@/utils/request'


export function info(data) {
  return request({
    url: 'index.php?act=finance&op=info',
    method: 'post',
    data
  })
}





export function invoiceList(data) {
  return request({
    url: 'index.php?act=finance&op=invoice_list',
    method: 'post',
    data
  })
}



export function invoiceRoleType(data) {
  return request({
    url: 'index.php?act=finance&op=invoice_role_type',
    method: 'post',
    data
  })
}
export function invoiceType(data) {
  return request({
    url: 'index.php?act=finance&op=invoice_type',
    method: 'post',
    data
  })
}




export function fetchFinanceTypeList(data) {
  return request({
    url: 'index.php?act=finance&op=get_wallet_type',
    method: 'post',
    data
  })
}


export function fetchFinanceList(data) {
  return request({
    url: 'index.php?act=finance&op=get_wallet_list',
    method: 'post',
    data
  })
}
