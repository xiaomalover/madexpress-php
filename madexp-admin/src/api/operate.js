import request from '@/utils/request'

export function info(data) {
  return request({
    url: 'index.php?act=operate&op=info',
    method: 'post',
    data
  })
}

export function createArticle(data) {
  return request({
    url: 'index.php?act=order&op=get_order_list',
    method: 'post',
    data
  })
}




export function kpileftData(data) {
  return request({
    url: 'index.php?act=operate&op=kpi_data_left',
    method: 'post',
    data
  })
}

export function kpirightData(data) {
  return request({
    url: 'index.php?act=operate&op=kpi_data_line',
    method: 'post',
    data
  })
}
