import request from '@/utils/request'

// 获取设置
export function fetchSetting(data) {
  return request({
    url: 'index.php?act=system&op=index',
    method: 'post',
    data
  })
}

// 保存
export function save_sms(data) {
  return request({
    url: 'index.php?act=system&op=save_sms',
    method: 'post',
    data
  })
}

// 保存
export function save_me(data) {
  return request({
    url: 'index.php?act=system&op=save_me',
    method: 'post',
    data
  })
}

// 保存
export function save_upload(data) {
  return request({
    url: 'index.php?act=system&op=save_upload',
    method: 'post',
    data
  })
}

// 保存
export function save_email(data) {
  return request({
    url: 'index.php?act=system&op=save_email',
    method: 'post',
    data
  })
}

// 保存
export function save_lang(data) {
  return request({
    url: 'index.php?act=system&op=save_lang',
    method: 'post',
    data
  })
}

// 保存
export function save_twilio(data) {
  return request({
    url: 'index.php?act=system&op=save_twilio',
    method: 'post',
    data
  })
}

// 获取语言配置
export function fetchLangList(data) {
  return request({
    url: 'index.php?act=system&op=system_lang',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: 'index.php?act=admin&op=logout',
    method: 'post'
  })
}

export function fetchLogList(data) {
  return request({
    url: 'index.php?act=system&op=getAdminListLog',
    method: 'post',
    data
  })
}

export function backupData() {
  return request({
    url: 'index.php?act=dbbackuprestore&op=backup',
    method: 'post'
  })
}

export function allbackup() {
  return request({
    url: 'index.php?act=dbbackuprestore&op=allbackup',
    method: 'post'
  })
}



// 保存
export function save_stripe(data) {
  return request({
    url: 'index.php?act=system&op=save_stripe',
    method: 'post',
    data
  })
}


// 保存
export function save_delivery(data) {
  return request({
    url: 'index.php?act=system&op=save_delivery',
    method: 'post',
    data
  })
}
