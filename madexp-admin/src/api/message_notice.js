import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms',
    method: 'post',
    data
  })
}



//文章$
export function fetchCmsInit(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms_init',
    method: 'post',
    data
  })
}

export function fetchInfo(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms_info',
    method: 'post',
    data
  })
}

export function fetchSave(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms_save',
    method: 'post',
    data
  })
}

export function fetchDel(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms_del',
    method: 'post',
    data
  })
}


//分类

export function fetchClassList(data) {
  return request({
    url: 'index.php?act=message_notice&op=index',
    method: 'post',
    data
  })
}

export function fetchClassInfo(data) {
  return request({
    url: 'index.php?act=message_notice&op=get_msg_info',
    method: 'post',
    data
  })
}

export function fetchClassSave(data) {
  return request({
    url: 'index.php?act=message_notice&op=notice_cms_class_save',
    method: 'post',
    data
  })
}
