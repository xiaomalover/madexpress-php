import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=merchant&op=get_list',
    method: 'post',
    data
  })
}

export function fetchDetails(data) {
  return request({
    url: 'index.php?act=merchant&op=adminInfo',
    method: 'post',
    data
  })
}

export function fetchMavinSave(data) {
  return request({
    url: 'index.php?act=merchant&op=mavinSave',
    method: 'post',
    data
  })
}

export function fetchMavinDel(data) {
  return request({
    url: 'index.php?act=merchant&op=adminDel',
    method: 'post',
    data
  })
}

export function fetchMavinToken(data) {
  return request({
    url: 'index.php?act=merchant&op=mavin_token',
    method: 'post',
    data
  })
}

export function fetchMavinEdit(data) {
  return request({
    url: 'index.php?act=merchant&op=adminState',
    method: 'post',
    data
  })
}
