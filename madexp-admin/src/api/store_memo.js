import request from '@/utils/request'

export function login(data) {
	console.log(data);
  return request({
    url: 'index.php?act=login&op=login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: 'index.php?act=admin&op=index',
    method: 'post',
    token
  })
}

export function logout() {
  return request({
    url: 'index.php?act=admin&op=logout',
    method: 'post'
  })
}
