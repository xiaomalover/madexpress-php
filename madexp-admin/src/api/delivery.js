import request from '@/utils/request'

export function fetchDeliveryList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_list',
    method: 'post',
    data
  })
}

export function fetchDeliveryDetails(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_info',
    method: 'post',
    data
  })
}

// 订单列表
export function fetchDeliveryOrderList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_order',
    method: 'post',
    data
  })
}

// 租赁记录
export function fetchDeliveryLeaseList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_lease',
    method: 'post',
    data
  })
}

// 反馈记录
export function fetchDeliveryFeedbackList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_feedback',
    method: 'post',
    data
  })
}

// 事件记录类型（分类 feedback的feedback_type）
export function fetchDeliveryFeedbackTpyeList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_feedback_type',
    method: 'post',
    data
  })
}

// 排班记录
export function fetchDeliveryShedulingList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_sheduling',
    method: 'post',
    data
  })
}

// 新增新的配送员
export function fetchDeliverySave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_save',
    method: 'post',
    data
  })
}

// 保存配送员信息
export function fetchDeliveryBaseSave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_base_save',
    method: 'post',
    data
  })
}



// 文件编辑
export function fetchFileEdit(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_file_edit',
    method: 'post',
    data
  })
}

// 基础信息
export function fetchDeliveryBase(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_basic',
    method: 'post',
    data
  })
}

// 统计信息
export function fetchDeliveryStat(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_stat',
    method: 'post',
    data
  })
}

// 修改问题状态
export function fetchDeliveryProblem(data) {
  return request({
    url: 'index.php?act=delivery&op=editProblem',
    method: 'post',
    data
  })
}

// 修改锁定状态
export function fetchDeliveryLock(data) {
  return request({
    url: 'index.php?act=delivery&op=lock_state',
    method: 'post',
    data
  })
}

// 修改接单状态
export function fetchDeliveryLine(data) {
  return request({
    url: 'index.php?act=delivery&op=line_state',
    method: 'post',
    data
  })
}








// 钱包详情
export function fetchWalletBase(data) {
  return request({
    url: 'index.php?act=delivery_wallet&op=wallet_info',
    method: 'post',
    data
  })
}




//钱包流水
export function fetchFinanceTypeList(data) {
  return request({
    url: 'index.php?act=delivery_wallet&op=get_type',
    method: 'post',
    data
  })
}


export function fetchFinanceList(data) {
  return request({
    url: 'index.php?act=delivery_wallet&op=get_activities',
    method: 'post',
    data
  })
}



export function fetchInvoiceList(data) {
  return request({
    url: 'index.php?act=delivery_wallet&op=invoice_list',
    method: 'post',
    data
  })
}

export function fetchInvoiceType(data) {
  return request({
    url: 'index.php?act=delivery_wallet&op=invoice_type',
    method: 'post',
    data
  })
}
