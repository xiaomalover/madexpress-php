import request from '@/utils/request'

// 获取区域
export function getRegionList() {
  return request({
    url: 'index.php?act=sorting&op=get_region',
    method: 'post'
  })
}

// 获取商铺
export function getStoreList(data) {
  return request({
    url: 'index.php?act=sorting&op=get_store',
    method: 'post',
    data
  })
}

// 获取配送员详情
export function getStoreItem(data) {
  return request({
    url: 'index.php?act=sorting&op=get_store_item',
    method: 'post',
    data
  })
}

// 获取配送员
export function getDeliveryList(data) {
  return request({
    url: 'index.php?act=sorting&op=get_delivery',
    method: 'post',
    data
  })
}

// 获取配送员详情
export function getDeliveryItem(data) {
  return request({
    url: 'index.php?act=sorting&op=get_delivery_item',
    method: 'post',
    data
  })
}

// 获取商家订单
export function getStoreOrderList(data) {
  return request({
    url: 'index.php?act=sorting&op=get_store_order',
    method: 'post',
    data
  })
}

// 获取订单详情
export function getOrderItem(data) {
  return request({
    url: 'index.php?act=sorting&op=get_order_item',
    method: 'post',
    data
  })
}

// 获取订单详情
export function getOrderItembycoordinates(data) {
  return request({
    url: 'index.php?act=sorting&op=get_order_list_bycoordinates',
    method: 'post',
    data
  })
}

// 获取订单数据统计
export function getAllocationTotal(data) {
  return request({
    url: 'index.php?act=sorting&op=sorting_total',
    method: 'post',
    data
  })
}

// 获取钱标志
export function getAllocationMoney(data) {
  return request({
    url: 'index.php?act=sorting&op=sorting_money',
    method: 'post',
    data
  })
}

// 获取默认所需数据
export function getAllocationData(data) {
  return request({
    url: 'index.php?act=sorting&op=sorting_ini',
    method: 'post',
    data
  })
}

// 搜索数据
export function getAllocationSearch(data) {
  return request({
    url: 'index.php?act=sorting&op=search',
    method: 'post',
    data
  })
}

// 问题单
export function getAllocationProblemList(data) {
  return request({
    url: 'index.php?act=sorting&op=problem_list',
    method: 'post',
    data
  })
}

// 问题单
export function getAllocationMapSetting(data) {
  return request({
    url: 'index.php?act=sorting&op=map_setting',
    method: 'post',
    data
  })
}

//获取钱圈围栏

export function getAllocationMapSector(data) {
  return request({
    url: 'index.php?act=sorting&op=money_list',
    method: 'post',
    data
  })
}
