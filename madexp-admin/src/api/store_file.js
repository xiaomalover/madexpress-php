import request from '@/utils/request'

export function fetchStoreFile(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=index',
    method: 'post',
    data
  })
}