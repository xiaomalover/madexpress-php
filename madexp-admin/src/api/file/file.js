import request from '@/utils/request'


//获取区域
export function getListFile(data) {
  return request({
    url: 'index.php?act=file_browse&op=file',
    method: 'post',
    data
  })
}

export function createFolders(data) {
  return request({
    url: 'index.php?act=file_browse&op=create',
    method: 'post',
    data
  })
}

export function deleteResources(data) {
  return request({
    url: 'index.php?act=file_browse&op=delete',
    method: 'post',
    data
  })
}



