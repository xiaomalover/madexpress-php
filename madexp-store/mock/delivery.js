const Mock = require('mockjs')

const List = []
const count = 100

const baseContent = '<p>I am testing data, I am testing data.</p><p><img src="https://wpimg.wallstcn.com/4c69009c-0fd4-4153-b112-6cb53d1cf943"></p>'
const image_uri = 'https://wpimg.wallstcn.com/e4558086-631c-425c-9430-56ffb46e70b3'

for (let i = 0; i < count; i++) {
  List.push(Mock.mock({
		distributor_state: '@increment',
		distributor_name:'@first',
		region_name:'@first',
		distributor_id:'@increment',
		order_state_take:'@float(0, 100, 2, 2)',
		order_state_send:'@float(0, 100, 2, 2)',
		order_state_success:'@float(0, 100, 2, 2)',
		order_state_canal:'@float(0, 100, 2, 2)',
		order_state_time:'@float(0, 100, 2, 2)',		
		waiter_region:'@float(0, 100, 2, 2)',
		evaluate_rate:'@float(0, 100, 2, 2)',
		timestamp: +Mock.Random.date('T'),
	
    
  }))
}

module.exports = [
  {
    url: '/vue-element-admin/delivery/list',
    type: 'post',
    response: config => {
      const { importance, type, title, page = 1, limit = 20, sort } = config.query

      let mockList = List.filter(item => {
        if (importance && item.importance !== +importance) return false
        if (type && item.type !== type) return false
        if (title && item.title.indexOf(title) < 0) return false
        return true
      })

      if (sort === '-id') {
        mockList = mockList.reverse()
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },

  {
    url: '/vue-element-admin/delivery/detail',
    type: 'get',
    response: config => {
      const { id } = config.query
      for (const article of List) {
        if (article.id === +id) {
          return {
            code: 20000,
            data: article
          }
        }
      }
    }
  },

]

