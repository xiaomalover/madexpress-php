import Cookies from 'js-cookie'
const TokenKey = 'Store-Token'

export function getToken() {
  return Cookies.get(TokenKey + sessionStorage.getItem( 'username' ))
}

export function setToken(token,username) {
  return Cookies.set(TokenKey + username, token)
}

export function removeToken(username) {
  return Cookies.remove(TokenKey + username)
}
