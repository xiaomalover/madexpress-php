import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */


/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)',
      component: () => import('@/views/redirect/index')
    }]
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    children: [{
      path: 'store_dashboard',
      component: () => import('@/views/store_dashboard/index'),
      name: 'store_dashboard',
      meta: {
        title: '运营数据',
        icon: 'icon_yunyingshuju',
        affix: true
      }
    }]
  },


]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [


  {

    path: '/store_order',
    component: Layout,
    redirect: '/store_order/index',
    name: 'order',
    meta: {
      title: '订单管理',
      icon: 'el-icon-s-help'
    },
    children: [{
        path: 'index',
        component: () => import('@/views/store_order/index'),
        name: 'orderindex',
        meta: {
          title: '订单管理',
          icon: 'icon_dingdanguanli',
          affix: false
        }
      },
      {
        path: 'details/:id(\\d+)',
        component: () => import('@/views/store_order/details'),
        name: 'orderdetails',
        meta: {
          title: '订单详情',
          activeMenu: '/store_order/index'
        },
        hidden: true
      },
    ]
  },

  {

    path: '/store_goods',
    component: Layout,
    redirect: '/store_goods/index',
    name: 'store_goods',
    meta: {
      title: '商品管理',
      icon: 'icon_shangjiaguanli'
    },
    children: [{
        path: 'index',
        component: () => import('@/views/store_goods/index'),
        name: 'store_goods',
        meta: {
          title: '商品管理',
          icon: 'icon_shangjiaguanli',
          affix: false
        }
      }, {
        path: 'options',
        component: () => import('@/views/store_goods/options'),
        name: 'options',
        meta: {
          title: '配件商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }, {
        path: 'info',
        component: () => import('@/views/store_goods/info'),
        name: 'info',
        meta: {
          title: '商品详情',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }, {
        path: 'add_goods',
        component: () => import('@/views/store_goods/add_goods'),
        name: 'add_goods',
        meta: {
          title: '添加商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }, {
        path: 'edit_goods/:id(\\d+)',
        component: () => import('@/views/store_goods/edit_goods'),
        name: 'edit_goods',
        meta: {
          title: '编辑商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }, {
        path: 'xls_goods',
        component: () => import('@/views/store_goods/xls_goods'),
        name: 'xls_goods',
        meta: {
          title: '导入商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }, {
        path: 'xls_goods_edit/:id(\\d+)',
        component: () => import('@/views/store_goods/xls_goods_edit'),
        name: 'xls_goods_edit',
        meta: {
          title: '导入商品编辑',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      },
      {
        path: 'meal_list',
        component: () => import('@/views/store_goods/meal_list'),
        name: 'meal_list',
        meta: {
          title: '套餐管理',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      },
      {
        path: 'add_meal_goods',
        component: () => import('@/views/store_goods/add_meal_goods'),
        name: 'add_meal_goods',
        meta: {
          title: '添加套餐商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      },
      {
        path: 'edit_meal_goods/:id(\\d+)',
        component: () => import('@/views/store_goods/edit_meal_goods'),
        name: 'edit_meal_goods',
        meta: {
          title: '编辑套餐商品',
          noCache: true,
          activeMenu: '/store_goods/index'
        },
        hidden: true
      }
    ]


  },


  {
    path: '/store_foodbox',
    component: Layout,
    children: [{
      path: 'store_foodbox',
      component: () => import('@/views/store_foodbox/index'),
      name: 'store_foodbox',
      meta: {
        title: '餐盒管理',
        icon: 'icon_canheguanli',
        affix: false
      }
    }]

  },

  {

    path: '/store_finance',
    component: Layout,
    redirect: '/store_finance/index',
    name: 'store_finance',
    meta: {
      title: '财务管理',
      icon: 'icon_caiwu'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/store_finance/index'),
      name: 'store_finance',
      meta: {
        title: '财务管理',
        icon: 'icon_caiwu',
        affix: false
      }
    }, {
      path: 'wallet_activities',
      component: () => import('@/views/store_finance/wallet_activities'),
      name: 'wallet_activities',
      meta: {
        title: 'Wallet Activities',
        noCache: true,
        activeMenu: '/store_finance/index'
      },
      hidden: true
    }, ]




  },


  //评价
  {
    path: '/store_evaluate',
    component: Layout,
    redirect: '/store_evaluate/index',
    name: 'store_evaluate',
    meta: {
      title: '店铺评价',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/store_evaluate/index'),
      name: 'scheduling',
      meta: {
        title: '店铺评价',
        icon: 'icon_pingjia',
        affix: false
      }
    }, {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/store_evaluate/info'),
      name: 'info',
      meta: {
        title: '评价详情',
        noCache: true,
        activeMenu: '/store_evaluate/index'
      },
      hidden: true
    }, ]


  },
  {

    path: '/store_voucher',
    component: Layout,
    redirect: '/store_voucher/index',
    name: 'store_voucher',
    meta: {
      title: '优惠券管理',
      icon: 'icon_caiwu'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/store_voucher/index'),
      name: 'store_finance',
      meta: {
        title: '优惠券管理',
        icon: 'icon_caiwu',
        affix: false
      }
    }, {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/store_voucher/info'),
      name: 'info',
      meta: {
        title: '优惠券详情',
        noCache: true,
        activeMenu: '/store_voucher/index'
      },
      hidden: true
    }]

  },

  {
    path: '/store_memo',
    component: Layout,
    children: [{
      path: 'store_memo',
      component: () => import('@/views/store_memo/index'),
      name: 'store_memo',
      meta: {
        title: '备忘录',
        icon: 'icon_beiwanglu',
        affix: false
      }
    }]

  },

  {
    path: '/store_examine',
    component: Layout,
    children: [{
      path: 'store_examine',
      component: () => import('@/views/store_examine/index'),
      name: 'store_examine',
      meta: {
        title: '审批管理',
        icon: 'icon_shenhe',
        affix: false
      }
    }]

  },

  {
    path: '/store_file',
    component: Layout,
    redirect: '/store_file/index',
    name: 'store_file',
    meta: {
      title: '文件管理',
      icon: 'icon_wenjian'
    },
    children: [{
      path: 'store_file',
      component: () => import('@/views/store_file/index'),
      name: 'store_file',
      meta: {
        title: '文件管理',
        icon: 'icon_wenjian',
        affix: false
      }
    }, {
      path: 'list/:type',
      component: () => import('@/views/store_file/list'),
      name: 'store_file_list',
      meta: {
        title: '文件列表',
        noCache: true,
        activeMenu: '/store_file/index'
      },
      hidden: true
    }]

  },

  {
    path: '/store_customer',
    component: Layout,
    children: [{
      path: 'store_customer',
      component: () => import('@/views/store_customer/index'),
      name: 'store_customer',
      meta: {
        title: '联系客服',
        icon: 'icon_kefu',
        affix: false
      }
    }]

  },  



  // 404 page must be placed at the end !!!
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
