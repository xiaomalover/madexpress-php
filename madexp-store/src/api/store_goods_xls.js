import request from '@/utils/request'

//商品
export function fetchXlsExcelClassList(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=goods_excel_class',
    method: 'post',
    data
  })
}



//xls商品
export function fetchXlsGoodsList(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=get_goods_list',
    method: 'post',
    data
  })
}

export function fetchGoodsInfo(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=get_goods_info',
    method: 'post',
    data
  })
}


//保存
export function fetchGoodsSave(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=save_goods',
    method: 'post',
    data
  })
}


//xls 删除商品
export function fetchXlsGoodsDel(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=xls_goods_del',
    method: 'post',
    data
  })
}

//xls 保存商品
export function fetchXlsGoodsSave(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=xls_goods_save',
    method: 'post',
    data
  })
}


//xls 清空商品
export function fetchXlsEmpty(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=excel_empty',
    method: 'post',
    data
  })
}

//xls 导入商品
export function fetchXlsImport(data) {
  console.log(data);
  return request({
    url: 'index.php?act=store_goods_xls&op=excel_import',
    method: 'post',
    data
  })
}


//xls 商品
export function fetchXlsNum(data) {
  return request({
    url: 'index.php?act=store_goods_xls&op=get_xls_init',
    method: 'post',
    data
  })
}
