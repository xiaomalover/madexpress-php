import request from '@/utils/request'

export function fetchEvaList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_evaluate&op=evaluate_list',
    method: 'post',
    data
  })
}

export function getEvaInfo(data) {
  return request({
    url: 'index.php?act=store_evaluate&op=evaluate_info',
    method: 'post',
    data
  })
}

export function saveStoreEva(data) {
  return request({
    url: 'index.php?act=store_evaluate&op=save_eva',
    method: 'post',
    data
  })
}

//统计
export function fetchStoreEvaStat(data) {
  return request({
    url: 'index.php?act=store_evaluate&op=save_eva',
    method: 'post',
    data
  })
}
