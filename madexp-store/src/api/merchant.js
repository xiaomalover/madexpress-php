import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=merchant&op=get_list',
    method: 'post',
   data
  })
}

export function fetchDetails(id) {
  return request({
    url: 'index.php?act=member&op=member_info',
    method: 'post',
    params: { id }
  })
}

