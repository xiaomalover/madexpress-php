import request from '@/utils/request'

export function fetchBaseInfo(data) {
  return request({
    url: 'index.php?act=store_stripe&op=base_info',
    method: 'post',
    data
  })
}


export function fetchBaseSave(data) {
  return request({
    url: 'index.php?act=store_stripe&op=save_stripe',
    method: 'post',
    data
  })
}


export function fetchBankInfo(data) {
  return request({
    url: 'index.php?act=store_stripe&op=bank_info',
    method: 'post',
    data
  })
}



export function fetchBankSave(data) {
  return request({
    url: 'index.php?act=store_stripe&op=save_bank',
    method: 'post',
    data
  })
}



export function fetchBankDel(data) {
  return request({
    url: 'index.php?act=store_stripe&op=del_bank',
    method: 'post',
    data
  })
}




export function fetchPersonsInfo(data) {
  return request({
    url: 'index.php?act=store_stripe&op=get_persons',
    method: 'post',
    data
  })
}



export function fetchPersonsSave(data) {
  return request({
    url: 'index.php?act=store_stripe&op=save_persons',
    method: 'post',
    data
  })
}
