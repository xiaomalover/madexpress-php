import request from '@/utils/request'

export function fetchClassList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_customer&op=customer_class',
    method: 'post',
    data
  })
}

export function fetchList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_customer&op=get_customer_list',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  console.log(data)
  return request({
    url: 'index.php?act=store_customer&op=feedback_info',
    method: 'post',
    data
  })
}