import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=store_order&op=get_order_list',
    method: 'post',
    data
  })
}


export function fetchOrderTab(data) {
  return request({
    url: 'index.php?act=store_order&op=get_order_tab',
    method: 'post',
    data
  })
}



export function fetchOrderDetail(data) {
  return request({
    url: 'index.php?act=store_order&op=order_info',
    method: 'post',
    data
  })
}


export function fetchDeliverySearch(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_search_list',
    method: 'post',
    data
  })
}



//ceo回复评价
export function fetchSaveCeoEva(data) {
  return request({
    url: 'index.php?act=store_order&op=save_ceo_eva',
    method: 'post',
    data
  })
}

//总台处理投诉反馈回复

export function fetchSaveFeedback(data) {
  return request({
    url: 'index.php?act=store_order&op=save_feedback_reply',
    method: 'post',
    data
  })
}


export function fetchOrderHistory(data) {
  return request({
    url: 'index.php?act=store_order&op=order_history',
    method: 'post',
    data
  })
}



//获取当前订单的退款数据
export function fetchOrderRefund(data) {
  return request({
    url: 'index.php?act=store_order&op=order_refund',
    method: 'post',
    data
  })
}




//回复反馈的问题
export function fetchSaveProblem(data) {
  return request({
    url: 'index.php?act=store_order&op=save_problem',
    method: 'post',
    data
  })
}


//获取商品操作。
export function fetchOrderGongju(data) {
  return request({
    url: 'index.php?act=store_order_gongju&op=order_info',
    method: 'post',
    data
  })
}

//获取商品操作。
export function storeOrderRefund(data) {
  return request({
    url: 'index.php?act=store_refund&op=save_refund',
    method: 'post',
    data
  })
}



//切换record
export function fetchRecordList(data) {
  return request({
    url: 'index.php?act=store_order&op=record_socket_list',
    method: 'post',
    data
  })
}




//编辑订单
export function fetchEditOrder(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=order_info',
    method: 'post',
    data
  })
}


//保存编辑订单
export function fetchSaveOrder(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=save_order',
    method: 'post',
    data
  })
}


//取消编辑订单
export function fetchCancelOrder(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=cancel_order_edit',
    method: 'post',
    data
  })
}


//商品数量触发退款
export function fetchRefundGoodsPlus(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=plus_goods',
    method: 'post',
    data
  })
}

export function fetchRefundGoodsReduce(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=reduce_goods',
    method: 'post',
    data
  })
}







//撤销整体退款
export function fetchRefundGoodsDel(data) {
  return request({
    url: 'index.php?act=store_order_edit&op=del_refund_goods',
    method: 'post',
    data
  })
}



//新增赠品
export function fetchGiveGoods(data) {
  return request({
    url: 'index.php?act=store_order_give&op=save_give',
    method: 'post',
    data
  })
}


//删除赠品
export function fetchDelGiveGoods(data) {
  return request({
    url: 'index.php?act=store_order_give&op=delGive',
    method: 'post',
    data
  })
}


//待替换商品信息
export function fetchOrderReplaceGoods(data) {
  return request({
    url: 'index.php?act=store_order_replace&op=order_goods_info',
    method: 'post',
    data
  })
}

//替换商品列表
export function fetchOrderReplaceList(data) {
  return request({
    url: 'index.php?act=store_order_replace&op=replace_list',
    method: 'post',
    data
  })
}


//替换商品列表
export function fetchOrderSaveReplace(data) {
  return request({
    url: 'index.php?act=store_order_replace&op=save_replace',
    method: 'post',
    data
  })
}

//确认替换
export function fetchSaveReplace(data) {
  return request({
    url: 'index.php?act=store_order_replace&op=save_order_replace',
    method: 'post',
    data
  })
}



//撤销替换
export function fetchDelReplace(data) {
  return request({
    url: 'index.php?act=store_order_replace&op=del_replace',
    method: 'post',
    data
  })
}



//退款数据
export function fetchRefundOrderInfo(data) {
  return request({
    url: 'index.php?act=store_refund&op=order_info',
    method: 'post',
    data
  })
}
