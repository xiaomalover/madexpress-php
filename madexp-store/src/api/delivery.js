import request from '@/utils/request'

export function fetchDeliveryList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_list',
    method: 'post',
    data
  })
}

export function fetchDeliveryDetails(id) {
  return request({
    url: 'index.php?act=delivery&op=delivery_info',
    method: 'post',
    params: { id }
  })
}


//订单列表
export function fetchDeliveryOrderList(query) {
  return request({
    url: 'index.php?act=delivery&op=delivery_order',
    method: 'post',
    params: query
  })
}


//租赁记录
export function fetchDeliveryLeaseList(query) {
  return request({
    url: 'index.php?act=delivery&op=delivery_lease',
    method: 'post',
    params: query
  })
}

//反馈记录
export function fetchDeliveryFeedbackList(query) {
  return request({
    url: 'index.php?act=delivery&op=delivery_feedback',
    method: 'post',
    params: query
  })
}

//排班记录
export function fetchDeliveryShedulingList(query) {
  return request({
    url: 'index.php?act=delivery&op=delivery_sheduling',
    method: 'post',
    params: query
  })
}


//新增新的配送员
export function fetchDeliverySave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_save',
    method: 'post',
    data
  })
}

//文件编辑
export function fetchFileSave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_base_save',
    method: 'post',
    data
  })
}



//基础信息
export function fetchDeliveryBase(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_basic',
    method: 'post',
    data
  })
}

