import request from '@/utils/request'


export function fetchVoucherList(data) {
  return request({
    url: 'index.php?act=store_voucher&op=index',
    method: 'post',
    data
  })
}


export function fetchVoucherDetail(data) {
  return request({
    url: 'index.php?act=store_voucher&op=order_info',
    method: 'post',
    data
  })
}


export function fetchVoucherSave(data) {
  return request({
    url: 'index.php?act=store_voucher&op=coupons_save',
    method: 'post',
    data
  })
}
