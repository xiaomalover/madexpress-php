import request from '@/utils/request'


export function fetchTaskList(data) {
  return request({
    url: 'index.php?act=task&op=index',
    method: 'post',
	data
  })
}


