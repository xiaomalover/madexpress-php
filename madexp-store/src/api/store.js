import request from '@/utils/request'


export function fetchList(data) {
  return request({
    url: 'index.php?act=store&op=store_list',
    method: 'post',
   data
  })
}


export function fetchClassList(data) {
  return request({
    url: 'index.php?act=store&op=store_class',
    method: 'post',
    data
  })
}





//保存商家
export function fetchStoreSave(data) {
  return request({
    url: 'index.php?act=store&op=store_save',
    method: 'post',
    data
  })
}

