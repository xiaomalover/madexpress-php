import request from '@/utils/request'

export function fetchStoreFile(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=index',
    method: 'post',
    data
  })
}


export function fetchFileList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=store_file_list',
    method: 'post',
    data
  })
}


export function fetchFileDel(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=store_file_del',
    method: 'post',
    data
  })
}

//合同详情
export function fetchContractEdit(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=contract_edit',
    method: 'post',
    data
  })
}

//合同详情
export function fetchLicenseEdit(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=license_edit',
    method: 'post',
    data
  })
}

//合同详情
export function fetchHygieneEdit(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=hygiene_edit',
    method: 'post',
    data
  })
}

//bill
export function fetchBillEdit(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_file&op=bill_edit',
    method: 'post',
    data
  })
}
