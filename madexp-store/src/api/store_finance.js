import request from '@/utils/request'
//
export function fetchFinanceTypeList(data) {
  return request({
    url: 'index.php?act=store_finance&op=get_type',
    method: 'post',
    data
  })
}

//
export function fetchFinanceList(data) {
  return request({
    url: 'index.php?act=store_finance&op=get_activities',
    method: 'post',
    data
  })
}

//获取钱包基本数据

export function fetchFinanceBase(data) {
  return request({
    url: 'index.php?act=store_finance&op=finance_base',
    method: 'post',
    data
  })
}


//获取票据列表
export function fetchInvoiceType(data) {
  return request({
    url: 'index.php?act=store_finance&op=invoice_type',
    method: 'post',
    data
  })
}

//获取票据列表
export function fetchInvoiceList(data) {
  return request({
    url: 'index.php?act=store_finance&op=invoice_list',
    method: 'post',
    data
  })
}


//保存提现
export function fetchCashSave(data) {
  return request({
    url: 'index.php?act=store_finance&op=cashAdd',
    method: 'post',
    data
  })
}

//获取钱包基本信息
export function fetchWalletInfo(data) {
  return request({
    url: 'index.php?act=store_finance&op=wallet_info',
    method: 'post',
    data
  })
}
