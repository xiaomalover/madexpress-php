import request from '@/utils/request'


//数据统计
export function fetchStat(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=stat',
    method: 'post',
    data
  })
}

//热销商品&餐盒数据
export function fetchHotFoodbox(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=hotgoods_foodbox',
    method: 'post',
    data
  })
}




//店铺信息
export function fetchStoreInfo(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=store_info',
    method: 'post',
    data
  })
}


//保存TIME
export function fetchStoreTimeSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=time_save',
    method: 'post',
    data
  })
}

//保存TEL
export function fetchStoreTelSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=tel_save',
    method: 'post',
    data
  })
}

//保存CLASS
export function fetchStoreClassSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=class_save',
    method: 'post',
    data
  })
}


//保存ADDRESS
export function fetchStoreAddressSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=address_save',
    method: 'post',
    data
  })
}

//保存ADDRESS
export function fetchStoreAboutSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=about_save',
    method: 'post',
    data
  })
}


//保存ADDRESS
export function fetchStoreNoticeSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=notice_save',
    method: 'post',
    data
  })
}






//保存保存简介
export function fetchStoreIntroduceSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=about_save',
    method: 'post',
    data
  })
}


//审核
export function fetchStoreExamine(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=store_examine',
    method: 'post',
    data
  })
}

//更新预计送达时间
export function fetchStoreEstimated(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=save_estimated_time',
    method: 'post',
    data
  })
}



//更新预计送达时间
export function fetchStoreTaking(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=save_taking',
    method: 'post',
    data
  })
}


//更新专员
export function fetchStoreMavinSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=save_mavin',
    method: 'post',
    data
  })
}

//专员列表
export function fetchStoreMavinList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=mavin_list',
    method: 'post',
    data
  })
}

//保存语言
export function fetchStoreLangSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=lang_save',
    method: 'post',
    data
  })
}


//保存指引图
export function fetchStoreGuideSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=guide_map_save',
    method: 'post',
    data
  })
}

// 上传店铺照片
export function fetchStoreBannerUploadSave(data) {
  console.log(data)
  return request({
    url: 'index.php?act=store_index&op=banner_upload',
    method: 'post',
    data
  })
}



//保存指引图
export function fetchStoreBannerSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=banner_save',
    method: 'post',
    data
  })
}


//保存头像
export function fetchStoreAvatarSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=avatar_save',
    method: 'post',
    data
  })
}

//地址搜索
export function fetchSearchAddress(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=get_mapbox_address',
    method: 'post',
    data
  })
}

//获取region16边形区域
export function fetchRegion16(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_index&op=get_region16',
    method: 'post',
    data
  })
}


//确认

export function fetchStripeAccept(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_stripe&op=acceptance',
    method: 'post',
    data
  })
}




