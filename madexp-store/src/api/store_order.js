import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=store_order&op=get_order_list',
    method: 'post',
    data
  })
}


export function fetchOrderTab(data) {
  return request({
    url: 'index.php?act=store_order&op=get_order_tab',
    method: 'post',
    data
  })
}



export function fetchOrderDetail(data) {
  return request({
    url: 'index.php?act=store_order&op=order_info',
    method: 'post',
    data
  })
}


export function fetchDeliverySearch(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_search_list',
    method: 'post',
    data
  })
}



//ceo回复评价
export function fetchSaveCeoEva(data) {
  return request({
    url: 'index.php?act=store_order&op=save_ceo_eva',
    method: 'post',
    data
  })
}

//总台处理投诉反馈回复

export function fetchSaveFeedback(data) {
  return request({
    url: 'index.php?act=store_order&op=save_feedback_reply',
    method: 'post',
    data
  })
}


export function fetchOrderHistory(data) {
  return request({
    url: 'index.php?act=store_order&op=order_history',
    method: 'post',
    data
  })
}



//获取当前订单的退款数据
export function fetchOrderRefund(data) {
  return request({
    url: 'index.php?act=store_order&op=order_refund',
    method: 'post',
    data
  })
}




//回复反馈的问题
export function fetchSaveProblem(data) {
  return request({
    url: 'index.php?act=store_order&op=save_problem',
    method: 'post',
    data
  })
}


//切换record
export function fetchRecordList(data) {
  return request({
    url: 'index.php?act=store_order&op=record_socket_list',
    method: 'post',
    data
  })
}



export function fetchOrderAccept(data) {
  return request({
    url: 'index.php?act=store_order&op=order_accept',
    method: 'post',
    data
  })
}
