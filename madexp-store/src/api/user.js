import request from '@/utils/request'

export function login(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_login&op=login',
    method: 'post',
    data
  })
}

export function autologin(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_login&op=autologin',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  return request({
    url: 'index.php?act=store_login&op=info',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: 'index.php?act=store_logout&op=index',
    method: 'post'
  })
}
