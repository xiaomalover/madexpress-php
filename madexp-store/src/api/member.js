import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=member&op=member_list',
    method: 'post',
   data
  })
}

export function fetchDetails(data) {
  return request({
    url: 'index.php?act=member&op=member_info',
    method: 'post',
    data
  })
}


export function updateMember(data) {
  return request({
    url: '/vue-element-admin/member/update',
    method: 'post',
    data
  })
}


//优惠券
export function fetchCouponList(data) {
  return request({
    url: 'index.php?act=member&op=member_coupon',
    method: 'post',
    data
  })
}

//我的收藏
export function fetchLikeList(data) {
  return request({
		url: 'index.php?act=member&op=member_like',
		method: 'post',
		data
  })
}

//反馈记录
export function fetchFeedbackList(data) {
  return request({
		url: 'index.php?act=member&op=member_feedback',
		method: 'post',
		data
  })
}

//订单列表
export function fetchOrderList(data) {
  return request({
			url: 'index.php?act=member&op=member_order',
			method: 'post',
			data
  })
}