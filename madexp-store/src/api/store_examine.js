import request from '@/utils/request'

export function fetchExamineList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_examine&op=examine_list',
    method: 'post',
    data
  })
}


export function fetchExamineInfo(data) {
  return request({
    url: 'index.php?act=store_examine&op=examine_info',
    method: 'post',
    data
  })
}

//tongji
export function fetchExamineStat(data) {
  return request({
    url: 'index.php?act=store_examine&op=examine_stat',
    method: 'post',
    data
  })
}

//审核
export function fetchExamineState(data) {
  return request({
    url: 'index.php?act=store_examine&op=examine_state',
    method: 'post',
    data
  })
}
