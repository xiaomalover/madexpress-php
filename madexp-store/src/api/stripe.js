import request from '@/utils/request'



//获取商户列表
export function getAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=get_accounts',
    method: 'post',
    data
  })
}


//创建子账号
export function createAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=create_accounts',
    method: 'post',
    data
  })
}


//修改子账号
export function updateAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=update_accounts',
    method: 'post',
    data
  })
}


//删除子账号
export function delAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=del_accounts',
    method: 'post',
    data
  })
}




//创建银行卡

export function createBankAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=create_bank_account',
    method: 'post',
    data
  })
}

//获取银行卡

export function getBankAccounts(data) {
  return request({
    url: 'index.php?act=stripe&op=get_bank_account',
    method: 'post',
    data
  })
}