<?php
/**
 * 车辆管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class vehicleControl extends mobileAdminControl{
  

	//1:空闲 2:使用中 3:维护中 4:待还车 5:召回中 6:过期'
	private $vehicleState = array(
	    10 => '空闲中',
	    20 => '使用中',
	    30 => '维护中',
	    40 => '待还车',
	    50 => '召回中',
	    60 => '过期',
		70 => '暂停',
	);
	
	private $vehicleStateColor = array(
		10 => '#A2CDF3',
		20 => '#EEF1F6',
		30 => '#EEF1F6',
		40 => '#EEF1F6',
		50 => '#EEF1F6',
		60 => '#EEF1F6',
		70 => '#EEF1F6',
	);
	


	public function __construct(){
        parent::__construct();   
    }


	//车辆分类
	public function vehicle_class_listOp(){		
		
		$class_list = model('vehicle_class')->vehicleClassList(TRUE);		
		
		$list = array();
		foreach($class_list as $v){
			$v['vehicle_class_count'] = $this->vehicle_count($v['vehicle_class_id']);
			$list[] = $v;			
		}		
		output_data(array('class_list'=>$list));
		
	}
	
	private function vehicle_count($class_id){
		
		$count = model('vehicle')->getVehicleCount(array('vehicle_class_id' => $class_id));
		
		return $count;
		
	}
	
	//地区
	public function vehicle_regionOp(){
		
		$region = model('region')->getRegionList(array('region_type'=> 1,'region_enable' => 1));		
		
		$list = array();
		$list[0]['region_en'] = 'ALL';
		
		foreach($region as $k=> $v){
			$list[$k+1] = $v;
			
			
		}
		
		
		
		output_data(array('region' => $list));
		
	}
	


   /**
     * 添加车辆
     */
    public function vehicle_addOp()
    {   
				
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input" => $_POST["vehicle_class_id"], "require" => "true", "message" => '车辆分类不能为空'),
                array("input" => $_POST["vehicle_brand"], "require" => "true", "message" => '品牌名称不能为空'),
                array("input" => $_POST["vehicle_type"], "require" => "true", "message" => '车辆型号不能为空'),
                array("input" => $_POST["vehicle_number"], "require" => "true", "message" => '车牌号不能为空'),
                array("input" => $_POST["vehicle_code"], "require" => "true", "message" => '车辆编号不能为空'),
            );
            $error = $obj_validate->validate();
            if ($error != '') {	   
				output_error($error);
            } else {
                $insertData = array();
                $insertData['vehicle_class_id'] = $_POST['vehicle_class_id'];
                $insertData['vehicle_brand'] = $_POST['vehicle_brand'];
                $insertData['vehicle_type'] = $_POST['vehicle_type'];
                $insertData['vehicle_code'] = $_POST['vehicle_code'];
                $insertData['vehicle_number'] = $_POST['vehicle_number'];
                $insertData['update_time'] = time();
                $insertData['vehicle_state'] = 10;

                $return = Model('vehicle')->vehicleAdd($insertData);
                if ($return) {
					output_data('添加成功');
        		} else {
					output_error('添加失败');	  			   
                }
            }
      }
   
		
	
	//车辆列表
	public function vehicle_listOp(){
			
					
		$condition = array();
		if ($_POST['query'] != '') {
			$condition[$_POST['qtype']] = $_POST['query'];
		}
		$order = '';
		$param = array('request_id');
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
			$order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
		}
		
		$model_vehicle = model('vehicle');
			
		if($_POST['class_id'] > 0){				
			$condition['vehicle_class_id'] = $_POST['class_id'];
		}
		
		$data = $model_vehicle->vehicleList($condition, '*', $this->page, $order);		
	
		$page_count = $model_vehicle->gettotalpage();
		$list_count = $model_vehicle->gettotalnum();
		
			
		foreach ($data as $value) {
			$param = array();
			$operation = '';
			$param['vehicle_id'] = $value['vehicle_id'];			
			$param['vehicle_code'] = $value['vehicle_code'];
			$param['vehicle_number'] = $value['vehicle_number'];
			$param['vehicle_brand'] = $value['vehicle_brand'];
			$param['vehicle_type'] = $value['vehicle_type'];
		//	$lastMaintenance = new DateTime($value['maintenance_time']);
		//	$nowTime = new DateTime('now');
		//	$diffTime = $lastMaintenance->diff($nowTime);
			$param['maintenance_time'] = '0天'; //上次维护到现在过去的天数
			$param['usable_days'] = $value['usable_days'] ? ($value['usable_days']  - $diffTime->days) : '0天'; //剩余使用天数 = 可使用天数 - 上次维护到现在的天数
			$param['insurance_days'] = $value['insurance_days'] ? $value['insurance_days'].'天' : '0天' ;
			$param['distributor_name'] = $this->getWaiterLease($value['lease_id']);
			$param['vehicle_state'] = $value['vehicle_state'];
			$param['recovery_state'] = $value['recovery_state'];
			$param['vehicle_state_desc'] = $this->vehicleState[$value['vehicle_state']];	
			$param['vehicle_state_color'] = $this->vehicleStateColor[$value['vehicle_state']];
			
			if ($value['vehicle_state'] == 6) $param['vehicle_state_desc'] = '过期'.$value['delay_days'];			
			
			$list[] = $param;
		}
		
	
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
	
		
		
	}
	
	
	
	//租车记录
	
	private function getWaiterLease($lease_id){		
		$where = array(
			'lease_id' => $lease_id
		);
		$row =model('vehicle_lease')->getLeaseInfo($where);
		return $row['distributor_name'];
	}
	
	
	//车辆详情
	public function vehicle_infoOp(){
		
		
		
		$vehicle_id = $_POST['id'];		
		
		$where = array(
			'vehicle_id' => $vehicle_id
		);		
		$vehicle_info = model('vehicle')->vehicleInfo($where);	
		if($vehicle_info){
			$class = $this->getVehicleClass($vehicle_info['vehicle_class_id']);		
			$vehicle_info['class_icon']	 =  ADMIN_FIDES_URL . '/style/res/'.$class['vehicle_class_icon_active'];
		}
		
		
		
		//当前租赁记录
		$where = array(
			'vehicle_id' => $vehicle_id
		
		);
		$list =model('vehicle_lease')->getLeaseList($where,'*',15);
		$vehicle_lease = array();
		foreach($list as $k => $v){			
			$vehicle_lease[$k] = $v;		
			$vehicle_lease[$k]['lease_start_time']	 = date('d/m/y',$v['lease_start_time']);
			$vehicle_lease[$k]['lease_end_time']	 	 = date('d/m/y',$v['lease_start_time']);	
			$vehicle_lease[$k]['lease_days']	 	 =$v['lease_days'].'天';									
			
		}
				
		output_data(array('vehicle_info' => $vehicle_info,'vehicle_lease' => $vehicle_lease ));
		
		
		
	}
	
	
	
	//获取分类
	private function getVehicleClass($class_id){
		
		$class = model('vehicle_class')->vehicleClassInfo(array('vehicle_class_id'=>$class_id),'vehicle_class_icon,vehicle_class_icon_active');	
	
		return $class;
	}
	
	
	
	//车辆租赁记录
	
	public function vehicle_leaseOp(){
		
		
		$model_lease = model('vehicle_lease');
		$where = array(
			'vehicle_id' => $_POST['vehicle_id']
		);
		
		$lease_list =$model_lease->getLeaseList($where,'*',$this->page);	
		$list = array();		
		foreach($lease_list as $k => $v){			
			$list[$k] = $v;		
			$list[$k]['lease_start_time']	 = date('d/m/y',$v['lease_start_time']);
			$list[$k]['lease_end_time']	 	 = date('d/m/y',$v['lease_start_time']);
		}
		
		$page_count = $model_lease->gettotalpage();
		$list_count = $model_lease->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
				
	}
	
	
	
	//历史维护	
	public function vehicle_maintainOp(){		
		
		$where = array(
			'vehicle_id' => $_POST['vehicle_id']
		);
		$model_maintain = model('vehicle_maintain');
		$data = $model_maintain->vehicleMaintainList($where,'*',$this->page);
			
	
		$list = array();
		foreach($data as $k => $v){			
			$list[$k] = $v;		
			$list[$k]['add_time']	 = date('d/m/y H:i:s',$v['add_time']);
		
		}
	
		$page_count = $model_maintain->gettotalpage();
		$list_count = $model_maintain->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
		
				
	}
	
	
	
	//文件管理
	public function vehicle_fileOp(){
	
		$file_model = model('vehicle_file');
		$where = array(
			'vehicle_id' => $_POST['vehicle_id']
		);
	
		$file = $file_model->getVehicleFileList($where);
		$list = array();
		foreach($file as $k=> $v){
			$list[$k] =$v;
			$list[$k]['addtime'] = date('m/d/y',$v['addtime']);
			$list[$k]['file_state_time'] = date('m/d/y',$v['file_state_time']);
			$list[$k]['file_end_time'] = date('m/d/y',$v['file_end_time']);
			
		}
		
		$page_count = $file_model->gettotalpage();
		$list_count = $file_model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
		

	
	}
	
	
	//事件管理	
	public function vehicle_eventOp(){
		
		$model =model('vehicle_event');
		$where = array(		
				'vehicle_id' => $_POST['vehicle_id']
		);
		$datas = $model->vehicleEventList($where,'*',$this->page);
	
		$state = array(
			0 => '待处理',
			1 => '处理中',
			2 => '已完成'
		);
		$type = array(
			'check' => '检查',
			'repair'=> '维修',
			'maintainane'=> '保养',
			'refuel'=> '加油',
			'clean'=> '清洁',
			'fitting'=> '改装'
		);
		
		$list = array();
		foreach($datas as $k => $v){			
			$list[$k] = $v;
			$list[$k]['service_type'] = $type[$v['service_type']];
			$list[$k]['service_state'] = $state[$v['service_state']];
			$list[$k]['add_time']	 = date('d/m/y',$v['add_time']);
		
		}
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
		
		
	}
	
	
	
	
	/*	
	*租赁计划开始	
	*
	*/
	
	
	//租赁计划列表
	public function rental_listOp(){						
		$list = Model('VehicleRental')->rentalList();		
		output_data(array('list'=> $list));
	}
	
	
	//删除租赁计划
	public function rental_delOp(){		
		$row = Model('VehicleRental')->delRental(array('id'=>$_POST['id']));
		if($row){
			 Model('VehicleRentalRule')->delRule(array('rental_id'=> $_POST['id']));
			 output_data('删除成功');
			 
		}else{
			output_error('删除失败');
		}
	}
	
	//保存租赁计划
	public function rental_saveOp(){		
			
		 	$rentalModel = Model('VehicleRental');
			
			$rentalRuleModel = Model('VehicleRentalRule');
			
			if($_POST['id'] > 0){	
				
				$name = $_POST['name'];
				$type = $_POST['type'];
				$base_rental = $_POST['base_rental'];
				$penalty = $_POST['penalty'];
				$remarks = $_POST['remarks'];			
				$rentalInsertData = array(
					'name' => $name,
					'type' => $type,
					'base_rental' => $base_rental,
					'penalty' => $penalty,
					'remarks' => $remarks,
					'created_at' => date('Y-m-d H:i:s'),
				);
				$where = array(
					'id' => $_POST['id']
				);
				$row = $rentalModel->rentalEdit($where,$rentalInsertData);
				if($row){
				
					$rentalRuleModel->delRule(array('rental_id'=> $_POST['id']));
					$rules = $_POST['spec'];
					$ruleInsertData = array();
					foreach ($rules as $rule) {
						$ruleInsertData[] = array(
							'rental_id' => $_POST['id'],
							'duration_time' => $rule['time'],
							'ratio' => $rule['ratio'],
						);
					}
					$rentalRuleModel->AddAllRules($ruleInsertData);					
					output_data('编辑成功');					 
				}else{
					output_error('编辑失败');
					 
				}
				
			}else{
				
					$name = $_POST['name'];
					$type = $_POST['type'];
					$base_rental = $_POST['base_rental'];
					$penalty = $_POST['penalty'];
					$remarks = $_POST['remarks'];
				
					$rentalInsertData = [
						'name' => $name,
						'type' => $type,
						'base_rental' => $base_rental,
						'penalty' => $penalty,
						'remarks' => $remarks,
						'created_at' => date('Y-m-d H:i:s'),
					];
						
					try{
						$rentalId = $rentalModel->vehicleAdd($rentalInsertData);
						$rules = $_POST['spec'];
						$ruleInsertData = [];
						foreach ($rules as $rule) {
							$ruleInsertData[] = [
								'rental_id' => $rentalId,
								'duration_time' => $rule['time'],
								'ratio' => $rule['ratio'],
							];
						}
						$rentalRuleModel->AddAllRules($ruleInsertData);
						output_data('添加成功');						
					}catch (Exception $e){
						output_error($e->getMessage());						
					}
			}		
	}
	
	
	
	

	

	//维护
	public function maintain_saveOp(){
		
	
	        $obj_validate = new Validate();
	        $obj_validate->validateparam = array(
	            array("input" => $_POST["maintain_content"], "require" => "true", "message" => '请填写送维原因'),             
	        );
	        $error = $obj_validate->validate();
	        if ($error != '') {
				output_error($error);				  
	        } else {
	            $insertData = array();
	            $insertData['maintain_region'] = $_POST['maintain_region'];
	            $insertData['maintain_content'] = $_POST['maintain_content'];          
	            $insertData['maintain_return_region'] = $_POST['maintain_return_region'];                
	            $insertData['add_time'] = time();
	            $insertData['vehicle_id'] = $_POST['vehicle_id'];
			    $insertData['maintain_replace'] = $_POST['maintain_replace'];	
	            $return = Model('vehicle_maintain')->vehicleMaintainAdd($insertData);				
	            if ($return) {					
						$where = array(
							'vehicle_id' => $_POST['vehicle_id']
						);
						$data = array(
							'vehicle_state' => 30,
							'maintain_id' => $return,
							'maintenance_time' => date('Y-m-d H:i:s',time())
						);
					model('vehicle')->editVehicle($where,$data);					  
					output_data('添加成功');
	    		} else {
					output_error('添加失败');
	            }
	        }
	 
	
	}
	
	//维护完成
	public function maintainok_saveOp(){
		
	        $obj_validate = new Validate();
	        $obj_validate->validateparam = array(
	            array("input" => $_POST["maintain_content_ok"], "require" => "true", "message" => '请填原因'),             
	        );
	        $error = $obj_validate->validate();
	        if ($error != '') {
				output_error($error);			
	        } else {
	            $insertData = array();
	            $insertData['maintain_return_region'] = $_POST['maintain_return_region'];
	            $insertData['maintain_content_ok'] 	  = $_POST['maintain_content_ok'];  
			    $insertData['maintain_state'] 	  = 1;  	                  
				$insertData['update_time'] 	  = time();                    
				$where = array(
					'maintain_id' => $_POST['maintain_id']
				);
	            $return = Model('vehicle_maintain')->editMaintainVehicle($where ,$insertData);				
	            if ($return) {					
					$where = array(
						'vehicle_id' => $_POST['vehicle_id']
					);
					$data =array(
						'vehicle_state' => 10,
						'maintain_id' => 0										
					);
					model('vehicle')->editVehicle($where,$data);							
					output_data('操作成功');				    
	    		} else {
					output_error('操作失败');
	            }
	        }
	
	}
	
	
	
	//暂停
	public function suspendOp(){		
				$where = array(
						'vehicle_id' => $_POST['vehicle_id']
				);
				$data =array(
						'vehicle_state' => 70,
						'recovery_state' => $_POST['recovery_state']																
				);				
				$row =   model('vehicle')->editVehicle($where,$data);		
				if($row){		
					output_data('操作成功');
	    		} else {
					output_error('操作失败');
			    }
	}
	
	
	//恢复
	public function recoveryOp(){			
				$where = array(
						'vehicle_id' => $_POST['vehicle_id']
				);
				$data =array(
						'vehicle_state' => 	$_POST['recovery_state'],
						'recovery_state' => 0				
				);
				$row =   model('vehicle')->editVehicle($where,$data);		
				if($row){					 
					output_data('操作成功');
	    		} else {
					output_error('操作失败');
			    }
	}
	
	
	
	
	
	//召回
	public function recall_saveOp(){
		
		
	        $obj_validate = new Validate();
	        $obj_validate->validateparam = array(
	            array("input" => $_POST["event_order_price"], "require" => "true", "message" => '请填写实际收款金额'),             
	        );
	        $error = $obj_validate->validate();
	        if ($error != '') {
				output_error($error);				
	        } else {
	             $insertData = array();
	             $insertData['vehicle_id'] = $_POST['vehicle_id'];
	             $insertData['event_order_price'] 	  = $_POST['event_order_price'];  	
				 $insertData['event_code'] 	  = $_POST['event_code'];// date('YmdHis'); 
				 $insertData['event_type'] 	  = $_POST['event_type'];  
				 $insertData['event_region'] 	  = $_POST['event_region'];  	
				 $insertData['event_category'] 	  = '召回';  					 				 			
				 $insertData['event_content'] 	  = $_POST['event_content'];  	 
				// $insertData['event_file'] 	  = $_POST['event_file'];  	                  
				 $insertData['add_time'] 	  = time(); 
	             $return = Model('vehicle_event')->vehicleEventAdd($insertData);				
	            if ($return) {					
					$where = array(
						'vehicle_id' => $_POST['vehicle_id']
					);
					$data =array(
						'vehicle_state' => 50															
					);
					model('vehicle')->editVehicle($where,$data);	
	             	output_data('操作成功');
	            } else {
	             	output_error('操作失败');
	            }
	        }	
	}
	
	
	
	//验收
	public function check_vehicleOp(){
			
		
			$obj_validate = new Validate();
	        $obj_validate->validateparam = array(
	          //  array("input" => $_POST["event_order_price"], "require" => "true", "message" => '请填写实际收款金额'),             
	        );
	        $error = $obj_validate->validate();
	        if ($error != '') {
				output_error($error);				
	        } else {
	             $insertData = array();
	             $insertData['vehicle_id'] = $_POST['vehicle_id'];
	             $insertData['is_damage'] 	  = $_POST['is_damage'];  	
				 $insertData['evaluate_content'] 	  =  $_POST['evaluate_content'];  	
				 $insertData['is_charging'] 	  = $_POST['is_charging'];  
				 $insertData['is_hygiene'] 	  = $_POST['is_hygiene'];  				
				 $insertData['evaluate_other'] 	  = $_POST['evaluate_other'];  	 
				// $insertData['event_file'] 	  = $_POST['event_file'];  	                  
				 $insertData['addtime'] 	  = time(); 
	             $return = Model('vehicle_lease_evaluate')->addLeaseEvaluate($insertData);				
	            if ($return) {					
					$where = array(
						'vehicle_id' => $_POST['vehicle_id']
					);
					$data =array(
						'vehicle_state' => 10,
						'lease_id' => 0														
					);
					model('vehicle')->editVehicle($where,$data);	
					output_data('操作成功');
	            } else {
	              	output_error('操作失败');
	            }
	        }
	    }	
	
	
	
	
	
	
	
	
	
}
