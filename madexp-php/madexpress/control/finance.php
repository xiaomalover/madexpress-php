<?php
/**
 * 车辆管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class financeControl extends mobileAdminControl{
  

	

	public function __construct(){
        parent::__construct();   
    }

	
	public function infoOp(){
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		 
		$info = array();
		$info['in_all'] = 0;
		$info['in_all_icon'] = "bottom";//上升top ,下降bottom
		$info['in_all_per'] = "0%";
		
		$info['in_money'] = 0;
		$info['in_money_icon'] = "top";//上升top ,下降bottom
		$info['in_money_per'] = "0%";
	
		$info['in_store_avg'] = 0;
		$info['in_store_avg_icon'] = "bottom";//上升top ,下降bottom
		$info['in_store_avg_per'] = "0%";
		
		$info['in_delivery_avg'] = 0;
		$info['in_delivery_avg_icon'] = "top";//上升top ,下降bottom
		$info['in_delivery_avg_per'] = "0%";
	
		$info['out_all'] = 0;
		$info['out_all_icon'] = "bottom";//上升top ,下降bottom
		$info['out_all_per'] = "0%";
		
		$info['out_avg'] = 0;
		$info['out_avg_icon'] = "top";//上升top ,下降bottom
		$info['out_avg_per'] = "0%";
	
		$info['out_charge'] = 0;
		$info['out_charge_icon'] = "bottom";//上升top ,下降bottom
		$info['out_charge_per'] = "0%";
		
		$info['out_downline'] = 0;
		$info['out_downline_icon'] = "top";//上升top ,下降bottom
		$info['out_downline_per'] = "0%";
	
	
	
	
		$info['in_x_val']  = ['21/08', '12', '13', '14', '15', '16', '17'];
		$info['in_y_val0'] = [3200, 33, 33, 33, 33, 33, 5];//收入商家
		$info['in_y_val1'] = [1232, 33, 33, 33, 33, 33, 6];//收入配送员
		$info['in_y_val2'] = [4233, 33, 33, 33, 33, 33, 7];//收入用户
		$info['in_y_val3'] = [5130, 33, 33, 33, 33, 33, 7];//收入stripe
	
	
	
	
	
		$info['out_y_val0'] = [33, 33, 33, 33, 33, 33, 100];//支出商家
		$info['out_y_val1'] = [33, 33, 33, 33, 33, 33, 100];//支出配送员
		$info['out_y_val2'] = [33, 33, 33, 33, 33, 33, 1];//支出用户
		$info['out_y_val3'] = [33, 33, 33, 33, 33, 33, 10];//项目1收入
		$info['out_y_val4'] = [33, 33, 33, 33, 33, 33, 20];//项目2收入
		
		
		output_data($info);
		
	}
	
	
	//账户充值
	public function topupsOp(){
		
		/*$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');	
		
		
		
		
		$stripe->topups->create([
		  'amount' => 2000,
		  'currency' => 'aud',
		  'description' => 'Top-up for Jenny Rosen',
		  'statement_descriptor' => 'Top-up',
		]);		
		*/
	   
	   
	   $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'stripe/stripe.php';
	   if(!is_file($inc_file)){
	       output_error('支付接口不存在');
	   }		
	   require($inc_file);
	   
	   
		
		
		
		
	}
	

	//票据类型
	public function invoice_role_typeOp(){

		$role = array(
			array(
				'name' => 'ME Platform',
				'code' => 0
			),
			array(
				'name' => 'Merchant',
				'code' => 1
			),
			array(
				'name' => 'Courier',
				'code' => 2
			),
			array(
				'name' => 'Customer',
				'code' => 3
			)
		);
		output_data(['role_list' => $role]);

	}


	public function invoice_typeOp(){


		$role_code = !empty($_POST['role_code']) ? $_POST['role_code'] : 0;
		$list = model('bill_type')->getBillTypeList(['bill_role_code' => $role_code]);

		foreach($list as $k => $v){
			$data[$k]['name'] = $v['bill_type_name'];
			$data[$k]['value'] = $v['bill_type_code'];
			$data[$k]['icon'] = $v['bill_type_code'];

		}

		output_data(['list' => $data]);

	}



	//票据列表
	public function invoice_listOp(){



		$model = model('bill');
		$condition = array();
	
		if($_POST['keyword']){

			$condition['order_sn'] = array('like','%'.$_POST['keyword'].'%');
		}

		if($_POST['date'] == 'month' && !empty($_POST['screen_date'])){

			$screen_date = explode('-',$_POST['screen_date']);
			
			$condition['bill_year'] = $screen_date[0];
			$condition['bill_month'] = $screen_date[1];

		}
		if($_POST['date'] == 'year' && !empty($_POST['screen_date'])){

			$condition['bill_year'] = $_POST['screen_date'];

		}



		if($_POST['bill_type'] == 'tax_summary'){

			$condition['bill_type'] = 'tax_summary';
			$condition['to_type'] = $_POST['to_type'];



		}
		
		if($_POST['bill_type'] == 'me_invoice'){

			$condition['bill_type'] =  array('in','me_platform_invoice');
			$condition['to_type'] = $_POST['to_type'];



		}

		if($_POST['bill_type'] == 'credit_note'){
			$condition['bill_type'] =  array('in','me_platform_credit_note');
			$condition['to_type'] = $_POST['to_type'];


		}

		
		if($_POST['bill_type'] == 'others'){

			if($_POST['to_type'] == '1'){
				
				$condition['bill_type'] =  array('in','refund_record,liquidated_damage_receipt,sales_reimbursement_invoice');
				

			}
			if($_POST['to_type'] == '2'){

				$condition['bill_type'] =  array('in','liquidated_damage_receipt,delivery_fee_reimbursement_invoice');

			}

			if($_POST['to_type'] == '3'){

				$condition['bill_type'] =  array('in','refund_record');

			}


			
		//	$condition['to_type'] = $_POST['to_type'];



		}



		$invoice_list = $model->getBillList($condition, '*',$this->page);		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		foreach ($invoice_list as $v) {

			//收方用户类型 0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			switch ($v['to_type']) {
				
				case 0 : 

					if($v['form_type'] == 1){

						$store = model('store')->getStoreInfo(['store_id' => $v['from_id']]);
						$v['to_name_primary'] = $store['store_name_primary'];
						$v['to_name_secondary'] = $store['store_name_secondary'];
						$v['link'] = 1;
					}

					if($v['form_type'] == 2){
						$delivery = model('waiter')->getWaiterInfo(['distributor_id' => $v['from_id']]);
						$v['to_name_primary'] = $delivery['distributor_name'];
						$v['link'] = 2;

					}
					
				break;
				case 1 : 

					$store = model('store')->getStoreInfo(['store_id' => $v['to_id']]);

					$v['to_name_primary'] = $store['store_name_primary'];
					$v['to_name_secondary'] = $store['store_name_secondary'];
					$v['link'] = 1;

				break;
				case 2 : 


					$delivery = model('waiter')->getWaiterInfo(['distributor_id' => $v['to_id']]);
					$v['to_name_primary'] = $delivery['distributor_name'];
					$v['link'] = 2;

				break;
				case 3 :
					
					$member = model('member')->getMemberInfo(['member_id' => $v['to_id']]);
					$v['to_name_primary']  = $member['member_code'];
					$v['link'] = 3;

				break;

			}


			$v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];
			$v['date_of_issue'] = date('d/m/y',$v['date_of_issue']);
			$data[] = $v;

		}


		output_data(array('list' => $data), mobile_page($page_count, $list_count));

	}







	//钱包类型
	public function  get_wallet_typeOp(){


		
		$where = array();
		$where['finance_user'] = 2;


		if($_POST['type'] == 'positive'){
			$where['finance_type'] = 0 ;
		}


		if($_POST['type'] == 'negative'){
			$where['finance_type'] = 1 ;
		}


		$positive = model('finance')->getFinanceList($where);				
		
		$list[0]['finance_project'] = 'ALL';
		$list[0]['finance_project_name'] = 'ALL';		

		foreach($positive as  $k => $v){
			$list[$k+1] = $v;		
		}		
			
		output_data(array('list' => $list));

	}
	
	//钱包流水
	public function get_wallet_listOp(){

		$model = model('me_wallet');
		
		$condition = array();		
		if($_POST['type'] == 'ALL'){			
			
		//	$condition['lg_type'] = $_POST['type'];
			if($_POST['type_act'] != 'ALL'){	
				$condition['lg_type'] = $_POST['type_act'];
			}
		}else{
		
			$condition['lg_type_activities'] = $_POST['type'] == 'positive' ? 0 : 1;
			if($_POST['type_act'] != 'ALL' ){
				$condition['lg_type']  = $_POST['type_act'];
			}
		}
		
				
		$order = 'lg_id desc';	
		
	//print_r($condition);
			  
		$wallet_list = $model->getPdLogList($condition, '*', $this->page, $order);
		
		$list = array();		
		
		foreach ($wallet_list as $v) {
				   			
			$v['lg_add_time'] =  date('d/m/Y',$v['lg_add_time']);		
			$list[] = $v;

		}		
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));




	}
	
	
	
	
}
