<?php
/**
 * 仓库管理
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class mavin_depotControl extends mobileMavinControl {
 
    
    public function __construct(){
        parent::__construct();	
    }

	
	
	public function depot_classOp(){
		
		$model =  Model('foodbox_order');
		$menu_array = array(
		    array('menu_key' => 'foodbox_examine',
		        'menu_name' => '审核中',
		        'menu_icon' => 'icon_7shenghe',	
				'order_state' => 0,
		        'count' => $model->getOrderCount(['order_status'=>0])),
		  
			array('menu_key' => 'foodbox_distribute',
		        'menu_name' => '待分配',
		        'menu_icon' => 'icon_7fenpei',		  		
				'order_state' => 10,
		        'count' => $model->getOrderCount(['order_status'=>10])),
		  
			array('menu_key' => 'foodbox_wait',
		        'menu_name' => '待配送',
		        'menu_icon' => 'icon_7daipeisong',				
				'order_state' => 20,
		        'count' => $model->getOrderCount(['order_status'=>20])),
		  
			array('menu_key' => 'foodbox_delivery',
		        'menu_icon' => 'icon_7peisongzhong',
		        'menu_name' => '配送中',		        
				'order_state' => 30,
		        'count' => $model->getOrderCount(['order_status'=>30]),),
		    
			array('menu_key' => 'foodbox_completed',
		        'menu_icon' => 'icon_7yiwancheng',
		        'menu_name' => '已完成',		        
				'order_state' => 40,
		        'count' => $model->getOrderCount(['order_status'=>40])),
		    
			array('menu_key' => 'foodbox_cancel',
		        'menu_icon' => 'icon_7yiquxiao',
		        'menu_name' => '已取消',		        
				'order_state' => 50,
		        'count' => $model->getOrderCount(['order_status'=>50])),
		);
		
		
		output_data(array('list'=>$menu_array));
		
		
	}
	
	
	//仓库审核列表
	public function depot_order_listOp(){
		
		$model = model('foodbox_order');
		 $where = array(
				'order_status' => 0
		 );
		$data_list = $model->getOrderList($where,'*',$this->page);
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		$order_list = array();
		foreach ($data_list as $k => $v) {			 
			 $order_list[$k] = $v;
		}
		
		output_data(array('list' => $order_list), mobile_page($page_count,$list_count));		
		
		
	}
	
	
	//餐盒类型管理
	public function depot_goodsOp(){
		
		$class_list = model('foodbox_class')->getClassList(True);
		$data = array();
		foreach ($class_list as $k => $v) {
		
		    $data[$k] = $v;
		    $data[$k]['child'] = $this->getDepotGoods($v['class_id']);
		
		}
		
		output_data(array('list' => $data));
	}
	
	private function getDepotGoods($cid){
	
	    $where = array(
	        'class_id' => $cid
	    );
	    $goods_list = model('foodbox_goods')->getGoodsList($where);
	
	    return $goods_list;
	}
	
	
	
	
	//仓库地区
	public function deopt_regionOp(){
		
	
		
		$region_list = model('region')->getRegionList(array('region_parent_id' => 0));
	
		$list = array();
		$list[0]['region_name'] = 'ALL';
		$list[0]['region_color'] = '#003056';		
		$i = 1;
		foreach($region_list as  $v){
			$list[$i] = $v;
			$i++;
		}
		
		
		
		output_data(array('list'=>$list));
		
		
	}
	
	//仓库日志
	
	public function deopt_logOp(){
		
		// 设置页码参数名称
		$condition = array();
		if ($_POST['query'] != '') {
		    $condition[$_POST['qtype']] = $_POST['query'];
		}
		$order = '';
		$param = array('log_id');
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
		    $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
		}
		
		$page = $_POST['rp'];
		$model = model('depot_log');
		$data['now_page'] = $model->shownowpage();
		$data['total_num'] = $model->gettotalnum();
		$list = $model->getDepotLogList($condition, '*', $page, $order);
		
		foreach ($list as $value) {
		    $param = array();
		    $param['log_sn'] = $value['log_sn'];
		    $param['depot_name'] = $value['depot_name'];
		    $param['log_type'] = $value['log_type'];
		    $param['update_time'] = date('Y-m-d H:i:s', $value['update_time']);
		    $param['admin_name'] = $value['admin_name'];
		    $param['log_content'] = $value['log_content'];
		    $data['list'][$value['log_id']] = $param;
		}
		
		
	}
	
	
}
