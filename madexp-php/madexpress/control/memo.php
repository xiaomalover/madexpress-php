<?php
/**
 * 专员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class memoControl extends mobileAdminControl{
 
	public function __construct(){
        parent::__construct();   
    }

	
	public function get_classOp(){
		
		$model = model('notepad');
		
		$where = array();
		$where['store_id'] = 0;
		
		$data = $model->getNotepadClassList($where);
		$list = array();
		foreach($data as $k=>$v){
			$v['child'] = $this->get_list($v['class_id']);			
			$list[$k] = $v;
		}
		
		output_data(array('list'=>$list));
		
		
	}
	
	private function get_list($class_id){
		
		$where = array();
		$where['class_id'] = $class_id;
		$where['store_id'] = 0 ;
		$data = model('notepad')->getNotepadList($where);
		$list = array();
		foreach($data as $k => $v){
			$v['notepad_time'] = date('d/m/y',$v['notepad_time']);
			$list[$k] = $v;
		}
		
		return $list;
	}
	
	
	public function class_saveOp(){		
		$data = array();
		$data['class_name'] = $_POST['class_name'];
		$data['store_id'] = 0;
		if(intval($_POST['class_id']) > 0){
			$where = array();
			$where['class_id'] = $_POST['class_id'];
			$row = model('notepad')->editNotepadClass($where,$data);
			
		}else{			
			$row = model('notepad')->addNotepadClass($data);
		}
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
		
	}
	
	
	public function memo_infoOp(){
		
		$where = array();
		$where['notepad_id'] = $_POST['id'];		
		$row = model('notepad')->getNotepadInfo($where);
		
		$gdurl = 'http://'.$_SERVER['SERVER_NAME'];
		
		if($row){			
			$data =array();
			$data['content'] = $row['notepad_content'];
			$data['title'] = $row['notepad_title'];
			$data['class_id'] = $row['class_id'];
			$data['store_id'] = $row['store_id'];
			$data['notepad_id'] = $row['notepad_id'];
			
			$notepad_file_arr = array();
			if(!empty($row['notepad_file'])){
				$notepad_file = explode(",",$row['notepad_file']);
				
				foreach($notepad_file as $k=>$v){
					$notepad_file_arr[$k]["name"] = $v;
					$notepad_file_arr[$k]["url"] = $gdurl."/data/upload/shop/store/file/".$v;
				}
			}
			$data['notepad_file'] = $notepad_file_arr;
			
			output_data($data);			
		}else{			
			output_error('获取失败');
		}
	}
	
	
	public function memo_saveOp(){		
		$data = array();
		$gdurl = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/shop/store/file/";
		
		$data['notepad_title'] = $_POST['title'];
		$data['notepad_content'] = $_POST['content'];
		$data['notepad_time'] = time();
		
		$fileList = $_POST['fileList'];
		$arr = array();
		if($fileList){
			foreach($fileList as $k=>$v){
				$url = str_replace($gdurl,"",$v["url"]);
				array_push($arr,$url);
			}
		}
		$fileList = implode(",",$arr);
		$data['notepad_file'] = $fileList;
		$data['class_id'] = $_POST['class_id'];
		$data['store_id'] = 0;
		
		if(intval($_POST['notepad_id']) > 0){
			$where = array();
			$where['notepad_id'] = $_POST['notepad_id'];
			$row = model('notepad')->editNotepad($where,$data);
			
		}else{			
			$row = model('notepad')->addNotepad($data);
		}
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}
	
	
	public function memo_delOp(){
		
		$where = array();		
		$where['notepad_id'] = $_POST['id'];		
		$row = model('notepad')->delNotepad($where);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}
	
	
	public function memo_class_delOp(){
		
		$where = array();		
		$where['class_id'] = $_POST['class_id'];		
		$row = model('notepad')->delNotepad($where);
		model('notepad')->delNotepadClass($where);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}

	public function upload_fileOp(){
		$gdurl = 'http://'.$_SERVER['SERVER_NAME'];
		
		$dir = '../data/upload/shop/store/file/'.date('ymd');
		if (!is_dir($dir)){
			mkdir($dir,0777,true);
		}
		$filename = date("his"). '_' . rand(10000, 99999).".".end(explode('.', $_FILES["file"]["name"]));//随机生成文件名加上后缀
		$store_path = $dir . "/" . $filename;
        move_uploaded_file( $_FILES["file"]["tmp_name"], $store_path);
		
		$data=array();
		$data["name"] = $_FILES["file"]["name"];
		$data["url"] = $gdurl."/data/upload/shop/store/file/".date('ymd'). "/" .$filename;
		output_data($data);
	}
	
}
