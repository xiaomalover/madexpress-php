<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class mavin_loginControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }



	public function loginOp(){
		
	
	
		if (process::islock('admin')) {
		    output_error('您的操作过于频繁，请稍后再试');
		}
		
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
		    array("input"=>$_POST["username"], "require"=>"true", "message"=>'账号不能为空'),
		    array("input"=>$_POST["password"],  "require"=>"true", "message"=>'密码不能为空')		  
		);
		$error = $obj_validate->validate();
		if ($error != '') {
		    output_error($error);
		} else {
		    $model_admin = Model('admin');
		    $array  = array();
		    $array['admin_name']    = $_POST['username'];
		    $array['admin_password']= md5(trim($_POST['password']));
		    $admin_info = $model_admin->infoAdmin($array);
		    if(is_array($admin_info) and !empty($admin_info)) {
				
				if($admin_info['admin_state'] == 0){
					 output_error('此账号已禁用');
				}
				
		        if ($admin_info['admin_gid'] > 0) {
		            $gamdin_info = Model('gadmin')->getGadminInfoById($admin_info['admin_gid']);
		            $group_name = $gamdin_info['gname'];
					
		        } else {
					
		            $group_name = '超级管理员';
					
		        }
				
		        $array = array();
		        $array['admin_name']  = $admin_info['admin_name'];
		        $array['admin_id']    = $admin_info['admin_id'];
		        $array['admin_login_time']  = $admin_info['admin_login_time'];
		        $array['admin_ip']    = getIp();
		        $array['admin_gid']   = $admin_info['admin_gid'];
		        $array['admin_group_name'] = $group_name;
		        $array['admin_is_super']    = $admin_info['admin_is_super'];
		        $array['admin_quick_link'] = $admin_info['admin_quick_link'];
				$array['admin_type'] = $admin_info['admin_type'];
				$array['admin_nickname'] =  $admin_info['nickname'];		
				
				$token = $this->_get_token($admin_info['admin_id'], $admin_info['admin_name'], 'mobile',$array);
				
				if($token) {
					
					$update_info    = array(
						'admin_id'=>$admin_info['admin_id'],
						'admin_login_num'=>($admin_info['admin_login_num']+1),
						'admin_login_time'=>TIMESTAMP
					);					
					$model_admin->updateAdmin($update_info);						
					process::clear('admin');												
					output_data(array('token'=>$token,'admin_id'=>$admin_info['admin_id'],'admin_name'=>$admin_info['admin_name']));
				
				}else{
					
					output_error('登陆失败');
					
				}
				
		        //@header('Location: index.php');exit;
		    } else {
		        process::addprocess('admin');
		        output_error('登陆失败');
		    }
		}
		
	}

	
	
	
	public function autologinOp(){
		
		$where = array();
		$where['token_id'] = $_POST['token_id'];
		$model_mb_seller_token = Model('mb_mavin_token');
		$token_info =$model_mb_seller_token->getMbAdminTokenInfo($where);
		if(!empty($token_info)){
			$model_admin = Model('admin');
			$admin_info = $model_admin->getAdminInfoNew($token_info['admin_id']);	
					
			output_data(array('token'=>$token_info['token'],'admin_id'=>$admin_info['admin_id'],'admin_name'=>$admin_info['admin_name']));
										
		}else{
			output_error('登陆失败');
		}
		
	}
	



	/**
	 * 登录生成token
	 */
	private function _get_token($admin_id, $admin_name, $client,$data = array()) {
	    $model_admin_token = Model('mb_mavin_token');
	
	    //重新登录后以前的令牌失效
	    //暂时停用
	    //$condition = array();
	    //$condition['member_id'] = $member_id;
	    //$condition['client_type'] = $client;
	    //$model_mb_user_token->delMbUserToken($condition);
	
	    //生成新的token
	    $mb_admin_token_info = array();
	    $token = md5($admin_name . strval(TIMESTAMP) . strval(rand(0,999999)));
	    $mb_admin_token_info['admin_id'] = $admin_id;
	    $mb_admin_token_info['admin_name'] = $admin_name;
	    $mb_admin_token_info['token'] = $token;
	    $mb_admin_token_info['login_time'] = TIMESTAMP;
	    $mb_admin_token_info['client_type'] = $client;
		
		$mb_admin_token_info['admin_gid'] = $data['admin_gid'];
		$mb_admin_token_info['admin_group_name'] = $data['admin_group_name'];
		$mb_admin_token_info['admin_nickname'] = $data['admin_nickname'];
		$mb_admin_token_info['admin_type'] = $data['admin_type'];
		$mb_admin_token_info['admin_quick_link'] = $data['admin_quick_link'];
		$mb_admin_token_info['admin_is_super'] =  $data['admin_is_super'];
		$mb_admin_token_info['admin_ip'] = $data['admin_ip'];

		
	
	    $result = $model_admin_token->addMbAdminToken($mb_admin_token_info);
	
	    if($result) {
	        return $token;
	    } else {
	        return null;
	    }
	
	}
	
	
	

	
	private function getGrade($member_id){
		$where = array(
			'member_id' => $member_id
		);
		$row = model('admin')->getAdminInfoNew($where);
		if(!empty($row)){
			
			if($row['admin_type'] == 0){
				return 1;
			}else{
				return 2;
			}
		}else{
				return 0;
		}		
	}

  
	 
	
	
	
}
