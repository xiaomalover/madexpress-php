<?php
/**
 * 首页数据统计
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class mavin_operateControl extends mobileMavinControl {

    public function __construct(){
        parent::__construct();
		
		
		
		import('function.datehelper');
		
		$model_stat = Model('stat');
		$this->search_arr = $_POST;
		//$this->search_arr = $_GET;
		//处理搜索时间
		if ($this->search_arr['stattype'] == 'yesterday') {
		    $this->search_arr['search_type'] = 'day';
		    $this->search_arr['show_type'] = 'hour';
		    $this->search_arr['day']['search_time'] = date('Y-m-d', time());
		    $this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
			
		} elseif ($this->search_arr['stattype'] == 'week') {
			
		    $this->search_arr['search_type'] = 'week';
		    $this->search_arr['show_type'] = 'week';
		    $searchweek_weekarr = getWeek_SdateAndEdate(time());
		    $this->search_arr['week']['current_week'] = implode('|', $searchweek_weekarr);
			
		} elseif ($this->search_arr['stattype'] == 'month') {
		   
			$this->search_arr['search_type'] = 'month';
		    $this->search_arr['show_type'] = 'day';
		    $this->search_arr['month']['current_year']= date('Y', time());
		    $this->search_arr['month']['current_month']= date('m', time());
			
		} elseif ($this->search_arr['stattype'] == 'year') {
			
		    $this->search_arr['search_type'] = 'year';
		    $this->search_arr['show_type'] = 'month';
		    $this->search_arr['year']['current_year']= date('Y', time());
			
		}else{
			
			$this->search_arr['search_type'] = 'day';
			$this->search_arr['show_type'] = 'hour';
			$this->search_arr['day']['search_time'] = date('Y-m-d', time());
			$this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
		}
		
		$searchtime_arr = $model_stat->getStarttimeAndEndtime($this->search_arr);
		
		$this->search_arr['stime'] = $searchtime_arr[0];
		$this->search_arr['etime'] = $searchtime_arr[1];
		
		
		
		
    }


	public function infoOp(){		
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		
		$store_ids_list = model('store')->getStoreList(array('admin_id' => $this->admin_info['admin_id']));
		foreach($store_ids_list as $v){
			$store_ids[] = $v['store_id'];
		}
		
		$order_day_count = 0;
		
		$model_order = model('order');		
		//日订单		
		$where  = array(
			'add_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime'])),
			'store_id' => array('in',$store_ids)
		);
					
		$day_count = $model_order->getOrderCount($where);
		
		if($day_count > 0 ){
			$order_day_count = $day_count;			
		} else{
			$order_day_count = 0;
		}
		 
		 
		$info = array();
		$info['order_day'] = $order_day_count > 0 ? $order_day_count : 0 ;
		$info['order_day_icon'] = "bottom";//上升top ,下降bottom
		$info['order_day_per'] = "10%";
		
		
		//日单价		
		$order_day_amount = $model_order->getOrderInfoN($where,'SUM(order_amount) as amount');		
		$info['price_day'] = $order_day_amount['amount'] > 0 ?  ($order_day_amount['amount'] / $order_day_count)  : 0;
		$info['price_day_icon'] = "top";//上升top ,下降bottom
		$info['price_day_per'] = "20%";
		
		//今日配送费
		$order_delivery_amount = $model_order->getOrderInfoN($where,'SUM(delivery_fee) as delivery_fee');			
		$info['fee_day'] = $order_delivery_amount['delivery_fee'] > 0  ? $order_delivery_amount['delivery_fee']: 0 ;
		$info['fee_day_icon'] = "bottom";//上升top ,下降bottom
		$info['fee_day_per'] = "30%";
		
		//日注册用户
		$where  = array(
			'member_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime']))
		);		
		$member_num = model('member')->getMemberCount($where);		
		$info['reg_day'] = $member_num;
		$info['reg_day_icon'] = "top";//上升top ,下降bottom
		$info['reg_day_per'] = "40%";
		
		//日营收
		
		
		
		$info['money_day'] = $order_day_amount['amount'];
		$info['money_day_icon'] = "bottom";//上升top ,下降bottom
		$info['money_day_per'] = "50%";
		
		//日商均单
		
		$store_count = model('store')->getStoreCount(array('store_state' => array('in','1,2'),'store_id' => array('in',$store_ids)));
		
		if($order_day_count > 0){
			$store_jun_order = $order_day_count / $store_count;		
		}else{
			 $store_jun_order = 0;
		}
		
		$info['num_day'] = $store_jun_order;
		$info['num_day_icon'] = "top";//上升top ,下降bottom
		$info['num_day_per'] = "60%";
		
		
		
		
		$info['he_day'] = 0;
		$info['he_day_icon'] = "bottom";//上升top ,下降bottom
		$info['he_day_per'] = "70%";
		
		$info['view_day'] = 0;
		$info['view_day_icon'] = "top";//上升top ,下降bottom
		$info['view_day_per'] = "80%";
		
		
		
		
		
		
		//图表
		$weeks = $this->get_weeks();
		
		foreach($weeks as $v){			
			$x_val[] = date('d', strtotime($v));
			$where  = array(
				'add_time' =>array('between',array(strtotime($v),strtotime($v)+ 86400 - 1)),
				'store_id' => array('in',$store_ids)
			);				
			$y_val[] = $model_order->getOrderCount($where);			
		}		
		$info['x_val'] = $x_val;
		$info['y_val'] = $y_val;		
		output_data($info);
		
	}
	



	private function  get_weeks($time = '', $format='Y-m-d'){
	  $time = $time != '' ? $time : time();
	  //组合数据
	  $date = [];
	  for ($i=1; $i<=7; $i++){
	    $date[$i] = date($format ,strtotime( '+' . $i-7 .' days', $time));
	  }
	  return $date;
	}
	 
	 
	
	
	
}
