<?php
/**
 * 第三方账号登录
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class connectControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }

    /**
     * 登录开关状态
     */
    public function get_stateOp() {
        $logic_connect_api = Logic('connect_api');
        $state_array = $logic_connect_api->getStateInfo();
        
        $key = $_GET['t'];
        if(trim($key) != '' && array_key_exists($key,$state_array)){
            output_data($state_array[$key]);
        } else {
            output_data($state_array);
        }
    }
  
	
    /**
     * 获取手机短信验证码
     */
    public function get_sms_captchaOp(){
        $sec_key = $_GET['sec_key'];
        $sec_val = $_GET['sec_val'];
        $phone = $_POST['phone'];
        $log_type = $_POST['type'];//短信类型:1为注册,2为登录,3为找回密码
       
	    /*$state_data = array(
            'state' => false,
            'msg' => '验证码或手机号码不正确'
            );*/
			
        
        //$result = Model('apiseccode')->checkApiSeccode($sec_key,$sec_val);
        //if ($result && strlen($phone) == 11){
        if (strlen($phone) == 11){

            $logic_connect_api = Logic('connect_api');
          	$state_data = $logic_connect_api->sendCaptcha($phone, $log_type);
        }
        $this->connect_output_data($state_data);
    }
    
	
	/**
     * 验证手机验证码
     */
    public function check_sms_captchaOp(){
        $phone = $_POST['phone'];
        $captcha = $_POST['captcha'];
        $log_type = $_POST['type'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->checkSmsCaptcha($phone, $captcha, $log_type);
        $this->connect_output_data($state_data, 1);
    }
   
    /**
     * 手机注册
     */
    public function sms_registerOp(){
        $phone = $_POST['phone'];
        $captcha = $_POST['captcha'];
        $password = $_POST['password'];
        $client = $_POST['client'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsRegister($phone, $captcha, $password, $client);
        $this->connect_output_data($state_data);
    }
	
    /**
     * 手机找回密码
     */
    public function find_passwordOp(){
        $phone = $_POST['phone'];
        $captcha = $_POST['captcha'];
        $password = $_POST['password'];
        $client = $_POST['client'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsPassword($phone, $captcha, $password, $client);
        $this->connect_output_data($state_data);
    }
	
	
	
	public function  find_waiterPasswordOp(){
		
		$phone = $_POST['phone'];
        $captcha = $_POST['captcha'];
        $password = $_POST['password'];
        $client = $_POST['client'];
        $logic_connect_api = Logic('connect_api');
        $state_data = $logic_connect_api->smsWaiterPassword($phone, $captcha, $password, $client);
        $this->connect_output_data($state_data);
		
	}
	
	
	
    /**
     * 格式化输出数据
     */
    public function connect_output_data($state_data, $type = 0){
        if($state_data['state']){
            unset($state_data['state']);
            unset($state_data['msg']);
            if ($type == 1){
                $state_data = 1;
            }
            output_data($state_data);
        } else {
            output_error($state_data['msg']);
        }
    }
}
