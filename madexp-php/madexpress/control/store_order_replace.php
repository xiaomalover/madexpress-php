<?php
/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class store_order_replaceControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();      
    }


	//替换列表
	public function replace_listOp(){
		
		$order_id =  intval($_POST['order_id']);		
		
		$where = array();
		$where['order_id']  = $order_id;
		$where['store_id'] = $this->store_info['store_id'];			
		$where['rec_id'] = intval($_POST['rec_id']);
		$lang = $_POST['lang'] ?  $_POST['lang']  : 'CHN-S';
		$replace_list = model('order_goods_replace')->getGoodsList($where);
		foreach ($replace_list as $k=> $replace) {
		    $replace_list[$k] = $replace;						
		
			$lang_data = $this->goodsLang($replace,$lang);		
		
		
		
			if(!empty($lang_data)){
			    
				  $replace_list[$k]['goods_name'] = $lang_data['goods_name'];				
				  $replace_list[$k]['goods_spec'] = $lang_data['goods_spec'];				
				  $replace_list[$k]['goods_size'] = $lang_data['goods_size'];	
				  $replace_list[$k]['goods_lang'] = $lang_data['goods_lang'];				  				
				  
			}		
			
			$replace_list[$k]['goods_optional'] = $this->goodsOptional($replace['goods_optional']);			
		
		}
		
		
		output_data(array('replace_list' => $replace_list,  'replace_count' => count($replace_list)));
		
	}
		
	
	
	//格式化当前数据根据现有值匹配出英文值	
	private function goodsLang($data,$lang){		
			
			 //根据现有数据获得规格的key
			$where = array(
				'goods_id' => $data['goods_id'],
				'lang_name' => $lang
			);		
			$lang_goods = model('goods_language')->getGoodsLangInfo($where);	
			$cart['goods_name'] = $lang_goods['goods_name'];
			//size			
			$size_row = 	model('store_goods_size')->getSizeInfo(array('size_id' => $data['goods_size_id'],'lang' => $lang,'is_old' => 0));
							
			$cart['goods_size'] = $size_row['size_value'];
			$specs_row =  model('store_goods_specs')->getSpecsList(array('specs_id'=>array('in',$data['goods_specs_id']),'lang' => $lang,'is_old' =>0));
			$specs_name = [];
			foreach($specs_row as $v){
				$specs_name[] = $v['specs_value'];
			}
			$specs_name = implode('/',$specs_name);			
			$cart['goods_spec'] = $specs_name;			
			$cart['goods_lang']	 = $lang;
			return $cart;
	}
	
	
	
		
		//查询语言参数
	private function goods_language($goods_id){		
			$where = array(
				'goods_id' =>$goods_id,
				'lang_id' => $this->store_info['store_default_lang']
			);
			$data = model('goods_language')->getGoodsLangInfo($where);
			if(!empty($data)){
				return $data;
			}else{
				return false;
			}
			
	}
		
		
		
		
		
		
	private function goodsOptional($ids){			
			$where = array();
			$where['options_id'] = array('in',$ids);
			$where['state'] = 0 ;
			$where['store_id'] = $this->store_info['store_id'];	
			$list = model('goods_options')->getOptionsList($where);		
			$optional = array();
			foreach($list as  $k=>$v){
				$optional[$k]['id'] = $v['options_id'];
				$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$this->store_info['store_id']);
				$optional[$k]['price'] = $v['options_price'];
			}
			return $optional;
	}
		
		
		//格式化配件商品的语言包
		private function optionsLang($data,$storeId){			
		    $langInfo = model('language')->getLangInfo(array('language_id'=>$this->store_info['store_default_lang']));
			$data = unserialize($data);
			foreach($data as $k => $v){
				$list[$v['lang_name']] = $v['options_name'];
			}	
			return $list[$langInfo['language_flag']];		
		}
	
	
		//获取当前值的KEY	
		private function find_by_array_spec($array,$find)
		{
			 foreach ($array as $key => $v)
			{
				if($v['value']==$find)
				{
					return $key;
				}
			}
		}
		
		
		private function find_by_array_size($array,$find)
		{
			foreach ($array as $key => $v)
			{
				if($v['name']==$find)
				{
					return $key;
				}
			}
		}
	
	
	
	
	
	
		//商品信息
	public function order_goods_infoOp(){
			
			$where = array(
				'rec_id' => $_POST['rec_id'],
				'order_id' => $_POST['order_id'],
				'store_id' => $this->store_info['store_id']
			);		
			$rec_info = model('order')->getOrderGoodsInfo($where);	
			
		
			$rec_num = model('order_goods_edit')->getGoodsInfo(['rec_id' => $rec_info['rec_id']],'SUM(rec_num) as goods_num');
		
			if(intval($rec_num['goods_num']) > 0){
				$rec_info['goods_usable_num'] = $rec_num['goods_num'];
			}



			$rec_info['goods_optional'] = null;
			if($rec_info['goods_optional']){
				$rec_info['goods_optional'] = unserialize($rec_info['goods_optional']);
			}
			$rec_info['goods_spec'] = null;
			if($rec_info['goods_spec']){
			    $rec_info['goods_spec'] = $rec_info['goods_spec'];
			}
			
			
			
			
			output_data(array('goods_info'=>$rec_info));
	}

	
	//记录替换商品
	public function save_replaceOp(){
		
		
		$goods_code = $_POST['goods_code'];
		$quantity = intval($_POST['quantity']);		
		$order_id = $_POST['order_id']	;

		$version_num = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
		

		if(!$goods_code  || $quantity <= 0) {
		    output_error('参数错误');
		}		
	
		$model_goods = Model('goods');
		$model_size = model('store_goods_size');
		$model_specs = model('store_goods_specs');
		$model_options = model('store_goods_options');
		
		$order_info = model('order')->getOrderInfoN(['order_id' => $_POST['order_id']]);


		$_POST['goods_lang'] = $order_info['menu_lang'];
	//	print_r($order_info);

		$goods_info = $model_goods->getGoodsInfo(array('goods_code'=> $goods_code,'is_old' => 0));
		$checkSpec = $this->getLangGoods($goods_code,$_POST['goods_lang']);
	
		$goods_size_price = 0 ;
		$meal_price = 0;
				
		if($goods_info['goods_is_set_meal'] == 0 ){


				//print_r(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
				//新规格
				if(!empty($_POST['goods_size'])){			
					$size_row = $model_size->getSizeInfo(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
					if(empty($size_row)){				
						output_error('尺寸不存在');
					}
					if($size_row['size_sale_price'] > 0){			
						$goods_size_price = $size_row['size_sale_price'];
						$goods_original_price = $size_row['size_price'];				
					}else{
						
						$goods_size_price = $size_row['size_price'];
						$goods_original_price = 0;			
					}
							
					$size_name = $size_row['size_value'];
					$box_id = $size_row['size_box_id'];
				}else{			
					output_error('尺寸不存在');			
				}
								
								
								
				//新规格 ,入参可能为多个specsid
				$spec_price = 0;
				$spec_name = array();
				if(!empty($_POST['goods_spec'])){			
					$specs_row = $model_specs->getSpecsList(array('specs_id' => array('in',$_POST['goods_spec']),'lang' =>$_POST['goods_lang'],'is_old' => 0 ));			
					foreach($specs_row as $v){
						$spec_price += $v['specs_price'];
						$spec_name[] = $v['specs_value'];								
					}			
					$spec_name = implode('/',$spec_name);			
				}	
					
			
					//新配件
					$optional_price = 0;
					if(!empty($_POST['goods_optional'])){
						$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
						foreach($options_row as $v){				
							$optional_price += $v['options_price'];
						}
					}
					
					
					$goods_price =  $goods_size_price + $optional_price + $spec_price  ; //折扣价
					$goods_original_price = $goods_original_price + $optional_price + $spec_price  ;	 //原价
								
				


						
		}else{

				//新配件
				$optional_price = 0;
				if(!empty($_POST['goods_optional'])){
					$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
					foreach($options_row as $v){				
						$optional_price += $v['options_price'];
					}
				}
				
				$goods_price = 0;

				$model = model('store_goods_meal');		
				$where = array();
		
				$where['is_old'] = 0;
				$where['lang'] = $_POST['goods_lang'];
				$where['meal_id'] = array('in',$_POST['meal_spec']);
				$list = $model->getMealList($where);
				$data = array();
				foreach($list as $k => $v){

					 $goods_price +=$v['meal_price'];

				}				
		
				
			  
				$goods_size_price = 0;
				$spec_price = 0;
				$goods_original_price = 0;






		}



		
		//验证是否可以购买
		if(empty($goods_info)) {
		    output_error('商品已下架或不存在');
		}
		   
			   
		if(intval($goods_info['goods_stock']) < 1 || intval($goods_info['goods_stock']) < $quantity) {
		    output_error('库存不足');
		}
		
		
		if(!empty($goods_info['goods_image'])){
			$data = explode(",",$goods_info['goods_image']);
			$image = $data[0];
		}else{
			$image = '';	
		}
		
	
	  //  print_r($checkSpec['goods_lang']);
	  
	
		$param = array();		
		$param['store_id']      = $goods_info['store_id'];
		$param['goods_id']      = $goods_info['goods_id'];
		$param['goods_name']    = $checkSpec['goods_name'];
		$param['goods_price']   = $goods_price;
		$param['goods_image']   = $image;
		//$param['store_name']  = $goods_info['store_name'];
	
		$param['goods_spec'] = 		$spec_name;
		$param['goods_spec_price'] 	= ncPriceFormat($spec_price);
		$param['goods_size'] 		= $size_name;
		$param['goods_size_price'] 	= ncPriceFormat($goods_size_price);		
	//	$param['goods_optional'] 	= $goods_optional;			
	//	$param['goods_optional_price'] = ncPriceFormat($optional_price);	
		
		
		//$param['goods_comment'] = $_POST['goods_comment'];		
		$param['goods_lang']     = $_POST['goods_lang'];
		$param['goods_class_id'] = $goods_info['gc_id'];		
		$param['order_id']       = $_POST['order_id'];						
		$param['rec_id']         = $_POST['rec_id'];		
		$param['goods_num']      = $quantity;
		
		$param['goods_size_id']      = $_POST['goods_size'];
		$param['goods_specs_id']     = $_POST['goods_spec'];
		$param['goods_code']         = $_POST['goods_code'];
		$param['goods_optional_ids'] = $_POST['goods_optional'];
		$param['version'] = $version_num + 2;
		$param['is_take_ffect'] = 2;
		
		if($goods_info['goods_is_set_meal'] == 1){
			$param['set_meal'] = 1;
			$param['set_meal_spec'] = implode(',',$_POST['meal_spec']);	
			$meal_list = model('store_goods_meal')->getMealList(array('lang'=> $_POST['goods_lang'],'meal_id' => array('in',$param['set_meal_spec'])));
			foreach($meal_list as $v){
				$goods_spec[] = $v['meal_value'];
			}
			$param['goods_spec'] = implode('/',$goods_spec);

		}

	    //print_r($param);
		
			
		$condition = array();		
		$condition['goods_id']      = $goods_info['goods_id'];
		$condition['rec_id']        = $_POST['rec_id'];					
		$condition['goods_size_id']    = $_POST['goods_size'];  	
		
		if(!empty($param['goods_spec'])){
			$condition['goods_specs_id'] = $param['goods_spec'];  	
		}

		
		if(!empty($param['goods_optional'])){
			$condition['goods_optional_ids'] = $param['goods_optional'];  	
		}


		
		//print_r($condition);			
					
		$check_cart = $this->check_cart($condition);		
		//print_r($check_cart);
		
		if($check_cart){			
		    
			$update = array();
			$update['goods_num'] = $check_cart['goods_num'] + $quantity;	
			$where = array();
			$where['replace_id'] =  $check_cart['replace_id'];
			$row = model('order_goods_replace')->editGoods($where,$update);
			
		}else{	
		    
			$row = model('order_goods_replace')->addGoods($param);
			
		}
		
		
		if($row){				    
		    
			output_data('ok');			
		}else{
			output_error('保存失败');
		}
	}
	
	//获取语言包属性
	private function getLangGoods($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_code' => $goods_id,
			'lang_name' => $lang_name,
			'is_old' => 0
			
		);		
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);		
		return $goods_lang;		
	}
	
		//更新主订单商品的可用商品数数量
	private function editGoodsUsable($data,$type){
		
		//更新主订单			
		$where = array(
			'rec_id' => $data['rec_id'],
			'order_id' => $data['order_id']
		);
		
		$info = model('order')->getOrderGoodsInfo($where);
		
		$data = array();
		
		if($type == 'plus'){
		    
		    $data['goods_usable_num'] =  $info['goods_usable_num'] + 1;
		    
		}else{
		    
		    $data['goods_usable_num'] =  $info['goods_usable_num'] - 1;
		    
		}
		
		model('order')->editOrderGoods($data,$where);	
		
	}
	
	
	
	//获取
	private function check_cart($where){
		
		//	print_r($where);
			$info  = model('order_goods_replace')->getGoodsInfo($where);		
		
		//	print_r($info);
			return $info;
	}


	
	
	/**
	 * 更新数量
	 */
	public function order_replace_numOp() {
	    $replace_id = intval(abs($_POST['replace_id']));
	    $quantity = intval(abs($_POST['quantity']));
		$store_id = $this->store_info['store_id'];
		
	     if(empty($replace_id) ||  empty($store_id)) {
	        output_error('参数错误');
	    }
	
	    $model_replace = Model('order_goods_replace');	
	    $replace_info = $model_replace->getGoodsInfo(array('replace_id'=>$replace_id, 'store_id'=> $store_id));
		   
	    $data = array();
	    $data['goods_num'] = $quantity;		
	    $update = $model_replace->editGoods(array('replace_id'=>$replace_id),$data);
	    if ($update) {
	        output_data('修改成功');
	    } else {
	        output_error('修改失败');
	    }
	}	
	
	//编辑商品	
	private function editReplace($data,$where){
		
				
		
	}
	
	
	//校验传过来的属性和规格是否正确	
	private function checkSpec($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_id' => $goods_id,
			'lang_name' => $lang_name
		);
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);
		
		$goods_sizespec = unserialize($goods_lang['goods_sizeprice']);
		
		$new_sizespec = array();
		foreach($goods_sizespec as $v){			
			$new_sizespec[$v['name']] = $v['price'];
		}
		
		
		$new_specs = array();
		$goods_specs = unserialize($goods_lang['goods_specs']);
		foreach($goods_specs as $v){			
			$new_specs[$v['value']] = $v['price'];			
		}
		
		
		
		$data['goods_sizespec'] = $new_sizespec;
		$data['goods_specs'] = $new_specs;
		$data['goods_lang'] = $goods_lang;
		
		return $data;
			
		
	}
	
	
	
	//回退商品和全部删除
	public function del_replaceOp(){
		$where= array();
		$where['order_id'] = $_POST['order_id'];
		if($_POST['rec_id'] > 0){

		    $where['rec_id'] = $_POST['rec_id'];
			
		}

		if($_POST['replace_id'] > 0){

			$where['replace_id'] = $_POST['replace_id'];

		}

		$where['store_id'] = $this->store_info['store_id'];

		$info = model('order_goods_replace')->getGoodsInfo($where);


		if($info){
		
			$row = model('order_goods_replace')->delGoods($where);
			
			model('order_goods_edit')->delGoods(['rec_id' => $info['rec_id'],'type' => 'replace']);
			
			//获取替换退差价的商品详情	
			if($row){	

				
				$refund_info = model('order_goods_refund')->getRefundInfo(['replace_rec_id' => $info['rec_id']]);
				if(!empty($refund_info)){

					model('order_goods_refund')->delRefund(['refund_id' => $refund_info['refund_id']]);		
					model('order_refund')->delRefund(['refund_id' => $refund_info['refund_id']]);

				}



				output_data('操作成功');
			}else{
				output_error('操作失败');
			}
					
		}else{
			output_error('操作失败');
		}		
		
	}
	
	
	//保存	
	public function save_order_replaceOp(){	
				
			$rec_id = intval($_POST['rec_id']);
			$order_id = intval($_POST['order_id']);
			$rec_num = intval($_POST['rec_num']);
			$where  =array(
				'rec_id' => $rec_id,
				'order_id' => $order_id,
				'store_id' => $this->store_info['store_id']
			);

			$update = array(
				'rec_num' => $rec_num
			);
			$row = model('order_goods_replace')->editGoods($where,$update);				
			if($row){

			    $goods_info = model('order')->getOrderGoodsInfo(['rec_id' => $rec_id]);
				// 更新价格				
				$old_replace_amount = $goods_info['goods_price'] * $rec_num;
				$replace_list = model('order_goods_replace')->getGoodsList(['rec_id' => $rec_id]);
				$replace_amount =0; //替换的商品总价
				$replace_num = 0;
				foreach($replace_list as $v){
					$replace_num += $v['goods_num'];
					$replace_amount += $v['goods_price'] * $v['goods_num'];
				}

				if( $replace_amount >= $old_replace_amount ){
					//获得单价
					$goods_price = $old_replace_amount / $replace_num;
					//更新价格
					foreach($replace_list as $v){
						model('order_goods_replace')->editGoods(['replace_id' => $v['replace_id']],['goods_price' => $goods_price]);
					}

				}elseif($replace_amount < $old_replace_amount){ //生成退款
					

					//计算要退的差价
					$refund_price = $old_replace_amount - $replace_amount;

					//获取历史单数量
					$version = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
					$version_num = $version + 2;

					$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1]);
					$refund_id = $refund_info['refund_id'];
					if (empty($refund_info)) { //如果当前没有正在编辑的退款记录，则创建一个新的退款记录。

						$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id, 'store_id' => $this->store_info['store_id']));
					
						$refund_stage = 0;
						if ($order_info['order_state'] == 40 || $order_info['order_state'] == 50) {
							$refund_stage = 1;
						}elseif($order_info['order_state'] == 60){
							$refund_stage = 2;
						}						
						$refund_type = 1;
						$data = array();
						$data['order_id']    = $order_info['order_id'];
						$data['order_sn']    = $order_info['order_sn'];
						$data['store_id']    = $order_info['store_id'];
						$data['store_name']  = $order_info['store_name'];
						$data['member_id']   = $order_info['buyer_id'];
						$data['role']       =  0;
						$data['refund_comment'] ='';
						$data['add_time']   = time();
						$data['refund_type'] = $refund_type;
						$data['refund_amount'] = 0;
						$data['refund_stage'] =  $refund_stage;
						$data['refund_log'] = ''; // serialize($order_refund);
						$data['is_delivery'] = 1;
						$data['is_commission'] = 1;    //商家操作是不退抽成的。
						$data['is_edit'] = 1; //证明这是订单编辑的
						$data['version'] = $version_num;
						$data['is_replace'] = 1;
						$refund_id = model('order_refund')->addRefund($data);						
					}

					//写入详细商品
							$data = array();
							$data['refund_id'] = $refund_id;
							$data['order_id'] = $goods_info['order_id'];
							$data['goods_id'] = $goods_info['goods_id'];
							$data['goods_name'] =  $goods_info['goods_name'];
							$data['goods_price'] = $refund_price;
							$data['goods_num'] = $rec_num;
							$data['goods_image'] = $goods_info['goods_image'];
							$data['goods_pay_price'] = $goods_info['goods_pay_price'];
							$data['store_id'] = $goods_info['store_id'];
							$data['buyer_id'] = $goods_info['buyer_id'];
							$data['gc_id'] = $goods_info['gc_id'];
							$data['goods_size_id'] = $goods_info['goods_size_id'];
							$data['goods_specs_id'] = $goods_info['goods_specs_id'];
							$data['goods_spec'] = $goods_info['goods_spec'];
							$data['goods_size'] = $goods_info['goods_size'];
							$data['original_goods_id'] = $goods_info['original_goods_id'];
							$data['original_goods_name'] = $goods_info['original_goods_name'];
							$data['original_goods_num'] = $goods_info['original_goods_num'];
							$data['original_goods_spec'] = $goods_info['original_goods_spec'];
							$data['original_goods_size'] = $goods_info['original_goods_size'];
							$data['original_goods_price'] = $goods_info['original_goods_price'];
							$data['goods_lang'] = $goods_info['goods_lang'];
							$data['goods_optional_ids'] = $goods_info['goods_optional_ids'];
							$data['goods_optional'] = $goods_info['goods_optional'];
							$data['original_goods_optional'] = $goods_info['original_goods_optional'];
							$data['foodbox_price'] = $goods_info['foodbox_price'];
							$data['foodbox_id'] = $goods_info['foodbox_id'];
							$data['foodbox_name'] = $goods_info['foodbox_name'];
							$data['foodbox_num'] = $goods_info['foodbox_num'];
							$data['rec_id'] = $goods_info['rec_id'];
							$data['goods_code'] = $goods_info['goods_code'];
							$data['version'] = $version_num;
							$data['is_replace'] = 1;
							$data['replace_rec_id'] = $rec_id;
							$row = model('order_goods_refund')->addRefund($data);
							
							$this->updateAmount($order_id);


							
				}

			
				/*
				//更新主商品信息。
			    $where = array(
			        'rec_id' => $rec_id
			    );
			    $update = array(
			        'goods_usable_num' => array('exp','goods_usable_num -'.$rec_num),
			        'replace_num' => array('exp','replace_num +'.$rec_num) 
			    );			    
			    $model_order = model('order')->editOrderGoods($update,$where);
				*/
				$goods_edit = model('order_goods_edit')->getGoodsInfo(['rec_id' => $rec_id,'type' => 'replace']);
				if(empty($goods_edit)){

					$data = array();
					$data['rec_id'] 	= $goods_info['rec_id'];
					$data['rec_num'] 	= $_POST['rec_num'];
					$data['type'] 		= 'replace';
					$data['order_id'] 	= $goods_info['order_id'];	
					//print_r($data);				
					model('order_goods_edit')->addGoods($data);

				}else{

					model('order_goods_edit')->editGoods(['rec_id' => $rec_id],['rec_num' => $_POST['rec_num']]);

				}

			    
				output_data('保存成功');				
			}else{
				output_error('保存失败');				
			}
		
	}
	

		//更新金额
		private function updateAmount($order_id){

			$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id, 'store_id' => $this->store_info['store_id']));

			//获取stripe支付的金额
			$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
			//获取钱包支付的金额
			$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			//获取优惠券抵扣的金额
			$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

			$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1]);

			$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
			$refund_amount = 0;
			foreach($refund_goods_list as $v){
				$refund_amount +=  $v['goods_price'] * $v['goods_num'];
			}

			$update = array();
			$update['refund_amount'] = $refund_amount;
			$update['refund_pay_amount'] = 0;
			$update['refund_wallet_amount'] = 0;
			$update['refund_coupon_amount'] = 0;
			
			if($stripe_amount > 0 ){ //stripe youqian
				
				if($stripe_amount >= $refund_amount){ //stripe 有钱，直接退	

					$update['refund_pay_amount'] = $refund_amount;	

				}elseif($stripe_amount < $refund_amount && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退		

					$update['refund_pay_amount'] = $stripe_amount;
					
					$refund_wallet_amount = $refund_amount - $stripe_amount ;	
					
					if($wallet_amount >= $refund_wallet_amount  ){
					
						$update['refund_wallet_amount'] = $refund_wallet_amount;	
					
					}elseif($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0 ){
						
						$update['refund_wallet_amount'] = $wallet_amount;	

						$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;
					
						$update['refund_coupon_amount'] = $refund_coupon_amount;

					}else{
						$update['refund_coupon_amount'] = $refund_wallet_amount;
					}

				}	

			}elseif($wallet_amount > 0 ){ //stripe 没钱，钱包 有钱。

				if($wallet_amount >= $refund_amount ){ //余额大于退款金额。直接退。

					$update['refund_wallet_amount'] = $refund_amount;

				}elseif($wallet_amount < $refund_amount &&  $wallet_amount > 0 ){ //有钱，但钱不够。
						
					$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

					$refund_coupon_amount = $refund_amount - $wallet_amount;
				
					$update['refund_coupon_amount'] = $refund_coupon_amount;

				}else{

					$update['refund_coupon_amount'] = $wallet_amount;

				}


			}elseif($voucher_amount > 0 ){ //优惠券支付的。

				if($voucher_amount >= $refund_amount ){
					
					$update['refund_coupon_amount'] = $refund_amount;

				}

			}
		
			model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);

}


	
	

}
