<?php

/*
stripe
*/

defined('InMadExpress') or exit('Access Invalid!');
class stripeControl extends mobileAdminControl {
   
    private $stripe;
	
    public function __construct(){
        parent::__construct();		
		require_once(BASE_PATH.DS.'api'.DS.'stripe-php/init.php');		
		$this->stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');				
    }



	//获取账号列表	
	public function get_accountsOp(){		
				
					
			$list_data = $this->stripe->accounts->all(array('limit' => 10));	
			$data = json_decode(json_encode($list_data),TRUE);
			$list = array();
			foreach($data['data'] as $k=>$v){				
				$list[$k] = $v;
			}						
			output_data($list);		
	}	

	
	public function create_storeOp(){		
		$model = model('store_stripe');
		$data = array();		
		$data['store_id'] 	= $_POST['store_id'];		
		$data['name'] 		= $_POST['name'];	
		$data['phone'] 		= $_POST['phone'];	
		$data['address'] 	= $_POST['address'];	
		$data['email'] 		= $_POST['email'];	
		$data['tax_id'] 	= $_POST['tax_id'];	
		$data['vat_id'] 	= $_POST['vat_id'];	
		$data['type'] 		= $_POST['type'];	
		$data['country'] 	= $_POST['country'];
		$row =$model->addStoreStripe($data);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}



	//创建账号
	
	public function create_accountsOp(){
		
		$data = array();
		$data['type'] = 'custom';
		$data['country'] = 'AU';
		$data['email'] = '470052365@qq.com';
		$data['capabilities'] = array(
			'card_payments' => array('requested'=> true),
			'transfers' =>  array('requested'=> true)
		);
		$data['business_type'] = 'company';
		$data['company'] = array(
			'address' => array(
				'city' => 'Melbourne',
				'country' => 'AU',
				'line1'	=> '6 May Road',				
				'postal_code' => '3000',
				'state' => 'VIC'
			),			
			'name' => '郑十八佛跳墙',							
			'phone' => '+61459666011',	
			'tax_id' => '000111222',
			'vat_id' => '123456'
		);			
		$data['business_profile'] = array(
			'mcc' => 5812,
			'url' => 'www.mad-express.com'
		);
		$data['tos_acceptance'] = array(
			'date' 		=> '',
			'ip' 		=> '',
			'user_agent' => ''
		);
		
		/*$data['external_account'] = array(
			'object' => '110000',
			'country' => 'AU',
			'currency' => 'AUD',
		//	'account_holder_name' => '',
		//	'account_holder_type' => '',
			//'routing_number' => '',
			'account_number' => '000123456',			
		);*/
		
		print_r($data);
	
		$result = $this->stripe->accounts->create($data);
		
		
		
		print_r($result);
		
	}
	
	
	
	
	//编辑账号
	public function update_accountsOp(){
		/*
	
	    $data = [		
			'company' =>[ //企业用户
				'address' =>[
					'city' => '',
					'country' => '',
					'line1'	=> '',
					'line2' => '',
					'postal_code' => '',
					'state' => ''
				],
				'address_kana'=> [
					'city' => '',
					'country' => '',
					'line1'	=> '',
					'line2' => '',
					'postal_code' => '',
					'state' => '',
					'town' => ''
				],
				'address_kanji' => [
					'city' => '',
					'country' => '',
					'line1'	=> '',
					'line2' => '',
					'postal_code' => '',
					'state' => '',
					'town' => ''
				],
				'directors_provided' => '',
				'executives_provided' => '',
				'name' => '',
				'name_kana' => '',
				'name_kanji' => '',
				'owners_provided' => '',
				'phone' => '',
				'structure' => '',
				'tax_id' => '',
				'tax_id_registrar' => '',
				'vat_id' => '',
				'verification' => [
					'document'=>[
						'back' => '',
						'front' => ''
					]
				]
			],
			'individual' => [
				'address'=>'',
				'address_kana' => '',
				'address_kanji' => '',
				'dob' => '',
				'email' => '',
				'first_name' => '',
				'first_name_kana' => '',
				'first_name_kanji' => '',
				'gender' => '',
				'id_number' => '',
				'last_name' => '',
				'last_name_kana' => '',
				'last_name_kanji' => '',
				'maiden_name' => '',
				'metadata'=> '',
				'phone' => '',
				'political_exposure' => '',
				'ssn_last_4' => '',
				'verification' => [
					'additional_document' => [
						'back' => '',
						'front' => ''
					],
					'document' => [
						'back' => '',
						'front' => ''
					]					
				]
			]
		];	
	*/
		$result = $this->stripe->accounts->update('acct_1HP19wK8d8ovzfDB',$data);
		
		
	}
	
	
	//删除账号
	public function del_accountsOp(){		
		
		$id = $_POST['id'];	
		$row  = $this->stripe->accounts->delete($id);
		print_r($row);
	}
	
	
	
	//添加银行账号	
	public function create_bank_accountOp(){	
		
		$id = $_POST['id'];		
		$data = array();
		$data['external_account'] = array(
			'object' => 'bank_account',
			'country' => $_POST['country'],//'AU',
			'currency' => $_POST['currency'] ,//'AUD',
			'routing_number' => $_POST['routing_number'],
			'account_number' => $_POST['account_number']
		);				
				
		print_r($data);
		print_r($id);
		
		$result = $this->stripe->accounts->createExternalAccount($id,$data);
		
		print_r($result);
		
	}
	
	
	//获得银行账号
	public function get_bank_accountOp(){
		
		
		$id= $_POST['id'];
		$data = array();
		$data['object'] = "bank_account";
		$data['limit'] = 10;
		$result = $this->stripe->accounts->allExternalAccounts($id,$data);
		$data = json_decode(json_encode($result),TRUE);
	//	print_r($data);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}						
		output_data(array('list'=>$list ));		
		
	}
	
	//编辑
	public function update_bank_accountOp(){
		
		
		
	}
	
	public function del_bank_accountOp(){		
		
		$id = $_POST['id'];		
		$bank_id = $_POST['bank_id']; 
		$result = $this->stripe->accounts->deleteExternalAccount($id,$bank_id);
				
		print_r($result);
		
	}
	
	
	
	/*
	
	
	
	*/
	
	
	//账号主体负责人
	public function get_personsOp(){
		
		
		$list_data = $this->stripe->accounts->allPersons(array('limit' => 10));
		$data = json_decode(json_encode($list_data),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}					
		output_data($list);
		
	} 
	
	
	//添加管理人员
	
	public function create_personOp(){
		
		$id = $_POST['id'];
		$data = array();
		$data['address']  = array(
			'city' 		=> '',
			'country' 	=> '',
			'line1' 	=> '',
			'line2' 	=> '',
			'postal_code' => '',
			'state' 	=> ''
		);		
		$data['dob'] = array(
			'day' 	=>	'',			
			'month' =>	'',
			'year' 	=> 	''
		);		
		$data['email']  	= '';
		$data['first_name'] = '';
		$data['id_number'] 	= '';
		$data['last_name'] 	= '';
		$data['phone'] 		= '';
		
		$result = $this->stripe->accounts->createPerson($id,$data);
		
		print_r($result);
		
		
		
	}
	
	//账号负责人详情
	public function get_persons_infoOp(){
		
		$id = $_POST['id'];
		$persons_id = $_POST['persons_id'];
		
		$result = $this->stripe->accounts->retrievePerson($id,$persons_id);
		
		print_r($result);
		
		
		
	}
	
	//编辑
	public function update_personOp(){
		
		
		$id 		= $_POST['id'];
		$persons_id = $_POST['persons_id'];
		
		
		$data = array();
		$data['address']  = array(
			'city' 		=> '',
			'country' 	=> '',
			'line1' 	=> '',
			'line2' 	=> '',
			'postal_code' => '',
			'state' 	=> ''
		);
		
		$data['dob'] = array(
			'day' 	=>	'',			
			'month' =>	'',
			'year' 	=> 	''
		);
		
		$data['email']  	= '';
		$data['first_name'] = '';
		$data['id_number'] 	= '';
		$data['last_name'] 	= '';
		$data['phone'] 		= '';
		
		$result = $this->stripe->accounts->updatePerson($id,$persons_id,$data);
		
		
		print_r($result);
		
	}
	
	public function del_personOp(){
				
		$id = $_POST['id'];
		$persons_id = $_POST['persons_id'];	
					
		$result = $this->stripe->accounts->deletePerson($id,$persons_id);
		
		print_r($result);
		
	}

}