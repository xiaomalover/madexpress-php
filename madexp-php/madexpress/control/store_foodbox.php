<?php
/**
 * 商户
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_foodboxControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	
	
	public function foodbox_listOp() {
	
		//获取餐盒规格		
		 $foodbox = model('foodbox_class')->getClassList(TRUE);
		 $foodbox_list = array();
		 
		 foreach($foodbox as $k=> $v){
			$foodbox_list[$k] = $v;
			$foodbox_list[$k]['class_name'] = $v['class_name_sub'];
			$foodbox_list[$k]['child'] = $this->getStoreGoods($v['class_id']);			 
		 }

		output_data(array('list' => $foodbox_list));
	}
	
	
	
	
	
	//获取当前商铺的剩余餐盒
	private function getStoreGoods($id){		
	
		$model_store_box = model('store_foodbox_goods');
		$where  =array(
			'foodbox_goods.class_id' => $id,
			'store_foodbox.store_id' => $this->store_info['store_id']
		);
		
		$box = $model_store_box->getStoreFoodboxGoodsJoinList($where);
	
		$data = array();
		foreach($box as  $k=> $v){
			$data[$k]['goods_id'] = $v['goods_id'];
			$data[$k]['goods_name'] = $v['goods_name'];
			$data[$k]['goods_num'] = $v['goods_num'];
			$data[$k]['goods_icon'] = $v['goods_icon'];
		}
		return $data;
				
	}
	
	
	
	public function boxAddOp(){
		
		  $order_model = model('foodbox_order');
		  
	
		  if (chksubmit()) {
			 	
			  $cantData = $this->cantData();
			  
			  $data = array();
			  $data['order_sn'] 			= $order_model->makeSn($this->store_info['store_id']);
			  $data['order_add_time'] 		= time();
			  $data['order_end_time'] 		= time() + (3600*72);  
			  $data['order_status'] 		= 10;
			  $data['order_notes'] 			= $_POST['order_notes'];			  
			  $data['goods_price'] 			= $cantData['goods_price'];
			  $data['goods_amount'] 		= $cantData['goods_amount'];
			 
			  if($_POST['order_type'] == 0){	
				   $data['dispatching_fee'] 		= 0;
			  }else{				  
				   $data['dispatching_fee'] 		= C('foodbox_dispatching_fee');		
				  
			  }
			  $data['order_amount'] = $cantData['order_amount'] + $data['dispatching_fee'];	
			
			  $where = array(
			  		'store_id' => $this->store_info['store_id']
			  );
			  
			  $store_info = model('store')->getStoreInfo($where,'region_id,region_sm_id,region_name');
			  
			  $data['store_id'] 			= $this->store_info['store_id'];
			  $data['store_name'] 			= $this->store_info['store_name'];
			  $data['store_region_id'] 		= $store_info['store_region_id'];
			  
			  $row = $order_model->addOrder($data);
			  			  
			  if($row) {
					$cantData = $this->cantData($row);
					model('foodbox_order_goods')->addOrderGoodsAll($cantData['goods_data']);
					output_data('操作成功');
			}else{
					output_error('操作失败');
			  }
		  }	
				
		 $foodBox = Model('foodbox_goods')->getGoodsList(TRUE);	
		 
		
	}
	
	
	//格式化子项
	private function cantData($id = ''){
		
		$stock = $this->stockGoods();
		
		$goods = $_POST['foodbox'];
		$goodsData = array();
		$goods_price = 0;
		$goods_amount = 0;
		
		foreach($goods as $v){					  
			$goodsData[] = array(
							'order_id' => $id,
							'store_id' =>  $this->store_info['store_id'],
							'goods_id' =>  $v['goods_id'],
							'goods_num' =>  $v['goods_num'],
							'goods_price'=>  $stock[$v['goods_id']]['goods_price'],
							'total_price'=>  $stock[$v['goods_id']]['goods_price'] * $v['goods_num'] ,
							'class_id' =>  $stock[$v['goods_id']]['class_id'],			
							'class_name' =>  $stock[$v['goods_id']]['class_name'],			
							'goods_name' =>  $stock[$v['goods_id']]['goods_name'],				
							
							
							
			);
			$goods_price += $goods[$v['goods_id']]['goods_price'] * $v['goods_num'] ;			 
		  }
		
			$data = array(
				'goods_price'  => $goods_price,
				'goods_amount' => $goods_price,
				'goods_data'   => $goodsData
			);
		
			return $data;
	}
	
	
	
	//获取仓库的餐盒参数
	private function stockGoods(){
	
		$model= model('foodbox_goods');
		
		$data = $model->getGoodsList(TRUE);
		$list = array();
		foreach($data as $v){
			$list[$v['goods_id']] = $v;
		}		
		return $list;
	}
	
	
	public function get_order_listOp(){
		
		 $model_foodbox = model('foodbox_order');
		 $where = array(
			'store_id' => $this->store_info['store_id']
		 );
		 
		 if($_POST['keyword']){
			$where["order_sn"] = $_POST['keyword'];
		 }
		 $data_list = $model_foodbox->getOrderList($where,'*',10);
		 $page_count = $model_foodbox->gettotalpage();
		 $list_count = $model_foodbox->gettotalnum();
		 
		 $status = $this->status();
		 $data= array();
		 $status = $this->status();
		 foreach($data_list as $k=> $v){
			 
			 $data[$k] = $v;
			 $data[$k]['order_add_time'] = date('d/m/y',$v['order_add_time']);	
			 $data[$k]['order_send_time'] = $v['order_send_time'] > 0 ? date('d/m/y',$v['order_send_time']) : 'N/A';			
			 $data[$k]['order_status'] = time() >$v['order_end_time'] ? $status[50] :  $status[$v['order_status']];
			 
		 }
		 
		 		 
		 output_data(array('list' => $data), mobile_page($page_count,$list_count));		
		 
	}
	
	private function status(){
		
		$data = array();
		
		$data[10] = '待配送';
		$data[20] = '配送中';
		$data[30] = '待自提';
		$data[40] = '已完成';
		$data[50] = '已超时';
		
		return $data;
		
	}
	
	
		
	
}
