<?php
/*
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class orderControl extends mobileAdminControl
{
    public function __construct()
    {
        parent::__construct();
    }


    

    
    public function get_order_listOp()
    {
        $model_order = Model('order');
        $condition  = array();
		
		if($_POST['order_state'] == 70){
			
			$condition['is_problem'] = 1;
		}else{
		
			$condition['order_state'] = $_POST['order_state'];
        }
        //    $this->_get_condition($condition);
    
        if(is_array($_POST['delivery_fee'])){				
                $deliver_fee = $_POST['delivery_fee'];
              

			    $condition['delivery_fee'] = array('between',$_POST['delivery_fee']);
		}
		if(is_array($_POST['order_amount'])){
			  $condition['order_amount'] = array('between',$_POST['order_amount']);
		}
		if(is_array($_POST['sales_amount'])){
			 $condition['sales_amount'] = array('between',$_POST['sales_amount']);
		}
	
		if ($_POST['keyword'] != '') {
			  $condition['order_sn|store_name|store_phone|store_address|buyer_name|buyer_phone|buyer_phone|buyer_address|buyer_code'] = array('like', '%' . $_POST['keyword'] . '%');
		}
		
    
        $sort_fields = array('buyer_name','store_name','order_id','payment_code','order_state','order_amount','sales_amount','order_from','pay_sn','rcb_amount','pd_amount','payment_time','finnshed_time','evaluation_state','refund_amount','buyer_id','store_id');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'], $sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
        
        
        //print_r($this->page);
        
        $order_list = $model_order->getOrderList($condition, $this->page, '*', 'order_id desc', '', array('store'));
    
        //print_r($order_list);
        $page_count = $model_order->gettotalpage();
        $list_count = $model_order->gettotalnum();
        
        $data = array();
        foreach ($order_list as $order_id => $order_info) {
            $order_info['state_desc'] = orderState($order_info);
            $list = array();
            $list = $order_info;
            $list['order_id'] = $order_info['order_id'];
            $list['order_sn'] = $order_info['order_sn'];
            $list['add_times'] = date('i"s"', $order_info['add_time']);
            $list['order_amount'] = ncPriceFormat($order_info['order_amount']);
            $list['shipping_fee'] = ncPriceFormat($order_info['shipping_fee']);
            $list['goods_amount'] = ncPriceFormat($order_info['goods_amount']);
            $list['order_state'] = $order_info['state_desc'];
            $list['pay_sn'] = empty($order_info['pay_sn']) ? '' : $order_info['pay_sn'];
            $list['payment_code'] = orderPaymentName($order_info['payment_code']);
            $list['payment_time'] = !empty($order_info['payment_time']) ? (intval(date('His', $order_info['payment_time'])) ? date('d M y H:i:s', $order_info['payment_time']) : date('Y-m-d', $order_info['payment_time'])) : '';
            $list['rcb_amount'] = ncPriceFormat($order_info['rcb_amount']);
            $list['pd_amount'] = ncPriceFormat($order_info['pd_amount']);
            $list['shipping_code'] = $order_info['shipping_code'];
            $list['refund_amount'] = ncPriceFormat($order_info['refund_amount']);
            $list['sales_amount'] = ncPriceFormat($order_info['sales_amount']);
            $list['store_id'] = $order_info['store_id'];
            $list['store_name'] = $order_info['store_name'];
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];
            $list['store_phone'] = $order_info['store_phone'];
            $list['buyer_phone'] = $order_info['buyer_phone'];
            //调用地区
            //$list['store_region'] = $region[$order_info['store_region_sm_id']];
            //	$list['buyer_region'] = $region[$order_info['buyer_region_id']];
                
            $list['store_name_primary'] = $order_info['extend_store']['store_name_primary'];
            $list['store_name_secondary'] = $order_info['extend_store']['store_name_secondary'];
            
            
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];
            $list['store_phone'] = $order_info['store_phone'];
			
			
			$where = array(
			    'distributor_id' =>intval($order_info['distributor_id'])
			);		
			$deliveryInfo = model('waiter')->getWaiterInfo($where);			
			$list['delivery_avatar'] = getWaiterAvatar($deliveryInfo['distributor_avatar']);
			
            $list['distributor_id'] = $order_info['distributor_id'];
            $list['distributor_name'] = $order_info['distributor_name'];
            $list['distributor_mobile'] = $order_info['distributor_mobile'];
            $list['distributor_start_time'] = $order_info['distributor_start_time'];
            $list['distributor_end_time'] = $order_info['distributor_end_time'] > 0 ? date('Y-m-d H:i:s', $order_info['distributor_end_time']) : 'N/A';
            $list['distributor_code'] = $order_info['distributor_code'];
            $list['is_delay'] = $order_info['is_delay'];
            $list['is_confirm'] = $order_info['is_confirm'];
            
			$list['problem_content'] = $order_info['problem_content'];
			
            $list['send_state'] = $order_info['distributor_end_time'] > $order_info['order_estimate_time'] ? '超时送达':'按时送达';
                
                
            $data[] = $list;
        }
        
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
        //exit(Tpl::flexigridXML($data));
    }
    

    
    
    
    //左侧菜单
    public function get_order_tabOp()
    {
        $menu_array = array(
        array('menu_key'=>'state_handle',
              'menu_name'=>'分配中',
              'menu_state' => 20,
              'menu_icon'=> 'icon_chulizhong_n',
              'menu_act_icon'=> 'icon_chulizhong_h',
			  'type' => 'svg',
              'menu_count' => $this->state_count(20),
            ),
              
        array('menu_key'=>'state_dist',
              'menu_name'=>'处理中',
              'menu_state' => 30,
             'menu_icon'=> 'icon_fenpeizhong_n',
              'menu_act_icon'=> 'icon_fenpeizhong_h',
			  'type' => 'svg',
               'menu_count' => $this->state_count(30),
              ),
              
        array('menu_key'=>'state_take',
              'menu_name'=>'取货中',
              'menu_state' => 40,
              'menu_icon'=> 'icon_quhuozhong_n',
              'menu_act_icon'=> 'icon_quhuozhong_h',
			  'type' => 'svg',
               'menu_count' => $this->state_count(40),
             ),
              
        array('menu_key'=>'state_send',
              'menu_icon'=> 'icon_peisongzhong_n',
              'menu_act_icon'=> 'icon_peisongzhong_h',
              'menu_name'=>'配送中',
              'menu_state' => 50,
			  'type' => 'svg',
               'menu_count' => $this->state_count(50),
              ),
        array('menu_key'=>'state_success',
              'menu_icon'=> 'icon_yiwancheng_n',
              'menu_act_icon'=> 'icon_yiwancheng_h',
              'menu_name'=>'已完成',
              'menu_state' => 60,
			  'type' => 'svg',
               'menu_count' => $this->state_count(60),
              ),
	/*	array('menu_key'=>'state_cancel',
		     'menu_icon'=> 'icon_wentidan_n',
		     'menu_act_icon'=> 'icon_wentidan_h',
		     'menu_name'=>'已取消',
		     'menu_state' => 0,
			  'type' => 'svg',
		      'menu_count' => $this->state_count(0),
		    ),	  
	    */		  
        array('menu_key'=>'state_question',
              'menu_icon'=> 'icon_wentidan_n',
              'menu_act_icon'=> 'icon_wentidan_h',
              'menu_name'=>'问题单',
              'menu_state' => 70,
			  'type' => 'svg',
              'menu_count' => $this->state_problem(),
             ),
         array('menu_key'=>'state_cancel',
              'menu_icon'=> 'icon_wentidan_n',
              'menu_act_icon'=> 'icon_wentidan_h',
              'menu_name'=>'未生成',
              'menu_state' => 0,
			  'type' => 'svg',
               'menu_count' => $this->state_count(0),
             )
        );
        
        
        output_data($menu_array);
    }
    
	
	private function state_problem(){
		
		$model = Model('order');
		$where = array(
		    'is_problem'=> 1
		 );
		$count = $model->getOrderCount($where);
		return $count;
		
	}
	
    private function state_count($state)
    {
        $model = Model('order');
        $where = array(
            'order_state'=> $state
         );
        $count = $model->getOrderCount($where);
        return $count;
    }
    
    
    //订单详情
        
    public function order_historyOp()
    {
        //编辑过的历史订单
            
        $where = array(
			'order_id' => $_POST['order_id']
        );
        $list = model('order_snapshot')->getSnapshot($where);
        $history = array();
        foreach ($list as $k=>$v) {
            $history[$k]['snapshot_id'] = $v['snapshot_id'];
            $history[$k]['order_sn'] 	= $v['order_sn'];
            $history[$k]['order_id'] 	= $v['order_id'];
            $history[$k]['is_old_num'] 	= $v['is_old_num'];
            $history[$k]['snapshot_amendment'] = $v['snapshot_amendment'];
            $history[$k]['date'] = date('d M y', $v['snapshot_time']);
            $history[$k]['time'] = date("H:i:s", $v['snapshot_time']);
            $history[$k]['type'] = 'snapshot';
        }
		
		//获取最新的订单
		$num = count($history);
		
		$where = array();
		$where['order_id'] = $_POST['order_id'];
		$order_info = model('order')->getOrderInfoN($where);
		
		
		$history[$num+1] = array(
			'snapshot_id' 	=> '0',
			'order_sn' 		=> $order_info['order_sn'],
			'order_id' 		=> $order_info['order_id'],
			'is_old_num' 	=> $order_info['is_old_num'],			
			'snapshot_amendment' => $order_info['snapshot_amendment'],
			'date' => date('d M y', $order_info['snapshot_time']),
			'time' => date("H:i:s", $order_info['snapshot_time']),
			'type' => 'order'
		);
		    
        output_data(array('list' => $history));
    }
        
        
    public function order_infoOp()
    {
        $type = $_POST['type'];
        if ($type == 'snapshot') {
            $this->snapshot_order_info();
        }
        if ($type == 'order') {
            $this->order_info();
        }
    }
        
        
    //快照订单
    private function snapshot_order_info()
    {
        $order_id = intval($_POST['order_id']);
        $snapshot_id = intval($_POST['snapshot_id']);
            
            
           
        if (!$order_id) {
            output_error('订单编号有误');
        }
		
        $model_order = Model('order_snapshot');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['snapshot_id'] = $snapshot_id;
        $order = $model_order->getOrderInfo($condition, array('order_common','order_goods','member','store','replace','goods_give','one_stage_refund','two_stage_refund','three_stage_refund','order_log'));
                         
        
     
        if (empty($order)) {
            output_error('订单信息不存在');
        }
        
        
     
        $order_item = array();
        //从新格式化
        $order_member = array();
        $order_member['member_name'] 	 = $order['extend_member']['member_name'];
        $order_member['member_truename'] = $order['extend_member']['member_truename'];
        $order_member['member_avatar'] 	 = $this->getMemberAvatar($order['extend_member']['member_avatar']);                            
        $order_member['member_code'] 	 = $order['extend_member']['member_code'];                        
		$order_member['member_id'] 	 		= $order['extend_member']['member_id'];
        $order_item['order_member'] = $order_member;                            
        $order_item['order_store'] = $order['extend_store'];                           
                            
        $order_user = array();
        $order_user['buyer_code'] 	= $order['buyer_code'];
        $order_user['buyer_name'] 	= $order['buyer_name'];
        $order_user['buyer_phone'] 	= $order['buyer_phone'];
        $order_user['buyer_comment'] = $order['buyer_comment'];
        $order_user['buyer_address'] = $order['buyer_address'];
        $order_user['buyer_is_new'] = $order['buyer_is_new'];
        $order_user['buyer_region'] = $order['buyer_region'];
        $order_user['buyer_region_color'] = $order['buyer_region_color'];                        
        $consignee_attr = Model('consignee_attr')->getConsigneeAttrInfo(array('attr_id'=>$order['consignee_attr_id']));
        $order_user['consignee_attr']  =  UPLOAD_SITE_URL.'/icon/'.$consignee_attr['attr_icon'];
        
        //根据账号调用账号信息
                            
                            
        //调用收获人信息
                    
                            
        $order_user['distributor_name'] = $order['distributor_name'];
        $order_user['distributor_mobile'] = $order['distributor_mobile'];
        $order_user['state'] = $order['order_state'];
        $order_user['evaluation_state'] = $order['evaluation_state'];
                            
        if ($order['evaluation_state'] == 1) {
            $model = model('evaluate_store');
            $where = array('store_evaluate_orderid' => $order['order_id']);
            $eva = $model->getEvaluateStoreInfo($where);
            $order['evaluation_text'] = $eva['store_evaluate_score_text'];
            $order['evaluation_content'] = $eva['store_evaluate_score_content'];
        }
                           
        $order_item['order_user'] = $order_user;    
        $order_item['member_address'] = unserialize($order['member_address']);
		
		
		$address_info = model('address')->getDefaultAddressInfo(array('address_id' => $order['address_attr_id']));
		
	
        $address_attr =  model('address_attr')->getAddressAttrInfo(array('address_icon' => $address_info['address_icon']));
        $order_item['member_address']['address_attr']  =  UPLOAD_SITE_URL.'/icon/'.$address_attr['address_icon_url'].'black@2x.png';

        //订单商品,原始商品，不做任何修改
        $order_goods = array();
        $goods =  $order['extend_order_goods'];
        $goods_num = 0;
        $goods_list = array();
        foreach ($goods as $k => $v) {
            $goods_list[] = $v;
            $goods_num += $v['goods_num'];
        }
                            
        $order_goods['goods_lang']  = $goods_list[0]['goods_lang'];
        $order_goods['goods_list']  = $goods_list;
        $order_goods['goods_num']   = $goods_num;
        $order_item['order_goods']  = $order_goods;
                            
                    
        //金额信息
        $order_amount = array();
        //商品小计
        $order_amount['goods_amount'] = $order['goods_amount'];
        //商品折扣
        $order_amount['sales_amount'] = $order['sales_amount'];
        //餐盒费
        $order_amount['foodbox_amount'] = $order['foodbox_amount'];
        //配送费
        $order_amount['delivery_amount'] = $order['delivery_fee'];
        //支付手续费                            
        $order_amount['pay_commission_amount'] = $order['pay_commission_amount'];
                            
        //平台抽成
        $order_amount['commission_amount'] = $order['commission_amount'];
        //订单价格
        $order_amount['order_amount'] = $order['order_amount'];
        //商家优惠券
        $order_amount['store_coupon_price'] = $order['store_coupon_price'];
        //平台优惠券
        $order_amount['platform_coupon_price']  = $order['platform_coupon_price'];
        //订单实付
        $order_amount['pay_order_amount'] = $order['pay_order_amount'];                            
        $order_amount['extxnfee_stripe'] = $order['extxnfee_stripe'];                            
        
        $order_item['order_amount'] = $order_amount;
                            
                            
        
        
        //一阶段退款
        $one_stage_refund = array();
        $refund = $order['extend_one_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }

        //统计所有退款的金额。
        $version = $order['is_old_num'];
        $goods_amount_data = model('order_snapshot_refund')->getRefundInfo(['refund_stage'=> 0 ,'snapshot_id' => $order['snapshot_id']],'SUM(refund_amount) as refund_amount');


        $one_stage_refund['total_amount'] = $goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;

        $one_stage_refund['goods_num'] 	= 	$goods_num;
        $one_stage_refund['goods_amount'] = $goods_amount;
        $one_stage_refund['goods_list'] = $order['extend_one_stage_refund'];
        $order_item['one_stage_refund'] = $one_stage_refund;
                
                
        		
        //二阶段退款
        $two_stage_refund = array();
        $refund = $order['extend_two_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
         
          
            //统计所有退款的金额。
            $version = $order['is_old_num'];
            $goods_amount_data = model('order_snapshot_refund')->getRefundInfo(['refund_stage'=> 1,'snapshot_id' => $order['snapshot_id']],'SUM(refund_amount) as refund_amount');


            $two_stage_refund['total_amount'] = $goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;

        $two_stage_refund['goods_num'] = 	$goods_num;
        $two_stage_refund['goods_amount'] = 	$goods_amount;
        $two_stage_refund['goods_list'] = $order['extend_two_stage_refund'];
        $order_item['two_stage_refund'] = $two_stage_refund;
        
        
        //三阶段退款
        $three_stage_refund = array();
        $aftersale = $order['extend_three_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($aftersale)) {
            foreach ($aftersale as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }


           //统计所有退款的金额。
           $version = $order['is_old_num'];
           $goods_amount_data = model('order_snapshot_refund')->getRefundInfo(['refund_stage'=> 2,'snapshot_id' => $order['snapshot_id']],'SUM(refund_amount) as refund_amount');

           $three_stage_refund['total_amount'] = $goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;

        $three_stage_refund['goods_num'] = 	$goods_num;
        $three_stage_refund['goods_amount'] = 	$goods_amount;
        $three_stage_refund['goods_list'] = $order['extend_three_stage_refund'];
        $order_item['three_stage_refund'] = $three_stage_refund;
        
        
		
		
		
          
        //全单退款
         
        //替换商品
        $order_replace = array();
        $goods_replace =  $order['extend_order_goods_replace'];
        $goods_list = array();
        foreach ($goods_replace as $v) {
            $v['replace_type'] = 0;
            $goods_list[] = $v;
                                                
            foreach ($v['replace_goods'] as $child) {
                $child['replace_type'] = 1;
                $goods_list[] = $child;
            }
        }

        $order_replace['goods_num'] = 	$order['extend_order_goods_replace_num'];
        $order_replace['goods_amount'] = $order['extend_order_goods_replace_amount'];
        $order_replace['goods_list'] = $goods_list;
        $order_item['order_replace'] = $order_replace;
                                            
        //商家赠送商品
        $order_goods_give = array();
        $give = $order['extend_order_goods_give'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($give)) {
            foreach ($give as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
                                            
        $order_goods_give['goods_num'] = 	$goods_num;
        $order_goods_give['goods_amount'] = 	$goods_amount;
        $order_goods_give['goods_list'] = $order['extend_order_goods_give'];
        $order_item['order_give'] = $order_goods_give;
        //print_r($order);
                            
        //商家赠送的优惠券
        $order_coupon_give = array();
      //  $order_coupon = model('order_coupon')->getCouponList(array('order_id' => $order['order_id']));
        //print_r($order_coupon);
        $order_item['order_coupon_give'] = $this->getCoupon($order['order_id'],$order['is_old_num']);
                            
        //订单信息
        $order_info = array();
        $order_info['order_sn'] = $order['order_sn'];
        $order_info['add_time'] = date('H:i:s d/m/y', $order['add_time']);
        $order_info['store_note'] = $order['store_note']?$order['store_note']:'--';
        $order_info['payment_name'] = $order['payment_name'];
        $order_info['countdown'] =  120;
        $order_info['delivery_time'] = $order['delivery_time'];
        $order_info['delivery_type'] = $order['delivery_type'];
		$order_info['snapshot_amendment'] = $order['snapshot_amendment'];
		$order_info['snapshot_id'] = $order['snapshot_id'];
		$order_info['is_old'] = $order['is_old'];
		$order_info['delivery_capacity'] = $order['delivery_capacity'];
		$order_info['store_capacity'] = $order['store_capacity'];
		$order_info['order_note'] = $order['order_note'];
		$order_info['delivery_note'] = $order['delivery_note'];
		
        $order_info['system_lang'] = $order['system_lang'];
        $order_info['menu_lang'] = $order['menu_lang'];
    


        $order_item['order_info'] = $order_info;
                            
                            
                            
                                
        //支付方式
        $payment_info = array();
        $payment_info['wallet_fee'] = $order['pd_amount']; //余额支付
        $payment_info['external_payment'] = array(
                                'bank_name' => $order['bank_name'],
                                'bank_pay_fee' => $order['pay_amount'] - $order['refund_pay_amount']
                            );
        $order_item['payment_info'] = $payment_info;
                            
        //收入信息
        $order_income = array();
        $order_income['commission_amount'] = $order['commission_amount'];
        $order_income['order_amount'] = $order['order_amount'];
        $order_item['order_income'] = $order_income;
                            
                                
        if ($order['distributor_id'] > 0) {
            $delivery = array();
            $delivery['delivery_id'] = $order['distributor_id'];
            $delivery['delivery_name'] = $order['distributor_name'];
            $delivery['delivery_mobile'] = $order['distributor_mobile'];
            $delivery['delivery_start_time'] = $order['distributor_start_time'];
            $delivery['delivery_end_time'] = $order['distributor_end_time'];
            $delivery['order_estimate_time'] = $order['order_estimate_time'];
            $delivery['delivery_code'] = $order['distributor_code'];
            $delivery['delivery_duration'] = $order['distributor_duration'];
            $delivery['delivery_coordinate'] = $order['distributor_coordinate'];
            $delivery['delivery_comment'] = $order['deliver_comment'];
            $order_item['delivery'] = $delivery;
        }
                            
                            
                            
        //按钮信息
        $order_btn = array();
        $order_btn['state'] = $order['order_state'];
        $order_btn['order_id'] = $order['order_id'];
        $order_item['order_btn'] = $order_btn;
        $order_item['state'] = $order['order_state'];
        $order_item['refund_state'] =$order['refund_state'];
        $order_item['is_update'] =$order['is_update'];
        $order_item['is_problem'] =$order['is_problem'];
        $order_item['problem_type'] =$order['problem_type'];
        $order_item['is_delay'] =$order['is_delay'];
        $order_item['order_type'] =$order['order_type'];
			  
                                            
        $problem = array();
        $problem['problem_content'] = $order['problem_content'];
        $problem['problem_type_name'] = $order['problem_type_name'];
        $problem['problem_image'] = $this->order_problem_image($order['problem_image']);//empty($order['problem_image']) ? array() : unserialize($order['problem_image']);
        $problem['problem_type_id'] = $order['problem_type_id'];
        $problem['problem_type'] = $order['problem_type'];
        $problem['problem_state'] = $order['problem_state'];
        $problem['problem_title'] = $order['problem_title'];
        $order_item['problem'] = $problem;
        $evaluate = null;          
			
        if ($order['evaluation_state'] > 0) {
            $eva_model = model('evaluate_store');
       
            $eva_info = $eva_model->getEvaluateStoreInfo(array('store_evaluate_orderid'=> $order['order_id']));
			$evaluate = null;
		
            if (!empty($eva_info)) {
                $eva_info['store_evaluate_image'] = $this->order_evaluate_image($eva_info['store_evaluate_image']);
                $eva_info['store_evaluate_addtime'] = date('d/m/y H:i', $eva_info['store_evaluate_addtime']);
                $eva_info['store_evaluate_reply_time'] = date('d/m/y H:i', $eva_info['store_evaluate_reply_time']);
                                    
                $evaluate = $eva_info;
            }
		}
			 
            $order_item['store_evaluate'] = $evaluate;
                       
							 
		 $evaluate = null;
		 if ($order['evaluation_state'] > 0) {						
            //
            if ($order['order_type'] == 1) {
                $eva_model = model('evaluate_waiter');
                $evaluate = null;
                $eva_info = $eva_model->getEvaluateWaiterInfo(array('waiter_evaluate_orderid'=> $order['order_id']));
                if (!empty($eva_info)) {
                    $eva_info['waiter_evaluate_image'] = $this->order_evaluate_image($eva_info['waiter_evaluate_image']);
                    $eva_info['waiter_evaluate_addtime'] = date('d/m/y H:i', $eva_info['waiter_evaluate_addtime']);
                    $eva_info['waiter_evaluate_reply_time'] = date('d/m/y H:i', $eva_info['waiter_evaluate_reply_time']);
                    $evaluate = $eva_info;
                }
              
            }
        }
        $order_item['delivery_evaluate'] = $evaluate;         
        //下单流程时间
        $order_item['order_log'] = $this->order_log($order['extend_order_log']);
        $order_item['order_step'] = count($order['extend_order_log']);                                            
        //订单反馈
        $order_item['order_feedback'] = $this->getFeedback($order);
        
		//订单操作记录
		$order_item['order_record'] = $this->getRecord($order['order_id']);
		$order_item['order_problem'] = $this->getProblem($order['order_id']);

	
		//单独摘出来 问题单的 record_data
		
		$order_item['order_problem_record'] = $this->getProblemRecord($order['order_id']);
		 

        output_data($order_item);
    }
        
        
        
           
    //订单详情
    private function order_info()
    {
        $order_id = intval($_POST['order_id']);
        if (!$order_id) {
            output_error('订单编号有误');
        }
        $model_order = Model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        //  $condition['store_id'] = $this->store_info['store_id'];
        
        $order = $model_order->getOrderInfo($condition, array('order_common','order_goods','member','store','replace_pc','goods_give_pc','one_stage_refund_pc','two_stage_refund_pc','three_stage_refund_pc','order_log','order_print_goods'));
                
          
			
               
        if (empty($order)) {
            output_error('订单信息不存在');
        }
        
        $order_item = array();
        //从新格式化
        $order_member = array();
        $order_member['member_name'] 	 = $order['extend_member']['member_name'];
        $order_member['member_truename'] = $order['extend_member']['member_truename'];
        $order_member['member_avatar'] 	 = $this->getMemberAvatar($order['extend_member']['member_avatar']);
        $order_member['member_id'] 	 		= $order['extend_member']['member_id'];        
        $order_member['member_code'] 	 = $order['extend_member']['member_code'];
            
        $order_item['order_member'] = $order_member;
                
        $order_item['order_store'] = $order['extend_store'];
                
                
        $order_user = array();
        $order_user['buyer_code'] 	= $order['buyer_code'];
        $order_user['buyer_name'] 	= $order['buyer_name'];
        $order_user['buyer_phone'] 	= $order['buyer_phone'];
        $order_user['buyer_comment'] = $order['buyer_comment'];
        $order_user['buyer_address'] = $order['buyer_address'];
        $order_user['buyer_is_new'] = $order['buyer_is_new'];
        $order_user['buyer_region'] = $order['buyer_region'];
        $order_user['buyer_region_color'] = $order['buyer_region_color'];
            
		
		
		
		
		
		
	
		
		$consignee_info = model('consignee')->getConsigneeInfo(array('consignee_id' =>$order['consignee_attr_id'] ));
			
	
        $consignee_attr = Model('consignee_attr')->getConsigneeAttrInfo(array('attr_id'=>$consignee_info['attr_id']));
        $order_user['consignee_attr']  =  UPLOAD_SITE_URL.'/icon/'.$consignee_attr['attr_icon'];
                

                
                
        //根据账号调用账号信息
                
            
        //调用收获人信息
        
                
        $order_user['distributor_name'] = $order['distributor_name'];
        $order_user['distributor_mobile'] = $order['distributor_mobile'];
        $order_user['state'] = $order['order_state'];
        $order_user['evaluation_state'] = $order['evaluation_state'];
                
        if ($order['evaluation_state'] == 1) {
            $model = model('evaluate_store');
            $where = array(
                        'store_evaluate_orderid' => $order['order_id']
                    );
            $eva = $model->getEvaluateStoreInfo($where);
            $order['evaluation_text'] = $eva['store_evaluate_score_text'];
            $order['evaluation_content'] = $eva['store_evaluate_score_content'];
        }
                
                
                
                
                
        $order_item['order_user'] = $order_user;
        $order_item['member_address'] = unserialize($order['member_address']);            
     
		$address_info = model('address')->getDefaultAddressInfo(array('address_id' => $order['address_attr_id']));				
		$address_attr =  model('address_attr')->getAddressAttrInfo(array('address_icon' => $address_info['address_icon']));	
			
        $order_item['member_address']['address_attr']  =  UPLOAD_SITE_URL.'/icon/'.$address_attr['address_icon_url'].'black@2x.png';
                
            
                        
        //订单商品,原始商品，不做任何修改
        $order_goods = array();
        $goods =  $order['extend_order_goods'];
        $goods_num = 0;
        $goods_list = array();
        foreach ($goods as $k => $v) {
			$goods_list[] = $v;
            $goods_num += $v['goods_num'];
        }
                
        $order_goods['goods_lang']  = $goods_list[0]['goods_lang'];
        $order_goods['goods_list']  = $goods_list;
        $order_goods['goods_num']   = $goods_num;
        $order_item['order_goods']  = $order_goods;
                
        
        //金额信息
        $order_amount = array();
        //商品小计
        $order_amount['goods_amount'] = $order['goods_amount'];
        //商品折扣
        $order_amount['sales_amount'] = $order['sales_amount'];
        //餐盒费
        $order_amount['foodbox_amount'] = $order['foodbox_amount'];
        //配送费
        $order_amount['delivery_amount'] = $order['delivery_fee'];
        //支付手续费
                
        $order_amount['pay_commission_amount'] = $order['pay_commission_amount'];
                
        //平台抽成
        $order_amount['commission_amount'] = $order['commission_amount'];
        //订单价格
        $order_amount['order_amount'] = $order['order_amount'];
        //商家优惠券
        $order_amount['store_coupon_price'] = $order['store_coupon_price'];
        //平台优惠券
        $order_amount['platform_coupon_price']  = $order['platform_coupon_price'];
        //订单实付
        $order_amount['pay_order_amount'] = $order['pay_order_amount'];
        $order_amount['extxnfee_stripe'] = $order['extxnfee_stripe'];                


        $order_item['order_amount'] = $order_amount;
                
                
            
        
        //一阶段退款
        $one_stage_refund = array();
        $refund = $order['extend_one_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }


        //统计所有退款的金额。
        $version = $order['is_old_num'];
        $goods_amount_data = model('order_refund')->getRefundInfo(['refund_stage' => 0,'order_id' => $order['order_id'],'version' => ['elt',$version]],'SUM(refund_amount) as refund_amount');
  
  

        $one_stage_refund['total_amount'] = $goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;
        $one_stage_refund['goods_num'] 	= 	$goods_num;
        $one_stage_refund['goods_amount'] = $goods_amount;
        $one_stage_refund['goods_list'] = $order['extend_one_stage_refund'];
        $order_item['one_stage_refund'] = $one_stage_refund;
                
                
				
        //二阶段退款
        $two_stage_refund = array();
        $refund = $order['extend_two_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
         //统计所有退款的金额。
         //统计所有退款的金额。
         $version = $order['is_old_num'];
         $goods_amount_data = model('order_refund')->getRefundInfo(['refund_stage' => 1,'order_id' => $order['order_id'],'version' => ['elt',$version]],'SUM(refund_amount) as refund_amount');


         $two_stage_refund['total_amount'] =$goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;

        $two_stage_refund['goods_num'] = 	$goods_num;
        $two_stage_refund['goods_amount'] = 	$goods_amount;
        $two_stage_refund['goods_list'] = $order['extend_two_stage_refund'];
        $order_item['two_stage_refund'] = $two_stage_refund;
        
        
        //三阶段退款
        $three_stage_refund = array();
        $aftersale = $order['extend_three_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($aftersale)) {
            foreach ($aftersale as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }



        //统计所有退款的金额。
        $version = $order['is_old_num'];
        $goods_amount_data = model('order_refund')->getRefundInfo(['refund_stage' => 2,'order_id' => $order['order_id'],'is_delivery' =>  0, 'version' => ['elt',$version]],'SUM(refund_amount) as refund_amount');

     
        $three_stage_refund['total_amount'] = $goods_amount_data['refund_amount'] > 0 ? $goods_amount_data['refund_amount']  : 0 ;

        $three_stage_refund['goods_num'] = 	$goods_num;
        $three_stage_refund['goods_amount'] = 	$goods_amount;
        $three_stage_refund['goods_list'] = $order['extend_three_stage_refund'];
        $order_item['three_stage_refund'] = $three_stage_refund;
        
		



        //全单退款
		   
        //替换商品
        $order_replace = array();
        $goods_replace =  $order['extend_order_goods_replace'];
        $goods_list = array();
        foreach ($goods_replace as $v) {
            $v['replace_type'] = 0;
            $goods_list[] = $v;
                                    
            foreach ($v['replace_goods'] as $child) {
                $child['replace_type'] = 1;
                $goods_list[] = $child;
            }
        }
                
                

                
        $order_replace['goods_num'] = 	$order['extend_order_goods_replace_num'];
        $order_replace['goods_amount'] = $order['extend_order_goods_replace_amount'];
        $order_replace['goods_list'] = $goods_list;
        $order_item['order_replace'] = $order_replace;
                                
        //商家赠送商品
        $order_goods_give = array();
        $give = $order['extend_order_goods_give'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($give)) {
            foreach ($give as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
                                
        $order_goods_give['goods_num'] = 	$goods_num;
        $order_goods_give['goods_amount'] = 	$goods_amount;
        $order_goods_give['goods_list'] = $order['extend_order_goods_give'];
        $order_item['order_give'] = $order_goods_give;
        //print_r($order);
                
        //商家赠送的优惠券
        $order_coupon_give = array();
        
		
        //print_r($order_coupon);
        $order_item['order_coupon_give'] =  $this->getCoupon($order['order_id'],$order['is_old_num']);
                
        //订单信息
        $order_info = array();
        $order_info['order_sn'] = $order['order_sn'];
        $order_info['add_time'] = date('H:i:s d/m/y', $order['add_time']);
        $order_info['store_note'] = $order['store_note']?$order['store_note']:'--';
        $order_info['payment_name'] = $order['payment_name'];
        $order_info['countdown'] =  120;
        $order_info['delivery_time'] = $order['delivery_time'];
        $order_info['delivery_type'] = $order['delivery_type'];
		$order_info['snapshot_amendment'] = $order['snapshot_amendment'];
		$order_info['is_old'] = $order['is_old'];
		//$order_info['buyer_comment'] = $order['buyer_comment'];
		$order_info['delivery_note'] = $order['delivery_note'];
		$order_info['order_note'] = $order['order_note'];
		$order_info['store_capacity'] = $order['store_capacity'];
		$order_info['delivery_capacity'] = $order['delivery_capacity'];
		$order_info['system_lang'] = $order['system_lang'];
        $order_info['menu_lang'] = $order['menu_lang'];

        $order_item['order_info'] = $order_info;
                
              
        //支付方式
        $payment_info = array();
        $payment_info['wallet_fee'] = $order['pd_amount']; //余额支付
		$payment_info['trade_no'] = $order['trade_no']; //余额支付
        $payment_info['external_payment'] = array(
                    'bank_name' => $order['bank_name'],
                    'bank_pay_fee' => $order['pay_amount'] - $order['refund_pay_amount']
                );
        $order_item['payment_info'] = $payment_info;
                
        //收入信息
        $order_income = array();
        $order_income['commission_amount'] = $order['commission_amount'];
        $order_income['order_amount'] = $order['order_amount'];
        $order_item['order_income'] = $order_income;
                
        $delivery = null;   
        if ($order['distributor_id'] > 0) {     
			
			$where = array(
			    'distributor_id' =>intval($order['distributor_id'])
			);		
			$deliveryInfo = model('waiter')->getWaiterInfo($where);
			
			$delivery['delivery_avatar'] = getWaiterAvatar($deliveryInfo['distributor_avatar']);
									
            $delivery['delivery_id'] 		= $order['distributor_id'];
            $delivery['delivery_name'] 		= $order['distributor_name'];
            $delivery['delivery_mobile'] 	= $order['distributor_mobile'];
            $delivery['delivery_start_time'] = $order['distributor_start_time'];
            $delivery['delivery_end_time'] = $order['distributor_end_time'];
            $delivery['order_estimate_time'] = $order['order_estimate_time'];
            $delivery['delivery_code'] 		= $order['distributor_code'];
            $delivery['delivery_duration'] = $order['distributor_duration'];
            $delivery['delivery_coordinate'] = $order['distributor_coordinate'];
            $delivery['delivery_comment'] = $order['deliver_comment'];
      
        }
        $order_item['delivery'] = $delivery;    
                
            
        //按钮信息
        $order_btn = array();
        $order_btn['state'] = $order['order_state'];
        $order_btn['order_id'] = $order['order_id'];
        $order_item['order_btn'] = $order_btn;
        $order_item['state'] = $order['order_state'];
        $order_item['refund_state'] =$order['refund_state'];
        $order_item['is_update'] =$order['is_update'];
        $order_item['is_problem'] =$order['is_problem'];
        $order_item['problem_type'] =$order['problem_type'];
        $order_item['is_delay'] =$order['is_delay'];
        $order_item['order_type'] =$order['order_type'];
                               
        $problem = array();
        $problem['problem_content'] = $order['problem_content'];
        $problem['problem_type_name'] = $order['problem_type_name'];
        $problem['problem_image'] = $this->order_problem_image($order['problem_image']);//empty($order['problem_image']) ? array() : unserialize($order['problem_image']);
        $problem['problem_type_id'] = $order['problem_type_id'];
        $problem['problem_type'] = $order['problem_type'];
        $problem['problem_state'] = $order['problem_state'];
        $problem['problem_title'] = $order['problem_title'];
        $order_item['problem'] = $problem;
       
  
	    $evaluate = null;   
        if ($order['evaluation_state'] > 0) {
            $eva_model = model('evaluate_store');
           
            $eva_info = $eva_model->getEvaluateStoreInfo(array('store_evaluate_orderid'=> $order['order_id']));
			
			
			
            if (!empty($eva_info)) {
                $eva_info['store_evaluate_image'] = $this->order_evaluate_image($eva_info['store_evaluate_image']);
                $eva_info['store_evaluate_addtime'] = date('d/m/y H:i', $eva_info['store_evaluate_addtime']);
                $eva_info['store_evaluate_reply_time'] = date('d/m/y H:i', $eva_info['store_evaluate_reply_time']);
                $eva_info['store_evaluate_log'] = $this->getStoreEvaLog($eva_info['store_evaluate_id']);
                $evaluate = $eva_info;
            }
			
		}	
		  
		$order_item['store_evaluate'] = $evaluate;
                  
		$evaluate = null;
		if ($order['order_type'] == 1 && $order['evaluation_state'] > 0) {
                $eva_model = model('evaluate_waiter');             
                $eva_info = $eva_model->getEvaluateWaiterInfo(array('waiter_evaluate_orderid'=> $order['order_id']));
                if (!empty($eva_info)) {
                    $eva_info['waiter_evaluate_image'] = $this->order_evaluate_image($eva_info['waiter_evaluate_image']);
                    $eva_info['waiter_evaluate_addtime'] = date('d/m/y H:i', $eva_info['waiter_evaluate_addtime']);
                    $eva_info['waiter_evaluate_reply_time'] = date('d/m/y H:i', $eva_info['waiter_evaluate_reply_time']);
                    $evaluate = $eva_info;
                }
        }
        $order_item['delivery_evaluate'] = $evaluate;
                
        //下单流程时间
        $order_item['order_log'] = $this->order_log($order['extend_order_log']);
        $order_item['order_step'] = count($order['extend_order_log']) - 1;                                
		
		
        //订单反馈
        $order_item['order_feedback'] = $this->getFeedback($order);
		
        //订单操作记录
        $order_item['order_record']  = $this->getRecord($order['order_id']);        
		
		$order_item['order_problem'] = $this->getProblem($order['order_id']);
         
			
		
		$order_item['order_problem_record'] = $this->getProblemRecord($order['order_id']);
		 			
					
		//票据相关
		$order_invoice = array();
		
		$order_invoice['sales_invoice'] = '';
		$order_invoice['delivery_invoice'] = '';
		$order_invoice['sales_credit_note'] = '';
		$order_invoice['delivery_credit_note'] = '';
					
		$order_item['order_invoice'] = $order_invoice;
					
		

        $to_type = array('平台','商家','配送员','顾客','专员','汽修厂');

		$bill = model('bill');
		//获取平台发出的票据
		$where = array();
		$where['form_type'] = 0;
		$where['order_id'] = $order['order_id'];
		$me_bill = $bill->getBillList($where);	
		$me_bill_list = array();
		foreach($me_bill as $k => $v){
			$v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];			
            $v['to_type_name'] = $to_type[$v['to_type']];
			$me_bill_list[$k] = $v;
		}
		$order_item['me_bill'] = $me_bill_list;	
		
		//获取商家发出的票据
		$where = array();
		$where['form_type'] = 1;
		$where['order_id'] = $order['order_id'];
		$store_bill = $bill->getBillList($where);	
		$store_bill_list = array();
		foreach($store_bill as $k => $v){
			$v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];		
            $v['to_type_name'] = $to_type[$v['to_type']];	
			$store_bill_list[$k] = $v;
		}			
		$order_item['store_bill'] = $store_bill_list;		
		
		
		//获取骑手发出的票据。		
		$where = array();
		$where['form_type'] = 2;
		$where['order_id'] = $order['order_id'];
		$delivery_bill = $bill->getBillList($where);
		$delivery_bill_list = array();
		foreach($delivery_bill as $k => $v){
			$v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];	
            $v['to_type_name'] = $to_type[$v['to_type']];		
			$delivery_bill_list[$k] = $v;
		}			
		$order_item['delivery_bill'] = $delivery_bill_list;			
				
				
					
            
        output_data($order_item);
    }
	
	//问题单拆分的小块数据。
	private function getProblemRecord($order_id){
		
		$where = array();
		$where['order_id'] = $order_id;
		$where['record_type'] =  array('in','action_required');
		$where['record_state'] = 0;
		$result = model('order_record')->getRecordList($where);
		$list = array();
		$data = null;
		if($result){
			foreach($result as $k => $v){
				$record_time['date'] 	= date('d M y',$v['record_time']);
				$record_time['time'] 	= date('H:i:s',$v['record_time']);							
				$list[$k]['record_id'] 	= $v['record_id'];
				$list[$k]['order_id'] 	= $v['order_id'];
				$list[$k]['record_msg'] 	= $v['record_msg'];
				$list[$k]['record_time'] 	= $record_time;
				$list[$k]['record_type'] 	= $v['record_type'];							
				$list[$k]['record_datas'] = $this->record_datas($v['record_type'],$v['record_datas'],$v['order_id']);
			}
			$data = $list;
		}		
		return $data;
	}
	
	
	

	//格式化赠送的券数据
	private function getCoupon($order_id,$version){		
		$order_coupon = model('order_coupon')->getCouponList(array('order_id' => $order_id,'version' => $version));		
		$coupon_list = array();
		foreach($order_coupon as  $k => $v){
			$v['add_time'] = date('d M y H:i:s',$v['add_time']);
			$coupon_list[$k] = $v;
		}
		return $coupon_list;
	}

	//骑手反馈订单
	private function getProblem($order_id){
		
		$where = array();
		$where['order_id'] =  $order_id;
		$result = model('order_problem')->getProblemInfo($where);		
		
		$data = null;
		if($result){
			$result['problem_image'] = explode(',',$result['problem_image']);
			$result['child'] = $this->getProblemLog($result['problem_id']);			
			$result['problem_time'] = date('d M y H:i:s',$result['problem_time']);			
		
			$data = $result;
			
		}		
		
		return $data;
	}
	
	
	
	//获取反馈的回复
	private function getProblemLog($problem_id){		
		$where = array();
		$where['problem_id'] = $problem_id;			
		$result = model('order_problem')->getProblemLogList($where);		
	
		$list = array();
		foreach($result as $k => $v){	
			$list[$k] = $v;	
			$list[$k]['log_file'] =  !empty($v['log_file']) ? explode(',',$v['log_file']) : array();
			$list[$k]['log_addtime'] = date('d M y H:i:s',$v['log_addtime']);		
			$list[$k]['admin_avatar'] = UPLOAD_SITE_URL.'/admin/admin-avatar.png';
		}				
		return $list;
	}
	
	
	
	
	//获取
	private function getRecord($order_id){
		
		$where = array();
		$where['order_id'] = $order_id;
		$result = model('order_record')->getRecordList($where);
		$list = array();
		$data = [];
		if($result){
			foreach($result as $k => $v){
				$record_time['date'] = date('d M y',$v['record_time']);
				$record_time['time'] = date('H:i:s',$v['record_time']);							
				$list[$k]['record_id'] 	= $v['record_id'];
				$list[$k]['order_id'] 	= $v['order_id'];
				$list[$k]['record_msg'] 	= $v['record_msg'];
				$list[$k]['record_time'] 	= $record_time;
				$list[$k]['record_type'] 	= $v['record_type'];							
                $list[$k]['record_popup'] 	= $v['record_popup'];							                
				$list[$k]['record_datas'] = $this->record_datas($v['record_type'],$v['record_datas'],$v['order_id']);
			}
			$data = $list;
		}
		
		return $data;
		
	}
	
	
	
	//拆分出来的 socket断线
	
	public function record_socket_listOp(){
		
		$type = $_POST['type'];		
		$where = array();
		$where['order_id'] = $_POST['order_id'];		
		if($type == 'crl'){
			$where['record_role']  = 'csl';
		}
		if($type == 'mrl'){
			$where['record_role']  = 'msl';			
		}		
		$result = model('order_record')->getRecordList($where);
		$list = array();
		$data = [];
		if($result){
			foreach($result as $k => $v){
				$record_time['date'] = date('d M y',$v['record_time']);
				$record_time['time'] = date('H:i:s',$v['record_time']);							
				$list[$k]['record_id'] 	= $v['record_id'];
				$list[$k]['order_id'] 	= $v['order_id'];
				$list[$k]['record_msg'] 	= $v['record_msg'];
				$list[$k]['record_time'] 	= $record_time;
				$list[$k]['record_type'] 	= $v['record_type'];	
                $list[$k]['record_popup'] 	= $v['record_popup'];											
				$list[$k]['record_datas'] = $this->record_datas($v['record_type'],$v['record_datas'],$v['order_id']);
			}			
		}		
		output_data(array('record_list' => $list));
		
	}
	
	
	private function record_datas($type,$data,$order_id = 0){		
		$data = unserialize($data);		
		switch($type){		   
					case 'order_placed': //订单下单	[代码已加] member_buy   member_payment
										
						$record_data['order_addtime'] = date('d/m/Y H:i:s',$data['order_addtime']); //订单下单时间
						
						
					break; 			
					case 'order_cancel': //订单已取消 [代码已加] member_order
						
						$record_data['order_cancel_time'] =  date('d/m/Y H:i:s',$data['order_cancel_time']); //$data['order_cancel_time']; //订单去取消时间
						$record_data['order_cancel_content'] = $data['order_cancel_content']; //订单取消备注
						$record_data['role'] = $data['role']; //操作方 Platform 平台 Merchant 商家 Customer 顾客					
												
					break; 								
					case 'order_confirmed_courier': // (courier) 配送员已接单  【直推的订单在member_buy】
					
						$record_data['delivery_code'] = $data['delivery_code']; //配送员编号
                        if($data['order_code'] == 1){
                            $order_num = 'First';
                        }
                        if($data['order_code'] == 2){
                            $order_num = 'Second';
                        }
                        if($data['order_code'] == 3){
                            $order_num = 'Third';
                        }
						$record_data['order_code'] = $order_num;//当前批次第几单

                        $order_list =  model('order')->getOrderListNew(['batch_code' => $data['batch_code']]);

                        foreach($order_list as $k => $v){
                            if($v['daycode'] == 1){
                                $daycode = 'First';
                            }
                            if($v['daycode'] == 2){
                                $daycode = 'Second';
                            }
                            if($v['daycode'] == 3){
                                $daycode = 'Third';
                            }
                            $order_list[$k]['daycode'] = $daycode;
                        }

                        $record_data['order_list'] = $order_list;
                        
						$record_data['send_delivery_time'] = date('d/m/Y H:i:s',$data['send_delivery_time']);// $data['send_delivery_time']; //推送给配送员的时间				
					
					break;
					
					case 'order_display': // (merchant) 商家展示订单 【代码已加】member_buy				
						$record_data['send_store_time'] = date('d/m/Y H:i:s',$data['send_store_time']); // $data['send_store_time']; //推送给商家的时间				
					
					break;
					
		  		    case 'order_confirmed_merchant': // (merchant)商家已接单 【代码已加】 seller_order line 402
					
						$record_data['store_taking_time'] = date('d/m/Y H:i:s',$data['store_taking_time']);// $data['store_taking_time']; //商家接单时间
						$record_data['store_taking_type'] = $data['store_taking_type']; //商家接单类型，自动 Auto，手动 Manual
						
				
					break;
									
		  		    case 'ca_merchant': //配送员到达商家附近  [代码已加] 
						
						$record_data['delivery_store_time'] = date('d/m/Y H:i:s',$data['delivery_store_time']);// $data['delivery_store_time']; //配送员抵达商家附近
					
					break;
										
					case 'order_collected': //已取餐 [代码已加] order_allocation line 294				
					
						if($data['pick_type'] == 'courier'){						
							$record_data['delivery_code'] = $data['delivery_code']; //如果在配送员哪里取餐，这个就必须传了。						
						}				
						$record_data['pick_type'] = $data['pick_type']; //取餐类型 在商家取餐merchant，在配送员取餐delivery
						$record_data['pick_time'] = date('d/m/Y H:i:s',$data['pick_time']);// $data['pick_time']; //取餐时间
						
						
						
					break;
					
		  		    case 'picked_by_customer': //顾客已取餐、自提单 [代码已加] member_order  line 295 
						
						$record_data['pick_type'] = $data['pick_type'];	//谁操作的自提，用户 Customer，商家 Merchant								
						$record_data['pick_time'] = date('d/m/Y H:i:s',$data['pick_time']);// $data['pick_time']; //取餐时间
					
						
					break;
					
		  		    case 'start_delivery': //配送员开始配送 [代码已加] order_allocation line 337
						 
						$record_data['order_num'] = $data['order_num']; //本次批次的订单数
						$record_data['batch_code'] = $data['batch_code']; //批次编号  配送员编号 +140821 + 序号 001

                        $order_list =  model('order')->getOrderListNew(['batch_code' => $data['batch_code']]);

                        foreach($order_list as $k => $v){
                            if($v['daycode'] == 1){
                                $daycode = 'First';
                            }
                            if($v['daycode'] == 2){
                                $daycode = 'Second';
                            }
                            if($v['daycode'] == 3){
                                $daycode = 'Third';
                            }
                            $order_list[$k]['daycode'] = $daycode;
                        }

                        $record_data['order_list'] = $order_list;


						$record_data['start_delivery_time'] =  date('d/m/Y H:i:s',$data['start_delivery_time']);// $data['start_delivery_time']; //开始配送时间，提取最后一餐的时间。
						
					
					break;
					
		  		    case 'ca_customer': //配送员到达顾客附近
						
						$record_data['delivery_customer_time'] = date('d/m/Y H:i:s',$data['delivery_customer_time']);// $data['delivery_customer_time']; //配送员抵达用户附近时间							
						
										
					
					break;
					
		  		    case 'order_delivered': //订单已送达		[代码已加]	order_alloction line 375
						
						$record_data['delivery_time'] = date('d/m/Y H:i:s',$data['delivery_time']); // $data['delivery_time']; //订单送达时间
					
					
						
					break;
					
		  		    case 'refund_request': //顾客退款申请 【功能已剔除】
					
					
						$record_data['refund_order_content'] = $data['refund_content']; //退款申请的理由
						$record_data['refund_order_time'] = date('d/m/Y H:i:s',$data['refund_order_time']);// $data['refund_order_time'];
					
						
					
					
		  		    case 'refund_request_granted': //顾客退款审核通过 【功能已剔除】
					
						
						$record_data['refund_order_content'] = $data['refund_content']; //退款申请的理由
						$record_data['refund_order_time'] = date('d/m/Y H:i:s',$data['refund_order_time']); // $data['refund_order_time'];
					
					
					break;
					
		  		    case 'refund_request_refused': //顾客退款审核驳回 【功能已剔除】
					
						
						$record_data['refund_refused_time'] = date('d/m/Y H:i:s',$data['refund_refused_time']); // $data['refund_refused_time']; //驳回时间
						$record_data['refund_refused_type'] = $data['refund_refused_type']; //驳回类型  Manual Auto
						
										
					break;				
		  		  
					case 'order_pending': //商家点击且慢后
						
						$record_data['order_pending_ids'] = $data['order_pending_ids']; //点击且慢的订单
						$record_data['order_pending_time'] = date('d/m/Y H:i:s',$data['order_pending_time']);// $data['order_pending_time']; //点击且慢的时间
						
					
						
						
					break;
		  		   
					case 'order_resume': //且慢状态解除后
						
						$record_data['order_resume_time'] = date('d/m/Y H:i:s',$data['order_resume_time']); //$data['order_resume_time']; //点击且慢的时间
					
						
					break;		
		  		   
					case 'merchant_refund': //商家操作退款 [代码已加] seller_refund line 200
						
						$record_data['merchant_refund_type'] = $data['merchant_refund_type']; //部分退款 全单退款
						$record_data['merchant_refund_time'] = date('d/m/Y H:i:s',$data['merchant_refund_time']);// $data['merchant_refund_time']; //推荐时间
					
										
					break;
		  		    
					case 'me_refund': //平台操作退款 [代码已加]
						
						$record_data['me_refund_type'] = $data['me_refund_type']; //部分退款 全单退款
						$record_data['me_refund_time'] = date('d/m/Y H:i:s',$data['me_refund_time']);// $data['me_refund_time']; //推荐时间
                        $record_data['me_refund_info'] = unserialize($data['me_refund_info']); //部分退款 全单退款
								
					break;
		  		    
					case 'socket_disconnect': //Socket断开
						/*
						断开类型
						Courier (A) 配送员断开（自动的）
						Courier (M) 配送员断开（手动如触发下线的）
						Merchant (A) 商户断开（自动）
						Merchant (M)商户断开（手动触发下线的）											
						*/					
						$record_data['socket_disconnect_type'] = $data['socket_disconnect_type']; // 断开类型
						$record_data['socket_disconnect_time'] = date('d/m/Y H:i:s',$data['socket_disconnect_time']);// $data['socket_disconnect_time']; // 断开时间
						
						
						
					break;
						
		  		    case 'attention': //订单可疑上报 【代码已加】 waiter_order 
						/*
						
						CA.Merchant 到店时间异常 2分钟
						Delivery Time 配送时间异常 2分钟
						Courier Socket 	配送员Socket链接异常 1分钟
						Merchant Socket 打单机Socket链接异常（断开超过10秒） 
						
						*/
						$record_data['attention_type'] = $data['attention_type'];
						$record_data['attention_time'] = date('d/m/Y H:i:s',$data['attention_time']);// $data['attention_time'];
								
					
					case 'action_required': //订单变为问题单  【代码已加】
						
						/*
						
						"CA.Merchant "到店时间超时 5分钟
						Delivery T.O 配送时间超时 5分钟
						Courier Socket T.O 	配送员Socket链接断开超时 5分钟
						Merchant Socket T.O 打单机Socket链接断开超时 5分钟
						Courier M.Offline 配送员手动下线
						Merchant M.Offline 商家手动下线
						Courier Report 	配送员问题上报
						Customer Report 顾客订单投诉/寻求帮助
						Merchant PTO"	商家超时未接单"						
		
						*/
						
						$record_data['action_required_type'] = $data['action_required_type']; //类型
						$record_data['action_required_time'] = date('d/m/Y H:i:s',$data['action_required_time']);// $data['action_required_time']; //时间
						$record_data['action_required_id'] = $data['action_required_id']; //如果有上报问题则关联
                                 

                        $where = array();
                        $where['order_id'] = $order_id;
                        $where['record_type'] =  array('in','action_required');                        
                        $list = model('order_record')->getRecordList($where);
                        foreach($list as $v){
                            $datas = unserialize($v['record_datas']);		 

                            $record_datas['action_required_type'] = $datas['action_required_type']; //类型
					    	$record_datas['action_required_time'] = date('d/m/Y H:i:s',$datas['action_required_time']);// $data['action_required_time']; //时间
					    	$record_datas['action_required_id'] = $datas['action_required_id']; //如果有上报问题则关联
                            
                            $v['record_data']   = $record_datas;


                            $list_data[] = $v;

                        }    

                        $record_data['action_required_list'] = $list_data;



										
		  		    case 'resolved': //问题单状态解除 21 [代码已加]
					
							/* "AR. CA.Merchant "到店时间超时   
								AR. Delivery Time 配送时间超时
								AR. Courier Socket T.O 配送员Socket链接断开超时
								AR. Merchant Socket T.O 打单机Socket链接断开超时
								AR. Courier M.Offline 配送员手动下线
								AR. Merchant M.Offline 商家手动下线
								AR. Courier Report 配送员问题上报
								AR. Customer Report" 顾客订单投诉/寻求帮助"
							*/
							$record_data['resolved_type'] = $data['resolved_type']; //类型
							$record_data['resolved_time'] = date('d/m/Y H:i:s',$data['resolved_time']); // $data['resolved_time']; //时间
						
														
					
		  		    case 'change_courier': //重新分配配送员
						
						$record_data['delivery_code'] = $data['delivery_code'];					
					
							
					
					break;
		  		    case 'voucher_issued': //优惠券派发
						
						/*
						优惠券派发类型
						Merchant $x
						ME $x
						*/					
						$record_data['voucher_type'] = $data['voucher_type'];					
						$record_data['voucher_code'] = $data['voucher_code'];
						$record_data['voucher_price'] = $data['voucher_price'];
						$record_data['voucher_time'] = $data['voucher_time'];
					
					
						
					break;
		
		}
		
		
		return $record_data;
		
		
		
		
	}
	
	

	//获取
	private function getStoreEvaLog($eva_id){
		
		$where = array();
		$where['eva_id'] = $eva_id;
		$list = model('evaluate_store')->getLogList($where);		
		foreach($list as $v){
			$v['log_addtime'] = date('d/m/y H:i',$v['log_addtime']);
			$eva_log[] = $v;
		}
		return $eva_log;		
	}

        
    //格式化订单LOG
    private function order_log($log)
    {
        $new_log = array();
        foreach ($log as $k => $v) {			
		//	$v['log_difference'] = $log_time;
            $new_log[$v['log_orderstate']] = $v;
        }
                        
        $state= array(
            20 => array(
                'log_msg' => '支付订单',
				'log_color' => ''
            ),
			30 => array(
                'log_msg' =>'配送员已接单',
				'log_color' => ''
            ),
			40=> array(
                'log_msg' =>'商家接单',
				'log_color' => ''
            ),
			41=> array(
			    'log_msg' =>'配送员到达商家附近',
				'log_color' => ''
			),
			50=> array(
                'log_msg' =>'已取餐',
				'log_color' => ''
            ),
			51=> array(
			    'log_msg' =>'配送开始',
				'log_color' => ''
			),
			52=> array(
			    'log_msg' =>'配送员到达顾客位置',
				'log_color' => ''
			),
			60=> array(
                'log_msg' =>'订单结束',
				'log_color' => ''
            ));
            
            
        foreach ($state as $k => $v) {
            $state_log[$k] = $v;
            if (!empty($new_log[$k])) {               
				
				$state_log[$k] = $new_log[$k];
				
                $floorTime = $this->floorTime($k,$new_log);
			
				$state_log[$k]['log_difference'] = $floorTime; // $k > 20 ? (rand(9,1).'′'.rand(60,2).'″') : 'N/A';
				
				
            }
        }
        $log = array();
		foreach($state_log as $v){
			$log[] = $v;
		}
        return $log;
    }




	private function floorTime($order_state,$log){
        

        switch($order_state){

            case 20:
                            
                return 'N/A';

            break;

            case 30:
                
                $startdate = $log[20]['log_times'];
                $enddate = $log[30]['log_times'];

            break;

            case 40:
            
                $startdate = $log[30]['log_times'];
                $enddate = $log[40]['log_times'];

            break;

            case 41:
            
                $startdate = $log[40]['log_times'];
                $enddate = $log[41]['log_times'];



            break;

            case 50:
            
                $startdate = $log[41]['log_times'];
                $enddate = $log[50]['log_times'];

            
            break;

            case 51:
            
                $startdate = $log[50]['log_times'];
                $enddate = $log[51]['log_times'];



            break;
            

            case 52:
            
                $startdate = $log[51]['log_times'];
                $enddate = $log[52]['log_times'];


            break;

            case 60:
            
                $startdate = $log[52]['log_times'];
                $enddate = $log[60]['log_times'];

            break;


        }
   
       /* print_r($enddate);
        print_r('|');
        print_r($startdate);
        print_r('%');
        */
		return $this->timediff($startdate,$enddate ) ;
		
	}

    private function timediff($begin_time,$end_time)
    {
        if($begin_time < $end_time){
            $starttime = $begin_time;
            $endtime = $end_time;
        }else{
            $starttime = $end_time;
            $endtime = $begin_time;
        }
        //计算天数
        $timediff = $endtime-$starttime;
        $days = intval($timediff/86400);
        //计算小时数
        $remain = $timediff%86400;
        $hours = intval($remain/3600);
        //计算分钟数
        $remain = $remain%3600;
        $mins = intval($remain/60);
        //计算秒数
        $secs = $remain%60;
        
        if($hours > 0){
            $mins += $hours * 60;
        }

        $res = $mins.'′'.$secs.'″'  ;// array("day" => $days,"hour" => $hours,"min" => $mins,"sec" => $secs);
        return $res;
    }



	//回复骑手反馈的问题
	public function save_problemOp(){		
		$data = array();
		$data['log_content'] = $_POST['log_content'];
		$data['problem_id'] = $_POST['problem_id'];
		$data['log_file'] = $_POST['log_file'];
		$data['log_addtime'] = time();
		$data['role_id'] = $this->admin_info['admin_id'];
		$data['role_name'] = $this->admin_info['nickname'];	
		$data['role_type'] = 0;
		$row = model('order_problem')->addProblemLog($data);
		if($row){			
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}		
	}



    //保存CEO的回复
    public function save_ceo_evaOp()
    {
        $data = array(
                'eva_id'	 	=> $_POST['eva_id'],
                'log_content' 	=> $_POST['ceo_reply'],
                'log_file' 		=> $_POST['ceo_file'],
                'log_addtime'	=> time(),
                'reply_user_id' => $this->admin_info['admin_id'],
                'reply_user_name' => $this->admin_info['admin_name']
            );
        $row = model('evaluate_store')->addLog($data);
        if ($row) {
            output_data('保存成功');
        } else {
            output_error('保存失败');
        }
    }
        
		
	//保存回复
	public function save_feedback_replyOp(){
		
		$data = array(
			'feedback_id' => $_POST['feedback_id'],
			'log_content' => $_POST['feedback_reply'],
			'log_file' 		=> $_POST['feedback_file'],
			'log_addtime' => time(),
			'admin_id' => $this->admin_info['admin_id'],
			'admin_name' => $this->admin_info['admin_name']	
		);
		$row = model('feedback')->addFeedBackLog($data);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
		
		
	}

    //获取订单反馈
    private function getFeedback($order)
    {
        $where = array();
        $where['feedback_order_id'] = $order['order_id'];
        $where['feedback_state'] = array('in','0,1');
        $feedback_data = model('feedback')->getFeedBackInfo($where);
        
        
		

		$feedback_info = null;
		if($feedback_data){
			$list = array();			
			$feedback_data['feedback_file'] = $this->feedback_image($feedback_info['feedback_file']);
			$feedback_data['user_avatar'] 	= $this->getMemberAvatar($order['extend_member']['member_avatar']);
			$feedback_data['feedback_time'] = date('d/m/y H:i', $feedback_info['feedback_time']);
			$feedback_data['feedback_log'] 	= $this->getFeedbackLog($feedback_info['id'],$order);			
			$feedback_info = $feedback_data;
		}		
		return $feedback_info;
    }
        
    private function getFeedbackLog($feedback_id,$order)
    {
        $log = model('feedback')->getFeedBackListLog(array('feedback_id' => $feedback_id));
        foreach ($log as $v) {
            $v['log_file'] 		=  	$this->feedback_image($v['log_file']);
            $v['log_addtime'] 	=  	date('d/m/y H:i', $v['log_addtime']);
            $v['user_avatar'] 	= 	$this->getMemberAvatar($order['extend_member']['member_avatar']);
            $log_list[] 		= 	$v;
        }
        return $log_list;
    }
        
    //格式化图片
    private function feedback_image($image)
    {
        $list = array();
        if (!empty($image)) {
            $image  =	explode(",", $image);
            foreach ($image as $v) {
                $list[] = UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.$v;
            }
            return $list;
        } else {
            return $list;
        }
    }
        
    private function getMemberAvatar($image)
    {
        if (file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !='') {
            return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
        } else {
            return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
        }
    }
        
        
    //格式化图片
    private function order_problem_image($image)
    {
        $list = array();
        if (!empty($image)) {
            $image  =	explode(",", $image);
            foreach ($image as $v) {
                $list[] = $v;
            }
            return $list;
        } else {
            return $list;
        }
    }
        
        
    //格式化图片
    private function order_evaluate_image($image)
    {
        $list = array();
            
        if (!empty($image)) {
            $image  =	explode(",", $image);
            //print_r($image);
            foreach ($image as $v) {
                $list[] = UPLOAD_SITE_URL.DS.ATTACH_EVALUATE.DS.$v;
            }
            return $list;
        } else {
            return $list;
        }
    }
        
    private function order_replace_goods($rec_id)
    {
        $list =    model('order')->getOrderGoodsList(array('replace_rec_id'=>$rec_id));
        $data = array();
        foreach ($list as $v) {
            $v['goods_optional'] =  $v['goods_optional'] !='' ? unserialize($v['goods_optional']) : array();
            $data[] = $v;
        }
            
        return $data;
    }
	
	
	//获取订单退款信息
	public function order_refundOp(){
	
		
		$data = array();
		$condition = array();
		$condition['order_id'] = $_POST['order_id'];
				
		$order = model('order')->getOrderInfoN($condition);
		
		$role = array(
			array(
				'name' => '平台',
				'role' => 'platform',
				'color' => 'bg-platform'
			),
			array(
				'name' => '商家',
				'role' => 'store',
				'color' => 'bg-store'
			),
			array(
				'name' => '骑手',
				'role' => 'delivery',
				'color' => 'bg-delivery'
			)
		);
		
		$data['role'] = $role;
		
		
  


		//金额信息
		$order_amount = array();

        $goods_amount = $order['order_amount'] - $order['refund_pay_commission_amount'] - $order['delivery_fee'] - $order['refund_pd_amount'] - $order['refund_pay_amount'];
        $old_goods_amount = $order['old_order_amount']  -  $order['delivery_fee'];

        //商品小计
		$order_amount[0]['name'] = '原订单价值 / 剩余可退（Excl.配送费）';
        $order_amount[0]['old_amount'] = ncPriceFormat($old_goods_amount);
		$order_amount[0]['amount'] = ncPriceFormat($goods_amount);
		$order_amount[0]['value'] = 'goods_amount';
		$order_amount[0]['role'] = '';
		$order_amount[0]['color'] = '';
		$order_amount[0]['role_name'] = '';
		


        
		//平台抽成

        $commission_amount = $order['commission_amount'] + $order['pay_commission_amount'] + $order['foodbox_amount'];

		$order_amount[1]['name'] = '平台抽成退款';
		$order_amount[1]['amount'] = ncPriceFormat($commission_amount - $order['refund_commission_amount'] - $order['refund_pay_commission_amount']);
		$order_amount[1]['value'] = 'commission_amount';
		$order_amount[1]['role'] = '';
		$order_amount[1]['color'] = '';
		$order_amount[1]['role_name'] = '';

        

		//配送费退款
		$order_amount[2]['name'] = '配送费退款';
		$order_amount[2]['amount'] = ncPriceFormat($order['delivery_fee'] - $order['refund_delivery_fee']);
		$order_amount[2]['value'] = 'delivery_amount';
		$order_amount[2]['role'] = '';
		$order_amount[2]['color'] = '';
		$order_amount[2]['role_name'] = '';
		
	
		
		/*
		//平台优惠券
		$order_amount[3]['name'] = '平台优惠券';
		$order_amount[3]['amount'] = $order['platform_coupon_price'];
		$order_amount[3]['value'] = 'platform_coupon_price';
		$order_amount[3]['role'] = '';
		$order_amount[3]['color'] = '';
		$order_amount[3]['role_name'] = '';
		
		//商家优惠券
		$order_amount[4]['name'] = '商家优惠券';
		$order_amount[4]['amount'] = $order['platform_coupon_price'];
		$order_amount[4]['value'] = 'new_coupon_price';
		$order_amount[4]['role'] = '';
		$order_amount[4]['color'] = '';
		$order_amount[4]['role_name'] = '';
		*/
		//其他退款补偿
		/*$order_amount[6]['name'] = '其他补偿';
		$order_amount[6]['amount'] = 0;
		$order_amount[6]['value'] = 'compensate_price';
		$order_amount[6]['role'] = '';
		$order_amount[6]['color'] = '';
		$order_amount[6]['role_name'] = '';
		*/
		
		
		$data['list'] = $order_amount;
		$data['order_amount'] = $order_amount[0]['amount'] + $order_amount[1]['amount'] + $order_amount[2]['amount'];
		
      
		//店铺优惠券
		$condition = array();
		$condition['voucher_t_gettype'] = 3;
		$condition['voucher_t_state'] 	= 1;
		$condition['voucher_t_end_date'] = array('gt', time());
		$condition['voucher_t_store_id'] = array('in', $order['store_id']);
		$voucher_template = Model('voucher')->getVoucherTemplateList($condition);
		$voucher = array();
		if (!empty($voucher_template)) {			
			foreach ($voucher_template as $val) {				
				$param = array();
				$param['coupons_id'] = $val['voucher_t_id'];
				$param['coupons_price'] = $val['voucher_t_price'];
				$param['coupons_title'] = $val['voucher_t_title'];
				//$param['voucher_t_limit'] = $val['voucher_t_limit'];
				//$param['voucher_t_end_date'] = date('Y年m月d日', $val['voucher_t_end_date']);
			  
				$voucher[] = $param;
				
			}					
			
		}
		 
		$data['store_coupons'] = $voucher;


        //平台可用券

        $condition = array();

        $model_redpacket = Model('redpacket');
		$coupon_list = $model_redpacket->getRptTemplateList($condition, '*');
		$list  = array();
		foreach ($coupon_list as $val) {		  
		    $i = array();
			$i['coupon_id'] = $val['rpacket_t_id'];
		    $i['coupon_title'] = $val['rpacket_t_title'];
		    $i['coupon_price'] = $val['rpacket_t_price'];
		    $i['coupon_limit'] = $val['rpacket_t_limit'];
			$i['coupon_total'] = $val['rpacket_t_total'];
			$i['coupon_amount'] = $val['rpacket_t_total'] * $val['rpacket_t_price'];
			$i['coupon_giveout'] = $val['rpacket_t_giveout'];					
		
		    $list[] = $i;
		}

	 
		$data['me_coupons'] = $list;
        $data['order_state'] = $order['order_state'];



		output_data($data);
		
	}
	
	
	//操作退款项	
	public function order_refund_saveOp(){
		
        $pdf_model = model('pdf');
		//退款金额。
        $order_id = $_POST['order_id'];
        $order_refund = $_POST['refund'];
        $coupon_id = $_POST['coupon_id'];
        $store_coupon_id = $_POST['store_coupon_id'];


	    $order_info = model('order')->getOrderInfoN(array('order_id'=>$order_id));
        if($order_info['refund_state'] == 2){
            output_error('此订单已全额退款');
        }



        $goods_amount = $order_refund[0];
        $is_store = 0;
        if($goods_amount['role']!='' && $goods_amount['amount'] > 0){

            $is_store = 1;
            //执行商家退款            
        }

        $commission_amount = $order_refund[1];
        $is_commission = 0;
        if($commission_amount['role']!='' && $commission_amount['amount'] > 0){
            $is_commission = 1;
        }


        $is_delivery = 0;
        $delivery_amount = $order_refund[2];        
        if($delivery_amount['role'] !='' && $delivery_amount['amount'] > 0){
        //执行骑手退款
            $is_delivery = 1;
        }

        
        $refund_type = 1;
        if($goods_amount['role']!='' && $delivery_amount['role'] !='' ){
            $refund_type = 2;
        }



       //获取历史单数量
        $version = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
        $version_num = $version + 2;
        //生成订单快照
        $sphot =  model('order_snapshot')->createSphot($order_id);            
        if($sphot){		    
            $order_num =  model('order_snapshot')->getSnaOrderCount(array('order_id' => $order_id));
        }else{
            output_error('历史单生成失败，订单修改失败');
        }

    

        //判断当前退款属于1,2,3阶段。
        //商家接单前直接取消
        //商家接单后要生成票据，部分退款refund code 
        //订单送达后，就是退款票据
        $refund_stage = 0;
        if($order_info['order_state'] == 40 || $order_info['order_state'] == 50){
            $refund_stage = 1;
        }
        if($order_info['order_state'] == 60){
            $refund_stage = 2;
        }       
		

        //配送费。
        if($is_delivery == 1){

                $data = array();       
                $data['order_id']       = $order_info['order_id'];
                $data['order_sn']       = $order_info['order_sn'];
                $data['store_id']       = $order_info['store_id'];
                $data['store_name']     = $order_info['store_name'];
                $data['member_id']      = $order_info['buyer_id'];        
                $data['role']           = '2';
                $data['refund_comment'] = $_POST['refund_comment'];    
                $data['add_time']       = time();
                $data['refund_type']    = $refund_type;
                $data['refund_amount']  = $delivery_amount['amount'];
                $data['refund_stage']   = $refund_stage;
                $data['refund_log']     = serialize($order_refund);
                $data['is_delivery']    = 1;
                $data['is_commission']  = 0;
                $data['is_edit']        = 2; //证明这是订单编辑的
                $data['version']        = $version_num ;
                $data['refund_state']   = 1; //退配送费的项目直接标记已完成
                $delivery_refund_id = model('order_refund')->addRefund($data);

                $this->updateAmountDelivery($order_id,$delivery_refund_id);


                $refund_info = model('order_refund')->getRefundInfo(['refund_id' => $delivery_refund_id]);

                $refund_pay_amount 		=  $refund_info['refund_pay_amount'];
                $refund_pd_amount 		=  $refund_info['refund_wallet_amount'];
                $refund_coupon_price 	=  $refund_info['refund_coupon_amount'];

    
				$update = array();
				$update['refund_pay_amount'] 	= array('exp','refund_pay_amount + '. $refund_pay_amount);
				$update['refund_pd_amount'] 	= array('exp','refund_pd_amount + '. $refund_pd_amount);
				$update['refund_coupon_price'] 	= array('exp','refund_coupon_price + '. $refund_coupon_price);	
                $update['refund_delivery_fee'] 	= array('exp','refund_delivery_fee + '. $order_info['delivery_fee']);	

                model('order')->editOrder($update,['order_id' => $order_id]);

            


        }


        //只退订单剩余价值
        if($is_store == 1 ){

            	//获取里面剩余的
                $goods_list  = model('order')->getOrderGoodsList(['order_id' => $order_id,'goods_usable_num'=>array('gt',0)]);
                                    
                $refund_amount = 0;
                foreach($goods_list as $v){	             
                    $refund_amount += $v['goods_price'] * $v['goods_usable_num'];
                }

                $refund_amount += $order_info['pay_commission_amount'];

                $data = array();       
                $data['order_id']    = $order_info['order_id'];
                $data['order_sn']    = $order_info['order_sn'];
                $data['store_id']    = $order_info['store_id'];
                $data['store_name']  = $order_info['store_name'];
                $data['member_id']   = $order_info['buyer_id'];        
                $data['role']        = '2';
                $data['refund_comment'] = $_POST['refund_comment'];    
                $data['add_time']    = time();
                $data['refund_type'] = $refund_type;
                $data['refund_amount'] = $refund_amount;
                $data['refund_stage'] = $refund_stage;
                $data['refund_log']  = serialize($order_refund);
                $data['is_delivery'] = 0;
                $data['is_commission'] = 0;
                $data['is_edit']     = 2; //证明这是订单编辑的
                $data['version']     =  $version_num ;
                $refund_id = model('order_refund')->addRefund($data);

                if($refund_id > 0 ){

                    foreach($goods_list as $v){
                        $data = array(); 
                        $data['refund_id'] = $refund_id;                        
                        $data['order_id'] = $v['order_id'];                        
                        $data['goods_id'] = $v['goods_id'];                             
                        $data['goods_name'] =  $v['goods_name'];                        
                        $data['goods_price'] = $v['goods_price'];                           
                        $data['goods_num'] = $v['goods_num'];                               
                        $data['goods_image'] = $v['goods_image'];                       
                        $data['goods_pay_price'] = $v['goods_pay_price'];                     
                        $data['store_id'] = $v['store_id'];                      
                        $data['buyer_id'] = $v['buyer_id'];                     
                        $data['gc_id'] = $v['gc_id'];                      
                        $data['goods_size_id'] = $v['goods_size_id'];                    
                        $data['goods_specs_id'] =$v['goods_specs_id'];                      
                        $data['goods_spec'] = $v['goods_spec'];                      
                        $data['goods_size'] = $v['goods_size'];                       
                        $data['original_goods_id'] = $v['original_goods_id'];                       
                        $data['original_goods_name'] = $v['original_goods_name'];                         
                        $data['original_goods_num'] = $v['original_goods_num'];                       
                        $data['original_goods_spec'] = $v['original_goods_spec'];                         
                        $data['original_goods_size'] = $v['original_goods_size'];                         
                        $data['original_goods_price'] = $v['original_goods_price'];                        
                        $data['goods_lang'] = $v['goods_lang'];                        
                        $data['goods_optional_ids'] = $v['goods_optional_ids'];                        
                        $data['goods_optional'] = $v['goods_optional'];                        
                        $data['original_goods_optional'] = $v['original_goods_optional'];                        
                        $data['foodbox_price'] = $v['foodbox_price'];                      
                        $data['foodbox_id'] = $v['foodbox_id'];                        
                        $data['foodbox_name'] = $v['foodbox_name'];                       
                        $data['foodbox_num'] =$v['foodbox_num'];                         
                        $data['rec_id'] = $v['rec_id'];     
                        $data['goods_code'] = $v['goods_code'];            
                        $data['version'] = $version_num ;        
                        $row = model('order_goods_refund')->addRefund($data);


                        
                        $data = array();
                        $data['rec_id'] 	= $v['rec_id'];
                        $data['rec_num'] 	= $v['goods_num'];
                        $data['type'] 		= 'refund';
                        $data['order_id'] 	= $order_id;
                        //print_r($data);                        
                        model('order_goods_edit')->addGoods($data);      
                                                        
                        //更新主商品信息的可操作商品数量
                    //  model('order')->editOrderGoods(['goods_usable_num'=>['exp','goods_usable_num -'.$v['goods_num']]],['rec_id' => $v['rec_id']]);		
                
                    }


                    $this->updateAmount($order_id,$refund_id);

                    $refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);

                    $refund_pay_amount 		=  $refund_info['refund_pay_amount'];
                    $refund_pd_amount 		=  $refund_info['refund_wallet_amount'];
                    $refund_coupon_price 	=  $refund_info['refund_coupon_amount'];
            
                    $update = array();
                    $update['refund_pay_amount'] 	= array('exp','refund_pay_amount + '. $refund_pay_amount);
                    $update['refund_pd_amount'] 	= array('exp','refund_pd_amount + '. $refund_pd_amount);
                    $update['refund_coupon_price'] 	= array('exp','refund_coupon_price + '. $refund_coupon_price);	
                    $update['refund_pay_commission_amount'] 	= array('exp','refund_pay_commission_amount + '.  $order_info['pay_commission_amount']);		


                 //   print_r($update);

                    model('order')->editOrder($update,['order_id' => $order_id]);
    




                }

                
               

        }


        //退平台抽成
        if($is_commission == 1){            

            $snapshot_amendment[] = '退平台抽成';

        }

        if($is_store == 1 && $is_delivery == 0){

            $snapshot_amendment[] = '部分退款';

        }

        if($is_store == 0 && $is_delivery == 1){

            $snapshot_amendment[] = '退配送费';

        }

        if($is_store == 1 && $is_delivery == 1){

            $snapshot_amendment[] = '全单退款';

        }

        if($coupon_id > 0){

            $snapshot_amendment[] = '赠送优惠券';

        }





        $where = array();
		$where['order_id'] = $_POST['order_id'];	
		$update = array();
		$update['order_update_time'] = time();
		$update['is_old_num'] = $order_num + 1;
		$update['store_note'] = $_POST['store_note'];
		$update['snapshot_amendment'] = implode(',', $snapshot_amendment); //当前新订单做了哪些处理	
   

		$row = model('order')->editOrder($update, $where);
        if($row){

            if($is_store == 1){ //检查是否有商品退款
                //更新原商品数量
                $edit_num_list = model('order_goods_edit')->getGoodsList(['order_id' => $order_id,'type' => 'refund']);
                if(!empty($edit_num_list)){
                    foreach($edit_num_list as $v){					
                        model('order')->editOrderGoods(['goods_usable_num' => ['exp','goods_usable_num -'.$v['rec_num']]], ['rec_id' => $v['rec_id']]);
                    }
                }

                //从新计算商品价格。
                $goods_list = model('order')->getOrderGoodsList(['order_id' =>  $order_id]);
                $goods_amount_new = 0;
                foreach($goods_list as $v){
                    if($v['goods_usable_num'] >0){
                        $goods_amount_new += $v['goods_price'] * $v['goods_usable_num'];
                        model('order')->editOrderGoods(['goods_num' => $v['goods_usable_num']],['rec_id' => $v['rec_id']]);
                    }
                }
                model('order')->editOrder(['goods_amount' => $goods_amount_new],['order_id' => $order_id]);

                //删除编辑的信息
			    model('order_goods_edit')->delGoods(['order_id' => $order_id]);

            }


            if($is_delivery == 1){

                $refund_delivery_fee = $delivery_amount['amount'];
                model('order')->editOrder(['refund_delivery_fee' => $refund_delivery_fee],['order_id' => $order_id]);

            }


            if($is_commission == 1){ //检查是否有推抽成

                $refund_commission_amount = $commission_amount['amount'];
                model('order')->editOrder(['refund_commission_amount' => $refund_commission_amount],['order_id' => $order_id]);

            }



            $record_data = array();
			$record_data['order_id'] = $order_id;
            
            $refund_type = 'Partial';

            if($is_delivery == 1 && $is_store == 1){
                $refund_type = 'Full';
            }elseif($is_store == 1 && $is_delivery == 0){
                $refund_type = 'Partial';
            }elseif($is_store == 0 && $is_delivery == 1){
                $refund_type = 'Partial';
            }



			$record_data['me_refund_type']       =  $refund_type;//
			$record_data['me_refund_time']          = time() ;//			
            $record_data['me_refund_info']      = serialize($order_refund);
            $record_data['me_refund_comment'] = $_POST['common'];
			model('order_record')->addRecord('me_refund',$record_data);
			  

            if($order_info['order_state'] == 60 && ($refund_id > 0 || $delivery_refund_id > 0)){ //

                model('order_bill')->admin_order_refund($order_id,$goods_amount,$delivery_amount,$commission_amount,$refund_id,$delivery_refund_id);
                
            }



            output_data('操作成功');


        }else{


            output_error('操作失败');

        }



          
        
	}	
	
	
    
        private function updateAmountDelivery($order_id,$refund_id){


            
			$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id));

			//获取stripe支付的金额
			$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];			
			//获取钱包支付的金额
			$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			//获取优惠券抵扣的金额
			$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);

		//	$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
			$refund_amount = $refund_info['refund_amount'];
		//	foreach($refund_goods_list as $v){
			//	$refund_amount +=  $v['goods_price'] * $v['goods_num'];
		//	}

			$update = array();
			$update['refund_amount'] 		= $refund_amount;
			$update['refund_pay_amount'] 	= 0;
			$update['refund_wallet_amount'] = 0;
			$update['refund_coupon_amount'] = 0;
			
			if($stripe_amount > 0 ){ //stripe youqian
				
				if($stripe_amount >= $refund_amount){ //stripe 有钱，直接退	

					$update['refund_pay_amount'] = $refund_amount;	

				}elseif($stripe_amount < $refund_amount && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退		

					$update['refund_pay_amount'] = $stripe_amount;
					
					$refund_wallet_amount = $refund_amount - $stripe_amount ;	
					
					if($wallet_amount >= $refund_wallet_amount  ){
					
						$update['refund_wallet_amount'] = $refund_wallet_amount;	
					
					}elseif($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0 ){
						
						$update['refund_wallet_amount'] = $wallet_amount;	

						$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;
					
						$update['refund_coupon_amount'] = $refund_coupon_amount;

					}else{
						$update['refund_coupon_amount'] = $refund_wallet_amount;
					}

				}	

			}elseif($wallet_amount > 0 ){ //stripe 没钱，钱包 有钱。

				if($wallet_amount >= $refund_amount ){ //余额大于退款金额。直接退。

					$update['refund_wallet_amount'] = $refund_amount;

				}elseif($wallet_amount < $refund_amount &&  $wallet_amount > 0 ){ //有钱，但钱不够。
						
					$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

					$refund_coupon_amount = $refund_amount - $wallet_amount;
				
					$update['refund_coupon_amount'] = $refund_coupon_amount;

				}else{

					$update['refund_coupon_amount'] = $wallet_amount;

				}


			}elseif($voucher_amount > 0 ){ //优惠券支付的。

				if($voucher_amount >= $refund_amount ){
					
					$update['refund_coupon_amount'] = $refund_amount;

				}

			}
		//	print_r($update);
		
			model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);


        }


        //更新商品金额
		private function updateAmount($order_id,$refund_id){

			$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id));

			//获取stripe支付的金额
			$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];			
			//获取钱包支付的金额
			$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			//获取优惠券抵扣的金额
			$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);
            
            $refund_amount  = $refund_info['refund_amount'];
            /*
			$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
			$refund_amount = 0;
			foreach($refund_goods_list as $v){
				$refund_amount +=  $v['goods_price'] * $v['goods_num'];
			}
            */
			$update = array();
			//$update['refund_amount'] 		= $refund_amount;
			$update['refund_pay_amount'] 	= 0;
			$update['refund_wallet_amount'] = 0;
			$update['refund_coupon_amount'] = 0;
			
			if($stripe_amount > 0 ){ //stripe youqian
				
				if($stripe_amount >= $refund_amount){ //stripe 有钱，直接退	

					$update['refund_pay_amount'] = $refund_amount;	

				}elseif($stripe_amount < $refund_amount && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退		

					$update['refund_pay_amount'] = $stripe_amount;
					
					$refund_wallet_amount = $refund_amount - $stripe_amount ;	
					
					if($wallet_amount >= $refund_wallet_amount  ){
					
						$update['refund_wallet_amount'] = $refund_wallet_amount;	
					
					}elseif($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0 ){
						
						$update['refund_wallet_amount'] = $wallet_amount;	

						$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;
					
						$update['refund_coupon_amount'] = $refund_coupon_amount;

					}else{
						$update['refund_coupon_amount'] = $refund_wallet_amount;
					}

				}	

			}elseif($wallet_amount > 0 ){ //stripe 没钱，钱包 有钱。

				if($wallet_amount >= $refund_amount ){ //余额大于退款金额。直接退。

					$update['refund_wallet_amount'] = $refund_amount;

				}elseif($wallet_amount < $refund_amount &&  $wallet_amount > 0 ){ //有钱，但钱不够。
						
					$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

					$refund_coupon_amount = $refund_amount - $wallet_amount;
				
					$update['refund_coupon_amount'] = $refund_coupon_amount;

				}else{

					$update['refund_coupon_amount'] = $wallet_amount;

				}


			}elseif($voucher_amount > 0 ){ //优惠券支付的。

				if($voucher_amount >= $refund_amount ){
					
					$update['refund_coupon_amount'] = $refund_amount;

				}

			}
		//	print_r($update);
		
			model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);

}




	//解除问题单
	public function update_problem_stateOp(){
		
		
		$comment = $_POST['comment'];
		$order_id = $_POST['order_id'];		
		$data = array(			
			'is_problem' => 0
		);		
		$where = array(
			'order_id' => $order_id
		);		
		$row = model('order') -> editOrder($data,$where);
		if($row){				
		    
		    
		    
		    $where = array();
		    $where['order_id'] = $order_id;
		    $where['record_type'] = array('in','attention,action_required');
		    $where['record_state'] = 0;
		    $list = model('order_record')->getRecordList($where);
		    foreach($list as $v){
		    
		                
		                $record_datas = unserialize($v['record_datas']);
		                
		                
		            	$data_record = array();
						$data_record['order_id'] = $v['order_id'];
						$data_record['resolved_type'] = 'AR. '.$record_datas['action_required_type'];
						$data_record['resolved_time'] = time();
						$data_record['resolved_comment'] = $comment;
						model('order_record')->addRecord('resolved',$data_record);
		    
		    
            			//更新子项的状态			
            			$where = array('record_id' => $v['record_id']);
            			$data = array(
            				'record_state' => 1,
            				'record_comment' => $comment
            			);			
            			$update = model('order_record')->editRecord($data,$where);	
            			
    			
		    }
			
			
			
			
			
			
			output_data('操作成功');
		}else{			
			output_error('操作失败');
		}
		
		
		
	}
	
    //从新指派配送员
    public function delivery_allocationOp(){

        $delivery_id = $_POST['delivery_id'];
        






    }


	
	
}
