<?php

/*
分单
*/

defined('InMadExpress') or exit('Access Invalid!');
class sortingControl extends mobileAdminControl {
   
    public function __construct(){
        parent::__construct();
     
    }





	public function searchOp(){
	
		
		//搜索商家
		$where = array();
		$where['store_name_primary|store_name_secondary|store_code|store_phone'] =  array('like', '%' . $_POST['keyword'] . '%');		
		$where['store_state'] = array('in','1,2');
		$store = model('store')->getStoreList($where);
		$store_list = array();
		foreach($store as $v){
			$storeItem['icon'] =   $this->store_icon($v['region_id']);
			$storeItem['title'] = $v['store_name_primary'];
			$storeItem['code'] = $v['store_code'];
			$storeItem['id'] = $v['store_id'];
			$storeItem['type'] = 'store';
			$store_list[] = $storeItem;
		}		
		
	//	print_r($store_list);
		//搜索配送员
		
		$where = array();
		$where['distributor_name|distributor_code|distributor_mobile'] =  array('like', '%' . $_POST['keyword'] . '%');		
		$where['distributor_status'] = 1;
		$waiter = model('waiter')->getWaiterList($where);
		$waiter_list = array();
		foreach($waiter as $v){
			$waiterItem['icon'] =  $this->deliver_icon($v['distributor_id']);
			$waiterItem['title'] = $v['distributor_name'];
			$waiterItem['code'] = $v['distributor_code'];
			$waiterItem['id'] = $v['distributor_id'];
				$waiterItem['type'] = 'deliver';
			$waiter_list[] = $waiterItem;
		}
		
	//print_r($waiter_list);
		
		//搜索订单	
	
		$where = array();
		$where['order_sn|buyer_name|buyer_code|buyer_phone'] =  array('like', '%' . $_POST['keyword'] . '%');		
		$where['order_state'] = array('in','20,30,40,50');
		$order = model('order')->getOrderListNew($where);
		$order_list = array();
		foreach($order as $v){
			$orderItem['icon'] =   $this->user_icon($v['buyer_region_id'],$v['is_problem']);
			$orderItem['title'] = $v['buyer_name'];
			$orderItem['code'] = $v['buyer_code'];
			$orderItem['id'] = $v['order_id'];
			$orderItem['type'] = 'order';
			
			$order_list[] = $orderItem;
		}
		
		//print_r($order_list);
	
		$result = array_merge((array)$store_list,(array)$waiter_list,(array)$order_list);
	
		output_data($result);
	}



//钱的标志
	public function sorting_moneyOp(){	
			$data = array(
				0 => array(
					'lat' => '-37.87909155379726',
					'lng' => '145.0434582916959',
					'name' => '钱1',
					'id' => 1
				),
				
				1 => array(
					'lat' => '-37.81365536608386',
					'lng' => '144.95772240082755',
					'name' => '钱2',
					'id' => 2		
				),
				
				2 => array(
					'lat' => '-37.820246312984736',
					'lng' => '145.12192174139483',
					'name' => '钱3',
					'id' => 3
				),
				
				3 => array(
					'lat' => '-37.8793630743023',
					'lng' => '145.162481999102',
					'name' => '钱4',
					'id' => 4
				)
			);
		
			foreach($data as  $k => $v){		
				
						$feature[$k]['type'] = 'Feature';
						$feature[$k]['properties']['iconSize'] = array(25,25);
						$feature[$k]['properties']['name'] = $v['name'];			
						$feature[$k]['properties']['id'] = $v['id'];
						$feature[$k]['geometry']['type'] = 'Point';						
						$feature[$k]['geometry']['coordinates'][0] = $v['lng'];			
						$feature[$k]['geometry']['coordinates'][1] = $v['lat'];		
					
			}
		
			$feature_list = array();
			$feature_list['type'] = 'FeatureCollection';
			$feature_list['features'] = $feature;					
			output_data(array('moeny'=>$feature_list));
	
	}
	//计算配送员于钱标的距离	
	private function DeliverMoneyDistance($deliver_coordinate){
	
		$data = array(
				0 => array(
					'lat' => '-37.87909155379726',
					'lng' => '145.0434582916959',
					'name' => '钱1',
					'id' => 1
				),
				
				1 => array(
					'lat' => '-37.81365536608386',
					'lng' => '144.95772240082755',
					'name' => '钱2',
					'id' => 2		
				),
				
				2 => array(
					'lat' => '-37.820246312984736',
					'lng' => '145.12192174139483',
					'name' => '钱3',
					'id' => 3
				),
				
				3 => array(
					'lat' => '-37.8793630743023',
					'lng' => '145.162481999102',
					'name' => '钱4',
					'id' => 4
				)
			);	
			$deliver_coordinate = explode(',',$deliver_coordinate);
			$result = false;
			foreach($data as  $k => $v){				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $v['lat'], $v['lng']);
				if($distance < 3000){
						$result = true;
				}
			}				
			return $result;
	}
	
	
	
	
	public function sorting_iniOp(){
	
		
		$data = array();	
		//商家总统计数
		$where = array(
			'store_state' => array('in','0,1,2')
		);		
		$storeTotal = model('store')->getStoreCount($where);	
		$data['StoreTotle']=  intval($storeTotal);	
		
		//配送员总统计数
		
		$where = array();
		$where['distributor_status'] = array('in','1,3');
		$where['distributor_online'] = array('in','20,30');			
		$waiterTotal = model('waiter')->getWaiterCount($where);	
		
		
		$data['WaiterTotle'] = intval($waiterTotal) ;			
		
		//订单总统计数
		$data['OrderTotle'] =  $this->orderStateCount(array('in','20,30,40,50,70'));			
		
		//订单池
		$data['OrderPoolTotle'] =  $this->orderStateCount(array('in','20'));			
		
	
		$data['checkOrderState'] = $this->orderStateTotal();	
		$data['checkWaiterState'] = $this->waiterStateTotal();
		$data['checkStoreState'] = $this->storeStateTotal();	
	
		//有排班		
		$data['checkWaiterYesSch'] = $this->waiterSchCount(1);			
		//无排班
		$data['checkWaiterNoSch'] = $this->waiterSchCount(0);			
		//有租车
		$data['checkWaiterYesVehicle'] = $this->waiterVehicleCount(1);			
		//无租车
		$data['checkWaiterNoVehicle'] = $this->waiterVehicleCount(0);	
		
		
		
		
		//问题单
		
		
		$where = array(
			'is_problem' => 1
		);
		$problem = model('order')->getOrderCount($where);
		
		$data['ProblemNum'] = intval($problem) ;	
	
		
		$where = array(
			'is_problem' => 2
		);
		$problem = model('order')->getOrderCount($where);
		
		$data['WarningNum'] = intval($problem) ;	
		
		//日期
		$data['date'] = array(
			'week' => date('D',time()),
			'date' => date('d/m/y',time())			
		);


	
		output_data($data);
	
	
	}


    /**
     * 分担地图
     */
    public function sortingOp(){
				
		//筛选操作
		
		//商家总统计数
		$where = array(
			'store_state' => array('in','0,1,2')
		);		
		$storeTotal = model('store')->getStoreCount($where);	
		Tpl::output('StoreTotle', intval($storeTotal));	
		
		//配送员总统计数
		
			$where = array();
			$where['distributor_status'] = array('in','1,3');
			$where['distributor_online'] = array('in','20,30');			
			$waiterTotal = model('waiter')->getWaiterCount($where);	
			
		
		
		Tpl::output('WaiterTotle', intval($waiterTotal));			
		
		//订单总统计数
		Tpl::output('OrderTotle',  $this->orderStateCount(array('in','20,30,40,50,70')));			
		
		//订单池
		Tpl::output('OrderPoolTotle',  $this->orderStateCount(array('in','20')));			
		
	
		Tpl::output('checkOrderState', json_encode($this->orderStateTotal()));	
		Tpl::output('checkWaiterState', json_encode($this->waiterStateTotal()));
		Tpl::output('checkStoreState', json_encode($this->storeStateTotal()));	
	
		//有排班		
		Tpl::output('checkWaiterYesSch', $this->waiterSchCount(1));			
		//无排班
		Tpl::output('checkWaiterNoSch', $this->waiterSchCount(0));			
		//有租车
		Tpl::output('checkWaiterYesVehicle', $this->waiterVehicleCount(1));			
		//无租车
		Tpl::output('checkWaiterNoVehicle', $this->waiterVehicleCount(0));	
		
		
		//问题单
		
		
		$where = array(
			'is_problem' => 1
		);
		$problem = model('order')->getOrderCount($where);
		
		Tpl::output('ProblemNum', intval($problem));	
		
	
	
		$big_region_list = model('region')->getRegionList(array('region_parent_id'=> 0));		
		$small_region = model('region')->getRegionList(array('region_type'=> 2));		
		$where = array();
		$where['store_state'] = 1;
		$store = model('store')->getStoreList($where);		
		foreach($store as $k => $v){			
			$data[$k]['store_id'] = $v['store_id'];
			$data[$k]['store_name'] = $v['store_name'];			
			$data[$k]['lat'] = $v['store_lat'];
			$data[$k]['lng'] = $v['store_lng'];
			$data[$k]['region_id'] = $v['region_id'];
			$data[$k]['out'] = 5000;
			$data[$k]['inner'] = 1500;
			//$data[$k]['store_yunli'] = $this->storeWaiter($v['store_lng'].','.$v['store_lat'],$v['store_name']);
			//$data[$k]['store_color'] = $color[$k];				 
			
		}
	
		
		Tpl::output('storeList', json_encode($data));			
		Tpl::output('regionList', json_encode($small_region));			
		Tpl::output('regionBigList', json_encode($big_region_list));			
		
		
		//获取当前用户数据。
		
		
		
		Tpl::setDirquna('shop');
        Tpl::showpage('sorting.index');
    }
	

	
	
	//统计
	private function orderStateTotal(){
		$orderstate = array(
			array(
			'name'=>'总计',
			'total' =>  $this->orderStateCount(0,1)	
			),
			array(
				'name'=>'分配中',
				'total' => $this->orderStateCount(20)	
			), 
			array(
				'name'=>'处理中', 
				'total' =>  $this->orderStateCount(30)	
			),
			array(
				'name'=>'取货中',
				'total' =>  $this->orderStateCount(40)	
			),
			array(
				'name'=>'配送中',
				'total' =>  $this->orderStateCount(50)	
			),
			array(
				'name'=>'已完成',
				'total' =>  $this->orderStateCount(60)	
			),
			array(
				'name'=>'已取消',
				'total' =>  $this->orderStateCount(0)	
			),
			
		);
		
		
		
		
		
		return $orderstate;
	}
	
	//查询订单统计数
	private function orderStateCount($state , $type = 0){
	
		$where = array();
		if($type == 0){
			$where['order_state'] = $state;
		}else {
			$where['order_state'] = array('in','0,20,30,40,50,60');			
		}
		$where['add_time'] =  array('between',array(strtotime(date('Y-m-d',time())),strtotime(date('Y-m-d',time()))+86400));
		
		$count = model('order')->getOrderCount($where);
		
		return $count;
	
	}
	
	
	//店铺统计
	private function storeStateTotal(){			
		$storestate = array(
			array(
				'name'=>'上线',
				'total' => $this->storeStateCount(1)			
			), 
			array(
				'name'=>'打烊', 
				'total' => $this->storeStateCount(2)
			),
			array(
				'name'=>'未上架',
				'total' => $this->storeStateCount(0)
			)			
		);		
		return $storestate;	
	}
	
	//查询店铺 
	private function storeStateCount($type){
	
		$where = array();
		if($type == 1){
			$where['store_state'] = 1;
			$where['store_online'] = 1;
		}
		if($type == 2){
			$where['store_state'] = 1;
			$where['store_online'] = 0;
		}
		if($type == 0){
			$where['store_state'] = 2;			
		}
		$count = model('store')->getStoreCount($where);		
		return $count;
	
	}
	
	
	
	//店铺统计
	private function waiterStateTotal(){	
		$waiterstate = array(
			
			array(
				'name'=>'总计',
				'total' => $this->waiterStateCount('all')	
			),
			array(
				'name'=>'接单中',
				'total' => $this->waiterStateCount(0)	
			), 						
			array(
				'name'=>'取餐中',
				'total' => $this->waiterStateCount(1)	
			), 
			array(
				'name'=>'配送中', 
				'total' => $this->waiterStateCount(2)	
			),
			array(
				'name'=>'空闲', 
				'total' => $this->waiterStateCount(3)	
			),
		);		
		return $waiterstate;	
	}
	
	
	//查询配送员 
	private function waiterStateCount($deliver_state = 0){
	
		
		$where = array();
		$where['distributor_status'] = 1;
		$where['distributor_online'] = 20;		
		
		if($deliver_state === 'all'){
			$where['delivery_state'] = array('in','0,1,2,3');
		}else{		
			$where['delivery_state'] = $deliver_state;
		}
		
		
		/*
		if($deliver_type == 'is_problem'){
			$where['distributor_status'] = 1;
			$where['is_problem'] = 1;
		}elseif($deliver_type == 'is_receipt'){
		
			$where['distributor_status'] = 1;
			$where['distributor_online'] = array('in','20,30');
			$where['delivery_state'] = 0;
		
		}else{
			
			$where['distributor_status'] = $state;
			$where['distributor_online'] = array('in','20,30');
			if($deliver_type != ''){
				$where['delivery_state'] = $deliver_type;
			}
		}
		*/
		//接单中
		
		//print_r($whereGLOBALS["name"]);
		
		
		
		
		
		$count = model('waiter')->getWaiterCount($where);		
		return $count;
	
	}
	
	//查询排班
	
	private function waiterSchCount($type = 0){
	
		$where = array();
		$where['distributor_online'] = array('in','20,30');
		$where['scheduling_is'] = $type;		
		$count = model('waiter')->getWaiterCount($where);		
		return intval($count);
	
	}
	
	//查询租车
	
	private function waiterVehicleCount($type = 0){
	
		$where = array();
		$where['distributor_online'] = array('in','20,30');
		$where['vehicle_lease_is'] = $type;		
		$count = model('waiter')->getWaiterCount($where);		
		return intval($count);
	
	}
	
	
	
	/************************************/
	//新功能mapbox
	
	//获取地区
	public function get_regionOp(){
		$data= array();
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable'=> 1));		
		
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo[$kk] = explode(",",$vv);
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
						
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		output_data(array('map_region'=>$result,'region_list'=> $region));
		//echo json_encode(array('code' => 200,'data'=>$data));		
		
	}
	
	//点击商家后返回的数据
	
	public function get_store_itemOp(){
		
		
		$where = array(
			'store_id' => $_POST['store_id']
		);
		
		$store = model('store')->getStoreInfo($where);
		if($store){		
		
			$store['order_polling'] = $this->storeOrderNum($store['store_id'],20); //订单池
			
			
			$store['order_pending'] = $this->storeOrderNum($store['store_id'],30); //待接单
			
			
			$store['order_meals'] = $this->storeOrderNum($store['store_id'],40); //备餐中
			
			
			$store['order_deliver'] = $this->storeOrderNum($store['store_id'],50); //配送中
			
			
			$store['order_problem'] = $this->storeOrderNum($store['store_id'],'is_problem'); //问题单
			
			
			$store['order_complete'] = $this->storeOrderNum($store['store_id'],60,date('Y-m-d',time())); //已完成
			
			
			$store['order_cancel'] = $this->storeOrderNum($store['store_id'],0,date('Y-m-d',time())); //已取消
			
			
			
			
			$delivery = $this->getStoreDeliver(array('store_lat'=>$store['store_lat'],'store_lng'=> $store['store_lng'],'store_id'=>$store['store_id']));
			
			//print_r($delivery);
			
			$store['order_capacity'] =  $delivery['count']; //运力
			
			
			
			
			// 获取今日总单
			$order_total_num = model('order')->getOrderCount(array('store_id'=>$store['store_id'],'order_state'=>array('in','20,30,40,50,60'),'order_date'=>date('Y-m-d',time())));
			
			
			$store['order_total_num'] = intval($order_total_num); //今日总单
			
			
			$where = array(
				'store_id' => $store['store_id'],
				'order_date' => date('Y-m-d',time()),
				'order_state' => array('in','20,30,40,50,60')
			);
			$order_total_amount = model('order')->getOrderInfoN($where,'SUM(order_amount) as total_amount');
			
			
		
			
			$store['order_total_amount'] = '$'.($order_total_amount['total_amount'] > 0 ? $order_total_amount['total_amount'] : '0.00' ); //今日总收入
			
			$store['order_average_amount'] = '$'.@sprintf("%.2f",(  $order_total_amount['total_amount'] / $order_total_num));
			
			
			$storeOrder = $this->getStoreOrder($store['store_id']);	
			
			//获取配送中的订单
			$store['order_list'] = $storeOrder['order_list'];
			
			$rate = @sprintf("%.2f",(($store['order_cancel'] / $store['order_total_num']) * 100));
			
			$store['order_success_rate'] = $rate > 0 ? (100 - $rate).'%' : '100%' ;
					
					
					
					
					
					
			//获取他周边配送员
			
			$store['deliver_list'] = $delivery['deliver'];		
				
			/*
			
			if(count($storeOrder['deliver_list']['features']) > 0){			
				$store['deliver_list'] = $storeOrder['deliver_list'];						
			}else{			
				$store['deliver_list'] = $delivery['deliver'];						
			}	
			*/
			
				
			$store['store_list'] = $this->getStoreOne($store['store_id']);						
			
		}		
		
		output_data(array('store_info'=>$store,'RefreshTime'=>date('H:i:s',time())));
			
	}
	
	
	//获取他本身的坐标
	private function getStoreOne($store_id){
		
				$where = array(
					'store_id' => $store_id
				);
			
				$store = model('store')->getStoreList($where);	
		
				$data = array();
				$feature = array();
				foreach($store as $k => $v){	
						
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(40,40);
					$feature[$k]['properties']['name'] = $v['store_name_primary'];
					$feature[$k]['properties']['state'] = $v['store_state'];
					$feature[$k]['properties']['store_id'] = $v['store_id'];			
					$feature[$k]['properties']['iconUrl'] = $this->store_icon($v['region_id']);
					$feature[$k]['geometry']['type'] = 'Point';			
					$feature[$k]['geometry']['coordinates'][0] = $v['store_lng'];			
					$feature[$k]['geometry']['coordinates'][1] = $v['store_lat'];		
					
					
				}
				
				$data['type'] = 'FeatureCollection';
				$data['features'] = $feature;		
				
		
			return $data;
	}
	
	
	
	//获取当前配送中，或者已接单的订
	
	private function get_store_order($store_id){
		
		$count = 0;
				
		
		$where = array();
		$where['store_id'] = $store_id;
	
		if($_POST['problem'] == 1){
			$where['is_problem'] = 1;
		}else{
			$where['order_state'] = array('in','30,40,50');
		}
				
		$list = model('order')->getOrderListNew($where,'*', 'order_id asc');
		$data = array();	
		$feature = array();
		$deliver = array();
		foreach($list as $k => $v ){					
			if($v['distributor_id'] > 0){
					//判断是否有骑手	
					$deliver_info  = model('waiter')->getWaiterInfo(array('distributor_id'=> $v['distributor_id']));
					
					
					$deliver[$k]['type'] = 'Feature';
					$deliver[$k]['properties']['iconSize'] = array(40,40);
					$deliver[$k]['properties']['name'] = $v['distributor_name'];			
					$deliver[$k]['properties']['id'] = $v['distributor_id'];
					$deliver[$k]['properties']['iconUrl'] =  $this->deliver_icon($v['distributor_id']); //UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
					
					$deliver[$k]['geometry']['type'] = 'Point';
				
					$deliver_coordinate = explode(',',$deliver_info['region_coordinate']);
					
					$deliver[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$deliver[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];					
			}
			
			
			
			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['order_sn'] = $v['order_sn'];
			$feature[$k]['properties']['order_id'] = $v['order_id'];
			$feature[$k]['properties']['iconUrl'] = $this->user_icon($v['buyer_region_id'],$v['is_problem']);
			$feature[$k]['geometry']['type'] = 'Point';			
			
			$buy_coordinate = explode(',',$v['buyer_coordinate']);			
			$feature[$k]['geometry']['coordinates'][0] = $buy_coordinate[0];			
			$feature[$k]['geometry']['coordinates'][1] = $buy_coordinate[1];		
			
						
			
		}
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;	
		
		$deliver_list['type'] = 'FeatureCollection';
		$deliver_list['features'] = $deliver;
		return array('order_list'=> $data , 'deliver_list'=> $deliver_list);
//		output_data(array('order_list'=>$data));
		
	}
	
	
	
	
	
	
	
	
	
	
	//获取商家周边范围的可用配送员
	private function getStoreDeliver($store){
			
			$where = array();
			$where['distributor_status'] = 1;			
			$where['distributor_online'] = 20;			
			$where['delivery_state'] = 0;
			
				
			
			$deliver_data = model('waiter')->getWaiterList($where);
			$feature = array();
			$count = 0;
			$k = 0;
			foreach($deliver_data as $v){
			
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				
				
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$store['store_id']);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$store['store_id']);
								
				if($superposition == 1 ){						
						
						$feature[$k]['type'] = 'Feature';
						$feature[$k]['properties']['iconSize'] = array(40,40);
						$feature[$k]['properties']['name'] = $v['distributor_name'];			
						$feature[$k]['properties']['id'] = $v['distributor_id'];
						$feature[$k]['properties']['iconUrl'] = $this->deliver_icon($v['distributor_id'],1); //UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
						
						$feature[$k]['geometry']['type'] = 'Point';
						$deliver_coordinate = explode(',',$v['region_coordinate']);
						$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
						$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];					
						$count += $this->getDeliveryCount($v['distributor_id']);						
						$k++;
						
				}else if($distance < 5000 &&  $superposition == 0 ){
				
						$feature[$k]['type'] = 'Feature';
						$feature[$k]['properties']['iconSize'] = array(40,40);
						$feature[$k]['properties']['name'] = $v['distributor_name'];			
						$feature[$k]['properties']['id'] = $v['distributor_id'];
						$feature[$k]['properties']['iconUrl'] = $this->deliver_icon($v['distributor_id'],1);// UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
						
						$feature[$k]['geometry']['type'] = 'Point';
						$deliver_coordinate = explode(',',$v['region_coordinate']);
						$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
						$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];	
					
						$count += $this->getDeliveryCount($v['distributor_id']);
						$k++;
				}
				
			
				
				
					
			}
		
			//追加上已经在执行的
			$store_order_delivery_ids = model('order')->getOrderListNew(array('store_id'=>$store['store_id'] ,'order_state'=>array('in','30,40,50')),'distributor_id');
			
		
			if($store_order_delivery_ids){
				foreach($store_order_delivery_ids as $v){				
					$delivery_ids[] = $v['distributor_id'];				
				}
				$delivery_ids = implode(',',$delivery_ids);	
				$deliver_data = model('waiter')->getWaiterList(array('distributor_id' =>array('in',$delivery_ids)));
				
				
				foreach($deliver_data as $v){
					
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(40,40);
					$feature[$k]['properties']['name'] = $v['distributor_name'];			
					$feature[$k]['properties']['id'] = $v['distributor_id'];
					$feature[$k]['properties']['iconUrl'] = $this->deliver_icon($v['distributor_id'],1);// UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
					
					$feature[$k]['geometry']['type'] = 'Point';
					$deliver_coordinate = explode(',',$v['region_coordinate']);
					$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];	
										
					$count += $this->getDeliveryCount($v['distributor_id']);
					$k++;
				}
								
			}
					
		
		
		
			$deliver_list = array();
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;		
			
			
			return array('deliver'=>$deliver_list,'count' => $count);	
	
	}
	
	//返回配送员状态、
	
	private function deliver_state_name($deliver_id){
	
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		$order_num = model('order')->getOrderCount($where);
		

		$info = model('waiter')->getWaiterInfo(array('distributor_id'=> $deliver_id));
	

		switch($info['delivery_state']){

			case 3 :
				
				$state_name = '空闲';

			break;			
			case 2 :
			
				$state_name = '配送中';
				
			break;			
			case 1 :
			
				$state_name = '取餐中';

			break;			
			case 0 :
			
				$state_name = '接单中';

			break;
			
		}
		

		return $state_name;

		
		/*
		if($info['delivery_state'] == 3 && $info['distributor_status'] == 1 && $info['is_problem'] == 1){	
			//配送中
			return '空闲';
	
		}elseif($info['delivery_state'] == 3 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){	
		//配送中
			return '空闲';

		}elseif($info['delivery_state'] == 2 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){	
		//配送中
			return '配送中';

		}elseif($info['delivery_state'] == 2 && $info['distributor_status'] == 1 && $info['is_problem'] == 1){	
			
			return '配送中';
			
		}elseif($info['delivery_state'] == 1 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){		

			return '配送中';

		}elseif($info['delivery_state'] == 1 && $info['distributor_status'] == 1 && $info['is_problem'] == 1){	
			//取餐中
			return '取餐中';
			
		}elseif($info['delivery_state'] == 0 && intval($order_num) >= 1 && $info['distributor_status'] == 1  && $info['is_problem'] == 0){		
		
			return '接单中';
		//有运力
		}elseif($info['delivery_state'] == 0 && intval($order_num) >= 1 && $info['distributor_status'] == 1  && $info['is_problem'] == 1){

			return '接单中';
	
		}else{
		
			return '空闲';
		
		}
		*/
		
	}
	
	
	
	//$type : 1 当单独加载某个数据的时候。显示带数字标记的图标
	private function deliver_icon($deliver_id,$type=0){
	
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		$order_num = model('order')->getOrderCount($where);
		

	
		$info = model('waiter')->getWaiterInfo(array('distributor_id'=> $deliver_id));	
		//空闲中
		if($info['delivery_state'] == 3 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){
			if($type == 1){
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p&z_3.png';				
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p_3.png';			
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_z_3.png';				
				}else{
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_n_3.png';
				}			
			}else{
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p&z.png';				
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p.png';			
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_z.png';				
				}else{
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_n.png';
				}
			}
			
		
		//配送中
		}elseif($info['delivery_state'] == 2 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){			
			if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_p&z.png';				
			}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_p.png';			
			}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_z.png';				
			}else{
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
			}
		//取餐中
		}elseif($info['delivery_state'] == 1 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){				
			if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_take_p&z.png';				
			}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_take_p.png';			
			}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_take_z.png';				
			}else{
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_take_n.png';
			}									
		//接单中							
		}elseif($info['delivery_state'] == 0 && intval($order_num) >= 1 && $info['distributor_status'] == 1  && $info['is_problem'] == 0){	
			//有运力		
			if($type == 1){
				$count = 3 - intval($order_num);
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
					if($count == 2){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p&z_2.png';						
					}elseif($count == 1){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p&z_1.png';		
					}
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
				
					if($count == 2){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p_2.png';						
					}elseif($count == 1){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p_1.png';		
					}
					//$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p.png';	
							
					
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){							
					if($count == 2){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_z_2.png';						
					}elseif($count == 1){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_z_1.png';	
					}					
				//	$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_z.png';												
				}else{					
					if($count == 2){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_n_2.png';						
					}elseif($count == 1){
						$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_n_1.png';		
					}			
					//$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_n.png';				
				}				
			}else{
			
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){					
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p&z.png';	
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){					
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_p.png';	
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){	
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_z.png';	
				}else{					
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_little_n.png';				
				}			
			}			
		//空闲
		}elseif($info['delivery_state'] == 0 && intval($order_num) == 0 && $info['distributor_status'] == 1 && $info['is_problem'] == 0){ //空闲
			if($type == 1){
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p&z_3.png';				
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p_3.png';			
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_z_3.png';				
				}else{
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_n_3.png';
				}			
			}else{
				if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p&z.png';				
				}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_p.png';			
				}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_z.png';				
				}else{
					$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_free_n.png';
				}
			}
		}elseif($info['is_problem'] == 1){ //有问题						
			if($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_problem_p&z.png';				
			}elseif($info['scheduling_is'] == 1 && $info['vehicle_lease_is'] == 0){				
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_problem_p.png';			
			}elseif($info['scheduling_is'] == 0 && $info['vehicle_lease_is'] == 1){			
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_problem_z.png';				
			}else{
				$icon = UPLOAD_SITE_URL.'/admin/allocation/couriers_problem_n.png';
			}
		}
		return $icon;
	}
	
	
	
	//统计订单
	private function storeOrderNum($store_id,$state,$date = ''){	
	
	
		$where = array();
		if($state == 'is_problem' ){
			$where['is_problem'] = 1;
		}else{
			$where['order_state'] = $state;
		}
		$where['store_id'] = $store_id;
		
		if($date != ''){
		
			$where['order_date'] = $date;
		}
		
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	//实时刷新商家	
	public function  get_storeOp(){
		
		/*$setting = Model('setting')->getListSetting();				
		$storeSeting = unserialize($setting['map_show_storeState']);	
		$where = array();
		$state = array(0,1,2);
		
		if(in_array('未上架',$storeSeting)){			
			unset($state[0]);
		}
		if(in_array('上线',$storeSeting)){
			unset($state[1]);
		}
		if(in_array('打烊',$storeSeting)){
			unset($state[2]);
		}

		if($state){
			$where['store_state'] = array('in', $state);
		}*/
	
		$storeSeting = $_POST['mapState'];		
		$userScreen = $_POST['userScreen'];
		$where = array();
		$state = array();
		if(in_array('未上架',$storeSeting)){
			$state[] = 0;
		}
		if(in_array('上线',$storeSeting)){
			$state[] = 1;
		}
		if(in_array('打烊',$storeSeting)){
			$state[] = 2;
		}

	
		if(count($state) > 0){
				$where['store_state'] = array('in', $state);
		}else{
				$where['store_state'] = 999;
		}
			
	
		//print_r($where);
		
		$store = model('store')->getStoreList($where);				
		$data = array();
		$feature = array();
		foreach($store as $k => $v){	
				
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['name'] = $v['store_name_primary'];
			$feature[$k]['properties']['state'] = $v['store_state'];
			$feature[$k]['properties']['store_id'] = $v['store_id'];			
			$feature[$k]['properties']['iconUrl'] = $this->store_icon($v['region_id']);
			$feature[$k]['geometry']['type'] = 'Point';			
			$feature[$k]['geometry']['coordinates'][0] = $v['store_lng'];			
			$feature[$k]['geometry']['coordinates'][1] = $v['store_lat'];		
			
		}
		
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		output_data(array('store_list'=>$data));
		
		
	}
	
	
	private function store_icon($region_id){
	
	
		if($region_id == 147){
			$icon = UPLOAD_SITE_URL.'/admin/allocation/shop_CBD_n.png';
		}
		
		if($region_id == 146 ){
			$icon = UPLOAD_SITE_URL.'/admin/allocation/shop_east_n.png';
		}
	
		if($region_id == 145){
			$icon = UPLOAD_SITE_URL.'/admin/allocation/shop_southeast_n.png';
		}	
		return $icon;
	}
	
	
	
	
	//实时刷新用户
	
	public function problem_listOp(){
	
		$where = array();
		$where['is_problem'] = 1;		
		$list = model('order')->getOrderListNew($where,'*', 'order_id asc');
		$list_data = array();
		foreach($list as  $k => $v){
		
			$v['store_icon'] = $this->store_icon($v['store_region_id']);
			$v['user_icon'] = $this->user_icon($v['buyer_region_id'],$v['is_problem']);
			$v['deliver_icon'] = $this->deliver_icon($v['distributor_id']);
			$store_info = model('store')->getStoreInfo(array('store_id'=>$v['store_id']),'store_name_primary,store_name_secondary');
			$v['store_name_primary'] = $store_info['store_name_primary'];	
			$v['store_name_secondary'] = $store_info['store_name_secondary'];
			$list_data[$k] = $v;
			
		
		}		
		output_data(array('order_list'=>$list_data,'RefreshTime'=>date('H:i:s',time())));
		
	}

	
	
	public function get_store_orderOp(){
		
		
	/*	$setting = Model('setting')->getListSetting();				
		$storeSeting = unserialize($setting['map_show_orderState']);	
		$where = array();
		
		$state = array(10,20,30,40,50,60);
		
		
		if(in_array('订单池',$storeSeting)){
			unset($state[0]);
		}
		if(in_array('配送已接单',$storeSeting)){
			unset($state[1]);
		}
		if(in_array('商家已接单',$storeSeting)){
			unset($state[2]);		
		}
		if(in_array('配送中',$storeSeting)){
			unset($state[3]);
		}
		if(in_array('已完成',$storeSeting)){
			unset($state[4]);
		}		
		if(in_array('问题单',$storeSeting)){
			unset($state[5]);
		}*/
		
	//	$_POST = json_decode(file_get_contents('php://input'),true);
		$storeSeting = $_POST['orderState'];		
		$userScreen = $_POST['userScreen'];
		$problem = $_POST['problem'];
		$where = array();
		$state = array();
		
		
		if($problem > 0){			
			$where['is_problem'] = $problem;		
		}else{


			if(in_array('分配中',$storeSeting)){
				$state[] = 20;
			}
			if(in_array('处理中',$storeSeting)){
				$state[] = 30;
			}
			if(in_array('取货中',$storeSeting)){
				$state[] = 40;
			}
			if(in_array('配送中',$storeSeting)){
				$state[] = 50;
			}
			if(in_array('已完成',$storeSeting)){
				$state[] = 60;
			}
			if(in_array('已取消',$storeSeting)){
				$state[] = 0;
			}
			
			if(in_array('问题单',$storeSeting)){
				$state[] = 70;
			}
			


		
			if($userScreen == 1){
				$where['order_state'] = array('in',$state);
			}else{
				$where['order_state'] = array('in','30,40,50');
			}

			$where['add_time'] =  array('between',array(strtotime(date('Y-m-d',time())),strtotime(date('Y-m-d',time()))+86400));
		
		}
		
		
		
		//print_r($where);
		
		//print_r($where);
		$list = model('order')->getOrderListNew($where,'*', 'order_id asc');
		$data = array();	
		$feature = array();
		foreach($list as $k => $v ){	
					
	
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['order_sn'] = $v['order_sn'];
			$feature[$k]['properties']['order_id'] = $v['order_id'];
			$feature[$k]['properties']['iconUrl'] = $this->user_icon($v['buyer_region_id'],$v['is_problem'],$v['buyer_coordinate']);
			$feature[$k]['geometry']['type'] = 'Point';			
			
			$buy_coordinate = explode(',',$v['buyer_coordinate']);			
			$feature[$k]['geometry']['coordinates'][0] = $buy_coordinate[0];			
			$feature[$k]['geometry']['coordinates'][1] = $buy_coordinate[1];		
			
					
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		output_data(array('order_list'=>$data,'RefreshTime'=>date('H:i:s',time())));
		
		
		
	}
	
	/*
	problem = 1 问题单  2 预警单	
	*/
	
	private function user_icon($region_id,$is_problem = 0,$buyer_coordinate=""){
		

		if($is_problem == 1){
			
			$icon = 'user_problem';
			
		}elseif($is_problem == 2){
			
			$icon = 'user_warning';
			
		}else{	
	
			if($region_id == 147 || $region_id == 4){
				$icon = 'user_CBD_n';
			}
			
			if($region_id == 146 || $region_id == 5){
				$icon = 'user_east_n';
			}
				
			if($region_id == 145 || $region_id == 6){
				$icon = 'user_southeast_n';
			}	
		
		}
		
		if(!empty($buyer_coordinate) && $is_problem==0){
			$where = array(
				'buyer_coordinate' => $buyer_coordinate,
				'order_state' => array('in','20,30')
			);
			$count = model('order')->getOrderCount($where);
			
			if($count>1 && $count<10){
				$icon = $icon."_".$count;
			}else if($count>10){
				$icon = $icon."_10";
			}
		}
		
	
		return UPLOAD_SITE_URL.'/admin/allocation/'.$icon.".png";
	}
	
	
	public function edit_orderOp(){	
			
			$where= array(
				'order_id' => $_POST['order_id']
			);
			$data = array(
				'buyer_coordinate' => $_POST['lng'].','.$_POST['lat']
			);
			
			$row = model('order')->editOrder($data,$where);		
	
	}
	
	
	public function get_order_itemOp(){

		$where = array(
			'order_id' => $_POST['order_id']
		);
		
		$order = model('order')->getOrderInfo($where);
		if($order){				
	
			$store_info = model('store')->getStoreInfo(array('store_id'=>$order['store_id']),'store_name_primary,store_name_secondary');
			$order['store_name_primary'] = $store_info['store_name_primary'];	
			$order['store_name_secondary'] = $store_info['store_name_secondary'];
	
		
	
		//他自己本身的坐标
			$order['order_list'] =  $this->getOrderOne($order['order_id']);			
			
			$user = $this->getUserOrder($order['order_id']);			
			//他自己本身的坐标
			$order['deliver_list'] =  $user['deliver_list'];			
			//他周边的商家			
			$order['store_list'] = $user['store_list'];
				
			//获取
			$order['record_warning'] = $this->getRecord($order['order_id']);
			
			$order['record_problem'] = $this->getProblemRecord($order['order_id']);
			
			
			
			
		}
		output_data(array('order_info'=>$order,'RefreshTime'=>date('H:i:s',time())));
		
	}

	//通过一个用户获取他相关的未完成的订单
	public function get_order_list_bycoordinatesOp(){
	
		
		$order_state = array(
			10 => '',
			20 => '分配中',
			30 => '处理中',
			40 => '取货中',
			50 => '配送中',
			60 => '已完成',
			0 => '已取消'
		
		);
	
		$storeSeting = $_POST['orderState'];
		$where = array();
		$where['buyer_coordinate'] = $_POST['buyer_coordinate'];
		if($_POST['problem'] > 0){
		//$_POST['buyer_id']="3";
			$where['is_problem'] = $_POST['problem'];
		}else{

			if(in_array('分配中',$storeSeting)){
				$state[] = 20;
			}
			if(in_array('处理中',$storeSeting)){
				$state[] = 30;
			}
			if(in_array('取货中',$storeSeting)){
				$state[] = 40;
			}
			if(in_array('配送中',$storeSeting)){
				$state[] = 50;
			}
			if(in_array('已完成',$storeSeting)){
				$state[] = 60;
			}
			if(in_array('已取消',$storeSeting)){
				$state[] = 0;
			}
			
		

			$where['order_state'] = array('in',$state);

		}

		
		$orderlist = model('order')->getOrderList($where);

		if($orderlist){	

			foreach($orderlist as $key=>$order)
			{
	
				$store_info = model('store')->getStoreInfo(array('store_id'=>$order['store_id']),'store_name_primary,store_name_secondary');
				$order['store_name_primary'] = $store_info['store_name_primary'];	
				$order['store_name_secondary'] = $store_info['store_name_secondary'];
					
		
				//他自己本身的坐标
				$order['order_list'] =  $this->getOrderOne($order['order_id']);			
				
				$user = $this->getUserOrder($order['order_id']);			
				//他自己本身的坐标
				$order['deliver_list'] =  $user['deliver_list'];			
				//他周边的商家			
				$order['store_list'] = $user['store_list'];
				
				$problem = array('Normal','Act.Req','Abnormal');
				
				
				$order['problem_text'] = $problem[$order['is_problem']];
				
				$order['order_state'] = $order_state[$order['order_state']];
					
				//获取
				$order['record_warning'] = $this->getRecord($order['order_id']);
				
				$order['record_problem'] = $this->getProblemRecord($order['order_id']);
				
				$orderlist[$key]=$order;
			}
				
		}
		output_data(array('order_info'=>$orderlist,'RefreshTime'=>date('H:i:s',time())));
		
	}

	
	//获取问题单
	private function getProblemRecord($order_id){
		
		$where = array();
		$where['order_id'] = $order_id;
		$where['record_warning'] = 2;
		$result = model('order_record')->getRecordList($where);
		$list = array();
		$data = null;
		if($result){
			foreach($result as $k => $v){		
				
				$list[$k]['record_id'] 	= $v['record_id'];
				$list[$k]['order_id'] 	= $v['order_id'];
				$list[$k]['record_msg'] 	= $v['record_msg'];
				$list[$k]['record_time'] 	= date('d M y H:i:s',$v['record_time']);
				
			}
			$data = $list;
		}
		
		return $data;
		
	}
	
	//获取
	private function getRecord($order_id){
		
		$where = array();
		$where['order_id'] = $order_id;
		$where['record_warning'] = 1;
		$result = model('order_record')->getRecordList($where);
		$list = array();
		$data = null;
		if($result){
			foreach($result as $k => $v){		
				
				$list[$k]['record_id'] 	= $v['record_id'];
				$list[$k]['order_id'] 	= $v['order_id'];
				$list[$k]['record_msg'] 	= $v['record_msg'];
				$list[$k]['record_time'] 	= date('d M y H:i:s',$v['record_time']);
				
			}
			$data = $list;
		}
		
		return $data;
		
	}
	
	//获取配送员现在手里的订单
	private function getUserOrder($order_id){
		
		$count = 0;
		$where = array(
			'order_id' => $order_id		
		);
		$order_data = model('order')->getOrderList($where);
		$order_list = array();
		$data = array();
		$feature = array();
		foreach($order_data as $k => $v){
		
			//$order_list[] = $v['order_sn'];
			
			if($v['distributor_id'] > 0){
				//判断是否有骑手
	
					$deliver_info  = model('waiter')->getWaiterInfo(array('distributor_id'=> $v['distributor_id']));
	
					
					
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(40,40);
					$feature[$k]['properties']['name'] = $v['distributor_name'];			
					$feature[$k]['properties']['id'] = $v['distributor_id'];
					$feature[$k]['properties']['iconUrl'] =  $this->deliver_icon($v['distributor_id']); //UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
					
					$feature[$k]['geometry']['type'] = 'Point';
				
					$deliver_coordinate = explode(',',$deliver_info['region_coordinate']);
					
					$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];					
			}
			
			
			
			//商家坐标
			$store_feature[$k]['type'] = 'Feature';
			$store_feature[$k]['properties']['iconSize'] = array(40,40);
			$store_feature[$k]['properties']['store_name'] = $v['store_name'];
			$store_feature[$k]['properties']['store_id'] = $v['store_id'];
			$store_feature[$k]['properties']['iconUrl'] = $this->store_icon($v['store_region_id']);
			$store_feature[$k]['geometry']['type'] = 'Point';			
			
			$store_coordinate = explode(',',$v['store_coordinate']);			
			$store_feature[$k]['geometry']['coordinates'][0] = $store_coordinate[0];			
			$store_feature[$k]['geometry']['coordinates'][1] = $store_coordinate[1];		
						
					
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		
		
		$store['type'] = 'FeatureCollection';
		$store['features'] = $store_feature;		
		
		
		
		return array('deliver_list'=>$data,'store_list'=>$store);
		
//		output_data(array('order_list'=>$data));
		
	}
	
	
	
	
	
	//获取他本身的坐标
	private function getOrderOne($order_id){	

			$state = array(
				20 => '订单池',
				30 => '待商家接单',
				40 => '商家备餐中',
				50 => '配送中',
				60 => '已送达'
				
				
			);	
			$where = array(
				'order_id' => $order_id
			);
			
			$deliver_data = model('order')->getOrderList($where);
			$feature = array();
			foreach($deliver_data as $k => $v){					
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(40,40);
					$feature[$k]['properties']['order_sn'] = $v['order_sn'];
					$feature[$k]['properties']['order_id'] = $v['order_id'];
					$feature[$k]['properties']['order_state'] = $state[$v['order_state']];
					$feature[$k]['properties']['iconUrl'] = $this->user_icon($v['buyer_region_id'],$v['is_problem']);
					$feature[$k]['geometry']['type'] = 'Point';			
					
					$buy_coordinate = explode(',',$v['buyer_coordinate']);			
					$feature[$k]['geometry']['coordinates'][0] = $buy_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $buy_coordinate[1];		
			}
		
			$deliver_list = array();
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;		
			return $deliver_list;
	}
	
	
	
	
	public function get_delivery_itemOp(){
	
	 		
		$where = array(
			'distributor_id' => $_POST['deliver_id']
		);
		
		$deliver = model('waiter')->getWaiterInfo($where);
		
		if($deliver){				
			
			if($deliver['vehicle_lease_id'] > 0 ){			
			
				$deliver['deliver_rentacar'] = $this->deliver_rentacar($deliver['distributor_id']); //租车信息
				
			}elseif($deliver['vehicle_lease_id'] == 0 && $deliver['is_vehicle'] == 1){
			
				$rentacar = array();
				if($deliver['vehicle_brand'] == 'PA-Bike'){
				$icon = UPLOAD_SITE_URL.'/admin/allocation/icon_ebike_n.png';
				}
				
				if($deliver['vehicle_brand'] == 'MA-Bike'){
					$icon = UPLOAD_SITE_URL.'/admin/allocation/icon_ddc_n.png';
				}	
				
				$rentacar['vehicle_icon'] = $icon;
				$rentacar['vehicle_type'] = $deliver['vehicle_type'];
				$rentacar['vehicle_number'] = $deliver['vehicle_number'];
				$rentacar['lease_id'] = 0;
				$deliver['deliver_rentacar'] = $rentacar;
				
			}
			
			
			$deliver['deliver_duration'] = $this->deliver_line_time($deliver['last_online_time']).' mins'; //在线时长
			$deliver['last_online_time'] = date('d/m/y H:i:s',$deliver['last_online_time']);
			
			$deliver['deliver_sch'] = $this->deliver_sch($deliver['scheduling_id']); //是否有排班
			
			$deliver_order =  $this->getDeliverOrder($deliver['distributor_id']);
			
			
			$deliver['deliver_order'] = $deliver_order['order_sn'];//当前订单	
					
			$deliver['deliver_state_name'] =  $this->deliver_state_name($deliver['distributor_id']);
			
			//他自己本身的坐标
			$deliver['deliver_list'] =  $this->getDeliveryOne($deliver['distributor_id']);			
			
			//他的订单订单			
			$deliver['order_list'] = $deliver_order['order_list'];			
			
			//他接的订单的周边的商家	如果没有。就显示他周边的商家
			if(count($deliver_order['store_list']['features']) > 0){
			
				$deliver['store_list'] = $deliver_order['store_list'];
				
			}else{
			
				$deliver['store_list'] = $this->getDeliverStore($deliver);
				
			}
				
			
		}		
		
		output_data(array('deliver_info'=>$deliver,'RefreshTime'=> date('H:i:s',time()) ) );
	
	}
	
	
	//获取配送员周边5公里内的配送员
	private function getDeliverStore($deliver){
						
			$where = array();
			$where['store_state'] = 1;
			$store_data = model('store')->getStoreList($where);		
			$data = array();
			$feature = array();
			$kk = 0;
			foreach($store_data as $k=> $v){				
				$deliver_coordinate = explode(',',$deliver['region_coordinate']);				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $v['store_lat'], $v['store_lng']);
				if($distance < 5000){							
						$feature[$kk]['type'] = 'Feature';
						$feature[$kk]['properties']['iconSize'] = array(40,40);
						$feature[$kk]['properties']['name'] = $v['store_name_primary'];
						$feature[$kk]['properties']['state'] = $v['store_state'];
						$feature[$kk]['properties']['store_id'] = $v['store_id'];			
						$feature[$kk]['properties']['iconUrl'] = $this->store_icon($v['region_id']);
						$feature[$kk]['geometry']['type'] = 'Point';			
						$feature[$kk]['geometry']['coordinates'][0] = $v['store_lng'];			
						$feature[$kk]['geometry']['coordinates'][1] = $v['store_lat'];
						$kk ++ ;		
				}
			
		}
		
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;			
		return $data;		
	}
	
	
	
	

	
	//获取当前租车信息
	private function deliver_rentacar($deliver_id){	
		
		$where = array(
			'distributor_id' => $deliver_id,
			'lease_state' => 40
		);
		
		//获取当前正在进行的租车信息
		$info = model('vehicle_lease')->getLeaseInfo($where);	
		if(!empty($info)){
		
			if($info['vehicle_class_name'] == 'PA-Bike'){
				$icon = UPLOAD_SITE_URL.'/admin/allocation/icon_ebike_n.png';
			}
			
			if($info['vehicle_class_name'] == 'MA-Bike'){
				$icon = UPLOAD_SITE_URL.'/admin/allocation/icon_ddc_n.png';
			}	
							
			$info['vehicle_icon'] = $icon;	
		
		}
		
		
		
		return $info;
				
	}
	
	//获取当前排班状态
	private function deliver_sch($scheduling_id){			
		
		$where = array(
			'scheduling_id' => $scheduling_id		
		);
		$info = model('scheduling')->getSchedulingInfo($where);	
		if(!empty($info)){
		
			$info['scheduling_start_time'] = date('H:i:s',$info['scheduling_start_time']);
			$info['scheduling_end_time'] = date('H:i:s',$info['scheduling_end_time']);
			$info['scheduling_addtime'] = date('d/m/y H:i:s',$info['scheduling_addtime']);
			$info['scheduling_sign_time'] = date('d/m/y H:i:s',$info['scheduling_sign_time']);
			$info['scheduling_signout_time'] = date('d/m/y H:i:s',$info['scheduling_signout_time']);
			$info['scheduling_date'] = date('d/m/y H:i:s',$info['scheduling_date']);


		}
		
				
		return $info;
		
	}
	
	//计算在线时长
	private function deliver_line_time($startdate){
	
		$enddate = time();	
		
		$minute = floor(($enddate-$startdate) % 86400/60);
		
		return $minute;	
		
	}
	
	//获取他本身的坐标
	private function getDeliveryOne($deliver_id){
		
			$where = array(
				'distributor_id' => $deliver_id
			);
			
			$deliver_data = model('waiter')->getWaiterList($where);
			$feature = array();
			foreach($deliver_data as $k => $v){					
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(40,40);
					$feature[$k]['properties']['name'] = $v['distributor_name'];			
					$feature[$k]['properties']['id'] = $v['distributor_id'];
					$feature[$k]['properties']['iconUrl'] =  $this->deliver_icon($v['distributor_id'],1);//UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
					
					$feature[$k]['geometry']['type'] = 'Point';
					$deliver_coordinate = explode(',',$v['region_coordinate']);
					$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];			
			}
		
			$deliver_list = array();
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;		
			return $deliver_list;
	}
	
	
	
	//后去配送员列表
	public function get_deliveryOp(){
		
			
		//	$setting = Model('setting')->getListSetting();				
		//	$deliverSeting = unserialize($setting['map_show_waiterState']);	
				
			
			$deliverSeting = $_POST['mapState'];		
			$userScreen = $_POST['userScreen'];
			$schTag = $_POST['schTag'];
			$vioTag = $_POST['vioTag'];
				
			$deliver_ids = array();
			
			if(in_array('接单中',$deliverSeting)){		
				$where = array();
				$where['distributor_status'] = 1;	
				$where['delivery_state'] = 0 ;
				$where['distributor_online'] = 20;
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}
			}
			
			
			if(in_array('取餐中',$deliverSeting)){		
				$where = array();
				$where['distributor_status'] = 1;	
				$where['delivery_state'] = 1 ;
				$where['distributor_online'] = 20;
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}
			}
			
		
			
			if(in_array('配送中',$deliverSeting)){
				$where = array();
				$where['distributor_status'] = 1;
				$where['delivery_state'] = 2 ;
				$where['distributor_online'] = 20;
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}
			}
			
			if(in_array('空闲',$deliverSeting)){
				$where = array();
				$where['distributor_status'] = 1;
				$where['delivery_state'] = 3;
				$where['distributor_online'] = array('in','20,30');
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}				
			}
			
			if(in_array('有',$schTag)){
				$where = array();
				$where['scheduling_is'] = 1;
				$where['distributor_online'] = array('in','20,30');
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}				
			}
			
			if(in_array('无',$schTag)){
				$where = array();
				$where['scheduling_is'] = 0;
				$where['distributor_online'] = array('in','20,30');
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}				
			}
			
			if(in_array('有',$vioTag)){
				$where = array();
				$where['vehicle_lease_is'] = 1;
				$where['distributor_online'] = array('in','20,30');
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}				
			}
			
			if(in_array('无',$vioTag)){
				$where = array();
				$where['vehicle_lease_is'] = 0;
				$where['distributor_online'] = array('in','20,30');
				$ids = model('waiter')->getWaiterList($where,'distributor_id');
				foreach($ids as $v){
					$deliver_ids[] = $v['distributor_id'];
				}				
			}
			
			

			if($userScreen == 1){				
				$where = array(
					'distributor_id' => array('in',$deliver_ids)
				);			
			}else{
				$where = array(
					'distributor_status' => 1,
					'distributor_online' =>array('in','20,30')
				);			
			}			
			
			
			
			
			$deliver_data = model('waiter')->getWaiterList($where);
			
			$deliver = array();
			$state = array(
				'10' => '已下线',
				'20' => '在线中'
			);
			$deliveryState = array(
				0 => '等待接单',
				1 => '配送中'
			);
			
			$feature = array();
			foreach($deliver_data as $k => $v){	
				
				
				
				$deliver[$k]['distributor_id'] = $v['distributor_id'];			
				$deliver[$k]['distributor_name'] = $v['distributor_name'];
				
			//	$deliver[$k]['order_list'] =  $this->getDeliverOrder($v['distributor_id']);
				$deliver[$k]['distributor_state'] =  $state[$v['distributor_online']];
				
				$deliver[$k]['delivery_state_name'] =  $deliveryState[$v['delivery_state']];
				$deliver[$k]['delivery_state'] = $v['delivery_state'];
				$deliver[$k]['distributor_online'] = $v['distributor_online'];
				
				
				//推送过来订单。展示中的
				$show_order = $this->getStockSendOrder($v['distributor_id']);;				
				$deliver[$k]['distributor_show_order'] = $show_order;
								
				$deliver[$k]['order_time_num'] = $show_order ?  ($show_order['addtime'] - time() + 50)   : 'N/A';								
				//等待推送的订单
				$deliver[$k]['distributor_stock_order'] = $this->getStockShowOrder($v['distributor_id']);
				$feature[$k]['type'] = 'Feature';
				$feature[$k]['properties']['iconSize'] = array(40,40);
				$feature[$k]['properties']['name'] = $v['distributor_name'];			
				$feature[$k]['properties']['id'] = $v['distributor_id'];
				$feature[$k]['properties']['iconUrl'] = $this->deliver_icon($v['distributor_id']); // UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
					
				
				$feature[$k]['geometry']['type'] = 'Point';
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
				$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];
				
				
								
			}
		
		
			$deliver_list = array();
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;		
			output_data(array('deliver'=>$deliver,'deliver_list'=>$deliver_list));
		
	}
	
	//获取配送员现在手里的订单
	private function getDeliverOrder($deliver_id){
		
		$state = array(
				20 => '订单池',
				30 => '待商家接单',
				40 => '商家备餐中',
				50 => '配送中'
		);	
		
		
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);
		$order_data = model('order')->getOrderList($where);
		$order_list = array();
		$data = array();
		$feature = array();
		$store_feature = array();
		foreach($order_data as $k => $v){
		
			$order_list[] = $v['order_sn'];
			
			//用户坐标
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['order_sn'] = $v['order_sn'];
			$feature[$k]['properties']['order_id'] = $v['order_id'];
			
			$feature[$k]['properties']['order_state'] = $state[$v['order_state']];
			
			$feature[$k]['properties']['iconUrl'] = $this->user_icon($v['buyer_region_id'],$v['is_problem']);
			$feature[$k]['geometry']['type'] = 'Point';			
			
			$buy_coordinate = explode(',',$v['buyer_coordinate']);			
			$feature[$k]['geometry']['coordinates'][0] = $buy_coordinate[0];			
			$feature[$k]['geometry']['coordinates'][1] = $buy_coordinate[1];		
			
			
			
			//商家坐标
			$store_feature[$k]['type'] = 'Feature';
			$store_feature[$k]['properties']['iconSize'] = array(40,40);
			$store_feature[$k]['properties']['store_name'] = $v['store_name'];
			$store_feature[$k]['properties']['store_id'] = $v['store_id'];
			$store_feature[$k]['properties']['iconUrl'] = $this->store_icon($v['store_region_id']);
			$store_feature[$k]['geometry']['type'] = 'Point';			
			
			$store_coordinate = explode(',',$v['store_coordinate']);			
			$store_feature[$k]['geometry']['coordinates'][0] = $store_coordinate[0];			
			$store_feature[$k]['geometry']['coordinates'][1] = $store_coordinate[1];		
						
					
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		
		
		$store['type'] = 'FeatureCollection';
		$store['features'] = $store_feature;		
		
		
		
		return array('order_list'=>$data,'order_sn'=>$order_list,'store_list'=>$store);
		
//		output_data(array('order_list'=>$data));
		
	}
	
	
	
	
	
	//获取当前给他推送的订单
	private function getStockSendOrder($deliver_id){	
		
		$model = model('order_polling_show');				
		$where = array(
			'waiter_id' => $deliver_id		
		);		
		$info = $model->getShowInfo($where);		
		return $info;	
	}
		
	
	private function getStockShowOrder($deliver_id){
		
		
		$model = model('order_polling_show');				
		$where = array(
			'waiter_id' => $deliver_id		
		);		
		$info = $model->getShowInfo($where);			
		
		$model = model('order_polling');		
		$where = array();
		$where['waiter_id'] = $deliver_id;
		$where['send_state'] = 1;
		
		if(!empty($info)){
			$where['order_sn'] =array('not in ',$info['order_sn']);
		}
			
		
		$list = $model->getPollingList($where,'','order_sort desc,order_id asc');	
		
		
		
		
		return $list;			
	}
		
		
		
	
	
	//获取个个商家范围内的可用运力
	private function storeWaiter($latlng,$store_id){
					
		
		$lbs = new lbs();
		$result = $lbs->searchPoi('waiter', $latlng);		
		
		
		if($result['status'] == 0){
			//查询当前配送员是否有订单。
			$yunli_data = 2;
			$yunli = 0;		
			foreach($result['contents'] as $v){				
				$orderNum = $this->waiterOrderNum($v['waiter_id']);
				//$lock = $this->checkLock($v['waiter_id']);
				
				if($orderNum < $yunli_data){
					$yunli += $yunli_data - $orderNum;	
				}else{
					$yunli += 0;
				}
			}	
			return  $yunli ;
		}else{
			return 0 ;	
		}	
	}
	
	
	
	
	//判断当前配送员有几个单子。	
	private function waiterOrderNum($waiter_id){		
	   $count = 0;
		$where = array(
			'distributor_id' =>$waiter_id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	
	//获取区域内未分配的订单	
	public function ajaxOrderOp(){		
		$where = array();
		$where['order_state'] = 20;
		$where['store_region_id'] = $_POST['region_id'];		
		$order_list = model('order')->getOrderList($where, 50, '*', 'order_id desc');
		$data = array();
		foreach($order_list as  $k => $v){
			$data[$k] = $v;
			$data[$k]['order_estimate_time'] = date("H'I''",$v['order_estimate_time']);
		}		
		echo json_encode(array('code' => 200,'order_list'=>$data));		
	}
	
	
	
	
	
	
	//指派订单
	public function sendOrderOp(){

		$stoket =  model('stoket');	
		if (chksubmit()){	
			$waiter_id = $_POST['waiter_id'];
			$order_sn  = $_POST['order_sn'];
		    $stoket->sendAppoint($order_sn,$waiter_id);				
		    showDialog('已指发送指派订单', 'reload', 'succ');					
		}
		//获取当前所有可以用配送员		
		$where = array(
			'distributor_online' => 20
		);
		$waiter = model('waiter')->getWaiterList($where);		
		$data = array();
		foreach($waiter as  $k => $v){
			$data[$k] = $v;
			$data[$k]['distributor_avatar'] = getWaiterAvatar($v['distributor_avatar']);			
		}

		
		Tpl::output('waiter', $data);	
		Tpl::output('order_sn', $_GET['order_sn']);	
		Tpl::setDirquna('shop');
        Tpl::showpage('sorting_wariter','null_layout');
	}

	
	
	//热点地图
	public function hotmapOp(){
		
		
		
		$big_region_list = model('region')->getRegionList(array('region_parent_id'=> 0));
		$small_region = model('region')->getRegionList(array('region_type'=> 2));			
		
		
		
		$where['store_state'] = 1;
		$store = model('store')->getStoreList($where);
		foreach($store as $k => $v){			
			$data[$k]['store_id'] = $v['store_id'];
			$data[$k]['store_name'] = $v['store_name'];			
			$data[$k]['lat'] = $v['store_lat'];
			$data[$k]['lng'] = $v['store_lng'];
			$data[$k]['population'] = 3000;
			//$data[$k]['store_yunli'] = $this->storeWaiter($v['store_lng'].','.$v['store_lat'],$v['store_name']);
			//$data[$k]['store_color'] = $color[$k];	
		}
	
	
		
		Tpl::output('storeList', json_encode($data));			
		Tpl::output('regionList', json_encode($small_region));			
		Tpl::output('regionBigList', json_encode($big_region_list));			
		
	
		$list_setting = Model('setting')->getListSetting();
		
		
		$data = array();
		$data['enclosure'] = $list_setting['map_show_enclosure']; //围栏
		$data['largearea'] = $list_setting['map_show_largearea']; //大区
		$data['smallarea'] = $list_setting['map_show_smallarea']; //小区
		$data['colour'] = $list_setting['map_show_colour'];  //颜色
		$data['radius'] = $list_setting['map_show_enclosure_radius']; //围栏大小
		
		
		Tpl::output('mapConfig', json_encode($data));	
		
		
		$data = array();
		$order_data = $this->getMemberOrder();
		Tpl::output('order_data', json_encode($order_data));			
	
		
		Tpl::setDirquna('shop');
        Tpl::showpage('sorting.hot');	
		
		
	}
	
	//保存商家状态
	
	public function storeSettingOp(){	
		$model_setting = Model('setting');
		$data = array();
		$data['map_show_storeState'] = serialize($_POST['storeState']);			
		$result = $model_setting->updateSetting($data);  	
		if($result === true){
			output_data('操作成功');			
        }else {			
			output_error('操作失败');
        }				
	}
	
	//保存配送员状态
	
	public function waiterSettingOp(){
	
		$model_setting = Model('setting');
		$data = array();
		$data['map_show_waiterState'] = serialize($_POST['waiterState']);		
		
		$result = $model_setting->updateSetting($data);  	
		if($result === true){
			output_data('操作成功');
        }else {			
			output_error('操作失败');
        }				
		
	}
	
	//保存客户状态
	public function orderSettingOp(){
	
		$model_setting = Model('setting');
		$data = array();
		$data['map_show_orderState'] = serialize($_POST['orderState']);		
		
		$result = $model_setting->updateSetting($data);  	
		if($result === true){
			output_data('操作成功');
        }else {			
			output_error('操作失败');
        }				
		
	}
	
	//保存圈的状态
	
	public function map_settingOp(){
	
		$model_setting = Model('setting');
		$setting = array();
				
		$mapState = array(
			0 => array(
				'name'=>'显示内圈',
				'state' => in_array('显示内圈',$_POST['mapState']) ? 1 : 0 
			),
			1 => array(
				'name'=>'显示外圈',
				'state' => in_array('显示外圈',$_POST['mapState']) ? 1 : 0 
			),
			2 => array(
				'name'=>'显示小区',
				'state' => in_array('显示小区',$_POST['mapState']) ? 1 : 0 
			),
			3 => array(
				'name'=>'显示钱圈',
				'state' => in_array('显示钱圈',$_POST['mapState']) ? 1 : 0 
			),
		);		
		
		$data['map_show_mapState'] = serialize($mapState);		
		
		$result = $model_setting->updateSetting($data);  	
		if($result === true){
			output_data(array('checkMapState' =>$mapState ));
		
        }else {			
			output_error('保存失败');
        }				
		
	}
		
	
	
	
	//热点地图
	public function hotmapSettingOp(){		
		
	    
		$checkList = $_POST['checkList'];
		foreach($checkList as $v){
			$_POST[$v] = 1;
		}
		
		$model_setting = Model('setting');
		$data = array();


		$data['map_show_enclosure'] = $_POST['enclosure'] == 1 ? 1 : 0;
		$data['map_show_largearea'] = $_POST['largearea']  == 1 ? 1 : 0;
		$data['map_show_smallarea'] = $_POST['smallarea']  == 1 ? 1 : 0;
		$data['map_show_colour'] = $_POST['colour']  == 1 ? 1 : 0;
		$data['map_show_enclosure_radius'] = $_POST['radius'];		
		$result = $model_setting->updateSetting($data);      
		
	    if($result === true){		
			
				$list_setting = Model('setting')->getListSetting();
			    $data = array();
				$data['enclosure'] = $list_setting['map_show_enclosure']; //围栏
				$data['largearea'] = $list_setting['map_show_largearea']; //大区
				$data['smallarea'] = $list_setting['map_show_smallarea']; //小区
				$data['colour'] = $list_setting['map_show_colour'];  //颜色
				$data['radius'] = $list_setting['map_show_enclosure_radius']; //围栏大小							
		
			output_data(array('mapConfig'=> $data));
        }else {			
			output_error('操作失败');
        }				
	}
	
	
	
	
	
	
	
	
	
	
	
	//调用订单数据
	private function getMemberOrder(){
			
			$where = array();
			$order_list = model('order')->getOrderList($where, 50, 'order_id,buyer_coordinate', 'order_id desc');
			return $order_list;
			
	}
	
	
	//点击商家获取商家信息
	
	
	public  function getStoreInfoOp(){
		
	
		$where = array();
		$where['store_id'] = $_POST['store_id'];
		$store = model('store')->getStoreInfo($where);	
		if($store){	
			$data = array();
			$data['store_name'] = $store['store_name'];
			$data['store_en'] = $store['store_en_name'];
			$data['order_list'] =  array();
			$data['order_state1'] = 0;
			$data['order_state2'] = 0;
			$data['order_state3'] = 0;
			$data['order_yl'] = 0;
			$data['order_total'] = 0;
			$data['order_amount'] = 0;
			$data['store_id'] = $store['store_id'];
			$data['show'] = 1;
			output_data(array('store'=>$data));	
        }else {			
			output_error('操作失败');			
        }		
		
	}
	
	
	
	private function getDistance($lat1, $lng1, $lat2, $lng2) {
      
	    $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }

	//配送员排序条件
	
	
	private function deliverySort($deliver_id,$store_id){		
		//有两单
		$count = $this->deliverOrderCount($deliver_id);
		
		switch ($count) {		
		   case 2: 
				
				$where = array(
					'distributor_id' => $deliver_id,		
					'store_id' => $store_id,			
					'order_state' => array('in','30,40')				
				);
				
				$count = model('order')->getOrderCount($where);
				
				if($count  == 2){
				
					return  1 ;	
				
				}elseif($count == 1){
				
					return  2 ;
				
				}elseif($count == 0){
				
					return  4;
				
				}					
			 break;
		   case 1:		   
				$where = array(
					'distributor_id' => $deliver_id,	
					'store_id' => $store_id,						
					'order_state' => array('in','30,40')				
				);
				$count = model('order')->getOrderCount($where);
				//有一单
				if($count  == 1){
					return 3;					
				}elseif($count == 0){					
					return 5;
				}			
			 break;			 
		   case 0:			   	   
				return 6;									
		   break;		  
		}
	}

	
	
	private function deliverOrderCount($id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	//获取配送员可接的叠加商户
	private function getOrderSuperpositionStore($deliver_id,$store_id){
		
		$count = 0;
		
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		
		$order = model('order')->getOrderInfoN($where,'store_id','distributor_start_time desc');				
		$store_info = model('store')->getstoreInfo(array('store_id'=>$order['store_id']));
		if($order['store_id'] == $store_id){					
			return 1; //店铺本身.
		}
		
		if($store_info['store_superpose'] == $store_id){
			return 1;
		}
		
		//print_r($order);
		if(!empty($order)){		
			//直接读取当前商户是否可以叠加其他的店铺
			$store = explode(',',$store_info['store_superpose']);
			if(in_array($store_id,$store)){  			
				return 1;				
			}else{			
				return 2;				
			}
			
		}else{
		
			return 0; //没有单子
		}
		
	}
	
	
	//计算运力
	public function getDeliveryCount($deliver_id){

		$count = 3;				
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);		
		$order_count = model('order')->getOrderCount($where);
		
		
		return $count - $order_count;
	
	}
	
	
	
		//获取当前配送中，或者已接单的订
		
	private function getStoreOrder($store_id){
			
			$count = 0;
			$where = array(
				'store_id' => $store_id,
				'order_state' => array('in','30,40,50')
			);
			$list = model('order')->getOrderListNew($where,'*', 'order_id asc');
			$data = array();	
			$feature = array();
			$deliver = array();
			foreach($list as $k => $v ){	
				
				
					
				if($v['distributor_id'] > 0){
					//判断是否有骑手
		
						$deliver_info  = model('waiter')->getWaiterInfo(array('distributor_id'=> $v['distributor_id']));
						
						
						$deliver[$k]['type'] = 'Feature';
						$deliver[$k]['properties']['iconSize'] = array(40,40);
						$deliver[$k]['properties']['name'] = $v['distributor_name'];			
						$deliver[$k]['properties']['id'] = $v['distributor_id'];
						$deliver[$k]['properties']['iconUrl'] =  $this->deliver_icon($v['distributor_id']); //UPLOAD_SITE_URL.'/admin/allocation/couriers_busy_n.png';
						
						$deliver[$k]['geometry']['type'] = 'Point';
					
						$deliver_coordinate = explode(',',$deliver_info['region_coordinate']);
						
						$deliver[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
						$deliver[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];					
				}
				
				
				
				
				$feature[$k]['type'] = 'Feature';
				$feature[$k]['properties']['iconSize'] = array(40,40);
				$feature[$k]['properties']['order_sn'] = $v['order_sn'];
				$feature[$k]['properties']['order_id'] = $v['order_id'];
				$feature[$k]['properties']['iconUrl'] = $this->user_icon($v['buyer_region_id'],$v['is_problem']);
				$feature[$k]['geometry']['type'] = 'Point';			
				
				$buy_coordinate = explode(',',$v['buyer_coordinate']);			
				$feature[$k]['geometry']['coordinates'][0] = $buy_coordinate[0];			
				$feature[$k]['geometry']['coordinates'][1] = $buy_coordinate[1];		
				
							
				
			}
			$data['type'] = 'FeatureCollection';
			$data['features'] = $feature;	
			
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $deliver;
			return array('order_list'=> $data , 'deliver_list'=> $deliver_list);
	//		output_data(array('order_list'=>$data));
			
		}

		

		//钱圈围栏

		public function money_listOp(){

	
		
		
			$show_list = model('region')->getRegionList(array('is_money' => 1));
			foreach ($show_list as $v){
				$ids[] = $v['region_id'];
			}
			
			
			
			$data= array();		
			$region = model('region_sector')->getRegionList(array('region_money_id' => array('in',$ids)));		
		//	$region_data = array();
			$i = 0;
			foreach($region as $k => $v){			
				$feature[$k]['type'] = 'Feature';
				$feature[$k]['id'] = $v['region_id'];	
				$feature[$k]['properties']['color'] = $v['region_color'];
				$feature[$k]['properties']['name'] = $v['region_name'];			
				$coordinate = explode("|", $v['region_coordinate']);
				$geo = array();
				foreach($coordinate as  $kk => $vv){
					$geo_c = explode(",",$vv);
					$geo[$kk][0] = floatval($geo_c[0]);
					$geo[$kk][1] = floatval($geo_c[1]);				
				}		
				$feature[$k]['geometry']['type'] = 'Polygon';
				$feature[$k]['geometry']['coordinates'][0] = $geo;			
				
			//	$region_data[$v['region_id']] = $v;
				$i++;
			}
			
			
			
			
			
			
					
			$data['type'] = 'FeatureCollection';
			$data['features'] = $feature;
			
			$result['type'] = 'geojson';
			$result['data'] = $data;
			
			/*
			$region_data_list = model('region_sector')->getRegionList(array('region_enable'=> 1));		
			$region_data = array();
			foreach($region_data_list as $v){			
				$v['options'] = '';
				$region_data[$v['region_id']] = $v;			
			}*/
			
			output_data(array('sector_region'=>$result));
		}


}
