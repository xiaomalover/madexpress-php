<?php

/*
stripe
*/

defined('InMadExpress') or exit('Access Invalid!');
class stripeControl extends mobileAdminControl {
   
    private $stripe;
	
    public function __construct(){
        parent::__construct();		
		require_once(BASE_PATH.DS.'api'.DS.'stripe-php/init.php');		
		$this->stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');				
    }



	//获取账号的列表
	public function get_accountsOp(){				
		$list_data = $this->stripe->accounts->all(array('limit' => 10));	
		$data = json_decode(json_encode($list_data),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}						
		output_data($list);		
	}	

	
	//获取一个账号的详情信息
	public function get_accounts_infoOp(){	
		$id= $_POST['id']; 	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$result = $this->stripe->accounts->retrieve($id);
		output_data($result);			
	}	
	
	//不和stripe交互
	public function create_storeOp(){		
		$model = model('store_stripe');
		$data = array();		
		$data['store_id'] 	= $_POST['store_id'];		
		$data['name'] 		= $_POST['name'];	
		$data['phone'] 		= $_POST['phone'];	
		$data['address'] 	= $_POST['address'];	
		$data['email'] 		= $_POST['email'];	
		$data['tax_id'] 	= $_POST['tax_id'];	
		$data['vat_id'] 	= $_POST['vat_id'];	
		$data['type'] 		= $_POST['type'];	
		$data['country'] 	= $_POST['country'];
		$row =$model->addStoreStripe($data);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}



	//创建stripe主体账号
	public function create_accountsOp(){
		
		$data = array();
		$data['type'] = 'custom'; //账户类型，不能编辑
		$data['country'] = 'AU';  //国家，不能编辑
		$data['email'] = '470052365@qq.com'; //邮箱

		//需要开通的账号功能，可以选择包含以下方式。这两个默认添加即可
		$data['capabilities'] = array(
			'card_payments' => array('requested'=> true), 
			'transfers' =>  array('requested'=> true)
		);


		$data['business_type'] = 'company'; //公司类型

		//公司的信息
		$data['company'] = array(
			'address' => array(
				'city' => 'Melbourne', //城市
				'country' => 'AU',    //国家
				'line1'	=> '6 May Road',	//具体地址			
				'postal_code' => '3000',   //邮编
				'state' => 'VIC'    //州/省
			),			
			'name' => '商户',	//公司商户信息，姓名	 					
			'phone' => '+61459666011',//公司商户信息，电话	 				
			'tax_id' => '000111222',//公司商户信息，税务登记号码 			
			'vat_id' => '123456' //公司商户信息，增值税号
		);	
		
		$data['business_profile'] = array(
			'mcc' => '5734',   //公司所属行业，这里看是否要对接，还是固定一个分类id
			'url' => 'www.mad-express.com' //公司网站
		);
		$result = $this->stripe->accounts->create($data);
		output_data($result);	
	}
	
	
	
	
	//编辑账号时，一些字段是不能编辑的。如type，country，同上create
	public function update_accountsOp(){

		$id = $_POST['id'];	

		$data = array();
		$data['email'] = '470052365@qq.com';
		$data['capabilities'] = array(
			'card_payments' => array('requested'=> true),
			'transfers' =>  array('requested'=> true)
		);
		$data['business_type'] = 'company';
		$data['company'] = array(
			'address' => array(
				'city' => 'Melbourne',
				'country' => 'AU',
				'line1'	=> '6 May Road',				
				'postal_code' => '3000',
				'state' => 'VIC'
			),			
			'name' => '商户',							
			'phone' => '+61459666011',	
			'tax_id' => '000111222',
			'vat_id' => '123456'
		);			
		$data['business_profile'] = array(
			'mcc' => '5734',
			'url' => 'www.mad-express.com'
		);

		$result = $this->stripe->accounts->update($id,$data);
		output_data($result);	
	}
	

	//删除账号
	public function del_accountsOp(){		
		$id = $_POST['id'];	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$result  = $this->stripe->accounts->delete($id);
		output_data(array('list'=>$result ));		
	}
	
	


	/*
		银行账号接口相关
	*/
	//获得银行账号
	public function get_bank_accountOp(){
		$id= $_POST['id']; 	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$data = array();
		$data['object'] = "bank_account"; 
		$data['limit'] = 10;
		$result = $this->stripe->accounts->allExternalAccounts($id,$data);
		$data = json_decode(json_encode($result),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}						
		output_data(array('list'=>$list ));			
	}


	//获得银行账号
	public function get_bank_account_infoOp(){
		$id= $_POST['id'];  	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$bank_id = $_POST['bank_id'];  //银行账户与借记卡id。类似：ba_1ITjSx2eZqi9GPKxMTL8LkYh
		$result = $this->stripe->accounts->retrieveExternalAccount($id,$bank_id);
		output_data($result);			
	}



	//添加银行账号	
	public function create_bank_accountOp(){	
		$id = $_POST['id'];		//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$data = array();
		$data['external_account'] = array(
			'object' => 'bank_account', 
			'country' => $_POST['country'],//国家  'AU',
			'currency' => $_POST['currency'] , //货币   'AUD',
			'routing_number' => $_POST['routing_number'],   //路径号码 测试版必须是测试的，如：110-000
			'account_number' => $_POST['account_number']    //账号，测试版必须是测试的，如：111111-116
		);				
				
		$result = $this->stripe->accounts->createExternalAccount($id,$data);
		output_data($result);			
	}

	//编辑银行账号信息
	public function update_bank_accountOp(){
		$id = $_POST['id'];	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$bank_id = $_POST['bank_id'];	 //银行账户与借记卡id。类似：ba_1ITjSx2eZqi9GPKxMTL8LkYh

		$data = array();

		//这里同stripe后台一致，只能编辑默认卡，不能编辑详细内容，true或false
		$data['default_for_currency'] = true;   

		$result = $this->stripe->accounts->updateExternalAccount($id,$bank_id,$data);
		output_data($result);				
	}
	
	//删除银行账号信息
	public function del_bank_accountOp(){			
		$id = $_POST['id'];		 //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$bank_id = $_POST['bank_id'];  //银行账户与借记卡id。类似：ba_1ITjSx2eZqi9GPKxMTL8LkYh
		$result = $this->stripe->accounts->deleteExternalAccount($id,$bank_id);		
		output_data($result);			
	}
	
	
	

	/*
		负责人管理接口相关
	*/

	// 本账号下所有的负责人列表
	public function get_personsOp(){
		$id = $_POST['id'];   //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$list_data = $this->stripe->accounts->allPersons($id,array('limit' => 10));
		$data = json_decode(json_encode($list_data),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}					
		output_data($list);	
	} 

	//本账号某一个负责人详情
	public function get_persons_infoOp(){
		$id = $_POST['id'];    //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];  //所有者的id 如：person_J5W2ya4TLzOeX6
		$result = $this->stripe->accounts->retrievePerson($id,$persons_id);
		output_data($result);		
	}

	//上传身份证信息
	public function upload_identity_documentOp(){

		//1.jpg为本服器的实际路径，这个接口必须在外网测试，本地有异常
		$fp = fopen('1.jpg', 'r');
		
		$data = array();
		$data['purpose'] ='identity_document';  //正反身份证标记
		$data['file'] = $fp;
		
		$result = $this->stripe->files->create($data);
		output_data($result);		
	}
	
	
	//添加负责人信息
	public function create_personOp(){

		$id = $_POST['id'];  //主体账号id。类似：acct_1IT0XY2clnzANjLz
		
		$data = array();
		$data['address']  = array(
			'city' 		=> '南京',  //城市
			'country' 	=> 'HK',    //国际
			'line1' 	=> '江宁万达', //具体地址1
			'line2' 	=> '10号楼', //具体地址2
			'state' 	=> 'Hong Kong' //州/省
		);		
		$data['dob'] = array(
			'day' 	=>	'20',	//生日的日期		
			'month' =>	'02',  	//生日的月份	
			'year' 	=> 	'1989'  //生日的年份	
		);		
		$data['email']  	= 'test@qq.com'; //负责人
		$data['first_name'] = '高';    //名
		$data['last_name'] 	= '敏';	  //姓
		$data['verification']=array(
			'document' 	=>	array(
				"back"=>"file_1ITLBIGlMTtpJfUALUvweG2R", //身份证，背面，这里是接口upload_identity_document返回的
				"front"=>"file_1ITLBaGlMTtpJfUAHvVVlCeY",//身份证，正面这两个不能相同
			 ),			
		);
		//注意身份证图片，两个不能相同

		$result = $this->stripe->accounts->createPerson($id,$data);
		output_data($result);		
	}
		
		

	//编辑某一个负责人信息
	public function update_personOp(){
		
		$id 		= $_POST['id'];   //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];    //所有者的id 如：person_J5W2ya4TLzOeX6
		
		$data = array();
		$data['address']  = array(
			'city' 		=> '南京',
			'country' 	=> 'HK',
			'line1' 	=> '江宁万达',
			'line2' 	=> '10号楼',
			'state' 	=> 'Hong Kong'
		);		
		$data['dob'] = array(
			'day' 	=>	'20',			
			'month' =>	'02',
			'year' 	=> 	'1989'
		);		
		$data['email']  	= 'test@qq.com';
		$data['first_name'] = '高';
		$data['last_name'] 	= '敏';
		$data['verification']=array(
			'document' 	=>	array(
				"back"=>"file_1ITKIsGlMTtpJfUA3FALS975",
				"front"=>"file_1ITKzpGlMTtpJfUAlOwNk6zq",
			 ),			
		);
		//注意身份证图片，两个不能相同
		
		$result = $this->stripe->accounts->updatePerson($id,$persons_id,$data);
		output_data($result);		
	}
	

	//删除某一个负责人信息
	public function del_personOp(){		
		$id = $_POST['id'];    //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];			  //所有者的id 如：person_J5W2ya4TLzOeX6	
		$result = $this->stripe->accounts->deletePerson($id,$persons_id);
	    output_data($result);			
	}

}