<?php
/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class store_order_giveControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();      
    }

	
	
	


	
	//记录替换商品
	public function save_giveOp(){
		
	
		$goods_code = $_POST['goods_code'];
		$quantity = intval($_POST['quantity']);		
		$order_id = $_POST['order_id'];
		if(!$goods_code || $quantity <= 0) {
		    output_error('参数错误');
		}		
		//获取历史单数量
		$version_num = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
		
	
		$model_goods = Model('goods');
		$model_size = model('store_goods_size');
		$model_specs = model('store_goods_specs');
		$model_options = model('store_goods_options');
		
		$goods_info = $model_goods->getGoodsInfo(array('goods_code'=> $goods_code,'is_old' => 0));
		
	
		$order_info = model('order')->getOrderInfoN(['order_id' => $_POST['order_id']]);

		$_POST['goods_lang'] = $order_info['menu_lang'];

		$checkSpec = $this->getLangGoods($goods_code,$_POST['goods_lang']);

		$goods_size_price = 0 ;
		$meal_price = 0;
			
		if($goods_info['goods_is_set_meal'] == 0 ){
					//print_r(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
				//新规格
				if(!empty($_POST['goods_size'])){			
					$size_row = $model_size->getSizeInfo(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
					if(empty($size_row)){				
						output_error('尺寸不存在');
					}
					if($size_row['size_sale_price'] > 0){			
						$goods_size_price = $size_row['size_sale_price'];
						$goods_original_price = $size_row['size_price'];				
					}else{
						
						$goods_size_price = $size_row['size_price'];
						$goods_original_price = 0;			
					}
							
					$size_name = $size_row['size_value'];
					$box_id = $size_row['size_box_id'];
				}else{			
					output_error('尺寸不存在');			
				}
								
								
								
				//新规格 ,入参可能为多个specsid
				$spec_price = 0;
				$spec_name = array();
				if(!empty($_POST['goods_spec'])){			
					$specs_row = $model_specs->getSpecsList(array('specs_id' => array('in',$_POST['goods_spec']),'lang' =>$_POST['goods_lang'],'is_old' => 0 ));			
					foreach($specs_row as $v){
						$spec_price += $v['specs_price'];
						$spec_name[] = $v['specs_value'];								
					}			
					$spec_name = implode('/',$spec_name);			
				}	
					
			
					//新配件
					$optional_price = 0;
					if(!empty($_POST['goods_optional'])){
						$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
						foreach($options_row as $v){				
							$optional_price += $v['options_price'];
						}
					}
					
					
					$goods_price =  $goods_size_price + $optional_price + $spec_price  ; //折扣价
					$goods_original_price = $goods_original_price + $optional_price + $spec_price  ;	 //原价
								
				

		}else{

			//新配件
			$optional_price = 0;
			if(!empty($_POST['goods_optional'])){
				$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
				foreach($options_row as $v){				
					$optional_price += $v['options_price'];
				}
			}
			
			$goods_price = 0;

			$model = model('store_goods_meal');		
			$where = array();
	
			$where['is_old'] = 0;
			$where['lang'] = $_POST['goods_lang'];
			$where['meal_id'] = array('in',$_POST['meal_spec']);
			$list = $model->getMealList($where);
			$data = array();
			foreach($list as $k => $v){

				 $goods_price +=$v['meal_price'];

			}				
	
				  
			$goods_size_price = 0;
			$spec_price = 0;
			$goods_original_price = 0;


		}

		
		
		
		//验证是否可以购买
		if(empty($goods_info)) {
		    output_error('商品已下架或不存在');
		}
		       
		if(intval($goods_info['goods_stock']) < 1 || intval($goods_info['goods_stock']) < $quantity) {
		    output_error('库存不足');
		}
		
		if(!empty($goods_info['goods_image'])){
			$data = explode(",",$goods_info['goods_image']);
			$image = $data[0];
		}else{
			$image = '';	
		}
			  	
		  		
		$param = array();		
		$param['store_id']  = $checkSpec['store_id'];
		$param['goods_id']  = $checkSpec['goods_id'];
		$param['goods_name'] = $checkSpec['goods_name'];
		$param['goods_price'] = 0;// $goods_price;
		$param['goods_image'] = $image;
		$param['buyer_id'] = $goods_info['buyer_id'];
		
		$param['goods_spec'] = 		$spec_name;
		$param['goods_spec_price'] 	= 0; // ncPriceFormat($spec_price);
		$param['goods_size'] 		= $size_name;
		$param['goods_size_price'] 	= 0; //ncPriceFormat($goods_size_price);		
		$param['goods_optional'] 	= $_POST['goods_optional'] ? serialize($_POST['goods_optional']) : serialize(array());			
		$param['goods_optional_price'] = ncPriceFormat($optional_price);
		
		
		
		//$param['goods_comment'] = $_POST['goods_comment'];		
		$param['goods_lang'] = $_POST['goods_lang'];
		$param['goods_class_id'] = $goods_info['gc_id'];		
		$param['order_id'] = $_POST['order_id'];						
		//$param['rec_id'] = $_POST['rec_id'];		
		$param['goods_num'] = $quantity;
			
		$param['give_type'] = $_POST['give_type']? $_POST['give_type'] : 0;			
			
		$param['goods_size_id'] = $_POST['goods_size'];	
		$param['goods_specs_id'] = $_POST['goods_spec'] != null ? implode(',',$_POST['goods_spec']) : '';	
		$param['goods_code'] = $_POST['goods_code'];							
		$param['goods_optional_ids'] = $_POST['goods_optional_ids'];
		$param['version'] = $version_num +2;

		
		if($goods_info['goods_is_set_meal'] == 1){
			$param['set_meal'] = 1;
			$param['set_meal_spec'] = implode(',',$_POST['meal_spec']);
		}



		$condition = array();		
		$condition['goods_code'] = $goods_code;
		//$condition['rec_id'] = $_POST['rec_id'];			
		$condition['order_id'] = $_POST['order_id'];
		$condition['goods_size_id'] = $param['goods_size_id'];  			
		if(!empty($param['goods_specs_id'])){
			$condition['goods_specs_id'] = $param['goods_specs_id'];  	
		}
		
		if(!empty($param['goods_optional_ids'])){
			$condition['goods_optional_ids'] = $param['goods_optional_ids'];  	
		}
				
	   // print_r($param);
		//print_r($condition);		
							
		$check_cart = $this->check_cart($condition);		
    //	print_r($check_cart);
		

		if(!empty($check_cart)){			
			
			
			
			$update = array();
			$update['goods_num'] = $check_cart['goods_num'] + $quantity;						
			$where = array();
			$where['rec_id'] =  $check_cart['rec_id'];
			$row = model('order_goods_give')->editGive($where,$update);

			model('order_goods_edit')->editGoods(['rec_id' => $check_cart['rec_id'],'type' => 'give'],['rec_num' => $check_cart['goods_num'] + $quantity]);
		


		}else{			

			$row = model('order_goods_give')->addGive($param);
			
			$data = array();
			$data['rec_id'] 	= $row;
			$data['rec_num'] 	= $quantity;
			$data['type'] 		= 'give';
			$data['order_id'] 	= $_POST['order_id'];				
		//	print_r($data);
			model('order_goods_edit')->addGoods($data);


		}
		
		if($row){			
			output_data('ok');			
		}else{
			output_error('保存失败');
		}
	}
	
	
	
	
		
	private function goodsOptional($ids){			
			$where = array();
			$where['options_id'] = array('in',$ids);
			$where['state'] = 0 ;
			$where['store_id'] = $this->store_info['store_id'];	
			$list = model('goods_options')->getOptionsList($where);		
			$optional = array();
			foreach($list as  $k=>$v){
				$optional[$k]['id'] = $v['options_id'];
				$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$this->store_info['store_id']);
				$optional[$k]['price'] = $v['options_price'];
			}
			return $optional;
	}
		
		
		//格式化配件商品的语言包
		private function optionsLang($data,$storeId){			
		    $langInfo = model('language')->getLangInfo(array('language_id'=>$this->store_info['store_default_lang']));
			$data = unserialize($data);
			foreach($data as $k => $v){
				$list[$v['lang_name']] = $v['options_name'];
			}	
			return $list[$langInfo['language_flag']];		
		}
	
	
		//获取当前值的KEY	
		private function find_by_array_spec($array,$find)
		{
			 foreach ($array as $key => $v)
			{
				if($v['value']==$find)
				{
					return $key;
				}
			}
		}
		
		
		private function find_by_array_size($array,$find)
		{
			foreach ($array as $key => $v)
			{
				if($v['name']==$find)
				{
					return $key;
				}
			}
		}
	
	
	
	
	
	//验证是否存在
	private function check_cart($where){
		
		//	print_r($where);

			$info  = model('order_goods_give')->getGiveInfo($where);		
		
		//	print_r($info);
			return $info;
	}
	
	
	
	
	/**
	 * 更新数量
	 */
	public function giveNumOp() {
	    
	    
	 //  print_r(234);
	    
	    $rec_id = intval($_POST['rec_id']);
	    $quantity = $_POST['quantity'];
	    
		$store_id = $this->store_info['store_id'];
		$order_id = intval($_POST['order_id']);
		
	     if(empty($rec_id) ) {
	        output_error('参数错误');
	    }
	
	    $model_give = Model('order_goods_give');	
           $where = array();
    	   $where['rec_id'] = $rec_id;
    	   $where['order_id'] = $order_id;
    	   $where['store_id'] = $store_id; 
    	   
	    if($quantity > 0 ){
	        
    	   $data = array();
    	   $data['goods_num'] = $quantity;	
    	   $row = $model_give->editGive($where,$data); //$this->editGive($data, array('rec_id'=>$rec_id,'order_id' => $order_id));
	    }else{
	        $row = $model_give->delGive($where);
	    }
	    if($row) {	
	        output_data('修改成功');
	    } else {
	        output_error('修改失败或商品不存在');
	    }
	    
	    
	    
	}
	
	
	//更改数量
	private function editGive($data,$where){
		
		
		
		$model_give = Model('order_goods_give');
		
		$row = $model_give->editGive($where,$data);
        
        return $row;
		
		
	}
	
	
	//删除所有赠送商品
	public function delGiveOp(){		
	
		$order_id = intval($_POST['order_id']);		
		
		$where = array();
		$where['order_id'] = $order_id;
		$where['store_id'] = $this->store_info['store_id'];
		if(intval($_POST['rec_id']) > 0){
			$where['rec_id'] = intval($_POST['rec_id']);
		}

		$row = model('order_goods_give')->delGive($where);		

		if($row){
			model('order_goods_edit')->delGoods(['rec_id' => $_POST['rec_id']]);
			output_data('ok');			
		}else{
			output_error('失败');			
		}		
	}
	
	
	
	//校验传过来的属性和规格是否正确
	
	private function getLangGoods($goods_code,$lang_name){
		
		$data = array();
		$where = array(
			'goods_code' => $goods_code,
			'lang_name' => $lang_name
		);
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);
		
		
		$data = $goods_lang;	 
		
		return $data;
			
		
	}
	
	
	
}
