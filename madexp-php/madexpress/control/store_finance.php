<?php
/**
 * 商家财务
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_financeControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	//财务基本信息
	public function finance_baseOp(){
				
		$data = array();
		$data['wallet_money'] =  $this->store_info['available_predeposit'];
		$data['wallet_freeze'] = $this->store_info['freeze_predeposit'];		
		$data['service_charge'] = '1%';
		
		//获取当前银行账户
		
		$bank_info = model('store_stripe_bank')->getStoreStripeBankInfo(['store_id' => $this->store_info['store_id']]);		
	
		if(!empty($bank_info)){
			$data['acctno'] = $bank_info['account_number'];
			$data['bsb'] =  $bank_info['routing_number'];
		}else{			
			$data['acctno'] = 'N/A';
			$data['bsb'] =  'N/A';
		}
				
		output_data($data);		
	}
	
	
	

	
	//钱包明细

	
	//申请提现
	public function cashAddOp(){
		
		
		$obj_validate = new Validate();
		$pdc_amount = abs(floatval($_POST['pdc_amount']));
		$validate_arr[] = array("input"=>$pdc_amount, "require"=>"true",'validator'=>'Compare','operator'=>'>=',"to"=>'0.01',"message"=>'提现金额不正确');
	//	$validate_arr[] = array("input"=>$_POST["pdc_bank_name"], "require"=>"true","message"=>'请输入收款银行');
		//$validate_arr[] = array("input"=>$_POST["pdc_bank_no"], "require"=>"true","message"=>'请输入收款账号');	
		$obj_validate -> validateparam = $validate_arr;
		$error = $obj_validate->validate();
		
			if ($error != ''){
				output_error($error);
			}
	
			$model_pd = Model('store_wallet');
			
			$pdc_fee =sprintf("%.2f",($pdc_amount * 0.01));
			//验证金额是否足够
			if (floatval($this->store_info['available_predeposit']) < $pdc_amount + ($pdc_fee)){
				output_error('余额不足以支付本次提现，其中包含提现手续费'.$pdc_fee);
			}
				
	
			$pdc_sn = $model_pd->makeSn();
			$data = array();
			$data['pdc_sn'] = $pdc_sn;
			$data['pdc_store_id'] = $this->store_info['store_id'];
			$data['pdc_store_name'] =  $this->store_info['store_name_primary'];
			$data['pdc_amount'] = $pdc_amount;			
		//	$data['pdc_bank_id'] = $_POST['bank_id'];
			$data['pdc_add_time'] = TIMESTAMP;
			$data['pdc_payment_state'] = 0;	
			$model_pd->addPdCash($data);						
			$row = $this->transfers($pdc_sn);
			if($row['code'] == 200){
				output_data(array('status'=>'ok'));
			}else{		
				output_error('提现失败');
			}
		}
		
		
	
	
	//向子账号划拨金额
	private function transfers($sn){
		
		$model_pd = Model('store_wallet');
		$inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'stripe/stripe.php';
		if(!is_file($inc_file)){
		    output_error('支付接口不存在');
		}
		require($inc_file);		
		$pd_info = $model_pd->getPdCashInfo(['pdc_sn' => $sn,'pdc_store_id' => $this->store_info['store_id']]);					
		$stripe_info = model('store_stripe')->getStoreStripeInfo(['store_id' => $this->store_info['store_id']]);
		
		if(!empty($stripe_info)){	
			
			\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');			
			$transfer = \Stripe\Transfer::create([
			  'amount' => $pd_info['pdc_amount'] * 100,
			  'currency' => 'aud',
			  'destination' => $stripe_info['stripe_id'],
			 // 'transfer_group' => '{ORDER10}',
			]);				
			if($transfer['id']){		
				$payout = \Stripe\Payout::create([
				  'amount' => $pd_info['pdc_amount'] * 100,
				  'currency' => 'aud',
				], [
				  'stripe_account' => $stripe_info['stripe_id'],
				]);
							
				$row = Model('store_wallet')->editPdCash(['stripe_sn' => $transfer['id'],'pdc_payment_time' => time(),'pdc_payment_state' => 1],['pdc_sn' => $sn,'pdc_store_id' => $this->store_info['store_id']]);								
				
				//扣款存款
				$data = array();
				$data['store_id'] = $this->store_info['store_id'];
				$data['store_name'] = $this->store_info['store_name_primary'];
				$data['amount'] = $pd_info['pdc_amount'];
				$data['order_sn'] = $pd_info['pdc_sn'];			
				$model_pd->changePd('withdraw',$data);	
				
				
				$pdc_fee =sprintf("%.2f",($pd_info['pdc_amount'] * 0.01));
				
				//扣除提现手续费
				$data = array();
				$data['store_id'] = $this->store_info['store_id'];
				$data['store_name'] = $this->store_info['store_name_primary'];
				$data['amount'] = $pdc_fee;
				$data['order_sn'] = $pd_info['pdc_sn'];
				$model_pd->changePd('withdraw_fee',$data);		
						
				
				
				//发送短消息
										
								
						
						
								
				return ['code' => 200];
			}else{				
				return ['code' => 400];
			}
		}else{
			
			return ['code' => 400];
			
		}
		
	}
	
	
	
	public function invoice_typeOp(){

		$where = array();
        $where['bill_role'] = 'Merchant';
        $where['bill_user'] = $_POST['code'];
        $list = model('bill_type')->getBillTypeList($where);
   
        output_data(array('list' => $list));
	}
	


	
	//票据列表
	public function invoice_listOp(){
		
		$model = model('bill');
		$condition = array();
		$condition['form_type|to_type'] = 1;
		$condition['from_id|to_id'] = $this->store_info['store_id'];		



		$condition['bill_type'] = $_POST['bill_type'];
		
	//	print_r($condition);
		$list = $model->getBillList($condition, '*', $this->page);
		$list_data = array();
		foreach($list as $k => $v){			
		
		
			
            //收方用户类型 0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			switch ($v['to_type']) {
				
				case 0 : 
					if($v['form_type'] == 1){

						$store = model('store')->getStoreInfo(['store_id' => $v['to_id']]);
						$v['to_name_primary'] = $store['store_name_primary'];
						$v['to_name_secondary'] = $store['store_name_secondary'];
						$v['link'] = 1;
					}

					if($v['form_type'] == 2){
						$delivery = model('waiter')->getWaiterInfo(['distributor_id' => $v['from_id']]);
						$v['to_name_primary'] = $delivery['distributor_name'];
						$v['link'] = 2;

					}

					

				break;
				case 1 : 

					$store = model('store')->getStoreInfo(['store_id' => $v['to_id']]);

					$v['to_name_primary'] = $store['store_name_primary'];
					$v['to_name_secondary'] = $store['store_name_secondary'];


				break;
				case 2 : 

					
					$delivery = model('waiter')->getWaiterInfo(['distributor_id' => $v['to_id']]);
					$v['to_name_primary'] = $delivery['distributor_name'];
					$v['link'] = 2;
					

				break;
				case 3 : 

					$member = model('member')->getMemberInfo(['member_id' => $v['to_id']]);
					$v['to_name_primary'] = $member['member_code'];


				break;

			}
		
		
		
			$v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];
			$v['date_of_issue'] = date('d/m/y',$v['date_of_issue']);

			$list_data[$k] = $v;			
		}
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list_data), mobile_page($page_count,$list_count));
	}
	
	
	
	
	
	
	
	
	
	
	//流水类型
	
	
	public function get_typeOp(){
		
		
		$where = array();
		$where['finance_user'] = 0;


		if($_POST['type'] == 'positive'){
			$where['finance_type'] = 0 ;
		}


		if($_POST['type'] == 'negative'){
			$where['finance_type'] = 1 ;
		}


		$positive = model('finance')->getFinanceList($where);				
		
		$list[0]['finance_project'] = 'ALL';
		$list[0]['finance_project_name'] = 'ALL';		

		foreach($positive as  $k => $v){
			$list[$k+1] = $v;		
		}		
			
		output_data(array('list' => $list));
				
	}
	
	
	//获取流水
	public function get_activitiesOp(){
		
		$model = model('store_wallet');
		
		if($_POST['type'] == 'ALL'){			
			
			//	$condition['lg_type'] = $_POST['type'];
				if($_POST['type_act'] != 'ALL'){	
					$condition['lg_type'] = $_POST['type_act'];
				}
		}else{
			
				$condition['lg_type_activities'] = $_POST['type'] == 'positive' ? 0 : 1;
				if($_POST['type_act'] != 'ALL' ){
					$condition['lg_type']  = $_POST['type_act'];
				}
		}
			
			  
		$wallet_list = $model->getPdLogList($condition, '*', $this->page, $order);
		
		$list = array();		
		
		foreach ($wallet_list as $v) {
			
		   			
			$v['lg_add_time'] =  date('d/m/Y',$v['lg_add_time']);		
			$list[] = $v;
		}		
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
		
	}
	
	
	
	
	
		
		
	private function status(){
			$data = array();
			$data[0] = '待审核';
			$data[1] = '已审核';
			$data[2] = '已驳回';
			return $data;
	}
		
		
		
		
	public function examine_stateOp(){
			
	
			$examine_id = $_POST['examine_id'];
			$type = $_POST['state']; //1 同意  2驳回
			
			
			if($type == 1){				
				$row = model('store_examine')->storeExamine($examine_id);
			}else{				
				$where = array(
					'store_id' => $this->store_info['store_id'],
					'examine_id' => $examine_id
				);
				$updata = array(
					'examine_state' => 2
				);						
				$row = model('store_examine')->editStoreExamine($updata,$where);
			}
			
			if($row){
				output_data('操作成功');				
			}else{
				output_error('操作失败');				
			}
			
		}
		
		
	
	
}
