<?php
/**
 * 商家管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class storeControl extends mobileAdminControl{
  
	

	public function __construct(){
        parent::__construct();   
		
		import('function.datehelper');
		
		$model_stat = Model('stat');
		$this->search_arr = $_POST;
		//$this->search_arr = $_GET;
		//处理搜索时间
		if ($this->search_arr['screen_type'] == 'day') {

		    $this->search_arr['search_type'] = 'day';
		    $this->search_arr['show_type'] = 'hour';
		    $this->search_arr['day']['search_time'] = date('Y-m-d', time());
		    $this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
	
		} elseif ($this->search_arr['screen_type'] == 'week') {
			$this->search_arr['search_type'] = 'week';
		    $this->search_arr['show_type'] = 'week';
		    $searchweek_weekarr = getWeek_SdateAndEdate(time());
		    $this->search_arr['week']['current_week'] = implode('|', $searchweek_weekarr);
	
		} elseif ($this->search_arr['screen_type'] == 'month') {
		   
			$this->search_arr['search_type'] = 'month';
		    $this->search_arr['show_type'] = 'day';
		    $this->search_arr['month']['current_year']= date('Y', time());
		    $this->search_arr['month']['current_month']= date('m', time());
			
		} elseif ($this->search_arr['screen_type'] == 'year') {
			
		    $this->search_arr['search_type'] = 'year';
		    $this->search_arr['show_type'] = 'month';
		    $this->search_arr['year']['current_year']= date('Y', time());
			
		}else{

			$this->search_arr['screen_type'] = 'day';
			$this->search_arr['show_type'] = 'hour';
			$this->search_arr['day']['search_time'] = date('Y-m-d', time());
			$this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
		}
		
		$searchtime_arr = $model_stat->getStarttimeAndEndtime($this->search_arr);

		$this->search_arr['stime'] = $searchtime_arr[0];
		$this->search_arr['etime'] = $searchtime_arr[1];
		
    }
	
	
	public function infoOp(){
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		 
		 
		$store_count = model('store')->getStoreCount(TRUE);		 
		$info = array();
		$info['store_num'] = intval($store_count);
		$info['store_num_icon'] = "top";//上升top ,下降bottom
		$info['store_num_per'] = "0%";
		
		
		$model_order = model('order');
		//日订单		
		$where  = array(
			'add_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime']))
		);			
		$order_day_count = $model_order->getOrderCount($where);
		
		$store_jun_order = $order_day_count / $store_count;		
		
		$info['store_count'] = intval($store_jun_order);
		$info['store_count_icon'] = "top";//上升top ,下降bottom
		$info['store_count_per'] = "0%";



		
		$order_day_amount = $model_order->getOrderInfoN($where,'SUM(order_amount) as amount');
		$info['store_price'] = ($order_day_amount['amount'] / $store_count);
		$info['store_price_icon'] = "top";//上升top ,下降bottom
		$info['store_price_per'] = "0%";
		
		
	
		
		$info['store_good'] = 0;
		$info['store_good_icon'] = "top";//上升top ,下降bottom
		$info['store_good_per'] = "0%";
		
		output_data($info);
		
	}


	//统计分类
	public function kpi_data_leftOp(){
		$type = $_POST['screen_type'] ? $_POST['screen_type'] : 'day';

		switch($type){
			case 'day' :
				$data_type = '日';
			break;
			case 'week' :
				$data_type = '周';
			break;
			case 'month' :
				$data_type = '月';
			break;
			case 'year' :
				$data_type = '年';
			break;

		}


		$data = array(
			array(
				'menu_name' => $data_type."商家总数",
				'menu_count' =>  $this->stat_store(),
				'menu_type' => 'stat_order',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."商家均单",
				'menu_count' =>  $this->stat_order_svg(),
				'menu_type' => 'stat_order_price',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."商家均销售",
				'menu_count' =>  $this->stat_store_svg_sales(),
				'menu_type' => 'stat_delivery_fee',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."商家总销售",
				'menu_count' =>  $this->stat_store_sales(),
				'menu_type' => 'stat_member',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."最受欢迎品类",
				'menu_count' =>  $this->stat_like_cate(),
				'menu_type' => 'stat_business',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."商家区域分布",
				'menu_count' =>  $this->stat_store_area(),
				'menu_type' => 'stat_store_svg',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."商家均时单",
				'menu_count' =>  $this->stat_store_time_svg(),
				'menu_type' => 'stat_delivery_svg',
				'tips' =>  "Min"
			),
			array(
				'menu_name' => $data_type."商家均出餐时间",
				'menu_count' =>  $this->stat_store_preparation(),
				'menu_type' => 'stat_view_num',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."今日订单王",
				'menu_count' =>  $this->stat_store_no1(),
				'menu_type' => 'stat_view_num',
				'tips' =>  ""
			)

		);


		output_data(array('menu' => $data));
	}


	//商家总数
	private function stat_store(){

		return 0;

	}

	//商家均单
	private function stat_order_svg(){

		return 0;

	}

	//商家均销售
	private function stat_store_svg_sales(){

		return 0;
	}

	private function stat_store_sales(){

		return 0;
	}

	//最受欢迎品类
	private function stat_like_cate(){

		return 0;
	}

	//区域分布
	private function stat_store_area(){

		return 0;
	}

	//商家均时单

	private function stat_store_time_svg(){

		return 0;

	}

	//均出餐时间
	private  function stat_store_preparation(){

		return 0;
	}

	//订单王
	private function stat_store_no1(){

		return 0;

	}



   	// 数据统计
	public function kpi_data_lineOp(){





		
	//	output_data(['chart_data' => ['data' =>$data['barChartData'],'type'=>'line' ], 'column_name' => $data['columnName']]);
	

	}











	private function storeState(){
		$data = array();
		$data[0] = '下架';
		$data[1] = '上线';
		$data[2] = '下线';
		return $data;
	}
	
	
	//大品类管理
	public function store_big_classOp(){
		
		$cuisine_list = model('category')->getGoodsClassList(array("cate_parent_id"=>0));
		$list = array();
		
		foreach($cuisine_list as $k => $v){
			$list[$k]['cate_id'] = $v['cate_id'];
			$list[$k]['cate_name'] = $v['cate_name'];
		}
		output_data($list);	
	}
	
	
		
	//品类管理
	public function store_classOp(){
		
		
		$cuisine_list = model('category')->getGoodsClassList(array('cate_parent_id' => 1));
		$list = array();
		
		$list[0]= array(
			'cate_name' => '全部',
			'child' => $this->getAllChildClass(1),
			'child_num' => count($this->getAllChildClass(1)),			
		);		
		
		foreach($cuisine_list as $k => $v){
			$list[$k+1] = $v;
			$list[$k+1]['child'] = $this->getSmallClass($v['cate_id']);
			$list[$k+1]['child_num'] = count($list[$k+1]['child']);
			$list[$k+1]['cate_icon'] = UPLOAD_SITE_URL."/icon/".$v['cate_icon'];
			$list[$k+1]['cuisine_lang'] = unserialize($v['cate_lang']);
			
		}		
		output_data($list);	
	}
	
	
	private function getAllChildClass($id){		
		$list  =  model('category')->getGoodsClassList(array('cate_parent_id' => $id));
		$ids = '';
		foreach($list as $v){
			$ids[] = $v['cate_id'];			
		}		
		$small_list =  model('category')->getGoodsClassList(array('cate_parent_id' => array('in',$ids)));
		foreach($small_list as $k=> $v){
			$small_lang = unserialize($v['cate_lang']);
			$v['small_lang'] = $small_lang;		
			$v['cate_icon'] = UPLOAD_SITE_URL."/icon/".$v['cate_icon'];
			$small_list[$k] = $v;
		}
		if(!empty($small_list)){			
			return $small_list;
		}else{
			return array();
		}
	}
	
	//子类
	private function getSmallClass($id){		
		$where = array();
		
		$small_list =  model('category')->getGoodsClassList(array('cate_parent_id' => $id));
		
		foreach($small_list as $k=> $v){
			$small_lang = unserialize($v['cate_lang']);
			$v['small_lang'] = $small_lang;		
			$v['cate_icon'] = UPLOAD_SITE_URL."/icon/".$v['cate_icon'];
			$small_list[$k] = $v;
		}
		
		return $small_list;
	}
	
	
	
	//调用语言配置
	private function get_system_lang(){
		
		$system_lang = model('language')->getLangList(array('language_system' => 1));		
		return $system_lang;
		
	}
	
	
	public function get_big_class_initOp(){		
		$lang = $this->get_system_lang();
		$info = array();
		
		foreach($lang as $k=> $v){
			$goods_lang[$k]['lang_name'] = $v['language_flag'];	
			$goods_lang[$k]['small_name'] = '';	
		}		
		$info['cate_id'] = 0;
		$info['cate_icon'] = '';		
		$info['cate_lang'] = $goods_lang;		
			
		output_data($info);
		
	}
	
	//大类详情
	public function get_big_class_infoOp(){
		
		
		$info = model('category')->getGoodsClassInfoById($_POST['id']);
		if(!empty($info)){			
			$info['cate_lang'] = unserialize($info['cate_lang']);
		}	
		$info['cate_icon'] = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/icon/".$info['cuisine_icon'];
		output_data(array('info' => $info));
	}
	
	//保存大类
	public function save_big_classOp(){
		
	
		
		$where = array();
		
		
		$base64_image = str_replace(' ', '+', $_POST["cuisine_icon"]);
		//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
		$img_url = '';//图片路径
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image, $result)){
			//定义图片储存文件目录
			$dir = '../data/upload/icon/';
			//定义文件名称
			$picname = date("his") . '_' . rand(10000, 99999);
			if (!is_dir($dir)){
				//如果不存在就创建该目录
				mkdir($dir,0777,true);  
			}  
			//获取图片后缀
			if($result[2] == 'jpeg'){
				$picdir=$picname.'.jpg';
			}else{
				$picdir=$picname.'.'.$result[2];
			}
			//图片名称
			$image_url = $dir.'/'.$picdir;
			//储存图片
			if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image)))){
				$img_url = $image_url;
				$img_url = str_replace("..","",$img_url);
			}
		}
		

		$lang = $_POST['cate_lang'];
		foreach($lang as $k => $v){
			$small_lang[$k]['lang_name'] = $v['lang_name'];
			$small_lang[$k]['small_name'] = $v['small_name'];			
		}
		
		$data = array();
		$data['cate_name'] = $small_lang[0]['small_name'];
		$data['cate_lang'] = serialize($small_lang);
		$data['cate_icon'] = $picdir;			
		$data['cate_level'] = 1;
		$data['cate_parent_id'] = 1;
		$data['cate_svg'] = '';
		
		if(intval($_POST['cate_id']) > 0){			
			$row = model('category')->editGoodsClass($data,['cate_id' => $_POST['cate_id']]);
		}else{
			$row = model('category')->addGoodsClass($data);
			
		}		
		
		
		
	}
	
	
	//添加数据
	public function get_small_class_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();
		
		foreach($lang as $k=> $v){
			$goods_lang[$k]['lang_name'] = $v['language_flag'];	
			$goods_lang[$k]['small_name'] = '';	
		}			
		
		$info['cate_id'] = 0;
		$info['cate_name'] = '';		
		$info['cate_icon'] = '';		
		$info['cate_parent_id'] = '';
		$info['cate_lang'] = $goods_lang;		
			
		output_data($info);
		
		
		
	}
	
	
	
	//获取小类的详情
	public function get_small_class_infoOp(){
		
	//	$where = array();
//		$where['cate_id'] = $_POST['id'];
		$info = model('category')->getGoodsClassInfoById($_POST['id']);
		if(empty($info)){			
			output_error('获取分类失败');
		}
		$info['cate_lang'] = unserialize($info['cate_lang']);		
		$info['cate_icon'] = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/icon/".$info['cate_icon'];		
		output_data($info);
	}
	



	//保存小类的数据
	public function save_small_classOp(){
		
		$where = array();
		

		$path = BASE_UPLOAD_PATH .DS. ATTACH_INGR .DS;
        $file_name = $this->base64_image_content($_POST['cate_icon'], $path);
		
	
		$lang = $_POST['cate_lang'];		
		foreach($lang as $k => $v){
			$small_lang[$k]['lang_name'] = $v['lang_name'];
			$small_lang[$k]['small_name'] = $v['small_name'];			
		}
		
		$data = array();
		$data['cate_name'] = $small_lang[0]['small_name'];
		$data['cate_lang'] = serialize($small_lang);
		$data['cate_icon'] = $file_name;			
		$data['cate_level'] = 2;
		$data['cate_parent_id'] = $_POST['cate_parent_id'];
		//$data['cate_svg'] = '';
		



		if(intval($_POST['cate_id']) > 0){			
			$row = model('category')->editGoodsClass($data,['cate_id' => $_POST['cate_id']]);
		}else{
			$row = model('category')->addGoodsClass($data);			
		}				


		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
			
		}
			
	}
	
	
	//删除功能
	
	public function del_classOp(){
		
		
		$row = model('category')->delGoodsClass(['cate_id' => $_POST['cate_id']]);
		if($row){
			output_data('删除成功');
		}else{
			output_error('删除失败');			
		}
		
		
	}
	
	
	
	
	
	
	//元素分类	
	public function ingr_class_listOp(){		
		$list = model('goods_attr')->getClassList(TRUE);
		$class_list = array();
		foreach($list as $v){
			$v['gc_lang'] = unserialize($v['gc_lang']);
			$v['child'] = $this->getIngrList($v['gc_id']);			
			$class_list[] = $v;
		}		
		
		output_data(array('list' => $class_list));
	}
	
	private function getIngrList($class_id){
		
		$ingr = model('goods_attr')->getGoodsAttrList(array('attr_class_id' => $class_id));		
	
		$ingr_data = array();		
		foreach($ingr as $k=> $v){
			if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);
				$ingr_data[$k]['attr_name'] 	= $new[0];
				$ingr_data[$k]['attr_icon'] 	= UPLOAD_SITE_URL.'/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/icon/'.$new[1];
			}else{
				$ingr_data[$k]['attr_name'] 	=  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] 	=   UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/icon/'.$v['attr_active_icon'];	
			}
			$ingr_data[$k]['active'] = 0;
			$ingr_data[$k]['attr_name_lang'] = unserialize($v['attr_name_lang']);			
			$ingr_data[$k]['attr_id'] = $v['attr_id'];
		}		
		
		return $ingr_data;
	}
	
	//元素分类
	public function ingr_class_infoOp(){
		
		$where = array();
		$where['gc_id'] = $_POST['gc_id'];
		
		$row = model('goods_attr')->getClassInfo($where);
		if($row){			
			$row['gc_lang'] = unserialize($row['gc_lang']);
			output_data(array('info' => $row));
		}else{
			output_error('获取失败');
		}
	
	}
	
	//元素分类保存
	public function ingr_class_saveOp(){
		
		$model = model('goods_attr');
		
		$data = array();
		$data['gc_name'] = $_POST['gc_name'];
		$data['gc_lang'] = serialize($_POST['gc_lang']);
		$data['gc_sort'] = $_POST['gc_sort'];	
			
		if(intval($_POST['gc_id']) > 0){			
			
			$where = array();
			$where['gc_id'] = $_POST['gc_id'];
			$row = $model->editClass($where,$data);
			
		}else{
			
			$row = $model->addClass($data);	
					
		}
		if($row){			
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	
	//元素分类删除
	public function ingr_class_delOp(){
		
		$where = array();
		$where['gc_id'] = $_POST['gc_id'];
		$row = model('goods_attr')->delClass($where);
		
		if($row){			
			output_data('删除成功');
		}else{
			output_error('删除失败');
		}		
	}
	
	
	//获取init 
	public function ingr_class_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();		
		$attr_lang = array();
		foreach($lang as $k=> $v){			
			$attr_lang[$k]['lang_name'] = $v['language_flag'];	
			$attr_lang[$k]['class_name'] = '';	
		}
		
		
		$data = array();
		$data['gc_name'] = '';
		$data['gc_lang'] = $attr_lang;
		$data['gc_sort'] = 0;
		
		output_data(array('info' => $data));
		
		
	}
	
	
	
	
	
	
	//元素列表
	public function ingr_listOp(){
		
		$ingr = model('goods_attr')->getGoodsAttrList(TRUE);
		
		$ingr_data = array();
		
		foreach($ingr as $k=> $v){
			
			if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);
				$ingr_data[$k]['attr_name'] = $new[0];
				$ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
			}else{
				$ingr_data[$k]['attr_name'] =  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/icon/'.$v['attr_active_icon'];	
			}
			$ingr_data[$k]['active'] = 0;			
			$ingr_data[$k]['attr_id'] = $v['attr_id'];
		}		
		output_data(array('list' => $ingr_data));		
	}
	
	//元素详情
	public function ingr_infoOp(){		
		$where = array();
		$where['attr_id'] = $_POST['id'];
		$row = model('goods_attr')->getGoodsAttrInfo($where,'*');
		if($row){
			
			$row['attr_icon'] =   ($row['attr_icon']) ? UPLOAD_SITE_URL.'/icon/'.$row['attr_icon'] : "";
			$row['attr_active_icon'] =   ($row['attr_active_icon']) ? UPLOAD_SITE_URL.'/icon/'.$row['attr_active_icon'] : "";

			$row['attr_name_lang'] = unserialize($row['attr_name_lang']);		

			output_data(array('info' => $row));			
		}else{
			output_error('无数据');
		}		
	}
	
	//元素保存
	public function ingr_saveOp(){
				
		
		$model = model('goods_attr');		
		$data = array();
		$data['attr_name'] 		= $_POST['attr_name_lang'][0]['attr_name'];
		//$data['attr_icon'] 		= $_POST['attr_icon'];
		


		//获取图像
		$path = BASE_UPLOAD_PATH .DS.  ATTACH_INGR .DS;


        $attr_icon = $this->base64_image_content($_POST['attr_icon'], $path);		
		$attr_active_icon =  $this->base64_image_content($_POST['attr_active_icon'], $path);
	
	
		$data['attr_active_icon'] 	= $attr_active_icon;
		$data['attr_icon'] 	= $attr_icon;


		


		$data['attr_sort']	 		= $_POST['attr_sort'];
		$data['is_show'] 			= $_POST['is_show'];
		//$data['disabled'] 			= $_POST['disabled'];
	//	$data['attr_code'] 			= $_POST['attr_code'];
		$data['attr_type'] 			= $_POST['attr_type'];
	//	$data['attr_content']		= $_POST['attr_content'];
		$data['attr_class_id'] 		= $_POST['attr_class_id'];
		

		$attr_class = $model->getClassInfo(array('gc_id' => $_POST['attr_class_id']));			
		
		$data['attr_class_name'] 	= $attr_class['gc_name'];		
		$data['attr_name_lang'] 	= serialize($_POST['attr_name_lang']);
		
		if(intval($_POST['attr_id'] > 0)){
			$where = array();
			$where['attr_id'] = $_POST['attr_id'];			
			$row = $model->editGoodsAttr($where,$data);			
		}else{
			$row = $model->addGoodsAttr($data);
		}
			

		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	//元素删除
	public function ingr_delOp(){
		
		$where = array();
		$where['attr_id'] = $_POST['id'];
		$row = model('goods_attr')->delGoodsAttr($where);
		if($row){			
			output_data('删除成功');
		}else{
			output_error('删除失败');
		}
		
	}
	
	public function ingr_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();		
		$attr_lang = array();
		foreach($lang as $k=> $v){
			
			$attr_lang[$k]['lang_name'] = $v['language_flag'];	
			$attr_lang[$k]['attr_name'] = '';	
			
		}
		
		$data['attr_name'] 			= '';
		$data['attr_icon'] 			= '';
		$data['attr_active_icon'] 	= '';
		$data['attr_sort'] 			= 0;
		$data['is_show'] 			= 0;
		$data['disabled'] 			= '';
		$data['attr_code'] 			='';
		$data['attr_type'] 			= '';
		$data['attr_content'] 		= '';
		$data['attr_class_id'] 		= '';
		$data['attr_class_name'] 	= '';
		$data['attr_name_lang'] 	= $attr_lang;	
		
		output_data(array('info' =>$data));	
		
	}
	
	
	
	
	
	
	
	
	
	public function store_listOp(){
		
		
		$model_store = Model('store');
		// 设置页码参数名称
		$condition = "";				 
	
		
		
		
		
		if ($_POST['state']) {			
			if($_POST['state'] == 'formal'){ //正常商家				

				$condition['store_state'] = 1;
				$condition['store_examine'] = 2;
				
			}			
			
			if($_POST['state'] == 'edit'){ //编辑中的
				$condition['store_examine'] = 0 ;
				
			}
			
			if($_POST['state'] == 'examine'){ //编辑中的
				$condition['store_examine'] = 1 ;				
			}		
		}

		//搜索
		if ($_POST['keyword'] != '') {
			$condition['store_name_primary|store_name_secondary|store_phone|store_code'] = array('like', '%' . $_POST['keyword'] . '%');			
		}

		//筛选	
		if(is_array($_POST['search_ing'])){
			$condition['order_state_peican'] = array('between',$_POST['search_ing']);
		}
		if(is_array($_POST['search_complate'])){
			$condition['order_state_wancheng'] = array('between',$_POST['search_complate']);
		}
		if(is_array($_POST['search_cancel'])){		
			$condition['order_state_quxiao'] = array('between',$_POST['search_cancel']);
		}

		
		$order = '';		
		$param = array('store_id','order_state_peican','order_state_wancheng','order_state_quxiao','store_score','order_state_avg');
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
		        $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
		}
		





	
		
		//店铺列表
		$store_list = $model_store->getStoreList($condition, $this->page, $order);		
		$page_count = $model_store->gettotalpage();
		$list_count = $model_store->gettotalnum();
		
		
		
		$data = array();
		$examine = array('编辑中','审核中','已通过');
		$gadmin = $this->admin_info['id'];
		
		$class_data = model('store_class')->getStoreClassList(TRUE);
		$storeState = $this->storeState();
		
		foreach($class_data as $v){
			$class_array[$v['class_id']] = $v;			
		}
		
		foreach ($store_list as $value) {
			$param = array();          
				
			$param['store_avatar'] =  $value['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$value['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');			
			$param['store_id'] = $value['store_id']; 	
			$param['store_name_primary'] = $value['store_name_primary']; 
			$param['store_name_secondary'] = $value['store_name_secondary']; 			
			
			$param['store_code'] = $value['store_code']; 		
			$param['admin_id'] = $gadmin == 1 ? 1 : $value['admin_id']; 
			$param['admin_name'] = $value['admin_name']; 
			$param['region_name'] = $value['region_name'];			
			$param['region_id'] = $value['region_id'];		
			$param['region_color'] = $value['region_color'];	
			$param['store_class_id'] = $value['store_class_id'];			
			$param['store_class_name'] = $class_array[$value['store_class_id']]['class_name'];			
			$param['store_phone'] = $value['store_phone'];
			$param['order_state_peican'] 	= $this->getStorePeican($value['store_id']);
			$param['order_state_wancheng'] 	= $this->getStoreSuccess($value['store_id']);
			$param['order_state_quxiao'] 	= $this->getStoreCancel($value['store_id']);
			$param['order_state_avg'] = $value['order_state_quxiao'];
			$param['store_score'] = $value['store_score'];	
			$param['store_state_name'] = $storeState[$value['store_state']];
				
			
			$param['store_state'] = $value['store_state'];		
			$param['store_examine'] = $value['store_examine'];		
			$param['store_examine_name'] = $examine[$value['store_examine']];		
		    $list[] = $param;
			
		}
		
		
		
		//统计店铺
		
		$store_one_num = $model_store->getStoreCount(array('store_state' => 1));
		$store_two_num = $model_store->getStoreCount(array('store_examine' => 0));
		$store_three_num = $model_store->getStoreCount(array('store_examine' => 1));
		
		
		
		
		output_data(array('list' => $list,'one_num' =>$store_one_num , 'two_num' =>$store_two_num,'three_num' => $store_three_num ), mobile_page($page_count,$list_count));		
		
		
	}
	
	
	//配餐中
	private function getStorePeican($store_id){		
		$count = model('order')->getOrderCount(['store_id' => $store_id,'order_state' => 40]);		
		return $count;
	}
	
	//已取消
	private function getStoreCancel($store_id){
		$count = model('order')->getOrderCount(['store_id' => $store_id,'order_state' => 0]);		
		return $count;		
	}
	
	
	
	//已完成		
	private function getStoreSuccess($store_id){
		$count = model('order')->getOrderCount(['store_id' => $store_id,'order_state' => 60]);		
		return $count;
		
	}

	

	
	//添加商户
	
	//搜索专员
	

	public function getMerchantSearchOp(){
		
		
		$where = "nickname like '%".$_POST['keyword']."%' or member_code  like '%".$_POST['keyword']."%'";
		
		$list = model('admin')->getAdminListNew($where);						
		
		output_data(array('list' => $list));
		
	}
	
	
	private function checkMemberName($memberName)
	{
	    // 判断store_joinin是否存在记录
	    $count = (int) Model('store')->getStoreCount(array(
	        'store_login_name' => $memberName,
	    ));
	    if ($count > 0){
	        return false;
		}
	    return true;
	}
	
	//生成编号
	
	private function store_code($store_code){		
		
		if($store_code == ''){
			$count = model('store')->getStoreCount(TRUE);
			$code = strtoupper('AUS01').str_pad($count + 1,4,0,STR_PAD_LEFT);
			return $code;
		}else{			
			return $store_code;
		}
		
	}
	
	
	//初始化新增店铺
	
	public function store_saveOp(){
		
		
			$model = model('store');			
			$store_code = $_POST['store_code'];
			if (!$this->checkMemberName($store_code)){				
				output_error('账号已被占用');				
			}
		
			$data = array(							
				'store_name_primary' => $_POST['store_name_primary'],
				'store_name_secondary' => $_POST['store_name_secondary'],
				'store_class_id' => $_POST['store_class_id'],
				'store_login_password' => md5($_POST['store_login_password']),
				'store_time'=>time(),
				'store_code' => $this->store_code($_POST['store_code']),
				'store_login_name' => $this->store_code($_POST['store_code']),			
				'store_shophours' => $this->getShopHours(),
				'store_estimated_time' => 'a:2:{s:10:"start_time";s:2:"12";s:8:"end_time";i:30;}'
			);
			
			
			if(!empty($_POST['admin_id'])){				
				$admin = model('admin')->getAdminInfoNew(array('admin_id' =>$_POST['admin_id']));					
				$data['admin_id'] = $admin['admin_id'];			
				$data['admin_name'] = $admin['nickname'];				
			}
				

			$store_id = $model->addStore($data);
			if($store_id){				
			
					$contract = array();
					$contract['store_id'] = $store_id;
					$contract['contract_rate'] = 10;
					$contract['addtime'] = time();
					$contract['is_default'] = 1;	
					model('store_contract')->addContract($contract);
					
					$license = array();
					$license['store_id'] = $store_id;
					$license['addtime'] = time();
					$license['is_default'] = 1;
					model('store_license')->addLicense($license);
					
					$hygiene = array();
					$hygiene['store_id'] = $store_id;
					$hygiene['addtime'] = time();
					$hygiene['is_default'] = 1;
					model('store_hygiene')->addHygiene($hygiene);
					
								
					
					output_data(array('store_id'=>$store_id));
			}else{
					output_error('操作失败');
	      	}
		
	}
	
	
	
	private function getShopHours(){
		
		return 'a:7:{i:0;a:4:{s:4:"name";s:9:"星期一";s:4:"code";s:3:"mon";s:2:"t1";a:3:{s:5:"stime";s:5:"08:00";s:5:"etime";s:5:"14:00";s:4:"show";i:1;}s:2:"t2";a:3:{s:5:"stime";s:5:"16:00";s:5:"etime";s:5:"22:00";s:4:"show";i:1;}}i:1;a:4:{s:4:"name";s:9:"星期二";s:4:"code";s:4:"tues";s:2:"t1";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}s:2:"t2";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}}i:2;a:4:{s:4:"name";s:9:"星期三";s:4:"code";s:3:"wed";s:2:"t1";a:3:{s:5:"stime";s:5:"08:00";s:5:"etime";s:5:"14:00";s:4:"show";i:1;}s:2:"t2";a:3:{s:5:"stime";s:5:"16:30";s:5:"etime";s:5:"22:00";s:4:"show";i:1;}}i:3;a:4:{s:4:"name";s:9:"星期四";s:4:"code";s:4:"thur";s:2:"t1";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}s:2:"t2";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}}i:4;a:4:{s:4:"name";s:9:"星期五";s:4:"code";s:3:"fri";s:2:"t1";a:3:{s:5:"stime";s:5:"08:00";s:5:"etime";s:5:"13:30";s:4:"show";i:1;}s:2:"t2";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}}i:5;a:4:{s:4:"name";s:9:"星期六";s:4:"code";s:3:"sat";s:2:"t1";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}s:2:"t2";a:3:{s:5:"stime";s:5:"16:00";s:5:"etime";s:5:"22:00";s:4:"show";i:1;}}i:6;a:4:{s:4:"name";s:9:"星期日";s:4:"code";s:3:"sun";s:2:"t1";a:3:{s:5:"stime";s:5:"09:00";s:5:"etime";s:5:"14:00";s:4:"show";i:1;}s:2:"t2";a:3:{s:5:"stime";s:0:"";s:5:"etime";s:0:"";s:4:"show";i:0;}}}';
		
		
	}
	



	public function store_stateOp(){

		$store_id = $_POST['store_id'];
		$store_state = $_POST['store_state'];

		$row = model('store')->editStore(['store_state' => $store_state],['store_id' => $store_id]);
		
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}

	}


	//上传图片
	private function base64_image_content($base64_image_content, $path)
    {
        //匹配出图片的格式
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)) {
            $type = $result[2];
            $new_file = $path;
            if (!file_exists($new_file)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($new_file, 0700);
            }
            $file_name = time().rand(10000, 99999).".{$type}";
            $new_file = $new_file.$file_name ;
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))) {
                return $file_name;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }





	
	
	public function store_tokenOp(){
		
		$model_store = Model('store');
		$store_info = $model_store->getStoreInfo(array('store_id' => $_POST['store_id']));   
		
		$token = $this->_get_token($store_info['store_id'], $store_info['store_login_name'], 'pc');
		if($token) {
		
			//	$member_token = $this->_get_member_token($member_info['member_id'], $member_info['member_name'], 'wap');
		 //   $memberinfo=array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $member_token);
			
			output_data(array('seller_name' => $store_info['store_login_name'], 'store_name_primary' => $store_info['store_name_primary'], 'token' => $token));
		} else {
		    output_error('登录失败');
		}
	}
	
	/**
	 * 登录生成token
	 */
	private function _get_token($seller_id, $seller_name, $client) {
	    $model_mb_seller_token = Model('mb_seller_token');
	
	    //重新登录后以前的令牌失效
	    $condition = array();
	    $condition['seller_id'] = $seller_id;
	    $model_mb_seller_token->delSellerToken($condition);
	
	    //生成新的token
	    $mb_seller_token_info = array();
	    $token = md5($seller_name. strval(TIMESTAMP) . strval(rand(0,999999)));
	    $mb_seller_token_info['seller_id'] = $seller_id;
	    $mb_seller_token_info['seller_name'] = $seller_name;
	    $mb_seller_token_info['token'] = $token;
	    $mb_seller_token_info['login_time'] = TIMESTAMP;
	    $mb_seller_token_info['client_type'] = $client;
		
		$mb_seller_token_info['is_admin'] = 1;
		$mb_seller_token_info['admin_id'] = $this->admin_info['admin_id'];
		$mb_seller_token_info['admin_type'] = 0; // 0 超管 1专员
		$mb_seller_token_info['admin_name'] = $this->admin_info['nickname'];
		
		
		
	
	    $result = $model_mb_seller_token->addSellerToken($mb_seller_token_info);
	
	    if($result) {
	        return $result;
	    } else {
	        return null;
	    }
	}
	
}
