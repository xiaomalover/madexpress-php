<?php
/**
 * mobile父类
 */

defined('InMadExpress') or exit('Access Invalid!');

/********************************** 前台control父类 **********************************************/

class mobileControl{
    //客户端类型
    protected $client_type_array = array('android', 'wap', 'wechat', 'ios', 'windows');
    //列表默认分页数
    protected $page = 5;

    public function __construct() {
        Language::read('mobile');
		$_POST = json_decode(file_get_contents('php://input'),true);
			
        //分页数处理
        $page = intval($_POST['page']);
        if($page > 0) {
            $this->page = $page;
        }
	
		
    }
}

class mobileHomeControl extends mobileControl{
    public function __construct() {
        parent::__construct();		
		
		$_POST = json_decode(file_get_contents('php://input'),true);
				
    }
}

class mobileAdminControl extends mobileControl{
	
    protected $admin_info = array();

    public function __construct() {
        parent::__construct();		
			
		$_POST = json_decode(file_get_contents('php://input'),true);
		
											
			$model_mb_admin_token = Model('mb_admin_token');			
			$key = $_SERVER['HTTP_X_TOKEN'];		
			if(empty($key)) {
				$key = $_POST['token'];
			}			
			$mb_admin_token_info = $model_mb_admin_token->getMbAdminTokenInfoByToken($key);		
			
			
			if(empty($mb_admin_token_info)) {				
				output_error('请登录', array('login' => '0'));				
			}
			$model_admin = Model('admin');        
			$this->admin_info  = $model_admin->getAdminInfoNew(array('admin_id'=>$mb_admin_token_info['admin_id']));		
			if(empty($this->admin_info)) {			
				output_error('请登录', array('login' => '0'));			
			} else {				
				$this->admin_info = $this->admin_info;
			}
      
    }
}


class mobileMavinControl extends mobileControl{
	
    protected $admin_info = array();

    public function __construct() {
        parent::__construct();		
			
		$_POST = json_decode(file_get_contents('php://input'),true);
		
											
			$model_mb_admin_token = Model('mb_mavin_token');			
			$key = $_SERVER['HTTP_X_TOKEN'];		
			if(empty($key)) {
				$key = $_POST['token'];
			}			
			$mb_admin_token_info = $model_mb_admin_token->getMbAdminTokenInfoByToken($key);		
			
			
			if(empty($mb_admin_token_info)) {				
				output_error('请登录', array('login' => '0'));				
			}
			$model_admin = Model('admin');        
			$this->admin_info  = $model_admin->getAdminInfoNew(array('admin_id'=>$mb_admin_token_info['admin_id']));		
			if(empty($this->admin_info)) {			
				output_error('请登录', array('login' => '0'));			
			} else {				
				$this->admin_info = $this->admin_info;
			}
      
    }
}









class mobileSellerControl extends mobileControl{
    protected $store_info = array();
    public function __construct() {
        parent::__construct();
		
		$_POST = json_decode(file_get_contents('php://input'),true);	
		
		
        $model_mb_seller_token = Model('mb_seller_token');
		$key = $_SERVER['HTTP_X_TOKEN'];	
		if(empty($key)) {
			$key = $_POST['token'];
			if(empty($key)){
				$key = $_GET['token'];
			}
		}		
		
        if(empty($key)) {
            output_error('请登录', array('login' => '0'));
        }

        $mb_seller_token_info = $model_mb_seller_token->getSellerTokenInfoByToken($key);
		
        if(empty($mb_seller_token_info)) {
            output_error('请登录', array('login' => '0'));
        }
 
        $model_store = Model('store');    			
		$this->store_info = $model_store->getStoreInfo(array('store_id' => $mb_seller_token_info['seller_id']));
		
		$model_admin = Model('admin');
		$this->admin_info  = $model_admin->getAdminInfoNew(array('admin_id'=>$mb_seller_token_info['admin_id']));	
	
		
       
    }
}


