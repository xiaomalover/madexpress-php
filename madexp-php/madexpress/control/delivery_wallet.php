<?php
/**
 * 会员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class delivery_walletControl extends mobileAdminControl
{
    const EXPORT_SIZE = 1000;

    public function __construct()
    {
        parent::__construct();
    }



    public function wallet_infoOp(){

    
        $where = array(
            'distributor_id' =>intval($_POST['delivery_id'])
        );
        $delivery_info = model('waiter')->getWaiterInfo($where);


        $data= array();
        $data['wallet_freeze'] = $delivery_info['freeze_predeposit'];
        $data['wallet_money'] =   $delivery_info['available_predeposit'];

        $common_info = model('waiter')->getDeliveryCommInfo(['delivery_id' => $_POST['delivery_id']]);


        $data['acctno'] = $common_info['delivery_abn'];
        $data['bsb'] = $common_info['delivery_bsb'];


        output_data($data);

    }


    public function get_typeOp(){

		$where = array();
		$where['finance_user'] = 1;


		if($_POST['type'] == 'positive'){
			$where['finance_type'] = 0 ;
		}


		if($_POST['type'] == 'negative'){
			$where['finance_type'] = 1 ;
		}


		$positive = model('finance')->getFinanceList($where);				
		
		$list[0]['finance_project'] = 'ALL';
		$list[0]['finance_project_name'] = 'ALL';		

		foreach($positive as  $k => $v){
			$list[$k+1] = $v;		
		}		
			
		output_data(array('list' => $list));

    }

    public function get_activitiesOp(){

        $model = model('waiter_wallet');
		
		if($_POST['type'] == 'ALL'){			
			
			//	$condition['lg_type'] = $_POST['type'];
				if($_POST['type_act'] != 'ALL'){	
					$condition['lg_type'] = $_POST['type_act'];
				}
		}else{
			
				$condition['lg_type_activities'] = $_POST['type'] == 'positive' ? 0 : 1;
				if($_POST['type_act'] != 'ALL' ){
					$condition['lg_type']  = $_POST['type_act'];
				}
		}
			
		$order = 'lg_id desc';			
			  
		$wallet_list = $model->getPdLogList($condition, '*', $this->page, $order);
		
		$list = array();		
		
		foreach ($wallet_list as $v) {
					   			
			$v['lg_add_time'] =  date('d/m/Y',$v['lg_add_time']);		
			$list[] = $v;
		}		
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));

    }


    //票据类型
    public function invoice_typeOp(){


        $where = array();
        $where['bill_role'] = 'Courier';
        $where['bill_user'] = $_POST['code'];
        $list = model('bill_type')->getBillTypeList($where);

		$type_list[0]['bill_type_name'] = 'ALL';
		$type_list[0]['bill_type_code'] = 'ALL';
		foreach($list as $k => $v){
			$type_list[$k+1] = $v;
		}
   
        output_data(array('list' => $type_list));


    }




    //票据列表
    public function invoice_listOp(){

        $model = model('bill');
		$condition = array();
		$condition['form_type|to_type'] = 2;
		$condition['from_id|to_id'] = $_POST['delivery_id'];
		
		if($_POST['invoice_code'] == 'customer' ){

			if($_POST['bill_type'] != 'ALL'){
				$condition['bill_type'] = $_POST['bill_type'];
			}else{

				$condition['bill_type'] = array('in','delivery_invoice,delivery_credit_note');
			}

		}

		if($_POST['invoice_code'] == 'platform' ){

			if($_POST['bill_type'] != 'ALL'){
				$condition['bill_type'] = $_POST['bill_type'];
			}else{
				$condition['bill_type'] = array('in','delivery_fee_reimbursement_invoice,incentive_invoice,incentive_credit_note,me_platform_invoice,me_platform_credit_note,liquidated_damage_recipt,tax_summary');
			}

		}


		if($_POST['invoice_code'] == 'others' ){

			if($_POST['bill_type'] != 'ALL'){
				$condition['bill_type'] = $_POST['bill_type'];
			}else{
				$condition['bill_type'] = 'online_sales_invoice';
			}

		}




		
       // print_r($condition);
		$list = $model->getBillList($condition, '*', $this->page);
		$list_data = array();
		foreach($list as $k => $v){			

            //收方用户类型 0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			switch ($v['to_type']) {
				
				case 0 : 


				break;
				case 1 : 

					$store = model('store')->getStoreInfo(['store_id' => $v['to_id']]);

					$v['to_name_primary'] = $store['store_name_primaray'];
					$v['to_name_secondary'] = $store['store_name_secondary'];


				break;
				case 2 : 
        
                   


				break;
				case 3 : 
                    $member = model('member')->getMemberInfo(['member_id' => $v['to_id']]);
					$v['to_name_primary'] = $member['member_code'];


				break;

			}

            $v['date_of_issue'] = date('d/m/y',$v['date_of_issue']);

            $v['bill_pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];
			$list_data[$k] = $v;			
		}
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list_data), mobile_page($page_count,$list_count));



    }





}
