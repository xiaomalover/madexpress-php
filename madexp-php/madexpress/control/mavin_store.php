<?php
/**
 * 车辆管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class mavin_storeControl extends mobileMavinControl{
  
	

	public function __construct(){
        parent::__construct();   
    }

	public function infoOp(){
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		 
		$store_count = model('store')->getStoreCount(array('admin_id' =>$this->admin_info['admin_id']));
	
		$info = array();
		$info['store_num'] = $store_count ;
		$info['store_num_icon'] = "bottom";//上升top ,下降bottom
		$info['store_num_per'] = "0%";
		
		$info['store_count'] = 0;
		$info['store_count_icon'] = "top";//上升top ,下降bottom
		$info['store_count_per'] = "0%";

		$info['store_price'] = 0;
		$info['store_price_icon'] = "bottom";//上升top ,下降bottom
		$info['store_price_per'] = "0%";
		
		$info['store_good'] = 0;
		$info['store_good_icon'] = "top";//上升top ,下降bottom
		$info['store_good_per'] = "0%";
		
		output_data($info);
		
	}


	private function storeState(){
		$data = array();
		$data[0] = '下架';
		$data[1] = '上线';
		$data[2] = '下线';
		return $data;
	}
	
	
	//品类管理
	public function store_classOp(){
		
		
		$cuisine_list = model('store_cuisine')->getStoreCuisineList(TRUE);
		$list = array();
		
		$list[0]= array(
			'cuisine_name' => '全部',
			'child' => $this->getSmallClass(0),
			'child_num' => count($this->getSmallClass(0)),			
		);		
		
		foreach($cuisine_list as $k => $v){
			$list[$k+1] = $v;
			$list[$k+1]['child'] = $this->getSmallClass($v['cuisine_id']);
			$list[$k+1]['child_num'] = count($list[$k+1]['child']);
			$list[$k+1]['icon'] = $v['cuisine_id'];
			$list[$k+1]['cuisine_lang'] = unserialize($v['cuisine_lang']);
			
		}		
		output_data($list);	
	}
	
	//子类
	private function getSmallClass($id){		
		$where = array();
		if($id == 0){
			$where['cuisine_id'] = array('gt',0);
		}else{
			$where['cuisine_id'] =  $id;
		}
		$small_list = model('store_small_class')->getStoreSmallClassList($where);	
		
		foreach($small_list as $k=> $v){
			$small_lang = unserialize($v['small_lang']);
			$v['small_lang'] = $small_lang;		
			$small_list[$k] = $v;
		}
		
		return $small_list;
	}
	
	
	
	//调用语言配置
	private function get_system_lang(){
		
		$system_lang = model('language')->getLangList(array('language_system' => 1));		
		return $system_lang;
		
	}
	
	
	public function get_big_class_initOp(){		
		$lang = $this->get_system_lang();
		$info = array();
		
		foreach($lang as $k=> $v){
			$goods_lang[$k]['lang_name'] = $v['language_flag'];	
			$goods_lang[$k]['cuisine_name'] = '';	
		}		
		$info['class_id'] = 1;
		$info['cuisine_icon'] = '';		
		$info['cuisine_lang'] = $goods_lang;		
			
		output_data($info);
		
	}
	
	//大类详情
	public function get_big_class_infoOp(){
		
		$where = array();
		$where['cuisine_id'] = $_POST['id'];
		$info = model('store_cuisine')->getStoreCuisineInfo($where);
		if(!empty($info)){			
			$info['cuisine_lang'] = unserialize($info['cuisine_lang']);
		}	
		output_data(array('info' => $info));
	}
	
	//保存大类
	public function save_big_classOp(){
		
	
		
		$where = array();
		$where['cuisine_id'] = $_POST['cuisine_id'];
		
		$lang = $_POST['cuisine_lang'];		
		foreach($lang as $k => $v){
			$cuisine_lang[$k]['lang_name'] = $v['lang_name'];
			$cuisine_lang[$k]['cuisine_name'] = $v['cuisine_name'];			
		}
		
		
		$data = array();
		$data['cuisine_name'] = $small_lang[0]['cuisine_name'];
		$data['cuisine_lang'] = serialize($cuisine_lang);
		
		$row = model('store_cuisine')->editStoreCuisine($data,$where);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败。');
			
		}
		
		
		
	}
	
	
	//添加数据
	public function get_small_class_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();
		
		foreach($lang as $k=> $v){
			$goods_lang[$k]['lang_name'] = $v['language_flag'];	
			$goods_lang[$k]['small_name'] = '';	
		}			
		
		
		$info['class_id'] = 1;
		$info['class_name'] = '';		
		$info['cuisine_id'] = '';
		$info['small_lang'] = $goods_lang;		
			
		output_data($info);
		
		
		
	}
	
	
	
	//获取小类的详情
	public function get_small_class_infoOp(){
		
		$where = array();
		$where['small_id'] = $_POST['id'];
		$info = model('store_small_class')->getStoreSmallClassInfo($where);
		if(empty($info)){			
			output_error('获取分类失败');
		}
		$info['small_lang'] = unserialize($info['small_lang']);		
		output_data($info);
	}
	
	//保存小类的数据
	public function save_small_classOp(){
		
		$where = array();
		$where['small_id'] = $_POST['small_id'];
		
		$lang = $_POST['small_lang'];		
		foreach($lang as $k => $v){
			$small_lang[$k]['lang_name'] = $v['lang_name'];
			$small_lang[$k]['small_name'] = $v['small_name'];			
		}
		

		$data = array();
		$data['small_name'] = $small_lang[0]['small_name'];
		$data['small_lang'] = serialize($small_lang);
		
		$row = model('store_small_class')->editStoreSmallClass($data,$where);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败。');
			
		}
			
	}
	
	
	
	//元素分类	
	public function ingr_class_listOp(){		
		$list = model('goods_attr')->getClassList(TRUE);
		$class_list = array();
		foreach($list as $v){
			$v['gc_lang'] = unserialize($v['gc_lang']);
			$v['child'] = $this->getIngrList($v['gc_id']);			
			$class_list[] = $v;
		}		
		
		output_data(array('list' => $class_list));
	}
	
	private function getIngrList($class_id){
		
		$ingr = model('goods_attr')->getGoodsAttrList(array('attr_class_id' => $class_id));		
	
		$ingr_data = array();		
		foreach($ingr as $k=> $v){
			if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);
				$ingr_data[$k]['attr_name'] 	= $new[0];
				$ingr_data[$k]['attr_icon'] 	= UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
			}else{
				$ingr_data[$k]['attr_name'] 	=  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] 	=   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];	
			}
			$ingr_data[$k]['active'] = 0;
			$ingr_data[$k]['attr_name_lang'] = unserialize($v['attr_name_lang']);			
			$ingr_data[$k]['attr_id'] = $v['attr_id'];
		}		
		
		return $ingr_data;
	}
	
	//元素分类
	public function ingr_class_infoOp(){
		
		$where = array();
		$where['gc_id'] = $_POST['gc_id'];
		
		$row = model('goods_attr')->getClassInfo($where);
		if($row){			
			$row['gc_lang'] = unserialize($row['gc_lang']);
			output_data(array('info' => $row));
		}else{
			output_error('获取失败');
		}
	
	}
	
	//元素分类保存
	public function ingr_class_saveOp(){
		
		$model = model('goods_attr');
		
		$data = array();
		$data['gc_name'] = $_POST['gc_name'];
		$data['gc_lang'] = serialize($_POST['gc_lang']);
		$data['gc_sort'] = $_POST['gc_sort'];	
			
		if(intval($_POST['gc_id']) > 0){			
			
			$where = array();
			$where['gc_id'] = $_POST['gc_id'];
			$row = $model->editClass($where,$data);
			
		}else{
			
			$row = $model->addClass($data);	
					
		}
		if($row){			
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	
	//元素分类删除
	public function ingr_class_delOp(){
		
		$where = array();
		$where['gc_id'] = $_POST['gc_id'];
		$row = model('goods_attr')->delClass($where);
		
		if($row){			
			output_data('删除成功');
		}else{
			output_error('删除失败');
		}		
	}
	
	
	//获取init 
	public function ingr_class_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();		
		$attr_lang = array();
		foreach($lang as $k=> $v){			
			$attr_lang[$k]['lang_name'] = $v['language_flag'];	
			$attr_lang[$k]['class_name'] = '';	
		}
		
		
		$data = array();
		$data['gc_name'] = '';
		$data['gc_lang'] = $attr_lang;
		$data['gc_sort'] = 0;
		
		output_data(array('info' => $data));
		
		
	}
	
	
	
	
	
	
	//元素列表
	public function ingr_listOp(){
		
		$ingr = model('goods_attr')->getGoodsAttrList(TRUE);
		
		$ingr_data = array();
		
		foreach($ingr as $k=> $v){
			
			if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);
				$ingr_data[$k]['attr_name'] = $new[0];
				$ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
			}else{
				$ingr_data[$k]['attr_name'] =  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];	
			}
			$ingr_data[$k]['active'] = 0;			
			$ingr_data[$k]['attr_id'] = $v['attr_id'];
		}		
		output_data(array('list' => $ingr_data));		
	}
	
	//元素详情
	public function ingr_infoOp(){		
		$where = array();
		$where['attr_id'] = $_POST['id'];
		$row = model('goods_attr')->getGoodsAttrInfo($where);
		if($row){
			$row['attr_name_lang'] = unserialize($row['attr_name_lang']);			
			output_data(array('info' => $row));			
		}else{
			output_error('无数据');
		}		
	}
	
	//元素保存
	public function ingr_saveOp(){
		
		
				
		
		
		$model = model('goods_attr');		
		$data = array();
		$data['attr_name'] 		= $_POST['attr_name_lang'][0]['attr_name'];
		$data['attr_icon'] 		= $_POST['attr_icon'];
		$data['attr_active_icon'] = $_POST['attr_active_icon'];
		$data['attr_sort']	 = $_POST['attr_sort'];
		$data['is_show'] 	= $_POST['is_show'];
		$data['disabled'] = $_POST['disabled'];
		$data['attr_code'] = $_POST['attr_code'];
		$data['attr_type'] = $_POST['attr_type'];
		$data['attr_content'] = $_POST['attr_content'];
		$data['attr_class_id'] = $_POST['attr_class_id'];
		
		$attr_class = $model->getClassInfo(array('gc_id' => $_POST['attr_class_id']));		
		
		
		$data['attr_class_name'] 	= $attr_class['gc_name'];		
		$data['attr_name_lang'] 	= serialize($_POST['attr_name_lang']);
		
		if(intval($_POST['attr_id'] > 0)){
			$where = array();
			$where['attr_id'] = $_POST['attr_id'];
			$row = $model->editGoodsAttr($where,$data);			
		}else{
			$row = $model->addGoodsAttr($data);
		}
		
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	//元素删除
	public function ingr_delOp(){
		
		$where = array();
		$where['attr_id'] = $_POST['id'];
		$row = model('goods_attr')->delGoodsAttr($where);
		if($row){			
			output_data('删除成功');
		}else{
			output_error('删除失败');
		}
		
	}
	
	public function ingr_initOp(){
		
		$lang = $this->get_system_lang();
		$info = array();		
		$attr_lang = array();
		foreach($lang as $k=> $v){
			
			$attr_lang[$k]['lang_name'] = $v['language_flag'];	
			$attr_lang[$k]['attr_name'] = '';	
			
		}
		
		$data['attr_name'] 			= '';
		$data['attr_icon'] 			= '';
		$data['attr_active_icon'] 	= '';
		$data['attr_sort'] 			= 0;
		$data['is_show'] 			= 0;
		$data['disabled'] 			= '';
		$data['attr_code'] 			='';
		$data['attr_type'] 			= '';
		$data['attr_content'] 		= '';
		$data['attr_class_id'] 		= '';
		$data['attr_class_name'] 	= '';
		$data['attr_name_lang'] 	= $attr_lang;	
		
		output_data(array('info' =>$data));	
		
	}
	
	
	
	
	
	
	
	
	
	public function store_listOp(){
		
		
		$model_store = Model('store');
		// 设置页码参数名称
		$condition = array();				 
	
		$condition['admin_id'] = $this->admin_info['admin_id'];
		
		
		if ($_POST['store_name'] != '') {
		    $condition['store_name'] = array('like', '%' . $_POST['store_name'] . '%');
		}
		
		if ($_POST['grade_id'] != '') {
		    $condition['grade_id'] = $_POST['grade_id'];
		}
		
		if ($_POST['store_state'] != '') {
		    $condition['store_state'] = $_POST['store_state'];
		}
						
		if ($_POST['examine'] != '') {
		    $condition['store_examine'] = $_POST['examine'];
		}
		
		if ($_POST['query'] != '') {
		    $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
		}
		
		$order = '';		
		$param = array('store_id','order_state_peican','order_state_wancheng','order_state_quxiao','store_score','order_state_avg');
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
		        $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
		}
		
	
							
		//店铺列表
		$store_list = $model_store->getStoreList($condition, $this->page, $order);		
		$page_count = $model_store->gettotalpage();
		$list_count = $model_store->gettotalnum();
		
		
		
		$data = array();
		$examine = array('编辑中','审核中','已通过');
		$gadmin = $this->admin_info['id'];
		
		$class_data = model('store_class')->getStoreClassList(TRUE);
		$storeState = $this->storeState();
		
		foreach($class_data as $v){
			$class_array[$v['class_id']] = $v;			
		}
		
		foreach ($store_list as $value) {
			$param = array();          
				
			$param['store_avatar'] =  $value['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$value['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');			
			$param['store_id'] = $value['store_id']; 	
			$param['store_name_primary'] = $value['store_name_primary']; 
			$param['store_name_secondary'] = $value['store_name_secondary']; 			
			
			$param['store_code'] = $value['store_code']; 		
			$param['admin_id'] = $gadmin == 1 ? 1 : $value['admin_id']; 
			$param['admin_name'] = $value['admin_name']; 
			$param['region_name'] = $value['region_name'];			
			$param['region_id'] = $value['region_id'];		
			$param['region_color'] = $value['region_color'];	
			$param['store_class_id'] = $value['store_class_id'];			
			$param['store_class_name'] = $class_array[$value['store_class_id']]['class_name'];			
			$param['store_phone'] = $value['store_phone'];
			$param['order_state_peican'] = $value['order_state_peican'];
			$param['order_state_wancheng'] = $value['order_state_wancheng'];
			$param['order_state_quxiao'] = $value['order_state_quxiao'];
			$param['order_state_avg'] = $value['order_state_quxiao'];
			$param['store_score'] = $value['store_score'];	
			$param['store_state_name'] = $storeState[$value['store_state']];
				
			
			$param['store_state'] = $value['store_state'];		
			$param['store_examine'] = $value['store_examine'];		
			$param['store_examine_name'] = $examine[$value['store_examine']];		
		    $list[] = $param;
			
		}
		
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
		
		
	}

	
	//添加商户
	
	//搜索专员
	

	public function getMerchantSearchOp(){
		
		
		$where = "nickname like '%".$_POST['keyword']."%' or member_code  like '%".$_POST['keyword']."%'";
		
		$list = model('admin')->getAdminListNew($where);						
		
		output_data(array('list' => $list));
		
	}
	
	
	private function checkMemberName($memberName)
	{
	    // 判断store_joinin是否存在记录
	    $count = (int) Model('store')->getStoreCount(array(
	        'store_login_name' => $memberName,
	    ));
	    if ($count > 0){
	        return false;
		}
	    return true;
	}
	
	//生成编号
	
	private function store_code($store_code){		
		
		if($store_code == ''){
			$count = model('store')->getStoreCount(TRUE);
			$code = strtoupper('AUS01').str_pad($count + 1,4,0,STR_PAD_LEFT);
			return $code;
		}else{			
			return $store_code;
		}
		
	}
	
	
	//初始化新增店铺
	
	public function store_saveOp(){
		
		
			$model = model('store');			
			$store_code = $_POST['store_code'];
			if (!$this->checkMemberName($store_code)){				
				output_error('账号已被占用');				
			}
		
			$data = array(			
					
				'store_name_primary' => $_POST['store_name_primary'],
				'store_name_secondary' => $_POST['store_name_secondary'],
				'store_class_id' => $_POST['store_class_id'],
				'store_login_password' => md5($_POST['store_login_password']),
				'store_time'=>time(),
				'store_code' => $this->store_code($_POST['store_code'])
				
			);
			
			
			if(!empty($_POST['admin_id'])){
				
				$admin = model('admin')->getOneAdmin($_POST['admin_id']);					
				$data['admin_id'] = $admin['admin_id'];			
				$data['admin_name'] = $admin['admin_name'];
				
			}
				
			$store_id = $model->addStore($data);
			if($store_id){				
			
					$contract = array();
					$contract['store_id'] = $store_id;
					$contract['contract_rate'] = 0;
					$contract['addtime'] = time();
					$contract['is_default'] = 1;	
					model('store_contract')->addContract($contract);
					
					$license = array();
					$license['store_id'] = $store_id;
					$license['addtime'] = time();
					$license['is_default'] = 1;
					model('store_license')->addLicense($license);
					
					$hygiene = array();
					$hygiene['store_id'] = $store_id;
					$hygiene['addtime'] = time();
					$hygiene['is_default'] = 1;
					model('store_hygiene')->addHygiene($hygiene);
					
					output_data(array('store_id'=>$store_id));
			}else{
					output_error('操作失败');
	      	}
		
	}
	
	
	
	public function store_tokenOp(){
		
		$model_store = Model('store');
		$store_info = $model_store->getStoreInfo(array('store_id' => $_POST['store_id']));   
		
		$token = $this->_get_token($store_info['store_id'], $store_info['store_login_name'], 'pc');
		if($token) {
		
			//	$member_token = $this->_get_member_token($member_info['member_id'], $member_info['member_name'], 'wap');
		 //   $memberinfo=array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $member_token);
			
			output_data(array('seller_name' => $store_info['store_login_name'], 'store_name_primary' => $store_info['store_name_primary'], 'token' => $token));
		} else {
		    output_error('登录失败');
		}
	}
	
	/**
	 * 登录生成token
	 */
	private function _get_token($seller_id, $seller_name, $client) {
	    $model_mb_seller_token = Model('mb_seller_token');
	
	    //重新登录后以前的令牌失效
	    $condition = array();
	    $condition['seller_id'] = $seller_id;
	    $model_mb_seller_token->delSellerToken($condition);
	
	    //生成新的token
	    $mb_seller_token_info = array();
	    $token = md5($seller_name. strval(TIMESTAMP) . strval(rand(0,999999)));
	    $mb_seller_token_info['seller_id'] = $seller_id;
	    $mb_seller_token_info['seller_name'] = $seller_name;
	    $mb_seller_token_info['token'] = $token;
	    $mb_seller_token_info['login_time'] = TIMESTAMP;
	    $mb_seller_token_info['client_type'] = $client;
	
	    $result = $model_mb_seller_token->addSellerToken($mb_seller_token_info);
	
	    if($result) {
	        return $result;
	    } else {
	        return null;
	    }
	}
	
}
