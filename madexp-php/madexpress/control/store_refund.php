<?php
/**
 * 商家退款

 */
defined('InMadExpress') or exit('Access Invalid!');

class store_refundControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }
    
	
	
	/*
	退款商品列表
	*/
	
	public function order_infoOp(){
		
		$order_id = intval($_POST['order_id']);
		if (!$order_id) {
		    output_error('订单编号有误');
		}
		$model_order = Model('order');
		$condition = array();
		$condition['order_id'] = $order_id;
		$condition['store_id'] = $this->store_info['store_id'];
		
		$order = $model_order->getOrderInfo($condition, array('order_common','order_goods'));
		
		
		if (empty($order)) {
		    output_error('订单信息不存在');
		}
		
		$order_item = array();		
		//从新格式化				
		
		$order_user = array();
		$order_user['buyer_code'] = $order['buyer_code'];
		$order_user['buyer_name'] = $order['buyer_name'];
		$order_user['buyer_phone'] = $order['buyer_phone'];
		$order_user['buyer_comment'] = $order['buyer_comment'];
		$order_user['buyer_is_new'] = $order['buyer_is_new'];
		$order_user['distributor_name'] = $order['distributor_name'];
		$order_user['distributor_mobile'] = $order['distributor_mobile'];
		$order_user['state'] = $order['order_state'];
		$order_user['evaluation_state'] = $order['evaluation_state'];
		
		if($order['evaluation_state'] == 1) {			
			$model = model('evaluate_store');		
			$where = array(			
				'store_evaluate_orderid' => $order['order_id']
			);		
			$eva = $model->getEvaluateStoreInfo($where);	
			$order['evaluation_text'] = $eva['store_evaluate_score_text'];
			$order['evaluation_content'] = $eva['store_evaluate_score_content'];	
		}
		
					
		$order_item['order_user'] = $order_user;
				
				
				
		//订单商品,原始商品，不做任何修改
		$order_goods = array();		
		$goods =  $order['extend_order_goods'];		
		$goods_num = 0;
		foreach($goods as $k => $v){		
            if($v['goods_price'] > 0){
                $v['goods_refund_num'] = $v['goods_usable_num'] ;
                $goods_list[] = $v;
			    $goods_num += $v['goods_num'];	
            }

		}
		
		$order_goods['goods_list'] = $goods_list;				
		$order_goods['goods_num'] = $goods_num;		
		$order_item['order_goods'] = $order_goods;
		
		
		//金额信息
		$order_amount = array();		
		//商品小计
		$order_amount['goods_amount'] = $order['goods_amount'];		
		//商品折扣
		$order_amount['sales_amount'] = $order['sales_amount'];		
		//餐盒费
		$order_amount['foodbox_amount'] = $order['foodbox_amount'];
		//配送费
		$order_amount['delivery_amount'] = $order['delivery_fee'];
		//支付手续费	
		
		$order_amount['pay_commission_amount'] = $order['pay_commission_amount'];		
		
		$order_item['order_amount'] = $order_amount;
		

		//商家优惠列表
		//店铺优惠券
		$condition = array();
		$condition['voucher_t_gettype'] = 3;
		$condition['voucher_t_state'] 	= 1;
		$condition['voucher_t_end_date'] = array('gt', time());
		$condition['voucher_t_store_id'] = array('in', $this->store_info['store_id']);
		$voucher_template = Model('voucher')->getVoucherTemplateList($condition);
		$voucher = array();
		if (!empty($voucher_template)) {			
			foreach ($voucher_template as $val) {				
				$param = array();
				$param['coupons_id'] = $val['voucher_t_id'];
				$param['coupons_price'] = $val['voucher_t_price'];
				$param['coupons_title'] = $val['voucher_t_title'];
				//$param['voucher_t_limit'] = $val['voucher_t_limit'];
				//$param['voucher_t_end_date'] = date('Y年m月d日', $val['voucher_t_end_date']);
			  
				$voucher[] = $param;
				
			}					
			
		}
		 
		$order_item['store_coupons'] = $voucher;


		
		output_data($order_item);		
		
	}
	
	
	//保存退款信息
	public  function  save_refundOp(){
		
		$model = model('order');
		$order_id 	= $_POST['order_id'];
		$refund_type = $_POST['refund_state'];
		$coupon_id = $_POST['coupon_id'];		
		$order_info = $model->getOrderInfoN(array('order_id'=>$order_id,'store_id'=>$this->store_info['store_id']));
		if(empty($order_info)){			
			output_error('订单参数错误');
		}		
		if($order_info['refund_state'] == 2){
			output_error('此订单已退款');
		}

        
            //获取历史单数量
        $version = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
        $version_num = $version + 2;
            //生成订单快照
        $sphot =  model('order_snapshot')->createSphot($order_id);            
        if($sphot){		    
            $order_num =  model('order_snapshot')->getSnaOrderCount(array('order_id' => $order_id));
        }else{
            output_error('历史单生成失败，订单修改失败');
        }
     



        //判断当前退款属于1,2,3阶段。
        //商家接单前直接取消
        //商家接单后要生成票据，部分退款refund code 
        //订单送达后，就是退款票据
        $refund_stage = 0;
        if($order_info['order_state'] == 40 || $order_info['order_state'] == 50){
            $refund_stage = 1;
        }
        if($order_info['order_state'] == 60){
            $refund_stage = 2;
        }       
		
        
        
        //部分退款
		if($refund_type == 1){			
		        
                $refund_amount = 0;		 
                $goods_ids = explode('|',$_POST['goods_ids']);			
                $refund_amount = 0;
                foreach($goods_ids as $v){	

                    $goods = explode(':',$v);	
                    $rec_info = model('order')->getOrderGoodsInfo(['rec_id' => $goods[0]]);
                    $rec_info['goods_num'] = $goods[1];
                    $refund_goods[] = $rec_info;
                    $refund_amount += $rec_info['goods_price'];

                }
                
                $data = array();       
                $data['order_id']    = $order_info['order_id'];
                $data['order_sn']    = $order_info['order_sn'];
                $data['store_id']    = $order_info['store_id'];
                $data['store_name']  = $order_info['store_name'];
                $data['member_id']   = $order_info['buyer_id'];        
                $data['role']       =  0;
                $data['refund_comment'] = $_POST['refund_comment'];    
                $data['add_time']   = time();
                $data['refund_type'] = $refund_type;
                $data['refund_amount'] = $refund_amount;
                $data['refund_stage'] = $refund_stage;
                $data['refund_log'] = '';// serialize($order_refund);
                $data['is_delivery'] = 0;
                $data['is_commission'] = 0;                    
                $data['is_edit'] = 2; //证明这是订单编辑的
				$data['version'] = $version_num ;


                $refund_partial_id = model('order_refund')->addRefund($data);    
                if($refund_partial_id){

                    
                    foreach($refund_goods as $goods_info){

                     
                        $data = array(); 
                        $data['refund_id'] = $refund_partial_id;                        
                        $data['order_id'] = $goods_info['order_id'];                        
                        $data['goods_id'] = $goods_info['goods_id'];                             
                        $data['goods_name'] =  $goods_info['goods_name'];                        
                        $data['goods_price'] = $goods_info['goods_price'];                           
                        $data['goods_num'] = $goods_info['goods_num'];                               
                        $data['goods_image'] = $goods_info['goods_image'];                       
                        $data['goods_pay_price'] = $goods_info['goods_pay_price'];                     
                        $data['store_id'] = $goods_info['store_id'];                      
                        $data['buyer_id'] = $goods_info['buyer_id'];                     
                        $data['gc_id'] = $goods_info['gc_id'];                      
                        $data['goods_size_id'] = $goods_info['goods_size_id'];                    
                        $data['goods_specs_id'] =$goods_info['goods_specs_id'];                      
                        $data['goods_spec'] = $goods_info['goods_spec'];                      
                        $data['goods_size'] = $goods_info['goods_size'];                       
                        $data['original_goods_id'] = $goods_info['original_goods_id'];                       
                        $data['original_goods_name'] = $goods_info['original_goods_name'];                         
                        $data['original_goods_num'] = $goods_info['original_goods_num'];                       
                        $data['original_goods_spec'] = $goods_info['original_goods_spec'];                         
                        $data['original_goods_size'] = $goods_info['original_goods_size'];                         
                        $data['original_goods_price'] = $goods_info['original_goods_price'];                        
                        $data['goods_lang'] = $goods_info['goods_lang'];                        
                        $data['goods_optional_ids'] = $goods_info['goods_optional_ids'];                        
                        $data['goods_optional'] = $goods_info['goods_optional'];                        
                        $data['original_goods_optional'] = $goods_info['original_goods_optional'];                        
                        $data['foodbox_price'] = $goods_info['foodbox_price'];                      
                        $data['foodbox_id'] = $goods_info['foodbox_id'];                        
                        $data['foodbox_name'] = $goods_info['foodbox_name'];                       
                        $data['foodbox_num'] =$goods_info['foodbox_num'];                         
                        $data['rec_id'] = $goods_info['rec_id'];     
                        $data['goods_code'] = $goods_info['goods_code'];                    
                        $data['version'] = $version_num ;
                  //      print_r($data);
                        $row = model('order_goods_refund')->addRefund($data);
                        

                        $data = array();
                        $data['rec_id'] 	= $goods_info['rec_id'];
                        $data['rec_num'] 	= $goods_info['goods_num'];
                        $data['type'] 		= 'refund';
                        $data['order_id'] 	= $order_id;

                        //print_r($data);                        
                        model('order_goods_edit')->addGoods($data);      

                      
                        //更新主商品信息的可操作商品数量
                     //   model('order')->editOrderGoods(['goods_usable_num'=>['exp','goods_usable_num -'.$goods_info['goods_num']]],['rec_id' => $goods_info['rec_id']]);		


                     
                    }

					//更新退款来路信息
                   $data =  $this->updateAmount($order_info['order_id'],$refund_partial_id);
				//   print_r($data);
				//	exit;

                    if($refund_stage == 2){ //部分退款

						$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_partial_id]);

                        $refund_pay_amount 		=  $refund_info['refund_pay_amount'];
                        $refund_pd_amount 		=  $refund_info['refund_wallet_amount'];
                        $refund_coupon_price 	=  $refund_info['refund_coupon_amount'];
            
                        $update = array();
                        $update['refund_pay_amount'] 	= array('exp','refund_pay_amount + '. $refund_pay_amount);
                        $update['refund_pd_amount'] 	= array('exp','refund_pd_amount + '. $refund_pd_amount);
                        $update['refund_coupon_price'] 	= array('exp','refund_coupon_price + '. $refund_coupon_price);	
                        model('order')->editOrder($update,['order_id' => $order_id]);

                        
                    }



                }		
		}


        //全单退款
        if($refund_type == 2){		
            
            //获取里面剩余的
            $goods_list  = model('order')->getOrderGoodsList(['order_id' => $order_info['order_id'],'goods_usable_num'=>array('gt',0)]);
            
            $refund_amount = 0;
            foreach($goods_list as $v){	             
                $refund_amount += $v['goods_price'] * $v['goods_usable_num'];
            }

            $refund_amount += $order_info['pay_commission_amount'];
                

            $data = array();       
            $data['order_id']    = $order_info['order_id'];
            $data['order_sn']    = $order_info['order_sn'];
            $data['store_id']    = $order_info['store_id'];
            $data['store_name']  = $order_info['store_name'];
            $data['member_id']   = $order_info['buyer_id'];        
            $data['role']       =  0;
            $data['refund_comment'] = $_POST['refund_comment'];    
            $data['add_time']   = time();
            $data['refund_type'] = $refund_type;
            $data['refund_amount'] = $refund_amount;
            $data['refund_stage'] =  $refund_stage;
            $data['refund_log'] = '';// serialize($order_refund);
            $data['is_delivery'] = 1;
            $data['is_commission'] = 0;    //商家操作是不退抽成的。
            $data['is_edit'] = 2; //证明这是订单编辑的
            $data['version'] = $version_num ;
          //  $data['refund_pay_amount'] = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
          //  $data['refund_wallet_amount'] = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
         //   $data['refund_coupon_amount'] = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

            $refund_id = model('order_refund')->addRefund($data);    
            if($refund_id){
             
                foreach($goods_list as $v){
                    $data = array(); 
                    $data['refund_id'] = $refund_id;                        
                    $data['order_id'] = $v['order_id'];                        
                    $data['goods_id'] = $v['goods_id'];                             
                    $data['goods_name'] =  $v['goods_name'];                        
                    $data['goods_price'] = $v['goods_price'];                           
                    $data['goods_num'] = $v['goods_num'];                               
                    $data['goods_image'] = $v['goods_image'];                       
                    $data['goods_pay_price'] = $v['goods_pay_price'];                     
                    $data['store_id'] = $v['store_id'];                      
                    $data['buyer_id'] = $v['buyer_id'];                     
                    $data['gc_id'] = $v['gc_id'];                      
                    $data['goods_size_id'] = $v['goods_size_id'];                    
                    $data['goods_specs_id'] =$v['goods_specs_id'];                      
                    $data['goods_spec'] = $v['goods_spec'];                      
                    $data['goods_size'] = $v['goods_size'];                       
                    $data['original_goods_id'] = $v['original_goods_id'];                       
                    $data['original_goods_name'] = $v['original_goods_name'];                         
                    $data['original_goods_num'] = $v['original_goods_num'];                       
                    $data['original_goods_spec'] = $v['original_goods_spec'];                         
                    $data['original_goods_size'] = $v['original_goods_size'];                         
                    $data['original_goods_price'] = $v['original_goods_price'];                        
                    $data['goods_lang'] = $v['goods_lang'];                        
                    $data['goods_optional_ids'] = $v['goods_optional_ids'];                        
                    $data['goods_optional'] = $v['goods_optional'];                        
                    $data['original_goods_optional'] = $v['original_goods_optional'];                        
                    $data['foodbox_price'] = $v['foodbox_price'];                      
                    $data['foodbox_id'] = $v['foodbox_id'];                        
                    $data['foodbox_name'] = $v['foodbox_name'];                       
                    $data['foodbox_num'] =$v['foodbox_num'];                         
                    $data['rec_id'] = $v['rec_id'];     
                    $data['goods_code'] = $v['goods_code'];            
                    $data['version'] = $version_num ;        
                    $row = model('order_goods_refund')->addRefund($data);


                    
                    $data = array();
                    $data['rec_id'] 	= $v['rec_id'];
                    $data['rec_num'] 	= $v['goods_num'];
                    $data['type'] 		= 'refund';
                    $data['order_id'] 	= $order_id;
                    //print_r($data);                        
                    model('order_goods_edit')->addGoods($data);      
                                                      
                    //更新主商品信息的可操作商品数量
                  //  model('order')->editOrderGoods(['goods_usable_num'=>['exp','goods_usable_num -'.$v['goods_num']]],['rec_id' => $v['rec_id']]);		
               
                }


                //更新
			
				$this->updateAmount($order_info['order_id'],$refund_id);

                //更订单退款数据

                $refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);

                $refund_pay_amount 		=  $refund_info['refund_pay_amount'];
                $refund_pd_amount 		=  $refund_info['refund_wallet_amount'];
                $refund_coupon_price 	=  $refund_info['refund_coupon_amount'];

    
				$update = array();
				$update['refund_pay_amount'] 	= array('exp','refund_pay_amount + '. $refund_pay_amount);
				$update['refund_pd_amount'] 	= array('exp','refund_pd_amount + '. $refund_pd_amount);
				$update['refund_coupon_price'] 	= array('exp','refund_coupon_price + '. $refund_coupon_price);	

                //新增配送费和支付手续费
                $update['refund_pay_commission_amount'] 	= array('exp','refund_pay_commission_amount + '. $order_info['pay_commission_amount']);	
          //      $update['refund_delivery_fee'] 	= array('exp','refund_delivery_fee + '. $order_info['delivery_fee']);	
		//		$update['refund_commission_amount'] = array('exp','refund_commission_amount + '. $order_info['commission_amount']);	




				model('order')->editOrder($update,['order_id' => $order_id]);


              // 

            }

		}

		//配送费。
		if($refund_type == 2){

			$data = array();       
			$data['order_id']       = $order_info['order_id'];
			$data['order_sn']       = $order_info['order_sn'];
			$data['store_id']       = $order_info['store_id'];
			$data['store_name']     = $order_info['store_name'];
			$data['member_id']      = $order_info['buyer_id'];        
			$data['role']           = '2';
			$data['refund_comment'] = $_POST['refund_comment'];    
			$data['add_time']       = time();
			$data['refund_type']    = $refund_type;
			$data['refund_amount']  = $order_info['delivery_fee'];
			$data['refund_stage']   = $refund_stage;
			$data['refund_log']     = '';
			$data['is_delivery']    = 1;
			$data['is_commission']  = 0;
			$data['is_edit']        = 2; //证明这是订单编辑的
			$data['version']        = $version_num ;
			$data['refund_state']   = 1; //退配送费的项目直接标记已完成
			$delivery_refund_id = model('order_refund')->addRefund($data);

			$this->updateAmountDelivery($order_id,$delivery_refund_id);


			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $delivery_refund_id]);

			$refund_pay_amount 		=  $refund_info['refund_pay_amount'];
			$refund_pd_amount 		=  $refund_info['refund_wallet_amount'];
			$refund_coupon_price 	=  $refund_info['refund_coupon_amount'];


			$update = array();
			$update['refund_pay_amount'] 	= array('exp','refund_pay_amount + '. $refund_pay_amount);
			$update['refund_pd_amount'] 	= array('exp','refund_pd_amount + '. $refund_pd_amount);
			$update['refund_coupon_price'] 	= array('exp','refund_coupon_price + '. $refund_coupon_price);	

			//新增配送费和支付手续费
		//   $update['refund_pay_commission_amount'] 	= array('exp','refund_pay_commission_amount + '. $order_info['pay_commission_amount']);	
			$update['refund_delivery_fee'] 	= array('exp','refund_delivery_fee + '. $order_info['delivery_fee']);	

			model('order')->editOrder($update,['order_id' => $order_id]);


		}

		
		$where =  array(
			'order_id' => $order_id,
			'store_id' => $this->store_info['store_id']			
		);	
		
		$update = array();
		$update['refund_state'] 	= $refund_type;
		$update['refund_amount'] 	= $refund_amount;
		$update['refund_comment'] 	= $_POST['refund_comment'];
		$update['refund_reason'] 	= $_POST['refund_reason'];	
        if($refund_type == 1){
            $snapshot_amendment[] = '部分退款';
        }else{
            $snapshot_amendment[] = '全单退款';
        }
        $update['order_update_time'] = time();		
    	$update['is_old_num'] = $order_num + 1;
    	$update['snapshot_amendment'] = implode(',',$snapshot_amendment); //当前新订单做了哪些处理
        
		$row = $model->editOrder($update,$where);	
		if($row){		



            $data_record = array();
            $data_record['order_id'] = $order_info['order_id'];
            $data_record['merchant_refund_type'] = $refund_type == 1 ? 'Partial' : 'Full';
            $data_record['merchant_refund_time'] = time();            
			$record_data['merchant_refund_comment'] = $_POST['refund_comment'];
			
            $data_record['refund_id'] = $refund_type == 1 ? $refund_partial_id : $refund_id ;
            $data_record['coupon_id'] = $coupon_id;
            model('order_record')->addRecord('merchant_refund',$data_record);

            //推送订单取消的状态。

            if($order_info['order_state'] <= 50){
            
                $data = array();
                $data['type'] = 'order_cancel';
                $data['data'] = $order_info['order_id'];
                //print_r($order_info);
                $data = json_encode($data);	
              //  model('stoket')->sendDelivery($$order_info['distributor_id'],$data);       

            }	
	


			//获取订单ID
			if($coupon_id > 0){		
                
				$t_id = intval($coupon_id);
				if($t_id <= 0){
				    output_error('店铺优惠券信息错误');
				}
				$model_voucher = Model('voucher');
				//验证是否可领取代金券
				$data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
				if ($data['state'] == false){
				    output_error($data['msg']);
				}
				try {
				    $model_voucher->beginTransaction();
				    //添加代金券信息
				    $data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
				  
                    $data_record = array();
                    $data_record['order_id'] = $order_info['store_id'];
                    $data_record['merchant_refund_type'] = $refund_type == 1 ? 'Partial' : 'Full';               
                    $data_record['coupon_id'] = $coupon_id;        
                    model('order_record')->addRecord('voucher_issued',$data_record);
        
                  
                    if ($data['state'] == false) {
				        throw new Exception($data['msg']);
				    }
				    $model_voucher->commit();			            
				} catch (Exception $e) {
				    $model_voucher->rollback();
				    output_error($e->getMessage());
				}
				
			}
			//调用退款接口。根据原支付退回。
			
		
			   
			
            //执行全单退款
			if($refund_type == 2 ){
                //执行票据和退款生成 
                model('order_bill')->store_bill_full($order_info['order_id'],$refund_id,$delivery_refund_id);
          
            }			

            if($refund_type == 1 ){
                //部分退款
                model('order_bill')->store_bill_partial($order_info['order_id'],$refund_partial_id);
          
            }

            $order_id = $order_info['order_id'];
            
            //更新原商品数量
			$edit_num_list = model('order_goods_edit')->getGoodsList(['order_id' => $order_id,'type' => 'refund']);
			if(!empty($edit_num_list)){
				foreach($edit_num_list as $v){					
					model('order')->editOrderGoods(['goods_usable_num' => ['exp','goods_usable_num -'.$v['rec_num']]], ['rec_id' => $v['rec_id']]);
				}
			}

            //从新计算商品价格。
			$goods_list = model('order')->getOrderGoodsList(['order_id' =>  $order_id]);
			$goods_amount = 0;
			foreach($goods_list as $v){
				if($v['goods_usable_num'] >0){
					$goods_amount += $v['goods_price'] * $v['goods_usable_num'];
					model('order')->editOrderGoods(['goods_num' => $v['goods_usable_num']],['rec_id' => $v['rec_id']]);
				}
			}
			model('order')->editOrder(['goods_amount' => $goods_amount],['order_id' => $order_id]);


			//删除编辑的信息
			model('order_goods_edit')->delGoods(['order_id' => $order_id]);



			output_data('退款操作成功');
		}else{
			
			output_error('退款操作失败');
			
		}
		
		
	}

	private function updateAmountDelivery($order_id,$refund_id){


            
		$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id));

		//获取stripe支付的金额
		$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];			
		//获取钱包支付的金额
		$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
		//获取优惠券抵扣的金额
		$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

		$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);

	//	$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
		$refund_amount = $refund_info['refund_amount'];
	//	foreach($refund_goods_list as $v){
		//	$refund_amount +=  $v['goods_price'] * $v['goods_num'];
	//	}

		$update = array();
		$update['refund_amount'] 		= $refund_amount;
		$update['refund_pay_amount'] 	= 0;
		$update['refund_wallet_amount'] = 0;
		$update['refund_coupon_amount'] = 0;
		
		if($stripe_amount > 0 ){ //stripe youqian
			
			if($stripe_amount >= $refund_amount){ //stripe 有钱，直接退	

				$update['refund_pay_amount'] = $refund_amount;	

			}elseif($stripe_amount < $refund_amount && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退		

				$update['refund_pay_amount'] = $stripe_amount;
				
				$refund_wallet_amount = $refund_amount - $stripe_amount ;	
				
				if($wallet_amount >= $refund_wallet_amount  ){
				
					$update['refund_wallet_amount'] = $refund_wallet_amount;	
				
				}elseif($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0 ){
					
					$update['refund_wallet_amount'] = $wallet_amount;	

					$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;
				
					$update['refund_coupon_amount'] = $refund_coupon_amount;

				}else{
					$update['refund_coupon_amount'] = $refund_wallet_amount;
				}

			}	

		}elseif($wallet_amount > 0 ){ //stripe 没钱，钱包 有钱。

			if($wallet_amount >= $refund_amount ){ //余额大于退款金额。直接退。

				$update['refund_wallet_amount'] = $refund_amount;

			}elseif($wallet_amount < $refund_amount &&  $wallet_amount > 0 ){ //有钱，但钱不够。
					
				$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

				$refund_coupon_amount = $refund_amount - $wallet_amount;
			
				$update['refund_coupon_amount'] = $refund_coupon_amount;

			}else{

				$update['refund_coupon_amount'] = $wallet_amount;

			}


		}elseif($voucher_amount > 0 ){ //优惠券支付的。

			if($voucher_amount >= $refund_amount ){
				
				$update['refund_coupon_amount'] = $refund_amount;

			}

		}
	//	print_r($update);
	
		model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);


	}

    //更新金额
		private function updateAmount($order_id,$refund_id){

			$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id, 'store_id' => $this->store_info['store_id']));

			//获取stripe支付的金额
			$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];			
			//获取钱包支付的金额
			$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			//获取优惠券抵扣的金额
			$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $refund_id]);

				
			if($refund_info['refund_amount'] > 0){

				$refund_amount = $refund_info['refund_amount'];

			}else{

				$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
				$refund_amount = 0;
				foreach($refund_goods_list as $v){
					$refund_amount +=  $v['goods_price'] * $v['goods_num'];
				}

			}



			$update = array();
			$update['refund_amount'] 		= $refund_amount;
			$update['refund_pay_amount'] 	= 0;
			$update['refund_wallet_amount'] = 0;
			$update['refund_coupon_amount'] = 0;
			
			if($stripe_amount > 0 ){ //stripe youqian
				
				if($stripe_amount >= $refund_amount){ //stripe 有钱，直接退	

					$update['refund_pay_amount'] = $refund_amount;	

				}elseif($stripe_amount < $refund_amount && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退		

					$update['refund_pay_amount'] = $stripe_amount;
					
					$refund_wallet_amount = $refund_amount - $stripe_amount ;	
					
					if($wallet_amount >= $refund_wallet_amount  ){
					
						$update['refund_wallet_amount'] = $refund_wallet_amount;	
					
					}elseif($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0 ){
						
						$update['refund_wallet_amount'] = $wallet_amount;	

						$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;
					
						$update['refund_coupon_amount'] = $refund_coupon_amount;

					}else{
						$update['refund_coupon_amount'] = $refund_wallet_amount;
					}

				}	

			}elseif($wallet_amount > 0 ){ //stripe 没钱，钱包 有钱。

				if($wallet_amount >= $refund_amount ){ //余额大于退款金额。直接退。

					$update['refund_wallet_amount'] = $refund_amount;

				}elseif($wallet_amount < $refund_amount &&  $wallet_amount > 0 ){ //有钱，但钱不够。
						
					$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

					$refund_coupon_amount = $refund_amount - $wallet_amount;
				
					$update['refund_coupon_amount'] = $refund_coupon_amount;

				}else{

					$update['refund_coupon_amount'] = $wallet_amount;

				}


			}elseif($voucher_amount > 0 ){ //优惠券支付的。

				if($voucher_amount >= $refund_amount ){
					
					$update['refund_coupon_amount'] = $refund_amount;

				}

			}
		//	print_r($update);
		
			model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);

}

	
	
	//获取商品信息
	private function getOrderGoods($rec_id){
		
		$where = array();
		$where['rec_id'] = $rec_id;
		$where['store_id'] = $this->store_info['store_id'];
		
		
		
		$goods_info = model('order')->getOrderGoodsInfo($where);
		return $goods_info;
	}
	
	
	
	
	
	/*
	获取退款的选项
	*/
	
	public function refund_reasonOp(){
		
		$list = model('refund_return')->getReasonList(TRUE);
		
		$data = array();
		foreach($list as $v){
		    
		    $data[] = $v;
		    
		}
		
		
		output_data($data);
		
	}
	
	
	public function order_replacementOp(){
		
		
		
		
		
	}
	
	
	
	
}
