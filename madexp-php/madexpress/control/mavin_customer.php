<?php
/**
 * 客服记录
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class mavin_customerControl extends mobileMavinControl {
 
    
    public function __construct(){
        parent::__construct();	
    }



	private function state_count($state){
		
		 $model_feedback = Model('feedback');
		 $where = array('feedback_state'=> $state);		
		 $count = $model_feedback->getFeedBackCount($where);
		 return $count;
	}
	
	private function state_order_count($state){
		
		 $model_feedback = Model('feedback');
		 $where = array('order_id'=> array('neq',0));		
		 $count = $model_feedback->getFeedBackCount($where);
		 return $count;
	}
	
	public function get_customer_listOp(){
	
	
		if(!$_POST['state_type']){
			$_POST['state_type'] = 0;
		}
				
		$model_feedback = Model('feedback');
		
		// 设置页码参数名称
		$condition = array();
		if ($_POST['keyword'] != '') {         
			  $condition['feedback_code'] = array('like', '%' . $_POST['keyword'] . '%');
		}
		
		if ($_POST['feedback_content'] != '') {         
			  $condition['feedback_content'] = array('like', '%' . $_POST['feedback_content'] . '%');
		}
		
		if($_POST['user_type']==="0"){
			$condition['user_type'] = 1;
		}else if($_POST['user_type']!="0"){
			$condition['user_type'] = $_POST['user_type']+1;
		}

		if($_POST['state_type'] <= 2){
			
			$condition['feedback_state'] = $_POST['state_type'];
		}else{
			$condition['order_id'] = array('neq',0);
			
		}
		      
			   
		$usertype = array(
		    0 =>'客户',
			1 =>'商户',
			2 =>'配送员'
		);
			   
		$state = array(
		    0 =>'待处理',
			1 =>'处理中',
			2 =>'已完成'
		);
			   
			   
		 $feedbacktype = array(
		    0=>'投诉客户',
			1=>'投诉商家',
			2=>'投诉配送员',
		    3=>'意见反馈',
		    4=>'商家问题',
		    5=>'配送问题',
		    6=>'其他',
		);
			   
			
		//	print_r($condition);
			
		
		$feedback_list = $model_feedback->getFeedBackList($condition, '*', $this->page );
		$page_count = $model_feedback->gettotalpage();
		$list_count = $model_feedback->gettotalnum();
			
		  foreach ($feedback_list as $value) {
		    $param = array();			  
			$param['feedback_id'] = $value['id']; 
			$param['feedback_code'] = $value['feedback_code']; 
			$param['user_type'] =  $usertype[$value['user_type']-1];
			$param['feedback_type'] = $value['feedback_type']; 
			$param['feedback_content'] = $value['feedback_content'];  			
			$param['feedback_time'] = date('Y-m-d H:i:s',$value['feedback_time']);			
			$label = ' <span class="badge hui-bg radius-all-2">'.$state[$value['feedback_state']].'</span>';
		    $param['last_reply_time'] = $value['last_reply_time']> 0?  date('Y-m-d H:i:s',$value['last_reply_time']) : 'N/A';
			$param['feedback_state'] = $label;
		    $list[] = $param;
		}
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
	}
	
	//反馈详情
		
	public function feedback_infoOp(){
		    
		    $model_feedback = Model('feedback');		    
		    $where = array();
		    //$where['user_id'] = $this->member_info['member_id'];
		    //$where['user_type'] = 1;
		    $where['id'] = $_POST['feedback_id'];
	        $info = $model_feedback->getFeedBackInfo($where);	        
	        
			$state = array(
				0 => '待处理',
				1 => '处理中',
				2 => '已完成',
			);
			
	        if(!empty($info)){
	            $info['feedback_file'] = $this->feedback_image($info['feedback_file']);
				$info['feedback_result_file'] = $this->feedback_image($info['feedback_result_file']);
				
	            $info['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
				$info['feedback_state_name'] = $state[$info['feedback_state']];
				$info['feedback_time'] =  date('d/m/y H:i',$info['feedback_time']);
				$info['updatetime'] =  date('d/m/y H:i',$info['updatetime']);
				
				if($info['user_type'] == 1){ //用户
					
					$member = model('member')->getMemberInfo(array('member_id' => $info['user_id']));					
					$info['member_id'] = $member['member_id'];
					$info['member_code'] = $member['member_code'];
					
				}
				if($info['user_type'] == 2){ //商家
					
					$store = model('store')->getStoreInfo(array('store_id' => $info['user_id']));
					$info['store_id'] = $member['store_id'];
					$info['store_name'] = $member['store_name_primary'];
					
				}
				if($info['user_type'] == 3){ //骑手					
					$delivery = model('waiter')->getWaiterInfo(array('distributor_id' => $info['user_id']));
					$info['delivery_id'] = $delivery['distributor_id'];
					$info['delivery_name'] = $delivery['distributor_truename'];
				}
				
				
				if($info['order_id'] > 0){
						
					$order = model('order')->getOrderInfoN(array('order_id' => $info['order_id']));					
					$info['order_sn'] = $order['order_sn'];
					$info['member_id'] = $order['buery_id'];
					$info['member_code'] = $order['buyer_code'];
					
					$info['store_name'] = $order['store_name'];
					$info['store_id'] = $order['store_id'];
					
					$info['delivery_name'] = $order['distributor_name'];
					$info['delivery_id'] = $order['distributor_id'];
								
					
				}
				
	            //
	            $log_list = array();
	            $log = $model_feedback->getFeedBackListLog(array('feedback_id' => $info['id']));
	            
	            foreach($log as $v){
	                $v['log_file'] =  $this->feedback_image($v['log_file']);
	                $v['log_addtime'] =  date('d/m/y H:i',$v['log_addtime']);
	                $v['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
	                $log_list[] = $v;
	                
	            }
	            
	            
	            $info['log'] = $log_list;
	            
	            output_data(array('info' => $info));    
	        }else{
	            output_error('未获取到数据');
	        }
		    
		}
		
		
		//继续反馈
		public function feedback_add_logOp(){
				$model_feedback = Model('feedback');		    
				$data = array();
				$data['log_content'] = $_POST['log_content'];				
				$log_file = '';
				if($_POST["log_file"]){
					$base64_image2 = str_replace(' ', '+', $_POST["log_file"]);
					//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
					$file_url = $_POST["log_file"]?str_replace($gdurl,"",$_POST["log_file"]):'';//编辑时没上传图片，不更新这个值
					if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image2, $result)){
						//定义图片储存文件目录
						$dir = BASE_UPLOAD_PATH.'/'.ATTACH_FEEDBACK;
						//定义文件名称
						$picname = date("his") . '_' . rand(10000, 99999);
						if (!is_dir($dir)){
							//如果不存在就创建该目录
							mkdir($dir,0777,true);  
						}  
						//获取图片后缀
						if($result[2] == 'jpeg'){
							$picdir=$picname.'.jpg';
						}else{
							$picdir=$picname.'.'.$result[2];
						}
						//图片名称
						$image_url = $dir.'/'.$picdir;
						//储存图片
						if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image2)))){						
							$log_file = $picdir;							
						}
					}
				}				
				
				$data['log_file'] 	= $log_file;
				$data['feedback_id'] = $_POST['feedback_id'];			   
				$data['log_addtime'] = time();
		       
				$data['admin_id'] = $this->admin_info['admin_id'];
				$data['admin_name'] = $this->admin_info['nickname'];
				$row = $model_feedback->addFeedBackLog($data);
		    
		     if($row){			 
				 
				  $model_feedback->editFeedBack(array('id' => $_POST['feedback_id']),array('last_reply_time' => time(),'feedback_state'=> 1));				 
				 
		        output_data('添加成功');    
		    }else{
		        output_error('未获取到数据');
		    }		    
		}
		
		
		//确认完成
		public function confirmFeedbackOp(){
			$model_feedback = Model('feedback');		    
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(       
			    array("input"=>$_POST["feedback_result"],"require"=>"true","message"=>'请填写结论'),
			    array("input"=>$_POST["feedback_result_text"],"require"=>"true","message"=>'请输入反馈详情'),			
			);
			$error = $obj_validate->validate();
			if ($error != ''){
			    output_error($error);
			}	
			
			$feedback_result_file = '';
			if($_POST["feedback_result_file"]){
				$base64_image2 = str_replace(' ', '+', $_POST["feedback_result_file"]);
				//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
				$file_url = $_POST["feedback_result_file"]?str_replace($gdurl,"",$_POST["feedback_result_file"]):'';//编辑时没上传图片，不更新这个值
				if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image2, $result)){
					//定义图片储存文件目录
					$dir = BASE_UPLOAD_PATH.'/'.ATTACH_FEEDBACK;
					//定义文件名称
					$picname = date("his") . '_' . rand(10000, 99999);
					if (!is_dir($dir)){
						//如果不存在就创建该目录
						mkdir($dir,0777,true);  
					}  
					//获取图片后缀
					if($result[2] == 'jpeg'){
						$picdir=$picname.'.jpg';
					}else{
						$picdir=$picname.'.'.$result[2];
					}
					//图片名称
					$image_url = $dir.'/'.$picdir;
					//储存图片
					if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image2)))){						
						$feedback_result_file = $picdir;							
					}
				}
			}				
			
			
			
			$data = array();
			$data['feedback_result'] = $_POST['feedback_result'];
			$data['feedback_result_text'] = $_POST['feedback_result_text'];
			$data['feedback_result_file'] = $feedback_result_file;
			$data['updatetime'] = time();
			$data['feedback_state'] = 2;
			$row = $model_feedback->editFeedBack(array('id' => $_POST['feedback_id']),$data);
			if($row){				
				output_data('操作成功');				
			}else{
				output_error('操作失败');
				
			}
			
			
		}
		
		
		
		
		
		
		
		//置顶
		public function editStateOp(){
			
	
			$where = array(
				'id' => $_POST['feedback_id']
			);
			$data = array(
				'is_top' => 1,
				'updatetime' => time()
			);
			
			$row= model('feedback')->editFeedBack($where,$data);;
			if($row){
				output_data('置顶成功');				
			}else{
				output_error('置顶失败');				
			}
			
			
		}	
		
		
		
		
			//格式化图片
		private function feedback_image($image){		
			$list = array();
			if(!empty($image)){			
				$image  =	explode(",", $image);			
				foreach($image as $k => $v){				
					$list[$k]['url'] = UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.$v;
					$list[$k]['name'] = $v;
					
				}			
				return $list;			
			}else{
				return $list;			
			}
		}
		
		private function getMemberAvatar($image){
			if(file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !=''){
				return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
			}else{
				return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
			}
		}
	
	
	
	//分类
	
	public function customer_classOp(){
		
		$menu_array = array(
			array('menu_key'=>'3',
				  'menu_name'=>'订单相关',
				  'menu_icon'=> 'icon_chulizhong_n',
				  'menu_active_icon'=> 'icon_chulizhong_h',
				  'menu_count' => $this->state_order_count(0),
				  ),					
			array('menu_key'=>'0',
				  'menu_name'=>'反馈相关',
				  'menu_icon'=> 'icon_chulizhong_n',
				  'menu_active_icon'=> 'icon_chulizhong_h',
				  'menu_count' => $this->state_count(0),
				  ),
			array('menu_key'=>'1',
				  'menu_name'=>'处理中',    
				  'menu_icon'=> 'icon_fenpeizhong_n',  
				  'menu_active_icon'=> 'icon_fenpeizhong_h',
					'menu_count' => $this->state_count(1),
				  ),
			array('menu_key'=>'2',
				  'menu_name'=>'已完成',     
				  'menu_icon'=> 'icon_yiwancheng_n', 
				  'menu_active_icon'=> 'icon_yiwancheng_h',
				  'menu_count' => $this->state_count(2),
				  
			)
		);
		output_data(array('class_list'=>$menu_array));
	}
	
	
	
	public function help_classOp(){
		
		$menu_array = array(
			array('menu_key'=>'state_one',
				  'menu_name'=>'客户端',
				  'menu_icon'=> 'icon_chulizhong_n',
				  'menu_active_icon'=> 'icon_chulizhong_h',		
				  'menu_id' =>1
				  ),					
			array('menu_key'=>'state_one',
				  'menu_name'=>'商户端',
				  'menu_icon'=> 'icon_chulizhong_n',
				  'menu_active_icon'=> 'icon_chulizhong_h',				  
				  'menu_id' =>2
				  ),
			array('menu_key'=>'state_two',
				  'menu_name'=>'骑手端',    
				  'menu_icon'=> 'icon_fenpeizhong_n',  
				  'menu_active_icon'=> 'icon_fenpeizhong_h',				
				  'menu_id' =>3
				  )
		
		);
		output_data(array('class_list'=>$menu_array));
	}
	
	
	public function help_listOp(){
		$model = Model('document');
		$where = array();
		if($_POST['cate_id'] > 0){
			$where['doc_cate_id'] = $_POST['cate_id'];
		}
		
		$list = Model('document')->getDocumentList($where);
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
		
		
	}
	
	public function  help_saveOp(){				
		$model = model('document');
		$data = array();
		$data['doc_title'] = $_POST['doc_title'];
		$data['doc_content'] = $_POST['doc_content'];		
		$data['doc_cate_id'] = $_POST['doc_cate_id'];
		$data['doc_cate_name'] = $_POST['doc_cate_name'];
		if($_POST['doc_id'] > 0){			
			$where = array();
			$where['doc_id'] = $_POST['doc_id'];
			$row = $model->editDocument($where,$data);			
		}else{
			$row = $model->addDocument($data);
		}
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
		
	}
	
	
	public function help_delOp(){
		
		$where = array();
		$where['doc_id'] = $_POST['doc_id'];
		$row = model('document')->delDocument($where);
		if($row){
			output_data('删除成功');
		}		
	}
	
	
	public function help_infoOp(){
		
		$where = array();
		$where['doc_id'] = $_POST['doc_id'];
		$row = model('document')->getDocumentInfo($where);
		if($row){
			output_data($row);
		}else{
			output_error('信息不存在');
		}
		
	}
	
}
