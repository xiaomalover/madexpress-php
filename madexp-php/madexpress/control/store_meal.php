<?php
/**
 * 商品管理
 */
defined('InMadExpress') or exit ('Access Invalid!');
class store_mealControl extends mobileSellerControl {
    public function __construct() {
        parent::__construct ();       
    }
	
	//套餐列表
	public function get_meal_listOp(){
		
		$model_goods = Model('goods');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['is_delete'] = 0;
		$where['is_old'] = 0;	
		$where['goods_is_set_meal'] = 1;
		
		//搜索关键词
		if(!empty($_POST['keyword'])){
			//	$ext_goods = $this->extSearchGoods($_POST['keyword']);				
			//	$where['goods_id'] = array('in',$ext_goods);
		}
		
		
		
		$goods_list = $model_goods->getGoodsList($where,'*');		
		$page_count = $model_goods->gettotalpage();
		$list_count = $model_goods->gettotalnum();
		
			
		foreach($goods_list as $k=> $v){
			$goods_data[$k] = $v;
			$goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
			$goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
			//$goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);			
		}				
		output_data(array('list' => $goods_data), mobile_page($page_count,$list_count));			
	}
	
	
	
	
	
	
	private function goodsImageFormat($image){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['file_name'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS .$v;
			$list[$k]['file'] = $v;
			$list[$k]['type'] ='url';
		}
		
		return $list;		
	}
	
	
	
	/*
	
	*/
	public function get_goods_listOp(){
		
		$model_goods = Model('goods');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['is_delete'] = 0;
		$where['is_old'] = 0;
		$where['gc_id'] = $_POST['class_id'];
		
		$goods_list = $model_goods->getGoodsList($where, '*' ,'','goods_id desc');		
		foreach ($goods_list as $k=> $v) {
		    $goods_data[$k] = $v;
			$lang_name = $this->getLangGoods($v['goods_id']);
		    $goods_data[$k]['lang_name'] = $lang_name;
			
			
			$goods_data[$k]['size'] = $this->getSizeFormat($v['goods_code']);
			$goods_data[$k]['specs'] = $this->getSpecsFormat($v['goods_code']);
		
			if($v['goods_is_set_meal']){
				$goods_data[$k]['goods_meal'] = $this->getMealFormat($v['goods_code'],$lang_name[0]['lang_name']);
			}
				


		//	 $goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
		//   $goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);
		//   $goods_data[$k]['goods_ingr_ids'] = $v['goods_ingr'];
		}
		output_data(array('list' => $goods_data));
	}


	//套餐规格
	private function getMealFormat($goods_code,$lang = 'CHN-S'){
			
		$model = model('store_goods_meal');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_parent_id'] = 0;

	

		$list = $model->getMealList($where);
		$data = array();

		foreach($list as $k => $v){
			
			$data[$k]['name'] = $v['meal_value'] .' (SELECT '.$v['meal_buy_num'].')';
			
			$data[$k]['buy_num'] = $v['meal_buy_num'];
			
			$data[$k]['check_num'] = 0;

			$data[$k]['list'] = $this->getMealChildFormat($v['meal_id'],$goods_code,$lang);
		}		
		
		return $data;
	}

	
	private function getMealChildFormat($meal_id,$goods_code,$lang){
		$model = model('store_goods_meal');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_parent_id'] = $meal_id;
		$list = $model->getMealList($where);
		$data = array();
		foreach($list as $k => $v){
			$data[$k]['name'] = $v['meal_value'];
			$data[$k]['size_name'] = $v['meal_size_name'];
			$data[$k]['specs_name'] = $v['meal_specs_name'];
			$data[$k]['meal_price'] = $v['meal_price'];
			$data[$k]['meal_id'] = $v['meal_id'];			
			$data[$k]['active'] = 0;
		}				
		return $data;
		
	}

	
	// 读取扩展分类的商品
	private function extGoods($stc_id)
	{
	    $ext_goods =  model('store_goods_extended')->getGoodsExtList(array('stc_id'=> $stc_id,'store_id'=> $this->store_info['store_id']), 'goods_code');
	    if (!empty($ext_goods)) {
	        foreach ($ext_goods as $v) {
	            $goods .=$v['goods_code'].',';
	        }
	        $goods = substr($goods, 0, -1);
	    } else {
	        $goods = '';
	    }
	    return $goods;
	}
	
	
	
	//格式化 size 	
	private function getSizeFormat($goods_code,$lang = 'CHN-S'){
		
		$model = model('store_goods_size');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSizeList($where);
		$data = array();
		foreach($list as $k => $v){		
			 // $v['active'] = $k == 0 ? 1:0;
			 $v['active'] = 0;
			$data[$k] = $v;
		}		
		return $data;		
	}
	
	
	//格式化 specs
	private function getSpecsFormat($goods_code,$lang = 'CHN-S'){		
		
		$model = model('store_goods_specs');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['specs_type'] = 0;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){
			$v['child'] = $this->getSpecsChildFormat($v['specs_id'],$lang);
			$data[$k] = $v;
		}		
		return $data;		
	}
	
	
	
	private function getSpecsChildFormat($specs_id,$lang){
		
		$model = model('store_goods_specs');	
		$where = array();
		$where['specs_parent_id'] = $specs_id;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){			
			$v['active'] = 0;
			$data[$k] = $v;			
		}		
		return $data;
	}
	
	
	
	
	
	
	
	/**
	 * 新增
	 */
	public function get_goods_initOp() {
	 
		$goods_data = array();
					
		$goods_data['store_goods_class'] = $this->jsonStoreClass();		
		//语言包。
		$lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));		
		foreach($lang as $k=> $v){
			
				$goods_name[$k]['lang'] = $v['language_en'];
				$goods_name[$k]['goods_name'] = '';
				$goods_description[$k]['lang'] = $v['language_en'];
				$goods_description[$k]['goods_description'] = '';
				
				$data_meal_specs['goods_list'] = array();
				$data_meal_specs['lang'][$k]=array(
					'lang' => $v['language_en'],
					'class_name' => '',					
				);	
				
				$store_language[$k]['lang'] = $v['language_en'];
		}
		
		
		$goods_data['store_language'] = $store_language;
		
	
	
		//初始化数据		
		$goods = array();
		$goods['goods_name'] = $goods_name;
		$goods['goods_description'] = $goods_description;
		$goods['goods_meal_specs'][] = $data_meal_specs;
	
				
		$goods['goods_ingr'] = array();
		$goods['is_old'] = 0;
		$goods['goods_code'] = $this->getGoodsCode();	
		$goods['goods_optional'] = array();		
		$goods['goods_state'] = 1 ;
		$goods['is_recommend'] = 0;
		$goods['goods_stock'] = 66 ;			
		$goods_data['goods'] = $goods;	
	

		$goodscode = array(
			array(
				'goods_code' => $goods['goods_code'],
				'goods_code_num' => 0
			)
		);
		
		$goods_data['goodscode'] = $goodscode;
		$goods_data['goods_image'] = array();
		$goods_data['new_meal_specs'] = $data_meal_specs;
		
		
		output_data($goods_data);
	
	}
	
	
	
	private function jsonStoreClass($type = ''){
					
		$class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
	
		if($type == 'list'){
			$store_goods_class[] = array(
				'stc_id' => 'all',
				'stc_name' =>'全部'
			);
		}
		
		foreach($class as $v){
			$data = $v;
			$data['stc_id'] = $v['stc_id'];
			$data['active'] = 0;
			$data['stc_lang_name'] = unserialize($v['stc_lang_name']);						
		//	$data['goods_count'] = $this->goodsCount($v['stc_id']);
			$store_goods_class[] = $data;
		}
	
		return $store_goods_class;
	
	}
	
	
	//生成商品编号
	private function getGoodsCode(){
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		$count = model('goods')->getGoodsCount($where);
		
		$code = $this->store_info['store_code'].'_i'.($count + 1);
	
		return $code;
		
	}
	
	
	
	public function get_meal_infoOp(){
		
		$goods_data = array();
		$goods_data['store_goods_class'] = $this->jsonStoreClass(); //菜单
		$goods_id = $_POST['goods_id'];		
		$goods_info = model('goods')->getGoodsInfo(array('store_id'=> $this->store_info['store_id'],'goods_id'=> $goods_id));
		
		if(!empty($goods_info)){
		
			$goods_info['goods_optional'] = $this->get_new_options($goods_info['goods_code']);
			$goods_info['goods_image'] = $this->goodsImageFormat($goods_info['goods_image']);
			$goods_info['is_recommend'] = $goods_info['is_recommend']  ;
			$goods_info['goods_state'] = $goods_info['goods_state'] ;
		
			//语言包。
			$lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));		
			foreach($lang as $k=> $v){						
					$store_language[$k]['lang'] = $v['language_en'];
					
					
					$data_meal_specs['goods_list'] = array();
					$data_meal_specs['lang'][$k]=array(
						'lang' => $v['language_en'],
						'class_name' => '',					
					);	
					
					
			}
			
			
			$goods_data['store_language'] = $store_language;
		
		
			$lang_goods =  $this->getLangGoods($goods_info['goods_id']);
			$data  = array();
			foreach($lang_goods as $k => $v){			
				$goods_name[$k]['lang'] = $v['lang_name'];
				$goods_name[$k]['goods_name'] = $v['goods_name'];								
				$goods_description[$k]['lang'] = $v['lang_name'];
				$goods_description[$k]['goods_description'] = $v['goods_description'];				
			}
			
			
		
			$goods_info['goods_name'] = $goods_name;			
			$goods_info['goods_description'] = $goods_description;														
			$goods_info['goods_meal_specs'] = $this->get_meal_specs($goods_info['goods_code']);		
			
			$goods_info['is_old'] = 0;
			$goods_info['goods_code'] = $this->getGoodsCode();	
			
				
			
		}		
		
		
		$goods_data['goods'] = $goods_info;
		$goods_data['goods_image'] = $goods_info['goods_image'];
		$goods_data['new_meal_specs'] = $data_meal_specs;
		
		output_data($goods_data);
		
		
	}
	
	
	/*
	格式化套餐数据
	*/
	private function get_meal_specs($code){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['meal_parent_id'] = 0;				
		$where['is_old'] = 0;
		$data = model('store_goods_meal')->getMealList($where);
		$list = array();
		foreach($data as $k => $v){
			$meal_id = $v['meal_id'];		
			$list[$meal_id]['lang'] =$this->get_meal_specs_name($v['goods_code'],$meal_id);
			$list[$meal_id]['goods_list'] =  $this->get_meal_specs_goods($v['goods_code'],$meal_id);
						
		}
		
		foreach($list as $v){
			$new_list[] = $v;			
		}			
					
		return $new_list;
		
				
	}
	
	//套餐子数据集
	private function get_meal_specs_goods($code,$meal_id,$lang="CHN-S"){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['meal_parent_id'] =$meal_id;
		$where['lang'] = $lang;
		$where['is_old'] = 0;
		$data = model('store_goods_meal')->getMealList($where);
		$child = array();
		$size_model = model('store_goods_size');
		$specs_model = model('store_goods_specs');
		$goods_lang_model = model('goods_language');
		foreach($data as $k => $v){		
			
			$meal_id = $v['meal_id'];
			$child[$meal_id]['goods_name'] = $this->get_meal_specs_goodsname($code,$v['meal_id']);			
			$child[$meal_id]['price'] 	= $v['meal_price'];
			
			$size = $size_model->getSizeInfo(array('size_id' => $v['meal_size_id'],'lang' => $lang));			
			$child[$meal_id]['size'] 	= $size;
			
			$specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['meal_specs_id']),'lang' => $lang));			
			$child[$meal_id]['specs'] 	= $specs;
			$child[$meal_id]['goods_code'] = $v['meal_goods_code'];
		}
		
		foreach($child as $v){			
			$new_child[] = $v;			
		}		
		return $new_child;		
		
	}
	
	
	
	
	//格式化名字	
	private function get_meal_specs_name($code,$meal_id){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['meal_id'] = $meal_id;
		$where['is_old'] = 0;
		$data = model('store_goods_meal')->getMealList($where);
		foreach($data as $k => $v){						
			$name[$k]['lang'] = $v['lang'];			
			$name[$k]['class_name'] = $v['meal_value'];
			
		}
		return $name;	
	}
	
	//格式化名字
	private function get_meal_specs_goodsname($code,$meal_id){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['meal_id'] = $meal_id;
		$where['is_old']  = 0;
		$data = model('store_goods_meal')->getMealList($where);
		foreach($data as $k => $v){						
			$name[$k]['lang_name'] = $v['lang'];			
			$name[$k]['goods_name'] = $v['meal_value'];
			
		}
		return $name;	
	}
	
	
	
	
	/*
		编辑商品获取最新的配件信息
		*/
	
	private function get_new_options($code,$lang ='CHN-S'){
		   
		   $where = array();
		   $where['goods_code'] = $code;
		   $where['is_old'] = 0;
		   $where['goods_code_num'] = 0;
		   $options_list = model('store_goods_options')->getOptionsList($where);
		   $data = array();	   
		   $size_model = model('store_goods_size');
		   $specs_model = model('store_goods_specs');
		   $goods_lang_model = model('goods_language');
		   foreach($options_list as $k =>$v){		  
			  
			   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang));
			   $data[$k]['size'] = $size;		   
			   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
			   $data[$k]['specs'] = $specs;		   
			   $data[$k]['price'] = $v['options_price'];		   
			   $goods_name = $goods_lang_model->getGoodsLangList(array('goods_code' => $v['options_goods_code']),'lang_id asc','goods_name,lang_name');		   
			   $data[$k]['goods_name'] = $goods_name;
			   $data[$k]['goods_code'] = $v['options_goods_code'];
			   
		   }
		   
		   return $data;
		   
		   
	}
	
	
	private function getLangGoods($goods_id){
		$list = model('goods_language')->getGoodsLangList(array('goods_id' => $goods_id));
		
		$data = array();
		foreach($list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['lang_name'] =  $v['lang_name'];	
			$data[$k]['goods_set_meal_specs'] = unserialize($v['goods_set_meal_specs']);					
		}
		return $data;
	}
	
	

	
	
	
	
	//保存套餐
	public function save_mealOp(){
		
		
		$model= model('goods');
		$meal_model = model('store_goods_meal');
		$options_model = model('store_goods_options');
		$lang_model = model('goods_language');
		//获取商品
		$where = array(
			'goods_id' => $_POST['goods_id'],
			'store_id' => $this->store_info['store_id']
		);
		
		$goods_info = $model->getGoodsInfo($where);
	//	print_r($goods_info);
		if(!empty($goods_info)){
			
			$goods_code = $goods_info['goods_code'];
			$goods_code_num = $goods_info['goods_code_num'] + 1;			
		
			//编辑
			$data = array('is_old' => 1);
			$where = array('goods_id' => $goods_info['goods_id']);					
			$model->editGoodsNew($data,$where);			
			
			//更新语言
			
			$where = array();
			$where['goods_code'] = $goods_code;
			$where['store_id'] = $this->store_info['store_id'];
			$update = array();
			$update['is_old'] = 1;			
			$lang_model->editGoodsLang($update,$where);
			
			
			
			//替换套餐			
			$where = array();
			$where['goods_code'] = $goods_code;
			$where['store_id'] = $this->store_info['store_id'];
			$update = array();
			$update['is_old'] = 1;			
			$meal_model->editMeal($where,$update);
						
						
			//配件
			$where = array();
			$where['goods_code'] = $goods_code;
			$where['store_id'] = $this->store_info['store_id'];
			$update = array();
			$update['is_old'] = 1;
			$update['goods_code_num'] = $goods_code_num;
			$options_model->editOptions($where,$update);
			
					
			
		}else{			
			
			
			$goods_code  = $_POST['goods_code'];
			$goods_code_num = 0;
			
		}
				
		$data=  array();	
		$data['goods_name'] = $_POST['goods_name'][0]['goods_name'];
		$data['gc_id'] = $_POST['gc_id'];
		$data['store_name'] = $_POST['store_name'];		
		
		
		//获取商品分类名称
		$gc_name = model('store_goods_class')->getOneById($_POST['gc_id']);		
		
		$data['gc_name'] = $gc_name['stc_name'];
		$data['store_name'] = $this->store_info['store_name_primary'];
		$data['goods_price'] = 0;
		$data['goods_code'] = $goods_code;
		$data['goods_addtime'] = time();		
		$goods_image = $this->imageAll($_POST['goods_image']);	
		$data['goods_image'] = $goods_image;					
		$data['goods_state'] = $_POST['goods_state'] ;
		$data['store_id'] = $this->store_info['store_id'];
		$data['is_recommend'] = $_POST['is_recommend'] ;
		$data['goods_code_num'] = $goods_code_num;
		$data['goods_is_set_meal'] = 1;		
		
		$row = $model->addGoods($data);			
		
		if($row){	
			//更新分类表
			model('store_goods_extended')->delGoodsExt(array('goods_code'=>$goods_info['goods_code'],'store_id'=> $this->store_info['store_id']));	
			$data_goods_ext = array(
				'stc_id' => $_POST['gc_id'],
				'store_id' => $this->store_info['store_id'],
				'goods_code' => $goods_code
			);		
			model('store_goods_extended')->addGoodsExt($data_goods_ext);
			
			$lang = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));		
			foreach($lang as $v){				
				$data_sub = array();	
				$data_sub['goods_id'] = $row;
				$goods_name = $_POST['goods_name'];
				foreach($goods_name as $vv){
					if($v['language_en'] == $vv['lang']){
						$data_sub['goods_name'] = $vv['goods_name'];
					}
				}	
				$data_sub['store_id'] 	 = $v['store_id'];	
				$goods_description = $_POST['goods_description'];
				foreach($goods_description as $vv){
					if($v['language_en'] == $vv['lang']){
						$data_sub['goods_description'] = $vv['goods_description'];
					}
				}	
				$data_sub['lang_id']       = $v['language_id'];			
				$data_sub['lang_name']       = $v['language_en'];	
				$data_sub['goods_code']   = $goods_code;
				$data_sub['goods_code_num'] = $goods_code_num;
				$data_sub['goods_addtime'] = time();
				model('goods_language')->addGoodsLang($data_sub);				
			
			}
			
			//写入套餐
			$meal_spec = $_POST['goods_meal_specs'];			
			$model_size = model('store_goods_size');
			$model_specs = model('store_goods_specs');
		
			foreach($meal_spec as $k=> $v){				
				$meal_id = $goods_code . '|' . ($k+1);							
				foreach($v['lang'] as $lang){	
					
					$data = array();
					$data['meal_value'] = $lang['class_name'];
					$data['lang'] = $lang['lang'];
					$data['meal_type'] = 0 ;
					$data['meal_id'] = $meal_id;
					$data['goods_code'] = $goods_code;	
					$data['meal_parent_id'] = 0;
					$data['store_id'] = $this->store_info['store_id'];
					$data['goods_code_num'] = $goods_code_num;
					$parent_id = model('store_goods_meal')->addMeal($data);		
												
				}	
				
				foreach($v['goods_list'] as $g => $goods){		
					
					$child_id = $goods_code . '|' . ($k+1) .'|'.($g+1);					
				
					foreach($goods['goods_name'] as $gn){
						
						$child_data = array();
						$child_data['meal_value'] = $gn['goods_name'];
						$child_data['lang'] 		= $gn['lang_name'];
						$child_data['meal_type'] 	= 1 ;
						$child_data['meal_id'] 	= $child_id;
						$child_data['goods_code'] = $goods_code;
						$child_data['meal_parent_id'] = $meal_id;
						$child_data['meal_size_id'] = $goods['size']['size_id'];		
						$child_data['store_id'] = $this->store_info['store_id'];
						//查询sizename
						
						$sizename = $model_size->getSizeInfo(array('size_id' => $goods['size']['size_id'],'lang' => $gn['lang_name']));
						
						$child_data['meal_size_name'] = $sizename['size_value'];
										
						if(is_array($goods['specs'])){				
							$specs_id = array();					
							foreach($goods['specs'] as $specs){
								$specs_id[] =$specs['specs_id'];						
							}						
							$specs_id = implode(',',$specs_id);						
							$child_data['meal_specs_id'] = $specs_id;						
							$where = array();
							$where['specs_id'] = array('in',$specs_id);
							$where['lang'] = $gn['lang_name'];					
							$specsname = $model_specs->getSpecsList($where);					
							$specs_name = array();
							foreach($specsname as $name){
								$specs_name[] = $name['specs_value'];
							}
							$specs_name = implode(',',$specs_name);						
							$child_data['meal_specs_name'] 	= $specs_name;		
						}							
															
																				
						$child_data['meal_price'] 	= $goods['price'];
						$child_data['meal_goods_code'] = $goods['goods_code'];
						$child_data['goods_code_num'] = $goods_code_num;
						
						model('store_goods_meal')->addMeal($child_data);
					}					
				}				
			}
									
			//写入配件
			
			$options = $_POST['goods_optional'];
			if(!empty($options)){
				foreach($options as $v){
					$data = array();
					$data['goods_code'] 	= $goods_code;
					$data['goods_size_id'] 	= $v['size']['size_id'];								
					$specs_id = '';
					if(!empty($specs)){
						$specs_id = [];				
						foreach($v['specs'] as $specs){
							$specs_id[] 		= $specs['specs_id'];
						}
						$specs_id = implode(',',$specs_id);	 
					}
					$data['goods_specs_id'] 		= $specs_id;
					$data['options_goods_code'] 	= $v['goods_code'];
					$data['options_price'] 			= $v['price'];	
					$data['store_id'] = $this->store_info['store_id'];
					model('store_goods_options')->addOptions($data);	
				}
			}
			
			//$meal_list = model('store_goods_meal')->getMealList();
			
			
			//检查所有的套餐属性。获取最小金额和最大金额。
			
			
			//检查所有套餐属性，获取INGR。
			
			
			
			
			
			
			
			
			//output_error('编辑失败');	
			output_data(array('message' => '编辑成功'));			
		}else{
			output_error('编辑失败');			
		}
		
	}
	
	//删除套餐
	public function del_mealOp(){
		
		
	}
	
	
	
	//上传商品图片
	
	public function  upload_imageOp(){
		
		$store_id = $this->store_info['store_id'];
	    $upload = new UploadFile();
	    $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath()); 
	    $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
	    $result = $upload->upfile('file');
	    if (!$result) {
			output_error($upload->error);
			
	    }else{
			$img_path = $upload->getSysSetPath() . $upload->file_name;
			output_data(array('image' => $img_path));			
		}
		
	}
	
	
	//保存裁剪的图片
	public function save_imageOp(){
	
	   $upload = new UploadFile();
	   $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();		
	   $file_name = $this->base64_image_content($_POST['image'],$path);
	
			output_data(array('image' => $file_name));		
		
	}
	
	private function imageAll($imagelist){
		$upload = new UploadFile();
	    $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();		
	
		$file_name = array();
		foreach($imagelist as $v){
			if($v['type'] == 'url'){
				$file_name[] = $v['file'];
			}else{
				$file_name[] = $this->base64_image_content($v['file_name'],$path);
			}
		}	
		
		return implode(",", $file_name);		
	}
	
	
	private function  base64_image_content($base64_image_content,$path){
	//匹配出图片的格式
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
			$type = $result[2];
			$new_file = $path;
			if(!file_exists($new_file)){
				//检查是否有该文件夹，如果没有就创建，并给予最高权限
				mkdir($new_file, 0700);
			}
			$file_name = time().rand(10000,99999).".{$type}";
			$new_file = $new_file.$file_name ;
			if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
				return $file_name;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	
	
	
	
}

?>