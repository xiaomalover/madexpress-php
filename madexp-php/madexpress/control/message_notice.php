<?php
/**
 * 管理
 */
defined('InMadExpress') or exit('Access Invalid!');
class message_noticeControl extends mobileAdminControl{
 
	public function __construct(){
        parent::__construct();   
    }

	//素材分类
	public function notice_cms_classOp(){		
		$where = array();		
		$list = model('notice_article_class')->getArticleClassList(TRUE);	
		output_data($list);		
	}
	
	//保存分类
	public function notice_cms_class_saveOp(){		
		$model = model('notice_article_class');		
		$data = array();
		$data['class_name'] = $_POST['class_name'];
		$data['class_sort'] = $_POST['class_sort'];		
		$row = $model->addArticleClass($data);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	//删除分类
	public function notice_cms_class_delOp(){		
		$model = model('notice_article_class');		
		$where = array();
		$where['class_id'] = $_POST['class_id'];		
		$row = $model->delArticleClass($where);
		if($row){
			output_data('删除成功');
		}else{
			output_error('删除失败');
		}		
	}
	
	
	
		
	//内容列表
	public function notice_cmsOp(){		
		$model = model('notice_article');		
		$where = array();				
		$list = $model->getArticleListNew($where,'*',$this->page);
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();		
		$data = array();
		foreach($list as $k=> $v){		
			
			$v['article_title_lang'] = unserialize($v['article_title_lang']);						
			$v['article_image'] = $v['article_image'];
			$v['article_addtime'] = date('d M Y',$v['article_addtime']);
			$data[$k] = $v;			
		}
		
		output_data(array('list' => $data), mobile_page($page_count,$list_count));
		
		
	}
	
	//调用语言配置
	private function get_system_lang(){		
		$system_lang = model('language')->getLangList(array('language_system' => 1));		
		return $system_lang;
	}
	
	
	//上传前的数据
	public function notice_cms_initOp(){
		
		$system_lang = $this->get_system_lang();
		$data = array();		
		foreach($system_lang as $k => $v){
			$data[$k]['lang_name'] = $v['language_name'];
			$data[$k]['lang_code'] = $v['language_flag'];			
			$data[$k]['article_content'] = '';
			$data[$k]['article_title'] = '';
			$data[$k]['article_images'] = '';			
		}
				
		output_data($data);	
	}
	
	
	//添加素材
	public function notice_cms_saveOp(){		
		
		$lang = $_POST['lang'];
		$data = array();
		$model = model('notice_article');		
		$data = array();		
		$data['article_addtime'] = time();		
		
		
		
		
		
		//更新标题		
		foreach($lang as $v){			
			$t['lang_name'] = $v['lang_name'];
			$t['article_title'] = $v['article_title'];	
			$title[] = $t;
			
			$c['lang_name'] = $v['lang_name'];
			$c['article_content'] =  $v['article_content'];	
			$content[] = $c;
			
		}		
		
		$data['article_data_lang'] = serialize($content);
		
		$data['article_title_lang'] = serialize($title);				
		
		if(intval($_POST['id'] > 0)){
			$where = array();
			$where['article_id'] = $_POST['id'];
			$row = $model ->editArticle($where,$data);
		}else{
			$row = $model->addArticle($data);
		}	
		
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	
	//编辑内容
	public function notice_cms_infoOp(){
		
		$cms = model('notice_article');
		$where = array();
		$where['article_id'] = $_POST['id'];
				
		$row = $cms->getArticleInfo($where);
		
		
		
		if($row){
			$content= unserialize($row['article_data_lang']);
			$title = unserialize($row['article_title_lang']);
				
			
			$system_lang = $this->get_system_lang();
			$data = array();		
			foreach($system_lang as $k => $v){
				$data[$k]['lang_name'] = $v['language_name'];
				$data[$k]['lang_code'] = $v['language_flag'];			
				$data[$k]['article_content'] =  $content[$k]['article_content'];
				$data[$k]['article_title'] = $title[$k]['article_title'];
				$data[$k]['article_images'] = '';			
			}
					
		
			output_data(array('info' => $data,'id'=>$row['article_id']));
		}else{
			output_error('获取失败');
		}
		
		
		
		
		
	}
	
	
	//删除
	public function notice_cms_delOp(){
		
		$model = model('notice_article');
		$where = array();
		$where['article_id'] = $_POST['id'];
		
		$row = $model->delArticle($where);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}		
	}
	
	
	
	//上传图片
	public function uploadOp(){
		
		
		
		
	}


	
}
