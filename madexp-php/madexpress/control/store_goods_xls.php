<?php
/**
 * 商品管理
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_goods_xlsControl extends mobileSellerControl
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    //获取是否有上传的菜单。
    
    public function get_xls_initOp()
    {
        $xls_goods = model('store_goods_xls')->getGoodsXlsCount(array('store_id'=> $this->store_info['store_id'],'state'=> 0));        
        output_data(array('xls_goods' => $xls_goods));
    }
    
    
    
    public function get_goods_classOp()
    {
		$type = $_POST['type'];
        $class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
        $store_goods_class = array();
        if ($type == 'list') {
            $store_goods_class[] = array(
                'stc_id' => 'all',
                'stc_name' =>'全部菜品',
				'stc_count' => $this->goodsCount(0)
            );
			
			$store_goods_class[] = array(
				'stc_id' => 'recommend',
				'stc_name' => '推荐商品'
			);
			
        }
      
        foreach ($class as $v) {
            $data = $v;
            $data['stc_id'] = $v['stc_id'];
            $data['active'] = 0;
            $data['stc_lang_name'] = unserialize($v['stc_lang_name']);
            $data['goods_count'] = $this->goodsCount($v['stc_id']);
            $store_goods_class[] = $data;
        }
        output_data(array('cate_list' => $store_goods_class));
    }
	
	
	
    
    public function get_goods_class_infoOp()
    {
        
        //初始化class
        $lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));
        $classInfo = array();
        foreach ($lang as $k=> $v) {
            $class_lang[$k]['lang_name']	= $v['language_en'];
            $class_lang[$k]['class_name'] = '';
        }
        $classInfo['lang_name'] = $class_lang;
        
                
        output_data(array('class_info' => $classInfo));
    }
    
    
    
    
    
    
    

    //获取商品列表
    public function get_goods_listOp()
    {
        $model_goods = Model('goods');
        $where = array();
        $where['store_id'] = $this->store_info['store_id'];
        $where['is_delete'] = 0;
        $where['is_old'] = 0;
		$where['goods_is_set_meal'] = 0;
		$where['import_state'] = array('in','0,1,2');
        
        if ($_POST['class_id'] ==  'recommend') {
            $where['is_recommend'] = 1;
        }
        
        if ($_POST['is_state_down'] > 0) {
            $where['goods_state'] = 0;
        }
        
        if ($_POST['is_state_up'] > 0) {
            $where['goods_state'] = 1;
        }
                
        
        if (intval($_POST['class_id']) > 0) {
         //   $ext_goods = $this->extGoods($_POST['class_id']);
            $where['gc_id'] = $_POST['class_id'];
        }
        
        //搜索关键词
        if (!empty($_POST['keyword'])) {
            $ext_goods = $this->extSearchGoods($_POST['keyword']);
            $where['goods_id'] = array('in',$ext_goods);
        }
        
        
        
        $goods_list = $model_goods->getGoodsList($where, '*','','goods_id desc');
        $page_count = $model_goods->gettotalpage();
        $list_count = $model_goods->gettotalnum();
        
    
        foreach ($goods_list as $k=> $v) {
            $goods_data[$k] = $v;
            $goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
            $goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
            $goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);
            $goods_data[$k]['goods_ingr_ids'] = $v['goods_ingr'];
        }
        output_data(array('list' => $goods_data), mobile_page($page_count, $list_count));
    }
    
	
	
	
	
	
	
	
	
	
	public function get_options_goods_listOp(){
		
		$model_goods = Model('goods');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['is_delete'] = 0;
		$where['is_old'] = 0;
		$where['is_options'] = 1;
		$goods_list = $model_goods->getGoodsList($where, '*');
		
		foreach ($goods_list as $k=> $v) {
		    $goods_data[$k] = $v;
		    $goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
			$goods_data[$k]['size'] = $this->getSizeFormat($v['goods_code']);
			$goods_data[$k]['specs'] = $this->getSpecsFormat($v['goods_code']);
	//	    $goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
		 //   $goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);
		 //   $goods_data[$k]['goods_ingr_ids'] = $v['goods_ingr'];
		}
		output_data(array('list' => $goods_data));
	}
	
	//格式化 size 	
	private function getSizeFormat($goods_code,$lang = 'CHN-S'){
		
		$model = model('store_goods_size');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSizeList($where);
		$data = array();
		foreach($list as $k => $v){		
			$v['active'] = $k == 0 ? 1:0;
			$data[$k] = $v;
		}		
		return $data;
		
	}
	
	//格式化 specs
	private function getSpecsFormat($goods_code,$lang = 'CHN-S'){		
		
		$model = model('store_goods_specs');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['specs_type'] = 0;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){
			$v['child'] = $this->getSpecsChildFormat($v['specs_id'],$lang);
			$data[$k] = $v;
		}
		
		return $data;
		
	}
	
	private function getSpecsChildFormat($specs_id,$lang){
		
		$model = model('store_goods_specs');	
		$where = array();
		$where['specs_parent_id'] = $specs_id;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){			
			$v['active'] = $k == 0 ? 1:0;
			$data[$k] = $v;			
		}		
		return $data;
	}
	
	
	
	//查看已关联此配件商品的主商品
	public function get_options_goodsOp(){
		
			$model_goods = Model('goods');
			$where = array();
			$where['store_id'] = $this->store_info['store_id'];
			$where['is_delete'] = 0;
			$where['is_old'] = 0;
			$where['is_options'] = 0;
			$where['goods_optional_ids'] = array('like','%,'.$_POST['goods_id'].',%');
			$goods_list = $model_goods->getGoodsList($where, '*');
			
			foreach ($goods_list as $k=> $v) {
			    $goods_data[$k] = $v;
			    $goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
		//	    $goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
			 //   $goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);
			 //   $goods_data[$k]['goods_ingr_ids'] = $v['goods_ingr'];
			}
			output_data(array('list' => $goods_data));
		
		
	}
	
    
    // 读取扩展分类的商品
    private function extGoods($stc_id)
    {
        $ext_goods =  model('store_goods_extended')->getGoodsExtList(array('stc_id'=> $stc_id,'store_id'=> $this->store_info['store_id']), 'goods_code');
        if (!empty($ext_goods)) {
            foreach ($ext_goods as $v) {
                $goods .=$v['goods_code'].',';
            }
            $goods = substr($goods, 0, -1);
        } else {
            $goods = '';
        }
        return $goods;
    }
    
    
    
    
    //搜索
    private function extSearchGoods($keyword)
    {
        $where = " goods_name like '%".$keyword."%' or goods_description  like '%".$keyword."%' or goods_specs like '%".$keyword."%' or goods_sizeprice  like '%".$keyword."%'";
        
        $ext_goods =  model('goods_language')->getGoodsLangList($where, 'goods_id');
    

        if (!empty($ext_goods)) {
            foreach ($ext_goods as $v) {
                $goods[$v['goods_id']] = $v['goods_id'];
            }
            $goods = implode(',', $goods);
        } else {
            $goods = '';
        }
        return $goods;
    }
    
    
    //商品ICON
    
    private function goodsIngr($ingr)
    {
        $where = array(
            'attr_id' => array('in',$ingr)
        );
        $ingr = model('goods_attr')->getGoodsAttrList($where);
        $ingr_data = array();
        foreach ($ingr as $k=> $v) {
            $ingr_data[$k]['attr_id'] =  $v['attr_id'];
            if ($v['attr_type'] == 1) {
                $new = explode("|", $v['attr_content']);
                $ingr_data[$k]['attr_name'] = $new[0];
                $ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
                $ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
            } else {
                $ingr_data[$k]['attr_name'] =  $v['attr_name'];
                $ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];
                $ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];
            }
        }
        return $ingr_data;
    }
    
    
    

    
    //上下架
    public function state_optionsOp()
    {
        $where = array(
            'options_id' => $_POST['options_id']
        );
        $data = array(
            'state' => $_POST['state']
        );
        $row = model('goods_options')->editOptions($where, $data);
        if ($row) {
            output_data(array('message' => '操作成功'));
        } else {
            output_error('操作失败');
        }
    }
        
		
		
		
		
		
    

    
    //推荐

    public function state_switchOp()
    {
        $where = array(
            'store_id' => $this->store_info['store_id'],
            'is_recommend' => 1
        );
        $count = model('goods')->getGoodsCount($where);
        if ($count >= 5 && $_POST['state'] == 1) {
            output_error('最多只能推荐5个商品');
        } else {
            $data = array(
                'is_recommend' => $_POST['state']
            );
            $where = array(
                'goods_id' => $_POST['goods_id'],
                'store_id' => $this->store_info['store_id']
            );
            $row = model('goods')->editGoodsNew($data, $where);
            if ($row) {
                output_data(array('message' => '操作成功'));
            } else {
                output_error('操作失败');
            }
        }
    }
	
	
	public function state_goods_switchOp(){
		
		$data = array(
		    'goods_state' => $_POST['state']
		);
		$where = array(
		    'goods_id' => $_POST['goods_id'],
		    'store_id' => $this->store_info['store_id']
		);
		$row = model('goods')->editGoodsNew($data, $where);
		if ($row) {
		    output_data(array('message' => '操作成功'));
		} else {
		    output_error('操作失败');
		}
		
		
	}
	
	public function options_goods_switchOp(){
		
		$data = array(
		    'is_options' => $_POST['state']
		);
		$where = array(
		    'goods_id' => $_POST['goods_id'],
		    'store_id' => $this->store_info['store_id']
		);
		$row = model('goods')->editGoodsNew($data, $where);
		if ($row) {
		    output_data(array('message' => '操作成功'));
		} else {
		    output_error('操作失败');
		}
		
		
	}


    //菜品分类
    private function goodsClass()
    {
        $store_goods_class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
        $data = array();
        foreach ($store_goods_class as $k=> $v) {
            $data[$k] = $v;
            $data[$k]['goods_count'] = model('goods')->getGoodsCount(array('gc_id'=> $v['stc_id']));
        }
        return $data;
    }
    
    //保存分类
    public function save_classOp()
    {
        $model =  model('store_goods_class');
        $data= array();
        $data['stc_lang_name'] = serialize($_POST['lang_name']);
        $data['stc_sort'] = 0;
        $count = $model->getStoreClassCount(array('store_id'=> $this->store_info['store_id']));
    
        $data['stc_code'] = $this->store_info['store_code'].'-'.$count+1;
        $data['stc_name'] = $_POST['lang_name'][0]['class_name'];
        $data['stc_hidden'] = $_POST['class_hidden'];
        if ($data['stc_name'] == '') {
            output_error('分类名称为空');
        }
        
        $data['store_id'] = $this->store_info['store_id'];
        
        $row = $model->addStoreGoodsClass($data);
        if ($row) {
            output_data(array('message' => '保存成功'));
        } else {
            output_error('保存失败');
        }
    }
    
    
    public function batch_class_saveOp()
    {
        $model =  model('store_goods_class');
        $data = $_POST['cate'];
        
        foreach ($data as $v) {
			if($v['stc_id'] > 0){
				$update = array(
					'stc_name' => $v['stc_lang_name'][0]['class_name'],
					'stc_lang_name' => serialize($v['stc_lang_name']),
					'stc_hidden' => $v['stc_hidden']
				);
				$where = array(
					'stc_id' => $v['stc_id'],
					'store_id' => $v['store_id']
				);
				model('store_goods_class')->editStoreGoodsClass($update, $where);
			}
			
        }
        
        output_data(array('message'=> '保存成功'));
    }
    
    
    //删除商品分类
    public function drop_classOp()
    {
        $where = array(
            'stc_id' => $_POST['class_id'],
            'store_id' => $this->store_info['store_id']
        );
		
		
		
		$count = model('store_goods_class')->getGoodsExtCount($where);
		if($count > 0){
			 output_error('分类下存在菜品，无法删除');
		}
		
        $row = model('store_goods_class')->delStoreGoodsClass($where);
        if ($row) {
            //删除扩展分类
            model('store_goods_extended')->delGoodsExt($where);
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    /**
     * 新增
     */
    public function get_goods_initOp()
    {
        $goods_data = array();
                    
        $goods_data['store_goods_class'] = $this->jsonStoreClass();
        $foodbox = Model('foodbox_goods')->getGoodsList(true);
        
        $goods_data['foodbox'] = $foodbox;

        //图标分类
        $ingr_class = model('goods_attr')->getClassList(true);
        $ingr_data = array();
        foreach ($ingr_class as $k => $v) {
            $v['child'] = $this->getIngrList($v['gc_id']);
            $ingr_data[$k] = $v;
        }
        $goods_data['ingr'] = $ingr_data;
        
        
        //语言包。
        $lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));
        foreach ($lang as $k=> $v) {
            $goods_name[$k]['lang'] = $v['language_en'];
            $goods_name[$k]['goods_name'] = '';
            $goods_description[$k]['lang'] = $v['language_en'];
            $goods_description[$k]['goods_description'] = '';
                
					
          
			$data_specs['specs_buy_num'] = 1; //可允许购买属性
            $data_specs['name'][$k]=array(
                    'lang' => $v['language_en'],
					'specs_value' => ''					
            );				
			$child_lang[$k] = array(
				'lang' => $v['language_en'],
				'specs_value' => ''
			);				
            $data_specs['child'][0]=array(
                    'lang' => $child_lang,            		
					'specs_price' => 0
            );  
				
				
				
				
            $data_size['box_id'] ='';
            $data_size['price'] = '';
            $data_size['sale_price'] = '';
            $data_size['lang'][$k]=array(
                    'lang' => $v['language_en'],
                    'name' => '',
                );
            $store_language[$k]['lang'] = $v['language_en'];
        }        
          
        $goods_data['store_language'] = $store_language;
			
			

        //初始化数据
        $goods = array();
        $goods['goods_name'] = $goods_name;
        $goods['goods_description'] = $goods_description;
        $goods['goods_specs'][] = $data_specs;
        $goods['goods_sizeprice'][] = $data_size;
       
        $goods['goods_ingr'] = array();
        $goods['is_old'] = 0;
        $goods['goods_code'] = $this->getGoodsCode();
        $goods['goods_optional'] = array();
        $goods['goods_state'] = 1 ;
        $goods['is_recommend'] = 0;
        $goods['goods_stock'] = 66 ;
		$goods['is_options'] = 0 ;
        
        $goods_data['goods'] = $goods;
        
       
		
        $goodscode = array(
            array(
                'goods_code' => $goods['goods_code'],
                'goods_code_num' => 0
            )
        );
        $goods_data['new_specs']  = $data_specs;
        $goods_data['new_specs_child'] = $data_specs['child'][0];
        $goods_data['goodscode'] = $goodscode;
        $goods_data['goods_image'] = array();
        
        
        output_data($goods_data);
    }
    
    
    
    private function getIngrList($class_id,$ingr_ids = '')
    {
        $where = array();
        $where['attr_class_id'] = $class_id;
    	$where['attr_type'] = array('in','0,1');
        $ingr = model('goods_attr')->getGoodsAttrList($where);
        $ingr_data = array();		
    	if(!empty($ingr_ids)){			
    		$ingr_ids = explode(',',$ingr_ids);			
    	}		
        foreach ($ingr as $k=> $v) {
    			$ingr_data[$k]['attr_id'] =  $v['attr_id'];
    			$ingr_data[$k]['disabled'] = $v['disabled'];              
            if ($v['attr_type'] == 1) {
                $new = explode("|", $v['attr_content']);
                $ingr_data[$k]['attr_name'] = $new[0];
                $ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
                $ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
            } else {
                $ingr_data[$k]['attr_name'] =  $v['attr_name'];
                $ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];
                $ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];
    			
            }
    		$ingr_data[$k]['active'] = 0;			
    		
    		if(!empty($ingr_ids)){
    			if(in_array($v['attr_id'],$ingr_ids)){					
    				$ingr_data[$k]['active'] = 1;		
    			}
    		}			
        }        
        return $ingr_data;
    }
    
    
    
    //生成商品编号
    private function getGoodsCode()
    {
        $where = array(
            'store_id' => $this->store_info['store_id']
        );
        $count = model('goods')->getGoodsCount($where);
        
        $code = $this->store_info['store_code'].'_i'.($count + 1);
    
        return $code;
    }
    
    

    public function get_goods_infoOp()
    {
        $goods_data = array();
        $goods_data['store_goods_class'] = $this->jsonStoreClass(); //菜单
        $goods_id = $_POST['goods_id'];
        
        if ($goods_id > 0) {
            $goods_info = model('goods')->getGoodsInfo(array('store_id'=> $this->store_info['store_id'],'goods_id'=> $goods_id));
            //$goods_info['lang_goods'] = $this->getLangGoods($goods_info['goods_id']);
            $goods_info['goods_optional'] = $this->get_new_options($goods_info['goods_code']);
            $goods_info['goods_image'] = $this->goodsImageFormat($goods_info['goods_image']);
            $goods_info['is_recommend'] = $goods_info['is_recommend'] ;
            $goods_info['goods_state'] = $goods_info['goods_state'] ;
            $goods_info['goods_is_specs'] = $goods_info['goods_is_specs'];
            $goods_info['is_options'] = $goods_info['is_options'] ;
    		$goods_info['goods_ingr'] = $goods_info['goods_ingr'] ? $goods_info['goods_ingr'] : '';
    		
            $lang_goods =  $this->getLangGoods($goods_info['goods_id']);
            $data  = array();
            
            foreach ($lang_goods as $k => $v) {
                $goods_name[$k]['lang'] = $v['lang_name'];
                $goods_name[$k]['goods_name'] = $v['goods_name'];
                $goods_description[$k]['lang'] = $v['lang_name'];
                $goods_description[$k]['goods_description'] = $v['goods_description'];
                
               /* foreach ($v['goods_specs'] as $s => $specs) {
                    $data_specs[$s]['lang'][$k]['lang'] = $v['lang_name'];
                    $data_specs[$s]['lang'][$k]['name'] = $specs['name'];
                    $data_specs[$s]['lang'][$k]['value'] = $specs['value'];
                    $data_specs[$s]['price'] = $specs['price'];
    				$data_specs[$s]['spec_id'] = $k+1;
                }
                                
                
                foreach ($v['goods_sizeprice'] as $s => $size) {
                    $data_size[$s]['lang'][$k]['lang'] = $v['lang_name'];
                    $data_size[$s]['lang'][$k]['name'] = $size['name'];
                    $data_size[$s]['box_id'] = $size['box_id'];
                    $data_size[$s]['price'] = $size['price'];
                    $data_size[$s]['sale_price'] = $size['sale_price'];
    				$data_size[$s]['size_id'] = $s+1;
                }
    			*/
    		   
    			
    			   
    		   
            }
            
    
            
            $goods_info['goods_name'] = $goods_name;
            $goods_info['goods_description'] = $goods_description;
            $goods_info['goods_specs'] = $this->get_new_specs($goods_info['goods_code']);
            $goods_info['goods_sizeprice'] = $this->get_new_size($goods_info['goods_code']);
    		
    		
        }
        
    	
        
        
        $foodbox = Model('foodbox_goods')->getGoodsList(true);        
        $goods_data['foodbox'] = $foodbox; //菜单    
        $lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));
        foreach ($lang as $k=> $v) {  		
    		
            $store_language[$k]['lang'] = $v['language_en'];
    		          
    		$data_specs['specs_buy_num'] = 1; //可允许购买属性
    		$data_specs['name'][$k]=array(
    		        'lang' => $v['language_en'],
    				'specs_value' => ''					
    		);				
    		$child_lang[$k] = array(
    			'lang' => $v['language_en'],
    			'specs_value' => ''
    		);				
    		$data_specs['child'][0]=array(
    		        'lang' => $child_lang,            		
    				'specs_price' => 0
    		);  
    			   
    		$data_size['box_id'] ='';
    		$data_size['price'] = '';
    		$data_size['sale_price'] = '';
    		$data_size['lang'][$k]=array(
    		        'lang' => $v['language_en'],
    		        'name' => '',
    		);
        }
    		   
        if(empty($goods_info['goods_specs'])){			
    		$goods_info['goods_specs'][] = $data_specs;			
    	}
    	
    	
    	
     //   $goods_data['goods_sizeprice'][] = $data_size;
     //   $goods_data['goods_specs'][] = $data_specs;		
        $goods_data['store_language'] = $store_language;                 
    	$goods_data['new_specs']  = $data_specs;
    	$goods_data['new_specs_child'] = $data_specs['child'][0];
    	$goods_data['new_size'] = $data_size;
       /* $ingr = model('goods_attr')->getGoodsAttrList(true);
        $ingr_data = array();
        $goods_ingr = explode(',', $goods_info['goods_ingr']);
        foreach ($ingr as $k=> $v) {
            $ingr_data[$k]['attr_id'] =  $v['attr_id'];
            if ($v['attr_type'] == 1) {
                $new = explode("|", $v['attr_content']);
                $ingr_data[$k]['attr_name'] = $new[0];
                $ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
                $ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
            } else {
                $ingr_data[$k]['attr_name'] =  $v['attr_name'];
                $ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];
                $ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];
            }
            $ingr_data[$k]['active'] = 0;
            foreach ($goods_ingr as $gi) {
                if ($v['attr_id'] == $gi) {
                    $ingr_data[$k]['active'] = 1;
                }
            }
        }*/
        
    	
    	//图标分类
    	$ingr_class = model('goods_attr')->getClassList(true);
    	$ingr_data = array();
    	foreach ($ingr_class as $k => $v) {
    	    $v['child'] = $this->getIngrList($v['gc_id'],$goods_info['goods_ingr']);
    	    $ingr_data[$k] = $v;
    	}		
    	
        $goods_data['ingr'] = $ingr_data;  
        $goods_data['goods_image'] = $goods_info['goods_image'];            
        //获取此商品的历史记录
        $goods_data['goodscode'] = $this->oldGoods($goods_info['goods_code']);    
        $goods_data['goods'] = $goods_info;
        
        
        output_data($goods_data);
    }
    
	/*
	编辑商品获取最新的配件信息
	*/
   
   private function get_new_options($code,$lang ='CHN-S'){
	   
	   $where = array();
	   $where['goods_code'] = $code;
	   $where['is_old'] = 0;
	   $where['goods_code_num'] = 0;
	   $options_list = model('store_goods_options')->getOptionsList($where);
	   $data = array();	   
	   $size_model = model('store_goods_size');
	   $specs_model = model('store_goods_specs');
	   $goods_lang_model = model('goods_language');
	   foreach($options_list as $k =>$v){		  
		  
		   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang));
		   $data[$k]['size'] = $size;		   
		   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
		   $data[$k]['specs'] = $specs;		   
		   $data[$k]['price'] = $v['options_price'];		   
		   $goods_name = $goods_lang_model->getGoodsLangList(array('goods_code' => $v['options_goods_code']),'lang_id asc','goods_name,lang_name');		   
		   $data[$k]['goods_name'] = $goods_name;
		   $data[$k]['goods_code'] = $v['options_goods_code'];
	   }
	   
	   return $data;
	   
	   
   }
	
	
	/*
	编辑商品获取最新属性规格	
	*/
	
	
	private function  get_new_size($code){
		
		$where = array();
		$where['goods_code'] = $code;	
		$data = model('store_goods_size')->getSizeList($where);			
		$data_size = array();
		foreach($data as $v){			
			$size_id = $v['size_id'];			
			$data_size[$size_id]['box_id'] = $v['size_box_id'];
			$data_size[$size_id]['price'] = $v['size_price'];
			$data_size[$size_id]['sale_price'] = $v['size_sale_price'];
			$data_size[$size_id]['lang'] = $this->get_new_size_lang($code,$size_id);
		}			
					
		
		foreach($data_size as $v){			
			$new_child[] = $v;			
		}					
					
		return $new_child;
	}
	
	private function get_new_size_lang($code,$size_id){
		$where = array();
		$where['goods_code'] = $code;	
		$where['size_id'] = $size_id;
		$data = model('store_goods_size')->getSizeList($where);
		$lang = array();
		foreach($data as $k => $v){		
			$lang[$k]['lang'] =  $v['lang'];
			$lang[$k]['name'] =  $v['size_value'];
		}
		return $lang;
	}
	
	
	
	//新specs
	private function get_new_specs($code){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['specs_parent_id'] = 0;
		$data = model('store_goods_specs')->getSpecsList($where);
		$list = array();
		foreach($data as $k => $v){
			$specs_id = $v['specs_id'];									
			$list[$specs_id]['specs_buy_num'] = $v['specs_buy_num'];
			$list[$specs_id]['name'] = $this->get_new_specs_name($code,$specs_id);
			$list[$specs_id]['child'] = $this->get_new_specs_child($code,$specs_id);
						
		}	
		$new_list = array();
		foreach($list as $v){
			$new_list[] = $v;			
		}			
			
		return $new_list;
	}
	
	private function get_new_specs_child($code,$specs_id){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['specs_parent_id'] =$specs_id;
		$data = model('store_goods_specs')->getSpecsList($where);
		$child = array();
		foreach($data as $k => $v){
		
			$child[$v['specs_id']]['lang'] = $this->get_new_specs_name($code,$v['specs_id']);			
			$child[$v['specs_id']]['specs_price'] = $v['specs_price'];
			
		}
		$new_child= array();
		foreach($child as $v){			
			$new_child[] = $v;			
		}		
		return $new_child;
		
	}
	
	
	private function get_new_specs_name($code,$specs_id){
		
		$where = array();
		$where['goods_code'] = $code;
		$where['specs_id'] =$specs_id;
		$data = model('store_goods_specs')->getSpecsList($where);
		foreach($data as $k => $v){						
			$name[$k]['lang'] = $v['lang'];			
			$name[$k]['specs_value'] = $v['specs_value'];
			
		}
		return $name;		
		
	}
	
	

    
    //获取商品的历史编辑记录
    private function oldGoods($goods_code)
    {
        $where = array(
            'goods_code' => $goods_code,
            'store_id' => $this->store_info['store_id']
        );
        $data = model('goods')->getGoodsList($where, 'goods_id,goods_code,goods_code_num,goods_id', '', 'goods_id asc');
        
        return $data;
    }
    
    //格式化图片
    private function goodsImageFormat($image)
    {
        if (empty($image)) {
            return array();
        }
        $data = explode(',', $image);
        foreach ($data as $k => $v) {
            $list[$k]['file_name'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS .$v;
            $list[$k]['file'] = $v;
            $list[$k]['type'] ='url';
        }
        
        return $list;
    }
        
    
    
    //保存当前
    public function save_goodsOp()
    {
        $model= model('goods');
        $size_model = model('store_goods_size');
		$specs_model = model('store_goods_specs');
		$options_model = model('store_goods_options');
		$language_model = model('goods_language');
        //获取商品
        $where = array(
            'goods_id' => $_POST['goods_id'],
            'store_id' => $this->store_info['store_id'],
			'import_state' => 1
        );
				
        $goods_info = $model->getGoodsInfo($where);
        if (empty($goods_info)) {
			output_error('参数不正确');			
        } 
		
		if(empty($_POST['goods_sizeprice'])){
			output_error('请填写尺寸和价格');			
		}
		$goods_name = $_POST['goods_name'];
		
		$goods_code = $goods_info['goods_code'];	                
        $data=  array();
        $data['goods_name'] = $goods_name[0]['goods_name'];
        $data['gc_id'] = $_POST['gc_id'];        
        //获取商品分类名称
		//获取商品分类名称
		$gc_name = model('store_goods_class')->getOneById($_POST['gc_id']);			
		$data['gc_name'] = $gc_name['stc_name'];
		
        $data['store_name'] = $this->store_info['store_name_primary'];        
        
        $data['goods_price'] = $_POST['goods_price'];
        $data['goods_code'] = $goods_code;
        $data['goods_addtime'] = time();
        $data['goods_ingr'] = $_POST['goods_ingr'];
        
        $goods_image = $this->imageAll($_POST['goods_image']);
        $data['goods_image'] = $goods_image;        
        $data['goods_stock'] = $_POST['goods_stock'];
        $data['goods_state'] = $_POST['goods_state'];
        $data['store_id'] = $this->store_info['store_id'];
        $data['is_recommend'] = $_POST['is_recommend'];
        $data['goods_code_num'] = 0;
      //  $data['goods_is_specs'] = $_POST['goods_is_specs'] == '是' ? 1: 0;		
		$data['import_state'] = $goods_info['import_state'];
		$data['import_success'] = 1;
		$data['import_error'] = 2;
	//	print_r($data);
		$where = array(
			    'goods_id' => $_POST['goods_id'],
			    'store_id' => $this->store_info['store_id'],
				'import_state' => 1				
		);
        $goods_id = $model->editGoodsNew($data,$where);
		
		
		
        if ($goods_id) {
        
			//删除之前的数据
			$where = array();
			$where['goods_code'] = $goods_code;
			$where['store_id'] = $this->store_info['store_id'];
			$where['import_state'] = 1;						
			
			$size_model->delSize($where);
			//print_r(1);
			$specs_model->delSpecs($where);
			//print_r(2);
			$options_model->delOptions($where);
			$language_model->delGoodsLang($where);	
		
		
            //更新分类表
            model('store_goods_extended')->delGoodsExt(array('goods_code'=>$goods_info['goods_code'],'store_id'=> $this->store_info['store_id']));
            
            $data_goods_ext = array(
                'stc_id' => $_POST['gc_id'],
                'store_id' => $this->store_info['store_id'],
                'goods_code' => $goods_code
            );
			
            model('store_goods_extended')->addGoodsExt($data_goods_ext);
            
			
			
            model('goods_ingr')->delGoodsIngr(array('goods_code'=>$goods_info['goods_code'],'store_id'=> $this->store_info['store_id'],'import_state' => 1));
        
            $ingr_data=  explode(",", $_POST['goods_ingr']);
            foreach ($ingr_data as $v) {
                $data_ingr[] = array(
                    'ingr_id' => $v,
                    'goods_code' => $goods_info['goods_code'],
                    'store_id' => $this->store_info['store_id'],
					'import_state' => 1
                );
            }
            model('goods_ingr')->addGoodsIngrAll($data_ingr);        			
			
			/*3.18保存*/		        
            $lang = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));        
       
			foreach ($lang as $v) {
                $data_sub = array();
                $data_sub['goods_id'] = $_POST['goods_id'];
                $goods_name = $_POST['goods_name'];				
                foreach ($goods_name as $vv) {
                    if ($v['language_en'] == $vv['lang']) {
                        $data_sub['goods_name'] = $vv['goods_name'];
                    }
                }
                $data_sub['store_id'] 	 = $v['store_id'];
             			  
				$goods_description = $_POST['goods_description'];
                foreach ($goods_description as $vv) {
                    if ($v['language_en'] == $vv['lang']) {
                        $data_sub['goods_description'] = $vv['goods_description'];
                    }
                }
              
                $data_sub['lang_id']       = $v['language_id'];
                $data_sub['lang_name']       = $v['language_en'];
				$data_sub['goods_code'] = $goods_code;
				$data_sub['goods_addtime'] = time();
				$data_sub['import_state'] = 1;
                model('goods_language')->addGoodsLang($data_sub);
			
            }
			
			//写入 size			
			$goods_size = $_POST['goods_sizeprice'];
			foreach($goods_size as $k=> $v){				
				foreach($v['lang'] as $l){					
					$data = array();
					$data['size_value'] = $l['name'];
					$data['lang'] = $l['lang'];
					$data['size_price'] = $v['price'];
					$data['size_sale_price'] = $v['sale_price'];
					$data['size_box_id'] = $v['box_id'];
					$data['size_box_name'] = '';
					$data['goods_code'] = $goods_code;			
					$data['size_id'] = $goods_code .'|'.($k+1);
					$data['store_id'] = $this->store_info['store_id'];
					$data['import_state'] = 1;
					$size_model->addSize($data);					
				}
			}
			
						
			//写入 specs
			
			$goods_specs = $_POST['goods_specs'];
			if(!empty($goods_specs)){			
				foreach($goods_specs as $k => $v){				
					$specs_id = $goods_code . '|' . ($k+1);
					foreach($v['name'] as $n){
						$parent = array();
						$parent['specs_value'] = $n['specs_value'];
						$parent['lang'] 		= $n['lang'];
						$parent['goods_code'] = $goods_code;
						$parent['specs_type'] = 0;
						$parent['specs_buy_num'] = $v['specs_buy_num'];
						$parent['specs_id'] = $specs_id;	
						$parent['store_id'] = $this->store_info['store_id'];					
						$parent['import_state'] = 1;
						//写入数据表					
						//print_r($parent);
						$parent_id = $specs_model->addSpecs($parent);						
					}
					
					foreach($v['child'] as  $kk => $c){						
						$specs_id = $goods_code .'|'.($k+1) . '|' . ($kk+1);
						foreach($c['lang'] as $l){
							$parent = array();
							$parent['specs_value'] = $l['specs_value'];
							$parent['lang'] = $l['lang'];
							$parent['goods_code'] = $goods_code;
							$parent['specs_type'] = 1;							
							$parent['specs_parent_id'] = $goods_code .'|'.($k+1);		
							$parent['specs_id'] = $specs_id;
							$parent['store_id'] = $this->store_info['store_id'];		
							$parent['import_state'] = 1;
							//print_r($parent);
							$specs_model->addSpecs($parent);
						}
					}	
				}	
			}
			//写入配件
			
			$options = $_POST['goods_optional'];				
			if(!empty($options)){
				foreach($options as $v){
					$data = array();
					$data['goods_code'] 	= $goods_code;
					$data['goods_size_id'] 	= $v['size']['size_id'];								
					$specs_id = [];				
					foreach($v['specs'] as $specs){
						$specs_id[] 		= $specs['specs_id'];
					}
					$specs_id = implode(',',$specs_id);								
					$data['goods_specs_id'] 		= $specs_id;
					$data['options_goods_code'] 	= $v['goods_code'];
					$data['options_price'] 			= $v['price'];	
					$data['store_id'] = $this->store_info['store_id'];	
					$data['import_state'] = 1;
					model('store_goods_options')->addOptions($data);	
				}
			}
						
		  //	output_error('编辑失败');
            output_data(array('message' => '编辑成功'));
        } else {
            output_error('编辑失败');
        }
    }
     

    
    

    /**
     * 编辑分类
     */
    public function edit_classOp()
    {
        // 实例化商品分类模型
        $model_goodsclass = Model('goods_class');
        // 商品分类
        $goods_class = $model_goodsclass->getGoodsClass($this->store_info['store_id']);

        // 常用商品分类
        $model_staple = Model('goods_class_staple');
        $param_array = array();
        $param_array['member_id'] = $_SESSION['member_id'];
        $staple_array = $model_staple->getStapleList($param_array);
    }
    
    
    
    
    private function jsonStoreClass($type = '')
    {
        $class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
    
        if ($type == 'list') {
            $store_goods_class[] = array(
                'stc_id' => 'all',
                'stc_name' =>'全部'
            );
        }
        
        foreach ($class as $v) {
            $data = $v;
            $data['stc_id'] = $v['stc_id'];
            $data['active'] = 0;
            $data['stc_lang_name'] = unserialize($v['stc_lang_name']);
            $data['goods_count'] = $this->goodsCount($v['stc_id']);
            $store_goods_class[] = $data;
        }
    
        return $store_goods_class;
    }
    
    
    private function jsonStoreClassXls()
    {
        $class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id'],'import_state' => 1));
        $store_goods_class[] = array(
            'stc_id' => 'all',
            'stc_name' =>'全部',
            'goods_count' => $this->goodsCountXls()
        );
        
        foreach ($class as $v) {
            $data = $v;
            $data['stc_id'] = $v['stc_id'];
            $data['active'] = 0;
            $data['stc_lang_name'] = unserialize($v['stc_lang_name']);
            $data['goods_count'] = $this->goodsCountXls($v['stc_id']);
            $store_goods_class[] = $data;
        }
    
        
        return $store_goods_class;
    }
    
    
    
    
    
    //计算分类下有多少个商品
    private function goodsCount($stc_id)
    {
		
		$where= array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['is_old'] = 0;
		if($stc_id > 0 ){
			$where['gc_id'] = $stc_id;
		}
        $ext_count =  model('goods')->getGoodsCount($where);
        return $ext_count;
    }
    
    
    //统计当前分类下是否全部修改完毕
    private function goodsCountXls($stc_id = 0)
    {
        $where =array();
        if ($stc_id > 0) {
            $where['gc_id'] = $stc_id;
        }
        $where['import_error'] = array('in', '0,1');
        $where['store_id'] = $this->store_info['store_id'];

        $count = model('goods')->getGoodsCount($where);
        
        return $count;
    }
    
    
    
    //验证specs是否无数据
    private function checkSpecsCount($lang)
    {
        $count = 0;
        
        foreach ($lang as $v) {
            foreach ($v['goods_specs'] as $vv) {
            }
        }
        return $count;
    }
    
    
    
    
    
    
     
     
 
    
    //成品导入正式数据库表
    
    public function excel_importOp()
    {
        
        //验证是否有未编辑的商品
        $model = model('goods');        
        $where = array(
            'store_id' => $this->store_info['store_id'],
			'import_state' => 1,
            'import_error' => array('in','0,1')
        );
        $count  = $model->getGoodsCount($where);
        if ($count > 0) {
            output_error('您导入商品尚未编辑，请编辑后导入');
        }      
	  
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['import_state'] = 1;
		
		$data = array();
		$data['import_state'] = 0;
		
		//修改主商品
		model('goods')->editGoodsNew($data,$where);
		//修改size
		model('store_goods_size')->editSize($where,$data);
		//修改specs
		model('store_goods_specs')->editSpecs($where,$data);
		//修改option
		model('store_goods_options')->editOptions($where,$data);		
		//修改lang
		model('goods_language')->editGoodsLang($data,$where);
		//修改商家分类		
		model('store_goods_class')->editStoreGoodsClass($data,$where);
		//修改ingr
		model('goods_ingr')->editGoodsIngr($data,$where);
 
        model('store_goods_xls')->delGoodsXls(array('store_id' => $this->store_info['store_id']));
        
        output_data(array('message' => '导入成功'));
    }
    
    

    
    
    
    //获取多语言数据
    private function getLangGoodsXls($goods_id)
    {
        $list = model('goods_lang_xls')->getGoodsXlsLangList(array('goods_id' => $goods_id));
        
        $data = array();
        foreach ($list as $k=> $v) {
            $data[$k] = $v;
            $data[$k]['lang_name'] =  $v['lang_name'];
            $data[$k]['goods_sizeprice'] = unserialize($v['goods_sizeprice']);
        
            if (empty($data[$k]['goods_sizeprice'])) {
                $data[$k]['goods_sizeprice'] = array(array(
                    'name'=>'',
                    'box_id' => '',
                    'price' => '',
                    'sale_price' => ''
                )) ;
            }
            $data[$k]['goods_specs'] = unserialize($v['goods_specs']) ;
            if (empty($data[$k]['goods_specs'])) {
                $data[$k]['goods_specs'] = array(array());
            }
            
            
            $data[$k]['goods_optional'] = unserialize($v['goods_optional']);
            
            
            if (empty($data[$k]['goods_optional'])) {
                $data[$k]['goods_optional'] = array(array(
                'goods_id' => '',
                'goods_price' => ''
            ));
            }
        }
        return $data;
    }
    
    
    //获取多语言数据
    private function getLangGoods($goods_id)
    {
        $list = model('goods_language')->getGoodsLangList(array('goods_id' => $goods_id));
        
        $data = array();
        foreach ($list as $k=> $v) {
            $data[$k] = $v;
            $data[$k]['lang_name'] =  $v['lang_name'];
        }
        return $data;
    }
    
    
    //上传商品图片
    
    public function upload_imageOp()
    {
        $store_id = $this->store_info['store_id'];
        $upload = new UploadFile();
        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
        $result = $upload->upfile('file');
        if (!$result) {
            output_error($upload->error);
        } else {
            $img_path = $upload->getSysSetPath() . $upload->file_name;
            output_data(array('image' => $img_path));
        }
    }
    
    
    //保存裁剪的图片
    public function save_imageOp()
    {
        $upload = new UploadFile();
        $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();
        $file_name = $this->base64_image_content($_POST['image'], $path);

        output_data(array('image' => $file_name));
    }
    
    private function imageAll($imagelist)
    {
        $upload = new UploadFile();
        $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();
    
        $file_name = array();
        foreach ($imagelist as $v) {
            if ($v['type'] == 'url') {
                $file_name[] = $v['file'];
            } else {
                $file_name[] = $this->base64_image_content($v['file_name'], $path);
            }
        }
        
        return implode(",", $file_name);
    }
    
    
    private function base64_image_content($base64_image_content, $path)
    {
        //匹配出图片的格式
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)) {
            $type = $result[2];
            $new_file = $path;
            if (!file_exists($new_file)) {
                //检查是否有该文件夹，如果没有就创建，并给予最高权限
                mkdir($new_file, 0700);
            }
            $file_name = time().rand(10000, 99999).".{$type}";
            $new_file = $new_file.$file_name ;
            if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))) {
                return $file_name;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    
    
    
    
    

    
    
    ///上传菜单
    public function upload_excelOp()
    {
        $upload = new UploadFile();
        $upload->set('default_dir', ATTACH_XLS);
        //上传图片
        
        if (!empty($_FILES['file']['tmp_name'])) {
            $result = $upload->upfile('file');
            if ($result) {
                $data = array(
                        'filename' => $upload->file_name,
                        'store_id' => $this->store_info['store_id'],
                        'addtime'  => time()
                    );
                $row = Model('store_goods_xls')->addGoodsXls($data);
                //写入数据库
                $this->goodsExcelSave($upload->file_name);
                output_data(array('fid' => $row));
            } else {
                output_error($upload->error);
            }
        } else {
            output_error('上传失败');
        }
    }
    
    
    //上传数据列表
    public function goods_excel_classOp()
    {
        $class = $this->jsonStoreClassXls();
        output_data(array('class' => $class));
    }
    
        
    //XLS商品列表数据
    public function get_xls_goods_listOp()
    {
        $where = array();
        $where['store_id'] = $this->store_info['store_id'];
        if ($_POST['class_id'] > 0) {
            $where['gc_id'] = $_POST['class_id'];
        }
        if ($_POST['keyword'] != '') {
            $where['goods_code'] = array('like','%'.$_POST['keyword'].'%');
            //$where['goods_specs'] = array('like','%'.$_POST['keyword'].'%');
                //$where['goods_description'] = array('like','%'.$_POST['keyword'].'%');
                //$where['goods_code'] = array('like','%'.$_POST['keyword'].'%');
        }
        $goods_list  = model('goods')->getGoodsList($where);
        $data = array();
        foreach ($goods_list as $k => $v) {
            $data[$k] = $v;
            $data[$k]['lang'] = $this->getLang($v['goods_id']);
        }
            
        output_data(array('list' => $data));
    }
    
    
    private function getLang($goods_id)
    {
        $where = array(
            'goods_id' => $goods_id
        );
        return model('goods_lang')->getGoodsLangList($where);
    }
    
    
    public function ceshiOp()
    {
        //写入数据库
        $this->goodsExcelSave('06711462921763522.xlsx');
    }
    
   
    //导入数据
    private function goodsExcelSave($filename)
    {
        $data = $this->excelToArray($filename);
	
        $xls_store = $data[0][1];
		
        if ($xls_store != $this->store_info['store_code']) {
            model('store_goods_xls')->delGoodsXls(array('store_id' => $this->store_info['store_id']));
            output_error('*上传菜单里的店铺编号与本店铺编号不符');
        }
        
        $store_lang_data = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));
        if (empty($store_lang_data)) {
            output_error('**店铺未设置菜单语言无法上传');
        }
                
        foreach ($store_lang_data as $v) {
            $store_lang[] = $v['language_en'];
        }
        
        $store_lang = implode(',', $store_lang);
        $xls_lang = str_replace(' ', '', $data[0][3]);
	
	
        if ($xls_lang != $store_lang) {
            model('store_goods_xls')->delGoodsXls(array('store_id' => $this->store_info['store_id']));            
            output_error('**上传菜单的语言与本店铺设置的菜单语言不符');
        }
    
        //print_r($data);
        //写入分类
        $class_list = $this->getClass($data);    
        $classCount =  model('store_goods_class')->getStoreClassCount(array('store_id'=> $this->store_info['store_id']));
        if ($classCount > 0) {
            $class_code = $classCount + 1;
        } else {
            $class_code = 1;
        }
	
        //分类编号    
        //分类去重          
        $xlslang = explode(',', $data[0][3]);		
		
		
        foreach ($class_list as $k => $v) {
        
			$storeClassCount = model('store_goods_class')->getStoreClassCount(array('store_id'=>$this->store_info['store_id'],'stc_name'=>$v[0]['class_name']));

            if ($storeClassCount == 0) {
                $lang_name = array();
                $class_data[] = array(
                    'stc_name' => count($xlslang) > 1 ? $v[1]['class_name'] : $v[0]['class_name'],
                    'store_id' => $this->store_info['store_id'],
                    'stc_code' => $this->store_info['store_code'].'_C'.$class_code,
                    'stc_lang_name' => serialize($v),
					'import_state' => 1
                );
                $class_code ++ ;
            }
        } 
					  
		$row = model('store_goods_class')->addStoreClassAll($class_data);                                            
		
		
		if(count($xlslang) == 1){				
			$goods_list = $this->getGoodsOne($data);				
		}else{
			//2种语言
			$goods_list = $this->getGoods($data);                				
		}
			
		//print_r($goods_list);
		//exit;
				
        $goodsCount = model('goods')->getGoodsCount(array('store_id'=> $this->store_info['store_id']));
        if ($goodsCount > 0) {			
            $code = $goodsCount + 1;
        } else {			
            $code = 1;
        }
        
        //编号          
        foreach ($goods_list as $k => $v) {			
          
			if ($k < 2) {
                continue;
            }
			
			//主商品            
            $data = array(
                'goods_name' => $v['goods_name'],
                'goods_price' => $v['disp_price']  > 0 ? $v['disp_price'] : 0 ,
                'goods_code' =>  $this->store_info['store_code'].'_i'.$code,  //$v['ItemID'],                
                'store_id' => $this->store_info['store_id'],
                'store_name' => $this->store_info['store_name_primary'],               
                'goods_stock' => $v['goodsStock'],
                'goods_state' => 1,
				'import_state' => 1,
				'goods_addtime' => time(),
				'is_import' => 1,
 				'import_error' => $v['error']
            );       
				 
            $data['gc_id'] = $this->getClassId($v['class_name']);              
			$data['gc_name'] = $v['class_name'];          
		//	print_r($data);
         //   $data['import_error'] =  $this->isnumeric($checkData);              
			
            $goods_id =model('goods')->addGoods($data);
			
            if ($goods_id) {				
				//语言包、、		
                $lang_data = array();
                foreach ($v['lang'] as $lang_item) {
						$lang_data[] = array(
                      //  'goods_specs' => serialize($lang_item['Specs']),
                        'goods_description' => $lang_item['description'],
                    //    'goods_sizeprice' => serialize($lang_item['SizePrice']),
                        'goods_name' => $lang_item['goods_name'],
                    //	'goods_price' => $v['DispPrice'],
                        'goods_code' => $data['goods_code'],
                    //	'goods_optional' => serialize($lang_item['Optional']),
                        'store_id' => $this->store_info['store_id'],
                        'goods_id' => $goods_id,
                       // 'lang_id' => $lang_item['LangId'],
                        'lang_name' => $lang_item['lang'],
						'goods_addtime' => time(),
						'import_state' => 1
                    );
                }
				
			///	print_r($lang_data);
				
				//写入langData
				
				model('goods_language')->addGoodsLangAll($lang_data);
				
				
				//size				
				foreach($v['size'] as $s => $sizes){
					$size_list = array();
				
					foreach($sizes as $k => $c){					
						$size_id = $data['goods_code'] .'|'.($k+1);
						$size = array();
						$size['size_value'] = $c['name'];
						$size['lang'] = $c['lang'];
						$size['size_price'] = $c['price'];
						$size['goods_code'] = $data['goods_code'];
						$size['size_id'] = $size_id;
						$size['import_state'] = 1;
						$size['store_id'] = $this->store_info['store_id'];						
						
						
						model('store_goods_size')->addSize($size);
						
										
					}
					
				}
				
				
				foreach($v['specs'] as $s => $specs){
				
					$specs_id = '';
					foreach($specs as $kk => $sp){
						$specs_id = $data['goods_code'].'|'.($kk+1);
						$specs_parent = array();
						$specs_parent['specs_value'] = $sp['name'];
						$specs_parent['lang'] = $sp['lang'];
						$specs_parent['goods_code'] = $data['goods_code'];
						$specs_parent['specs_type'] = 0;
						$specs_parent['specs_parent_id']  = 0;
						$specs_parent['specs_buy_num'] = 1;
						$specs_parent['specs_id'] = $specs_id;
						$specs_parent['import_state'] = 1;
						$specs_parent['store_id'] = $this->store_info['store_id'];
						model('store_goods_specs')->addSpecs($specs_parent);
						//print_r($specs_parent);
					}
					
					$specs_id = '';
					foreach($specs as $kk => $child){
					
						foreach($child['child'] as $c => $ccc){
							$specs_id =  $data['goods_code'].'|'.($kk+1).'|'.($c+1);
							$child = array();
							$child['specs_value'] = $ccc['value'];
							$child['lang'] = $ccc['lang'];
							$child['goods_code'] = $data['goods_code'];
							$child['specs_type'] = 1;							
							$child['specs_buy_num'] = 0;
							$child['specs_id'] = $specs_id;
							$child['specs_parent_id'] =  $data['goods_code'].'|'.($kk+1);
							$child['store_id'] = $this->store_info['store_id'];
							$child['import_state'] = 1;
							model('store_goods_specs')->addSpecs($child);
						}											
						
					}
					
				}
				
            }         
		   $code ++;
        }
		
	
		
		
		
		
    }
        
    

	
	
	//两个语言的XLS
    private function excelToArray($filename)
    {
        require_once BASE_DATA_PATH. '/resource/phpexcel/PHPExcel/IOFactory.php';
        //加载excel文件
        $filename = BASE_UPLOAD_PATH.'/shop/xls/'.$filename;
        $objPHPExcelReader = PHPExcel_IOFactory::load($filename);
        $reader = $objPHPExcelReader->getWorksheetIterator();
        
    
        //循环读取sheet
        foreach ($reader as $sheet) {
            //读取表内容
            $content = $sheet->getRowIterator();
            //逐行处理
            $res_arr = array();
            foreach ($content as $key => $items) {
                $rows = $items->getRowIndex();              //行
                 $columns = $items->getCellIterator();       //列
                 $row_arr = array();
                //确定从哪一行开始读取
                if ($rows < 1) {
                    continue;
                }
                //逐列读取
                foreach ($columns as $head => $cell) {
                    //获取cell中数据
                    $data = $cell->getValue();
                    $row_arr[] = $data;
                }
                $res_arr[] = $row_arr;
            }
        }
          
        return $res_arr;
    }
    
    
    //提取分类
    private function getClass($data)
    {
        $lang = explode(',', str_replace(' ', '', $data[0][3]));
        foreach ($data as $k=> $v) {
            if ($v[0] == 'CLASS INFORMATION') {
                $class = array();
            }
            if ($k > 1) {
                foreach ($lang as  $kk => $l) {
                    $lang_data[$kk]['lang_name'] = $l;
                    $lang_data[$kk]['class_name'] = $v[$kk];
                }
                $class[] = $lang_data;
            }
            if ($v[0] == 'ITEM INFORMATION') {
                break;
            }
        }
        $count = count($class);
        unset($class[0]);
        unset($class[$count-1]);
        return  $class;
    }
    
	private function getClassOne($data){
		
		$lang = explode(',', str_replace(' ', '', $data[0][3]));
		foreach ($data as $k=> $v) {
		    if ($v[0] == 'CLASS INFORMATION') {
		        $class = array();
		    }
		    if ($k > 1) {
		        foreach ($lang as  $kk => $l) {
		            $lang_data[$kk]['lang_name'] = $l;
		            $lang_data[$kk]['class_name'] = $v[$kk];
		        }
		        $class[] = $lang_data;
		    }
		    if ($v[0] == 'ITEM INFORMATION') {
		        break;
		    }
		}
		$count = count($class);
		unset($class[0]);
		unset($class[$count-1]);
		return  $class;
		
	}
	
	
    
    //获取class_id
    private function getClassId($class_name)
    {
        if ($class_name == '') {
			$where = array(
			    'stc_hidden'=> 1,
			    'store_id' => $this->store_info['store_id']
			);
			$item = model('store_goods_class')->getStoreGoodsClassInfo($where);
			
            return $item['stc_id'] ;
        }
        $where = array(
            'stc_name'=> $class_name,
            'store_id' => $this->store_info['store_id']
        );
        $item = model('store_goods_class')->getStoreGoodsClassInfo($where);
        if (empty($item)) {
            return 0;
        }
        return $item['stc_id'];
    }
    
	//一个语言的XLS
	private function getGoodsOne($data){
		
		//print_r($data);
		$class = $this->getClassOne($data);
		$lang = explode(',', str_replace(' ', '', $data[0][3]));
		
		$i=0;
		foreach ($data as $k=> $v) {
		    if ($v[0] == 'ITEM INFORMATION') {				
				$i = 1;
		    }
			if($i == 1){
		    $goods['id'] = $k;
			$goods['goods_name'] 	= $v[0];
		    $goods['class_name']	= $v[1];
		    $goods['disp_price']	= $this->shopPirce($v[2], $this->size(@trim($v[4]),$v[2],$lang[0]));
		    $goods['description']	= $v[3];
		    $goods['goodsStock']	= 66;
			$goods['error']  = 0;					
			$size1 = $this->size(@trim($v[4]),$v[2],$lang[0]);		
			$goods['size'][0] = $size1;				
			$specs1 = $this->specs($v[5],$lang[0]);
			$goods['specs'][0]= $specs1;				
			$goods['lang'][0] = array(
		                        'lang' => $lang[0],                                
		                        'goods_name' => $v[0],
		                        'description'=> $v[3],                          
		                    );		
		    $list[]= $goods;                                                                                                                              
		   }
		}    
		
		return  $list;
		
		
		
	}
	
	
	//两个语言的XLS
    private function getGoods($data)
    {
        $class = $this->getClass($data);
        $lang = explode(',', str_replace(' ', '', $data[0][3]));
     //   print_r($data);
		
		$i=0;
		
        foreach ($data as $k=> $v) {
            if ($v[0] == 'ITEM INFORMATION') {				
				$i = 1;
            }
			if($i == 1){
				
				$goods['id'] = $k;
				$goods['goods_name'] 	= $v[0];
				$goods['class_name']	= $v[2];
			
				$goods['description']	= $v[5];
				$goods['goodsStock']	= 66;
				$goods['error']  = 0;
				
				
				$size1_value = !empty($v[7]) ? $v[7] : $v[6];
				
				//print_r($size1);
				$size2_value = !empty($v[6]) ? $v[6] : $v[7];
				
				$size2 = $this->size(@trim($size2_value),$v[3],$lang[0]);			
				$size1 = $this->size(@trim($size1_value),$v[3],$lang[1]);	//一般是英文
				
				$goods['disp_price']	= $this->shopPirce($v[3],$size1);
				
				if(!empty($size1_value) || !empty($size2_value)){
					
						if(count($size1) > count($size2)){ //第一个比第二个大，补齐第二个				
							//补几个
							$num = count($size1) - count($size2);
													
							for($i = 1; $i<=$num; $i++){
								$size2[$num] = array(
									'lang' => $size2[0]['lang'],
									'name' => '',
									'price' => 0
								);
							}			
							$goods['error'] = 1; //凡是有错误的地方就标记一下；			
							
						}elseif(count($size1) < count($size2)){ //第二个比第一个大			
									
							$num = count($size2) - count($size1);
							for($i =1; $i<=$num; $i++){
								$size1[$num] = array(
									'lang' => $size1[0]['lang'],
									'name' => '',
									'price' => 0
								);
							}		
						
							$goods['error'] = 1; //凡是有错误的地方就标记一下；
							
						}					
				}
								
				$goods['size'][0] = $size1;			
				$goods['size'][1] = $size2;			
				
			
				//规格导入开始
				$specs1_value = !empty($v[9]) ? $v[9] : $v[8];
				$specs2_value = !empty($v[8]) ? $v[8] : $v[9];
				$specs1 = $this->specs($specs1_value,$lang[1]);
				$specs2 = $this->specs($specs2_value,$lang[0]);
				
				
				
				if(!empty($specs1) && empty($specs2)){					
					foreach($specs1 as $sp1){			
						$sp1['lang'] = $lang[0];
						foreach($sp1['child'] as $sk => $child){							
							$sp1['child'][$sk]['lang'] = $lang[0];							
							$sp1['child'][$sk]['value'] = $child['value'];							
							$sp1['child'][$sk]['price'] = $child['price'];							
						}						
						$specs2[] = $sp1;						
					}					
				}
				
				if(empty($specs1) && !empty($specs2)){
					foreach($specs2 as $sp2){	
						$sp2['lang'] = $lang[1];
						foreach($sp2['child'] as $sk=> $child){							
							$sp2['child'][$sk]['lang'] = $lang[1];							
							$sp2['child'][$sk]['value'] = $child['value'];							
							$sp2['child'][$sk]['price'] = $child['price'];							
						}						
						$specs1[] = $sp2;						
					}					
				}
				
				
				if(empty($specs1_value) || empty($specs2_value)){
					
					
					
					//$specs1 = array();
					//$specs2 = array();
				
				
					/*foreach($specs1 as $s1 => $sp1){						
						foreach($specs2 as $s2 => $sp2){						
							if($sp1['num'] > $sp2['num']){						
								$spnum = $sp1['num'] - $sp2['num'];												
								for($i =1; $i<=$spnum; $i++){
									$specs2[$spnum] = array(
										'lang' => $specs2[0]['lang'],
										'name' => '',
										'child' => 	array()
									);							
								}														
							}elseif($sp1['num'] < $sp2['num']){							
								$spnum = $sp2['num'] - $sp1['num'];						
								for($i =1; $i<=$spnum; $i++){
									$specs1[$spnum] = array(
										'lang' => $specs1[0]['lang'],
										'name' => $spnum,
										'child' => array()
									);							
								}						
							}						
							if($sp1['child_num'] > $sp2['child_num']){
								$goods['error'] = 1;
								$spnum = $sp1['child_num'] - $sp2['child_num'];
								for($i =1; $i<=$spnum; $i++){
									$specs2[$s2]['child'][$spnum] = array(
										'lang' => $specs2[0]['lang'],
										'value' => '',
										'price' => 0
									);
								}													
							}elseif($sp1['child_num'] < $sp2['child_num']){						
								$goods['error'] = 1;
								$spnum = $sp2['child_num'] - $sp1['child_num'];
								for($i =1; $i<=$spnum; $i++){
									$specs1[$s1]['child'][$spnum] = array(
										'lang' => $specs1[0]['lang'],
										'value' => '',
										'price' => 0
									);
								}								
							}						
						}					
					}
					*/
					
					
				}
										
				$goods['specs'][0]= $specs1;
				$goods['specs'][1]= $specs2;	
			
				$goods['lang'][0] = array(
									'lang' => $lang[1],                                
									'goods_name' => $v[1],
									'description'=> $v[5],                          
								);
								
				$goods['lang'][1] = array(
									'lang' =>  $lang[0],                              
									'goods_name' => $v[0],
									'description'=> $v[4],                              
								);
								
				$list[]= $goods;                                                                                                                              
           }
		   
        }    

        return  $list;
    }
    
	
    
    //通过price 获取价格
    private function shopPirce($select_price, $data)
    {
				
		if(is_numeric($select_price)){			
			return $select_price;		
		}		
        $price = '';		
        foreach ($data as $v) {
            if ($v['name'] == $select_price) {
                $price = $v['price'];
            }
        }
        return $price;
    }
    
    
    
    
    
    private function size($size ='' ,$price = 0,$lang)
    {
        if (empty($size)) {
			if($price > 0 ){
				$price = $price;
			}else{
				$price = 0;
			}
			$data[] = array(
				'lang' => $lang,
				'name' => 'DefaultSize',
				'price' => $price,
			);
            return $data;
        }
        
        $data = explode(',', $size);
        foreach ($data as $v) {
            $arr_data = explode(':', $v);
            $arr[] = array(
				'lang' => $lang,
                'name' => @trim($arr_data[0]),
                'price' => @trim($arr_data[1])
            );
        }
        return $arr;
    }
    
    private function specs($specs,$lang)
    {
        if (empty($specs)) {
            return array();
        }
        $data = $data_num = explode(',', $specs);
		$arr = array();
		//print_r($data);
        foreach ($data as $v) {			
            $arrs_data = explode(':', $v);	
			
			if(count($arrs_data) > 1){				
				$arr_data = $arrs_data;				
			}else{						
				return array();
				break;
//				$new_data = explode(':',$v.':');				
	//			$arr_data = $new_data;
			}
				
			//print_r($arr_data);
			
			$arr[$arr_data[1]]['lang'] = $lang;
			$arr[$arr_data[1]]['name'] = @trim($arr_data[1]);
            $arr[$arr_data[1]]['child'][] = array(
				'lang' => $lang,
                'value' => @trim($arr_data[0]),
                'price' => $arr_data[2] ? @trim($arr_data[2]) : 0
            );			
        }
		$specs = array();
		foreach($arr as  $v){			
			$v['num'] = count($arr);
			$v['child_num']  = count($v['child']);			
			$specs[] =  $v;
			
			
		}
		
		
        return $specs;
    }
    
    
    private function optional($optional)
    {
        if (empty($optional)) {
            return array();
        }
        $data = explode(',', $optional);
        foreach ($data as $v) {
            $arr_data = explode(':', $v);
            $arr[] = array(
                'name' => @trim($arr_data[0]),
                'price' => $arr_data[1] ? @trim($arr_data[1]) : 0
                //'price' => $arr_data[2] ? @trim($arr_data[2]) : 0
            );
        }
        return $arr;
    }
    
    
    
    
    //校验是否为数字或者金额
    private function isnumeric($data)
    {
        $num = 0;
        //检验分类是否为空
        
        if ($data['gc_id'] == 0) {
            $num +=  1;
        }
        //检查价格是否为空
        if ($data['goods_price'] == '' || $data['goods_price'] <= 0) {
            $num += 1;
        }
        
        foreach ($data['lang'] as $k => $v) {
            if ($v['Name'] == '') {
                $num += 1;
            }
                    
            if ($v['Description'] == '') {
                $num += 1;
            }
            
            
            foreach ($v['SizePrice'] as $d) {
                if (!is_numeric($d['price'])) {
                    $num += 1;
                }
                $lang_size[$k][] = $d;
            }
            
            foreach ($v['Specs'] as $s) {
                if ($s['name'] == '') {
                    $num +=1;
                }
                if ($s['value'] == '') {
                    $num +=1;
                }
                
                $lang_specs[$k][] = $s;
            }
        }
        
        
        if (count($lang_size[0]) != count($lang_size[1])) {
            $num += 1;
        }
        
        if (count($lang_specs[0]) != count($lang_specs[1])) {
            $num += 1;
        }
        
        
        if ($num > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    
    

    /**
     * 删除商品
     */
    public function drop_goodsOp()
    {
        $where = array(
            'store_id' => $this->store_info['store_id'],
            'goods_code' => array('in',$_POST['goodsIds'])
        );
        $data = array(
            'is_delete' => 1
        );
        $row = model('goods')->editGoodsNew($data, $where);
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    
    //清空本次上传的商品
    public function excel_emptyOp()
    {
        $where = array(
            'store_id' => $this->store_info['store_id'],
			'import_state' => array('in','1,2')
        );

        $row = model('goods')->delGoods($where);
        model('goods_language')->delGoodsLang($where);
		model('store_goods_specs')->delSpecs($where);
		model('store_goods_size')->delSize($where);		
        model('store_goods_class')->delStoreGoodsClass($where);
        
		model('store_goods_xls')->delGoodsXls(array('store_id' => $this->store_info['store_id']));
		
		
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    //商品扩展分类
    public function extended_classOp()
    {
        $ext_model = model('store_goods_extended');

        $goodsIds = explode(',', $_POST['goodsIds']);
        $class_id = $_POST['classId'];
        foreach ($goodsIds as $v) {
            $count = $ext_model->getGoodsExtCount(array('goods_code'=> $v,'stc_id'=>$class_id,'store_id'=> $this->store_info['store_id']));
            if ($count == 0) {
                $data[] = array(
                    'goods_code' => $v,
                    'stc_id' => $class_id,
                    'store_id' => $this->store_info['store_id']
                );
            }
        }
        
        $row = $ext_model ->addGoodsExtAll($data);
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    
    public function down_xlsOp()
    {
        require_once BASE_DATA_PATH. '/resource/phpexcel/PHPExcel/IOFactory.php';
        
	
		$lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));
		if(count($lang) == 1){			
			//加载excel文件
			$filename = BASE_UPLOAD_PATH.'/goods_temp/temp1.xlsx';			
		}else{			
			//加载excel文件
			$filename = BASE_UPLOAD_PATH.'/goods_temp/temp2.xlsx';			
		}	
		
		
		
		
        $objPHPExcel  = PHPExcel_IOFactory::load($filename);
    
        $sheet = $objPHPExcel->getSheet(0); // 读取第一個工作表
        $highestColumm = $sheet->getHighestColumn(); // 取得总列数
        $highestRow = $sheet->getHighestRow(); // 取得总行数
        
        $exlCASArr = array();
        
        
        /*
        for ($row =2; $row <= $highestRow; $row++){//行数是以第1行开始
            $ex = $sheet->getCell('E'.$row)->getValue();


            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$row, $casInfo['a']);//修改数据
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$row, $casInfo['b']);
        }
        */
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', $this->store_info['store_code']);//修改数据
        
        //获取商家的语言设置。
     
        $lang_data = array();
        foreach ($lang as $v) {
            $lang_data[] = $v['language_en'];
        }
        $langData = implode(',', $lang_data);
        
        
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', $langData);//修改数据
        
		
		if(count($lang) == 1){	
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Class: '.$lang[0]['language_en']);//修改数据			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'Item: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D6', 'Description: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E6', 'Size & Price: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F6', 'Specs: '.$lang[0]['language_en']);//修改数据
		
		}else{
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Class: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B3', 'Class: '.$lang[1]['language_en']);//修改数据
			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A6', 'Item: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B6', 'Item: '.$lang[1]['language_en']);//修改数据
			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E6', 'Description: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F6', 'Description: '.$lang[1]['language_en']);//修改数据
			
			
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G6', 'Size & Price: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H6', 'Size & Price: '.$lang[1]['language_en']);//修改数据
						
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I6', 'Specs: '.$lang[0]['language_en']);//修改数据
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J6', 'Specs: '.$lang[1]['language_en']);//修改数据
						
						
		}
		
		
		
		
		
        
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
    
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: p_w_upload;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.date('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        
        $url =  BASE_UPLOAD_PATH .'/store_xlsx/'.$this->store_info['store_code'].'.xlsx';
        $http_url = UPLOAD_SITE_URL.'/store_xlsx/'.$this->store_info['store_code'].'.xlsx';
        $objWriter->save($url);
            
        output_data(array('url' => $http_url));
    }
}
