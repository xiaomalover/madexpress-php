<?php

/*
stripe
*/

defined('InMadExpress') or exit('Access Invalid!');
class store_stripeControl extends mobileSellerControl {
   
  
    public function __construct(){
        parent::__construct();	
		$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$this->stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');		
	
    }

	//获取账号的列表
	public function get_accountsOp(){				
		$list_data = $this->stripe->accounts->all(array('limit' => 10));	
		
				
	//	print_r($list_data->data[0]->business_profile);
		//exit;	
		$data = json_decode(json_encode($list_data),true);
	
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}						
		output_data($list);		
	
	}	

	
	
	private function  object2array_pre($object) {
        if (is_object($object)) {
            $arr = (array)($object);
        } else {
            $arr = $object;
        }
        if (is_array($arr)) {
            foreach($arr as $varName => $varValue){
                $arr[$varName] = $this->object2array_pre($varValue);
            }
        }
        return $arr;
    }
	

	

	
	//获取一个账号的详情信息
	public function get_accounts_infoOp(){
		$id= 'acct_1IV4j7PeaJCheQQ5';// $_POST['stripe_id']; 	//主体账号id。类似：acct_1IT0XY2clnzANjLz		
		$result = $this->stripe->accounts->retrieve($id);
		
		
		
		$data = json_decode(json_encode($result->company),true);
		
		print_r($data);
		exit;
		
		
		output_data($result);			
	}	
	
	//不和stripe交互
	public function save_stripeOp(){		
				
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
		    array("input"=>$_POST["name"],"require"=>"true","message"=>'名称不能为空'),
		    array("input"=>$_POST["phone"],"require"=>"true","message"=>'电话不能为空'),
		    array("input"=>$_POST["address"],"require"=>"true","message"=>'地址不能空'),
		    array("input"=>$_POST["tax_id"],"require"=>"true","message"=>'ACN/ABN'),	
		    array("input"=>$_POST["vat_id"],"require"=>"true","message"=>'增值税号'),	
		);
		$error = $obj_validate->validate();
		if ($error != ''){
		    output_error($error);
		}		
				
		
		$model = model('store_stripe');
		$data = array();		
		$data['store_id'] 	= $this->store_info['store_id'];		
		$data['name'] 		= $_POST['name'];	
		$data['phone'] 		= $_POST['phone'];	
		$data['address'] 	= serialize($_POST['address']);	
		$data['email'] 		= $_POST['email'];	
		$data['tax_id'] 	= $_POST['tax_id'];	
		$data['vat_id'] 	= $_POST['vat_id'];	
		$data['type'] 		= 'custom';	
		$data['country'] 	= 'AU';
		
		$data['url'] 	= $_POST['url'];	
		$data['mcc'] 	= $_POST['mcc'];	
		
		
		if(intval($_POST['id']) > 0){			
			$where = array();
			$where['id'] = $_POST['id'];			
			$row = $model->editStoreStripe($data,$where);								
		}else{			
			$row =$model->addStoreStripe($data);	
			$result = $this->_create_accounts();		
		
		
			output_data($result);			
		}		
		
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	public function base_infoOp(){		
		$model = model('store_stripe');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];		
		$info = $model->getStoreStripeInfo($where);		
		if(!empty($info)){			
			$info['address'] = unserialize($info['address']);			
			output_data(array('base_info' => $info));					
		}else{
			output_data(array('base_info' => 'no'));
		}		
	}
	
	
	
	
	//创建stripe主体账号
	private function _create_accounts(){
		
		$model = model('store_stripe');
		
		$base_info = $model->getStoreStripeInfo(array('store_id' => $this->store_info['store_id']));
		$address_info = unserialize($base_info['address']);
		
		$data = array();
		$data['type'] = 'custom'; //账户类型，不能编辑
		$data['country'] = 'AU';  //国家，不能编辑
		$data['email'] = $base_info['email']; //邮箱

		//需要开通的账号功能，可以选择包含以下方式。这两个默认添加即可
		$data['capabilities'] = array(
			'card_payments' => array('requested'=> true), 
			'transfers' =>  array('requested'=> true)
		);
		$data['business_type'] = 'company'; //公司类型

		//公司的信息
		$data['company'] = array(
			'address' => array(
				'city' 		=> $address_info['city'] ,//'Melbourne', //城市
				'country' 	=>  $address_info['country'] ,//'AU',    //国家
				'line1'		=>  $address_info['line1'] ,// '6 May Road',	//具体地址			
				'postal_code' =>   $address_info['postal_code'] ,//'3000',   //邮编
				'state' 	=>   $address_info['state'] ,//'VIC'    //州/省
			),			
			'name' 		=> $base_info['name'],	//公司商户信息，姓名	 					
			'phone' 	=> $base_info['phone'],//公司商户信息，电话	 				
			'tax_id' 	=> $base_info['tax_id'],//公司商户信息，税务登记号码 			
			'vat_id' 	=> $base_info['vat_id'] //公司商户信息，增值税号
		);	
		
		$data['business_profile'] = array(
			'mcc' => '5812',   //公司所属行业，这里看是否要对接，还是固定一个分类id
			'url' => 'www.mad-express.com' //公司网站
		);
		
		$result = $this->stripe->accounts->create($data);		
		
		if($result){				
			$update = array(
				'stripe_id' => $result->id
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$row = $model->editStoreStripe($update,$where);			
			model('store')->editStore($update,$where); //更新主表			
			return $row;
		}else{			
			return 0;
		}		
	}
	
		
	/*
	/编辑账号时，一些字段是不能编辑的。如type，country，同上create
	private function _update_accounts(){

		$id = $_POST['id'];	
		$data = array();
		$data['email'] = '470052365@qq.com';
		$data['capabilities'] = array(
			'card_payments' => array('requested'=> true),
			'transfers' =>  array('requested'=> true)
		);
		$data['business_type'] = 'company';
		$data['company'] = array(
			'address' => array(
				'city' => 'Melbourne',
				'country' => 'AU',
				'line1'	=> '6 May Road',				
				'postal_code' => '3000',
				'state' => 'VIC'
			),			
			'name' => '商户',							
			'phone' => '+61459666011',	
			'tax_id' => '000111222',
			'vat_id' => '123456'
		);			
		$data['business_profile'] = array(
			'mcc' => '5734',
			'url' => 'www.mad-express.com'
		);

		$result = $this->stripe->accounts->update($id,$data);
		output_data($result);	
	}
	

	//删除账号
	public function del_accountsOp(){		
		$id = $_POST['id'];	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$result  = $this->stripe->accounts->delete($id);
		output_data(array('list'=>$result ));		
	}
	*/
	
	
	//获取银行详情
	public function bank_infoOp(){
		$model = model('store_stripe_bank');		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];	
		$bank_info = $model->getStoreStripeBankInfo($where);		
		if(!empty($bank_info)){			
			output_data(array('bank_info' => $bank_info));			
		}else{
			output_data(array('bank_info' => 'nodata'));
		}
	}
	

	/*
		银行账号接口相关
	*/
	public function save_bankOp(){
		
		$model = model('store_stripe_bank');		
		$data = array();		
		$data['store_id'] = $this->store_info['store_id'];
		$data['country'] = $_POST['country'];
		$data['currency'] = $_POST['currency'];
		$data['routing_number'] = $_POST['routing_number'];
		$data['account_number'] = $_POST['account_number'];
		
		if(intval($_POST['id']) > 0){
			$where = array();
			$where['id'] = $_POST['id'];
			$row = $model->editStoreStripeBank($data,$where);
			
			if($row){
				output_data('保存成功');				
			}else{
				output_error('保存失败');
			}
			
		}else{
			
			$row = $model->addStoreStripeBank($data);
			if($row){				
				
				$this->_create_bank_account($row); //更新到stripe上
				
				output_data('保存成功');				
			}else{
				output_error('保存失败');
			}
			
		}
		
		
		
	}
   
   
    /*
	//获得银行账号
	public function get_bank_accountOp(){
		$id= $_POST['id']; 	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$data = array();
		$data['object'] = "bank_account"; 
		$data['limit'] = 10;
		$result = $this->stripe->accounts->allExternalAccounts($id,$data);
		$data = json_decode(json_encode($result),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}						
		output_data(array('list'=>$list ));			
	}


	//获得银行账号
	public function get_bank_account_infoOp(){
		$id= $_POST['id'];  	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$bank_id = $_POST['bank_id'];  //银行账户与借记卡id。类似：ba_1ITjSx2eZqi9GPKxMTL8LkYh
		$result = $this->stripe->accounts->retrieveExternalAccount($id,$bank_id);
		output_data($result);			
	}

	*/

	//添加银行账号	
	private function _create_bank_account($bank_id){			
		$model = model('store_stripe_bank');			
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['id'] = $bank_id;
		$bank_info = $model->getStoreStripeBankInfo($where);	
		$stripe_info = model('store_stripe')->getStoreStripeInfo(['store_id' => $this->store_info['store_id']]);		
		$stripe_id = $stripe_info['stripe_id'];		//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$data = array();
		$data['external_account'] = array(
			'object' => 'bank_account', 
			'country' => $bank_info['country'],   //国家  'AU',
			'currency' => $bank_info['currency'] ,   //货币   'AUD',
			'routing_number' => $bank_info['routing_number'],   //路径号码 测试版必须是测试的，如：110-000
			'account_number' => $bank_info['account_number']    //账号，测试版必须是测试的，如：111111-116
		);				
				
		$result =   $this->stripe->accounts->createExternalAccount($stripe_id,$data);				
		if($result){			
			$where = array(
				'id' => $bank_id
			);
			$data = array(
				'bank_id' => $result->id,
				'stripe_id' => $stripe_id
			);			
			$row = $model->editStoreStripeBank($data,$where);
			return $row;			
		}else{			
			return 0 ;
		}
	}

	/*
	//编辑银行账号信息
	public function update_bank_accountOp(){
		$id = $_POST['id'];	//主体账号id。类似：acct_1IT0XY2clnzANjLz
		$bank_id = $_POST['bank_id'];	 //银行账户与借记卡id。类似：ba_1ITjSx2eZqi9GPKxMTL8LkYh

		$data = array();

		//这里同stripe后台一致，只能编辑默认卡，不能编辑详细内容，true或false
		$data['default_for_currency'] = true;   

		$result = $this->stripe->accounts->updateExternalAccount($id,$bank_id,$data);
		output_data($result);				
	}
	
	
	*/
	
	//删除银行账号信息
	public function del_bankOp(){			
	
		$model = model('store_stripe_bank');		
		
		$bank_info = $model->getStoreStripeBankInfo(['store_id' => $this->store_info['store_id'],'id' => $_POST['bank_id']]);
		
		if(!empty($bank_info)){
			
			$model->delStoreStripeBank(['store_id' => $this->store_info['store_id'],'id' => $_POST['bank_id']]);			
			$this->stripe->accounts->deleteExternalAccount($bank_info['stripe_id'],$bank_info['bank_id']);	
			
			output_data('操作成功');
		}else{			
			output_error('删除失败');
		}	
	}
	
   
   
   //获取信息
   public function get_personsOp(){
	   $model = model('store_stripe_persons');
	   $where = array();
	   $where['store_id'] = $this->store_info['store_id'];
	   $persons_info = $model->getStoreStripePersonsInfo($where);
	   if(!empty($persons_info)){		   
		   output_data(array('persons_info' => $persons_info));		   
	   }else{
		   output_data(array('persons_info' => 'nodata'));
	   }	   
   }
	
	

	public function save_personsOp(){		
				
		$model = model('store_stripe_persons');
		$data = array();		
		$data['store_id'] = $this->store_info['store_id'];
		$data['first_name'] = $_POST['first_name'];
		$data['last_name'] = $_POST['last_name'];
		$data['relationship'] = $_POST['relationship'];
		$data['title'] = $_POST['title'];
		$data['email'] = $_POST['email'];
		$data['phone'] = $_POST['phone'];
		$data['birday'] = $_POST['birday'];
		$data['address'] = serialize($_POST['address']);
		$data['document'] = serialize($_POST['document']);
		if(intval($_POST['id']) > 0){
			$where = array();
			$where['id'] = $_POST['id'];
			$row = $model->editStoreStripePersons($data,$where);
			
			if($row){
				output_data('保存成功');				
			}else{
				output_error('保存失败');
			}
			
		}else{
			
			$row = $model->addStoreStripePersons($data);
			if($row){				
				
				$this->_create_person(); //更新到stripe上
				
				output_data('保存成功');				
			}else{
				output_error('保存失败');
			}			
		}		
	}

	

	/*
		负责人管理接口相关
	

	// 本账号下所有的负责人列表
	public function get_personsOp(){
		$id = $_POST['id'];   //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$list_data = $this->stripe->accounts->allPersons($id,array('limit' => 10));
		$data = json_decode(json_encode($list_data),TRUE);
		$list = array();
		foreach($data['data'] as $k=>$v){				
			$list[$k] = $v;
		}					
		output_data($list);	
	} 



	//本账号某一个负责人详情
	public function get_persons_infoOp(){
		$id = $_POST['id'];    //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];  //所有者的id 如：person_J5W2ya4TLzOeX6
		$result = $this->stripe->accounts->retrievePerson($id,$persons_id);
		output_data($result);		
	}

*/


	//上传身份证信息
	public function upload_identity_documentOp(){
	
		
		$upload = new UploadFile();
		$upload->set('default_dir', ATTACH_SFZ);
		//上传图片
		
		if (!empty($_FILES['file']['tmp_name'])) {
		    $result = $upload->upfile('file');
		    if ($result) {
		      
			   $file =   BASE_UPLOAD_PATH.'/'.ATTACH_SFZ.'/'.$upload->file_name;
			   
			  // print_r($file);
			 //  exit;
			 //  $data = array();
			 //  $data['purpose'] ='identity_document';  //正反身份证标记
			 //  $data['file'] =  fopen( $file , 'r');
			    
			 //  $result = $this->stripe->files->create($data);
			// 
					   
		        output_data(array('file' => $file));
		    } else {
		        output_error($upload->error);
		    }
			
			
		} else {
		    output_error('上传失败');
		}
		
	

	/*	//1.jpg为本服器的实际路径，这个接口必须在外网测试，本地有异常
		$fp = fopen('1.jpg', 'r');
		
		
		output_data($result);	*/
	}
	
	
	//添加负责人信息
	private function _create_person(){
		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$persons_info = model('store_stripe_persons')->getStoreStripePersonsInfo($where);
		$id =  $this->store_info['stripe_id'];  //主体账号id。类似：acct_1IT0XY2clnzANjLz		
		$address = unserialize($persons_info['address']);
		$document = unserialize($persons_info['document']);
		$data = array();
		$data['address']  = array(
			'city' 		=> $address['city'],  //城市
			'country' 	=> $address['country'],    //国际
			'line1' 	=> $address['line1'], //具体地址1
			'line2' 	=> $address['line2'], //具体地址2
			'state' 	=> $address['state'] //州/省
		);		
		$data['dob'] = array(
			'day' 	=>	'20',	//生日的日期		
			'month' =>	'02',  	//生日的月份	
			'year' 	=> 	'1989'  //生日的年份	
		);		
		$data['email']  	= $persons_info['email'];		//'test@qq.com'; //负责人
		$data['first_name'] = $persons_info['first_name'];	//'高';    //名
		$data['last_name'] 	= $persons_info['last_name'];	//'敏';	  //姓
		$data['verification']=array(
			'document' 	=>	array(
				"back"=> $document['back'], 	//身份证，背面，这里是接口upload_identity_document返回的
				"front"=>  $document['front'],	//身份证，正面这两个不能相同
			 ),			
		);
		//注意身份证图片，两个不能相同

		$result = $this->stripe->accounts->createPerson($id,$data);
		output_data($result);		
	}
		
		
	/*
	//编辑某一个负责人信息
	public function update_personOp(){
		
		$id 		= $_POST['id'];   //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];    //所有者的id 如：person_J5W2ya4TLzOeX6
		
		$data = array();
		$data['address']  = array(
			'city' 		=> '南京',
			'country' 	=> 'HK',
			'line1' 	=> '江宁万达',
			'line2' 	=> '10号楼',
			'state' 	=> 'Hong Kong'
		);		
		$data['dob'] = array(
			'day' 	=>	'20',			
			'month' =>	'02',
			'year' 	=> 	'1989'
		);		
		$data['email']  	= 'test@qq.com';
		$data['first_name'] = '高';
		$data['last_name'] 	= '敏';
		$data['verification']=array(
			'document' 	=>	array(
				"back"=>"file_1ITKIsGlMTtpJfUA3FALS975",
				"front"=>"file_1ITKzpGlMTtpJfUAlOwNk6zq",
			 ),			
		);
		//注意身份证图片，两个不能相同
		
		$result = $this->stripe->accounts->updatePerson($id,$persons_id,$data);
		output_data($result);		
	}
	

	//删除某一个负责人信息
	public function del_personOp(){		
		$id = $_POST['id'];    //主体账号id。类似：acct_1IT0XY2clnzANjLz
		$persons_id = $_POST['persons_id'];			  //所有者的id 如：person_J5W2ya4TLzOeX6	
		$result = $this->stripe->accounts->deletePerson($id,$persons_id);
	    output_data($result);			
	}
	*/


	//确认是否统一服务条款
	public function acceptanceOp(){
		
		// Set your secret key. Remember to switch to your live secret key in production.
		// See your keys here: https://dashboard.stripe.com/apikeys
	//	\Stripe\Stripe::setApiKey('sk_test_4eC39HqLyjWDarjtT1zdp7dc');
	
		$stripe_info = model('store_stripe')->getStoreStripeInfo(['store_id' => $this->store_info['store_id']]);
	
	
		$stripe_id = $stripe_info['stripe_id'];
		$this->stripe->accounts->update($stripe_id,
		  [
		    'tos_acceptance' => [
		      'date' => time(),
		      'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
		    ],
		  ]
		);
		
		$row = model('store_stripe')->editStoreStripe(['acceptance' => 1],['store_id' =>  $this->store_info['store_id']]);
		if($row){			
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
		
		
		
						
	}




}