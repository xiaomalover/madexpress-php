<?php
/**
 * 首页数据统计
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class operateControl extends mobileAdminControl {

    public function __construct(){
        parent::__construct();
		
		
		import('function.datehelper');
		
		$model_stat = Model('stat');
		$this->search_arr = $_POST;
		//$this->search_arr = $_GET;
		//处理搜索时间
		if ($this->search_arr['screen_type'] == 'day') {

		    $this->search_arr['search_type'] = 'day';
		    $this->search_arr['show_type'] = 'hour';
		    $this->search_arr['day']['search_time'] = date('Y-m-d', time());
		    $this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
	
		} elseif ($this->search_arr['screen_type'] == 'week') {
			$this->search_arr['search_type'] = 'week';
		    $this->search_arr['show_type'] = 'week';
		    $searchweek_weekarr = getWeek_SdateAndEdate(time());
		    $this->search_arr['week']['current_week'] = implode('|', $searchweek_weekarr);
	
		} elseif ($this->search_arr['screen_type'] == 'month') {
		   
			$this->search_arr['search_type'] = 'month';
		    $this->search_arr['show_type'] = 'day';
		    $this->search_arr['month']['current_year']= date('Y', time());
		    $this->search_arr['month']['current_month']= date('m', time());
			
		} elseif ($this->search_arr['screen_type'] == 'year') {
			
		    $this->search_arr['search_type'] = 'year';
		    $this->search_arr['show_type'] = 'month';
		    $this->search_arr['year']['current_year']= date('Y', time());
			
		}else{

			$this->search_arr['screen_type'] = 'day';
			$this->search_arr['show_type'] = 'hour';
			$this->search_arr['day']['search_time'] = date('Y-m-d', time());
			$this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
		}
		
		$searchtime_arr = $model_stat->getStarttimeAndEndtime($this->search_arr);

		$this->search_arr['stime'] = $searchtime_arr[0];
		$this->search_arr['etime'] = $searchtime_arr[1];
		
		
    }


	public function infoOp(){		
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		
		$model_order = model('order');		
		//日订单		
		$where  = array(
			'add_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime']))
		);
	
		$order_day_count = $model_order->getOrderCount($where);
		 
		$info = array();
		$info['order_day'] = $order_day_count > 0 ? $order_day_count : 0;
		$info['order_day_icon'] = "top";//上升top ,下降bottom
		$info['order_day_per'] = "0%";
		
		
		//日单价		
		$order_day_amount = $model_order->getOrderInfoN($where,'SUM(order_amount) as amount');		
		
		
		$info['price_day'] = $order_day_amount['amount'] > 0? ($order_day_amount['amount'] / $order_day_count) : 0;
		$info['price_day_icon'] = "top";//上升top ,下降bottom
		$info['price_day_per'] = "0%";

		//今日配送费
		$order_delivery_amount = $model_order->getOrderInfoN($where,'SUM(delivery_fee) as delivery_fee');			
		$info['fee_day'] = $order_delivery_amount['delivery_fee'] > 0 ? $order_delivery_amount['delivery_fee'] : 0;
		$info['fee_day_icon'] = "top";//上升top ,下降bottom
		$info['fee_day_per'] = "0%";
		
		//日注册用户
		$where  = array(
			'member_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime']))
		);		
		$member_num = model('member')->getMemberCount($where);		
		$info['reg_day'] = $member_num;
		$info['reg_day_icon'] = "top";//上升top ,下降bottom
		$info['reg_day_per'] = "0%";

		//日营收
		
		

		$info['money_day'] = $order_day_amount['amount'];
		$info['money_day_icon'] = "top";//上升top ,下降bottom
		$info['money_day_per'] = "0%";
		
		//日商均单
		
		$store_count = model('store')->getStoreCount(array('store_state' => array('in','1,2')));

		$store_jun_order = $order_day_count / $store_count;		
		
		
		$info['num_day'] = $store_jun_order;
		$info['num_day_icon'] = "top";//上升top ,下降bottom
		$info['num_day_per'] = "0%";




		$info['he_day'] = 0;
		$info['he_day_icon'] = "top";//上升top ,下降bottom
		$info['he_day_per'] = "0%";
		
		$info['view_day'] = 0;
		$info['view_day_icon'] = "top";//上升top ,下降bottom
		$info['view_day_per'] = "0%";




		
		
		//图表
		$weeks = $this->get_weeks();
		
		foreach($weeks as $v){			
			$x_val[] = date('d', strtotime($v));
			$where  = array(
				'add_time' =>array('between',array(strtotime($v),strtotime($v)+ 86400 - 1))
			);				
			$y_val[] = $model_order->getOrderCount($where);			
		}		
		$info['x_val'] = $x_val;
		$info['y_val'] = $y_val;		
		output_data($info);
	}
	


	private function  get_weeks($time = '', $format='Y-m-d'){
	  $time = $time != '' ? $time : time();
	  //组合数据
	  $date = [];
	  for ($i=1; $i<=7; $i++){
	    $date[$i] = date($format ,strtotime( '+' . $i-7 .' days', $time));
	  }
	  return $date;
	}
	 

	//统计分类
	public function kpi_data_leftOp(){
		$type = $_POST['screen_type'] ? $_POST['screen_type'] : 'day';

		switch($type){
			case 'day' :
				$data_type = '日';
			break;
			case 'week' :
				$data_type = '周';
			break;
			case 'month' :
				$data_type = '月';
			break;
			case 'year' :
				$data_type = '年';
			break;

		}


		$data = array(
			array(
				'menu_name' => $data_type."订单",
				'menu_count' =>  $this->stat_order(),
				'menu_type' => 'stat_order',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."单价",
				'menu_count' =>  $this->stat_order_price(),
				'menu_type' => 'stat_order_price',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."送餐费",
				'menu_count' =>  $this->stat_delivery_fee(),
				'menu_type' => 'stat_delivery_fee',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."注册用户",
				'menu_count' =>  $this->stat_member(),
				'menu_type' => 'stat_member',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."营收",
				'menu_count' =>  $this->stat_business(),
				'menu_type' => 'stat_business',
				'tips' =>  "$"
			),
			array(
				'menu_name' => $data_type."商户均单",
				'menu_count' =>  $this->stat_store_svg(),
				'menu_type' => 'stat_store_svg',
				'tips' =>  ""
			),
			array(
				'menu_name' => $data_type."配送员均时单",
				'menu_count' =>  $this->stat_delivery_svg(),
				'menu_type' => 'stat_delivery_svg',
				'tips' =>  "Min"
			),
			array(
				'menu_name' => $data_type."浏览量",
				'menu_count' =>  $this->stat_view_num(),
				'menu_type' => 'stat_view_num',
				'tips' =>  ""
			)
		);


		output_data(array('menu' => $data));
	}

	//linedata
	public function kpi_data_lineOp(){

		$type = $_POST['type'];
		switch($type){
			
			case 'stat_order':				
				$data = $this->stat_order_line();			
			break;
			case 'stat_order_price':
		

			break;			
			case 'stat_delivery_fee':
				
				$data = $this->stat_delivery_fee_line();

			break;			
			case 'stat_member':
				$data = $this->stat_member_line();
			break;
			case 'stat_business':
			

			break;
			case 'stat_store_svg':
			

			break;
			case 'stat_view_num':
			

			break;

		}

			

		output_data(['chart_data' => ['data' =>$data['barChartData'],'type'=>'line' ], 'column_name' => $data['columnName']]);
	

		

	}


	//统计订单
	private function stat_order(){

		$model_order = model('order');				
		$where  = array();
		$where['order_state'] = array('in','30,40,50,60');
		$where['add_time'] = array('between',array($this->search_arr['stime'],$this->search_arr['etime']));		
		$count = $model_order->getOrderCount($where);
		return $count;

	}

	//订单统计图
	private function stat_order_line(){

	
		$data_type = $_POST['screen_type'] ? $_POST['screen_type'] : 'day';

		switch($data_type){
			
			case 'day':			
				//获取排班设置
				$setting = model('scheduling_setting')->getSchedulingNew(TRUE);				
			//	print_r($setting);				
				$start_time = strtotime(date('Y-m-d',time()).' '.$setting[0]['start_time_date']);
				
				$end_time = strtotime(date('Y-m-d',time()).' '.$setting[1]['end_time_date']);  

				for($i = date('H',$start_time) ; $i<=date('H',$end_time); $i++){
					$columnName[]	= $i;
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;	

			break;
			case 'weeks':

				//图表
				$weeks = $this->get_weeks();	
				
				print_r($weeks);
				
				foreach($weeks as $v){			
					$columnName[] = date('d', strtotime($v));
					$where  = array(
						'add_time' =>array('between',array(strtotime($v),strtotime($v)+ 86400 - 1))
					);				
					$barChartData[] = model('order')->getOrderCount($where);			
				}		
				$columnName = $columnName;
				$barChartData = $barChartData;		


			break;			
			case 'month':

				$date = date('Y-m',time());
				$days = date('t',time());

				for($i = 1 ; $i<= $days ; $i++){
					$columnName[]	= date('d/M',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;


			break;			
			case 'year':
				$date = date('Y',time());
				for($i = 1 ; $i<= 12 ; $i++){
					$columnName[]	=  date('M Y',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;			

			break;
		}

		return ['columnName' => $columnName , 'barChartData' => $barChartData ];

	}


	//单价
	private function stat_order_price(){

		return 0;

	}




	//送餐费
	private function stat_delivery_fee(){

		$model_order = model('order');		
		//日订单		
		$where  = array();
		$where['order_state'] = array('in','30,40,50,60');
		$where['add_time'] = array('between',array($this->search_arr['stime'],$this->search_arr['etime']));		
		
		$amount = $model_order->getOrderInfoN($where,'SUM(delivery_fee) as delivery_fee');		
		return $amount['delivery_fee'] > 0 ? $amount['delivery_fee'] : 0;


	}

	//送餐费图表
	private function stat_delivery_fee_line(){

		$data_type = $_POST['screen_type'] ? $_POST['screen_type'] : 'day';

		switch($data_type){
			
			case 'day':			
				//获取排班设置
				$setting = model('scheduling_setting')->getSchedulingNew(TRUE);				
			//	print_r($setting);				
				$start_time = strtotime(date('Y-m-d',time()).' '.$setting[0]['start_time_date']);
				
				$end_time = strtotime(date('Y-m-d',time()).' '.$setting[1]['end_time_date']);  

				for($i = date('H',$start_time) ; $i<=date('H',$end_time); $i++){
					$columnName[]	= $i;
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;	

			break;
			case 'weeks':

				//图表
				$weeks = $this->get_weeks();	
				
				print_r($weeks);
				
				foreach($weeks as $v){			
					$columnName[] = date('d', strtotime($v));
					$where  = array(
						'add_time' =>array('between',array(strtotime($v),strtotime($v)+ 86400 - 1))
					);				
					$barChartData[] = model('order')->getOrderCount($where);			
				}		
				$columnName = $columnName;
				$barChartData = $barChartData;		


			break;			
			case 'month':

				$date = date('Y-m',time());
				$days = date('t',time());

				for($i = 1 ; $i<= $days ; $i++){
					$columnName[]	= date('d/M',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;


			break;			
			case 'year':
				$date = date('Y',time());
				for($i = 1 ; $i<= 12 ; $i++){
					$columnName[]	=  date('M Y',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;			

			break;
		}

		return ['columnName' => $columnName , 'barChartData' => $barChartData ];



	}



	//注册用户
	private function stat_member(){

		//日注册用户
		$where  = array();
		$where['member_time'] = array('between',array($this->search_arr['stime'],$this->search_arr['etime']));			
		$count = model('member')->getMemberCount($where);	
		return $count;

	}

	//送餐费图表
	private function stat_member_line(){

		$data_type = $_POST['screen_type'] ? $_POST['screen_type'] : 'day';

		switch($data_type){
			
			case 'day':			
				//获取排班设置
				$setting = model('scheduling_setting')->getSchedulingNew(TRUE);				
			//	print_r($setting);				
				$start_time = strtotime(date('Y-m-d',time()).' '.$setting[0]['start_time_date']);				
				$end_time = strtotime(date('Y-m-d',time()).' '.$setting[1]['end_time_date']);  

				for($i = date('H',$start_time) ; $i<=date('H',$end_time); $i++){
					$columnName[]	= $i;
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;	

			break;
			case 'weeks':

				//图表
				$weeks = $this->get_weeks();	
				
				print_r($weeks);
				
				foreach($weeks as $v){			
					$columnName[] = date('d', strtotime($v));
					$where  = array(
						'add_time' =>array('between',array(strtotime($v),strtotime($v)+ 86400 - 1))
					);				
					$barChartData[] = model('order')->getOrderCount($where);			
				}		
				$columnName = $columnName;
				$barChartData = $barChartData;		


			break;			
			case 'month':

				$date = date('Y-m',time());
				$days = date('t',time());

				for($i = 1 ; $i<= $days ; $i++){
					$columnName[]	= date('d/M',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;


			break;			
			case 'year':
				$date = date('Y',time());
				for($i = 1 ; $i<= 12 ; $i++){
					$columnName[]	=  date('M Y',strtotime($date.'-'.$i));
					$barChartData [] = rand(0,99);
				}				
				$columnName = $columnName;
				$barChartData = $barChartData;			

			break;
		}

		return ['columnName' => $columnName , 'barChartData' => $barChartData ];



	}


	//营业额
	private function stat_business(){

		$model_order = model('order');						
		$where  = array();
		$where['order_state'] = array('in','30,40,50,60');
		$where['add_time'] = array('between',array($this->search_arr['stime'],$this->search_arr['etime']));				
		$amount = $model_order->getOrderInfoN($where,'SUM(order_amount) as order_amount');		
		return $amount['order_amount'] > 0 ? $amount['order_amount'] : 0;

	}

	//商户均单
	private function stat_store_svg(){

		return 0;

	}


	//均时单
	private function stat_delivery_svg(){

		return 0;

	}

	//浏览量
	private function stat_view_num(){

		return 0;

	}
	
}
