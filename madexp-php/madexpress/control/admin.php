<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class adminControl extends mobileAdminControl {

    public function __construct(){
        parent::__construct();
    }


	public function indexOp(){		
		
		$admin_info = array();
		$admin_info['roles'] = ['admin'];
		$admin_info['introduction'] = 'I am a super administrator';
		$admin_info['avatar'] = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif';
		$admin_info['name'] = 'Super Admin';
		//$admin_info['']
		output_data($admin_info);
		
	}
	
	//
	public function logoutOp(){
		
		output_data('安全登出');
		
		
	}


	 
	
	
	
}
