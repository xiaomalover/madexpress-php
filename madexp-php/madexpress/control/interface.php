<?php
/**
 * 界面操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class interfaceControl extends mobileAdminControl {
  
    
    public function __construct(){
        parent::__construct();
	
    }

	
	//列表
	public function interface_listOp(){		
		
		
		$model  = Model('adv_ap');
		$list = $model->getAdvApList(TRUE);		
		$data = array();
		foreach($list as $k => $v){				
			$ap_list[$v['ap_id']] = $v['ap_name'];
		}	
		
		
		
		
		$model_adv  = Model('adv_new');
		$condition  = array();
		
		if ($_POST['adv_title'] != '') {
			$condition["adv_title"] = $_POST['adv_title'];
		}
		
		if ($_POST['query'] != '' && in_array($_POST['qtype'],array('ap_name'))) {
		    $condition[$_POST['qtype']] = $_POST['query'];
		}
		
		$condition['ap_id'] = $_POST['ap_id']  > 0 ?$_POST['ap_id']  : 3  ;
			
		$adv_list  = $model_adv->getAdvList($condition,'*',$this->page,'adv_id desc');   
		
		$page_count = $model_adv->gettotalpage();
		$list_count = $model_adv->gettotalnum();
		$list = array();
		
		
		
		
		
		
		
		foreach ($adv_list as $k => $v) {
		   	$data = array();		
			$data['adv_code'] = $v['adv_code'];
			$data['adv_id'] = $v['adv_id'];
			$data['ap_name'] = $ap_list[$v['ap_id']];			
			$data['adv_title'] = $v['adv_ap_title'];
		    $data['adv_title'] = $v['adv_title'];
		    $data['adv_start_date'] = date('Y-m-d',$v['adv_start_date']);
		    $data['adv_end_date'] = date('Y-m-d',$v['adv_end_date']);
		    $data['is_show'] = $v['is_show'] == 0 ? false : true;
		    $data['click_num'] = $v['click_num'];	
				
		    $list[] = $data;
		}
				
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
			
	}


	
	//
	
	public function adv_initOp(){
		
		$where = array();
		$where['language_system'] = 1;
		$lang_list = model('language')->getLangList($where);	
		foreach($lang_list as $k=> $v){			
		
		//	$adv_title[$k]['lang_name'] = $v['language_flag'];
		//	$adv_title[$k]['adv_title'] = '';			
		
			$adv_content[$k]['lang_name'] = $v['language_flag'];
			$adv_content[$k]['adv_content'] = '';			
		
			//$adv_text[$k]['lang_name'] = $v['language_flag'];
			//$adv_text[$k]['adv_text'] = '';
			
			
		}	
		
		$data = array();
		$data['adv_title'] ='';
		$data['adv_content'] = $adv_content;
		$data['adv_text'] = $adv_text;
		$data['adv_start_date'] = '';
		$data['adv_end_date'] = '';
		$data['slide_sort'] = 255;
		$data['click_num'] = 0;
		$data['is_show'] = 1;		
		$data['adv_code'] = 'MEO'.rand(10000,99999);
		$data['adv_pic_url'] = '';		
		
		
		output_data($data);
			
			
		
	}
	



	 public function adv_saveOp(){

	 		$adv  = Model('adv_new');	      
		
	         $obj_validate = new Validate();
	         $validate_arr = array();
	         $validate_arr[] = array("input"=>$_POST["adv_title"], "require"=>"true", "message"=>$lang['adv_can_not_null']);         
	         $validate_arr[] = array("input"=>$_POST["ap_id"], "require"=>"true", "message"=>$lang['must_select_ap']);
	         $validate_arr[] = array("input"=>$_POST["adv_start_time"], "require"=>"true", "message"=>$lang['must_select_start_time']);
	         $validate_arr[] = array("input"=>$_POST["adv_end_time"], "require"=>"true", "message"=>$lang['must_select_end_time']);
	      //   $validate_arr[] = array("input"=>$_FILES['adv_pic']['name'], "require"=>"true", "message"=>$lang['picadv_null_error']);
	         $obj_validate->validateparam = $validate_arr;
	         $error = $obj_validate->validate();
	         if ($error != ''){
	               exit(json_encode(array('code'=> 400,'msg'=>$error))); 
	 			  
	         }else {				
	             $insert_array['adv_title']       = trim($_POST['adv_title']);
	             $insert_array['ap_id']           = intval($_POST['ap_id']);
	             $insert_array['adv_start_date']  = strtotime($_POST['adv_start_time']);
	             $insert_array['adv_end_date']    = strtotime($_POST['adv_end_time']);				
	 			if($insert_array['adv_end_date'] <   $insert_array['adv_start_date']){					
	 				 exit(json_encode(array('code'=> 400,'msg'=>'结束时间不得小于开始时间'))); 
				 }
				 

	            $insert_array['is_show']         	= '1';
	 			$insert_array['adv_code']           = $_POST['adv_code'];	
	 			$insert_array['adv_pic_url']        = $_POST['adv_pic_url'];	
				$insert_array['adv_pic_text']       = $_POST['adv_pic_text'];	
				 $insert_array['adv_text']           = $_POST['adv_text'];				
				 
				$adv_pic = $_POST['adv_content'];
				if($adv_pic){
					$path = BASE_UPLOAD_PATH .DS. 'interface' .DS;
					foreach($adv_pic as $k => $v){
						$v['lang_name'] = $v['lang_name'];
						if($v['file_name']){
							$v['adv_content'] = $v['file_name'];
						}else{
							$v['adv_content'] = $this->base64_image_content($v['adv_content'],$path);
						}	
						$adv_content[$k] = $v;
					}
				}

				$insert_array['adv_content']        = serialize($adv_content);		
					
	 		    if($_POST['adv_id'] > 0){
	 			   $where = array(
	 			  	 	'adv_id' => $_POST['adv_id']
	 			   );
	 			   $result = $adv->editAdv($where,$insert_array);
	 		   }else{
	 			   $result = $adv->addAdv($insert_array);				   
	 			}
	             //广告信息入库
	             if ($result){
	                  output_data($result);
	             }else {
	                  output_error($result);
	             }
	 		}
	 	
	 }
	 
	 //上传图片
	 
	 public function uploadAdsOp(){
	 	  $upload     = new UploadFile();		  
	 	  $upload->set('default_dir',ATTACH_ADV);
	       $result = $upload->upfile('file');
	       if (!$result){
	 			echo json_encode(array('code'=> 40000,'msg'=>$upload->error));
	        }else{
	 		    echo json_encode(array('code'=> 20000,'url'=>$upload->file_name)); 
	 	 }
	 		
	 		
	 	
	 }
	
	public function adv_editOp(){
		
		$adv  = Model('adv_new');
		
		$where = $data = array();
		$where["adv_id"] = $adv_id = $_POST['adv_id'];
		
		$data["is_show"] = $is_show = ($_POST['is_show']) ? 1 : 0;

		$result = $adv->editAdv($where,$data);
		
		output_data($result);
	}
		
		
	public function adv_delOp(){
		$adv  = Model('adv_new');
		
		$where = array();
		$where["adv_id"] = $adv_id = $_POST['adv_id'];

		$result = $adv->delAdv($where);
		
		output_data($result);
	}
	
	
	public function ap_manageOp(){
			
	
		$model  = Model('adv_ap');	   
		$list = $model->getAdvApList(TRUE);		
		$data = array();
		foreach($list as $k => $v){		
			$v['adv_num'] = $this->getAdvNum($v['ap_id']);		
			$data[$k] = $v;
		}	
		output_data($data);
	
	}
	
	private function getAdvNum($ap_id){
			$adv  = Model('adv_new');			
			$where = array();
			$where['ap_id'] = $ap_id;
			return $adv->getAdvCount($where);
	}
	
	
	public function getAdvInfoOp(){
			$adv  = Model('adv_new');			
			$where = array();
			$where['adv_id'] = $_POST['adv_id'];
			$res = $adv->getAdvInfo($where);
			
			$res["adv_start_time"] = date("Y-m-d",$res["adv_start_date"]);
			$res["adv_end_time"] = date("Y-m-d",$res["adv_end_date"]);
			
			$adv_content = unserialize($res['adv_content']);
			foreach($adv_content as $k => $v){
				$v['file_name'] = $v['adv_content'];
				$v['adv_content'] = UPLOAD_SITE_URL.'/interface/'.$v['adv_content'];
			
				$list[$k] = $v;
				
			}
			
			
			$res["adv_content"]  = $list;
			

			output_data($res);
			
	}


	//上传图片
	private function base64_image_content($base64_image_content, $path)
	{
		//匹配出图片的格式
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)) {
			$type = $result[2];
			$new_file = $path;
			if (!file_exists($new_file)) {
				//检查是否有该文件夹，如果没有就创建，并给予最高权限
				mkdir($new_file, 0700);
			}
			$file_name = time().rand(10000, 99999).".{$type}";
			$new_file = $new_file.$file_name ;
			if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))) {
				return $file_name;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}


	
	
}
