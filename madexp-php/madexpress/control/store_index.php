<?php
/**
 * 商户订单管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_indexControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
		
		
		import('function.datehelper');
		
		$model_stat = Model('stat');
		$this->search_arr = $_POST;
		//$this->search_arr = $_GET;
		//处理搜索时间
		if ($this->search_arr['stattype'] == 'yesterday') {
		    $this->search_arr['search_type'] = 'day';
		    $this->search_arr['show_type'] = 'hour';
		    $this->search_arr['day']['search_time'] = date('Y-m-d', time());
		    $this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
			
		} elseif ($this->search_arr['stattype'] == 'week') {
			
		    $this->search_arr['search_type'] = 'week';
		    $this->search_arr['show_type'] = 'week';
		    $searchweek_weekarr = getWeek_SdateAndEdate(time());
		    $this->search_arr['week']['current_week'] = implode('|', $searchweek_weekarr);
			
		} elseif ($this->search_arr['stattype'] == 'month') {
		   
			$this->search_arr['search_type'] = 'month';
		    $this->search_arr['show_type'] = 'day';
		    $this->search_arr['month']['current_year']= date('Y', time());
		    $this->search_arr['month']['current_month']= date('m', time());
			
		} elseif ($this->search_arr['stattype'] == 'year') {
			
		    $this->search_arr['search_type'] = 'year';
		    $this->search_arr['show_type'] = 'month';
		    $this->search_arr['year']['current_year']= date('Y', time());
			
		}else{
			
			$this->search_arr['search_type'] = 'day';
			$this->search_arr['show_type'] = 'hour';
			$this->search_arr['day']['search_time'] = date('Y-m-d', time());
			$this->search_arr['day']['search_time'] = strtotime($this->search_arr['day']['search_time']);		 
		}
		
		$searchtime_arr = $model_stat->getStarttimeAndEndtime($this->search_arr);
		
		$this->search_arr['stime'] = $searchtime_arr[0];
		$this->search_arr['etime'] = $searchtime_arr[1];
		
		
		
		
		
    }
	
	
	public function indexOp(){
		
		$seller_info = array();
		$seller_info['roles'] = ['admin'];
		$seller_info['introduction'] = 'I am a super administrator';
		$seller_info['avatar'] = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif';
		$seller_info['name'] = $this->store_info['store_name_primary'];
		//$admin_info['']
		output_data($seller_info);
		
	}
	
	//数据统计
	public function statOp(){
		
		
	}
	
	
	//热销商品
	public function hotgoods_foodboxOp(){
		
		
		$where = array(
			'store_id' => $this->store_info['store_id'],
			'is_old' => 0,
			'is_delete' => 0
		);
		$sales_list = model('goods')->getGoodsList($where, '*','','goods_sales_month desc', 10);
		
		$data = array();
		foreach($sales_list as $k => $v){
				$lang = $this->goods_language($v['goods_id']);
				$data[$k] = $v;
				$data[$k]['goods_name'] = $lang['goods_name'];
				$data[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);   
		}			
		 
		 
		$foodbox_list  = model('foodbox_class')->getClassList(TRUE);
		foreach($foodbox_list as $k => $v){
		 			$foodbox[$k]['class_name'] = $v['class_name'];
		 			$foodbox[$k]['class_id'] = $v['class_id'];
		 			$foodbox[$k]['foodbox_num'] = $this->foodboxNum($v['class_id']);
		 			
		}		 
		
		output_data(array('hot_list'=> $data,'foodbox' => $foodbox));
	}
	

	
	//店铺信息
	public function store_infoOp(){
		
		
		$model = model('store');
		$store_info = $model->getStoreInfo(array('store_id'=> $this->store_info['store_id']));
		$store_info['store_avatar'] =  $store_info['store_avatar']
		    ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_info['store_avatar']
		    : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
		
		$store_info['store_banner'] != '' ?  explode(",", $store_info['store_banner']) : array();		
		
		
		$bindclass= model('store_bind_class')->getStoreBindClassList(array('store_id'=> $this->store_info['store_id'] ));
		$class = '';
		foreach($bindclass as $v){
			$class .= $v['class_name'].'; ';			
		}		
		$store_info['store_class'] = $class;
		
		//分组的店铺分类
		
		$cuisine_data = model('category')->getGoodsClassList(array('cate_parent_id' => 1));		
		$data = array();
		foreach($cuisine_data as $k => $v){
			$data[$k] = $v;
			$data[$k]['child'] = $this->getSmallChild($v['cate_id']);
		}
		$store_info['smallClass'] = $data;
		
		
		//绑定的语言报
		$bindlang = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));				
		$data = array();
		foreach($bindlang as $v){
			$data[] = $v['language_name'];
		}
		$bindlang = implode(",", $data);		
		$store_info['store_lang'] = $bindlang;
		
		//日期
		$dateTime = array(
		 	'day' => date('d',time()),
			'month' => date('M',time()),
			'year' => date('y',time())
		);			 
		
		$store_info['store_shophours'] = unserialize($store_info['store_shophours']);	
		
		
		$store_info['store_estimated_time'] = !empty($store_info['store_estimated_time']) ? unserialize($store_info['store_estimated_time']) : array('start_time'=> 20,'end_time'=>40);
		
		//$store_info['store_stripe_info'] = $this->get_stripe_info();
		
		$store_info['store_taking_type'] = $store_info['store_taking_type'];
		
		
		//店铺照片
		$gdurl = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/shop/store/";
		$b=array();
		if(!empty($store_info['store_banner'])){
			$store_info['store_banner'] = explode(",", $store_info['store_banner']);
			foreach($store_info['store_banner'] as $k=>$v){
				$b[$k]["file_name"] = $gdurl.$v;
			}
		}
		$store_info['store_banner'] = $b;
		
		//指引图照片
		$guide=array();
		if(!empty($store_info['store_guide_map'])){
			/*$store_info['store_guide_map'] = explode(",", $store_info['store_guide_map']);
			foreach($store_info['store_guide_map'] as $k=>$v){
				$guide[$k]["file_name"] = $gdurl.$v;
			}*/
			$guide = json_decode($store_info['store_guide_map'],true);
			if($guide){
				foreach($guide as $k=>$v){
					$guide[$k]["file_name"] = $gdurl.$v["file_name"];
				}
			}else{
				for($i=0; $i<3;$i++){
					$guide[$i]["file_name"] = $gdurl."nopic.jpg";
					$guide[$i]["text"] = "";
				}
			}
		}else{
			for($i=0; $i<3;$i++){
				$guide[$i]["file_name"] = $gdurl."nopic.jpg";
				$guide[$i]["text"] = "";
			}
		}
		$store_info['store_guide_map'] = $guide;
		$store_info['nopic'] = $gdurl."nopic.jpg";

		
		//平台所有语言
		$language = model('language')->getLangList(TRUE);	
		
		
		//STRIPE账户信息
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];		
		$stripeinfo =  model('store_stripe')->getStoreStripeInfo($where);		
		if(!empty($stripeinfo)){
			$stripeinfo['data'] = 'yes';
			$stripeinfo['address'] = unserialize($stripeinfo['address']);									
		}else{
			$stripeinfo = array('data' => 'no');
		}		
		$store_info['stripe_base'] = $stripeinfo;
		
		
		
		
		//公司负责人	
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$persons_info = model('store_stripe_persons')->getStoreStripePersonsInfo($where);
		if(!empty($persons_info)){		   
			$persons_info['data'] = 'yes';
			$persons_info = $persons_info;		   
		}else{
			$persons_info = array('data' => 'no');
		}
		$store_info['persons_info'] = $persons_info;
		
		
		
		//银行账户
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];	
		$bank_list = model('store_stripe_bank')->getStoreStripeBankList($where);		
		if(!empty($bank_list)){			
			$stripe_banklist = $bank_list;			
		}else{
			$stripe_banklist = array();
		}
		$store_info['stripe_bank'] = $stripe_banklist;
		
		
		
		//统计
		
		$model_order = model('order');
		//日订单		
		$where  = array(
			'add_time' =>array('between',array($this->search_arr['stime'],$this->search_arr['etime'])),
			'store_id' => $this->store_info['store_id']			
		);			
	

		$order_day_count = $model_order->getOrderCount($where);
	
		$info = array();
		$info['today_num'] = intval($order_day_count);
		$info['today_num_icon'] = "top";//上升top ,下降bottom;
		$info['today_num_per'] = "0%";
		
		
		$order_day_amount = $model_order->getOrderInfoN($where,'SUM(order_amount) as amount');		
		
		$info['today_money'] = $order_day_amount['amount'] > 0 ? $order_day_amount['amount'] : 0 ;
		$info['today_money_icon'] = "top";//上升top ,下降bottom;
		$info['today_money_per'] = "0%";
		
		
		$info['date_d'] = date("d");
		$info['date_ym'] = gmstrftime("%b %Y",time());
		
		$info['high_num'] = 0;
		$info['high_num_icon'] = "bottom";//上升top ,下降bottom
		$info['high_num_per'] = "0%";
		
		if($order_day_amount['amount'] > 0){		
			$avg_price = ($order_day_amount['amount'] / $order_day_count);
		}else{
			$avg_price = 0;
		}
		
		$info['avg_price'] = $avg_price > 0 ? $avg_price : 0;
		$info['avg_price_icon'] = "top";//上升top ,下降bottom
		$info['avg_price_per'] = "0%";

		$info['customer'] = 0;
		$info['customer_icon'] = "bottom";//上升top ,下降bottom
		$info['customer_per'] = "0%";
		
		$info['good_comment'] = 0;
		$info['good_comment_icon'] = "top";//上升top ,下降bottom
		$info['good_comment_per'] = "0%";
		
		
		
		
		
		
		
		
		
		output_data(array('store_info' => $store_info,'info' => $info,'language' => $language));
		
	}
	
	
	//获取子类
	private function getSmallChild($cuisine_id){
		
		$where = array(
			'cate_parent_id' => $cuisine_id
		);
		$list= model('category')->getGoodsClassList($where);
		$data = array();
		foreach($list as $k => $v){
			$data[$k] = $v;
		}
		
		return $data;
	}
	
	
		
	private function foodboxNum($class_id){		
			$where = array(
				'class_id' => $class_id,
				'store_id' => $this->store_info['store_id']
			);
			$data = model('store_foodbox_goods')->getStoreFoodboxGoodsInfo($where,'SUM(goods_num) as num' );		
			return $data['num'];
	}
		
		
	private function goodsImageUrl($image,$store_id){
			
			if(empty($image)){
				return '';
			}
			$image = explode(",", $image);			
			return UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS . $image[0];
			
	}
	
	//查询语言参数
	private function goods_language($goods_id){		
			$where = array(
				'goods_id' =>$goods_id,
				'lang_id' => $this->store_info['store_default_lang']
			);
			$data = model('goods_language')->getGoodsLangInfo($where);
			if(!empty($data)){
				return $data;
			}else{
				return false;
			}
			
	}
		
	
	
	
	
	//修改营业时间	
	public function time_saveOp(){
			$model = model('store_examine');
			if(empty($_POST['time'])){
				output_error('请选择时间');					
			}	
			
			$update = array(
				'store_shophours' =>  serialize($_POST['time'])
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = model('store')->editStore($update,$where);
			
			
			/*
			
			if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){					
				
				$update = array(
					'store_shophours' =>  serialize($_POST['time'])
				);
				$where = array(
					'store_id' => $this->store_info['store_id']
				);			
				$result = model('store')->editStore($update,$where);							
				
			}else{
			
				$data = array(
					'examine_type' => 'shophours',
					'examine_title' => '修改经营时间',
					'examine_old_data' => $this->store_info['store_shophours'],
					'examine_new_data' => serialize($_POST['time']),
					'store_id' => $this->store_info['store_id'],
					'addtime' => time(),
					'examine_sn' => 'MEO'.date('YmdHis',time())
					
				);			
				
				$result = $model->addStoreExamine($data);					
			}
			*/
			
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');				
			}
	}
	
	
	
	
	
	//修改经营品类
	public function class_saveOp(){
			$model = model('store_examine');			
			
		//	print_r($this->admin_info);
			if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){				
				model('store_bind_class')->delStoreBindClass(array('store_id'=> $this->store_info['store_id']));				
							
				$where = array(
					'cate_id' => array('in',implode(",", $_POST['class_first']))
				);			
				$small_class = model('category')->getGoodsClassList($where);		
				
				foreach($small_class as $v){
					$bind_class[] = array(
						'store_id' => $this->store_info['store_id'],
						'class_id' => $v['cate_id'],
						'class_name' => $v['cate_name'],
						'class_type' => 1
					);
				}
						
			
				$where = array(
					'cate_id' => array('in',implode(",", $_POST['class_last']))
				);			
				$small_class = model('category')->getGoodsClassList($where);				
				foreach($small_class as $v){
					$bind_class[] = array(
						'store_id' => $this->store_info['store_id'],
						'class_id' => $v['cate_id'],
						'class_name' => $v['cate_name'],
						'class_type' => 0
					);
				}
				
				
				
				
				$result = model('store_bind_class')->addStoreBindClassAll($bind_class);
				
				
			}else{						
				$data = array(
					'examine_type' => 'class',
					'examine_title' => '修改经营类目',
					'examine_old_data' => $_POST['store_old_class'],
					'examine_new_data' => $_POST['store_new_class'],
					'store_id' => $this->store_info['store_id'],
					'addtime' => time()
				 );		
				$result = $model->addStoreExamine($data);				
			}
						
			if($result){	
					output_data('操作成功');
			}else{
					output_error('操作失败');	
			}
	}
	
	
	
	//修改LOGO	
	public function avatar_saveOp(){
		
		
		$upload = new UploadFile();
		
		$path = BASE_UPLOAD_PATH .DS. ATTACH_STORE . DS . $upload->getSysSetPath();
		$file_name = $this->base64_image_content($_POST['image'],$path);
		
		
		
		$model = model('store');
		$result = $model->editStore(array('store_avatar' => $file_name),array('store_id' => $this->store_info['store_id']));
		
		output_data(array('image' => $file_name));		
		
		/*
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
		*/
	}
	
	//上传店铺照片
	public function banner_uploadOp(){
		$upload = new UploadFile();
		
		$path = BASE_UPLOAD_PATH .DS. ATTACH_STORE . DS . $upload->getSysSetPath();
		
		$file_name = $this->base64_image_content($_POST['image'],$path);
		
		output_data(array('image' => 'http://'.$_SERVER['SERVER_NAME']."/data/upload/shop/store/".$file_name));
	}
	
	//保存店铺照片
	public function banner_saveOp(){
		$arr=array();
		$gdurl = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/shop/store/";
		if($_POST["store_banner"]){
			foreach($_POST["store_banner"] as $k=>$v){
				array_push($arr,str_replace($gdurl,"",$v["file_name"]));
			}
		}
		
		$store_banner = implode(",",$arr);
		$model = model('store');
		$result = $model->editStore(array('store_banner' => $store_banner),array('store_id' => $this->store_info['store_id']));
		
		output_data('操作成功');	
	}
	
	//保存指引图照片
	public function guide_saveOp(){
		$arr=array();
		$gdurl = 'http://'.$_SERVER['SERVER_NAME']."/data/upload/shop/store/";
		if($_POST["store_guide_map"]){
			foreach($_POST["store_guide_map"] as $k=>$v){
				//array_push($arr,str_replace($gdurl,"",$v["file_name"]));
				$arr[$k]["file_name"] = str_replace($gdurl,"",$v["file_name"]);
				$arr[$k]["text"] = $v["text"];
			}
		}
		
		$store_guide_map = json_encode($arr);
		$model = model('store');
		$result = $model->editStore(array('store_guide_map' => $store_guide_map),array('store_id' => $this->store_info['store_id']));
		
		output_data('操作成功');	
	}
	
	private function  base64_image_content($base64_image_content,$path){
	//匹配出图片的格式
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
			$type = $result[2];
			$new_file = $path;
			if(!file_exists($new_file)){
				//检查是否有该文件夹，如果没有就创建，并给予最高权限
				mkdir($new_file, 0700);
			}
			$file_name = time().rand(10000,99999).".{$type}";
			$new_file = $new_file.$file_name ;
			if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
				return $file_name;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	//修改公告
	public function notice_saveOp(){		
			$model = model('store');		
			$update = array(
				'store_notice' => $_POST['notice']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	
	//修改联系电话
	public function tel_saveOp(){		
			$model = model('store');		
			$update = array(
				'store_phone' => $_POST['tel']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	//修改经营信息
	public function stripe_saveOp(){		
			$model = model('store_stripe');
			
			$update = array(
				'name' => $_POST['info']['name'],
				'phone' => $_POST['info']['phone'],
				'address' => $_POST['info']['address'],
				'email' => $_POST['info']['email'],
				'tax_id' => $_POST['info']['tax_id'],
				'type' => $_POST['info']['type'],
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);
			
			$exist = $model->getStoreStripeInfo($where);
			
			if(empty($exist)){
				$update["store_id"] = $this->store_info['store_id'];
				$result = $model->addStoreStripe($update);
			}else{
				$result = $model->editStoreStripe($update,$where);
			}

			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	//修改接单状态
	public function store_taking_type_saveOp(){		
			$model = model('store');		
			$update = array(
				'store_taking_type' => $_POST['store_taking_type']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	
	//修改简介
	public function about_saveOp(){
		
			$model = model('store');						
			$update = array(
				'store_about' => $_POST['about']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
	}
	
	//获取mapbox地址
	/*
	public function get_mapbox_addressOp(){
		
		
		$keyword = $_POST['keyword'];
		
		$data = $this->posturl('http://api.mapbox.com/geocoding/v5/mapbox.places/',$keyword);
		
		print_r($data);
		output_data(array('list' => $data['features']));
		
		
		
	}
	
	private function posturl($url,$params=false,$ispost=0){
	    $httpInfo = array();
	    $ch = curl_init();
	 
	    curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
	    curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
	    curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
	   // curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true);
	    if( $ispost )
	    {
	        curl_setopt( $ch , CURLOPT_POST , true );
	        curl_setopt( $ch , CURLOPT_POSTFIELDS , $params );
	        curl_setopt( $ch , CURLOPT_URL , $url );
	    }
	    else
	    {
	        if($params){
	            curl_setopt( $ch , CURLOPT_URL , $url.$params.'.json?access_token=pk.eyJ1IjoibWFkZXhwcmVzcyIsImEiOiJjazNyc2R5NWswYjdhM2RvMDFiajJqYWtzIn0.NXEtoljG67Oa2ao0vBTNLQ');
	        }else{
	            curl_setopt( $ch , CURLOPT_URL , $url);
	        }
	    }
	    $response = curl_exec( $ch );
	    if ($response === FALSE) {
	      //  echo "cURL Error: " . curl_error($ch);
	        return false;
	    }
	    $httpCode = curl_getinfo( $ch , CURLINFO_HTTP_CODE );
	    $httpInfo = array_merge( $httpInfo , curl_getinfo( $ch ) );
	    curl_close( $ch );
	    return $response;
	}
	*/
	
	//修改地址	
	public function address_saveOp(){		
		
			$model = model('store_examine');	
		//	print_r($this->admin_info);
		/*	if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){												
				$update = array(
					'store_address' => $_POST['address'],
					'store_lat' => $_POST['lat'],
					'store_lng' => $_POST['lng']
				);				
				$where = array(
					'store_id' => $this->store_info['store_id']
				);	
				$result = model('store')->editStore($update,$where);	
			}else{
				
				$data = array(
					'examine_type' => 'address',
					'examine_title' => '修改商户地址',
					'examine_old_data' => $this->store_info['store_address'],
					'examine_new_data' => $_POST['address'],
					'examine_code' => 'ME'.date('YmdHis',time()),
					'store_id' => $this->store_info['store_id'],
					'addtime' => time()
				 );						
				$result = $model->addStoreExamine($data);		
					
			}*/
			
			$update = array(
				'store_address' => $_POST['address'],
				'store_lat' => $_POST['lat'],
				'store_lng' => $_POST['lng'],
				'bind_money_nav' => $_POST['bind_money']
			);				
			$where = array(
				'store_id' => $this->store_info['store_id']
			);	
			$result = model('store')->editStore($update,$where);
			if($result){					
				$this->editStoreDj();				
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
		
	}
	
	private function editStoreDj(){
		$store = model('store')->getStoreList(TRUE);
		foreach($store as $v){
			$die = array();
			foreach($store as $b){			
				$distance = model('order_allocation')->getDistance($v['store_lat'], $v['store_lng'], $b['store_lat'], $b['store_lng']);					
				if($distance <= 1500 && $v['store_id'] != $b['store_id']){
					$die[] = $b['store_id'];			
				}
			}
			$where = array(
				'store_id' => $v['store_id']
			);
			$update = array(
				'store_superpose' => implode(',',$die)
			);
			model('store')->editStore($update,$where);
		}	
	}
	
	
	public function get_region16Op(){		
		$region_list =  model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));		
		$data = array();
	  	foreach ($region_list as $k => $v) {
		    $data[$k]['region_id'] 		= $v['region_id'];
		    $data[$k]['region_name'] 	= $v['region_name'];
		    $data[$k]['region_color'] 	= $v['region_color'];	    		  
		}
		output_data(['list' => $data]);
	}
	
	
	
	//保存预计送达时间
	public function save_estimated_timeOp(){	
		
		$update = array(
			'store_estimated_time' => serialize($_POST['time'])			
		);
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);			
		
		$result = model('store')->editStore($update,$where);	
		
		if($result){
			output_data('操作成功');
		}else{
			output_error('操作失败');	
		}		
	}
	
	
	//获取基本信息
	private function get_stripe_info(){
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);		
		$row = model('store_stripe')->getStoreStripeInfo($where);		
		return $row;		
	}
	
	
	
	//修改专员
	public function  save_mavinOp(){		
		
		$update = array(
			'admin_id' => $_POST['admin_id'],
			'admin_name' => $_POST['admin_name']
		);
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);					
		$result = model('store')->editStore($update,$where);			
		if($result){
			output_data('操作成功');
		}else{
			output_error('操作失败');	
		}			
	}
	
	
	//专员列表
	public function mavin_listOp(){
		
		$model_admin = model('admin');
		$where = array();
		$where['admin_type'] = 1;
		$admin_list = Model('admin')->getAdminList($where,1);
		foreach($admin_list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['examine_num'] = 0;
			$data[$k]['store_up'] = 0;
			$data[$k]['store_num'] = 0;			
			$data[$k]['admin_state_name'] = $v['admin_state'] == 1 ? '启用' : '禁用';
			
		}
				
		output_data(array('list' => $data));
		
		
		
		
	}
	
	//修改
	public function lang_saveOp(){
		
		$lang =  implode(',',$_POST['lang']);
		$where = array();
		$where['language_name'] = array('in',$lang);		
		$list = model('language')->getLangList($where);
		
		
		//删除以往设置的
		model('store_language')->delStoreLang(array('store_id' => $this->store_info['store_id']));
		
		
		foreach($list as $k=> $v){
			$data = array();			
			$data['language_id'] = $v['language_id'];
			$data['store_id'] 	= $this->store_info['store_id'];
			$data['language_name'] = $v['language_name'];
			$data['language_en'] = $v['language_flag'];
			$data['language_icon'] = $v['language_icon'];
			$data['is_default'] = $k == 0 ? 1 :0 ;
			$inst_data[] = $data;
		}
		
		$row = model('store_language')->addStoreLangAll($inst_data);
		
		if($row){			
			output_data('修改完成');
		}else{
			output_error('修改失败');
		}
		
	}
	
	
	
}
