<?php
/**
 * 专员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class merchantControl extends mobileAdminControl{
 
	public function __construct(){
        parent::__construct();   
    }

	
	public function get_listOp(){
		
		$model_admin = model('admin');
		$where = array();
		$where['admin_type'] = 1;
		if($_POST["keyword"]){
			$where['admin_name'] = $_POST['keyword'];
		}
        $admin_list = Model('admin')->getAdminList($where,1);
		
		$page_count = $model_admin->gettotalpage();
		$list_count = $model_admin->gettotalnum();
		
		if(!empty($admin_list)){
			foreach($admin_list as $k=> $v){
				$data[$k] = $v;
				$data[$k]['examine_num'] = 0;
				$data[$k]['store_up'] = 0;
				$data[$k]['store_num'] = $this->get_store_num($v['admin_id']) ;			
				$data[$k]['admin_state_name'] = $v['admin_state'] == 1 ? '启用' : '禁用';
				
			}
		}
				
		output_data(array('list' => $data), mobile_page($page_count,$list_count));
		
		
	}
	
	private function get_store_num($admin_id){
		
		
		return   model('store')->getStoreCount(array('admin_id' => $admin_id));	
		
	}
	
	
	//
	public function mavinSaveOp(){
		
	
			// 验证用户名是否重复
			$check_member_name  = model('member')->getMemberInfo(array('member_code'=>$_POST['member_code']));
			if(empty($check_member_name)) {			
				output_error('您输入的会员编号不存在');
			}
				
			$data = array();
			$data['admin_name'] = $_POST['admin_name'];
			$data['nickname'] 	= $_POST['nickname'];			
			$data['admin_type']  = 1;
			$data['member_code'] = $_POST['member_code'];			
			$data['member_id'] = $check_member_name['member_id'];
			if($_POST['admin_id'] > 0 ){			
			
				$data['admin_id'] = $_POST['admin_id'];
				if($_POST['admin_password']!="******"){
					$data['admin_password'] = md5($_POST['admin_password']);
				}
				$admin_id = model('admin')->updateAdmin($data);				
				model('member')->editMember(['member_code' => $_POST['member_code']],['admin_id' => $_POST['admin_id']]);


			}else{
				$data['admin_password'] = md5($_POST['admin_password']);
				$admin_id = model('admin')->addAdmin($data);				

				model('member')->editMember(['member_code' => $_POST['member_code']],['admin_id' => $admin_id]);


			}
			if($admin_id){
				//更新
				output_data('添加成功');				
			}else{
				output_error('添加失败');				
			}
	
		
	}
	
	
	
	
	

	public function adminInfoOp(){
		$model_admin = model('admin');
		$row = $model_admin->getAdminInfoNew(array("admin_id"=>$_POST['admin_id']));	
		$row["admin_password"] = "******";
		
		if($row){
			output_data($row);			
		}else{
			output_error('操作失败');			
		}
	}
	   
	 //删除
	public function adminDelOp(){		
		$row = model('admin')->delAdmin($_POST['admin_id']);
		if($row){

			model('member')->editMember(['admin_id' => $_POST['admin_id']],['admin_id' => 0]);


			output_data('操作成功');			
		}else{
			output_error('操作失败');			
		}
	}
	
	//修改状态
	
	public function adminStateOp(){		
		$data = array();
		$data['admin_id'] = $_POST['admin_id'];
		$data['admin_state'] = $_POST['admin_state'];
		$row = Model('admin')->updateAdmin($data);
		if($row){
			output_data('操作成功');			
		}else{
			output_error('操作失败');			
		}
	}
	
	
	
	public function mavin_tokenOp(){
		
		$model_admin = Model('admin');
		$admin_info = $model_admin->getAdminInfoNew(array('admin_id' =>$_POST['admin_id']));	
		
	
		$token = $this->_get_token($admin_info['admin_id'], $admin_info['admin_name'], 'pc',$admin_info);
		if($token) {		
	
			output_data(array('token'=>$token,'admin_id'=>$admin_info['admin_id'],'admin_name'=>$admin_info['admin_name']));
		
		} else {
		    output_error('登录失败');
		}
		
	}
	
	
	/**
	 * 登录生成token
	 */
	private function _get_token($admin_id, $admin_name, $client,$data = array()) {
	    $model_admin_token = Model('mb_mavin_token');
	
	    //重新登录后以前的令牌失效
	    //暂时停用
	    //$condition = array();
	    //$condition['member_id'] = $member_id;
	    //$condition['client_type'] = $client;
	    //$model_mb_user_token->delMbUserToken($condition);
	
	    //生成新的token
	    $mb_admin_token_info = array();
	    $token = md5($admin_name . strval(TIMESTAMP) . strval(rand(0,999999)));
	    $mb_admin_token_info['admin_id'] = $admin_id;
	    $mb_admin_token_info['admin_name'] = $admin_name;
	    $mb_admin_token_info['token'] = $token;
	    $mb_admin_token_info['login_time'] = TIMESTAMP;
	    $mb_admin_token_info['client_type'] = $client;
		
		$mb_admin_token_info['admin_gid'] = $data['admin_gid'];
		$mb_admin_token_info['admin_group_name'] = $data['admin_group_name'];
		$mb_admin_token_info['admin_nickname'] = $data['admin_nickname'];
		$mb_admin_token_info['admin_type'] = $data['admin_type'];
		$mb_admin_token_info['admin_quick_link'] = $data['admin_quick_link'];
		$mb_admin_token_info['admin_is_super'] =  $data['admin_is_super'];
		$mb_admin_token_info['admin_ip'] = $data['admin_ip'];
	
		
	
	    $result = $model_admin_token->addMbAdminToken($mb_admin_token_info);
	
	    if($result) {
	        return $result;
	    } else {
	        return null;
	    }
	
	}
	
	
	
}
