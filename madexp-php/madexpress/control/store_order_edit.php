<?php

/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class store_order_editControl extends mobileSellerControl
{

	public function __construct()
	{
		parent::__construct();
	}


	//		
	public function order_infoOp()
	{
		$order_id = intval($_POST['order_id']);

		if (!$order_id) {
			output_error('订单编号有误');
		}


		$model_order = Model('order');
		$condition = array();
		$condition['order_id'] = $order_id;
		$condition['store_id'] = $this->store_info['store_id'];

		$order = $model_order->getOrderInfo($condition, array('order_common', 'order_goods', 'member', 'store', 'order_goods_new'));

		if ($order['order_state'] == 0 || $order['order_state'] >= 50 ) {
			output_error('订单无法编辑，请确认订单状态');
		}


		if (empty($order)) {
			output_error('订单信息不存在');
		}

		$order_item = array();
		//从新格式化			

		$order_user = array();
		$order_user['buyer_code'] = $order['buyer_code'];
		$order_user['buyer_name'] = $order['buyer_name'];
		$order_user['buyer_phone'] = $order['buyer_phone'];
		$order_user['buyer_comment'] = $order['buyer_comment'];
		$order_user['buyer_is_new'] = $order['buyer_is_new'];
		$order_user['distributor_name'] = $order['distributor_name'];
		$order_user['distributor_mobile'] = $order['distributor_mobile'];
		$order_user['state'] = $order['order_state'];
		$order_user['evaluation_state'] = $order['evaluation_state'];

		if ($order['evaluation_state'] == 1) {
			$model = model('evaluate_store');
			$where = array(
				'store_evaluate_orderid' => $order['order_id']
			);
			$eva = $model->getEvaluateStoreInfo($where);
			$order['evaluation_text'] = $eva['store_evaluate_score_text'];
			$order['evaluation_content'] = $eva['store_evaluate_score_content'];
		}


		$order_item['order_user'] = $order_user;





		//订单商品,原始商品，不做任何修改
		$order_goods = array();
		$goods =  $order['extend_order_goods'];
		$goods_num = 0;
		foreach ($goods as $k => $v) {

			$info = model('order_goods_edit')->getGoodsInfo(['rec_id' => $v['rec_id']], 'SUM(rec_num) as usable_num');
			$v['goods_usable_num'] = $v['goods_usable_num'] - $info['usable_num'];
			$goods_list[$k] = $v;

			$goods_num += $v['goods_num'];
		}

		$order_goods['goods_list'] = $goods_list;
		$order_goods['goods_num'] = $goods_num;
		$order_item['order_goods'] = $order_goods;





		//获取当前订单的退款商品



		//退款商品	[售前]
		$order_presale_refund = array();
		$where = array();
		$where['order_id'] = $order['order_id'];
		$where['is_take_ffect'] = 0;
		$refund  = model('order_goods_refund')->getRefundList($where);

		$goods_num = 0;
		$goods_amount = 0;
		$refund_list = array();
		if (!empty($refund)) {
			foreach ($refund as $k => $v) {
				$v['goods_optional'] = unserialize($v['goods_optional']);
				$refund_list[] = $v;
				$goods_num += $v['goods_num'];
				$goods_amount += $v['goods_num'] * $v['goods_price'];
			}
		}
		$order_presale_refund['goods_num'] = 	$goods_num;
		$order_presale_refund['goods_amount'] = $goods_amount;
		$order_presale_refund['goods_list'] = $refund_list;
		$order_item['order_presale_refund'] = $order_presale_refund;



		//替换商品		
		$order_replace = array();
		$where = array();
		$where['order_id'] = $order['order_id'];
		$where['is_take_ffect'] = 2;
		$replace =  model('order_goods_replace')->getGoodsList($where); //$order['extend_order_goods_replace'];		

		$replace_ids = array();
		foreach ($replace as $v) {
			$replace_ids[] = $v['rec_id'];
			$replace_new_list[$v['rec_id']]['goods_num'] = $v['rec_num'];
		}

		$replace_ids = 	implode(',', $replace_ids);

		$goods_num = 0;
		$goods_amount = 0;
		if (!empty($replace)) {
			$replace_data = $model_order->getOrderGoodsList(array('rec_id' => array('in', $replace_ids)));
			foreach ($replace_data as $k => $v) {
				$v['goods_num'] = $replace_new_list[$v['rec_id']]['goods_num'];
				$v['replace_type'] = 0;
				$v['goods_optional'] =  $v['goods_optional'] != '' ? unserialize($v['goods_optional']) : array();
				$v['goods_size'] = $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'];
				$replace_list[] = $v;

				$child_list = $this->order_replace_goods($v['rec_id']);
				foreach ($child_list as $kk => $vv) {
					$vv['replace_type'] = 1;
					$vv['goods_size'] = $vv['goods_size'] == 'DefaultSize' ? '' : $vv['goods_size'];
					$replace_list[] = $vv;
				}

				$goods_num += $v['goods_num'];
				$goods_amount += $v['goods_num'] * $v['goods_price'];
			}
		}

		$order_replace['goods_num'] 	= 	$goods_num;
		$order_replace['goods_amount'] = 	$goods_amount;
		$order_replace['goods_list'] = $replace_list;
		$order_item['order_replace'] = $order_replace;




		//商家赠送商品
		$order_goods_give = array();
		$where = array();
		$where['order_id'] = $order['order_id'];
		$where['give_type'] = 0;
		$where['is_take_ffect'] = 0;
		$give =  model('order_goods_give')->getGiveList($where); //$order['extend_order_goods_give'];

		$give_list = array();
		$goods_num = 0;
		$goods_amount = 0;
		if (!empty($give)) {
			foreach ($give as $k => $v) {
				$v['goods_optional'] = unserialize($v['goods_optional']);
				$v['goods_size'] = $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'];
				$give_list[] = $v;
				$goods_num += $v['goods_num'];
				$goods_amount += $v['goods_num'] * $v['goods_price'];
			}
		}

		$order_goods_give['goods_num'] = 	$goods_num;
		$order_goods_give['goods_amount'] = 	$goods_amount;
		$order_goods_give['goods_list'] 	= $give_list;
		$order_item['order_give'] = $order_goods_give;





		//商家赠送的优惠券
		$order_coupon_give = array();
		$order_coupon_give['order_coupon_give_id']		 = $order['order_coupon_give_id'];
		$order_coupon_give['order_coupon_give_price']	 = $order['order_coupon_give_price'];
		$order_coupon_give['order_coupon_give_name']	 = $order['order_coupon_give_name'];
		$order_item['order_coupon_give'] = $order_coupon_give;


		//商家优惠列表
		//店铺优惠券
		$condition = array();
		$condition['voucher_t_gettype'] = 3;
		$condition['voucher_t_state'] 	= 1;
		$condition['voucher_t_end_date'] = array('gt', time());
		$condition['voucher_t_store_id'] = array('in', $this->store_info['store_id']);
		$voucher_template = Model('voucher')->getVoucherTemplateList($condition);
		$voucher = array();
		if (!empty($voucher_template)) {
			foreach ($voucher_template as $val) {
				$param = array();
				$param['coupons_id'] = $val['voucher_t_id'];
				$param['coupons_price'] = $val['voucher_t_price'];
				$param['coupons_title'] = $val['voucher_t_title'];
				//$param['voucher_t_limit'] = $val['voucher_t_limit'];
				//$param['voucher_t_end_date'] = date('Y年m月d日', $val['voucher_t_end_date']);

				$voucher[] = $param;
			}
		}

		$order_item['store_coupons'] = $voucher;





		output_data($order_item);
	}


	private function order_replace_goods($rec_id)
	{

		$list =    model('order_goods_replace')->getGoodsList(array('rec_id' => $rec_id));

		$data = array();
		foreach ($list as $v) {

			$v['goods_optional'] =  $v['goods_optional'] != '' ? unserialize($v['goods_optional']) : array();
			$data[] = $v;
		}

		return $data;
	}





	/*
	商家保存编辑
	*/
	public function save_orderOp()
	{


		$model = model('order');
		$snapshot_model = model('order_snapshot');

		$order_id = $_POST['order_id'];
		$store_id = $this->store_info['store_id'];

		$where = array();
		$where['order_id'] = $order_id;
		$where['store_id'] = $store_id;
		$order_info = $model->getOrderInfo($where);


		$snapshot_amendment = array();




		//生成订单快照
		$sphot = $snapshot_model->createSphot($order_id);

		if ($sphot) {
			$order_num = $snapshot_model->getSnaOrderCount(array('order_id' => $order_id));
		} else {
			output_error('历史单生成失败，订单修改失败');
		}


		//替换商品		
		$replace_list = model('order_goods_replace')->getGoodsList(array('is_take_ffect' => 2, 'order_id' => $order_id, 'store_id' => $store_id));
		//判断是否有商品替换		
		if (!empty($replace_list)) {


			$row = $this->addOrderGoods($replace_list, $order_id);

			if (!$row) {
				output_error('替换商品写入失败');
			}

			//更新原商品数量
			$edit_num_list = model('order_goods_edit')->getGoodsList(['order_id' => $order_id, 'type' => 'replace']);
			if (!empty($edit_num_list)) {
				foreach ($edit_num_list as $v) {
					model('order')->editOrderGoods(['goods_usable_num' => ['exp', 'goods_usable_num -' . $v['rec_num']]], ['rec_id' => $v['rec_id']]);
				}
			}


			$where = array();
			$where['is_take_ffect'] = 2;
			$where['order_id'] = $order_id;
			$where['store_id'] = $store_id;

			$update = array();
			$update['is_take_ffect'] = 1;
			$row = model('order_goods_replace')->editGoods($where, $update);
			if ($row) {

				$snapshot_amendment[] = '替换商品';
			}
		}


		//	print_r($where);


		//售前退款
		$refund_list = model('order_refund')->getRefundList(array('is_edit' => 1,  'order_id' => $order_id, 'store_id' => $store_id));
		//判断是否有退款		
		if (!empty($refund_list)) {
			$refund_ids  = [];
			$refund_pay_amount 		= 0;
			$refund_pd_amount 		= 0;
			$refund_coupon_price 	= 0;
			foreach ($refund_list as $v) {

				$refund_ids[] = $v['refund_id'];
				$refund_pay_amount += $v['refund_pay_amount'];
				$refund_pd_amount += $v['refund_wallet_amount'];
				$refund_coupon_price += $v['refund_coupon_amount'];

				model('order_refund')->editRefund(['is_edit' => 1, 'order_id' => $order_id, 'store_id' => $store_id], ['is_edit' => 2]);
				model('order_goods_refund')->editRefund(['refund_id' => $v['refund_id']], ['is_take_ffect' => 1]);
				if ($v['is_replace'] == 0) {
					if ($v['refund_stage'] == 0) {
						$snapshot_amendment[] = '一阶段退款';
					} else {
						$snapshot_amendment[] = '二阶段退款';
					}
				}
			}
			//更新原商品数量
			$edit_num_list = model('order_goods_edit')->getGoodsList(['order_id' => $order_id, 'type' => 'refund']);
			if (!empty($edit_num_list)) {
				foreach ($edit_num_list as $v) {
					model('order')->editOrderGoods(['goods_usable_num' => ['exp', 'goods_usable_num -' . $v['rec_num']]], ['rec_id' => $v['rec_id']]);
				}
			}




			//	$snapshot_amendment[] = '二阶段退款';



			$data_record = array();
			$data_record['order_id'] = $order_info['store_id'];
			$data_record['merchant_refund_type'] = 'Partial';
			$data_record['merchant_refund_time'] = time();
			$data_record['refund_id'] =  implode(',',$refund_ids);
			model('order_record')->addRecord('merchant_refund', $data_record);


			//更订单退款数据
			$update = array();
			$update['refund_pay_amount'] 	= array('exp', 'refund_pay_amount + ' . $refund_pay_amount);
			$update['refund_pd_amount'] 	= array('exp', 'refund_pd_amount + ' . $refund_pd_amount);
			$update['refund_coupon_price'] 	= array('exp', 'refund_coupon_price + ' . $refund_coupon_price);
			model('order')->editOrder($update, ['order_id' => $order_id]);
		}





		$give_list = model('order_goods_give')->getGiveList(array('is_take_ffect' => 0, 'order_id' => $order_id, 'store_id' => $store_id, 'give_type' => 0));
		//判断是否有赠送商品
		if (!empty($give_list)) {

			$row = $this->addOrderGoods($give_list, $order_id);

			$where = array();
			$where['is_take_ffect'] = 0;
			$where['order_id'] = $order_id;
			$where['store_id'] = $store_id;

			$update = array();
			$update['is_take_ffect'] = 1;

			$row = model('order_goods_give')->editGive($where, $update);
			if ($row) {
				$snapshot_amendment[] = '赠送商品';
			}
		}



		//判断是否有赠送店铺优惠券		
		if ($_POST['coupon_id'] > 0) {

			$t_id = intval($_POST['coupon_id']);
			if ($t_id <= 0) {
				output_error('店铺优惠券信息错误');
			}
			$model_voucher = Model('voucher');
			//验证是否可领取代金券
			$data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
			//  print_r($data);
			$data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
			if ($data['state'] == false) {
				output_error($data['msg']);
			}
			$voucher = $data['voucher'];
			//	print_r($voucher);
			//更新订单优惠券
			//优惠券写入到优惠券表
			$coupon = array();
			$coupon['order_id'] =  $order_id;
			$coupon['store_id'] = $this->store_info['store_id'];
			$coupon['coupon_code'] = $voucher['voucher_code'];
			$coupon['add_time'] = time();
			$coupon['coupon_price'] = $voucher['voucher_price'];
			$coupon['coupon_name'] = $voucher['voucher_title'];
			$coupon['coupon_id'] = $voucher['voucher_t_id'];
			$coupon['version'] = $order_num + 1;
			//	print_r($coupon);
			$row = model('order_coupon')->addCoupon($coupon);
			if ($row) {
				$snapshot_amendment[] = '赠送优惠券';
			}
		}



		//生成退款记录		
		$where = array();
		$where['order_id'] = $_POST['order_id'];
		$where['store_id'] = $this->store_info['store_id'];
		$update = array();
		$update['order_update_time'] = time();
		$update['is_old_num'] = $order_num + 1;
		$update['store_note'] = $_POST['store_note'];
		$update['snapshot_amendment'] = implode(',', $snapshot_amendment); //当前新订单做了哪些处理
		if ($_POST['coupon_id']) {
			$update['store_coupon_give_name'] 	= $voucher['voucher_title'];
			$update['store_coupon_give_id'] 	= $voucher['voucher_code'];
			$update['store_coupon_give_price'] 	= $voucher['voucher_price'];
		}
		$row = model('order')->editOrder($update, $where);
		if ($row) {

			//从新计算商品价格。
			$goods_list = model('order')->getOrderGoodsList(['order_id' =>  $order_id]);
			$goods_amount = 0;
			foreach ($goods_list as $v) {
				if ($v['goods_usable_num'] > 0) {
					$goods_amount += $v['goods_price'] * $v['goods_usable_num'];
					model('order')->editOrderGoods(['goods_num' => $v['goods_usable_num']], ['rec_id' => $v['rec_id']]);
				}
			}

			//重新计算抽成
			//查询当前店铺的平台抽成
			$contract = model('store_contract')->getContractInfo(array('store_id' =>  $this->store_info['store_id'], 'is_default' => 1));
			$contract_rate = ($contract['contract_rate'] / 100) * $goods_amount  + $order_info['pay_commission_amount']; //平台抽成			

			//修改退款信息
			$order_amount = $goods_amount + $order_info['delivery_fee'] + $order_info['foodbox_amount'] - $order_info['coupon_price'];

			//订单价值
			$pay_order_amount = $order_amount + $order_info['pay_commission_amount'];

			//

			model('order')->editOrder(['goods_amount' => $goods_amount, 'commission_amount' => $contract_rate, 'order_amount' => $order_amount, 'pay_order_amount' => $pay_order_amount], ['order_id' => $order_id]);

			//删除编辑的信息

			model('order_goods_edit')->delGoods(['order_id' => $order_id]);


			output_data('编辑成功');
		} else {

			output_error('编辑失败');
		}
	}


	private function addOrderGoods($list, $order_id)
	{

		$order_info = model('order')->getOrderInfoN(['order_id' => $order_id]);
		foreach ($list as $val) {
			$goods_data[] = array(
				'order_id' => $order_id,
				'goods_id' => $val['goods_id'],
				'goods_name' => $val['goods_name'],
				'goods_price' => $val['goods_price'],
				'goods_num' => $val['goods_num'],
				'goods_image' => $val['goods_image'],
				'goods_pay_price' => $val['goods_price'],
				'store_id' => $order_info['store_id'],
				'buyer_id' => $order_info['buyer_id'],
				'gc_id' => $val['goods_class_id'],
				'goods_spec' => $val['goods_spec'],
				'goods_size' => $val['goods_size'],
				'goods_optional' => $val['goods_optional'],
				'goods_comment' => $val['goods_comment'],
				'goods_lang' => $val['goods_lang'],
				'foodbox_price' => $val['foodbox_price'],
				'foodbox_id' => $val['foodbox_id'],
				'foodbox_name' => $val['foodbox_name'],
				'foodbox_num' => $val['foodbox_num'],
				'goods_usable_num' => $val['goods_num'],
				'goods_specs_id' => $val['goods_specs_id'],
				'goods_size_id' => $val['goods_size_id'],
				'goods_code' => $val['goods_code'],
				'goods_optional_ids' => $val['goods_optional_ids'],
				'set_meal' => $val['set_meal'],
				'set_meal_spec' => $val['set_meal_spec'],
				'is_edit' => 1 //表示商品是后编辑的。
			);
		}



		$row = model('order')->addOrderGoods($goods_data);

		return $row;
	}

	private function checkGoods($goods, $state)
	{

		$where = array();
		$where['goods_id']          = $goods['goods_id'];
		$where['state']             = $state;
		$where['goods_spec']        = $goods['goods_spec'];
		$where['goods_size']        = $goods['goods_size'];
		$where['goods_optional']    = $goods['goods_optional'];

		return model('order')->getOrderGoodsInfo($where);
	}



	/*
	//编辑订单商品信息
	public function goodsUpdateOp()
	{

		$type = $_POST['type'];
		$rec_id = $_POST['rec_id'];
		$order_id = $_POST['order_id'];

		$where = array();
		$where['rec_id'] = $rec_id;
		$where['order_id'] = $order_id;

		$rec_info = model('order')->getOrderGoodsInfo($where);
		$goods_info = model('goods')->getGoodsInfo(array('goods_id' => $rec_info['goods_id']));

		$model_refund = model('order_goods_refund');
		if ($rec_info) {

			$update  = array();
			//增加数量，撤回退款商品
			if ($type == 'plus') {

				$plus = $this->delGoodsRefund($rec_info, $order_id);

				if (!$plus) {

					output_error('已无可撤回商品');

				} else {

					$this->editLogGoods($rec_info, $type);

					output_data('操作成功');
				}
			}

			//减少数量，增加退款商品
			if ($type == 'reduce') {


				$reduce = $this->addGoodsRefund($order_id, $rec_info);

				if (!$reduce) {

					output_error('可操作数量超限');

				} else {

					$this->editLogGoods($rec_info, $type);
					output_data('操作成功');
				}
			}
		} else {
			output_error('商品不存在或参数错误');
		}
	}*/

	//退款
	public function reduce_goodsOp()
	{

		//	$type = $_POST['type'];
		$rec_id = $_POST['rec_id'];
		$order_id = $_POST['order_id'];

		$where = array();
		$where['rec_id'] = $rec_id;
		$where['order_id'] = $order_id;

		$rec_info = model('order')->getOrderGoodsInfo($where);

		$reduce = $this->addGoodsRefund($order_id, $rec_info);

		if (!$reduce) {

			output_error('可操作数量超限');
		} else {

			$this->editLogGoods($rec_info, 'reduce');
			output_data('操作成功');
		}
	}

	//撤销退款
	public function plus_goodsOp()
	{

		//	$type = $_POST['type'];
		$rec_id = $_POST['rec_id'];
		$order_id = $_POST['order_id'];

		$where = array();
		$where['rec_id'] = $rec_id;
		$where['order_id'] = $order_id;

		$rec_info = model('order')->getOrderGoodsInfo($where);

		$plus = $this->delGoodsRefund($rec_info, $order_id);

		if (!$plus) {

			output_error('已无可撤回商品');
		} else {

			$this->editLogGoods($rec_info, 'plus');

			output_data('操作成功');
		}
	}





	//生成退款
	private function addGoodsRefund($order_id, $goods_info)
	{




		//获取历史单数量
		$version = model('order_snapshot')->getSnaOrderCount(['order_id' => $order_id]);
		$version_num = $version + 2;

		$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1, 'is_replace' => 0]);

		$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id, 'store_id' => $this->store_info['store_id']));


		if (empty($refund_info)) { //如果当前没有正在编辑的退款记录，则创建一个新的退款记录。


			$refund_stage = 0;
			if ($order_info['order_state'] == 40 || $order_info['order_state'] == 50) {
				$refund_stage = 1;
			}

			if ($order_info['order_state'] == 60) {
				$refund_stage = 2;
			}


			$refund_type = 1;
			$data = array();
			$data['order_id']    = $order_info['order_id'];
			$data['order_sn']    = $order_info['order_sn'];
			$data['store_id']    = $order_info['store_id'];
			$data['store_name']  = $order_info['store_name'];
			$data['member_id']   = $order_info['buyer_id'];
			$data['role']       =  0;
			$data['refund_comment'] = $_POST['refund_comment'];
			$data['add_time']   = time();
			$data['refund_type'] = $refund_type;
			$data['refund_amount'] = 0;
			$data['refund_stage'] =  $refund_stage;
			$data['refund_log'] = ''; // serialize($order_refund);
			$data['is_delivery'] = 1;
			$data['is_commission'] = 1;    //商家操作是不退抽成的。
			$data['is_edit'] = 1; //证明这是订单编辑的
			$data['version'] = $version_num;
			$refund_id = model('order_refund')->addRefund($data);
			if ($refund_id) {


				$data = array();
				$data['refund_id'] = $refund_id;
				$data['order_id'] = $goods_info['order_id'];
				$data['goods_id'] = $goods_info['goods_id'];
				$data['goods_name'] =  $goods_info['goods_name'];
				$data['goods_price'] = $goods_info['goods_price'];
				$data['goods_num'] = 1;
				$data['goods_image'] = $goods_info['goods_image'];
				$data['goods_pay_price'] = $goods_info['goods_pay_price'];
				$data['store_id'] = $goods_info['store_id'];
				$data['buyer_id'] = $goods_info['buyer_id'];
				$data['gc_id'] = $goods_info['gc_id'];
				$data['goods_size_id'] = $goods_info['goods_size_id'];
				$data['goods_specs_id'] = $goods_info['goods_specs_id'];
				$data['goods_spec'] = $goods_info['goods_spec'];
				$data['goods_size'] = $goods_info['goods_size'];
				$data['original_goods_id'] = $goods_info['original_goods_id'];
				$data['original_goods_name'] = $goods_info['original_goods_name'];
				$data['original_goods_num'] = $goods_info['original_goods_num'];
				$data['original_goods_spec'] = $goods_info['original_goods_spec'];
				$data['original_goods_size'] = $goods_info['original_goods_size'];
				$data['original_goods_price'] = $goods_info['original_goods_price'];
				$data['goods_lang'] = $goods_info['goods_lang'];
				$data['goods_optional_ids'] = $goods_info['goods_optional_ids'];
				$data['goods_optional'] = $goods_info['goods_optional'];
				$data['original_goods_optional'] = $goods_info['original_goods_optional'];
				$data['foodbox_price'] = $goods_info['foodbox_price'];
				$data['foodbox_id'] = $goods_info['foodbox_id'];
				$data['foodbox_name'] = $goods_info['foodbox_name'];
				$data['foodbox_num'] = $goods_info['foodbox_num'];
				$data['rec_id'] = $goods_info['rec_id'];
				$data['goods_code'] = $goods_info['goods_code'];
				$data['version'] = $version_num;
				$row = model('order_goods_refund')->addRefund($data);


				//更新金额 
				$this->updateAmount($order_id);

				/*
				$update = array();
				$update['refund_amount'] = $goods_info['goods_price'];
				if($stripe_amount >= $goods_info['goods_price']){ //stripe 有钱，直接退
				
					$update['refund_pay_amount'] = $goods_info['goods_price'];

				}elseif($stripe_amount < $goods_info['goods_price'] && $stripe_amount > 0 ){ //stripe 不够，退一部分，剩余的余额退

					$update['refund_pay_amount'] = $stripe_amount;
					$refund_wallet_amount= $stripe_amount - $goods_info['goods_price'];

					if($wallet_amount >= $refund_wallet_amount){
						$update['refund_wallet_amount'] = $refund_wallet_amount;
					}					

				}
				//更新退款总金额。
				model('order_refund')->editRefund(['refund_id' => $refund_id],$update);
				*/


				return true;
			}
		} else { //当前已有退款记录。


			$check = $this->checkRefund($goods_info, $refund_info['refund_id']);



			if (!empty($check)) {


				$refund_goods_edit = model('order_goods_edit')->getGoodsInfo(['rec_id' => $goods_info['rec_id']], 'SUM(rec_num) as num');

				if ($refund_goods_edit['num'] == $goods_info['goods_usable_num']) {
					return false;
				}

				$where = array();
				$where['rec_id'] = $goods_info['rec_id'];
				$where['order_id'] = $goods_info['order_id'];
				$where['refund_id'] = $refund_info['refund_id'];
				$update = array();
				$update['goods_num'] = array('exp', 'goods_num + 1');
				model('order_goods_refund')->editRefund($where, $update);

				/*
				$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
				$refund_amount = 0;
				foreach($refund_goods_list as $v){

					$refund_amount +=  $v['goods_price'] * $v['goods_num'];

				}

				
				$update = array();
				$update['refund_amount'] = $refund_amount;
				//更新退款总金额。
				model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);
				*/
				//更新金额 
				$this->updateAmount($order_id);



				return true;
			} else {

				$data = array();
				$data['refund_id'] = $refund_info['refund_id'];
				$data['order_id'] = $goods_info['order_id'];
				$data['goods_id'] = $goods_info['goods_id'];
				$data['goods_name'] =  $goods_info['goods_name'];
				$data['goods_price'] = $goods_info['goods_price'];
				$data['goods_num'] = 1;
				$data['goods_image'] = $goods_info['goods_image'];
				$data['goods_pay_price'] = $goods_info['goods_pay_price'];
				$data['store_id'] = $goods_info['store_id'];
				$data['buyer_id'] = $goods_info['buyer_id'];
				$data['gc_id'] = $goods_info['gc_id'];
				$data['goods_size_id'] = $goods_info['goods_size_id'];
				$data['goods_specs_id'] = $goods_info['goods_specs_id'];
				$data['goods_spec'] = $goods_info['goods_spec'];
				$data['goods_size'] = $goods_info['goods_size'];
				$data['original_goods_id'] = $goods_info['original_goods_id'];
				$data['original_goods_name'] = $goods_info['original_goods_name'];
				$data['original_goods_num'] = $goods_info['original_goods_num'];
				$data['original_goods_spec'] = $goods_info['original_goods_spec'];
				$data['original_goods_size'] = $goods_info['original_goods_size'];
				$data['original_goods_price'] = $goods_info['original_goods_price'];
				$data['goods_lang'] = $goods_info['goods_lang'];
				$data['goods_optional_ids'] = $goods_info['goods_optional_ids'];
				$data['goods_optional'] = $goods_info['goods_optional'];
				$data['original_goods_optional'] = $goods_info['original_goods_optional'];
				$data['foodbox_price'] = $goods_info['foodbox_price'];
				$data['foodbox_id'] = $goods_info['foodbox_id'];
				$data['foodbox_name'] = $goods_info['foodbox_name'];
				$data['foodbox_num'] = $goods_info['foodbox_num'];
				$data['rec_id'] = $goods_info['rec_id'];
				$data['goods_code'] = $goods_info['goods_code'];
				$data['version'] = $version_num;
				$row = model('order_goods_refund')->addRefund($data);


				/*	$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
				$refund_amount = 0;
				foreach($refund_goods_list as $v){
					$refund_amount +=  $v['goods_price'] * $v['goods_num'];
				}

				$update = array();
				$update['refund_amount'] = $refund_amount;
				//更新退款总金额。
				model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']],$update);
			*/

				//更新金额 
				$this->updateAmount($order_id);



				return true;
			}
		}
	}


	//更新临时数量

	private function editLogGoods($goods, $type)
	{

		//print_r($type);
		$where = array();
		$where['rec_id'] = $goods['rec_id'];
		$where['type'] = 'refund';
		$where['order_id'] = $goods['order_id'];
		//print_r($where);
		$info = model('order_goods_edit')->getGoodsInfo($where);
		if (!empty($info)) {

			if ($type == 'plus') {


				model('order_goods_edit')->editGoods(['id' => $info['id']], array('rec_num' => array('exp', 'rec_num - 1')));
			} else {

				model('order_goods_edit')->editGoods(['id' => $info['id']], array('rec_num' => array('exp', 'rec_num + 1')));
			}
		} else {

			$data = array();
			$data['rec_id'] 	= $goods['rec_id'];
			$data['rec_num'] 	= 1;
			$data['type'] 		= 'refund';
			$data['order_id'] 	= $goods['order_id'];

			//print_r($data);

			model('order_goods_edit')->addGoods($data);
		}
	}


	/*
	//更新主订单商品的可用商品数数量
	private function editGoodsUsable($data, $type)
	{

		//更新主订单			
		$where = array(
			'rec_id' => $data['rec_id'],
			'order_id' => $data['order_id']
		);

		$info = model('order')->getOrderGoodsInfo($where);

		$data = array();
		if ($type == 'plus') {
			$data['goods_usable_num'] =  $info['goods_usable_num'] + 1;
		} else {
			$data['goods_usable_num'] =  $info['goods_usable_num'] - 1;
		}

		model('order')->editOrderGoods($data, $where);
	}
	*/


	//回退商品
	private function delGoodsRefund($data, $order_id)
	{


		$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1]);

		$model = model('order_goods_refund');
		$check = $this->checkRefund($data, $refund_info['refund_id']);
		//print_r($check);
		//验证商品
		if (!empty($check)) {

			if ($check['goods_num'] > 1) {

				$where = array();
				$where['rec_id']        = $data['rec_id'];
				$where['order_id']      = $data['order_id'];
				$where['refund_id'] = $refund_info['refund_id'];

				$update = array();
				$update['goods_num']    = array('exp', 'goods_num - 1');
				$model->editRefund($where, $update);

				model('order_goods_edit')->delGoods(['rec_id' => $data['rec_id'], 'order_id' => $data['order_id']], array('rec_num' => array('exp', 'rec_num - 1')));
			} else {


				$where = array();
				$where['order_id']  = $data['order_id'];
				$where['rec_id']    = $data['rec_id'];
				$where['refund_id'] = $refund_info['refund_id'];
				$where['is_replace'] = 0;
				$row = $model->delRefund($where);
				//检查是否还有可退的商品
				$count = $model->getRefundCount(['order_id' => $data['order_id'], 'refund_id' => $refund_info['refund_id']]);
				if (intval($count) == 0) {



					model('order_refund')->delRefund(['order_id' => $order_id, 'is_edit' => 1]);
				}
			}

			//更新金额 
			$this->updateAmount($order_id);

			return true;
		} else {

			return false;
		}
	}

	//更新金额
	private function updateAmount($order_id)
	{

		$order_info = model('order')->getOrderInfoN(array('order_id' => $order_id, 'store_id' => $this->store_info['store_id']));

		//获取stripe支付的金额
		$stripe_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
		//获取钱包支付的金额
		$wallet_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
		//获取优惠券抵扣的金额
		$voucher_amount = $order_info['coupon_price'] - $order_info['refund_coupon_price'];

		$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1]);

		$refund_goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);
		$refund_amount = 0;
		foreach ($refund_goods_list as $v) {
			$refund_amount +=  $v['goods_price'] * $v['goods_num'];
		}

		$update = array();
		$update['refund_amount'] = $refund_amount;
		$update['refund_pay_amount'] = 0;
		$update['refund_wallet_amount'] = 0;
		$update['refund_coupon_amount'] = 0;

		if ($stripe_amount > 0) { //stripe youqian

			if ($stripe_amount >= $refund_amount) { //stripe 有钱，直接退	

				$update['refund_pay_amount'] = $refund_amount;
			} elseif ($stripe_amount < $refund_amount && $stripe_amount > 0) { //stripe 不够，退一部分，剩余的余额退		

				$update['refund_pay_amount'] = $stripe_amount;

				$refund_wallet_amount = $refund_amount - $stripe_amount;

				if ($wallet_amount >= $refund_wallet_amount) {

					$update['refund_wallet_amount'] = $refund_wallet_amount;
				} elseif ($wallet_amount < $refund_wallet_amount &&  $wallet_amount > 0) {

					$update['refund_wallet_amount'] = $wallet_amount;

					$refund_coupon_amount = $refund_wallet_amount - $wallet_amount;

					$update['refund_coupon_amount'] = $refund_coupon_amount;
				} else {
					$update['refund_coupon_amount'] = $refund_wallet_amount;
				}
			}
		} elseif ($wallet_amount > 0) { //stripe 没钱，钱包 有钱。

			if ($wallet_amount >= $refund_amount) { //余额大于退款金额。直接退。

				$update['refund_wallet_amount'] = $refund_amount;
			} elseif ($wallet_amount < $refund_amount &&  $wallet_amount > 0) { //有钱，但钱不够。

				$update['refund_wallet_amount'] = $wallet_amount;	//有多少退多少。

				$refund_coupon_amount = $refund_amount - $wallet_amount;

				$update['refund_coupon_amount'] = $refund_coupon_amount;
			} else {

				$update['refund_coupon_amount'] = $wallet_amount;
			}
		} elseif ($voucher_amount > 0) { //优惠券支付的。

			if ($voucher_amount >= $refund_amount) {

				$update['refund_coupon_amount'] = $refund_amount;
			}
		}

		model('order_refund')->editRefund(['refund_id' => $refund_info['refund_id']], $update);
	}



	//获取退款的商品
	private function checkRefund($data, $refund_id)
	{

		$model = model('order_goods_refund');

		$where = array();
		$where['order_id'] = $data['order_id'];
		$where['rec_id'] = $data['rec_id'];
		$where['refund_id'] = $refund_id;
		$where['is_replace'] = 0;
		//print_r($where);

		//	print_r($where);
		$info = $model->getRefundInfo($where);

		//print_r($info);
		return $info;
	}


	//回退，删除整个

	public function del_refund_goodsOp()
	{

		$model = model('order_goods_refund');
		$model_order = model('order');


		$order_id = $_POST['order_id'];
		$refund_rec_id = $_POST['rec_id'];

		$refund_info = model('order_refund')->getRefundInfo(['order_id' => $order_id, 'is_edit' => 1]);

		if (intval($_POST['rec_id']) > 0) {


			$info = model('order_goods_refund')->getRefundInfo(['refund_id' => $refund_info['refund_id'], 'refund_rec_id' => $refund_rec_id, 'order_id' => $order_id]);

			if ($info['replace_rec_id'] > 0) {

				model('order_goods_replace')->delGoods(['rec_id' => $info['rec_id']]);
				model('order_goods_edit')->delGoods(['rec_id' => $info['rec_id']]);
			}

			model('order_goods_edit')->delGoods(['rec_id' => $info['rec_id'], 'type' => 'refund']);

			//	$model_order->editOrderGoods(array('goods_usable_num' => array('exp', 'goods_usable_num +' . $info['goods_num'])), array('rec_id' => $info['rec_id'], 'order_id' =>  $order_id));

			$result = model('order_goods_refund')->delRefund(['refund_rec_id' => $refund_rec_id, 'refund_id' => $refund_info['refund_id']]);

			$count = $model->getRefundCount(['order_id' => $order_id, 'refund_id' => $refund_info['refund_id']]);

			if ($count == 0) {

				model('order_refund')->delRefund(['order_id' => $order_id, 'is_edit' => 1]);
			}
		} else {


			//更新主商品数量
			$goods_list = model('order_goods_refund')->getRefundList(['refund_id' => $refund_info['refund_id']]);

			foreach ($goods_list as $v) {

				//	$model_order->editOrderGoods(array('goods_usable_num' => array('exp', 'goods_usable_num +' . $v['goods_num'])), array('rec_id' => $v['rec_id']));

				$result = $model->delRefund(['refund_rec_id' => $v['refund_rec_id']]);
			}

			model('order_refund')->delRefund(['order_id' => $order_id, 'is_edit' => 1]);
		}


		if ($result) {


			output_data('操作成功');
		} else {
			output_error('操作失败');
		}
	}



	//编辑订单备注

	public function update_store_commentOp()
	{


		$where = array(
			'order_id' => $_POST['order_id'],
			'store_id' => $this->store_info['store_id']
		);

		$data = array(
			'store_comment' => $_POST['store_comment']
		);

		$row  = model('order')->editOrder($data, $where);

		if ($row) {

			output_data('ok');
		} else {

			output_error('保存失败');
		}
	}



	//取消编辑商品
	public function cancel_order_editOp()
	{

		$where = array(
			'order_id' => $_POST['order_id'],
			'store_id' => $this->store_info['store_id'],
			'is_take_ffect' => 0
		);

		//删除曾平
		$row = model('order_goods_give')->delGive($where);


		$refund_list = model('order_refund')->getRefundList(['order_id' => $_POST['order_id'], 'store_id' => $this->store_info['store_id'], 'is_edit' => 1]);
		foreach ($refund_list as $v) {
			//恢复商品
			$refund_goods = model('order_goods_refund')->getRefundList(['refund_id' => $v['refund_id']]);
			foreach ($refund_goods as $goods) {
				$update = array();
				$update['goods_usable_num'] = array('exp', 'goods_usable_num + ' . $goods['goods_num']);
				model('order')->editOrderGoods($update, ['rec_id' => $goods['rec_id']]);
				model('order_goods_refund')->delRefund(['refund_rec_id' => $goods['refund_rec_id']]); //删除商品

			}
			//删除商品
			//model('order_goods_refund')->delRefund(['refund_id' => $v['refund_id']]);
			model('order_refund')->delRefund(['refund_id' => $v['refund_id']]);
		}
		//删除替换		
		$row = model('order_goods_replace')->delGoods($where);
		output_data('取消成功');
	}
}
