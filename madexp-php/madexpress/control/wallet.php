<?php
/**
 * 钱包操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class walletControl extends mobileAdminControl {
  //每次导出订单数量
    const EXPORT_SIZE = 1000;
    private $gettype_arr;
    private $templatestate_arr;
    private $redpacket_state_arr;
    private $member_grade_arr;
    
    public function __construct(){
        parent::__construct();
		$model_redpacket = Model('redpacket');
		$this->gettype_arr = $model_redpacket->getGettypeArr();
		$this->templatestate_arr = $model_redpacket->getTemplateState();
		$this->redpacket_state_arr = $model_redpacket->getRedpacketState();
		$this->member_grade_arr = Model('member')->getMemberGradeArr();
    }

	public function wallet_statOp(){
		 
		 		 
		 
		 
		$info = array();
		$info['price_used'] = 1;
		$info['price_use_no'] = 2;		
		
		$coupon_list = model('redpacket')->getRptTempNew(array('rpacket_t_state'=>1));
		$total_coupon_price = 0;
		foreach($coupon_list as $v){			
			$total_coupon_price  += $v['rpacket_t_price'] * $v['rpacket_t_total'];			
		}				
		$info['coupon_price'] = $total_coupon_price; //优惠券
		$act_one = model('activity')->getActivityInfo(['activity_type' => 1],'SUM(activity_amount) as amount');
		$info['coupon_price_online'] = $act_one['amount'] > 0 ? $act_one['amount']: 0 ; //线上活动
		$act_two = model('activity')->getActivityInfo(['activity_type' => 2],'SUM(activity_amount) as amount');
		$info['coupon_price_down'] = $act_two['amount'] > 0 ? $act_two['amount'] : 0; //线下活动
		$act_three = model('activity')->getActivityInfo(['activity_type' => 3],'SUM(activity_amount) as amount');
		$info['coupon_price_fuli'] = $act_three['amount'] > 0 ? $act_three['amount'] : 0 ; //会员福利		
		output_data($info);
		
	}
	
	//使用列表
	public function coupon_order_listOp(){		
		
		
		$model_redpacket = Model('redpacket');		
		$where = array();
		$where['rpacket_state'] = 2;		
		$data = $model_redpacket->getRedpacketList($where, '*', 0, $this->page, 'rpacket_id desc');
		
		
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list = array();
		foreach ($data as $val) {
		    
			$i = $val;
			
			$order = $this->getOrderStore($val['rpacket_order_id']);
			
		    $i['rpacket_code'] = $val['rpacket_code'];		
		    $i['rpacket_owner_name'] = $val['rpacket_owner_name']?$val['rpacket_owner_name']:'未领取';
		    $i['rpacket_active_datetext'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_active_date']):'';
			$i['rpacket_usage_time'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_usage_time']):'';
			
			$i['rpacket_store_name'] = $order['store_name'];
			
			
		    $list[] = $i;
		}
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
	}


	private function getOrderStore($sn){
		
		$where= array(
			'order_sn' => $sn
		);		
		$row = model('order')->getOrderInfoN($where);
		
		return $row;	
		
		
	}
	 
	
	 
	 
	
	//优惠券列表
	
	public function coupon_listOp(){
		$where = array();
		if ($_POST['advanced']) {
		    if (strlen($q = trim($_POST['rpt_title']))) {
		        $where['rpacket_t_title'] = array('like', '%' . $q . '%');
		    }
		    if (($q = (int) $_POST['rpt_POSTtype']) > 0) {
		        $where['rpacket_t_POSTtype'] = $q;
		    }
		    if (($q = (int) $_POST['rpt_state']) > 0) {
		        $where['rpacket_t_state'] = $q;
		    }
		    if (strlen($q = trim($_POST['rpt_recommend']))) {
		        $where['rpacket_t_recommend'] = (int) $q;
		    }
		
		    if (trim($_POST['sdate']) && trim($_POST['edate'])) {
		        $sdate = strtotime($_POST['sdate']);
		        $edate = strtotime($_POST['edate']);
		        $where['rpacket_t_updatetime'] = array('between', "$sdate,$edate");
		    } elseif (trim($_POST['sdate'])) {
		        $sdate = strtotime($_POST['sdate']);
		        $where['rpacket_t_updatetime'] = array('egt', $sdate);
		    } elseif (trim($_POST['edate'])) {
		        $edate = strtotime($_POST['edate']);
		        $where['rpacket_t_updatetime'] = array('elt', $edate);
		    }
		
		    $pdates = array();
		    if (strlen($q = trim((string) $_POST['pdate1'])) && ($q = strtotime($q . ' 00:00:00'))) {
		        $pdates[] = "rpacket_t_end_date >= {$q}";
		    }
		    if (strlen($q = trim((string) $_POST['pdate2'])) && ($q = strtotime($q . ' 00:00:00'))) {
		        $pdates[] = "rpacket_t_start_date <= {$q}";
		    }
		    if ($pdates) {
		        $where['pdates'] = array('exp',implode(' and ', $pdates));
		    }
		} else {
		    if (strlen($q = trim($_POST['query']))) {
		        switch ($_POST['qtype']) {
		            case 'rpt_title':
		                $where['rpacket_t_title'] = array('like', "%$q%");
		                break;
		        }
		    }
		}
		
		switch ($_POST['sortname']) {
		    case 'rpacket_t_price':
		    case 'rpacket_t_limit':
		        $sort = $_POST['sortname'];
		        break;
		    case 'rpacket_t_mgradelimittext':
		        $sort = 'rpacket_t_mgradelimit';
		        break;
		    case 'rpacket_t_updatetimetext':
		        $sort = 'rpacket_t_updatetime';
		        break;
		    case 'rpacket_t_start_datetext':
		        $sort = 'rpacket_t_start_date';
		        break;
		    case 'rpacket_t_end_datetext':
		        $sort = 'rpacket_t_end_date';
		        break;
		    case 'rpacket_t_statetext':
		        $sort = 'rpacket_t_state';
		        break;
		    case 'rpacket_t_recommend':
		        $sort = 'rpacket_t_recommend';
		        break;
		    default:
		        $sort = 'rpacket_t_id';
		        break;
		}
		if ($_POST['sortorder'] != 'asc') {
		    $sort .= ' desc';
		}
		
		$model_redpacket = Model('redpacket');
		$data = $model_redpacket->getRptTemplateList($where, '*', 0, $this->page, $sort);
		
	
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list  = array();
		foreach ($data as $val) {
		  
		    $i = array();
			$i['coupon_id'] = $val['rpacket_t_id'];
		    $i['coupon_title'] = $val['rpacket_t_title'];
		    $i['coupon_price'] = $val['rpacket_t_price'];
		    $i['coupon_limit'] = $val['rpacket_t_limit'];
			$i['coupon_total'] = $val['rpacket_t_total'];
			$i['coupon_amount'] = $val['rpacket_t_total'] * $val['rpacket_t_price'];
				$i['coupon_giveout'] = $val['rpacket_t_giveout'];
			
			
			
		    $i['coupon_mgradelimittext'] = $val['rpacket_t_mgradelimittext'];
		    $i['coupon_updatetimetext'] = date('d/m/Y', $val['rpacket_t_updatetime']);
		    $i['coupon_start_datetext'] = date('d/m/Y', $val['rpacket_t_start_date']);
		    $i['coupon_end_datetext'] = date('d/m/Y', $val['rpacket_t_end_date']);
		    $i['coupon_type_text'] = $val['rpacket_t_gettype_text'];
		    $i['coupon_statetext'] = $val['rpacket_t_state_text'];
		  
		
		    $list[] = $i;
		}
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
		
		
	}
	
	//优惠券详情
	public function coupon_infoOp(){
		
		$model_redpacket = Model('redpacket');		
		$where = array(
			'rpacket_t_id' => $_POST['coupon_id']
		);		
		$info = $model_redpacket->getRptTemplateInfo($where);
		if($info){
			$info['coupon_start_datetext'] = date('Y-m-d', $info['rpacket_t_start_date']);
			$info['coupon_end_datetext'] = date('Y-m-d', $info['rpacket_t_end_date']);
			$info['rpacket_t_nogive'] = $info['rpacket_t_total'] - $info['rpacket_t_giveout'];
			$info['rpacket_t_noused'] = $info['rpacket_t_total'] - $info['rpacket_t_used'];
		}
		
		output_data(array('info' => $info));
	}
	
	
	
	public function coupon_info_listOp(){		
		$model_redpacket = Model('redpacket');
		$where = array();
		$where['rpacket_t_id'] = $_POST['coupon_id'];
		if($_POST['keyword']){
			$where['rpacket_owner_name'] = $_POST['keyword'];
		}
		$data = $model_redpacket->getRedpacketList($where, '*', 0, $this->page, 'rpacket_id desc');
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list = array();
		foreach ($data as $val) {			    
				$i = $val;
			    $i['rpacket_code'] = $val['rpacket_code'];		
			    $i['rpacket_owner_name'] = $val['rpacket_owner_name']?$val['rpacket_owner_name']:'未领取';
			    $i['rpacket_active_datetext'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_active_date']):'N/A';
				$i['rpacket_usage_time'] = $val['rpacket_order_id']>0?date('Y-m-d H:i', $val['rpacket_usage_time']):'N/A';				
				$i['rpacket_order_id'] = $val['rpacket_order_id']>0?$val['rpacket_order_id']:'N/A';				
			    $list[] = $i;
		}
		output_data(array('list' => $list), mobile_page($page_count,$list_count));	
	}
	
	//搜索发送的用户
	public function coupon_member_listOp(){
		
		
		
	}
	
	//发放
	public function coupon_giveOp(){				
		$member_ids =$_POST['member_ids'];
		$tid = $_POST['tid'];
		$model = model('redpacket');
		
	
		$where = array(
			'rpacket_t_id' => $_POST['tid']
		);		
		$info = $model->getRptTemplateInfo($where);


		if(!empty($member_ids)){	
			foreach($member_ids as $v){			
				$redpacket = $model->getCanChangeTemplateInfo($tid,$v);				
				if($redpacket['state'] == true){					
					$model->exchangeRedpacket($redpacket['info'],$v);				
					
					// 发送买家消息
					$param = array();
					$param['code'] = 'coupon_distribution';
					$param['member_id'] = $v;
					$param['param'] = array(			
						'coupon_name' => $info['rpacket_t_title']					
					);
					QueueClient::push('sendMemberMsg', $param);
					
											
						
				}else{
					output_error($redpacket['msg']);
					
				}
			}
			output_data('发放成功');
		}else{
			output_error('请勾选用户');		
		}
			
	}
	
	
	
	//添加优惠券
	public function coupon_saveOp(){
		
		
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
		         array("input"=>$_POST['rpt_title'], "require"=>"true","validator"=>"Length","min"=>"1","max"=>"20","message"=>'优惠券名称不能为空且小于20个字符'),
		        // array("input"=>$_POST['rpt_gettype'], "require"=>"true","message"=>'请选择领取方式'),
		         array("input"=>$_POST['rpt_data'][0], "require"=>"true","message"=>'请选择有效期开始时间'),
		         array("input"=>$_POST['rpt_data'][1], "require"=>"true","message"=>'请选择有效期结束时间'),
		         array("input"=>$_POST['rpt_price'], "require"=>"true","validator"=>"Number","min"=>"1","message"=>'面额不能为空且为大于1的整数'),
		         array("input"=>$_POST['rpt_total'], "require"=>"true","validator"=>"Number","min"=>"1","message"=>'可发放数量不能为空且为大于1的整数'),
		         array("input"=>$_POST['rpt_orderlimit'], "require"=>"true","validator"=>"Double","min"=>"0","message"=>'单笔使用消费限额不能为空且必须是数字')
		         
		 );
		 $error = $obj_validate->validate();
		 //开始时间不能大于结束时间
		 $stime = strtotime($_POST['rpt_data'][0]);
		 $etime = strtotime($_POST['rpt_data'][1]);
		 if ($stime > $etime){
		     $error.= '开始时间不能大于结束时间';
		 }
		 //验证红包面额不能大于订单限额
		 $price = floatval($_POST['rpt_price'])>0?floatval($_POST['rpt_price']):0;
		 $limit = floatval($_POST['rpt_orderlimit'])>0?floatval($_POST['rpt_orderlimit']):0;
		 if($limit>0 && $price>=$limit) $error.= '面额不能大于消费限额';
		 //验证卡密红包发放数量
		 $gettype = trim(2);
		 if($gettype == 'pwd'){
		     if (intval($_POST['rpt_total']) > 10000){
		         $error.= '领取方式为卡密兑换的红包，发放总数不能超过10000张';
		     }
		 }
		 //验证积分
		 $points = intval($_POST['rpt_points']);
		 if($gettype == 'points' && $points < 1){
		     $error.= '兑换所需积分不能为空且为大于1的整数';
		 }
		 if ($error){
		     output_error($error);
		 }else {
		     $model_redpacket = Model('redpacket');
		     $insert_arr = array();
		     $insert_arr['rpacket_t_title'] = trim($_POST['rpt_title']);
		     $insert_arr['rpacket_t_desc'] = trim($_POST['rpt_title']);
		     $insert_arr['rpacket_t_start_date'] = $stime;
		     $insert_arr['rpacket_t_end_date'] = $etime;
		     $insert_arr['rpacket_t_price'] = $price;
		//     $insert_arr['rpacket_t_limit'] = $limit;
		
		
		     $insert_arr['rpacket_t_adminid'] = $this->admin_info['admin_id'];
		     $insert_arr['rpacket_t_state'] = $this->templatestate_arr['usable']['sign'];
		     $insert_arr['rpacket_t_total'] = intval($_POST['rpt_total']);
		     $insert_arr['rpacket_t_giveout'] = 0;
		     $insert_arr['rpacket_t_used'] = 0;
		     $insert_arr['rpacket_t_updatetime'] = time();
		     $insert_arr['rpacket_t_points'] = $points;
		     $insert_arr['rpacket_t_eachlimit'] = ($t = intval($_POST['rpt_eachlimit']))>0?$t:0;
		     $insert_arr['rpacket_t_recommend'] = 0;
		     $insert_arr['rpacket_t_gettype'] = in_array($gettype,array_keys($this->gettype_arr))?$this->gettype_arr[$gettype]['sign']:$this->gettype_arr[$model_redpacket::GETTYPE_DEFAULT]['sign'];
		     $insert_arr['rpacket_t_isbuild'] = 0;
		     $mgrade_limit = intval($_POST['rpt_mgradelimit']);
		     $insert_arr['rpacket_t_mgradelimit'] = in_array($mgrade_limit,array_keys($this->member_grade_arr))?$mgrade_limit:$this->member_grade_arr[0]['level'];
		  
			//print_r($insert_arr);
		  
		     $rs = $model_redpacket->addRptTemplate($insert_arr);
			 
		     if($rs){
		         //生成卡密红包
		         if($gettype == 'pwd'){
		             QueueClient::push('buildPwdRedpacket', $rs);
		         }
		        
				output_data(array('message' => '优惠券添加成功'));		         
		     }else{
				 output_error('添加失败');		         
		     }
		 }
		
	}
	
	//删除优惠券
	public function coupon_delOp(){
		
		$t_id = intval($_POST['tid']);
		if ($t_id <= 0){
		  output_error('优惠券不存在');
		}
		$model_redpacket = Model('redpacket');
		//查询模板信息
		$where = array();
		$where['rpacket_t_id'] = $t_id;
		$where['rpacket_t_giveout'] = array('elt',0);
		$where['rpacket_t_isbuild'] = 0;
		$result = $model_redpacket->dropRptTemplate($where);
		if ($result){	 		  
		    output_data('删除成功');		
		}else {
		    output_error('无法删除，此优惠券已发放给用户');
		}
		
	}
	
	
	
	//线上活动，线下活动，会员福利
	public function activity_listOp(){
		
		$model= model('activity');
		$where = array();
		$where['activity_type'] = $_POST['type'];
		$list = $model->getActivityList($where);
		$data = array();	
		foreach ($list as $k => $v) {					
			
			$v['activity_start_date'] = date('d/m/Y',$v['activity_start_date']);
			$v['activity_end_date'] = date('d/m/Y',$v['activity_end_date']);

			$data[$k] = $v;
		}		
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();		
		output_data(array('list' => $data), mobile_page($page_count,$list_count));		
	}


	
	public function activity_addOp(){
		$model= model('activity');
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(       
			array("input"=>$_POST["activity_title"],"require"=>"true","message"=>'请填活动名称'),
			array("input"=>$_POST["activity_amount"],"require"=>"true","message"=>'请填活动金额'),
		    array("input"=>$_POST["activity_text"],"require"=>"true","message"=>'请输入活动描述'),			
		);
		$error = $obj_validate->validate();
		if ($error != ''){
		    output_error($error);
		}	
		
		$activeity_date = $_POST['activity_date'];

		$data = array(
			'activity_title' 	=> $_POST['activity_title'],
			'activity_type' 	=> $_POST['activity_type'],			
			'activity_start_date' => strtotime($activeity_date[0]),
			'activity_end_date' => strtotime($activeity_date[1]),			
			'activity_text' 	=> $_POST['activity_text'],		
			'activity_amount' 	=> $_POST['activity_amount'],		
		);
		

		$row = $model->addActivity($data);
		if($row){
			output_data('添加成功');
		}else{
			output_error('添加失败');			
		}		
		
		
	}
	
	
	

	
	
	
	
	
	
	
}
