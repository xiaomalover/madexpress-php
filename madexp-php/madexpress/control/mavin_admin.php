<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class mavin_adminControl extends mobileMavinControl {

    public function __construct(){
        parent::__construct();
    }


	public function indexOp(){		
		
		$admin_info = array();
		$admin_info['roles'] = ['admin'];
		$admin_info['introduction'] = 'I am a super administrator';
		$admin_info['avatar'] = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif';
		$admin_info['name'] = 'Super Admin';
		//$admin_info['']
		output_data($admin_info);
		
	}
	
	//
	public function logoutOp(){
		
		output_data('安全登出');
		
		
	}



	public function infoOp(){		
		
		if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 //var_dump($condition);die;
		 
		 
		$info = array();
		$info['order_day'] = 1;
		$info['order_day_icon'] = "bottom";//上升top ,下降bottom
		$info['order_day_per'] = "10%";
		
		$info['price_day'] = 2;
		$info['price_day_icon'] = "top";//上升top ,下降bottom
		$info['price_day_per'] = "20%";

		$info['fee_day'] = 3;
		$info['fee_day_icon'] = "bottom";//上升top ,下降bottom
		$info['fee_day_per'] = "30%";
		
		$info['reg_day'] = 4;
		$info['reg_day_icon'] = "top";//上升top ,下降bottom
		$info['reg_day_per'] = "40%";

		$info['money_day'] = 5;
		$info['money_day_icon'] = "bottom";//上升top ,下降bottom
		$info['money_day_per'] = "50%";
		
		$info['num_day'] = 6;
		$info['num_day_icon'] = "top";//上升top ,下降bottom
		$info['num_day_per'] = "60%";

		$info['he_day'] = 7;
		$info['he_day_icon'] = "bottom";//上升top ,下降bottom
		$info['he_day_per'] = "70%";
		
		$info['view_day'] = 8;
		$info['view_day_icon'] = "top";//上升top ,下降bottom
		$info['view_day_per'] = "80%";

		$info['x_val'] = ['08', '09', '10', '11', '12', '13', '14', '15', '16', '18'];
		$info['y_val'] = [150, 230, 0,0, 0, 147, 2600, 100, 147, 26];
		
		output_data($info);
		
	}
	
	 
	
	
	
}
