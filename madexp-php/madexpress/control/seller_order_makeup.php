<?php
/**
 * 补送订单
 */

defined('InMadExpress') or exit('Access Invalid!');
class seller_order_makeupControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();      
    }

	
	//主订单详情
	public function order_infoOp(){
		
		$order_id = intval($_POST['order_id']);		
		if (!$order_id) {
		    output_error('订单编号有误');
		}
		$model_order = Model('order');
		$condition = array();
		$condition['order_id'] = $order_id;
		$condition['store_id'] = $this->store_info['store_id'];
		
		$order = $model_order->getOrderInfo($condition, array('order_common','order_goods'));
		

		if (empty($order)) {
		    output_error('订单信息不存在');
		}
		
		$order_item = array();
		
		//从新格式化
		$order_user = array();
		$order_user['buyer_code'] = $order['buyer_code'];
		$order_user['buyer_name'] = $order['buyer_name'];
		$order_user['buyer_phone'] = $order['buyer_phone'];
		$order_user['buyer_comment'] = $order['buyer_comment'];
		$order_user['buyer_is_new'] = $order['buyer_is_new'];
		$order_user['distributor_name'] = $order['distributor_name'];
		$order_user['distributor_mobile'] = $order['distributor_mobile'];
		$order_user['state'] = $order['order_state'];
		$order_user['evaluation_state'] = $order['evaluation_state'];
		
		if($order['evaluation_state'] == 1) {			
			$model = model('evaluate_store');		
			$where = array(			
				'store_evaluate_orderid' => $order['order_id']
			);		
			$eva = $model->getEvaluateStoreInfo($where);	
			$order['evaluation_text'] = $eva['store_evaluate_score_text'];
			$order['evaluation_content'] = $eva['store_evaluate_score_content'];	
		}
		
					
		$order_item['order_user'] = $order_user;
				
				
				
		//订单商品,原始商品，不做任何修改
		$order_goods = array();		
		$goods =  $order['extend_order_goods'];		
		$goods_num = 0;
		foreach($goods as $k => $v){			
			$goods_num += $v['goods_num'];	
		}
		
		$order_goods['goods_list'] = $order['extend_order_goods'];				
		$order_goods['goods_num'] = $goods_num;		
		$order_item['order_goods'] = $order_goods;
		
		
		
		//加载已经补送的商品
		
		
		
			//商家赠送商品
		$order_goods_give = array();
		$where = array();
		$where['order_id'] = $order['order_id'];
		$where['give_type'] = 1;
		$give =  model('order_goods_give')->getGiveList($where); //$order['extend_order_goods_give'];
		
		
		$goods_num = 0;
		$goods_amount = 0;
		if(!empty($give)){
		    $give_list = array();
			foreach($give as $k => $v){			
			    $v['goods_optional'] = unserialize($v['goods_optional']);
			    $give_list[] = $v;
				$goods_num += $v['goods_num'];	
				$goods_amount +=$v['goods_num'] * $v['goods_price'];
			}
		}
		
		
		$order_goods_give['goods_num'] = 	$goods_num;	
		$order_goods_give['goods_amount'] = 	$goods_amount;			
		$order_goods_give['goods_list'] 	= $give_list;
		$order_item['order_give'] = $order_goods_give;
		
		
		
		
		
		
		
		
		
		
		
		output_data($order_item);
			
	}
	
	
	
	//保存补送订单
	public function order_saveOp(){
		
		$model = model('order');
		$order_id 	= $_POST['order_id'];
		$coupon_id = $_POST['coupon_id'];
		
		$order_info = $model->getOrderInfoN(array('order_id'=>$order_id,'store_id'=>$this->store_info['store_id']));
		
		if(empty($order_info)){			
			output_error('订单参数错误');
		}
		
				
		if(empty($_POST['goods_ids'])){			
			output_error('请选择补送商品');
		}
		
	
		//更新订单信息
		
		$makeup_order = $order_info;
		$makeup_order['is_makeup'] = 1;
		$makeup_order['makeup_order_id'] = $order_info['order_id'];
		$makeup_order['goods_amount'] = 0;
		$makeup_order['sales_amount'] = 0;
		$makeup_order['order_amount'] = 0;
		$makeup_order['foodbox_amount'] = 0;
		$makeup_order['commission_amount'] = 0;		
		$makeup_order['delivery_fee'] = $order_info['delivery_fee'];
		$makeup_order['order_state'] = 10;		
		$makeup_order['makeup_comment'] = $_POST['comment'];
		
		
		
		unset($makeup_order['distributor_id']);
		unset($makeup_order['distributor_name']);
		unset($makeup_order['distributor_mobile']);
		unset($makeup_order['distributor_start_time']);
		unset($makeup_order['distributor_end_time']);
		unset($makeup_order['order_estimate_time']);
		unset($makeup_order['distributor_code']);
		unset($makeup_order['distributor_duration']);
		unset($makeup_order['distributor_coordinate']);
		unset($makeup_order['order_id']);		
		unset($makeup_order['refund_state']);		
		unset($makeup_order['refund_comment']);		
		unset($makeup_order['refund_reason']);			
		unset($makeup_order['refund_amount']);
		unset($makeup_order['order_invoice_pdf']);		
		
		$order_id  = model('order')->addOrder($makeup_order);		
		if($order_id){
				
				$goods_ids = explode('|',$_POST['goods_ids']);
				foreach($goods_ids as $v){				
					$goods = explode(',',$v);				
					$goods_info = $this->getOrderGoods($goods[0]);				
					
					if($goods[1] > $goods_info['goods_num']){					
					    
						output_error('补送商品数量超限');	
						
					}				
					
					$goods_info['goods_num'] = $goods[1];
					$goods_info['state'] = 5;
					$goods_info['order_id'] = $order_id;
					unset($goods_info['rec_id']);				
					$makeup_goods[] = $goods_info;	
				}			
							
							
				//给订单写入补送商品
				model('order')->addOrderGoods($makeup_goods);	

				$order_goods_give = array();
				$where = array();
				$where['order_id'] = $order['order_id'];
				$where['give_type'] = 1;
				$model_give = model('order_goods_give');
				$give_list = $model_give->getGiveList($where); //$order['extend_order_goods_give'];		
				
				
				//判断是否有赠送商品
				if(!empty($give_list)){		
					foreach($give_list as $v){	
					    
					    $goods_optional = 	$this->goodsOptional($v['goods_optional']);	
					    
						$goods_info = array();
						$goods_info['order_id'] = $order_id;
						$goods_info['goods_id'] = $v['goods_id'];
						$goods_info['goods_name'] = $v['goods_name'];
						$goods_info['goods_price'] = $v['goods_price'];
						$goods_info['goods_num'] = $v['goods_num'];
						$goods_info['goods_image'] = $v['goods_image'];				
						$goods_info['goods_pay_price'] = 0;
						$goods_info['store_id'] = $v['store_id'];
						$goods_info['goods_spec'] = $v['goods_spec'];
						$goods_info['goods_size'] = $v['goods_size'];				
						$goods_info['state'] = 3;				
						$goods_info['goods_lang'] = $v['goods_lang'];
						$goods_info['goods_optional'] = $goods_optional;
						$goods_info['replace_rec_id'] = $v['rec_id'];				
						$goods_info['is_take_ffect'] = 1;
						$goods_info['give_type'] = 1;
						
						$model->addOrderGood($goods_info);	
						
						
					}
				}
				
				//删除已选赠送商品
				$model_give->delGive($where);
				
				output_data('补送单创建成功');
				
				//开始扣款,补送运费。		
			//	$row = model('store_wallet')->changePd();
				
				
							
		}else{
				output_error('补送失败');
		}
	}
		
	
	private function goodsOptional($ids){			
			$where = array();
			$where['options_id'] = array('in',$ids);
			$where['state'] = 0 ;
			$where['store_id'] = $this->store_info['store_id'];	
			$list = model('goods_options')->getOptionsList($where);		
			$optional = array();
			foreach($list as  $k=>$v){
				$optional[$k]['id'] = $v['options_id'];
				$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$store_id);
				$optional[$k]['price'] = $v['options_price'];
			}
			return $optional;
	}
		
		
		//格式化配件商品的语言包
		private function optionsLang($data,$storeId){			
		    $langInfo = model('language')->getLangInfo(array('language_id'=>$this->store_info['store_default_lang']));
			$data = unserialize($data);
			foreach($data as $k => $v){
				$list[$v['lang_name']] = $v['options_name'];
			}	
			return $list[$langInfo['language_flag']];		
		}
	
	
		//获取当前值的KEY	
		private function find_by_array_spec($array,$find)
		{
			 foreach ($array as $key => $v)
			{
				if($v['value']==$find)
				{
					return $key;
				}
			}
		}
		
		
		private function find_by_array_size($array,$find)
		{
			foreach ($array as $key => $v)
			{
				if($v['name']==$find)
				{
					return $key;
				}
			}
		}
	
	
		
		
	//获取商品信息
	private function getOrderGoods($rec_id){
		
		$where = array();
		$where['rec_id'] = $rec_id;
		$where['store_id'] = $this->store_info['store_id'];
		
		
		
		$goods_info = model('order')->getOrderGoodsInfo($where);
		return $goods_info;
	}
	
	
	
}
