<?php
/**
 * 排班记录
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class schedulingControl extends mobileAdminControl
{
    public function __construct()
    {
        parent::__construct();
    }
    
	public function infoOp(){
		 
		$info = array();
		if($_GET['type']==0){
			$info['car_ed'] = [80, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
			$info['car_plan'] = [360, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
		}else if($_GET['type']==1){
			$info['car_ed'] = [180, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
			$info['car_plan'] = [360, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
		}else if($_GET['type']==2){
			$info['car_ed'] = [280, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
			$info['car_plan'] = [360, 52, 200, 334, 390, 330, 220, 200, 334, 390, 330, 220];
		}
		$info['x_val'] = ['01', '02', '03', '04', '05', '06', '07','08','09','10', '11', '12'];
		
		output_data($info);
	}

    //获取
    public function get_schedulingOp()
    {
    

        
          // 设置页码参数名称
        $scheduling_model = Model('scheduling');
        $condition = array();
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = $_POST['query'];
        }
        if ($_POST['keyword'] != '') {
            $condition["scheduling_sn"] = $_POST['keyword'];
        }
		
        $order = '';
        $param = array('scheduling_id');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $condition['order'] = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        $condition['scheduling_status'] = array('in','10,20');
                
        $scheduling_list = $scheduling_model->getSchedulingList($condition, $this->page);
		
    
        $page_count = $scheduling_model->gettotalpage();
        $list_count = $scheduling_model->gettotalnum();
        
        
        $status = $this->getStatus();
        $data = array();
    
    
        foreach ($scheduling_list as $k => $v) {
            $param = array();
            $param['scheduling_id'] = $v['scheduling_id'];
            $param['scheduling_sn'] = $v['scheduling_sn'];
        
            $waiter =$this->getDistrInfo($v['distributor_id']);
           
            $param['delivery_avatar'] = $waiter['avatar'];
            $param['delivery_name'] = $waiter['distributor_name'];
            $param['delivery_code'] = $waiter['distributor_code'];
            $param['region_name'] =  $v['region_name'];
            $param['region_color'] =  $v['region_color'];
            $param['region_id'] =  $v['region_id'];
            $param['scheduling_start_time'] =  date('y-m-d', $v['scheduling_start_time']);
            $param['scheduling_sign_time'] = $v['scheduling_sign_time'] > 0 ? date('Y-m-d', $v['scheduling_sign_time']) . ' ' . $v['scheduling_t_name'] : '';
            
			$param['vehicle'] = $this->vehicle($v['distributor_id']);
			
             
            $param['scheduling_date'] =  date('d/m/y', $v['scheduling_date']) .' '. $v['scheduling_t_name'];
            $param['status'] = $status[$v['scheduling_status']];
            $list[] = $param;
        }
		
        output_data(array('list' => $list), mobile_page($page_count, $list_count));
    }
    
    
	//取消排班
	
	public function cancel_schOp(){
		
		$id = $_POST['id'];		
		$row = model('scheduling')->editScheduling(['scheduling_status' => 40],['scheduling_id' => $id]);

		if($row){
			
			output_data('取消成功');
			
		}else{
			output_error('取消失败');
			
		}
		
		
		
		
		
		
		
	}
	
	
	
    //获取车辆
    
    private function getVehicleInfo($id)
    {
		/*
        $where = array(
            'vehicle_id' => $id
        );
        $vehicle_info = model('vehicle')->vehicleInfo($where);
        $class = $this->getVehicleClass($vehicle_info['vehicle_class_id']);
        $vehicle_info['vehicle_class_icon']	 = $class['vehicle_class_icon'];
		*/
		
		
        return $vehicle_info;
    }
    
    //获取分类
    private function getVehicleClass($class_id)
    {
        $class = model('vehicle_class')->vehicleClassInfo(array('vehicle_class_id'=>$class_id), 'vehicle_class_icon,vehicle_class_icon_active');
    
        return $class;
    }
    
    
    //获取配送员的数据信息
    private function getDistrInfo($id)
    {
        $where = array(
            'distributor_id' => $id
        );
        $Waiter = model('waiter')->getWaiterInfo($where);
        $Waiter['avatar'] = getWaiterAvatar($Waiter['distributor_avatar']);
        return $Waiter;
    }
    
    private function getStatus()
    {
        $data = array();
        $data[10] = '待上班';
        $data[20] = '进行中';
        $data[30] = '已完成';
        $data[40] = '已取消';
        $data[50] = '未达标';
        return $data;
    }
    
    
    
    
    //获取排班地区配置
    public function get_regionOp()
    {
        $region_list =  model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1));
        $data = array();
        foreach ($region_list as $k=> $v) {
            $data[$k]['region_name'] = $v['region_name'];
            $data[$k]['region_color'] = $v['region_color'];
            $data[$k]['region_id'] = $v['region_id'];
            $data[$k]['t1']['PA'] 	= array(
                'num'=>0,
                'price' => 0
            );
            $data[$k]['t1']['M'] 	= array(
                'num'=>0,
                'price' => 0
            );
            $data[$k]['t2']['PA'] 	= array(
                'num'=>0,
                'price' => 0
            );
            $data[$k]['t2']['M'] 	= array(
                'num'=>0,
                'price' => 0
            );
        }
        
        output_data(array('list' => $data));
    }
    
    //获取T1 T2设置
    public function get_settingOp()
    {
        $where = array();
        $list = model('scheduling_setting')->getSchedulingNew($where);
        $data = array();
        foreach ($list as $k => $v) {
            $data[$k]['name'] 		= $v['name'];
            $data[$k]['start_time'] = date('H:i', $v['start_time']);
            $data[$k]['end_time'] 	= date('H:i', $v['end_time']);
            $data[$k]['id'] 		= $v['id'];
        }
        output_data(array('t1'=>$data[0],'t2'=>$data[1]));
    }
    
    
    public function save_settingOp()
    {
        $where = array();
        $model = model('scheduling_setting');
        $where['id'] = $_POST['id'];
        $data = array();
        $data['start_time'] = strtotime('2021-01-01'.' '.$_POST['start_time']);
        $data['end_time'] = strtotime('2021-01-01'.' '.$_POST['end_time']);
        $row  = $model->editScheduling($where, $data);
        if ($row) {
            output_data('保存成功');
        } else {
            outupt_error('保存失败');
        }
    }
    
    

    
    
    
    
    //本次上班详情
    public function get_infoOp()
    {
        $status = array(
                    '10' => '待上班',
                    '20' => '进行中',
                    '30' => '已完成',
                    '40' => '已取消'
            );
            
        $model = Model('scheduling');
        $where = array(
                'scheduling_id' => $_POST['id']
            );
            

        $data = $model->getSchedulingInfo($where);
            
        $scheduling =array();
        $scheduling['scheduling_id']= $data['scheduling_id'];
    
        $scheduling['status'] = $status[$data['scheduling_status']];
        $scheduling['name'] = $data['scheduling_t_name'];
            
        $scheduling['region_name'] = $data['region_name'];
        $scheduling['region_color'] = $data['region_color'];
            
        $scheduling['scheduling_sn'] = $data['scheduling_sn'];
            
            
        $scheduling['scheduling_date'] = $this->workN($data['scheduling_date']);
        $scheduling['scheduling_addtime'] =  date('m/d/y H:i:s', $data['scheduling_addtime']);
        
        $scheduling['scheduling_start_time'] = date('m/d/y H:i:s', $data['scheduling_start_time']);
        $scheduling['scheduling_end_time'] = date('m/d/y H:i:s', $data['scheduling_end_time']);
            
        $scheduling['scheduling_sign_time'] = date('H:i:s', $data['scheduling_sign_time']);
        $scheduling['scheduling_signout_time'] = date('H:i:s', $data['scheduling_signout_time']);
        
                        
        $scheduling['scheduling_rest'] = $this->restLog($data['scheduling_id']);
        $scheduling['scheduling_sign_address'] = $data['scheduling_sign_address'];
        $scheduling['vehicle'] = $this->vehicle($data['distributor_id']);
            
        $scheduling['scheduling_price'] = $data['scheduling_price'];
        $scheduling['scheduling_time_num'] = $data['scheduling_time_num'];
            
            
        $scheduling['total_amount'] = $data['total_amount'];
        $scheduling['pay_state'] = $data['pay_state'];
        $scheduling['total_time'] = $data['total_time'];
        $scheduling['nav_coordinate'] = $data['nav_coordinate'];
            
            
        output_data(array('info' => $scheduling ));
    }
    
        
    private function workN($date)
    {
        $arr = array('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
                
        $work = $arr[date('w', $date)] .' '. date('d', $date) .' '. date('M', $date)  ;
                
        return $work;
    }
            
    //休息记录
    private function restLog($id)
    {
        $where = array(
                    'scheduling_id' => $id
                );
        $data = model('scheduling_log')->getSchedulingLogList($where);
        $rest = array();
        foreach ($data as $k=> $v) {
            $rest[$k] = $v;
            $rest[$k]['log_start_time'] = date('H:i:s', $v['log_start_time']);
            $rest[$k]['log_end_time'] = date('H:i:s', $v['log_end_time']);
        }
        return $rest;
    }
    
    //查询车辆
    private function vehicle($waiter_id)
    {
        
		$delivery_info = model('waiter')->getWaiterInfo(array('distributor_id' => $waiter_id));
		
		$vehicle =  array();
		$vehicle['vehicle_brand']	 = $delivery_info['vehicle_brand'];
		$vehicle['vehicle_type']	 = $delivery_info['vehicle_type'];
		$vehicle['vehicle_number']	 = $delivery_info['vehicle_number'];
		
		return $vehicle;
		
		
    }
        
    
    
    //获取车辆
    
    public function get_vehicle_leaseOp()
    {
        $state = array(
            0 => '待支付',
            10 => '待确认',
            20 => '待取车',
            30 => '进行中',
            40 => '待付款',
            50 => '已完成',
            60 => '已评价',
            70 => '无效'
        );
        
        
        $model_lease = model('vehicle_lease');
        
        $where = array();
        $where['vehicle_class_id'] =  $_POST['vehicle_class_id'];
        if ($_POST['keyword'] != '') {
            $where["vehicle_code"] = $_POST['keyword'];
        }
        
        $lease_list =$model_lease->getLeaseList($where, '*', $this->page);
        $list = array();
        foreach ($lease_list as $k => $v) {
            $list[$k] = $v;
            $list[$k]['lease_state_name'] = $state[$v['lease_state']];
            $list[$k]['lease_start_time']	 = date('d/m/y', $v['lease_start_time']);
            $list[$k]['lease_end_time']	 	 = date('d/m/y', $v['lease_start_time']);
            
            $class = $this->getVehicleClass($v['vehicle_class_id']);
            $list[$k]['vehicle_class_icon']	 = $class['vehicle_class_icon'];
        }
        
        $page_count = $model_lease->gettotalpage();
        $list_count = $model_lease->gettotalnum();
        
        output_data(array('list' => $list), mobile_page($page_count, $list_count));
    }
        
        
    
    //获取日期
    public function get_work_listOp()
    {
        extract($_POST);
        
        if (!$year) {
            $year = date("Y");
        }
        if (!$month) {
            $month = date("m");
        }
        if (!$today) {
            $today = date("d");
        }
        
        if ($prev) {
            $today -= 7;
        }
        if ($next) {
            $today += 7;
        }
        
        $t = mktime(0, 0, 0, $month, $today, $year);
        list($year, $month, $today) = explode('-', date('Y-m-d', $t));
        $w = date('w', $t);
        $t = strtotime("-$w day", $t);
        $weekArr=array( "Sun ", "Mon ", "Tues ", "Wed ", "Thur ", "Fri ", "Sat");
        
        for ($n=0;$n <7;$n++) {
            $day_list[$n]['week'] = $weekArr[$n];
            $day_list[$n]['day'] = date("d", strtotime("$n day", $t));
            $day_list[$n]['month'] = date("M", strtotime("$n day", $t));
            $day_list[$n]['year'] = date("Y", strtotime("$n day", $t));
            
            $day_list[$n]['m'] = date("m", strtotime("$n day", $t));
                      
            $day_list[$n]['t_id'] = $t_id;
            $region =  $this->get_sch_region($day_list[$n]);
            $day_list[$n]['region_list'] =$region;
        }
        
        $range_day = array(
            'start' => $day_list[0],
            'end' 	=> $day_list[6]
        );
        
        $date = array(
            'year' => $year,
            'month' => $month,
            'today' => $today,
            'prev' => $prev,
            'next' => $next,
            't_id' => $t_id
        );
        
        output_data(array('list' => $day_list,'range_day' => $range_day,'date'=>$date));
    }
        
    
    
    //获取当前设置
    private function get_sch_region($date)
    {
        $region_list =  model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));
        foreach ($region_list as $k => $v) {
            $data[$k]['region_id'] 		= $v['region_id'];
            $data[$k]['region_name'] 	= $v['region_name'];
            $data[$k]['region_color'] 	= $v['region_color'];
            
            $date = $date;
            $date['region_id'] = $v['region_id'];
            $sch_region = $this->sch_region_info($date);
            
            if (empty($sch_region)) {
                $data[$k]['id'] = 0;
                $data[$k]['m_bike']  = 0;
                $data[$k]['pa_bike'] = 0;
                $data[$k]['m_bike_price'] =	0;
                $data[$k]['pa_bike_price'] = 0;
            } else {
                $data[$k]['id'] = $sch_region['id'];
                $data[$k]['m_bike']  = $sch_region['m_bike'];
                $data[$k]['pa_bike'] = $sch_region['pa_bike'];
                $data[$k]['m_bike_price'] =	$sch_region['m_bike_price'];
                $data[$k]['pa_bike_price'] = $sch_region['pa_bike_price'];
                $data[$k]['operation'] = 0;
            }
        }
        return $data;
    }
    
    private function sch_region_info($data)
    {
        $where = array();
        $where['day'] 	= $data['day'];
        $where['month'] = $data['m'];
        $where['year'] 	= $data['year'];
        $where['setting_id'] = $data['t_id'];
        $where['region_id'] = $data['region_id'];
                
                
        $info = model('scheduling_region')->getSchedulingInfo($where);
        
        return $info;
    }
    
    //保存设置
    public function scheduling_item_saveOp()
    {
        $where = array();
        $where['id'] = $_POST['id'];
            
        $update = array();
        $update['m_bike'] = $_POST['m_bike'];
        $update['m_bike_price'] = $_POST['m_bike_price'];
        $update['pa_bike'] = $_POST['pa_bike'];
        $update['pa_bike_price'] = $_POST['pa_bike_price'];
        $update['num'] = $_POST['m_bike'] + $_POST['pa_bike'];
            
        $row = model('scheduling_region')->editScheduling($update, $where);
            
        if ($row) {
            output_data('保存成功');
        } else {
            output_error('保存失败');
        }
    }
    
        
    
    
    //任务
    public function get_task_listOp()
    {
        $list = model('scheduling_task')->getTaskList(true);
        
        output_data(array('list' => $list));
    }

    //任务详情
    public function get_task_infoOp()
    {
        $where = array();
        $where['id'] = $_POST['id'];
        $info = model('scheduling_task')->infoTask($where);
        if ($info) {
            $info['value'] =unserialize($info['value']);
        }
        
        output_data(array('info' => $info));
    }


    //任务保存
    public function get_task_saveOp()
    {
        $data = array();
        $data['name'] = $_POST['name'];
        $data['content'] = $_POST['content'];
        $data['enable'] = $_POST['enable'];
        $data['value'] = serialize($_POST['value']);
                
        if ($_POST['id'] > 0) {
            $where = array();
            $where['id'] = $_POST['id'];
            $row = model('scheduling_task')->editTask($data, $where);
        } else {
            $row = model('scheduling_task')->addTask($data);
        }
        
        if ($row) {
            output_data('操作成功');
        } else {
            output_error('操作失败');
        }
    }


    
    //保存批量设定
    public function scheduling_batch_saveOp()
    {
        $list =  json_decode($_POST['region_data'], true);
        $model = model('scheduling_batch');
        foreach ($list as $v) {
            if ($v['t1']) {
                $t1_data =  $v['t1'];
                $where = array(
                    'id' => $t1_data['id']
                );
                unset($t1_data['id']);
                $model -> editBatch($t1_data, $where);
            }
            
            if ($v['t2']) {
                $t2_data =  $v['t2'];
                $where = array(
                    'id' => $t2_data['id']
                );
                unset($t2_data['id']);
                $model -> editBatch($t2_data, $where);
            }
        }
        
        output_data('保存成功');
    }
    
    //批量列表
    public function scheduling_batch_listOp()
    {
        $where = array();
        
        $list = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));
        $region_list = array();
        foreach ($list as $k => $v) {
            $region_list[$k]['region_id'] = $v['region_id'];
            $region_list[$k]['region_name'] = $v['region_name'];
            $region_list[$k]['region_color'] = $v['region_color'];
            $region_list[$k]['t1'] = $this->scheduling_t_list($v['region_id'], 1);
            $region_list[$k]['t2'] = $this->scheduling_t_list($v['region_id'], 2);
        }
        output_data(array('list' => $region_list));
    }
	
    
    //T1 T2 的batch 数据
    private function scheduling_t_list($region_id, $t)
    {
        $where =  array(
            'region_id' => $region_id,
            'setting_id' => $t
        );
        return  model('scheduling_batch')->getBatchInfo($where);
    }
    
    

    public function update_regionOp()
    {
        $model = model('scheduling_batch');
        $list = $model->getBatchList(true);
            
		//scheduling_setting_date 上次生成的月份
		//本次执行生成月份。		
		$lasttime = C('scheduling_setting_date');		
		
        $day = date('d', time());
        $month = date('m', time());
        $year = date('Y', time());
        
		$month_day = intval(date("t"));
	
		for($i = 1 ;  $i<=$month_day; $i++ ){	
				$batch = array();
				foreach ($list as $v) {
					$data = array();
					$data['region_id'] = $v['region_id'];
					$data['region_name'] = $v['region_name'];
					$data['num'] = $v['num'];
					$data['setting_id'] = $v['setting_id'];
					$data['m_bike'] = $v['m_bike'];
					$data['pa_bike'] = $v['pa_bike'];
					$data['day'] = $i < 10 ? '0'.$i : $i;
					$data['month'] = $month;
					$data['year'] = $year;
					$data['m_bike_price'] = $v['m_bike_price'];
					$data['pa_bike_price'] = $v['pa_bike_price'];
					$data['region_color'] = $v['region_color'];
					$batch[] = $data;
				}
				
				
				$row = model('scheduling_region')->addSchedulingAll($batch);
				
		}	
		
        if ($row) {			
			$update_array = array();
			$update_array['scheduling_setting_date'] = date('Y-m',time());			
			model('setting')->updateSetting($update_array);
			
            output_data('操作成功');
        } else {
            output_error('操作失败');
        }
    }
	
	
	//申请的排班审核
	
	public function get_examine_schOp(){
				
		
		  // 设置页码参数名称
		$scheduling_model = Model('scheduling');
		$condition = array();
		if ($_POST['query'] != '') {
		    $condition[$_POST['qtype']] = $_POST['query'];
		}
		$order = '';
		$param = array('scheduling_id');
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
		    $condition['order'] = $_POST['sortname'] . ' ' . $_POST['sortorder'];
		}
		$condition['is_apply'] = 1;
		$condition['apply_state'] = 0;
				
		$scheduling_list = $scheduling_model->getSchedulingList($condition, $this->page);
		    
		$page_count = $scheduling_model->gettotalpage();
		$list_count = $scheduling_model->gettotalnum();
				
		$status = $this->getStatus();
		$data = array();		    
		    
		foreach ($scheduling_list as $k => $v) {
		    $param = array();
		    $param['scheduling_id'] = $v['scheduling_id'];
		    $param['scheduling_sn'] = $v['scheduling_sn'];
		
		    $waiter =$this->getDistrInfo($v['distributor_id']);
		    
		    $param['delivery_avatar'] = $waiter['avatar'];
		    $param['delivery_name'] = $waiter['distributor_name'];
		    $param['delivery_code'] = $waiter['distributor_code'];
		    $param['region_name'] =  $v['region_name'];
		    $param['region_color'] =  $v['region_color'];
		    $param['region_id'] =  $v['region_id'];
		    $param['scheduling_start_time'] =  date('y-m-d', $v['scheduling_start_time']);
		    $param['scheduling_sign_time'] = $v['scheduling_sign_time'] > 0 ? date('Y-m-d', $v['scheduling_sign_time']) . ' ' . $v['scheduling_t_name'] : '';
		    
		    $param['vehicle'] = $this->getVehicleInfo($v['vehicle_id']);
		    
		    $param['scheduling_date'] =  date('d/m/y', $v['scheduling_date']) .' '. $v['scheduling_t_name'];
		    $param['status'] = $status[$v['scheduling_status']];
		    $list[] = $param;
		}
		output_data(array('list' => $list), mobile_page($page_count, $list_count));
		
		
		
	}
	
	
	//通过或者拒绝
	public function examine_stateOp(){
		
		$sch_ids = $_POST['ids'];
		$where = array();				
		$where['scheduling_id'] = array('in',$sch_ids);
				
		$data = array();
		$data['apply_state'] = $_POST['state'];
				
		$row = Model('scheduling')->editScheduling($data,$where);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
				
	}
	
	
	
}
