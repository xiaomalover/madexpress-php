<?php
/**
 * 手机接口初始化文件
 */
error_reporting(0);
$origin = isset($_SERVER['HTTP_ORIGIN']) ? $_SERVER['HTTP_ORIGIN']:''; 
$allow_origin = array( 
    'http://test-admin.mad-express.com', 
    'http://test-store.mad-express.com',
	'http://test-mavin.mad-express.com' 
); 
if(in_array($origin,$allow_origin)) 
{ 
    @header("Access-Control-Allow-Origin:".$origin); 
}

@header("Access-Control-Allow-Methods:GET,POST,OPTIONS");
@header("access-control-allow-headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With,x-token");
@header('Access-Control-Allow-Credentials:true');


define('APP_ID','madexpress');
define('IGNORE_EXCEPTION', true);
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));

require __DIR__ . '/../me.php';
require __DIR__ . '/../vendor/autoload.php';
define('MOBILE_RESOURCE_SITE_URL',MOBILE_SITE_URL.DS.'resource');

if (!is_null($_GET['key']) && !is_string($_GET['key'])) {
    $_GET['key'] = null;
}
if (!is_null($_POST['key']) && !is_string($_POST['key'])) {
    $_POST['key'] = null;
}
if (!is_null($_REQUEST['key']) && !is_string($_REQUEST['key'])) {
    $_REQUEST['key'] = null;
}

//框架扩展
require(BASE_PATH.'/framework/function/function.php');

if (!@include(BASE_PATH.'/control/control.php')) exit('control.php isn\'t exists!');

Base::run();
?>