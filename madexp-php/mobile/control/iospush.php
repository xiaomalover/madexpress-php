<?php 

defined('InMadExpress') or exit('Access Invalid!');

class iospushControl extends mobileHomeControl {
  
    
    public function __construct(){
        parent::__construct();	
		$this->deviceToken = 'e9e8139cb1cc3056993f602f6fe849814eb38de4decd0fd488cd27fc4b031476';
    }
	
	public function indexOp(){
	
	  $keyfile 	= 'AuthKey_4222Q6PWAX.p8';               # <- Your AuthKey file
	  $keyid 	= '4222Q6PWAX';                            # <- Your Key ID
	  $teamid 	= 'FH99JDB99P';                           # <- Your Team ID (see Developer Portal)
	  $bundleid = 'com.madexpress.merider';             # <- Your Bundle ID
	  $url 		= 'https://api.development.push.apple.com';  # <- development url, or use http://api.push.apple.com for production environment
	  $token 	= $this->deviceToken;              		# <- Device Token
	 
	  $message 	= '{"aps":{"alert":"Hi there!","sound":"default"}}';
	 
	  $key 		= openssl_pkey_get_private('file://'.$keyfile);
	 
	  $header = ['alg'=>'ES256','kid'=>$keyid];
	  $claims = ['iss'=>$teamid,'iat'=>time()];
	 
	  $header_encoded = $this->base64($header);
	  $claims_encoded = $this->base64($claims);
	 
	  $signature = '';
	  openssl_sign($header_encoded . '.' . $claims_encoded, $signature, $key, 'sha256');
	  $jwt = $header_encoded . '.' . $claims_encoded . '.' . base64_encode($signature);
	 
	  // only needed for PHP prior to 5.5.24
	  if (!defined('CURL_HTTP_VERSION_2_0')) {
	      define('CURL_HTTP_VERSION_2_0', 3);
	  }
	 
	 $url = $url."/3/device/".$token;
	 
	  $http2ch = curl_init();
	  curl_setopt($http2ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_2_0);
	  curl_setopt($http2ch,CURLOPT_URL,$url);

	  curl_setopt($http2ch,CURLOPT_PORT,443);	  
	  curl_setopt($http2ch,CURLOPT_HTTPHEADER,array(
	      "apns-topic: {$bundleid}",
	      "authorization: bearer $jwt"
	    ));
	  curl_setopt($http2ch,CURLOPT_POST,1);

	curl_setopt($http2ch,CURLOPT_POSTFIELDS,$message);
	curl_setopt($http2ch,CURLOPT_RETURNTRANSFER,TRUE);
	curl_setopt($http2ch,CURLOPT_TIMEOUT,30);
	curl_setopt($http2ch,CURLOPT_HEADER,1);
	   
	  
	  /*
	  curl_setopt_array($http2ch, array(
	    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
	    CURLOPT_URL => $url,
	    CURLOPT_PORT => 443,
	    CURLOPT_HTTPHEADER => array(
	      "apns-topic: {$bundleid}",
	      "authorization: bearer $jwt"
	    ),
	    CURLOPT_POST => 1,
	    CURLOPT_POSTFIELDS => $message,
	    CURLOPT_RETURNTRANSFER => TRUE,
	    CURLOPT_TIMEOUT => 30,
	    CURLOPT_HEADER => 1,
		
		
	  ));
	 */
	  $result = curl_exec($http2ch);
	  if ($result === FALSE) {
	    throw new Exception("Curl failed: ".curl_error($http2ch));
	  }
	 
	  $status = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);
	  echo $status;
	  
	 }
	 
	 
	 
	 
	 private  function base64($data) {
	    return rtrim(strtr(base64_encode(json_encode($data)), '+/', '-_'), '=');
	  }

	
	
	/*
	
	public function newOp(){
		
		$url = "api.sandbox.push.apple.com/3/device/".$this->deviceToken;		
		$headers = array(
			"apns-topic: com.example.exampleapp",
			"apns-push-type: alert",
			"Content-Type: application/x-www-form-urlencoded",
		);		
		$certificate_file = "push_php.pem";		
		$payloadArray['aps'] = [
			'alert' => [
				'title' => "Test Push Notification",
				'body' => "Ohhh Yes working", 
			],
			'sound' => 'default',
			'badge ' => 1		
		];		
		$data = json_encode($payloadArray);		
		$client = new Client();		
		$response = $client->post($url, [
			'headers' => $headers,
			'cert' => $certificate_file,
			'curl' => [
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
			],
			'body'=> $data,		
		]);
		
		
	}
	
	
	
	
	
	public function index2Op(){
		
		
		$deviceToken = 'e9e8139cb1cc3056993f602f6fe849814eb38de4decd0fd488cd27fc4b031476';   //推送设备id
		$passphrase = '123456';    //证书私钥密码
		$pemfilename = 'push_php.pem';   //推送证书
		// SIMPLE PUSH 
		$body['aps'] = array(
		    'alert' => array(
		        'title' => "You have a notification",
		        'body' => "Body of the message",
		    ),
		    'badge' => 1,
		    'sound' => 'default',
		    ); // Create the payload body
		////////////////////////////////////////////////////////////////////////////////
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		$fp = stream_socket_client(
		    'ssl://gateway.sandbox.push.apple.com:2195', $err,
		    $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // Open a connection to the APNS server
		if (!$fp)
		    exit("Failed to connect: $err $errstr" . PHP_EOL);
		echo 'Connected to APNS' . PHP_EOL;
		$payload = json_encode($body); // Encode the payload as JSON
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload; // Build the binary notification
		$result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
		if (!$result)
		    echo 'Message not delivered' . PHP_EOL;
		else
		    echo 'Message successfully delivered' . PHP_EOL;
		fclose($fp); // Close the connection to the server
	


	
		}

*/
}
?>