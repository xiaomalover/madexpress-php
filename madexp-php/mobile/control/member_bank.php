<?php
/**
 * 我的银行卡

 */



defined('InMadExpress') or exit('Access Invalid!');

class member_bankControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
        $this->model_bank = Model('member_bank');
    }

    /**
     * 银行卡列表
     */
    public function bank_listOp() {
        $bank_list = $this->model_bank->getMemberBankList(array('member_id'=>$this->member_info['member_id']));	
        output_data(array('bank_list' => $bank_list));
        output_data('ok');
    }

    /**
     * 新增银行卡
     */
    public function bank_addOp() {
        $bank_info = $this->_bank_valid();

		$where = array('member_id'=> $this->member_info['member_id']);		
		if($_POST['is_default'] == 1){
	    	$this->model_bank->editMemberBank(array('is_default'=> 0),$where);
		}
        $result = $this->model_bank->addMemberBank($bank_info);
        if($result) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }
	
	
	
	public function bank_delOp(){
		
		$where = array(
			'member_id'=>$this->member_info['member_id'],
			'bank_id' =>$_POST['bank_id']
		);		
		$row = $this->model_bank->delMemberBank($where);
		if($row){			
			output_data('ok');
        } else {
            output_error('删除失败');
			
		}
		
	}

	public function bank_infoOp(){
	    
	    $where = array(
			'member_id'=>$this->member_info['member_id'],
			'bank_id' =>$_POST['bank_id']
		);		
		$row = $this->model_bank->getMemberBankInfo($where);
		if($row){			
			output_data($row);
        } else {
            output_error('获取失败');
			
		}
	    
	}
	
		
    
    public function bank_defaultOp(){
        
        
        
		$where = array('member_id'=> $this->member_info['member_id']);		
	    $data = array();
	    $data['is_default'] = 0;
		$this->model_bank->editMemberBank($data,$where);
        $where = array(
            'member_id'=> $this->member_info['member_id'],
            'bank_id' => $_POST['bank_id']
        );
        $row = $this->model_bank->editMemberBank(array('is_default' => 1),$where);
        if($row){
            output_data('修改成功');
        }else{
            output_error('修改失败');
        }
        
    }



    /**
     * 验证数据
     */
    private function _bank_valid() {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["bank_code"],"require"=>"true","message"=>'银行卡号不能为空'),
            array("input"=>$_POST["bank_name"],"require"=>"true","message"=>'银行卡类型不能为空'),
            array("input"=>$_POST["bank_validity"],"require"=>"true","message"=>'有效期不能为空'),
           // array("input"=>$_POST["bank_country"],"require"=>"true","message"=>'所属国家不能为空'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }
		
		
		$where = array(
			'bank_code' => $_POST['bank_code'],
			'member_id' => $this->member_info['member_id']			
		);
	
		$count = Model('member_bank')->getMemberBankCount($where);
		
		if($count > 0){
			output_error('已存在相同银行卡');
		}

        $data = array();
        $data['member_id'] = $this->member_info['member_id'];
        $data['bank_code'] = $_POST['bank_code'];
        $data['bank_name'] = $_POST['bank_name'];
        $data['bank_cvv'] = $_POST['bank_cvv'];
        $data['bank_validity'] = $_POST['bank_validity'];
      //  $data['bank_country'] = $_POST['bank_country'];
		$data['is_default'] = $_POST['is_default'] == 1 ? 1 : 0;
		
		
		
		
        return $data;
    }

}
