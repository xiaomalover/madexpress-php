<?php
/**
 * 我的反馈
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_feedbackControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加反馈
     */
    public function feedback_addOp() {
		
        $model_feedback = Model('feedback');
       	
       	$param = array();
        $param['feedback_content'] = $_POST['feedback_content'];
        $param['user_type'] = 1;
		$param['feedback_type'] = $_POST['feedback_type'];		
		$param['feedback_type_id'] = $_POST['feedback_type_id'];		
        $param['feedback_time'] = TIMESTAMP;
        $param['user_id']   = $this->member_info['member_id'];
        $param['user_name'] = $this->member_info['member_name'];
		$param['feedback_file'] = $_POST['feedback_file'];	
		$param['feedback_code'] = $this->feedbackCode();
		$param['feedback_order_id'] = $_POST['order_id'];


        $result = $model_feedback->addFeedback($param);

        if($result) {
            output_data('反馈提交成功');
        } else {
            output_error('保存失败');
        }
    }
	
	private function feedbackCode(){
		
		$length = 7;
		$count = model('feedback')->getFeedBackCount(TRUE);
        $len = $length - strlen($count+1);
        $code = strtoupper('MEO').str_pad($count+1,$len,0,STR_PAD_LEFT);
        return $code;
	   		
	}




	//反馈列表
	public function feedback_listOp(){
	    
	    $model_feedback = Model('feedback');
	    $condition = array();
	
	    $condition['user_id'] = $this->member_info['member_id'];    
		$condition['user_type'] = 1;
		
		$list = $model_feedback->getFeedBackList($condition, $this->page);	
		
		
		$page_count = $model_feedback->gettotalpage();
		$list_count = $model_feedback->gettotalnum();
		
		$data = array();	
		foreach($list as $k => $v){

			$v['feedback_time'] = date('d/m/y H:i',$v['feedback_time']);
			$v['updatetime'] = date('d/m/y H:i',$v['updatetime']);
			$v['last_reply_time'] = date('d/m/y H:i',$v['last_reply_time']);


			$data[$k] = $v;
		}
		output_data(array('list' => $data), mobile_page($page_count,$list_count));
	}
	
	
	//反馈详情
	
	public function feedback_infoOp(){
	    
	    $model_feedback = Model('feedback');
	    
	    $where = array();
	    $where['user_id'] = $this->member_info['member_id'];
	    $where['user_type'] = 1;
	    $where['id'] = $_POST['feedback_id'];
        $info = $model_feedback->getFeedBackInfo($where);
        
        if(!empty($info)){
            $info['feedback_file'] = $this->feedback_image($info['feedback_file']);
            $info['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
        
	        $info['feedback_time'] = date('d/m/y H:i',$info['feedback_time']);
	        
	   
	    //
            $log_list = array();
            $log = $model_feedback->getFeedBackListLog(array('feedback_id' => $info['id']));
            
        
            foreach($log as $v){
                $v['log_file'] =  $this->feedback_image($v['log_file']);
                $v['log_addtime'] =  date('d/m/y H:i',$v['log_addtime']);
                $v['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
                $log_list[] = $v;
                
            }
            
            
            $info['log'] = $log_list;
            
            output_data(array('info' => $info));    
        }else{
            output_error('未获取到数据');
        }
	    
	}
	
		//格式化图片
	private function feedback_image($image){		
		$list = array();
		if(!empty($image)){			
			$image  =	explode(",", $image);			
			foreach($image as $v){				
				$list[] = UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.$v;
			}			
			return $list;			
		}else{
			return $list;			
		}
	}
	
	private function getMemberAvatar($image){
		if(file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !=''){
			return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
		}else{
			return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
		}
	}
	
	public function feedback_typeOp(){
	        $model_feedback = Model('feedback');
	        $type_list = $model_feedback->getFeedBackType(array('type_user' => 1,'type_parent_id' => 0));
	        foreach($type_list as $v){
	            $v['child'] = $model_feedback->getFeedBackType(array('type_parent_id' => $v['type_id']));
	            $type[] =  $v;
	        }
			output_data($type);
	}
	


	//继续反馈
	public function feedback_add_logOp(){
	      $model_feedback = Model('feedback');
	    
	       $data = array();
	       $data['log_content'] = $_POST['log_content'];
	       $data['log_file'] = $_POST['log_file'];
	       $data['feedback_id'] = $_POST['feedback_id'];
	       
	       $data['user_id'] = $this->member_info['member_id'];
	       $data['user_name'] = $this->member_info['member_truename'];
	       $row = $model_feedback->addFeedBackLog($data);
	    
	     if($row){
            output_data('添加成功');    
        }else{
            output_error('未获取到数据');
        }
	    
	    
	    
	    
	}
	
	
	
	
	
	
	
}
