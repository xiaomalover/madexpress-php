<?php
/**
 * 我的消息
 */

defined('InMadExpress') or exit('Access Invalid!');
class member_messageControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
        $this->model_msg = Model('member_msg');
    }
	
	
    /**
     * 消息列表
     */
    public function indexOp() {
		
		$where = array('member_id'=>$this->member_info['member_id']);
		$where['msg_readids'] = $_POST['msg_read'];
		
		
        $data = $this->model_msg->getMemberMsgList($where,'*',$this->page);	
		$page_count = $this->model_msg->gettotalpage();
		$list_count = $this->model_msg->gettotalnum();
		
		
	
		$msg_list = array();
		
		
		foreach($data as $k => $v){
			$msg_list[$k]['msg_id'] = $v['msg_id'];
			$msg_list[$k]['msg_code'] = $v['msg_code'];
			$msg_list[$k]['msg_code_name'] = $this->getLangName($v['msg_code_name']);
			$msg_list[$k]['msg_readids'] = $v['msg_readids'];	
			$msg_list[$k]['msg_content'] = $this->getLangContent($v['msg_content']);			
			$msg_list[$k]['msg_addtime'] = date('d/m/y H:i',$v['msg_addtime']);
			$msg_list[$k]['msg_url'] = $v['msg_url'] ? $v['msg_url'] : '' ;			
			$msg_list[$k]['msg_data_id'] = $v['msg_data_id'] > 0 ?  $v['msg_data_id'] : 0 ;			
		}
		
		output_data(array('msg_list' => $msg_list), mobile_page($page_count,$list_count));
		
    }
	
	
	
	//根据当前系统语言反馈消息中英文
	
	private function getLangName($data){
		
		$lang = $this->member_info['member_language'];		
		$data = unserialize($data);
		
		foreach($data as $v){			
			$msg[$v['lang_name']] = $v['mmt_name'];
		}		
		return $msg[$lang];		
		
		
	}
	
	private function getLangContent($data){
		
		$lang = $this->member_info['member_language'];
		$data = unserialize($data);		
		foreach($data as $v){			
			$msg[$v['lang_name']] = $v['content'];
		}		
		return $msg[$lang];		
		
	}
	
	
	
	
	
	
	
	
	//
	public function message_countOp(){
	    
	    $where = array();
	    $where['member_id'] = $this->member_info['member_id'];
	    $where['msg_readids'] = 0;
	    
	    $data = $this->model_msg->getMemberMsgCount($where);	
	    
	    output_data(array('count'=>$data));
	    
	}
	
	
	//消息详情
	public function message_infoOp(){
		
		$where = array();
		$where['member_id'] = $this->member_info['member_id'];
		$where['msg_id'] = $_POST['msg_id'];
		
		$data = $this->model_msg->getMemberMsgInfo($where);	
	   
	    
	    output_data(array('info'=>$data));
		
			
		
	}
	
	
	//全部已读
	public function all_stateOp(){
		
        $where = array();
        $where['member_id'] = $this->member_info['member_id'];
        if($_POST['msg_id'] > 0){
            $where['msg_id'] = $_POST['msg_id'];
        }
        $update = array();
        $update['msg_readids'] = 1;
        
		$row = $this->model_msg->editMemberMsg($where,$update);
		
		if($row){
		    output_data('操作成功');
		}else{
		    output_error('操作失败');
		}
		
		
	}
	
	
	public function delMsgOp(){
		$where = array(
			'msg_id' => $_POST['msg_id'],
			'member_id' => $this->member_info['member_id']
		);
		$result = $this->model_msg->delMemberMsg($where);
		if($result){
			output_data('ok');			
		}else{
			output_data('删除失败');	
		}
		
	}
	
	
	
	


}
