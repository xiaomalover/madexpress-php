<?php
/**
 * 账户充值 
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_payment_rechargeControl extends mobileMemberControl {

    private $payment_code;

    private $payment_config;


	public function __construct(){
		parent::__construct();
		
		$this->payment_code = 'stripe';
		
	}

	//扫码页面
	public function indexOp(){
		
		$data = array();
		$data['member_code'] = $this->member_info['member_code'];
		$data['member_wallet'] = $this->member_info['available_predeposit'];		
		output_data($data);		
		
	}
	
	
	public  function  tipsOp(){
	    
	    $data = array();
	    $data['content'] = '这里后台编辑内容';
	    
	    output_data($data);
	    
	}
	

    /**
     * 账户
     */
    public function pd_payOp() {
        $pay_sn = $_POST['pay_sn'];      
        $model_order= Model('predeposit');
        $pd_info = $model_order->getPdRechargeInfo(array('pdr_sn'=>$pay_sn,'pdr_member_id'=>$this->member_info['member_id']));
        if(empty($pd_info)){
            output_error('收款单不存在!');
            exit();
        }
        if (intval($pd_info['pdr_payment_state'])) {
            output_error('您的订单已经支付，请勿重复支付!');
            exit();
        }

       $this->_api_pay_pd(array('pay_id'=>$pd_info['pdr_id'],'pay_sn'=>$pd_info['pdr_sn'],'pay_amount'=>$pd_info['pdr_amount']));

    }

    /**
     * 第三方在线支付接口
     *
     */
    private function _api_pay_pd($order_info) {
		
	//	print_r($order_info);
		$model_pd = Model('predeposit');
		$member_money = $this->member_info['available_predeposit'];		
		
		if($_POST['bank_id'] > 0){
	    	$bank_info = Model('member_bank') -> getMemberBankInfo(array('bank_id' => $_POST['bank_id']));
	    	if(empty($bank_info)){
	    	    output_error('银行卡不存在');
	    	}
		}
		
		
		
	//	print_r($member_money);
		if($member_money >= $order_info['pay_amount']){ //钱包够了直接扣除钱包的，订单完成
		
								$data_pd = array();
			                    $data_pd['member_id'] = $this->member_info['member_id'];
			                    $data_pd['member_name'] = $this->member_info['member_name'];
			                    $data_pd['amount'] = $order_info['pay_amount'];
			                    $data_pd['order_sn'] = $order_info['pay_sn'];
			                    $data_pd['order_id'] = $order_info['pay_id'];
			                    
			                 //   print_r($data_pd);
								$insert = $model_pd->changePd('offline_payment',$data_pd);
						        if($insert) { //扣款成功									
								
									$data = array();
									$data['pdr_payment_type'] = 0;
									$data['pdr_wallet_amount'] = $order_info['pay_amount'];
									$data['pdr_payment_state'] = 1;
									$data['pdr_payment_time'] = time();
									
									$where = array();
									$where['pdr_sn'] = $order_info['pay_sn'];									
									$row = $model_pd->editPdRecharge($data,$where);
									if($row){
										
										$order_info = array();
										$model = model('stoket');			
										$order_info = $model_pd->getPdRechargeInfo(array('pdr_sn'=>$order_info['pay_sn'],'pdr_member_id'=>$this->member_info['member_id']));										
										$data = array();
										$data['type'] = 'payment_notice';
										$data['data'] = $order_info;		
										$data = json_encode($data);	
										$model->sendStore($order_info['store_id'],$data);
										
										output_data('付款成功');											
									}else{										
										output_error('付款失败');
									}
								 
						        }else{
									output_error('扣款失败');
								}
							    								
			
		}elseif($member_money <= $order_info['pay_amount'] && $member_money > 0){ //钱包的钱不够，有多少冻结多少。然后剩余的走stripe支付
						
			
			if(!$_POST['stripeToken']){
			    output_error('缺少stripe参数');
			}
									
			//先冻结款项
			$data_pd = array();
			$data_pd['member_id'] = $this->member_info['member_id'];
			$data_pd['member_name'] = $this->member_info['member_name'];
			$data_pd['amount'] = $member_money;
			$data_pd['order_sn'] = $order_info['pay_sn'];
			$data_pd['order_id'] = $order_info['pay_id'];
			$insert = $model_pd->changePd('offline_payment_freeze',$data_pd);			
			if($insert){
				
				$data = array();			
				$data['pdr_wallet_amount'] = $member_money;
				$data['pdr_stripe_amount'] = $order_info['pay_amount'] - $member_money;
				$data['pdr_payment_type'] = 2;				
				$data['pdr_bank_id']   = $bank_info['bank_id'];
				$data['pdr_bank_code'] = $bank_info['bank_code'];
				$data['pdr_bank_name'] = $bank_info['bank_name'];
				$where = array();
				$where['pdr_sn'] = $order_info['pay_sn'];									
				$row = $model_pd->editPdRecharge($data,$where);
				if($row){
					$this->_api_pay($order_info);									
				}else{
					output_error('付款失败');				
				}				
			}else{
				output_error('付款失败');
			}
			
		}else{		
		    
		    if(!$_POST['stripeToken']){
			    output_error('缺少stripe参数');
			}
		    
			$data = array();
			$data['pdr_stripe_amount'] = $order_info['pay_amount'];
			$data['pdr_payment_type'] = 1;				
			$data['pdr_bank_id'] = $bank_info['bank_id'];
			$data['pdr_bank_code'] = $bank_info['bank_code'];
			$data['pdr_bank_name'] = $bank_info['bank_name'];
			$where = array();
			$where['pdr_sn'] = $order_info['pay_sn'];									
			$row = $model_pd->editPdRecharge($data,$where);
			if($row){
				$this->_api_pay($order_info);									
			}else{
				output_error('付款失败');				
			}	
			
					
		}
		
    }
	
	
	 /**
	     * 第三方在线支付接口
	     *
	     */
	    private function _api_pay($order_info) {
	        
			$model_pd = Model('predeposit');				
			$order_info = $model_pd->getPdRechargeInfo(array('pdr_sn'=>$order_info['pay_sn'],'pdr_member_id'=>$this->member_info['member_id']));
			
			$inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';		
	        if(!is_file($inc_file)){
	            output_error('支付接口不存在');
	        }
			//print_r($order_info);
	        require($inc_file);	       
	        // wxpay_jsapi
	         if ($this->payment_code == 'stripe') {			
				// Set your secret key: remember to change this to your live secret key in production
				// See your keys here: https://dashboard.stripe.com/account/apikeys				
				
				//通过前端传值来判断
				//正常通道		
						
			 	\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');
				$token = $_POST['stripeToken'];
				if($token == ''){				
					output_error('支付卡token错误');		
				}			 
				$charge = \Stripe\Charge::create([
					'amount' => $order_info['pdr_stripe_amount'] * 100,
					'currency' => 'usd',
					'description' => '支付订单号：'.$order_info['pdr_sn'],
					'source' => $token,
				]);				
				
			//	print_r($charge);
				if($charge['status'] == 'succeeded'){		
				
					if($order_info['pdr_payment_type'] == 2){
													
						$data_pd = array();
						$data_pd['member_id'] = $this->member_info['member_id'];
						$data_pd['member_name'] = $this->member_info['member_name'];
						$data_pd['amount'] = $order_info['pdr_wallet_amount'];
						$data_pd['order_sn'] = $order_info['pdr_sn'];
						$data_pd['order_id'] = $order_info['pdr_id'];
						$model_pd->changePd('offline_payment_thawfreeze',$data_pd);				
				
					}
				
					$data = array();
					$data['pdr_payment_time'] = time();
					$data['pdr_payment_state'] = 1;	
					$data['pdr_trade_sn'] = $charge['id'];				
					$where = array();
					$where['pdr_sn'] = $order_info['pdr_sn'];									
					$row = $model_pd->editPdRecharge($data,$where);
					if($row){
						
					
						$model = model('stoket');			
						$order_info = $model_pd->getPdRechargeInfo(array('pdr_sn'=>$order_info['pdr_sn'],'pdr_member_id'=>$this->member_info['member_id']));					
						
						$data = array();
						$data['type'] = 'payment_notice';
						$data['data'] = $order_info;		
						$data = json_encode($data);	
						$model->sendStore($order_info['store_id'],$data);
						
						
						output_data('付款成功');					
					}else{
						output_error('付款失败');				
					}	
				}else{				
					output_error($charge['error']);				
				}
				
	        }
			
	    }
	
    
}
