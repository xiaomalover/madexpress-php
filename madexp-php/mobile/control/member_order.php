<?php
/**
 * 我的订单
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_orderControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 订单列表
     */
	 
	
	 
    public function orderListOp() {
		
        $model_order = Model('order');

        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];    
        
        if($_POST['state'] == 0){
            $condition['order_state'] =  array('in',array(20,30,40,50));  	
        }else{
		    $condition['order_state'] = array('in',array(0,60));  
        }
		
		if ($_POST['keyword']) {
       
            $condition['order_sn|store_name|store_phone|buyer_name|buyer_phone'] = array('LIKE','%'. $_POST['keyword'].'%');
       
        
            $keyword_ids = $this->_getOrderIdByKeyword($_POST['keyword']);
            if($keyword_ids){
              $condition['order_id'] = array('in',$this->_getOrderIdByKeyword($_POST['keyword']));
            }
        }
		
		$query_start_price  = $_POST['query_start_price'];
		$query_end_price 	= $_POST['query_end_price'];
		
		$price_from = preg_match('/^[\d.]{1,20}$/',$query_start_price) ?$query_start_price : null;		
		$price_to = preg_match('/^[\d.]{1,20}$/',$query_end_price) ? $query_end_price : null;
		
		if ($price_from && $price_to) {
		    $condition['order_amount'] = array('between',"{$price_from},{$price_to}");
		} elseif ($price_from) {
		    $condition['order_amount'] = array('egt',$price_from);
		} elseif ($price_to) {
		    $condition['order_amount'] = array('elt',$price_to);
		}
		
		$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_POST['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_POST['query_end_date']): null;
     
	    if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
		
		//筛选评价
		if(intval($_POST['score']) > 0 ){
			$condition['evaluate_store_score'] = $_POST['score'];
		}
				
		
	
        $order_list = $model_order->getOrderList($condition, 50, '*', 'order_id desc');
        
    //    print_r($order_list);
     	$state = $this->_orderState();
     	
     	$member_order_list = array();
		foreach($order_list as  $k => $v){
		    
			$store =  $this->_getOrderStoreId($v['store_id']);	
		   
		   
			$member_order_list[$k]['store_name_primary'] = 	$store['store_name_primary'];
            $member_order_list[$k]['store_name_secondary'] = 	$store['store_name_secondary'];	
            $member_order_list[$k]['order_id'] = 	$v['order_id'];
            $member_order_list[$k]['order_sn'] = 	$v['order_sn'];
            $member_order_list[$k]['pay_amount'] = 	$v['pay_amount'];
            $member_order_list[$k]['order_amount'] = 	$v['order_amount']; 
			$member_order_list[$k]['order_state'] =$v['order_state'];
			$member_order_list[$k]['goods'] = 	$this->_getOrderGoods($v['order_id']);			
			$member_order_list[$k]['store_address'] = 	$v['store_address']; 
			$member_order_list[$k]['evaluate'] = $this->_getOrderEva($v['order_id']);
			$member_order_list[$k]['order_date'] =  date('Y-m',$v['add_time']);
			$member_order_list[$k]['add_time'] = $v['add_time'];
			$member_order_list[$k]['order_type'] = $v['order_type'];
		
		}    
        output_data(array('order_list' => $member_order_list));
    }
	
	
	public function serachorderListNewOp(){
	
	  $model_order = Model('order');

		 //搜索
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];  
		$condition['order_state'] =  array('in',array(50,60));  
        if (preg_match('/^\d{10,20}$/',$_POST['keyword'])) {
            $condition['order_sn'] = $_POST['keyword'];
        } elseif ($_GET['keyword'] != '') {
            $condition['order_id'] = array('in',$this->_getOrderIdByKeyword($_POST['keyword']));
        }
		
        if (preg_match('/^\d{10,20}$/',$_POST['pay_sn'])) {
            $condition['pay_sn'] = $_POST['pay_sn'];
        }
		
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_POST['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_POST['query_end_date']): null;
     
	    if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
		
		
		
		$query_start_price  = $_POST['query_start_price'];
		$query_end_price 	= $_POST['query_end_price'];
		
		$price_from = preg_match('/^[\d.]{1,20}$/',$query_start_price) ?$query_start_price : null;		
		$price_to = preg_match('/^[\d.]{1,20}$/',$query_end_price) ? $query_end_price : null;
		
		if ($price_from && $price_to) {
		    $condition['order_amount'] = array('between',"{$price_from},{$price_to}");
		} elseif ($price_from) {
		    $condition['order_amount'] = array('egt',$price_from);
		} elseif ($price_to) {
		    $condition['order_amount'] = array('elt',$price_to);
		}
		
				
	  	$order_list = $model_order->getOrderList($condition, $this->page, '*', 'add_time desc');
		$order_data = array();
		foreach($order_list as $k => $v){
			$year = date('Y-m',$v['add_time']);
			$order_list = $v;
			$order_list['order_state'] = $v['order_state'];
			$store =  $this->_getOrderStoreId($v['store_id']);			
			$order_list[$k]['store_name_primary'] = 	$store['store_name_primary'];
            $order_list[$k]['store_name_secondary'] = 	$store['store_name_secondary'];	
			$order_list['goods'] = 	$this->_getOrderGoods($v['order_id']);		
			$order_list['evaluate'] = $this->_getOrderEva($v['order_id']);
			$order_data[$year][] = $order_list;
		}
		
		  
		 $page_count = $model_order->gettotalpage();
		 output_data(array('order_list' => $order_data), mobile_page($page_count));
		
		
	}
	
	
	public function serachorderListOp(){
	
	  $model_order = Model('order');

		 //搜索
        $condition = array();
        $condition['buyer_id'] = $this->member_info['member_id'];  
		$condition['order_state'] =  array('in',array(50,60));  
        if (preg_match('/^\d{10,20}$/',$_POST['keyword'])) {
            $condition['order_sn'] = $_POST['keyword'];
        } elseif ($_GET['keyword'] != '') {
            $condition['order_id'] = array('in',$this->_getOrderIdByKeyword($_POST['keyword']));
        }
		
        if (preg_match('/^\d{10,20}$/',$_POST['pay_sn'])) {
            $condition['pay_sn'] = $_POST['pay_sn'];
        }
		
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_POST['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_POST['query_end_date']): null;
     
	    if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
		
				
	  	$order_list = $model_order->getOrderList($condition, $this->page, '*', 'add_time desc');
		$order_data = array();
		foreach($order_list as $k => $v){
			$year = date('Y-m',$v['add_time']);
			$order_list = $v;
			$order_list['order_state'] = $v['order_state'];
			$store =  $this->_getOrderStoreId($v['store_id']);			
			$order_list['store_en_name'] = 	$store['store_en_name'];
			$order_list['goods'] = 	$this->_getOrderGoods($v['order_id']);		
			$order_list['evaluate'] = $this->_getOrderEva($v['order_id']);
			$order_data[$year][] = $order_list;
		}
		
		  
		 $page_count = $model_order->gettotalpage();
		 output_data(array('order_list' => $order_data), mobile_page($page_count));
		
		
		
		
	}
	
	 private function _getOrderIdByKeyword($keyword) {
        $goods_list = Model('order')->getOrderGoodsList(array('goods_name'=>array('like','%'.$keyword.'%')),'order_id',100,null,'', null,'order_id');
        return array_keys($goods_list);
    }
	
	

	private function _orderState(){
		
		$state = array(	
			'0' => '已取消',
			'10' => '待付款',			
			'20' => '疯狂寻找配送员',			
			'30' => '等待商家接单',			
			'40' => '商家备货中',												
			'50' => '配送中',												
			'60' => '已送达',
			
		);
		
		return $state;	
		
		
	}


	private function _getOrderGoods($order_id,$type='list'){
		    $order_goods_list = model('order')->getOrderGoodsList(array('order_id'=>$order_id));
			$order_goods = '';
			if (!empty($order_goods_list)) {
                foreach ($order_goods_list as $v) {
					$order_goods .= $v['goods_name'].' ';
                }
            } else {
                $order_goods = '';
            }
		return $order_goods;
	}

    private function _getOrderStoreId($store_id) {
        $store = Model('store')->getStoreInfo(array('store_id'=>$store_id));
        return $store;
    }

	private function _getOrderEva($order_id){
		
		$where = array(
			'store_evaluate_orderid' => $order_id
		);
	
		$eva = model('evaluate_store')->getEvaluateStoreInfo($where,'store_evaluate_score as score,store_evaluate_score_text as score_text');
		if(empty($eva)){
		    
		    	$eva = null;
		}
		
		
		return $eva;
		
	}


  /**
     * 取消订单
     */
    public function order_cancelOp() {
        
        $model_order = Model('order');
        $logic_order = Logic('order');
        $order_id = intval($_POST['order_id']);

        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['order_type'] = array('in',array(1,3));
        $order_info = $model_order->getOrderInfo($condition);	
        /*  
        $if_allow = $model_order->getOrderOperateState('buyer_cancel',$order_info);		
		
        if (!$if_allow) {
            output_error('无权操作');
        }
        */        
		$cancal_msg = $_POST['cancel_content'];
		
        $result = $logic_order->changeOrderStateCancel($order_info,'buyer', $this->member_info['member_name'], $cancal_msg);
        if(!$result['state']) {			
            output_error($result['msg']);			
        } else {


				
            //更新商家已取消订单数据
            $update = array();
            $update['order_state_quxiao'] = array('exp','order_state_quxiao + 1');
            model('store')->editStore($update,['store_id' => $order_info['store_id']]);


			
			//添加订单record
			$data_record = array();
			$data_record['order_id'] = $order_id;
			$record_data['order_cancel_time']       =  time(); //配送员编号
			$record_data['order_cancel_content']    =  $cancal_msg;//当前批次第几单
			$record_data['role']  = 'Customer'; //推送给配送员的时间
			model('order_record')->addRecord('order_cancel',$data_record);
			 
			 
			
			$pdf_model = model('pdf');
			$model = model('order');
			$wallet = model('predeposit'); 
			
			if($order_info['pd_amount'] > 0){
				//执行退款退回				
				$data = array();
				$data['amount'] = $order_info['pd_amount'] ;
				$data['order_sn'] = $order_info['order_sn'];
				$data['order_id'] = $order_info['order_id'];
				$data['member_id'] = $order_info['store_id'];
				$data['member_name'] = $order_info['store_name'];
				$wallet->changePd('order_refund',$data);								
			}	
			
			if($order_info['pay_amount'] > 0){
				
				


					
			
			}
			
			
			//生成退款记录单
			$refund_recode_data = array();
			$refund_recode_data['order_id'] 			= $order_info['order_id'];
			$refund_recode_data['refund_amount'] 		= $order_info['order_amount'];
			$refund_recode_data['refund_payment_amount']= $order_info['pay_amount'];
			$refund_recode_data['refund_wallet_amount'] = $order_info['pd_amount'];
			$refund_recode_data['voucher_re'] = 'No';	
			$refund_amount =  $order_info['order_amount'];		
			$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
			
					
		
				
			// 发送买家消息
			$param = array();
			$param['code'] = 'order_cancel';
			$param['member_id'] = $order_info['buyer_id'];
			$param['param'] = array(			
				'order_sn' => $order_info['order_sn'],
				'id' => $order_info['order_id']
			);
			QueueClient::push('sendMemberMsg', $param);
				
				
			
					$model_stoket=model('stoket');
					$data = array();
					$data['type'] = 'order_cancel'; //取消订单
					$data['data'] = array(
						'order_sn' => $order_info['order_sn']
					);
					// print_r($order_info);
					$data = json_encode($data);			 
					$model_stoket->sendDelivery($order_info['distributor_id'],$data);
					
					$data = array();
					$data['type'] = 'order_cancel'; //取消订单
					$data['data'] = array(
					'order_sn' => $order_info['order_sn']
					);
					// print_r($order_info);
					$data = json_encode($data);			 
					$model_stoket->sendStore($order_info['store_id'],$data);
					
					
					//判断当前骑手手里是否还有订单。如果有没有了。就直接推送从新接单的消息
					    
						$wheres = array(
            				'distributor_id' => $order_info['distributor_id'],
            				'order_state' => array('in','30,40,50')
            			);			
            			$list = model('order')->getOrderListNew($wheres);		
            			if(count($list)<=0){	
            			    
            			    
            			    
            		    		model('waiter')->editWaiter(['distributor_id' => $order_info['distributor_id']],['delivery_state' => 0]);	
            					$data = array();
            					$data['type'] = 'receiving_orders';
            					$data['data'] ='receiving_orders';
            					// print_r($order_info);
            					$data = json_encode($data);			 
            					$model_stoket->sendDelivery($order_info['distributor_id'],$data);
            				
            			}
            					
            					
					
					
				
				
				
				
				
            output_data('订单已取消');
        }
    }


    /**
     * 删除订单
     */
    public function order_deleteOp() {
        
        $model_order = Model('order');
        $logic_order = Logic('order');
        $order_id = intval($_POST['order_id']);
    
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['buyer_id'] = $this->member_info['member_id'];
        $condition['order_type'] = array('in',array(1,3));
        $order_info = $model_order->getOrderInfo($condition);
        
        $if_allow = $model_order->getOrderOperateState('delete',$order_info);
        
        if (!$if_allow) {
            output_error('无权操作');
        }

        $result = $logic_order->changeOrderStateRecycle($order_info,'buyer','delete');
        if(!$result['state']) {
            output_error($result['msg']);
        } else {
            output_data('1');
        }
    }

	//等待5分钟后。从新开始轮训
	public function order_polling_timeOp(){
		
		   $order_id = intval($_POST['order_id']);		
           output_data('ok');
		
	}



	public function order_infoOp(){
		
		
		$model_order =  model('order');
		$where = array(
			'buyer_id' => $this->member_info['member_id'],
			'order_id' => $_POST['order_id']
		);

	  $order = $model_order->getOrderInfo($where, array('order_common','order_goods','member','store','presale_refund','aftersale_refund','replace','goods_give','order_log'));
		
//	print_r($order);
		if (empty($order)) {
           output_error('订单不存在');
        }
		
	//	print_r($order);
	
	
	
	$order_item = array();	
	//从新格式化
				
	$order_user = array();
	$order_user['buyer_code'] = $order['buyer_code'];
	$order_user['buyer_name'] = $order['buyer_name'];
	$order_user['buyer_phone'] = $order['buyer_phone'];
	$order_user['buyer_comment'] = $order['buyer_comment'];
	//调用地址昵称
	
	$buyer_address = unserialize($order['member_address']);
	
	
	$order_user['buyer_address'] = empty($buyer_address) ? $order['buyer_address'] :$buyer_address['address_nickname'].' '. $buyer_address['addres'];
	$order_user['buyer_is_new'] = $order['buyer_is_new'];
	$order_user['state'] = $order['order_state'];
	$order_user['evaluation_state'] = $order['evaluation_state'];
	$order_user['buyer_coordinate'] = $order['buyer_coordinate'];
	
	if($order['evaluation_state'] == 1) {			
		$model = model('evaluate_store');		
		$where = array(			
			'store_evaluate_orderid' => $order['order_id']
		);		
		$eva = $model->getEvaluateStoreInfo($where);	
		$order['evaluation_text'] = $eva['store_evaluate_score_text'];
		$order['evaluation_content'] = $eva['store_evaluate_score_content'];	
	}
	
				
	$order_item['order_user'] = $order_user;
	
	//商家信息
	$order_store = array();
	$store = $order['extend_store'];	
	
	$order_store['store_id']	 =  $store['store_id'];
	$order_store['store_name_primary']	 =  $store['store_name_primary'];
	$order_store['store_name_secondary']	 =  $store['store_name_secondary'];
	$order_store['store_code']	 =  $store['store_code'];
	$order_store['store_address']	 =  $store['store_address'];
	$order_store['store_avatar']	 =  $store['store_avatar'];				
	$order_store['store_phone']	 =  $store['store_phone'];
	$order_store['store_score']	 =  $store['store_score'];			
	$order_store['store_lat']	 =  $store['store_lat'];	
	$order_store['store_lng']	 =  $store['store_lng'];	
	$order_item['order_store']		 = $order_store;
			
			
			
	$order_delivery = array();

	$order_delivery['distributor_id']	 		=  $order['distributor_id'];
	$order_delivery['distributor_name']	 		=  $order['distributor_name'];
	$order_delivery['distributor_mobile']	 	=  $order['distributor_mobile'];
	$order_delivery['distributor_start_time']	=  $order['distributor_start_time'];
	$order_delivery['distributor_end_time']	 	=  $order['distributor_end_time'];
	$order_delivery['order_estimate_time']	 	=  $order['order_estimate_time'];				
	$order_delivery['distributor_code']			=  $order['distributor_code'];
	$order_delivery['distributor_duration']	 	=  $order['distributor_duration'];			
	$order_delivery['distributor_coordinate']	=  $order['distributor_coordinate'];		
	
	$waiter_info = model('waiter')->getWaiterInfo(array('distributor_id' => $order['distributor_id']));	
	$order_delivery['distributor_avatar'] = getWaiterAvatar($waiter_info['distributor_avatar']);
	
//	$order_delivery['store_lng']	 =  $order['store_lng'];	

	$order_item['order_delivery']		 = $order_delivery;
	
	
		
	//订单商品,原始商品，不做任何修改
	$order_goods = array();		
	$goods =  $order['extend_order_goods'];		
	$goods_num = 0;
	foreach($goods as $k => $v){			
		$goods_num += $v['goods_num'];	
	}
	
	$order_goods['goods_list'] = $order['extend_order_goods'];				
	$order_goods['goods_num'] = $goods_num;		
	$order_item['order_goods'] = $order_goods;
	
	
	
	
	//金额信息
	$order_amount = array();		
	//商品小计
	$order_amount['goods_amount'] = ($order['goods_amount']);		
	//商品折扣
	$order_amount['sales_amount'] = ($order['sales_amount']);		
	//餐盒费
	$order_amount['foodbox_amount'] = ($order['foodbox_amount']);
	//配送费
	$order_amount['delivery_amount'] = ($order['delivery_fee']);
	//支付手续费	
	
	$order_amount['pay_commission_amount'] = ($order['pay_commission_amount']);		
	
	//平台抽成
	$order_amount['commission_amount'] = ($order['commission_amount']);		
	//订单价格		
	$order_amount['order_amount'] = ($order['order_amount']);			
	//商家优惠券
	$order_amount['store_coupon_price'] = $order['store_coupon_id'] > 0 ? ($order['store_coupon_price']) : '0.00';		
	//平台优惠券
	$order_amount['platform_coupon_price']  = $order['platform_coupon_price'] > 0 ? ($order['platform_coupon_price']) : '0.00';	
	//订单实付		
	$order_amount['pay_order_amount'] = ($order['order_amount']);					
	
	$order_item['order_amount'] = ($order_amount);
	
	
	
	
	/*
	
        //一阶段退款
        $one_stage_refund = array();
        $refund = $order['extend_one_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        $one_stage_refund['goods_num'] 	= 	$goods_num;
        $one_stage_refund['goods_amount'] = $goods_amount;
        $one_stage_refund['goods_list'] = $order['extend_one_stage_refund'];
        $order_item['one_stage_refund'] = $one_stage_refund;
                
        
        //二阶段退款
        $two_stage_refund = array();
        $refund = $order['extend_two_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
                
        $two_stage_refund['goods_num'] = 	$goods_num;
        $two_stage_refund['goods_amount'] = 	$goods_amount;
        $two_stage_refund['goods_list'] = $order['extend_two_stage_refund'];
        $order_item['two_stage_refund'] = $two_stage_refund;
        

		$order_item['order_presale_refund'] = '';



        
        //三阶段退款
        $three_stage_refund = array();
        $refund = $order['extend_three_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        $three_stage_refund['goods_num']    = 	$goods_num;
        $three_stage_refund['goods_amount'] = 	$goods_amount;
        $three_stage_refund['goods_list'] = $order['extend_three_stage_refund'];
        $order_item['three_stage_refund'] = $three_stage_refund;
        
	
		*/
	
      //售前
	  $presale_refund = array();
	  $refund = $order['extend_presale_refund'];
	  $goods_num = 0;
	  $goods_amount = 0;
	  if (!empty($refund)) {
		  foreach ($refund as $k => $v) {
			  $goods_num += $v['goods_num'];
			  $goods_amount +=$v['goods_num'] * $v['goods_price'];
		  }
	  }
	  $presale_refund['goods_num']    = 	$goods_num;
	  $presale_refund['goods_amount'] = 	$goods_amount;
	  $presale_refund['goods_list'] = $order['extend_presale_refund'];
	  $order_item['presale_refund'] = $presale_refund;
	  


		
      //售后
	  $aftersale_refund = array();
	  $refund = $order['extend_aftersale_refund'];
	  $goods_num = 0;
	  $goods_amount = 0;
	  if (!empty($refund)) {
		  foreach ($refund as $k => $v) {
			  $goods_num += $v['goods_num'];
			  $goods_amount +=$v['goods_num'] * $v['goods_price'];
		  }
	  }
	  $aftersale_refund['goods_num']    = 	$goods_num;
	  $aftersale_refund['goods_amount'] = 	$goods_amount;
	  $aftersale_refund['goods_list'] = $order['extend_aftersale_refund'];
	  $order_item['aftersale_refund'] = $aftersale_refund;
	  



    	
    	
    //替换商品
        $order_replace = array();
        $where = array();
        $where['order_id'] = $order['order_id'];
        $replace =  model('order_goods_replace')->getGoodsList($where);//$order['extend_order_goods_replace'];
        
        $replace_ids = array();
        foreach ($replace as $v) {
            $replace_ids[] = $v['rec_id'];
        }
        
        $replace_ids = 	implode(',', $replace_ids);
        
        $goods_num = 0;
        $goods_amount = 0;
		$replace_list = array();
        if (!empty($replace)) {
            $replace_data = $model_order->getOrderGoodsList(array('rec_id'=> array('in',$replace_ids)));
            foreach ($replace_data as $k => $v) {
                $v['goods_optional'] =  $v['goods_optional'] !='' ? unserialize($v['goods_optional']) : array();
                $replace_list[$k] = $v;
                $replace_list[$k]['replace_goods'] =  $this->order_replace_goods($v['rec_id']);
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        
        $order_replace['goods_num'] 	= 	$goods_num;
        $order_replace['goods_amount'] = 	$goods_amount;
        $order_replace['goods_list'] = $replace_list;
        $order_item['order_replace'] = $order_replace;
        
    	
	
    	//商家赠送商品
    	$order_goods_give = array();
    	$give = $order['extend_order_goods_give'];
    	$goods_num = 0;
    	$goods_amount = 0;
    	if(!empty($give)){
    		foreach($give as $k => $v){			
    			$goods_num += $v['goods_num'];	
    			$goods_amount +=$v['goods_num'] * $v['goods_price'];
    		}
    	}
    	
    	$order_goods_give['goods_num'] = 	$goods_num;	
    	$order_goods_give['goods_amount'] = $goods_amount;			
    	$order_goods_give['goods_list'] = count($give) > 0 ? $give : array()  ;
    	$order_item['order_give'] = $order_goods_give;
    	
	
	
    	//商家赠送的优惠券
		$order_coupon_give = array();
		
		//获取赠送的优惠券列表。
		$order_coupon_list = model('order_coupon')->getCouponList(['order_id' => $order['order_id']]);


    	//$order_coupon_give['order_coupon_give_id']		 = $order['store_coupon_give_id'];
    //	$order_coupon_give['order_coupon_give_price']	 = $order['store_coupon_give_price'];
    	//$order_coupon_give['order_coupon_give_name']	 = $order['store_coupon_give_name'];		
    	$order_item['order_coupon_give'] = $order_coupon_list;
    	
		
	
    	//订单信息
    	$order_info = array();
    	$order_info['order_sn'] = $order['order_sn'];
    	$order_info['add_time'] = date('H:i:s d/m/y',$order['add_time']);
    	$order_info['server_msectime'] = $this->msectime(); //毫秒
    	$order_info['server_time'] = time() * 1000;//秒
    	$order_info['add_time_stamp'] = $order['add_time'] * 1000;
    //	$order_info['date_time'] = date('H:i:s d/m/y',time());//秒
    	
    	
    	
    	
    	$order_info['store_comment'] = $order['store_comment']?$order['store_comment']:'--';
    //	$order_info['payment_name'] = $order['payment_name'];		
    
        $order_info['store_buyer_distance'] = $order['store_buyer_distance'];
    	$order_info['distributor_store_distance'] = $order['distributor_store_distance'];
    	$order_info['evaluation_state'] = $order['evaluation_state'];
    	$order_info['renew_payment_time_date'] = $order['renew_payment_time_date'];
    	$order_info['payment_time_date'] = $order['payment_time_date'];
    	$order_info['countdown'] = 300;
    	$order_info['order_type'] = $order['order_type'];
    	$order_info['bank_id'] = $order['bank_id'];
    	$order_info['bank_code'] = $order['bank_code'];
    	$order_info['bank_name'] = $order['bank_name'];
    	$order_info['delivery_type'] = $order['delivery_type'];
    	$order_info['delivery_comment'] = $order['deliver_comment'];
    
    	
    	$order_item['order_info'] = $order_info;
    		
      
    		
		
    	//支付方式
    	$payment_info = array();
    	$payment_info['wallet_fee'] = $order['pd_amount'] > 0 ? ncPriceFormat($order['pd_amount']) : '0.00'; //余额支付
    	$payment_info['external_payment'] = array(
    		'bank_name' => $order['bank_name'],			
    		'bank_pay_fee' => ncPriceFormat($order['order_amount'] - $order['pd_amount'])
    	);
    	
    	$order_item['payment_info'] = $payment_info;
    	
	
    	//退款方式
    	$refund_info = array();
    	if($order['refund_state'] == 2){
        	$refund_info['wallet_fee'] = $order['pd_amount'] > 0 ? ncPriceFormat($order['pd_amount']) : '0.00'; //余额支付
        	$refund_info['external_payment'] = array(
        		'bank_name' => $order['bank_name'],			
        		'bank_pay_fee' => ncPriceFormat($order['order_amount'] - $order['pd_amount'])
        	);
    	}
    	
    	$order_item['refund_info'] = $refund_info;
    	
    	
    	
    	
    	//收入信息		
    	$order_income = array();
    	$order_income['commission_amount'] = $order['commission_amount'];	
    	$order_income['order_amount'] = $order['order_amount'];
    	$order_item['order_income'] = $order_income;
    	
	
    	//按钮信息
    	$order_btn = array();
    	$order_btn['state'] = $order['order_state'];
    	$order_btn['order_id'] = $order['order_id'];
    	$order_item['order_btn'] = $order_btn;
    	$order_item['state'] = $order['order_state'];
    	
    	
    //	print_r($order);
    	//订单LOG
    	$order_item['order_log'] = $order['extend_order_log'];
		
    	
    	if($order['evaluation_state'] == 1){		    
			$eva_model = model('evaluate_store');
			$evaluate = array();			
			$eva_info = $eva_model->getEvaluateStoreInfo(array('store_evaluate_orderid'=> $order['order_id']));
		
			if(!empty($eva_info)){
				$eva_info['store_evaluate_image'] = $this->order_evaluate_image($eva_info['store_evaluate_image']);
				$evaluate = $eva_info;	
			}			
			
				
			$order_item['evaluate'] = $evaluate;			
			
			
			$eva_model = model('evaluate_waiter');
			$evaluate = array();			
			$eva_info = $eva_model->getEvaluateWaiterInfo(array('waiter_evaluate_orderid'=> $order['order_id']));
					
			if(!empty($eva_info)){
				$eva_info['waiter_evaluate_image'] = $this->order_evaluate_image($eva_info['waiter_evaluate_image']);
				$evaluate_delivery = $eva_info;	
			}									
			
			$order_item['evaluate_delivery'] = $evaluate_delivery;
			
			
			
			
			
		}
	
	
	    output_data($order_item);
		
	}
	
	
	
    private function order_replace_goods($rec_id)
    {
        $list =    model('order_goods_replace')->getGoodsList(array('rec_id'=>$rec_id));
        $data = array();
        foreach ($list as $v) {
            $v['goods_optional'] =  $v['goods_optional'] !='' ? unserialize($v['goods_optional']) : array();
            $data[] = $v;
        }
        return $data;
    }
	
	
	private function msectime()
    {
        list($msec, $sec) = explode(' ', microtime());
        $msectime =  (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }
    	


        
	
		//格式化图片
	private function order_evaluate_image($image){		
		$list = array();
		
		if(!empty($image)){			
			$image  =	explode(",", $image);			
		    //print_r($image);
			foreach($image as $v){				
				$list[] = UPLOAD_SITE_URL.DS.ATTACH_EVALUATE.DS.$v;
			}			
			return $list;			
		}else{
			return $list;			
		}
	}
	
	
	
	//票据
	public function order_invoiceOp(){
		$where = array();
		$where['to_id'] = $this->member_info['member_id'];
		$where['to_type'] = 3;
		$where['order_id'] = $_POST['order_id'];
		$bill = model('bill')->getBillList($where);		
	    $data = array();
	   
		foreach($bill as $k => $v){			
			$data[$k]['name'] = $v['bill_type_name'];
			$data[$k]['url']  = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];			
		}	    
		
		
	    output_data(array('list' => $data));
	    
	    
	}
	
	
	
	
	//自提单触发完成操作
	public function scan_orderOp(){
	    
	    $model_order = model('order');
	    
	    $where = array();
	    $where['buyer_id'] = $this->member_info['member_id'];
	    $where['order_sn'] = $_POST['order_sn'];
	    $where['order_type'] = 2;
	    
	   
	    
	    $order_info = $model_order->getOrderInfo($where);		
	    
	    if(!$order_info){
	        output_error('订单不存在');
	    }
	    
	    $data = array();
	    $data['order_state'] = 60;
        $row = $model_order->editOrder($data,$where);	
	    if($row){
	        
	            	$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = 'user';
					$data['log_msg'] = '用户已自提';
					$data['log_user'] = $order_info['member_id'];
					$data['log_orderstate'] = 60;				
					$model_order->addOrderLog($data);
					
					output_data('自提完成');
	    }else{
	                output_error('自提失败');
	    }
	    
	    
	}
	
	
	
	/**/
	
	public function tiaoshiOp(){
		
		$model_order =  model('order');
		$order_id = $_POST['order_id'];
		$store_id = $_POST['store_id'];		
		$waiter_id = $_POST['waiter_id'];	
		$order_state = $_POST['order_state'];
		$where = array(
			'buyer_id' => $this->member_info['member_id'],
			'order_id' => $order_id
		);
		$order_info = $model_order->getOrderInfo($where,array('order_goods','order_common','store'));		
		if($order_id){			
			$data = array();
			if($order_state < 70){
				$data['order_state'] = $order_state;
			}
			if($order_state == 999){
				$data['evaluation_state'] = 1;
			}
			$condition = array();
			$condition['order_id'] = $order_info['order_id'];
			
			if($waiter_id > 0 && $order_state < 70 ){
				$model_waiter = Model('waiter');
				$where =array('distributor_id'=>$waiter_id);
				$waiter = $model_waiter->getWaiterInfo($where,$data);
				$data['distributor_id'] = $waiter['distributor_id'];
				$data['distributor_name'] = $waiter['distributor_name'];
				$data['distributor_mobile'] = $waiter['distributor_mobile'];
				if($order_state == 40){
					$data['distributor_start_time'] = time();	
				}
				if($order_state == 50){
					$data['distributor_end_time'] = time();					
				}	
			}			
			$row = $model_order->editOrder($data,$condition);			
			if($row){			 
				if($order_state < 70){
					if($order_state == 0  ){
						$log_role = 'seller';
						$log_msg = '商家已取消';
					}
					if($order_state == 10  ){
						$log_role = 'user';
						$log_msg = '用户已付款';
					}	
									
					if($order_state == 20  ){
						$log_role = 'seller';
						$log_msg = '商家已接单';
					}
					if($order_state == 30  ){
						$log_role = 'seller';
						$log_msg = '商家已出餐';
					}
					if($order_state == 40  ){
						$log_role = 'waiter';
						$log_msg = '配送员已取货';
					}
					if($order_state == 50  ){
						$log_role = 'waiter';
						$log_msg = '配送员已送达';
					}
					if($order_state == 60  ){
						$log_role = 'seller';
						$log_msg = '问题单';
					}
					
					$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = $log_role;
					$data['log_msg'] = $log_msg;
					$data['log_user'] = $order_info['store_id'];
					$data['log_orderstate'] = 20;				
					$model_order->addOrderLog($data);
				}
				output_data('ok');
				
			}
			
		}
	}
	
	
	
	
	
	
/*	//保存退款信息
	public  function  save_refundOp(){
		
		$model = model('order');
		$order_id 	= $_POST['order_id'];
		$refund_state = $_POST['refund_state'];
		
		$order_info = $model->getOrderInfoN(array('order_id'=>$order_id,'buyer_id' => $this->member_info['member_id'],));
		if(empty($order_info)){			
			output_error('订单参数错误');
		}
		
		if($order_info['refund_state'] == 2){
			output_error('此订单已退款');
		}
		
		
		
		//部分退款
		if($refund_state == 1){	
			$goods_ids = explode('|',$_POST['goods_ids']);
			foreach($goods_ids as $v){
				$goods = explode(',',$v);				
				$goods_info = $this->getOrderGoods($goods[0]);
				if($goods[1] > $goods_info['goods_num']){					
					output_error('退款商品数量超限');					
				}
				
				$goods_info['goods_num'] = $goods[1];
				$goods_info['state'] = 4;
				unset($goods_info['rec_id']);				
				$refund_goods[] = $goods_info;				
				$refund_amount += $goods_info['goods_price'] * $goods[1];
			
			}
		
		}elseif($refund_state == 2){
			
			$refund_amount = $order_info['order_amount'];
			
		}
		
		$where =  array(
			'order_id' => $order_id,
			'buyer_id' => $this->member_info['member_id']	
		);	
		
		$update = array();
		$update['refund_state'] 	= $refund_state;
		$update['refund_amount'] 	= $refund_amount;
		$update['refund_comment'] 	= $_POST['refund_comment'];
		$update['refund_reason'] 	= $_POST['refund_reason'];	
		
		if($refund_state == 2){
		
			$update['order_state'] = 0;
			
		   
		}
		$row = $model->editOrder($update,$where);
		if($row){
			if($refund_state == 1){				
			    $model->addOrderGoods($refund_goods);				
			}
			output_data('退款申请成功');
		}else{
			
			output_error('退款申请失败');
			
		}
		
		
		
		
		
	}*/
	
		//获取商品信息
	private function getOrderGoods($rec_id){
		
		$where = array();
		$where['rec_id'] = $rec_id;
		
		$goods_info = model('order')->getOrderGoodsInfo($where);
		return $goods_info;
	}
	
	
	
}
