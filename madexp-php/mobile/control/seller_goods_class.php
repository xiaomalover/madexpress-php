<?php
/**
 * 商家商品分类
 */



defined('InMadExpress') or exit('Access Invalid!');
class seller_goods_classControl extends mobileSellerControl{

    public function __construct() {
        parent::__construct();
    }

    public function indexOp() {
        $this->class_listOp();
    }

    /**
     * 返回商家商品分类列表
     */
  public function class_listOp() {
		
		$where = array(
			'store_id'=> $this->store_info['store_id']
		);		
		$store_class = model('store_goods_class')->getStoreGoodsClassList($where);
		$class_list = array();
	  	
	  
	  
	  
	  
	  
		foreach($store_class as $k=> $v){			
			$class_list[$k]['class_id'] = $v['stc_id'];
			$class_list[$k]['class_name'] = $v['stc_name'];
		//	$goods_data[$k]['goods_image'] = $this->goods_list_data($v['stc_id']);
		}	
        output_data(array('class_list' => $class_list));
    }

    
  public function classAddOp(){
	
	  
	  $model = model('store_goods_class');
	  $data= array();
	  $data['stc_name'] = $_POST['class_name'];
	  $data['store_id'] = $this->store_info['store_id'];
	  $data['stc_sort'] = $_POST['class_sort'];
	  
	  $count = $model->getStoreClassCount(array('stc_name'=> $_POST['class_name'],'store_id'=>$this->store_info['store_id']));
	  
	  if($count > 0){
		  	  output_error('该分类已存在');
	  }
	  
	  $row = model('store_goods_class')->addStoreGoodsClass($data);
	  if($row){
		  output_data(array('class_id'=> $row));
	  }else{
		  output_error('添加失败');
	  }
	  
	  
	  
  }
	
	
}
