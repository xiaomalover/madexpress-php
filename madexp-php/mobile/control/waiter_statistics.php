<?php
/**
 * 我的消息
 */

defined('InMadExpress') or exit('Access Invalid!');
class waiter_statisticsControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
      
    }
	
	
    /**
     * 消息列表
     */
    public function indexOp() {
		

		$data = array(
				'income' => $this->stat_order_income(),
				'number' =>	$this->stat_order(),			
				'time'	=> 0,
				'ranking' => 0
		);
		
		output_data($data);
    }
	

    //统计今日订单
    
    private function stat_order(){
        $where = array();
        $where['order_state'] = array('in','60');
        $where['distributor_id'] = $this->waiter_info['distributor_id'];
      	
		$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['start_time']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['end_time']);
        $start_unixtime = $if_start_date ? strtotime($_POST['start_time']) : strtotime(date('Y-m-d',time()));
        $end_unixtime = $if_end_date ? strtotime($_POST['end_time']): strtotime(date('Y-m-d',time())) +86400;
     
	    if ($start_unixtime || $end_unixtime) {
            $where['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
       
  
        $count = model('order')->getOrderCount($where);
        return $count;
        
        
    }
    
    //统计今日收入
    private function stat_order_income(){
        $where = array();
        $where['order_state'] = array('in','60');
        $where['distributor_id'] = $this->waiter_info['distributor_id'];
       
		$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['start_time']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['end_time']);
		   $start_unixtime = $if_start_date ? strtotime($_POST['start_time']) : strtotime(date('Y-m-d',time()));
		   $end_unixtime = $if_end_date ? strtotime($_POST['end_time']): strtotime(date('Y-m-d',time())) +86400;
            
     
	    if ($start_unixtime || $end_unixtime) {
            $where['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
       
       
       
        $income = model('order')->getOrderInfoN($where,'SUM(delivery_fee) as total');
        
        return $income['total'] ? $income['total'] : '0.00';
        
    }
    
    private function stat_online_time(){
        
        
        
    }

	
	
	
	
	


}
