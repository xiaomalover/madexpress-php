<?php
/**
 * 我的反馈
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_helpControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加反馈
     */
    public function indexOp() {
		
        	$article_model	= Model('article');
			
			$where= array(
				'ac_id' => 3
			);
			$data = $article_model->getArticleList($where);
			
		    $help_list  = array();
		    
			foreach($data as $k=> $v){
				$help_list[$k]['title'] = $v['article_title'];
				$help_list[$k]['content'] = $v['article_content'];
			}
			
			output_data(array('list'=>$help_list,'tel' => '10000'));
						
    }
}
