<?php
/**
 * 手机端首页控制 

 *
 */



defined('InMadExpress') or exit('Access Invalid!');
class indexControl extends mobileMemberControl{

    public function __construct() {
        parent::__construct();
    } 

    /**
     * 首页
     */
    public function indexOp() {
	
	    /*
		$data = array();
		//加载分类
		$model_class = Model('store_class');
        $cate_data = $model_class->getStoreClassList();
		
		foreach($cate_data as $k => $v){
			$cate_list[$k] = $v;
			$cate_list[$k]['name'] = $v['class_name'];		
			$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['class_icon'];
			
		}
		
		$data['list'] = $cate_list;				
		output_data($data);
		*/
		
		   $model_cate = model('category');	   
    	   $cate_data = $model_cate->getGoodsClassListByParentId(0);
    	   $list = array();
    	   foreach($cate_data as $k => $v){
    		   $list[$k]['class_id'] = $v['cate_id'];
    		   $list[$k]['class_name'] = $v['cate_name'];
    		   $list[$k]['class_sort'] = $v['cate_sort'];
    		   $list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];
    		   $list[$k]['class_enable'] = $v['cate_enable'];
    		    $list[$k]['name'] = $v['cate_name'];
    	   }
    	   $data['list'] = $list;
    	   output_data($data);
    	   
    	   
		
		
		
    }   
	
	
	//0519 新增获取分类的多语言
	
	
	
	
	
	
	
	
	//排序分类 1012
    public function get_sort_categoryOp(){		
    		
    		$class_id = intval($_POST['class_id']);
    		$model_cate = model('category');		
    		if($class_id > 0){
    			
    			$cate_data = $model_cate->getGoodsClassList(array('cate_parent_id' => $class_id ,'cate_level' => 2));
    			$cate_list = array();				
    			foreach($cate_data as $k => $v){
    				$cate_list[$k]['class_id']= $v['cate_id'];
    				$cate_list[$k]['class_name'] = $v['cate_name'];		
    				$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];					
    			}			
    			
    		}else{
    			output_error('参数错误');
    		}
    				
    		output_data(array('list'=>$cate_list));
    		
    	}
	
	
	
	/*
	public function get_categoryOp(){
		
		$level = $_POST['level'] ;
		$class_id = $_POST['class_id'];
		
		switch($level){
				case 0: 		
					$model_class = Model('store_class');
					$cate_data = $model_class->getStoreClassList();		
					foreach($cate_data as $k => $v){
						$cate_list[$k]['class_id'] = $v['class_id'];
						$cate_list[$k]['class_name'] = $v['class_name'];		
						$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['class_icon'];					
						//$cate_list[$k]['level'] = 0;
					}
			
				break;						
				case 1: 	
						
						
						if($class_id == 2){
							
								$cate_data = Model('store_small_class')->getStoreSmallClassList(array('class_id'=>$class_id));
								
								foreach($cate_data as $v){
									$small[$v['small_id']] = $v;	
								}	
								
								$member_bind_class = model('member_bind_class')->getMemberBindClassList(array('member_id'=>$this->member_info['member_id'],'class_id'=>$_POST['class_id']),'bind_sort asc');	
											
								if($_POST['class_id'] > 0 && count($member_bind_class) > 0){
									$cate_list = array();
									$cate_list[0]['class_id'] = 0;
									$cate_list[0]['class_name'] = '全部';
									$cate_list[0]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.'food_all@2x.png';
									
									foreach($member_bind_class as $k=> $v){						
										$cate_list[$k+1]['class_id']= $v['bind_class_id'];
										$cate_list[$k+1]['class_name'] = $v['bind_class_name'];
										$cate_list[$k+1]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$small[$v['bind_class_id']]['small_icon'];																		
										
									}					
									
								}else{				
									
									$model_small = Model('store_small_class');
									$where = array();
									$where['class_id'] = $_POST['class_id'];
									$cate_data = $model_small->getStoreSmallClassList($where);					
									$cate_list = array();
									
									$cate_list[0]['class_id'] = 0;
									$cate_list[0]['class_name'] = '全部';
									$cate_list[0]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.'food_all@2x.png';
											
									foreach($cate_data as $k => $v){
										$cate_list[$k+1]['class_id']= $v['small_id'];
										$cate_list[$k+1]['class_name'] = $v['small_name'];		
										$cate_list[$k+1]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['small_icon'];					
									}
									
								}
							
							
						}else{
						
							$model_cuisine = Model('cuisine');
							$where = array();
							if($_POST['class_id'] > 0 ){
								$where['class_id'] = $_POST['class_id'];
							}
							
							//获取下级是否有分类
								
							$cate_data = $model_cuisine->getCuisineList($where);						
							$cate_list = array();
							$cate_list[0]['class_id'] = 0;
							$cate_list[0]['class_name'] = '全部';
							$cate_list[0]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.'food_all@2x.png';
							foreach($cate_data as $k => $v){
							
								$cate_list[$k+1]['class_id']= $v['cuisine_id'];
								$cate_list[$k+1]['class_name'] = $v['cuisine_name'];		
								$cate_list[$k+1]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cuisine_icon'];					
								
								
							}
							
						
						}
						
						
						
						
						break;									
				case 2: 
				
				
					if($_POST['class_id'] > 0 ){
							$where['cuisine_id'] = $_POST['class_id'];
					}
					
					
					//获取现有的商铺分类
					$group_class = model('store_bind_class')->getStoreBindGroupClassList(array(),'class_id');
					foreach($group_class as $v){
						
						$examine = $this->getStoreState($v['store_id']);
						if($examine['store_examine'] == 2 && $examine['store_state'] > 0){
							$group[$v['class_id']] = $v['class_id'];
						}
						
						
					}
					
					
					
					
					$cate_data = Model('store_small_class')->getStoreSmallClassList(TRUE);
					foreach($cate_data as $v){
						$small[$v['small_id']] = $v;	
					}	
					
					
					
					
					$member_bind_class = model('member_bind_class')->getMemberBindClassList(array('member_id'=>$this->member_info['member_id'],'bind_cuisine_id'=>$_POST['class_id']),'bind_sort asc');	
								
								
					if($_POST['class_id'] > 0 && count($member_bind_class) > 0){
						$cate_list = array();
						foreach($member_bind_class as $k=> $v){						
						
							if(!empty($group[$v['bind_class_id']])){
								
								$cate_list[] = array(
									'class_id' => $v['bind_class_id'],
									'class_name' => $v['bind_class_name'],
									'class_icon' => UPLOAD_SITE_URL.'/icon/'.$small[$v['bind_class_id']]['small_icon']
								);
								
							/*	$cate_list[$k]['class_id']= $v['bind_class_id'];
								$cate_list[$k]['class_name'] = $v['bind_class_name'];
								$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$small[$v['bind_class_id']]['small_icon'];																						
							* /	
							}
							
						}					
						
					}else{					
						$model_small = Model('store_small_class');
						$where = array();
						if($_POST['class_id'] > 0 ){
							$where['cuisine_id'] = $_POST['class_id'];
						}else{
							$where['class_id'] = 1;
						}
						$cate_data = $model_small->getStoreSmallClassList($where);					
						$cate_list = array();
						foreach($cate_data as $k => $v){
							
							if(!empty($group[$v['small_id']])){
								
								$cate_list[] = array(
									'class_id' => $v['small_id'],
									'class_name' => $v['small_name'],
									'class_icon' => UPLOAD_SITE_URL.'/icon/'.$v['small_icon']
								);
								
							/*	$cate_list[$k]['class_id']= $v['bind_class_id'];
								$cate_list[$k]['class_name'] = $v['bind_class_name'];
								$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$small[$v['bind_class_id']]['small_icon'];																						
							* /	
							}
							
							/*$cate_list[$k]['class_id']= $v['small_id'];
							$cate_list[$k]['class_name'] = $v['small_name'];		
							$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['small_icon'];					* /
						}
						
					}
					
					
				break;	
		}		
		
		 output_data(array('list'=>$cate_list));
		
	}
	*/
	
	public function get_categoryOp(){
		
		$level = $_POST['level'] ;
		$class_id = $_POST['class_id'];
		$lang = $_POST['lang'];
		
		$model_cate = model('category');
		switch($level){
				case 0: 		
			
				    $cate_data = $model_cate->getGoodsClassListByParentId(0);
					foreach($cate_data as $k => $v){
						$cate_list[$k]['class_id'] = $v['cate_id'];
						$cate_list[$k]['class_name'] = $v['cate_name'];		
						$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];					
						$cate_list[$k]['name'] = $v['cate_name'];		
						//$cate_list[$k]['level'] = 0;
					}
				break;						
				case 1: 		
					
					if($class_id == 0){
				    	$cate_data = $model_cate->getGoodsClassList(array('cate_parent_id' => 1 ,'cate_level' => 1));	
					}else{
				    	$cate_data = $model_cate->getGoodsClassList(array('cate_parent_id' => $class_id ,'cate_level' => 1));	
					}
					$cate_list = array();
					$cate_list[0]['class_id'] = 0;
					$cate_list[0]['class_name'] = '全部';
					$cate_list[0]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.'food_all@2x.png';
					foreach($cate_data as $k => $v){
						$cate_list[$k+1]['class_id']= $v['cate_id'];
						$cate_list[$k+1]['class_name'] = $v['cate_name'];		
						$cate_list[$k+1]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];					
					}
					break;		
									
				case 2: 
				
					if($class_id == 0){
						$cate_data = $model_cate->getGoodsClassList(array('cate_level' => 2));						
						$cate_list = array();
						foreach($cate_data as $k => $v){
							$cate_list[$k]['class_id']= $v['cate_id'];
							$cate_list[$k]['class_name'] = $v['cate_name'];		
							$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];					
						}
					}else{	
						//自定义排序
						$where = array();
						$where['member_id'] = $this->member_info['member_id'];
						$where['parent_id'] = $class_id;						
						$bind_class = model('member_bind_class')->getMemberBindClassList($where,'bind_sort asc');					
					    $cate_data = $model_cate->getGoodsClassList(array('cate_parent_id' => $class_id ,'cate_level' => 2));
						if(count($bind_class) > 0){ 
							$cate = array();
							foreach($cate_data as $v){
								$cate[$v['cate_id']] = $v;
							}
							//有自定义排序							
							foreach($bind_class as $k => $v){
								$cate_list[$k]['class_id']= $v['bind_class_id'];
								$cate_list[$k]['class_name'] = $v['bind_class_name'];									
								$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$cate[$v['bind_class_id']]['cate_icon'];					
							}							
						}else{ 														
							//没有自定义							
							foreach($cate_data as $k => $v){
								$cate_list[$k]['class_id']= $v['cate_id'];
								$cate_list[$k]['class_name'] = $v['cate_name'];		
								$cate_list[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['cate_icon'];					
							}								
							
						}						
					}				
					break;	
		}		
		
		 output_data(array('list'=>$cate_list));
		
	}
	
	//获取店铺当前状态
	
	private function  getStoreState($store_id){
		$where = array(
			'store_id' => $store_id
		);
		$store = model('store')->getStoreInfo($where,'store_examine,store_state');
		return $store;
	}
	
	

	//获取小分类
	public function get_small_classOp(){
		
		$class_id = $_POST['class_id'];
		if($class_id == 1){ //获取当前用户绑定的分类
		        $where = array(
					'member_id' => $this->member_info['member_id']			
				);
				
				$member_bind_class = model('member_bind_class')->getMemberBindClassList($where,'bind_sort asc');			
			
				$small_class = model('store_small_class')->getStoreSmallClassList(TRUE);			
				
				foreach($small_class as $v){
					
					$small[$v['small_id']] = $v;	
					
				}
			
				if(count($member_bind_class) > 0 ) {					
					
					$data = array();									
					
					foreach($member_bind_class as  $k => $v){	
						$data[$k]['class_id'] = $v['bind_class_id'];
						$data[$k]['class_name'] = $v['bind_class_name'];
						$data[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$small[$v['bind_class_id']]['small_icon'];						
					}
					
					output_data($data);
					
				}else{
					
					$data = array();
					
					output_data($data);
				}
				
			
		}else{
			
					$where = array(
						'class_id' => $class_id
					);
					
					$small_class = model('store_small_class')->getStoreSmallClassList($where);					
					$data = array();
				
					foreach($small_class as  $k => $v){			
						$data[$k]['class_id'] = $v['small_id'];
						$data[$k]['class_name'] = $v['small_name'];
						$data[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'. $v['small_icon'];	
					}					
					output_data($data);	
					
					
			
		}
		
		
		
		
		
	}
	
	
	
	
	
	//记载首页图片分类
	public  function bannerOp(){
		$model_banner = Model('adv_new');		
		$where = array();
		$list = $model_banner->getAdvList($where);
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['en_pic'] =  UPLOAD_SITE_URL.'/banner/'.$v['adv_content'];
			$data[$k]['cn_pic'] =  UPLOAD_SITE_URL.'/banner/'.$v['adv_content'];
			$data[$k]['tn_pic'] =  UPLOAD_SITE_URL.'/banner/'.$v['adv_content'];
			$data[$k]['url'] = $v['adv_pic_url']		;	
		}
		output_data($data);
	}
		
		
	
	
	//店铺列表
	
	public function store_listOp($class_id = 0){
		//获取用户喜爱的
		$where = array();
		if($class_id > 0){
			$where['store_class_id'] = $class_id;
		}
		$where['store_state'] = array('in','1,2');		
		$store = model('store')->getStoreList($where);
		$list = array();
		foreach($store as $k=> $v){
								$list[$k]["store_id"] = $v['store_id'];
								$list[$k]["store_name"] = $v['store_name'];
								$list[$k]["store_name_sub"] = $v['store_en_name'];
								$list[$k]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');		
									
									
								$list[$k]["store_cate_tag"] = $this->store_bind_class($v['store_id']);
								$list[$k]["store_score"] = $v['store_score'];
								$list[$k]["store_distance"] = '0KM';
								$list[$k]["store_state"] = $v['store_state'];
								$list[$k]["store_follow"] = $this->store_follow($v['store_id']);
		}
		return $list;
		
	}
	
	
	
	//已绑定距离
	
	public function serach_store_listOp(){
	        	$logic_distribution = Logic('distribution');
				$keyword = $_POST['keyword'];
				$class_id = intval($_POST['class_id']);
				$level = intval($_POST['level']) ;
				$big_class_id = intval($_POST['big_class_id']) == 0 ? 1 : intval($_POST['big_class_id']);
				$where = array();
				if($keyword){
					$where['store_name'] = array('like','%'.$keyword.'%');
					$where['store_state'] = array('in','1,2');
				}	//0322修改	
				$order = array();
				$where = array();	
				//0322修改	
				if($class_id > 0){
						if($level == 1){ //				
							$wheres = array('cate_parent_id' => $class_id ,'cate_level' => 2);					
						}elseif($level == 2){
							$wheres = array('cate_id' => $class_id , 'cate_level' =>2 );
						}				
						$cate_data = model('category')->getGoodsClassList($wheres);
						$class_ids = array();
						foreach($cate_data as $v){
							$class_ids[] = $v['cate_id'];					
						}
						$class_ids = implode(',',$class_ids);
					
						$bind_class = model('store_bind_class')->getStoreBindClassListGroup(array('class_id'=> array('in',$class_ids)),'store_id');
					//	print_r($bind_class);
						$ids = array();
						foreach($bind_class as $v){
							$ids[] = $v['store_id'];
						}						   
						$ids = implode(',',$ids);
					//	print_r($ids);
						$where['store_id'] = array('in',$ids);
						//0322修改结束
				}
	
        		if($_POST['sort'] < 3 ){
        			$where['store_examine'] = 2;
        			$where['store_state'] =  array('in','1,2');
        		}else{
        			$grade = $this->getGrade($this->member_info['member_id']);
        			if($_POST['sort'] == 4){ //编辑中
        				if($grade['admin_type'] == 1){	
        					$where['admin_id'] = $grade['admin_id'];
        				}
        				$where['store_examine'] = 0;
        			}
        			
        			if($_POST['sort'] == 5){ //审核中
        				if($grade['admin_type'] == 1){	
        					$where['admin_id'] = $grade['admin_id'];
        				}				
        				$where['store_examine'] = 1;				
        			}	
        			
        			if($_POST['sort'] == 6){ //已审核
        				if($grade['admin_type'] == 1){	
        					$where['admin_id'] = $grade['admin_id'];
        				}				
        				$where['store_state'] =  0;
        				$where['store_examine'] = 2;				
        			}	
        		}		
		
        		if(!empty($_POST['latlng'])){
        				$member_points = explode(',',$_POST['latlng']);
        				$points = array(
        					'lng'=>$member_points[0],
        					'lat'=>$member_points[1]
        				);		
        				$area = $this->regionMember($points);
        		        if(!empty($area)){
            		        $area_sm = model('region')->getRegionList(array('region_city_id' => $area['region_city_id'],'region_parent_id' => 0));
            		        foreach($area_sm as $v){
            		            $area_ids[] = $v['region_id'];
            		        }
            		        $area_ids = implode(',',$area_ids);
            		        //获取当前大区
            		       // print_r($area_sm_ids);
            				$where['region_id'] = array('in',$area_ids);// $sm_id > 0 ? $sm_id : 0;
        		        }
        		    
        		}
		
            	//	print_r($sm_id);
            	//	print_r($where);
	        	$store = model('store')->getStoreList($where,100,'store_state asc');
        		$list = array();
        		$i = 0 ;
        		$member_store_km = 5000;
		
        		foreach($store as $k => $v){
        					
        			$distance = $this->getDistance($points['lat'],$points['lng'],$v['store_lat'],$v['store_lng']);
        			$delivery = $logic_distribution->getStoreDeliver(array('store_lat'=>$v['store_lat'],'store_lng'=> $v['store_lng'],'store_id'=>$v['store_id']));
        			if(!empty($area_ids)){
                        $list[$i]["store_id"] = $v['store_id'];
                        $list[$i]["store_name_primary"] = $v['store_name_primary'];
                        $list[$i]["store_name_secondary"] = $v['store_name_secondary'];
                        $list[$i]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');	
                        $list[$i]["store_cate_tag"] = $this->store_bind_class($v['store_id'],1);
                        $list[$i]["store_score"] = $v['store_score'];																
                        $list[$i]["store_distance"] = $this->parseDistance($distance);
                        $list[$i]["store_distance_m"] = $distance;						
                        $list[$i]["store_state"] = $v['store_state'];
                        $list[$i]["store_follow"] = $this->store_follow($v['store_id']);
                        $latlng = $v['store_lng'].','.$v['store_lat'];
                        //	$list[$i]["store_capacity"] = $this->storeWaiter($latlng,$v['store_id']);
                        $list[$i]['store_capacity'] = $delivery['count'];
                        
                        //新增分类
                        $list[$i]['store_class'] = $this->store_bind_class_first($v['store_id']);
                        //新增商家公告
                        $list[$i]['store_notice'] = $v['store_notice'];
                        //新增营业时间
                        $list[$i]['store_time'] = $this->store_opening_hours($v['store_shophours']);
                        //预计送达时间
                        $list[$i]['store_estimated_time'] = unserialize($v['store_estimated_time']);
                        $i++;
        									
        			}elseif($distance <= $member_store_km){
                        $list[$i]["store_id"] = $v['store_id'];
                        $list[$i]["store_name_primary"] = $v['store_name_primary'];
                        $list[$i]["store_name_secondary"] = $v['store_name_secondary'];
                        $list[$i]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');	
                    				
                        $list[$i]["store_cate_tag"] = $this->store_bind_class($v['store_id'],1);
                        $list[$i]["store_score"] = $v['store_score'];   
                        $list[$i]["store_distance"] = $this->parseDistance($distance);
                        $list[$i]["store_distance_m"] = $distance;						
                        $list[$i]["store_state"] = $v['store_state'];
                        $list[$i]["store_follow"] = $this->store_follow($v['store_id']);
                        $latlng = $v['store_lng'].','.$v['store_lat'];
                        //	$list[$i]["store_capacity"] = $this->storeWaiter($latlng,$v['store_id']);
                        $list[$i]['store_capacity'] = $delivery['count'];
                        //爆单
                        $list[$i]['store_busy_protection'] = $v['store_busy_protection'];
                        $i++;
        			}
        		}
        		
        		if($_POST['sort'] == 3){
        			$lists = $list;
        			$list = array();
        			foreach($lists as $k => $v){
        				if($v['store_capacity'] > 0){
        					$list[] = $v;
        				}
        			}
        		}
        		
		    //排序
			$sort = intval($_POST['sort']);
			switch($sort){
				case 0: 	
					$sort = 'store_distance_m';	
					$list = $this->sortArrByManyField($list,'store_state',SORT_ASC,'store_distance_m',SORT_ASC);
				//	array_multisort(array_column($list,$sort),SORT_ASC,$list);
				break;	
					
				case 1: 	
					$sort = 'store_distance_m';		
					array_multisort(array_column($list,$sort),SORT_ASC,$list);
					
        		break;	
					
				case 2: 	
					$sort = 'store_score';
					array_multisort(array_column($list,$sort),SORT_DESC,$list); 
        		break;
					
				case 3: 	
					$sort = 'store_distance_m';
					array_multisort(array_column($list,$sort),SORT_ASC,$list); 
				
        		break;	
				default:
				
					$sort = 'store_state';
					array_multisort(array_column($list,$sort),SORT_ASC,$list); 
					
				break;	
		
			}
		
	    	output_data($list);
		
	}
	
	
    //营业时间
	
	private function store_opening_hours($time){
	
	    $store_time = unserialize($time); 
	    $week = strtolower(date('D'));
	    $data = null;
	    foreach($store_time as $v){
	        if($week == $v['code']){
	            $data['week'] =$v['code'];
	            if($v['t1']['show'] == 1){
	              $data['time'][] = $v['t1']['stime'].'-'.$v['t1']['etime'];
	            }
	             if($v['t2']['show'] == 1){
	                $data['time'][] = $v['t2']['stime'].'-'.$v['t2']['etime'];
	             }
	            return $data;
	            
	            
	        }
	    }
	 
	 //return date("D");
	 
	 
	    
	}
	
	//获取当前商家的可用配送员
	
	
	
	
	
	
	//多参数排序
	private	function sortArrByManyField(){
		  $args = func_get_args();
		  if(empty($args)){
			return null;
		  }
		  $arr = array_shift($args);
		  if(!is_array($arr)){
			throw new Exception("第一个参数不为数组");
		  }
		  foreach($args as $key => $field){
			if(is_string($field)){
			  $temp = array();
			  foreach($arr as $index=> $val){
				$temp[$index] = $val[$field];
			  }
			  $args[$key] = $temp;
			}
		  }
		  $args[] = &$arr;//引用值
		  call_user_func_array('array_multisort',$args);
		  return array_pop($args);
		}
	
	
	
	
	
	//计算距离
	  /**
     *  @desc 根据两点间的经纬度计算距离
     *  @param float $lat 纬度值
     *  @param float $lng 经度值
     */
    private function getDistance($lat1, $lng1, $lat2, $lng2) {
        $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }
	
	
	private function parseDistance($num = 0){
		$num = floatval($num);
		if ($num >= 1000) {
			$num = $num/1000;
			return str_replace('.0','',number_format($num,1,'.',''));
		} else {
			return $num;
		}
	}
	
	
		
	private function getGrade($member_id){
		$where = array(
			'member_id' => $member_id
		);
		return  model('admin')->getAdminInfoNew($where);
		
	}
	
	
	
	//检测该店铺是否被收藏	
	private function store_follow($id){	
		
		$where = array(
			'store_id' => $id,
			'member_id' => $this->member_info['member_id']
		);		
		$count = model('favorites')->getFavoritesCount($where);
		if($count > 0){			
			return 1;			
		}else{
			return 0;
		}		
	}
	
	
	
	
	
	//店铺绑定的子类
	private function store_bind_class($store_id,$type = 0){
		$where = array(
			'store_id' =>  $store_id,
		);
		$bind_class = model('store_bind_class')->getStoreBindClassList($where);
		$list = array();
		foreach($bind_class as $k => $v){
		 
			$list[]['tag_name'] = $v['class_name'];
		       if($k == 1){
		          break;
		    }	
		}
		
		
		if($type == 1){
			$where = array();
			$where['store_id'] = $store_id;
			$where['is_recommend'] = 1;
			$goods = model('goods')->getGoodsList($where,'goods_name','','goods_sales_month desc',5);
			foreach($goods as $v){
				$list[]['tag_name'] = $v['goods_name'];				
			}			
		}
		
		
		return $list;
		
	}
	
	//新增主分类
	private function store_bind_class_first($store_id){
	    
	    $where = array(
			'store_id' =>  $store_id,
			'class_type' => 1
		);
		$bind_class = model('store_bind_class')->getStoreBindClassInfo($where);
	    
	    $info = model('category')->getGoodsClassInfoById($bind_class['class_id']);
		if($info){
		    $info['small_name'] = $info['cate_name'];
		    $info['small_icon'] = UPLOAD_SITE_URL.'/icon/'.$info['cate_icon'];
		    $info['small_lang'] = unserialize($info['cate_lang']);
		}    
		    
		 return $info;
	    
	}
	
	

	private function small_class($class_id){
		
		$where = array(
			'member_id' => $this->member_info['member_id']			
		);
		$member_bind_class = model('member_bind_class')->getMemberBindClassList($where);
		
		$where = array();
		if(count($member_bind_class) > 0 ) {
			foreach($member_bind_class as $v){
				$ids .= $v['bind_class_id'].',';
			}
			$where['small_id'] = array('in',substr($ids,0,-1));
			$where['class_id'] = $class_id;
			$small_class = model('store_small_class')->getStoreSmallClassList($where);
				$data = array();
			    $data[0]['id'] = 0;
				$data[0]['name'] = '全部';
				$data[0]['icon'] = 'food_all@2x.png';
				$data[0]['action'] =  1 ;
				$i = 1;				
				foreach($small_class as  $k => $v){			
					$data[$k+1]['id'] = $v['small_id'];
					$data[$k+1]['name'] = $v['small_name'];
					$data[$k+1]['icon'] = $v['small_icon'];
					$data[$k+1]['action'] = 0;
					$i++;
				}
				
				$data[$i+1]['id'] = 0;
				$data[$i+1]['name'] = '添加品类';
				$data[$i+1]['icon'] = 'food_all@2x.png';
				$data[$i+1]['action'] =  0 ;
				$data[$i+1]['type'] =  'add';
	
				return $data;
			}else{
				
				$data[0]['id'] = 0;
				$data[0]['name'] = '全部';
				$data[0]['icon'] = 'food_all@2x.png';
				$data[0]['action'] =  1 ;
				$data[1]['id'] = 0;
				$data[1]['name'] = '添加品类';
				$data[1]['icon'] = 'food_all@2x.png';
				$data[1]['action'] =  0 ;
				$data[1]['type'] =  'add';
				return $data;
			}
		
	}
    


	
	
    /**
     * 默认搜索词列表
     */
    public function search_key_listOp() {
       
		$list = @explode(',',C('hot_search'));
		
	
        if (!$list || !is_array($list)) { 
            $list = array();
        }
  
        output_data(array('list'=>$list));
    }

	
	
	
	
    /**
     * 热门搜索列表
     */
    public function search_hot_infoOp() {
        if (C('rec_search') != '') {
            $rec_search_list = @unserialize(C('rec_search'));
        }
        $rec_search_list = is_array($rec_search_list) ? $rec_search_list : array();
        $result = $rec_search_list[array_rand($rec_search_list)];
        output_data(array('hot_info'=>$result ? $result : array()));
    }

	
	
	
	
    /**
     * 高级搜索
     */
    public function search_advOp() {
        $area_list = Model('area')->getAreaList(array('area_deep'=>1),'area_id,area_name');
        if (C('contract_allow') == 1) {
            $contract_list = Model('contract')->getContractItemByCache();
            $_tmp = array();$i = 0;
            foreach ($contract_list as $k => $v) {
                $_tmp[$i]['id'] = $v['cti_id'];
                $_tmp[$i]['name'] = $v['cti_name'];
                $i++;
            }
        }
        output_data(array('area_list'=>$area_list ? $area_list : array(),'contract_list'=>$_tmp));
    }
	
	
	
	//lbs云，获取当前定位下的店铺
	private function lbs_store($latlng = ''){
		
		
		
			$lbs = new lbs();			
			$result = $lbs->searchPoi('store', $latlng);				
			if($result['total'] > 0){				
					foreach($result['contents'] as   $v){
						$store_ids .= $v['store_id'].',';				
						$distance[$v['store_id']] = $v['distance'];
					}	
					$store_ids = substr($store_ids,0,-1);				
					return array('store_ids'=>$store_ids,'distance'=>$distance);	
				
			}else{
				
					return false;
				
			}
		
	}
	
	
	//搜索结果
	public function search_storeOp(){
		
		$store_model = model('store');
		
		$keyword = $_POST['keyword'];
		$where = array(
			'goods_name' => array('like','%'.$keyword.'%')
		);
		
		//索引有满足条件的店铺
		$goods = model('goods_language')->getGoodsLangList($where,'goods_name,store_id,goods_id');
	
		foreach($goods as $v){			
			$store_ids[$v['store_id']] = $v['store_id'];	
		}
		
		
		$where = array();
		$where['store_name_primary|store_name_secondary|store_address'] = array('like','%'.$keyword.'%');
		
		$store_list = 	$store_model->getStoreList($where);
		
		foreach($store_list as $v){
		    
		    $store_ids[$v['store_id']] = $v['store_id'];
		    
		}
		
		
		$where = array();
	
		/*
		$latlng ='118.839881,31.999344';
		$ids = '';
		$lbs = $this->lbs_store($latlng);
		if($lbs){			 
			$distance = $lbs['distance'];
		}else{
			output_error('附近暂无商家');
		}
		*/
		
		$points = explode(',',$_POST['lnglat']);
		
		
		
		
		if(empty($store_ids)){
		    output_data(array());
		}
		
		
		$store_ids = implode(',',$store_ids);
        
		$where['store_id'] = array('in',$store_ids);
		
		
		$store = model('store')->getStoreList($where,100,$order);		
		$list = array();
		foreach($store as $i=> $v){
							
							$distance = $this->getDistance($points[1],$points[0],$v['store_lat'],$v['store_lng']);
		
							
							$list[$i]["store_id"] = $v['store_id'];
                            $list[$i]["store_name_primary"] = $v['store_name_primary'];
                            $list[$i]["store_name_secondary"] = $v['store_name_secondary'];
                            $list[$i]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');	
                            $list[$i]["store_cate_tag"] = $this->store_bind_class($v['store_id'],1);
                            $list[$i]["store_score"] = $v['store_score'];																
                            $list[$i]["store_distance"] = $this->parseDistance($distance);
                            $list[$i]["store_distance_m"] = $distance;						
                            $list[$i]["store_state"] = $v['store_state'];
                            $list[$i]["store_follow"] = $this->store_follow($v['store_id']);
                            $latlng = $v['store_lng'].','.$v['store_lat'];
                            //	$list[$i]["store_capacity"] = $this->storeWaiter($latlng,$v['store_id']);
                            $list[$i]['store_capacity'] = $delivery['count'];
                            
                            //新增分类
                            $list[$i]['store_class'] = $this->store_bind_class_first($v['store_id'],1);
                            //新增商家公告
                            $list[$i]['store_notice'] = $v['store_notice'];
                            //新增营业时间
                            $list[$i]['store_time'] = $this->store_opening_hours($v['store_shophours']);
                            //预计送达时间
                            $list[$i]['store_estimated_time'] = unserialize($v['store_estimated_time']);
                           
            				$list[$i]['store_goods'] = $this->store_search_goods($v['store_id'],$keyword);			
							
							
							
							/*
								$list[$k] = $v;
								$list[$k]["store_id"] = $v['store_id'];
								$list[$k]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');	
			
								$list[$k]["store_cate_tag"] = $this->store_bind_class($v['store_id']);
								$list[$k]["store_score"] = $v['store_score'];
								$list[$k]["store_distance"] = '6.1'; //number_format($distance[$v['store_id']] /1000,1) ;
								$list[$k]["store_state"] = $v['store_state'];
								$list[$k]["store_follow"] = $this->store_follow($v['store_id']);								
								$list[$k]['store_goods'] = $this->store_search_goods($v['store_id'],$keyword);*/
		}
		
		output_data($list);
		
	}
	
	
	
	
	//计算运力
/*	private function storeWaiter($latlng,$store_id){
							
		$lbs = new lbs();
		$result = $lbs->searchPoi('waiter', $latlng);						
	
		if($result['status'] == 0){
			//查询当前配送员是否有订单。
			$yunli_data = 3;
			$yunli = 0;		
			foreach($result['contents'] as $v){				
				$orderNum = $this->waiterOrderNum($v['waiter_id']);				
				if($orderNum < $yunli_data ){
					$yunli += $yunli_data - $orderNum;	
				}else{
					$yunli += 0;
				}
			}	
			return  $yunli ;
		}else{
			return 0 ;	
		}	
	}
	*/
	
	//判断当前配送员有几个单子。	
	private function waiterOrderNum($waiter_id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$waiter_id,
			'order_state' => array('in','30,40,50')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	
	//搜索店铺的商品
	public function store_search_goods($store_id,$keyword){
		
		$where = array(
			'store_id' => $store_id,
			'goods_name' => array('like','%'.$keyword.'%')			
		);		
		
		$lang_goods = model('goods_language')->getGoodsLangList($where,'goods_name,store_id,goods_id');
		$data = array();
		foreach($lang_goods as $k=> $v){
			
			if($k < 3){
			    
				$goods_common = $this->getGoodsCommon($v['goods_id']);
				$data[$k] = $goods_common;
				$data[$k]['goods_name'] = $v['goods_name'];
				$data[$k]['icon'] = $this->goods_icon($goods_common['goods_ingr']);
				
			}
		}		
		
		return $data;
		
	}
	
	private function getGoodsCommon($goods_id){
		
		$where = array(
			'goods_id' => $goods_id
		);		
		$goods = model('goods')->getGoodsInfo($where);
		return $goods;
	
	}
		
	//icon 
	private function goods_icon($icon_id){
		
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		foreach($row as $v){
			
			
			
			if($v['attr_type'] == 1){			
				$content = explode('|',$v['attr_content']);
				$v['attr_name'] = $content[0];
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[1];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[2];
			}else{
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
				
			}
			
			
			
			
			$data[$v['attr_class_id']]['name'] = $v['attr_class_name'];
			$data[$v['attr_class_id']]['child'][] = $v;
		}		
		
		$list = array();
		foreach($data as $v){
			$list[] = $v;
		}
		
		return $list;	
		
	}
	
	
	
	//用户操作排序
	public function get_small_class_sortOp(){
		$model_cate = model('category');
		$data = array();		
		$parent_id = intval($_POST['parent_id']);
		$type = intval($_POST['type']);
		$ids = explode(',',$_POST['class_id']);				
		$where = array();
		$where['member_id'] = $this->member_info['member_id'];		
		$where['parent_id'] = $parent_id;
		$count = model('member_bind_class')->getMemberBindClassCount($where);				
		if($count > 0){			
			foreach($ids as $k=> $v){
				$where = array();
				$where['member_id'] = $this->member_info['member_id'];
				$where['bind_class_id'] = $v;			
				$data = array(
					'bind_sort' => $k+1
				);
				$row = model('member_bind_class')->editMemberBindClass($data,$where);							
			}
		}else{					
			$cate_data = $model_cate->getGoodsClassList(array('cate_parent_id' => $parent_id ,'cate_level' => 2));	
			foreach($cate_data as $k=> $v){				
				$data[] = array(
					'member_id' => $this->member_info['member_id'],
					'bind_class_name'	=> $v['cate_name'],
					'bind_class_id'		=> $v['cate_id'],
					'bind_sort'			=> $k,				
					'parent_id' 		=> $parent_id
				);				
			}	
			$row = model('member_bind_class')->addMemberBindClassAll($data);
		}
		
		if($row){
			output_data('ok');
		}else{
			output_error('排序失败');			
		}	
		
	}
	
	
	//计算多边形围栏
	
	
/*
	public function xxxOp(){
	    
	    $list = model('ceshi_user')->getAddressList(TRUE);
	 //   print_r($list);
	    
	    
	    foreach($list as $k=> $v){
	        $points = array();
	        $points['lng'] = $v['lng'];
	        $points['lat'] = $v['lat'];
	        
	        
	    
	        $bb =   $this->regionMember($points);
	        
	        
	        if($bb){
	            
	            
	            $update = array(
	                'region_id' => $bb['region_id'], 
	                'region_name' => $bb['region_name'],
	                'region_color' => $bb['region_color']
	                );
	                $where = array(
	                    'address_id' => $v['address_id']
	                );
	            
	           
	                model('ceshi_user')->editAddress($update,$where);
	            
	        }
	        
	        
	       
	    }
	    
	    
	   
	    
	}
		*/
		
	public function xxxOp(){
	    
	    $list = model('store')->getStoreList(TRUE);
	 //   print_r($list);
	    
	    
	    foreach($list as $k=> $v){
	        $points = array();
	        $points['lng'] = $v['store_lng'];
	        $points['lat'] = $v['store_lat'];
	        
	        
	    
	        $bb =   $this->regionMember($points);
	        
	        
	        if($bb){
	            
	            
	            $update = array(
	                'region_id' => $bb['region_parent_id'], 
	                'region_sm_id' => $bb['region_id'],
	                'region_name' => $bb['region_name'],
	                'region_color' => $bb['region_color']
	                );
	                $where = array(
	                    'store_id' => $v['store_id']
	                    );
	            
	           
	            model('store')->editStore($update,$where);
	            
	        }
	        
	        
	       
	    }
	    
	    
	   
	    
	}
		
	
	
  private function regionMember($points){
		 //145.003877,
		$region =  model('region')->getRegionList(array('region_type'=> 2));		
		foreach($region as $k =>  $v){		 	
		   $abc = $this->points_array($v['region_coordinate']);
		 //  print_r($abc);
		   $row = 	$this->is_point_in_polygon($points,$abc);			
		  
			if($row == 1){
				return $v;
				break;
			}
			
		}
  }

  private function points_array($points){
		$points_array = explode('|',$points);
		$data = array();
		foreach($points_array as $k => $v){
			$bs = explode(',',$v);
			$data[$k]['lat'] = $bs[1];
			$data[$k]['lng'] = $bs[0];
		}
		return $data;
		
	}
	
	
	
	private function is_point_in_polygon($point, $pts) {
		$N = count($pts);
		$boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
		$intersectCount = 0;//cross points count of x 
		$precision = 2e-10; //浮点类型计算时候与0比较时候的容差
		$p1 = 0;//neighbour bound vertices
		$p2 = 0;
		$p = $point; //测试点
	 
		$p1 = $pts[0];//left vertex        
		for ($i = 1; $i <= $N; ++$i) {//check all rays
			// dump($p1);
			if ($p['lng'] == $p1['lng'] && $p['lat'] == $p1['lat']) {
				return $boundOrVertex;//p is an vertex
			}
			 
			$p2 = $pts[$i % $N];//right vertex            
			if ($p['lat'] < min($p1['lat'], $p2['lat']) || $p['lat'] > max($p1['lat'], $p2['lat'])) {//ray is outside of our interests
				$p1 = $p2; 
				continue;//next ray left point
			}
			 
			if ($p['lat'] > min($p1['lat'], $p2['lat']) && $p['lat'] < max($p1['lat'], $p2['lat'])) {//ray is crossing over by the algorithm (common part of)
				if($p['lng'] <= max($p1['lng'], $p2['lng'])){//x is before of ray
					if ($p1['lat'] == $p2['lat'] && $p['lng'] >= min($p1['lng'], $p2['lng'])) {//overlies on a horizontal ray
						return $boundOrVertex;
					}
					 
					if ($p1['lng'] == $p2['lng']) {//ray is vertical                        
						if ($p1['lng'] == $p['lng']) {//overlies on a vertical ray
							return $boundOrVertex;
						} else {//before ray
							++$intersectCount;
						}
					} else {//cross point on the left side
						$xinters = ($p['lat'] - $p1['lat']) * ($p2['lng'] - $p1['lng']) / ($p2['lat'] - $p1['lat']) + $p1['lng'];//cross point of lng
						if (abs($p['lng'] - $xinters) < $precision) {//overlies on a ray
							return $boundOrVertex;
						}
						 
						if ($p['lng'] < $xinters) {//before ray
							++$intersectCount;
						} 
					}
				}
			} else {//special case when ray is crossing through the vertex
				if ($p['lat'] == $p2['lat'] && $p['lng'] <= $p2['lng']) {//p crossing over p2
					$p3 = $pts[($i+1) % $N]; //next vertex
					if ($p['lat'] >= min($p1['lat'], $p3['lat']) && $p['lat'] <= max($p1['lat'], $p3['lat'])) { //p.lat lies between p1.lat & p3.lat
						++$intersectCount;
					} else {
						$intersectCount += 2;
					}
				}
			}
			$p1 = $p2;//next ray left point
		}
	 
		if ($intersectCount % 2 == 0) {//偶数在多边形外
			return 0;
		} else { //奇数在多边形内
			return 1;
		}
	}
		
		

    public function activityOp(){
        
        $data = array();
        $data['id'] = 1;
        $data['activity_title'] = '测试活动弹窗';
        $data['activity_image'] = 'http://test-api.mad-express.com/data/upload/activity/popup.png';
        $data['activity_url'] = 'http://test-api.mad-express.com/wap/tmpl/adv.html?adv_id=966';
        
        
        
        
        output_data($data);
    }
	
	
	public function activity_infoOp(){
	    
	    $row = model('adv')->getOneById('966');
	    $text = unserialize($row['adv_text']);
	    $lang = $_POST['lang'];
	    if(!empty($lang)){
	        $row['adv_text'] = $text[$lang]['text'];
	    }else{
	        $row['adv_text']  = $text['ENG']['text'];
	    }
	    $data['lang'] = model('language')->getLangList(array('language_system' => 1));
        $data['activity_title'] = $row['adv_title'];
        $data['activity_url'] = $row['adv_pic_url'].'&lang='.$_POST['lang'];
        
        
	    output_data(array('info' => $data));
	    
	}
	
	

	
	
	
}
