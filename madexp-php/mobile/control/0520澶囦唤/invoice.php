<?php
/**
 * 新增
 * 
 **/

defined('InMadExpress') or exit('Access Invalid!');
class invoiceControl extends mobileHomeControl{

	public function __construct() {
        parent::__construct();
		
		$data = array();
		//商家信息
		$store = array();
		$store['store_name'] = 'Yi Ming Chuan Restaurant'; //餐厅名称
		$store['store_company_name'] = 'Yi Ming Chuan Pty Ltd'; //餐厅公司主体名称
		$store['store_address'] = '5 Mad Street, Box Hill, VIC, AUS'; //餐厅地址
		$store['store_abn'] = '9999 99999';		
		$store['store_code'] = 'AUS01 6666 01';		
		$data['store'] = $store; //店铺信息
		
		//用户信息
		$user = array();
		$user['user_code'] = '78911222';
		$data['user'] = $user; //用户信息
		
		
		//配送员信息
		$delivery = array();		
		$delivery['delivery_code'] = 'R6666 6666';
		$delivery['delivery_truename'] = 'San Zhang';
		$delivery['delivery_address'] = '5 Express Drive, Box Hill, VIC, AUS';
		$delivery['delivery_abn'] = '8888 88888';
		$data['delivery'] = $delivery;
		
		
		//平台
		$platform = array();		
		$platform['platform_company_name'] = 'Mad Express Pty Ltd';
		$platform['platform_address_line1'] = 'U7/153-155 Rooks Road,';
		$platform['platform_address_line2'] = 'Vermont South, VIC, AUS';
		$platform['platform_abn'] = '6666 66666';
		$data['platform'] = $platform;
		
		//订单信息
		$order = array();
		$order['order_sn'] = 'AUS010001250221019336'; //订单号
		$order['merchant_voucher'] = array( //商家优惠券
			'voucher_code' => '537090651378685023',
			'voucher_amount' => '3.00'
		);
		$order['platform_voucher'] = array(  //平台优惠券
			'voucher_code' => '259120606731970771',
			'voucher_amount' => '8.00'
		);
		$order['payment_txn_fee'] = '1.12'; //手续费
		$order['total_amount'] = '48.12'; //总金额
		$order['subtotal'] = '55.00';
		$order['eng_date'] = date('d M Y',time());	
		$goods_list = array();		
		$goods_list[0] = array(
			'goods_num' => 1,
			'goods_name' => 'Spicy Noodle',
			'goods_specs' => 'BT Spicy, Without Coriander',
			'unit_price'  => '10.00',
			'unit_gst'	=>'1.00',
			'goods_price' => '11.00'
		);
		$goods_list[1] = array(
			'goods_num' => 2,
			'goods_name' => 'Wonton Delight',
			'goods_specs' => '(unit Discount $1.10)',
			'unit_price'  => '20.00',
			'unit_gst'	=>'2.00',
			'goods_price' => '44.00'
		);
		$order['goods_list'] = $goods_list;		
		$data['order'] = $order;
		
		//销售费补贴发票
		$data['sales_sn'] = 'MESRI'. date('dmy',time()).'0001';
		$sales_list = array();
		$sales_list[0] = array(
			'item_name' => 'Sales Reimbursement',
			'item_credit_note_sn' => 'CN-AUS010001250221019336-01',		
			'item_amount' => '48.12'
		);		
		$data['sales_list'] = $sales_list;
		
		
		//优惠券补贴发票
		$data['voucher_sn'] = 'MEVR'. date('dmy',time()).'0000001';
		$voucher_list = array();
		$voucher_list[0] = array(
			'item_name' => 'Voucher Reimbursement of #259120606731970771',
			'item_order_sn' => 'Order #AUS010001250221019336',		
			'item_amount' => '5.00'
		);		
		$data['voucher_list'] = $voucher_list;
		$data['voucher_amount'] = '5.00';
		
		
		//配送费发票	
		$delivery_list = array();
		$delivery_list[0] = array(
			'item_name' => 'Deliery Fee',
			'item_order_sn' => 'Order #AUS010001250221019336',		
			'item_amount' => '5.99'
		);		
		$data['delivery_list'] = $delivery_list;
		$data['delivery_amount'] = '5.99';
		
		
		
		//配送费补贴发票
		$data['medr_sn'] = 'MEDR'. date('dmy',time()).'0001';
		$medr_list = array();
		$medr_list[0] = array(
			'item_name' => 'Delivery Fee Reimbursement',
			'item_credit_note_sn' => 'CN-AUS010001250221019336-02',		
			'item_amount' => '5.99'
		);		
		$data['medr_list'] = $medr_list;
		$data['medr_amount'] = '5.99';
		
		
		//激励金发票
		
		$data['incentive_sn'] = 'MECI'.$delivery['delivery_code'].date('dmy',time());
		$incentive_list = array();
		$incentive_list[0] = array(
			'item_name' => "'MPOIG' Incentive",
			'item_order_sn' => 'AUS010001250221019336(D)',		
			'item_amount' => '1.01'
		);		
		$incentive_list[1] = array(
			'item_name' => "'MPOIG' Incentive",
			'item_order_sn' => 'AUS010001250221019337(D)',		
			'item_amount' => '1.01'
		);		
		$incentive_list[2] = array(
			'item_name' => "'MPOIG' Incentive",
			'item_order_sn' => 'AUS010001250221019338(D)',		
			'item_amount' => '1.01'
		);		
		$incentive_list[3] = array(
			'item_name' => "'Schedule Urself' Incentive",
			'item_order_sn' => 'AUSC01050520T1SYRA12',		
			'item_amount' => '13.65'
		);		
		$data['incentive_list'] = $incentive_list;
		$data['incentive_amount'] = '16.68';
		
		$incentive_credit_list = array();
		$incentive_credit_list[0] = array(
			'item_name' => "'MPOIG' Incentive",
			'item_order_sn' => 'AUS010001250221019336(D)',		
			'item_amount' => '1.01'
		);		
		$data['incentive_credit_list'] = $incentive_credit_list;
		$data['incentive_credit_amount'] = '1.01';
		
		
		/*
		平台服务集成发票 商家数据
		ME Platform Invoice
		*/
	   
	   $data['mepimel_sn'] = 'MEPIMEL'. date('dmy',time()).'0001';
	   $mepimel_list = array();
	   $mepimel_list[0] = array(
	   	'item_name' => "Commission & TXN Fee",
	   	'item_desc' => 'AUS010001250221019336',		
	   	'item_amount' => '6.32'
	   );		
	   $mepimel_list[1] = array(
	   	'item_name' => "Commission & TXN Fee",
	   	'item_desc' => 'AUS010001250221019337',		
	   	'item_amount' => '14.29'
	   );		
	   $mepimel_list[2] = array(
	   	'item_name' => "Commission & TXN Fee",
	   	'item_desc' => 'AUS010001250221019338',		
	   	'item_amount' => '4.15'
	   );		
	   $mepimel_list[3] = array(
	   	'item_name' => "Withdraw Fee",
	   	'item_desc' => 'of $1000.00',		
	   	'item_amount' => '9.00'
	   );		
	   $data['mepimel_list'] = $mepimel_list;
	   $data['mepimel_amount'] = '33.76';
	   
	   
	   //配送员 数据
	   
	   $mepimel_courier_list = array();
	   $mepimel_courier_list[0] = array(
	   	'item_name' => "Withdraw Fee",
	   	'item_desc' => 'of $100.00',		
	   	'item_amount' => '0.90'
	   );		
	   
		$data['mepimel_courier_list'] = $mepimel_courier_list;
		$data['mepimel_courier_amount'] = '0.90';
		$data['mepimel_courier_peirod'] = date('d M y',time() - (24*60*60 *8)) .' - '. date('d M y',time() - (24*60*60));
		
		
		
		//平台服务集成发票退款 商家
		$mepimel_credit_merchant_list = array();
		$mepimel_credit_merchant_list[0] = array(
			'item_name' => "Commission & TXN Fee",
			'item_desc' => 'AUS010001250221019336',		
			'item_amount' => '6.32'
		);		
		$mepimel_credit_merchant_list[1] = array(
			'item_name' => "Commission & TXN Fee",
			'item_desc' => 'AUS010001250221019337',		
			'item_amount' => '14.29'
		);		
		$data['mepimel_credit_merchant_list'] = $mepimel_credit_merchant_list;
		$data['mepimel_credit_merchant_amount'] = '20.61';
		
		
		//平台服务集成发票退款 商家
		$mepimel_credit_courier_list = array();
		$mepimel_credit_courier_list[0] = array(
			'item_name' => "Withdraw Fee",
			'item_desc' => 'of $100.00',		
			'item_amount' => '0.90'
		);		
	
		$data['mepimel_credit_courier_list'] = $mepimel_credit_courier_list;
		$data['mepimel_credit_courier_amount'] = '0.90';
		
		
		/**
		违约金收据 [商家]
		Liquidated Damage Receipt
		*/
	   
		$data['receipt_sn'] = 'AUS010001250221019336';
		$receipt_merchant_list = array();	   
		$receipt_merchant_list[0] = array(
			'item_name' => "Delivery Fee Reimbursement",
			'item_desc' => 'CN-AUS010001250221019336-02',		
			'item_amount' => '5.99'
		);
		$receipt_merchant_list[1] = array(
			'item_name' => "Voucher Re-Issued",
			'item_desc' => 'MEVR110819000001',		
			'item_amount' => '5.00'
		);			
			
		$data['receipt_merchant_list'] = $receipt_merchant_list;
		$data['receipt_merchant_amount'] = '10.99';
		
		/**
		违约金收据 [配送员]
		Liquidated Damage Receipt
		*/
			   
		$data['receipt_sn'] = 'AUS010001250221019336';
		$receipt_courier_list = array();	   
		$receipt_courier_list[0] = array(
			'item_name' => "Sales Reimbursement",
			'item_desc' => 'CN-AUS010001250221019336-01',		
			'item_amount' => '48.12'
		);
		$receipt_courier_list[1] = array(
			'item_name' => "Voucher Re-Issued",
			'item_desc' => 'AUS010001250221019336(S)',		
			'item_amount' => '8.00'
		);			
			
		$data['receipt_courier_list'] = $receipt_courier_list;
		$data['receipt_courier_amount'] = '56.13';
		
		
		
		
		
		//票据集成表
		//Tax Summary (2)		
		$type = $_GET['date'] ? $_GET['date'] : 'year' ;
		$data['tax_merchant'] = $this->tax_summary_merchant($type);				
		$data['tax_courier'] = $this->tax_summary_courier(type);
		$this->info = $data;
		
    }
  
  //给商家的tax summary 数据
	
	private function tax_summary_merchant($type='year'){		
		$tax_summary = array();
		$tax_summary['revenue_breakdown']['sub_total'] = $this->numberFormat(383406.62,$type);
		$tax_summary['revenue_breakdown']['list'] =array(
			array(
				'name' => 'Online Sales Income',
				'amount' => $this->numberFormat(382683.12,$type)
			),
			array(
				'name' => 'Offline Sales Income',
				'amount' => $this->numberFormat(723.50,$type)
			)
		);
		$tax_summary['incentives_reimbursements']['sub_total'] = $this->numberFormat(1418.72,$type);
		$tax_summary['incentives_reimbursements']['list'] =array(
			array(
				'name' => 'Sales Reimbursement ',
				'amount' => $this->numberFormat(265.42,$type)
			),
			array(
				'name' => 'Voucher Reimbursement',
				'amount' => $this->numberFormat(920.00,$type)
			),
			array(
				'name' => 'Platform Bonus',
				'amount' => $this->numberFormat(102.10,$type)
			),
			array(
				'name' => 'ME Refund',
				'amount' => $this->numberFormat(131.20,$type)
			)
		);
		$tax_summary['expenditure_breakdown']['sub_total'] = $this->numberFormat(78786.53,$type);
		$tax_summary['expenditure_breakdown']['list'] =array(
			array(
				'name' => 'Commission & TXN Fee',
				'amount' => $this->numberFormat(75670.78,$type)
			),
			array(
				'name' => 'Container Delivery Fee',
				'amount' => $this->numberFormat(150.00,$type)
			),
			array(
				'name' => 'Withdraw Fee',
				'amount' => $this->numberFormat(2745.32,$type)
			),
			array(
				'name' => 'Liquidated Damage',
				'amount' => $this->numberFormat(52.67,$type)
			),
			array(
				'name' => 'Sales Refund',
				'amount' => $this->numberFormat(213.21,$type)
			),
		);
		$tax_summary['gross_income'] = $this->numberFormat(306038.81,$type);
		$tax_summary['tax_title_date'] = $this->tax_title_date($type);
		$tax_summary['tax_date'] ='01 Aug 2019';// date('d M y',time());
		$tax_summary['url'] = $this->tax_merchant_url($type);
		$tax_summary['type'] = $type;
		return $tax_summary;
		
	}
	
	private function tax_merchant_url($type ='year'){
		$url = array();
		if($type == 'year'){	
			$url['title'] = 'URL OF TAX SUMMARIES';
			$list[0] = array(
				'name' => 'TAX SUMMARY 2018 Q3',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'TAX SUMMARY 2018 Q4',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'TAX SUMMARY 2019 Q1',
				'link' => '#'
			);
			$list[3] = array(
				'name' => 'TAX SUMMARY 2019 Q2',
				'link' => '#'
			);
		}
		if($type == 'quarter'){
			$url['title'] = 'URL OF TAX SUMMARIES';
			$list[0] = array(
				'name' => 'TAX SUMMARY JAN 2019',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'TAX SUMMARY FEB 2019',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'TAX SUMMARY MAR 2019',
				'link' => '#'
			);	
		}
		
		if($type == 'month'){
			$url['title'] = 'URL OF RECODES';
			$list[0] = array(
				'name' => 'ONLINE SALES INVOICES',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'OFFLINE SALES INVOICES',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'REIMBURSEMENT INVOICES',
				'link' => '#'
			);
			$list[3] = array(
				'name' => 'ME CREDIT NOTES',
				'link' => '#'
			);	
			$list[4] = array(
				'name' => 'REFUND CREDIT NOTES',
				'link' => '#'
			);	
			$list[5] = array(
				'name' => 'ME PLAFTORM INVOICES',
				'link' => '#'
			);	
			$list[6] = array(
				'name' => 'L.D RECEIPT',
				'link' => '#'
			);	
			
		}		
		$url['list'] = $list;		
		return $url;
		
		
	}
  
	private function tax_title_date($type = 'year'){		
		if($type == 'year'){
			$date = 'FY2018/2019';
		}
		if($type == 'quarter'){			
			$date = 'Q1 2019';
		}
		if($type == 'month'){			
			$date = 'JAN 2019';
		}				
		return $date;
	}
  
	private function numberFormat($amount,$type = ''){		
		if($type == 'quarter'){			
			$amount = $amount / 4;			
		}		
		
		if($type == 'month'){
			$amount = $amount / 12;
		}		
		
		return number_format($amount,2);
	}
	
	
	//给配送员的 tax summary 数据
	
	
	
	private function tax_summary_courier($type='year'){		
		$tax_summary = array();
		$tax_summary['revenue_breakdown']['sub_total'] = $this->numberFormat(39240.68,$type);
		$tax_summary['revenue_breakdown']['list'] =array(
			array(
				'name' => 'Delivery Income',
				'amount' => $this->numberFormat(39240.68,$type)
			)
			
		);
		$tax_summary['incentives_reimbursements']['sub_total'] = $this->numberFormat(2321.62,$type);
		$tax_summary['incentives_reimbursements']['list'] =array(
			array(
				'name' => 'Incentive (MPOIG)',
				'amount' => $this->numberFormat(722.56,$type)
			),
			array(
				'name' => "Incentive ('Schedule Urself')",
				'amount' => $this->numberFormat(1425.50,$type)
			),
			array(
				'name' => 'Platform Bonus',
				'amount' => $this->numberFormat(102.10,$type)
			),
			array(
				'name' => 'Delivery Reimbursement',
				'amount' => $this->numberFormat(23.56,$type)
			)
		);
		$tax_summary['expenditure_breakdown']['sub_total'] = $this->numberFormat(974.45,$type);
		$tax_summary['expenditure_breakdown']['list'] =array(
			array(
				'name' => 'Rental Fee',
				'amount' => $this->numberFormat(320.21,$type)
			),
			array(
				'name' => 'Equipment Purchase',
				'amount' => $this->numberFormat(150.55,$type)
			),
			array(
				'name' => 'Withdraw Fee',
				'amount' => $this->numberFormat(354.65,$type)
			),
			array(
				'name' => 'Other Platform Service',
				'amount' => $this->numberFormat(0.00,$type)
			),
			array(
				'name' => 'Delivery Refund',
				'amount' => $this->numberFormat(23.56,$type)
			),
			array(
				'name' => 'Liquidated Damage',
				'amount' => $this->numberFormat(134.53,$type)
			),
			
		);
		$tax_summary['gross_income'] = $this->numberFormat(40587.65,$type);
		$tax_summary['tax_title_date'] = $this->tax_title_date($type);
		$tax_summary['tax_date'] ='01 Aug 2019';// date('d M y',time());
		$tax_summary['url'] = $this->tax_courier_url($type);
		$tax_summary['type'] = $type;
		return $tax_summary;
		
	}
	
	private function tax_courier_url($type ='year'){
		$url = array();
		if($type == 'year'){	
			$url['title'] = 'URL OF TAX SUMMARIES';
			$list[0] = array(
				'name' => 'TAX SUMMARY 2018 Q3',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'TAX SUMMARY 2018 Q4',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'TAX SUMMARY 2019 Q1',
				'link' => '#'
			);
			$list[3] = array(
				'name' => 'TAX SUMMARY 2019 Q2',
				'link' => '#'
			);
		}
		if($type == 'quarter'){
			$url['title'] = 'URL OF TAX SUMMARIES';
			$list[0] = array(
				'name' => 'TAX SUMMARY JAN 2019',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'TAX SUMMARY FEB 2019',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'TAX SUMMARY MAR 2019',
				'link' => '#'
			);	
		}
		
		if($type == 'month'){
			$url['title'] = 'URL OF RECODES';
			$list[0] = array(
				'name' => 'DELIVERY INVOICES',
				'link' => '#'
			);
			$list[1] = array(
				'name' => 'ME CREDIT NOTES',
				'link' => '#'
			);
			$list[2] = array(
				'name' => 'REIMBURSEMENT INVOICES',
				'link' => '#'
			);
			$list[3] = array(
				'name' => 'REFUND CREDIT NOTES',
				'link' => '#'
			);	
		
			$list[4] = array(
				'name' => 'ME PLAFTORM INVOICES',
				'link' => '#'
			);	
			$list[5] = array(
				'name' => 'L.D RECEIPT',
				'link' => '#'
			);	
			
		}		
		$url['list'] = $list;		
		return $url;
		
		
	}
	  


	
	
	
	
	
	
	
	public function indexOp(){		
		$model = model('pdf');		
		//商家发出的票据 Online Sales Invoice 生成		
		$page = $_GET['type'];//.'tax_summary_courier'; 		
		$data = $this->info;		
		$model->createPdf($data,$page);	
		
		
	}


	public function invoice_listOp(){
		
		$list = array();
		$list[0] = array(
			'name' => '商家发出',
			'child' => array(
				array(
					'type' => '线上销售发票',
					'name' => 'Online Sales Invoice [收票方：顾客]',
					'act' => 'online_sales_invoice_customer'
				),
				array(
					'type' => '线上销售发票',
					'name' => 'Online Sales Invoice [收票方：配送员]',
					'act' => 'online_sales_invoice_courier'
				),
				array(
					'type' => '销售退款单【全单退款】',
					'name' => 'Sales Credit Note [收票方：顾客]',
					'act' => 'sales_credit_note_full'
				),
				array(
					'type' => '销售退款单【部分退款】',
					'name' => 'Sales Credit Note [收票方：顾客]',
					'act' => 'sales_credit_note_partial'
				),
				
				
				array(
					'type' => '销售费补贴发票',
					'name' => 'Sales Reimbursement Invoice [收票方：平台]',
					'act' => 'sales_reimbursement_invoice'
				),
				array(
					'type' => '优惠券使用补贴发票',
					'name' => 'Voucher Reimbursement Invoice [收票方：平台]',
					'act' => 'voucher_reimbursement_invoice'
				)				
			)
		);
		
		$list[1] = array(
			'name' => '配送员发出',
			'child' => array(
				array(
					'type' => '订单配送发票',
					'name' => 'Delivery Invoice [收票方：顾客]',
					'act' => 'delivery_invoice'
				),
				array(
					'type' => '订单配送退款单',
					'name' => 'Delivery Credit Note [收票方：顾客]',
					'act' => 'delivery_credit_note'
				),
				array(
					'type' => '配送费补贴发票',
					'name' => 'Delivery Fee Reimbursement Invoice [收票方：平台]',
					'act' => 'delivery_fee_reimbursement_invoice'
				),
				array(
					'type' => '激励金发票',
					'name' => 'Incentive Invoice [收票方：平台]',
					'act' => 'incentive_invoice'
				),
				
				
				array(
					'type' => '激励金退款单',
					'name' => 'Incentive Credit Note [收票方：平台]',
					'act' => 'incentive_credit_note'
				)
							
			)
		);
		
		$list[2] = array(
			'name' => '平台发出',
			'child' => array(
				array(
					'type' => '平台服务集成发票',
					'name' => 'ME Platform Invoice [收票方：商家]',
					'act' => 'me_platform_invoice_merchant'
				),
				array(
					'type' => '平台服务集成发票',
					'name' => 'ME Platform Invoice [收票方：配送员]',
					'act' => 'me_platform_invoice_courier'
				),
				array(
					'type' => '平台服务集成退款单',
					'name' => 'ME Platform Credit Note [收票方：商家]',
					'act' => 'me_platform_credit_note_merchant'
				),
				array(
					'type' => '平台服务集成退款单',
					'name' => 'ME Platform Credit Note [收票方：配送员]',
					'act' => 'me_platform_credit_note_courier'
				),
				
				
				array(
					'type' => '违约金收据',
					'name' => 'Liquidated Damage Receipt [收票方：商家]',
					'act' => 'liquidated_damage_receipt_merchant'
				),
				array(
					'type' => '违约金收据',
					'name' => 'Liquidated Damage Receipt [收票方：配送员]',
					'act' => 'liquidated_damage_receipt_courier'
				),
				array(
					'type' => '退款记录单',
					'name' => 'Refund Record [收票方：顾客]',
					'act' => 'refund_record'
				),
				array(
					'type' => '票据集成表[商家]',
					'name' => 'Tax Summary 年度 [收票方：商家]',
					'act' => 'tax_summary_merchant&date=year'
				),
				array(
					'type' => '票据集成表[商家]',
					'name' => 'Tax Summary 季度 [收票方：商家]',
					'act' => 'tax_summary_merchant&date=quarter'
				),
				array(
					'type' => '票据集成表[商家]',
					'name' => 'Tax Summary 月度 [收票方：商家]',
					'act' => 'tax_summary_merchant&date=month'
				),
				array(
					'type' => '票据集成表[配送员]',
					'name' => 'Tax Summary 年度 [收票方：配送员]',
					'act' => 'tax_summary_courier&date=year'
				),
				array(
					'type' => '票据集成表[配送员]',
					'name' => 'Tax Summary 季度 [收票方：配送员]',
					'act' => 'tax_summary_courier&date=quarter'
				),
				array(
					'type' => '票据集成表[配送员]',
					'name' => 'Tax Summary 月度 [收票方：配送员]',
					'act' => 'tax_summary_courier&date=month'
				)
							
			)
		);
		
		output_data(array('list' => $list));
		
	}
	
	


	
  
  
  
  
	public function rctiOp(){
			
			$model = model('pdf');		
			
			$page = 'rcti';
			
			$data  =array();
		
		
			$model->createPdf($data,$page);	
		
		
	}
  
	public function invoiceOp(){
			
			
		$order_id =  $_POST['order_id'];
		$invoice = $_POST['invoice'];
			
		$model = model('pdf');		
		$where = array(
			'order_id' => $order_id
		);
		$order = model('order')->getOrderInfo($where,array('store','order_goods','delivery'));
		
		
		$goods = $order['extend_order_goods'];
		$goods_list = array();
		foreach($goods as $k => $v){			
			
			
			$lang = $this->goodsLang($v);			
			$v['goods_name'] = $lang['goods_name'];					
		//	$v['goods_size'] = $lang['goods_size'];
			//$v['goods_spec'] = $lang['goods_spec'];
			
			$specs_data = array();
			if($lang['goods_size']){
				$specs_data[] = $lang['goods_size'];
			}
			
			if($lang['goods_spec']){
				$specs_data[] = $lang['goods_spec'];
			}			
			
			$v['goods_spec'] = implode("/", $specs_data);
			
			
			$v['unit_price'] = ncPriceFormat($v['goods_price'] * 0.9);
			$v['unit_gst'] = ncPriceFormat($v['goods_price'] * 0.1);						
			$goods_list[$k] = $v;			
			
		}	
		$order['eng_date'] = date('d M Y',$order['add_time']);		
		$order['goods_list'] = $goods_list;			
		$model->createPdf($order,$invoice);			
	}
  
  
  
	
	private function goodsLang($data){
		
			
			 //根据现有数据获得规格的key
			$where = array(
				'goods_id' => $data['goods_id'],
				'lang_name' => $data['goods_lang']
			);			
			//print_r($where);
			$current_data = model('goods_language')->getGoodsLangInfo($where);				
			//print_r($current_data);
			$current_sizeprice = unserialize($current_data['goods_sizeprice']);			
			if($data['goods_size'] != ''){			
				$size_key = $this->find_by_array_size($current_sizeprice,$data['goods_size']);			
			}
		
			$current_specs = unserialize($current_data['goods_specs']);
		
			//print_r($current_specs);
			
			$specs = explode('/',$data['goods_spec']);
			$specs_key = array();
			if(!empty($data['goods_spec'])){
				foreach($specs as $v){
					$specs_key[] = $this->find_by_array_spec($current_specs,$v);
				}
			}	
			
			
			//获取切换后的语言
			
	    	$data_lang = $this->goods_language($data['goods_id']);	
					
			//print_r($data_lang);
			
			$cart['goods_name'] = $data_lang['goods_name'];
			$lang_size = unserialize($data_lang['goods_sizeprice']);
			
			$cart['goods_size'] = $lang_size[$size_key]['name'];
			$lang_specs = unserialize($data_lang['goods_specs']);
			
			foreach($specs_key as $v){
				$specs_data[] = $lang_specs[$v]['value'];
			}		
			
			//print_r($specs_data);
			
			$cart['goods_spec'] = $specs_data ? implode("/", $specs_data) : '';
			$cart['goods_lang'] = $data_lang['lang_name'];	
		
			
			return $cart;
				
				
	}
  
	//查询语言参数
	private function goods_language($goods_id){		
			$where = array(
				'goods_id' =>$goods_id,
				'lang_id' => 2
			);
			$data = model('goods_language')->getGoodsLangInfo($where);
			if(!empty($data)){
				return $data;
			}else{
				return false;
			}
			
		}
		
  
	//获取当前值的KEY
	
	private function find_by_array_spec($array,$find)
	{
		foreach ($array as $key => $v)
		{
			if($v['value']==$find)
			{
				return $key;
			}
		}
	}
	
	private function find_by_array_size($array,$find)
	{
		foreach ($array as $key => $v){
			if($v['name']==$find)
			{
				return $key;
			}
		}
	}
  
  
  
  
  
    
}
