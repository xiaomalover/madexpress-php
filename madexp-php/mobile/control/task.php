<?php

/*
stripe
*/

defined('InMadExpress') or exit('Access Invalid!');
class taskControl extends mobileAdminControl {
   
    
    public function __construct(){
        parent::__construct();				
    }


	public function indexOp() {
        $this->listOp();
    }
	
	
	
	public function listOp()
	{
	    $list = Model()->table('task')->select();
		foreach($list as $k => $v){
				$v['starttime'] = date('Y-m-d',$v['starttime']);
				$v['lastrun'] = date('Y-m-d',$v['lastrun']);
				$v['endtime'] = date('Y-m-d',$v['endtime']);
				
				$data[$k] = $v;
				
			
			
		}
		output_data(array('list' => $data));
	
	}


}