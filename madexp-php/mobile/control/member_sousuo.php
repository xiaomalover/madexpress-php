<?php
/**
 * 我的银行卡

 */



defined('InMadExpress') or exit('Access Invalid!');

class member_sousuoControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
  
    }

    public function indexOp() {
		
		$keyword = $_POST['keyword'];
		$where = array();
		$where['goods_name']   = array('like','%'.$keyword.'%');		
		$list = model('goods_language')->getGoodsLangList($where);	
		$keydata = array();
		foreach($list as $k => $v){	
			$keydata[$k]['value'] = $v['goods_name'];
		
		}		
		
		for($i=0;$i<count($keydata)-1;$i++){
		    $source = $keydata[$i];
		    foreach($keydata as $k=>$v){
		        if($source['value'] == $v['value'] && $k!= $i){
		            unset($keydata[$k]);
		        }
		    }
		}
		
		$keydatas = array();
		foreach($keydata as $v){
		    $keydatas[] = $v;
		}
						
				
		$where = array();
		
		$where['store_name_primary|store_name_secondary'] = array('like','%'.$keyword.'%');
		
		$store_list = model('store')->getStoreList($where);
		$list = array();
		foreach($store_list as $k => $v){
						
									$list[$k]["store_id"] = $v['store_id'];
									


									$list[$k]["store_name_primary"] = $v['store_name_primary'];
									$list[$k]["store_name_secondary"] = $v['store_name_secondary'];
									$list[$k]["store_image"] = $v['store_avatar'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$v['store_avatar'] : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');	
				
									$list[$k]["store_cate_tag"] = $this->store_bind_class($v['store_id']);
									$list[$k]["store_score"] = $v['store_score'];
									$list[$k]["store_distance"] = '6.1'; //number_format($distance[$v['store_id']] /1000,1) ;
									$list[$k]["store_state"] = $v['store_state'];
									$list[$k]["store_follow"] = $this->store_follow($v['store_id']);								
								//	$list[$k]['store_goods'] = $this->store_search_goods($v['store_id'],$keyword);
				
			}
		
		output_data(array('keyword'=>$keydatas,'store'=>$list));
		     
    }


	private function unsetarr($list){

	    foreach($list as $k=>$v){
	
	        if($list[0]['value'] == $v['value']  && $k!=0){
	
	            unset($list[$k]);
	
	            $this->unsetarr($list);
	
	        }
	
	    }
	
	    return $list;
	
	}
	
	//店铺绑定的子类
	private function store_bind_class($store_id){
		$where = array(
			'store_id' =>  $store_id
		);
		$bind_class = model('store_bind_class')->getStoreBindClassList($where);
		$list = array();
		foreach($bind_class as $v){
			$list[]['tag_name'] = $v['class_name'];
		}
		return $list;
		
	}

	
	//检测该店铺是否被收藏	
	private function store_follow($id){	
		
		$where = array(
			'store_id' => $id,
			'member_id' => $this->member_info['member_id']
		);		
		$count = model('favorites')->getFavoritesCount($where);
		if($count > 0){			
			return 1;			
		}else{
			return 0;
		}		
	}


}
