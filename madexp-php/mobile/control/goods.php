<?php
/**
 * 商品
 */



defined('InMadExpress') or exit('Access Invalid!');
class goodsControl extends mobileMemberControl{
    private $PI = 3.14159265358979324;
    private $x_pi = 0;
    
    public function __construct() {
        $this->x_pi = 3.14159265358979324 * 3000.0 / 180.0;
        parent::__construct();
    }

  
	
	
    /**
     * 商品详细页
     */
    public function goodsDetailOp() {
				
        $goods_id = intval($_POST['goods_id']); 
        if($goods_id <= 0){
            output_error('商品不存在');
        }
        
		// 商品详细信息
        $model_goods = Model('goods');
        $goods_detail = $model_goods->getGoodsInfo(array('goods_id'=>$goods_id,'is_old' => 0));		
        if (empty($goods_detail)) {
            output_error('商品不存在');
        }
		$goods_detail = $this->_goods_detail_extend($goods_detail);				
      
        output_data($goods_detail);
    }

	//icon 
	private function goods_icon($icon_id){		
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		foreach($row as $k=> $v){			
			$data[$k] = $v;
			$data[$k]['attr_icon'] =  $v['attr_icon'];
			$data[$k]['attr_active_icon'] = $v['attr_active_icon'];
		}
		return $data;	
	}
	
	
    /**
     * 商品详细信息处理
     */
    private function _goods_detail_extend($goods_detail) {
        
		      $store_id = $goods_detail['store_id'];
			
		        $lang = $this->goods_language($goods_detail['goods_id']);
			
				$goods_detail['goods_name'] = $lang['goods_name'];	
				$goods_detail['goods_description'] = $lang['goods_description'];	
				$goods_detail['goods_specs'] = $this->getSpecsFormat($lang['goods_code'],$lang['lang_name']);	
				$goods_detail['goods_sizeprice'] = $this->getSizeFormat($lang['goods_code'],$lang['lang_name']) ;
				
				//  unserialize($lang['goods_sizeprice']);
				//$data[$k]['goods_optional'] = unserialize($lang['goods_optional']);	
			
			
				$goods_detail['goods_lang'] = $lang['lang_name'];				
				$goods_detail['goods_image'] = $this->goodsImageFormat($goods_detail['goods_image'],$store_id) ;		
				$goods_detail['goods_ingr'] = $this->goods_ingr($goods_detail['goods_ingr']);
				$goods_detail['goods_optional'] = $this->goodsOptional($goods_detail['goods_code'],$lang['lang_name']);
				
				//$goods_detail['cart_num'] = $this->cart_count($v['goods_id'],$store_id);
				
		    	//套餐规格
				$goods_detail['goods_meal'] = $this->getMealFormat($goods_detail['goods_code'],$lang['lang_name']);
				
				
		
		
                return $goods_detail;
    }
	
	
	//套餐规格
	private function getMealFormat($goods_code,$lang = 'CHN-S'){
		
		$model = model('store_goods_meal');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_parent_id'] = 0;
	
	   
	
		$list = $model->getMealList($where);
		$data = array();
	
		foreach($list as $k => $v){
		    
			$data[$k]['name'] = $v['meal_value'] .' (SELECT '.$v['meal_buy_num'].')';
			
			$data[$k]['buy_num'] = $v['meal_buy_num'];
			
			$data[$k]['list'] = $this->getMealChildFormat($v['meal_id'],$goods_code,$lang);
		}		
		
		return $data;
	}
	
	
	private function getMealChildFormat($meal_id,$goods_code,$lang){
		$model = model('store_goods_meal');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_parent_id'] = $meal_id;
		$list = $model->getMealList($where);
		$data = array();
		foreach($list as $k => $v){
			$data[$k]['name'] = $v['meal_value'];
			$data[$k]['size_name'] = $v['meal_size_name'];
			$data[$k]['specs_name'] = $v['meal_specs_name'];
			$data[$k]['meal_price'] = $v['meal_price'];
			$data[$k]['meal_id'] = $v['meal_id'];			
		}				
		return $data;
		
	}
	
	
	
	
	
	//格式化 size
	private function getSizeFormat($goods_code,$lang = 'CHN-S'){
		
		$model = model('store_goods_size');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSizeList($where);
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['name'] 		= $v['size_value'];
			$data[$k]['price'] 		= $v['size_price'];
			$data[$k]['sale_price'] = $v['size_sale_price'];
			$data[$k]['box_id'] 	= $v['size_box_id'];
			$data[$k]['size_id'] 	= $v['size_id'];
		}		
		return $data;
		
	}
	
	
	//格式化 specs
	private function getSpecsFormat($goods_code,$lang = 'CHN-S'){		
		
		$model = model('store_goods_specs');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['specs_type'] = 0;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){	
			$data[$k]['name'] = $v['specs_value'] .' (SELECT '. $v['specs_buy_num'].')';
			$data[$k]['buy_num'] = $v['specs_buy_num'];
			$data[$k]['options'] = $this->getSpecsChildFormat($v['specs_id'],$lang,$v['specs_value']);
		}		
		return $data;
		
	}
	
	private function getSpecsChildFormat($specs_id,$lang,$specs_name){
		
		$model = model('store_goods_specs');	
		$where = array();
		$where['specs_parent_id'] = $specs_id;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){						
			$data[$k]['name'] = $specs_name;			
			$data[$k]['value'] = $v['specs_value'];
			$data[$k]['price'] = $v['specs_price'];
			$data[$k]['specs_id'] = $v['specs_id'];
		}		
		return $data;
	}
	
	
	
	
	private function goodsOptional($code,$lang ='CHN-S'){
		   
		   $where = array();
		   $where['goods_code'] = $code;
		   $where['is_old'] = 0;
		 //  $where['goods_code_num'] = 0;
		   $options_list = model('store_goods_options')->getOptionsList($where);
		   $data = array();	   
		   $size_model = model('store_goods_size');
		   $specs_model = model('store_goods_specs');
		   $goods_lang_model = model('goods_language');
		   foreach($options_list as $k =>$v){			  
			   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang));
			   $data[$k]['size'] = $size['size_value'];		   
			   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
			   $specs_name = array();
			   foreach($specs as $vs){
				   $specs_name[] = $vs['specs_value'];
			   }
			   $specs_name = implode(',',$specs_name);
			   $data[$k]['specs'] = $specs_name;		   
			   $data[$k]['price'] = $v['options_price'];		   
			   $goods_name = $goods_lang_model->getGoodsLangInfo(array('goods_code' => $v['options_goods_code'],'lang_name' => $lang));		   
			   $data[$k]['name'] = $goods_name['goods_name'];
		//	   $data[$k]['goods_code'] = $v['options_goods_code'];
			   $data[$k]['id'] = $v['options_id'];
		   }
		   
		   return $data;
		   
		   
	}
	
	/*
	//获取配件商品
	private function goodsOptional($optional,$store_id){
		
		$data = unserialize($optional);		
		foreach($data as $v){
			$ids .=$v['options_id'].',';		
		}		
		$ids = substr($ids,0,-1);		
		$where = array();
		$where['options_id'] = array('in',$ids);
		$where['state'] = 0 ;
		$where['store_id'] = $store_id;
		
		
		$list = model('goods_options')->getOptionsList($where);
		
		$optional = array();
		foreach($list as  $k=>$v){
			$optional[$k]['id'] = $v['options_id'];
			$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$store_id);
			$optional[$k]['price'] = $v['options_price'];
			
		}
		
		return $optional;
		
	}
	
	*/
	

	//格式化配件商品的语言包
	private function optionsLang($data,$storeId){
		
	    $langInfo = model('language')->getLangInfo(array('language_id'=> $this->member_info['menu_language']));

	
		$data = unserialize($data);	
		foreach($data as $k => $v){
			$list[$v['lang_name']] = $v['options_name'];
		}	
		return $list[$langInfo['language_flag']];		
		
	}

	//格式化图片
	private function goodsImageFormat($image,$store_id){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['url'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS .$v;		
		}
		return $list;		
	}

	



	//格式化数组
	private function FormatData($format_data,$type = '',$attr_name){
		
		$data = array();
		
		$result = array(); 
		if(!is_array($format_data)){
			return array();
		}
		
		foreach ($format_data as $data) { 
			
			if($data['name'] != ''){
			  isset($result[$data['name']]) || $result[$data['name']] = array(); 
			  $result[$data['name']]['name'] = $data['name'].$attr_name;
			  
			  $arr['name'] = $data['name'];
				 if($type == 'specs'){ 
					 $arr['value'] = $data['value'];
				 }
			  $arr['price'] = $data['price'];
			  $result[$data['name']]['options'][] = $arr;
			}
			  
			  
		}
		
		$data = array();
		foreach($result as $v){
			$data[] = $v;
		}		
		return $data;
		
	}
	

		//获取商品语言
	private function goods_language($goods_id){		
		$where = array(
			'goods_id' =>$goods_id,
			'lang_id' => $this->member_info['menu_language'],
			'is_old' => 0
		);
		
	
		$data = model('goods_language')->getGoodsLangInfo($where);
		
		switch ($data['lang_id']) {
			case 1:
				$data['spec_attr_name'] = "(必选)";
				break;
			case 2:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
			case 3:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
		}	
		
		
		
		
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
		
	}


    /*
	//商品成分记录
	private function goods_ingr($icon_id){
		
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		
		foreach($row as $k=> $v){			
			$data[$k] = $v;
			$data[$k]['attr_icon'] =   UPLOAD_SITE_URL.DS. ATTACH_INGR . DS .$v['attr_icon'];
			$data[$k]['attr_active_icon'] =  UPLOAD_SITE_URL.DS. ATTACH_INGR . DS .$v['attr_active_icon'];
		}
		return $data;
	}
	*/
	
		//商品成分记录
	private function goods_ingr($icon_id){
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		foreach($row as $v){
			if($v['attr_type'] == 1){			
				$content = explode('|',$v['attr_content']);
				$v['attr_name'] = $content[0];
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[1];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[2];
			}else{
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
				
			}
			$data[$v['attr_class_id']]['name'] = $v['attr_class_name'];
			$data[$v['attr_class_id']]['child'][] = $v;
		}		
		
		$list = array();
		foreach($data as $v){
			$list[] = $v;
		}
		
		return $list;	
		
		
		
	}
	


    public function goods_evaluateOp() {
        $goods_id = intval($_GET['goods_id']);
        $type = intval($_GET['type']);

        $condition = array();
        $condition['geval_goodsid'] = $goods_id;
        switch ($type) {
            case '1':
                $condition['geval_scores'] = array('in', '5,4');
                break;
            case '2':
                $condition['geval_scores'] = array('in', '3,2');
                break;
            case '3':
                $condition['geval_scores'] = array('in', '1');
                break;
            case '4':
                $condition['geval_image|geval_image_again'] = array('neq', '');
                break;
            case '5':
                $condition['geval_content_again'] = array('neq', '');
                break;
        }
        
        //查询商品评分信息
        $model_evaluate_goods = Model("evaluate_goods");
        $goods_eval_list = $model_evaluate_goods->getEvaluateGoodsList($condition, 10);
        $goods_eval_list = Logic('member_evaluate')->evaluateListDity($goods_eval_list);

        $page_count = $model_evaluate_goods->gettotalpage();
        output_data(array('goods_eval_list' => $goods_eval_list), mobile_page($page_count));
    }



    /**
     * 经纬度转换
     * @param unknown $bdLat
     * @param unknown $bdLon
     * @return multitype:number
     */
    public function bd_decrypt($bdLat, $bdLon) {
        $x = $bdLon - 0.0065; $y = $bdLat - 0.006;
        $z = sqrt($x * $x + $y * $y) - 0.00002 * sin($y * $this->x_pi);
        $theta = atan2($y, $x) - 0.000003 * cos($x * $this->x_pi);
        $gcjLon = $z * cos($theta);
        $gcjLat = $z * sin($theta);
        return array('lat' => $gcjLat, 'lon' => $gcjLon);
    }

    /**
     *  @desc 根据两点间的经纬度计算距离
     *  @param float $lat 纬度值
     *  @param float $lng 经度值
     */
    private function getDistance($lat1, $lng1, $lat2, $lng2) {
        $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }
	private function parseDistance($num = 0){
		$num = floatval($num);
		if ($num >= 1000) {
			$num = $num/1000;
			return str_replace('.0','',number_format($num,1,'.','')).'km';
		} else {
			return $num.'m';
		}
	}
}
