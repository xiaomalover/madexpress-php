<?php
/**
 * 租车接口

 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_vehicleControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
    }
	
	
	//规则图弹窗
	public function vehicle_ruleOp(){
	    
	    $rule_text = '规则贵则规则规则最规则';
	    
	    output_data($rule_text);
	    
	}
	
	
	//收费规则
	
	public function vehicle_lease_ruleOp(){
	    
	    
	    $where = array(
	       'id' => $_POST['id']
	    );
	    $rental = model('VehicleRental')->rentalInfo($where);
	    
	    
	    output_data($rental);
	    
	}
	
	
	
	/*
	租车订单列表
	*/
	
	public function vehicle_order_listOp(){		
		
		$this->checkLeaseTime();
		
		$state = array(
			0 => '待支付',
			10 => '待确认',
			20 => '待取车',
			30 => '进行中',
			40 => '待付款',
			50 => '已完成',
			60 => '已评价',
			70 => '无效'
		);
		
		$model = model('vehicle_lease');		
		$where = array();
		
		if($_POST['state'] == 1){
			$where['lease_state'] = array('in','0,10,20,30,40,50');
		}
		
		if($_POST['state'] == 2){
			$where['lease_state'] = array('in','60,70');
		}
		
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		
		$ids = $this->getHeZu();
		
		if($ids){
			$where['lease_id'] = array('in',$ids);
		}
		
		$query_start_date = $_POST['start_date'];
		$query_end_date = $_POST['end_date'];
		
		$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_start_date);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_end_date);
        $start_unixtime = $if_start_date ? strtotime($query_start_date) : null;
        $end_unixtime = $if_end_date ? strtotime($query_end_date): null;
        if ($start_unixtime || $end_unixtime) {
            $where['lease_add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
	   // print_r($where);
		$list = $model->getLeaseList($where,'*',$this->page);	
		
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['start_time'] 	= date('d/m/y',$v['lease_start_time']);
			$data[$k]['end_time'] 		= date('d/m/y',$v['lease_end_time']);		
			$data[$k]['class_name'] 	= $v['vehicle_class_name'];
			$waiter = $this->waiter($v['distributor_id']);			
			$data[$k]['waiter_id'] = $waiter['distributor_id'];
			$data[$k]['waiter_name'] =  $waiter['distributor_name'];			
			$data[$k]['waiter_code'] =  $waiter['distributor_code'];		
		    $data[$k]['waiter_avatar'] = getWaiterAvatar($this->waiter_info['distributor_avatar']);	
			$data[$k]['lease_sn']    = $v['lease_sn'];
			$data[$k]['lease_id']    = $v['lease_id'];			
			$data[$k]['lease_state'] = $v['lease_state'];		
			$data[$k]['rule_name']   = $v['rule_name'];	
			$data[$k]['rule_id']   = $v['rule_id'];	
			$data[$k]['region_name']   = 'SE01';	
			$data[$k]['region_color']   = '#FFA4A4';	
			
		}
		
	    $page_count = $model->gettotalpage();  //检查是否有已过期的预约
		output_data(array('order_list'=>$data), mobile_page($page_count));
		
		
	}
	
	//获取我的合租订单
	private function getHeZu(){
			$where = array(
				'distributor_id' => $this->waiter($v['distributor_id'])
			);
			$model = model('vehicle_lease_cotenant');		
			$list = $model->getLeaseCotenantList($where);		
			foreach($list as $v){
				$lease_id = $v['lease_id'].',';
			}
			return substr($lease_id,0,-1);
	}
	
	
	//被动检查并更改状态
	
	private function checkLeaseTime(){
		$model = model('vehicle_lease');
		
		//基础金额 1天10元
		$where = array();
		$where['lease_end_time']  = array('gt',time());
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		$where['lease_state'] = 30;
		
		$update = array();
		$update['lease_state'] = 40;
		$model->editLease($where,$update);
				
	}
	
	
	//获取配送员详情
	
	private function waiter($waiter_id){
		$filed ='distributor_id,distributor_name,distributor_code,distributor_mobile';		
		$info = model('waiter')->getWaiterInfo(array('distributor_id'=>$waiter_id),$filed);		
		return $info;
	}
	
	
	
	
	
	public function vehicle_order_infoOp(){
		
		
		
		$state = array(
			0 => '待支付',
			10 => '待确认',
			20 => '待取车',
			30 => '进行中',
			40 => '待付款',
			50 => '已完成',
			60 => '已评价',
			70 => '无效'
		);
		
		
		$model = model('vehicle_lease');
		$where = array(
			'lease_id' => $_POST['lease_id']
		);
		$info = $model->getLeaseInfo($where);		
		$data = array();
		
		//基本信息				
		$lease_info = array();
		$lease_info['start_time'] = date('d/m/y',$info['lease_start_time']);
		$lease_info['end_time'] = date('d/m/y',$info['lease_end_time']);
		$lease_info['lease_state_name'] = $state[$info['lease_state']];
		$lease_info['vehicle_class'] = $info['vehicle_class_name'];
		$lease_info['lease_days'] = $info['lease_days'];
		$lease_info['rule_name'] = $info['rule_name'];
		$lease_info['rule_id']    = $info['rule_id'];
		$lease_info['region_name']   = 'SE01';	
		$lease_info['region_color']   = '#FFA4A4';	
		$data['lease_info'] = $lease_info;
		
		//主组人信息
		$lease_main = array();
		$lease_main['waiter_avatar'] = getWaiterAvatar($info['distributor_code']);
		$lease_main['waiter_name'] = $info['distributor_name'];
		$lease_main['waiter_id'] = $info['distributor_id'];
		$lease_main['waiter_code'] = $info['distributor_code'];
		$data['lease_main'] = $lease_main;
		
		
		//合租人		
		$data['lease_cotenant'] = $this->cotenant($info['lease_id']);		
		
		
		if($info['lease_handover_id'] > 0){		//交接人		
			
    				    //交接人信息
    	    $lease_handover = array();
    	    if( $info['lease_handover_id'] > 0){
        	    $lease_handover['lease_handover_id'] = $info['lease_handover_id'];
                $lease_handover['lease_handover_code'] = $info['lease_handover_code'];
                $lease_handover['lease_handover_address'] = $info['lease_handover_address'];
                $lease_handover['lease_handover_time'] = date('d/m/y',$info['lease_handover_time']);
                $lease_handover['lease_handover_type'] = $info['lease_handover_type'];
                  $lease_handover['lease_handover_avatar'] =  getWaiterAvatar($info['lease_handover_code']);
    	    }
    	    $data['lease_handover'] = $lease_handover;
    	    
			
			
			
		}else{
				$data['lease_conveyor'] = $this->conveyor();			
		}
		
		//押金
		$lease_deposit['lease_insure'] = 20;
		$lease_deposit['lease_deposit'] = 30;		
		$data['lease_deposit'] = $lease_deposit;
		
		
		if($info['vehicle_id'] > 0 ){	//车辆信息	
		
			
			$vehicle = $this->getVehicleInfo($info['vehicle_id']);			
			
			$lease_vehicle['vehicle_number'] = $vehicle['vehicle_number'];
			$lease_vehicle['vehicle_code'] = $vehicle['vehicle_code'];
			$lease_vehicle['vehicle_brand'] = $vehicle['vehicle_brand'];
			$data['lease_vehicle'] = $lease_vehicle;
		}
		
		//
		$lease_time_log = array();
		
		$lease_time_log = $this->lease_rule_log($info['lease_id']);
		
		
	//	print_r($lease_time_log);
		
		
		$data['lease_time_log']  = $lease_time_log;
		
		
		//评价信息
		if($info['lease_evaluation_id'] > 0){
		    
		    
		 //   print_r($info['lease_evaluation_id']);
			$data['lease_evaluate'] = $this->lease_evaluation($info['lease_evaluation_id']);
			
		}
		
		
	    //事件记录
	    $lease_event = array();
	    
	    
	    $lease_event = $this->lease_event($info['lease_id']);
	    
	    
	    $data['lease_event'] = $lease_event;
	    
	    

		
		//各种按钮状态
		$lease_pay['lease_state'] = $info['lease_state'];
		$lease_pay['lease_id'] = $info['lease_id'];		
		$data['lease_pay'] = $lease_pay;		
		$data['lease_state'] = $info['lease_state'];
		
		
		output_data($data);
		
	}
	
	//租车规则
	private function lease_rule_log($lease_id){
	    
	    $where =array();
	    $where['lease_id'] = $lease_id;
	    
	    $data = model('vehicle_lease_rule_log')->getLeaseRuleList($where);
	    
	 
	    return $data;
	}
	
	
	private function lease_event($lease_id){
	    
	    $where = array();
	    $where['lease_id'] = $lease_id;
	    $data = model('vehicle_event')->vehicleEventList($where);
	    foreach($data as $k => $v){
	        $v['add_time'] = date('d/m/y',$v['add_time']);
	        $list[$k] = $v;
	    }
	    
	    return $list;
	}
	
	
	//获取当前收车评价
	private function lease_evaluation($evaluate_id){
		
		$model = model('vehicle_lease_evaluate');
	//	print_r($evaluate_id);
		$data = $model->getLeaseEvaluateInfo(array('evaluate_id'=>$evaluate_id));
		if($data['evaluate_image']){
		   
		  
		   $image_list = explode(',',$data['evaluate_image']);
		   foreach($image_list as $v){
		       $image[] = UPLOAD_SITE_URL.DS.ATTACH_VEHICLE_EVALUATE.DS.$v;
		   }
		    
		    $data['evaluate_image'] = $image;
		}
		
	//	print_r($data);
		return  $data;
	}
	
	
	//合租人信息
	private function cotenant($id){
		
		$model = model('vehicle_lease_cotenant');		
		$where = array();
		$where['lease_id'] = $id;
		
		$list = $model->getLeaseCotenantList($where);	
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['waiter_avatar'] = getWaiterAvatar($v['distributor_code']);
			$data[$k]['waiter_id'] 	 = $v['distributor_id'];
			$data[$k]['waiter_name'] = $v['distributor_name'];
			$data[$k]['waiter_code'] = $v['distributor_code'];
			$data[$k]['lentime'] = $v['lentime'];
			$data[$k]['state'] = $v['state'];
		}	
		return $data;	
	}
	
	
	
	//确认交接人
	public function conveyroConOp(){
	
		$where = array(
			'distributor_id' => $_POST['waiter_id']
		);
		$waiter_info = model('waiter')->getWaiterInfo($where);
		if(empty($waiter_info)){
			output_error('交接人不存在');
		}	
		$where = array(
			'lease_id' => $_POST['lease_id']
		);			
		$data = array(		
					'lease_handover_id'   => $waiter_info['distributor_id'],
					'lease_handover_name' => $waiter_info['distributor_name'],
					'lease_handover_code' => $waiter_info['distributor_code'],
		);
		$info = model('vehicle_lease')->editLease($where,$data);	
		if($info){
		
			output_data('交接人已确认');	
		}else{
			output_data('交接人确认失败');	
		}
	}
	
	
	
	
	//交接人列表
	private function conveyor(){
		
		$model = model('vehicle_lease');		
		$where = array();
		$where['lease_state'] = array('in','50');
		
		$list = $model->getLeaseList($where,'*', 0, 'lease_id desc', 5);	
		$data = array();
		foreach($list as $k=> $v){	
			$data[$k]['lease_id'] = $v['lease_id'];		
			$data[$k]['waiter_avatar'] = getWaiterAvatar($v['distributor_code']);
			$data[$k]['waiter_id'] = $v['distributor_id'];
			$data[$k]['waiter_name'] = $v['distributor_name'];
			$data[$k]['waiter_code'] = $v['distributor_code'];
			$data[$k]['waiter_mobile'] = $v['distributor_mobile'];
		}		
		return $data;		
	}
		
		
		
		
	
	
	//获取车辆信息
	private function getVehicleInfo($vehicle_id){
		
		$where = array(
			'vehicle_id' => $vehicle_id
		);
		$vehicle = model('vehicle')->vehicleInfo($where);
	
		return $vehicle;
		
		
	}
	
	
	
	
	/*车辆分类*/
	
	public function vehicle_classOp(){		
		$where = array(
			'vehicle_class_state' => 1
		);
		$list = model('vehicle_class')->vehicleClassList($where);
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['class_id']  = $v['vehicle_class_id'];
			$data[$k]['class_name']  = $v['vehicle_class_name'];
			$data[$k]['class_icon']  =  UPLOAD_SITE_URL.'/icon/waiter/'.$v['vehicle_class_icon'];			
		}		
		output_data($data);		
	}
		
	
	
	
	
	
	/*配送员员搜索*/
	
	public function waiter_searchOp(){
		
		$keyword = $_POST['keyword'];
		
		$where = array();
		$where['distributor_name|distributor_code|distributor_mobile|distributor_truename'] = array('like','%'.$keyword.'%');		
		$field = 'distributor_id,distributor_name,distributor_truename,distributor_code,distributor_mobile,distributor_avatar';		
		$list = model('waiter')->getWaiterList($where,$field);
		$data  =array();
		foreach($list as $k => $v){		
			$data[$k]['waiter_id'] = $v['distributor_id'];
			$data[$k]['waiter_name'] = $v['distributor_name'];
			$data[$k]['waiter_code'] = $v['distributor_code'];
			$data[$k]['waiter_avatar'] = getWaiterAvatar($v['distributor_code']);;		
			$data[$k]['waiter_mobile'] = $v['distributor_mobile'];		
		}
		output_data($data);
	}
	
	
	
	
	
	
	/*选中配送员后通过ID获取详细信息*/
	
	public function selectWaiterOp(){		
		$ids = $_POST['ids'];	
		$where = array(
			'distributor_id' => array('in',$ids)
		);		

		$data = model('waiter')->getWaiterList($where);
		$list = array();
		foreach($data as $k => $v){
			$list[$k]['waiter_id'] = $v['distributor_id'];
			$list[$k]['waiter_name'] = $v['distributor_name'];
			$list[$k]['waiter_code'] = $v['distributor_code'];
			$list[$k]['waiter_avatar'] = $v['distributor_avatar'];		
			$list[$k]['waiter_mobile'] = $v['distributor_mobile'];	
		}
		output_data($list);
	}
	
	
	
	
	 public function makePaySn($waiter_id) {
        return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $waiter_id % 1000);
    }

	
	
	
	
	private function checkLeaseTimes($data) {
		
		 $where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'lease_state' => array('in','0,10,20,30,40')
		  );	
	      $list = model('vehicle_lease')->getLeaseList($where);	
		  
		 foreach($list as $v){
		   
		   $beginTime1= $v['lease_start_time'];
		   $endTime1= $v['lease_end_time'];
		   
		   $beginTime2= strtotime($data['start_time']);
		   $endTime2= strtotime($data['end_time']);
		
			  $status = $beginTime2 - $beginTime1;
			  if ($status > 0) {
				$status2 = $beginTime2 - $endTime1;
				if ($status2 >= 0) {
				  return 0;
				} else {
				  return 1;
				}
			  } else {
				$status2 = $endTime2 - $beginTime1;
				if ($status2 > 0) {
				  return 1;
				} else {
				  return 0;
				}
			  }
			  
			  
		  }
		  
		  
		  
		  
		}
	
	

	
	/*提交租车订单*/
	
	public function saveLeaseOp(){		
		
	 	$lease_model = model('vehicle_lease');	
		//			
		
		//检查当前是否有一个租车
		
		
		
		if($this->checkLeaseTimes($_POST) == 1){
			output_error('同时段内不可预约多辆');
		}		
		
		
		if($this->leaseCount() >= 2){			
			output_error('最多只能有两个正在执行租车计划');			
	    }	

		//验证是否存在重叠
		
		$data = array();
		$where =array(
			'vehicle_class_id' => $_POST['class_id']
		);				
		$vehicle_class = model('vehicle_class')->vehicleClassInfo($where);			
	
		$data['vehicle_class_id'] 		= $vehicle_class['vehicle_class_id'];
		$data['vehicle_class_name'] 	= $vehicle_class['vehicle_class_name'];		
		$data['distributor_id']  	= $this->waiter_info['distributor_id'];
		$data['distributor_name']	= $this->waiter_info['distributor_name'];
		$data['distributor_code']	= $this->waiter_info['distributor_code'];
		
		$data['lease_type'] 		= $_POST['lease_type'];
		$data['lease_start_time'] 	= strtotime($_POST['start_time']);
		$data['lease_end_time']		= strtotime($_POST['end_time']);		

		$data['lease_days'] = floor((strtotime($_POST['end_time']) - strtotime($_POST['start_time']))/86400);
		
		
		$data['lease_time_type']	= $_POST['lease_time_type'];
		$data['lease_start_address']= $_POST['start_address'];
		$data['lease_end_address']  = $_POST['end_address'];
		$data['alternate_address']  = $_POST['alternate_address'];
		$data['lease_state'] 		= 0;
		//$data['lease_days'] 		= 1;
		$data['lease_sn'] 			= $lease_model->makeSn();				
		$data['lease_cotenant'] = $_POST['waiter_ids'] != '' ? 1 : 0;
		
		$data['lease_add_time']	 = time();
		$data['lease_update_time']	 = time();
			
		$vehicle_deposit = 30; //押金。首次叫押金
		$lease_insure = 20 ;
		
		if($this->waiter_info['vehicle_deposit'] <= 0 ){ // 押金	
			$data['lease_insure'] =  $lease_insure;			
			$data['lease_first_amount'] = $vehicle_deposit + $lease_insure; //支付保险+押金的总额
		}else{
			$data['lease_insure'] =  $lease_insure;			
			$data['lease_first_amount'] = $lease_insure; //支付保险总额
		}		
		
		
		//租车计划
		
		$rental = model('VehicleRental')->rentalInfo(array('is_enable'=>1));
		if(!empty($rental)){			
			$rental_price =  $rental['base_rental'];//每天基础租金		
			$data['rule_name'] = $rental['name']; //支付保险总额
			$data['rule_id'] = $rental['id']; //支付保险总额
		}else{
			output_error('平台未设置租车计划，暂无法租车');	
		}
		//计算基础租车费用		
		//$price = 10;		
		$data['lease_last_amount'] = $data['lease_days'] * $rental_price;		
		
		//写入第一个支付单号
		$pay_sn = $this->makePaySn($this->waiter_info['distributor_id']);	
		
		$order_pay = array();
		$order_pay['pay_sn'] = $pay_sn;
		$order_pay['waiter_id'] = $this->waiter_info['distributor_id'];
		$order_pay['pay_type'] = 0;		
		$order_pay_id = $lease_model->addOrderPay($order_pay);
		$data['pay_sn_first'] = $pay_sn;
		
		$row = $lease_model->addLease($data);
		
		
		if($row){			
			
				$where = array(
						'distributor_id' => array('in',$_POST['waiter_ids'])
				);				
				$waiter_list = model('waiter')->getWaiterList($where);			
				foreach($waiter_list as $v){
					$waiter_data[] = array(
						'lease_id' => $row,
						'distributor_id' => $v['distributor_id'],
						'distributor_name' => $v['distributor_name'],
						'state' => 0,
						'distributor_code' => $v['distributor_code'],
						'addtime' => time()					
					);
				}				
				model('vehicle_lease_cotenant')->addLeaseCotenantAll($waiter_data);	
				
				output_data(array('lease_id'=>$row,'lease_sn'=>$data['lease_sn']));			
		}else{			
			output_error('写入失败');
		
		}
		
			
	}
	
	
	
	//检测当前有一个正在执行的祖册计划
	
	private function leaseCount(){		
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'lease_state' => array('in','0,10,20,30,40,50')
		);	
		$count = model('vehicle_lease')->getLeaseCount($where);		
		return $count;		
	}
		
	
	
	
	/*配送员确认*/
	
	public function leaseStateOp(){			
		//验证当前用户是否已确认
		
		$wid = $_POST['wid'];
		
		
		$where = array(
			'distributor_id' => $wid ,// $this->waiter_info['distributor_id'],
			'lease_id' => $_POST['lease_id']
		);	
				
		$row = model('vehicle_lease_cotenant')->getLeaseCotenantInfo($where);		
	
		if($row['state'] == 1){			
			output_error('您已确认，请勿重复点击');
		}
		
		//验证是否超过10分钟确认时间
		$where = array(
			'lease_id' => $_POST['lease_id']	
		);
		
		$lease = model('vehicle_lease')->getLeaseInfo($where);
		
		$endtime =  $lease['payment_time'] + 60;
	
		if(time() > $endtime){ //确认时间超过10分钟，则进入取消阶段
			
				$where = array(					
					'lease_id' => $_POST['lease_id']
				);
				$data = array(
					//'lease_content' => '合租人确认超时，本次租车取消',
					'lease_state'   => 70
				);			
				
				$info = model('vehicle_lease')->editLease($where,$data);	
				
				output_error('确认超时，本次租车已取消');	
						
		}
		
		
		$model = model('vehicle_lease_cotenant');
		$data = array(
			'state' => 1,
			'updatetime' => time()
		);
		
		$where = array(
			'distributor_id' => $wid , // $this->waiter_info['distributor_id'],
			'lease_id' => $_POST['lease_id']
		);				
		$info = $model-> editLeaseCotenant($where,$data);	
		if($info){
			
			//全部确认完毕后，修改状态为 20
			$where = array(
				'lease_id' => $_POST['lease_id'],
				'state' => 0
			);
			$count = $model->getLeaseCotenantCount($where);			
			if($count <= 0){
				//修改订单状态
				$where = array(
					'lease_id' => $_POST['lease_id']
				);				
				$update = array(
					'lease_state' => 20
				);
				model('vehicle_lease')->editLease($where,$update);
			}
			output_data($info);			
		}else{
			
			output_error('确认失败');
		}
		
		
	}
	
	
	//扫码取车
	public function leaseScanVehicleOp(){		
		
		$lease_id = $_POST['lease_id'];	
		$vehicle_code = $_POST['vehicle_code'];		
		$where = array(
			'vehicle_code'=> $vehicle_code
		);		
		$vehicle = model('vehicle')->vehicleInfo($where);	
		
		//检查车辆状态，空闲状态 可以直接扫码骑走
		
		if($vehicle['vehicle_state'] == 10){
						
			$data = array(
				'vehicle_id' => $vehicle['vehicle_id'],
				'vehicle_code' => $vehicle['vehicle_code'],
				'vehicle_state' => 1,
				'lease_state' => 30 //切换到正常用车状态
			);
			$where = array(			
				'lease_id' => $lease_id
			);
			$row = model('vehicle_lease')->editLease($where,$data);	
			
			//变更车辆状态
			if($row){				
				
				$data = array(
					'vehicle_state' => 20,
					'lease_id' => $lease_id
				);
				
				$where = array(
					'vehicle_id' => $vehicle['vehicle_id']
				);
				
				//变更车辆
				model('vehicle')->editVehicle($where,$data);
              
				$data = array();
				$data['vehicle_code'] = $vehicle['vehicle_code'];
				$data['vehicle_number'] = $vehicle['vehicle_number'];
				output_data(array('vehicle'=>$data));										
			}
			
	  }else{
			
			output_error('车辆异常');
			
			
		}
		
	}
	
	
	
	//记录合租人信息	
	public function add_cotenantOp(){
		
		$list = $_SESSION['list'];
		
		if(count($list) > 2){			
			output_error('合租人最多3人');			
		}		
		
		$list[$_POST['waiter_id']] = $_POST['waiter_id'];
		
		$_SESSION['list'] = $list;
			
		//记录		
		output_data('ok');
		
		
	}
	
	//合租人列表
	public function list_contenantOp(){
		
		$list = $_SESSION['list'];
		foreach($list as $v){			
			$ids .= $v.',';
		}		
		$where = array(
			'distributor_id' => array('in',$ids)
		);
		$waiter_list = model('waiter')->getWaiterList($where);
		$waiter = array();
		foreach($waiter_list as $k=> $v){
			
			$waiter[$k]['distributor_id'] = $v['distributor_id'];
			$waiter[$k]['distributor_name'] = $v['distributor_name'];
			$waiter[$k]['distributor_code'] = $v['distributor_code'];
			$waiter[$k]['distributor_mobile'] = $v['distributor_mobile'];
			$waiter[$k]['distributor_avatar'] = $v['distributor_avatar'];
			
			
		}
		
		output_data($waiter);
		
	}
	
	
		
		/*
	最近合租人
	*/
	
	public function lately_cotenantOp(){
		
	$where = array();
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		$where['lease_type'] = 1;		
		$lease_list = model('vehicle_lease')->getLeaseList($where,'lease_id');
	//	print_r($lease_list);
		
		if(empty($lease_list)){
			output_data(array());
		}
		$lease_ids = array();
		
		foreach($lease_list as $v){			
			$lease_ids[] =  $v['lease_id'];			
		}		
		$lease_ids = implode(',',$lease_ids);		
		$model = model('vehicle_lease_cotenant');		
		$where = array();
		$where['lease_id'] = array('in',$lease_ids);		
		
	//	print_r($where);
		
		$data = $model->getLeaseCotenantList($where,'distributor_id,distributor_name,distributor_code',0,'id desc','distributor_id');
		
		$list = array();
		foreach($data as $k =>$v){
		    
		    $list[$k]['waiter_id'] = $v['distributor_id'];
			$list[$k]['waiter_name'] = $v['distributor_name'];
			$list[$k]['waiter_code'] = $v['distributor_code'];
			$list[$k]['waiter_avatar'] = getWaiterAvatar($v['distributor_code']);
			$list[$k]['waiter_mobile'] = $v['distributor_mobile'];	
		    
			
		}
		output_data($list);		
		
		
	}
	
	
	
	
	//收车评价
	
	public function vehicle_evaluateOp(){
		
		$obj_validate = new Validate();
        $obj_validate->validateparam = array(       
            array("input"=>$_POST["lease_id"],"require"=>"true","message"=>'租车订单异常')
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }
		
		
		$where = array(
			'lease_id' => $_POST['lease_id']
		);
		
		$lease = model('vehicle_lease')->getLeaseInfo($where);
	
		if(empty($lease)){
			output_error('租车订单不存在');
		}
		
		
		
				
		$data = array();
		$data['vehicle_id'] = $lease['vehicle_id'];
		$data['distributor_id'] =  $this->waiter_info['distributor_id'];
		$data['distributor_name'] = $this->waiter_info['distributor_name'];
		$data['lease_id'] = $lease['lease_id'];
		$data['is_damage'] = $_POST['is_damage'];
		
		//有无损坏 1 有损坏，接受信息
		if($_POST['is_damage'] == 1){
			$data['evaluate_content'] = $_POST['evaluate_content'];
			$data['evaluate_image'] = $_POST['evaluate_image'];
		}
		
		
		$data['is_charging'] =  $_POST['is_charging'];
		$data['is_hygiene'] =  $_POST['is_hygiene'];
		$data['evaluate_other'] = $_POST['evaluate_other'];
		$data['addtime'] = time();
		
		$row = model('vehicle_lease_evaluate')->addLeaseEvaluate($data);		
		if($row){			
			output_data(array('evaluate_id'=>$row));			
		}else{
			output_error('收车失败');			
		}
		
		
	}
	
	
	
	public function vehicle_uploadOp(){
		
		
			$complain_pic = array();
	        $complain_pic[1] = 'file1';
	        $complain_pic[2] = 'file2';
	        $complain_pic[3] = 'file3';
	    	$complain_pic[4] = 'file4';
	    	$complain_pic[5] = 'file5';
	    	$complain_pic[6] = 'file6';
	    	$complain_pic[7] = 'file7';
	    	$complain_pic[8] = 'file8';
	    	$complain_pic[9] = 'file9';
	      
	        $pic_name = array();
	        $upload = new UploadFile();
	        $uploaddir = ATTACH_VEHICLE_EVALUATE;
	        $upload->set('default_dir',$uploaddir);
	        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
	        $count = 1;
	        foreach($complain_pic as $pic) {
	            if (!empty($_FILES[$pic]['name'])){
	                $result = $upload->upfile($pic);
	                if ($result){
	                    $pic_name[] = $upload->file_name;
	                    $upload->file_name = '';
	                } else {
	                    $pic_name[] = '';
	                }
	            }
	            $count++;
	        }
	        
	        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.ATTACH_VEHICLE_EVALUATE.'/'));
	        
	        
				
	}
	

	
	
	
	
	
}
