<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_loginControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 登录
     */
    public function loginOp(){
		
		if(empty($_POST['username']) || empty($_POST['password'])) {          
			output_error('账号密码不能为空');
		}
		
        $model_waiter = Model('waiter');
        $login_info = array();
        $login_info['username'] = $_POST['username'];
        $login_info['password'] = $_POST['password'];	
	
        $waiter_info = $model_waiter->login($login_info);
       
		
		if(!$waiter_info) {           	
			output_error('没有这个账号哦');
        }		
		
		
		if($waiter_info['login_password'] != md5($_POST['password'])){ 
			output_error('账号和密码不匹配哦');
		}
		
		
		if(isset($waiter_info['error'])) {
            output_error($waiter_info['error']);       
		} else {
            $token = $this->_get_token($waiter_info['distributor_id'], $waiter_info['login_name'], 'mobile');
            if($token) {
                output_data(array('username' => $waiter_info['login_name'], 'waiter_id' => $waiter_info['distributor_id'], 'key' => $token));
            } else {
                output_error('登录失败');  
            }
        }
    }

    /**
     * 登录生成token
     */
    private function _get_token($member_id, $member_name, $client) {
        $model_mb_waiter_token = Model('mb_waiter_token');
      
        //生成新的token
        $mb_waiter_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_waiter_token_info['waiter_id'] = $member_id;
        $mb_waiter_token_info['waiter_name'] = $member_name;
        $mb_waiter_token_info['token'] = $token;
        $mb_waiter_token_info['login_time'] = TIMESTAMP;
        $mb_waiter_token_info['client_type'] = $client;
        $result = $model_mb_waiter_token->addMbWaiterToken($mb_waiter_token_info);
        if($result) {
            return $token;
        } else {
            return null;
        }

    }

   
	 
	
	
	
}
