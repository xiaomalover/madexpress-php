<?php
/**
 * 收货人

 */



defined('InMadExpress') or exit('Access Invalid!');

class member_consigneeControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
        $this->model_consignee = Model('consignee');
    }

    /**
     * 收货人列表
     */
    public function consigneeListOp() {
			
		$attr = $this->getAttr();
        $consignee_list = $this->model_consignee->getConsigneeList(array('member_id'=>$this->member_info['member_id']));
		$data = array();
		foreach($consignee_list as $k => $v){
			$data[$k] = $v;
			$data[$k]['attr_name'] = $attr[$v['attr_id']]['attr_name'];
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['attr_id']]['attr_icon'];
		}
		
        output_data(array('consignee_list' => $data));
    }

	
	
	private function getAttr($id = 0){
		
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);
		$data = array();
		foreach($list as $k=> $v){
			$data[$v['attr_id']] = $v;
			$data[$v['attr_id']]['action'] = $v['attr_id'] == $id ? 1: 0;
			
			
		}
		
		return $data;
	}

	
	
    /**
     * 收货人详细信息
     */
    public function consigneeInfoOp() {
		
		
		
		
		$data = array();
		$data['consignee_info'] = array();
      	if($_POST['consignee_id'] > 0){
	
			$consignee_id = intval($_POST['consignee_id']);
			$condition = array();
			$condition['consignee_id'] = $consignee_id;
			$consignee_info = $this->model_consignee->getConsigneeInfo($condition);
			$data['consignee_info'] = $consignee_info;
			
		}
		
		$data_attr = model('consignee_attr')->getConsigneeAttrList(TRUE);
		foreach($data_attr as $k=> $v){
			$attr[$k] = $v;
			$attr[$k]['action'] = $v['attr_id'] == $consignee_info['attr_id'] ? 1: 0;			
			$attr[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];
			
		}
		$data['attr'] = $attr;

        if($data) {
            output_data($data);
        } else {
            output_error('收货人不存在');
        }
    }

    /**
     * 删除收货人
     */
    public function consignee_delOp() {
        $consignee_id = intval($_POST['consignee_id']);

        $condition = array();
        $condition['consignee_id'] = $consignee_id;
        $condition['member_id'] = $this->member_info['member_id'];
        $this->model_consignee->delConsignee($condition);
        output_data('ok');
    }

    /**
     * 新增收货人
     */
    public function consignee_addOp() {
        $consignee_info = $this->_consignee_valid();

		$where = array('member_id'=> $this->member_info['member_id']);	
		$this->model_consignee->editConsignee($where,array('is_default'=> 0));
        $result = $this->model_consignee->addConsignee($consignee_info);
        if($result) {
            
            
            
            
            
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }

    /**
     * 编辑收货人
     */
    public function consigneeSaveOp() {
		
        $consignee_id = intval($_POST['consignee_id']);
		$consignee_info = $this->_consignee_valid();
        
		
		
		if($consignee_id > 0){			
			$where =  array(
				'consignee_id'=> $consignee_id,
				'member_id'=> $this->member_info['member_id']
			);
			
			$result = $this->model_consignee->editConsignee($where,$consignee_info);
			
		}else{
			$result = $this->model_consignee->addConsignee($consignee_info);			
		}	
		
        if($result) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }

    private function _consignee_attr(){

        $model_consignee_attr = model('consignee_attr');

        $list = $model_consignee_attr->getConsigneeAttr(TRUE);
        $data = array();
        foreach($list as $v){
            $data[$v['attr_id']] =$v;
        }
        return $data;

    }



    /**
     * 验证收获人数据
     */
    private function _consignee_valid() {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["consignee_name"],"require"=>"true","message"=>'收货人姓名不能为空'),
            array("input"=>$_POST["consignee_mobile"],"require"=>"true","message"=>'收货人电话号码不能为空'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }
		if(!preg_match('/^0?(13|15|17|18|14)[0-9]{9}$/i',   $_POST['consignee_mobile'])){		 	
			output_error('请输入正确手机号');			
		}

        $data = array();
        $data['member_id'] = $this->member_info['member_id'];
        $data['consignee_name'] = $_POST['consignee_name'];
        $data['consignee_mobile'] = $_POST['consignee_mobile']; 
        $data['attr_id'] = $_POST['attr_id'];    
		$data['attr_name'] = $_POST['attr_name'];    
		
		      
        return $data;
    }

}
