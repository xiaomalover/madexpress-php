<?php
/**
 * 商家注销

 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_logoutControl extends mobileWaiterControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 注销
     */
    public function indexOp(){
        if(empty($_POST['login_name']) || !in_array('android', $this->client_type_array)) {
            output_error('参数错误');
        }

        $model_mb_waiter_token = Model('mb_waiter_token');
        if($this->waiter_info['login_name'] == $_POST['login_name']) {
            $condition = array();
            $condition['waiter_id'] = $this->waiter_info['distributor_id'];
            $model_mb_waiter_token->delMbWaiterToken($condition);
            output_data('1');
        } else {
            output_error('参数错误');
        }
    }

}
