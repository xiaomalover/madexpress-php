<?php
/**
 * 我的反馈
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_schedulingControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
    }


    public function ruleOp(){
        
        $desc = "内容内容内容内容内容内容内容内容内容内容内容内容内容内容内容";
        output_data(array('desc' => $desc,'pdf_url' => '1.pdf'));
        
    }

    /**
     * 添加反馈
     */
    public function indexOp() {		
        	
			$model = Model('scheduling');		
			$where = array();
			$where['distributor_id'] = $this->waiter_info['distributor_id'];		
			
			if($_POST['type'] <= 0){				
				output_error('参数错误');				
			}
			
			if($_POST['type'] == 1){
				$where['scheduling_status'] = array('in','10,20');
			}
		
			if($_POST['type'] == 2){
				$where['scheduling_status'] = array('in','30,60');				
			}
			
			if($_POST['type'] == 3){
				$where['scheduling_status'] = array('in','40,50');				
			}
			
	
			
	
			$start_unixtime = strtotime($_POST['start_time']);
			$end_unixtime = strtotime($_POST['end_time']);
			if ($start_unixtime || $end_unixtime) {
				$where['scheduling_date'] = array('time',array($start_unixtime,$end_unixtime));
			}

			
			
			$status = array(
				'10' => '待上班',
				'20' => '进行中',
				'30' => '已完成',
				'40' => '已取消',
				'50' => '未达标',
				'60' => '迟到'
				
				
			);
		
		 
			$reigon = $this->region();
			$data = $model->getSchedulingList($where);
			$list = array();
			$tt = $this->tt();
					
			foreach($data as $k=> $v){				
				//$list[$k] = $v;
				$list[$k]['id'] = $v['scheduling_id'];
				$region = $reigon[$v['region_id']];				
				$list[$k]['region_name'] = $region['region_name'];
				$list[$k]['region_color'] = $region['region_color'];
				$time =$this->work($v['scheduling_date']);				
				$list[$k]['day'] = $time['day'];
				$list[$k]['mon'] = $time['mon'];
				$list[$k]['work'] = $time['work'];				
				$list[$k]['name'] = $v['scheduling_t_name'];
				$list[$k]['time'] = $tt[$v['scheduling_t_id']];				
				$list[$k]['status'] = $status[$v['scheduling_status']];
			}	
			$count = count($list);				
			output_data(array('list'=> $list,'total'=>$count));
						
    }
	
	//获取当前周几
	private function work($date){
		
		$arr = array('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
		$work['day'] = date('d',$date);
		$work['mon']  =  date('M',$date) ;
		$work['work'] = $arr[date('w',$date)];
		$work['year']  =  date('y',$date) ;
		return $work;
		
	}
	
	private function tt(){
		
		$list =model('scheduling_setting')->getSchedulingNew(TRUE);
		$data;
		foreach($list as $v){
			$data[$v['id']]	 = date('H:s',$v['start_time']).'-' .date('H:s',$v['end_time']);
		}
		return $data;	
		
	}
	
	//本次上班详情
	public function infoOp(){
		
		
			$status = array(
				'10' => '待上班',
				'20' => '进行中',
				'30' => '已完成',
				'40' => '已取消'
				
				
			);
		
		
		
		$model = Model('scheduling');		
		$where = array(
			'scheduling_id' => $_POST['id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		
		$region = $this->region();
		$data = $model->getSchedulingInfo($where);
		
		$scheduling =array();
		$scheduling['scheduling_id']= $data['scheduling_id'];
		
		$region = $region[$data['region_id']];	
		
		$scheduling['region_name'] = $region['region_name'];
		$scheduling['region_color'] = $region['region_color'];
		$scheduling['status'] = $status[$data['scheduling_status']];
		$scheduling['scheduling_status'] = $data['scheduling_status'];	
		$scheduling['name'] = $data['scheduling_t_name'];	
		$scheduling['scheduling_date'] = $this->workN($data['scheduling_date']);		
		$scheduling['scheduling_addtime'] =  date('d/m/y H:i:s',$data['scheduling_addtime']);
	
		$scheduling['scheduling_start_time'] = date('H:i:s',$data['scheduling_start_time']);
		$scheduling['scheduling_end_time'] = date('H:i:s',$data['scheduling_end_time']);
		
		$scheduling['scheduling_sign_time'] = $data['scheduling_sign_time'] > 0 ? date('d/m/y H:i:s',$data['scheduling_sign_time']) : 'N/A';
		$scheduling['scheduling_signout_time'] = $data['scheduling_signout_time'] > 0 ? date('H:i:s',$data['scheduling_signout_time']) : 'N/A';
	
		
		
		$scheduling['scheduling_rest'] = $this->restLog($data['scheduling_id']);
		$scheduling['scheduling_sign_address'] = $data['scheduling_sign_address'];		
		$scheduling['vehicle'] = $this->vehicle($data['distributor_id']);
		
		$scheduling['scheduling_price'] = $data['scheduling_price'];
		$scheduling['scheduling_time_num'] = $data['scheduling_time_num'];
		
		
		$scheduling['total_amount'] = $data['total_amount'];	
		$scheduling['pay_state'] = $data['pay_state'];		
		$scheduling['total_time'] = $data['total_time'];		
		$scheduling['nav_coordinate'] = $data['nav_coordinate'];		
		
		$scheduling['income_guarantee'] = $data['scheduling_price'];
		$scheduling['current_income'] = $data['scheduling_status'] == 30 ? '31.00':'N/A';
		$scheduling['su_incentive'] =  $data['scheduling_status'] == 30 ? '29.00':'N/A';
		
		$scheduling['scheduling_sn'] = $data['scheduling_sn'];
	
        /*	
		$condition_log = array(
		    array(
		        'name' => '在规定时间上线',
		        'state' => 1,
		        'desc' => '在规定时间上线'
		    ),array(
		        'name' => '离线累计不超过10分钟',
		        'state' => 0,
		        'desc' => '离线累计不超过10分钟'
		    ),array(
		        'name' => '按照规定休息',
		        'state' => 0,
		        'desc' => '按照规定休息'
		    ),array(
		        'name' => '实在想不出来了',
		        'state' => 1,
		        'desc' => '实在想不出来了'
		    ),array(
		        'name' => '这行凑数的',
		        'state' => 0,
		        'desc' => '这行凑数的'
		    )
		);
		
	
		*/

		$task_log = model('scheduling_task_log')->getTaskLogList(array('sch_id' => $data['scheduling_id']));
	
	  //  print_r($task_log);
		$condition_log = array();
		foreach($task_log as $k => $v){
		    
			$condition_log[$k]['name'] = $v['name'];			
			$condition_log[$k]['state'] = $v['value'];
			$condition_log[$k]['desc'] = $v['content'];
			
		}
		
		    $scheduling['condition_log'] = 	$condition_log;
		
		    output_data($scheduling);
		
	}
	
	private function vehicle($waiter_id){
		if($this->waiter_info['is_vehicle'] == 1){
			return '自有车辆 M-Bike';
		}else{
			//查询是否有正在租车的订单
			return '';	
		}
		
		
		
		
	}
	
	private function workN($date){
		
		$arr = array('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
		
		$work = $arr[date('w',$date)] .' '. date('d',$date) .' '. date('M',$date)  ;
		
		return $work;
		
	}
	
	
	//新增排班
	
	public function initOp(){
		$model = model('region');
		$data = $model->getRegionList(array('region_parent_id'=>0),'region_id,region_name,region_color');
		$list = array();
		foreach($data as $k => $v){	 			
			$child = $this->getRegionList($v['region_id']);	
			if(count($child) > 0){
				$v['child'] = $child;
				$list[] = $v;				
			}				
		}
		
		output_data($list);
		
	}
	
	private function getRegionList($id){
		
		//获取已开启钱标排班的区域
		$region_list = model('region')->getRegionList(array('region_parent_id'=> $id,'region_enable' => 1,'is_money' => 1));		
		
		foreach ($region_list as $k => $v) {
		    $data[$k]['region_id'] 		= $v['region_id'];
		    $data[$k]['region_name'] 	= $v['region_name'];
		    $data[$k]['region_color'] 	= $v['region_color'];	    		  
		}
		
		return $data;
		
		
		
		
	}
	
	
	
	//根据地区和时间来查询当前区域剩余时间段
	
	public function workingOp(){
		
		$datetime = explode('-',$_POST['datetime']);
		//获取是去
		$where = array(
			'region_id' => $_POST['region_id'],
			'year' => $datetime[0],
			'month' => $datetime[1],
			'day' => $datetime[2],
		);
		
		$data = model('scheduling_region')->getSchedulingList($where);
		$list = array();
		foreach($data as $k => $v){
		        $info = model('scheduling_setting')->getSchedulingInfo(array('id' => $v['setting_id']));
				$list[$k]['id'] =   $v['id'];				
				$list[$k]['num'] = $this->countScheduling($v['num'],$v['id']) ;				
				$list[$k]['name'] =  $info['name'];	
				$list[$k]['start_time'] = $info['start_time_date'];
				$list[$k]['end_time'] = $info['end_time_date'];
		}
		
		output_data($list);		
	}
	
	//统计剩余量
	private  function countScheduling($num,$tid){
		$where = array(
			'scheduling_status'	 => array('in','10,20,30'),
			'scheduling_t_id' => $tid,
			'scheduling_date' => strtotime($_POST['datetime']),
			'region_id' => $_POST['region_id']
		);
		$count = model('scheduling')->countScheduling($where);		
		return $num - $count;
	}
	
	
	
	//申请排班
	public function addOp(){		
	
		$model = model('scheduling');
		$obj_validate = new Validate();
        $obj_validate->validateparam = array(       
            array("input"=>$_POST["region_id"],"require"=>"true","message"=>'请选择区域'),
			array("input"=>$_POST["select_date"],"require"=>"true","message"=>'请选择日期'),
			array("input"=>$_POST["work_id"],"require"=>"true","message"=>'请选择时段')           
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }
		
		if($this->waiter_info['is_lock'] == 1){
			output_error('账号已被禁用，无法申请排班。');
		}
	
	
	
		//验证当前区域是否有剩余时间
		//if(!$this->checkLeaseSh($_POST)){			
			//	output_error('合租人不得预约同一个时间点'); 			
	//	}
		
		
		//验证是否有租车计划
		// if(!$this->checkVehicle()){
		//	output_error('您当前暂无可用配送车辆'); 
		//}
				
		//验证当前是否可以申请			
		$where = array(
			'scheduling_status' => array('in','10,20'),
			'distributor_id' => $this->waiter_info['distributor_id']
		);
				
		$count = $model->countScheduling($where);		
		if($count >= 5){
			output_error('最多可申请5个排班记录');
		}
		
			//调用当前区域详细设置
		$work_info = model('scheduling_region')->getSchedulingInfo(array('id' =>$_POST['work_id']));
		
		
		$data = array();		
		$data['distributor_id'] = $this->waiter_info['distributor_id'];
		$data['distributor_name'] = $this->waiter_info['distributor_name'];
		
		$data['region_id'] = $_POST['region_id'];
		$big_region = model('region')->getRegionInfo(array('region_id'=>$_POST['region_id']));
	
		$data['region_big_id'] = $big_region['region_parent_id'];
		$data['scheduling_t_id'] = $work_info['setting_id'];
		
		
	
		
		
		//调用当前时区
		$where  =array(
			'id' => $work_info['setting_id']
		);
		$t = model('scheduling_setting')->getSchedulingInfo($where);	
		$region = model('region')->getRegionInfo(array('region_id'=>$_POST['region_id']));
	
		$data['scheduling_sn'] = 'AUSC01'.date('dmy',time()).$t['name'].$region['region_name'].rand(10,99);
	    
		$data['region_name'] = $region['region_name'];
		$data['region_color'] = $region['region_color'];
		$data['scheduling_t_name'] = $t['name'];

		$data['scheduling_price'] = $work_info['pa_bike_price'];
		$data['scheduling_time_num'] = $t['wages'];
		$data['scheduling_start_time'] = strtotime($_POST['select_date'].' '.$t['start_time_date']);
	    $data['scheduling_end_time'] = strtotime($_POST['select_date'].' '.$t['end_time_date']);
		$data['scheduling_addtime'] = time();
		$data['scheduling_date'] = strtotime($_POST['select_date']);		
		$data['nav_coordinate'] = $big_region['region_nav_coordinate'];
		
		
		//检查重复排班申请		
		$where = array(
			'scheduling_t_id' =>$data['scheduling_t_id'],
			//'region_id' => $data['region_id'],
			'scheduling_date' => $data['scheduling_date'],
			'distributor_id' => $data['distributor_id']			
		);
		$count = $model->countScheduling($where);
	
		if($count > 0){
			output_error('请勿重复预约同一时间段');
		}
		
		$row = $model->addScheduling($data);
		
		if($row){	
		    
		    //获取本次排班的任务记录
			$task = model('scheduling_task')->getTaskList(array('enable' => 1));
		//print_r($task);
			   
			foreach($task as $k => $v){	
			    $task_list = array();
				$task_list['task_id'] = $v['id'];
				$task_list['sch_id'] = $row;
				$task_list['code'] = $v['code'];
				$task_list['name'] = $v['name'];
				$task_list['content'] = $v['content'];
				$task_list['addtime'] = time();
				$task_log_data[] = $task_list;
				
			}			
			
			//print_r($task_list);
			model('scheduling_task_log')->addTaskLogAll($task_log_data);
		    
			model('scheduling_region')->editScheduling(['m_bike' => $work_info['m_bike'] - 1,'num' => $work_info['num'] - 1]);
			
			
		    
		    
			output_data('申请成功');			
		}else{
			output_error('申请失败');			
		}
		
		
	}
	
	
	//验证当前执行的排版下是否有人预约了相同时间段
	private function checkLeaseSh($data){
		
		$where['lease_state'] = 40;
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		$info  = model('vehicle_lease')->getLeaseInfo($where);	
	//	$ids =$info['distributor_id'].',';
		$ids =  $this->getHeZu($info['lease_id']);
		
		$where = array(
			'distributor_id' => array('in',$ids),
			'region_id' => $data['region_id'],
			'select_date' => $data['select_date'],
			'work_id' => $data['work_id']
			
		);
		$count = model('scheduling')->countScheduling($where);
		if($count > 0){
			return false;	
		}else{
			return  true;
		}		
		
	}
	
	
	//验证是否有车辆，或者租车计划
	private function checkVehicle(){
		if($this->waiter_info['is_vehicle'] == 1){ //配送员有自己车辆不需要租赁		
				return true;
		}else{
			
			//检查配送员当前是否有执行中的租车计划
			$lease_ids = $this->getHeZu(); //先获取合租的订单
			$where =array();
			$where['lease_state'] = 40;
			$where['distributor_id'] = $this->waiter_info['distributor_id'];
			if($lease_ids !=''){
					$where['lease_id'] = array('in',$lease_ids);
			}
			$list = model('vehicle_lease')->getLeaseCount($where);	
			if($list > 0){
				return true;
			}else{
				return false;
			}
			
		}
		
	}
	
	
	
		//获取我的合租订单
	private function getHeZu($lease_id){
			$where = array(
				'distributor_id' =>  $this->waiter_info['distributor_id'],
				'lease_id' => $lease_id
			);
			$model = model('vehicle_lease_cotenant');		
			$list = $model->getLeaseCotenantList($where);		
			foreach($list as $v){
				$lease_id = $v['lease_id'].',';
			}
			return substr($lease_id,0,-1);
	}
	
	
	
	
	//申请上班
	public function stateOp(){
		
		//检查是否到达指定区域
		
		
		
		//修改状态。开始上班		
		$data = array(
			'scheduling_sign_time' => time(),
			'scheduling_status' => 20
		);		
		$where = array(
			'scheduling_id' => $_POST['id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		$result = model('scheduling')->editScheduling($data);		
		if($result){			
			output_data('已上班');			
		}else{
			output_error('上班失败');			
		}
				
	}
	
	
	//获取5天的日期
	public function dayOp(){
		
		$day = $this->get_weeks();
		foreach($day as $k => $v){			
			$data[$k]['en_time'] = $v;
			
			$work = $this->work(strtotime($v));		 
			
			$data[$k]['week'] = $work['work'].' '.$work['day'] .' '. $work['mon'] .' '. $work['year'];
			
				
		}
		
		output_data($data);
		
		
		
	}
	
	private function get_weeks($time = '', $format='d M y'){
		  $time = $time != '' ? $time : time();
		  //组合数据
		  $date = array();
		  for ($i=0; $i<=4; $i++){
			$date[$i] = date($format ,strtotime( '+' . $i .' days', $time));
		  }
		  return $date;
	}
	
	
	
	
	//获取地区数据
	private function region(){
		
		$data = model('region')->getRegionList(TRUE);
		$region=array();
        foreach ($data as $v) {
            $region[$v['region_id']] = $v;
        }
        return $region;
		
		
		
	}
	
	private function restLog($id){		
		$where = array(
			'scheduling_id' => $id
		);		
		$data = model('scheduling_log')->getSchedulingLogList($where);	
		$rest = array();
		foreach($data as $k=> $v){
			$rest[$k] = $v;
			$rest[$k]['log_start_time'] = date('H:i:s',$v['log_start_time']);
			$rest[$k]['log_end_time'] = date('H:i:s',$v['log_end_time']);
		}
		return $rest;			
	}
	
	
	//触发计划任务此处代码仅供测试用
	public function jihuaOp(){
		
		
		$order_amount = $_POST['order_amount'];
		
		$where = array(
			'scheduling_status' => 30,
			'pay_state' => 0,
			'scheduling_id' => $_POST['id']
		);
		$model = model('scheduling');
	
		$info = $model -> getSchedulingInfo($where);
		if(!empty($info)){
			
			
			$zhuT = $this->minute($info['scheduling_signout_time'],$info['scheduling_sign_time']);
			$restT = $this->restTotle($info['scheduling_id']);
			$total_time = $zhuT - $restT;
			
			$min_price = round( $info['scheduling_price'] / $info['scheduling_time_num'] ,2); //每分钟的价格
			
			$total_amount = round($total_time * $min_price,2);
			
			
			$updata = array(
				'total_amount' =>  $order_amount > $total_amount ? $order_amount : $total_amount ,
				'total_time' => $total_time,			
			);
			$model->editScheduling($updata,$where);			
		
			$this->pd($info['scheduling_id']);
		
			output_data($updata);
		
			
		}
		
		
	}
	
	
	private function restTotle($id){
		
			$where = array(
				'scheduling_id' => $id
			);
			$list = model('scheduling_log')->getSchedulingLogList($where);
			$time = 0;
		
			foreach($list as $v){
				$time += $v['total_time'];
			}	
		
			return floor($time /60);
			
		
	}
	
	
	private function minute($enddate,$startdate){		
		$minute=floor(($enddate-$startdate)%86400/60);
		return $minute;
		
	}
	
	
	private  function pd($id){
		
		$model_pd = Model('waiter_wallet');		
		$model_sch = model('scheduling');	
		try {
			
			$model_pd->beginTransaction();
			$info= $model_sch->getSchedulingInfo(array('scheduling_id'=>$id));			
			$data = array();
			$data['distributor_id'] = $info['distributor_id'];
			$data['distributor_name'] = $info['distributor_name'];
			$data['amount'] = $info['total_amount'];
			$data['order_sn'] = $info['scheduling_sn'];
			
			
	
			
			$model_pd->changePd('bill_scheduling',$data);
			$model_pd->commit();
			output_data(array('status'=>'ok'));
		} catch (Exception $e) {
			$model_pd->rollback();
			output_error('系统繁忙，提交失败');
			
		}
		
		
	}
	
	
	
	public function chidaoOp(){	
		$where = array(
			'scheduling_id' => $_POST['id']
		);
		$model = model('scheduling');	
		$info = $model -> getSchedulingInfo($where);	
		if(!empty($info)){			
			
			$kk = $this->minute($info['scheduling_end_time'],time());
			if($kk > 60){								
					$minute = $this->minute(time(),$info['scheduling_start_time']);			
					if($minute > 5){
						output_data('您已迟到：'.$minute - 5);
					}else{
						output_data($minute);
					}						
			}else{
				
				output_data('排版已取消');
			}
			
			
			
			
			
		}
	}
	
	
    public function vehicle_depotOp(){
    	    
    		
    		$model = model('vehicle_depot');
    	    
    		$result  = $model->getDepotList(TRUE);
    		
    	    
    	   
    	   output_data($result);
    	    
    	    
    	    
    	}
	
	
}
