<?php
/*
	twilio相关的服务接口
*/
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VoiceGrant;
use Twilio\TwiML\VoiceResponse;

defined('InMadExpress') or exit('Access Invalid!');

class twilioControl {
   
    private $client;
    public function __construct(){
		
		//parent::__construct();	
		//框架整体有冲入，需提前写在之前的
		$oldFunctions = spl_autoload_functions();//返回所有已注册的__autoload()函数。
		// 逐个卸载
		if ($oldFunctions){
			foreach ($oldFunctions as $f) {
				spl_autoload_unregister($f);//注销已注册的__autoload()函数
			}
		}

		require BASE_PATH.DS.'api'.DS.'twilio-php/src/Twilio/autoload.php';	
		
		//定义twilio相关账号
		$this->account_sid = 'AC0ffdb60ca31a05e89fe3fc3b0fe53610';
		$this->auth_token = '67b9da801f56e63d40cc8686c1f5c252';
		$this->twilio_number = "+61730500666";
		

		//js-sdk客户端配置
		$this->twilioApiKey = 'SKcd9b97d7ec8400f8b63bc02757ca0cee';
		$this->twilioApiSecret = '6e3EHYGMQkvHTufQfDWaJqlSRVHP26wZ';

		// Required for Voice grant
		$this->outgoingApplicationSid = 'AP951c5d7853c9e87db31fa0927139f696';
		// An identifier for your app - can be anything you'd like
		$this->identity = "MadExpress";

		//proxy服务
		//不需要动态创建，直接获取目前的1个server，放配置中
		//$services = $this->client->proxy->v1->services->read(1);
		$this->services_sid="KSbbe40815018d001cf22d020beb0db298";
		$this->proxy_phone = "+61742436699";
		


		$this->client = new Client($this->account_sid, $this->auth_token);

		// 解决twilio框架冲突，再将原来的自动加载函数放回去
		if ($oldFunctions){
			foreach ($oldFunctions as $f) {
				spl_autoload_register($f);
			}
		}

    }


	//这个是为twilio的回调函数
	//jssdk，js客户端，网页post数据到这里，这样就可以网页打开麦克风，拨打电话了(post请求)
	public function callresponseOp(){
		//购买者的电话
		$call_phone = $_POST['call_phone'];

		//Start our TwiML response
		$response = new VoiceResponse;
		
	   //生成对应的xml文件
		$response->dial("+".$call_phone,array('callerId' => $this->twilio_number));
		echo $response;exit;
	}
	

	//给客户端提供的token信息
	public function getclienttokenOp()
	{
		
		// Create access token, which we will serialize and send to the client
		$token = new AccessToken(
			$this->account_sid,
			$this->twilioApiKey,
			$this->twilioApiSecret,
			3600,
			$this->identity
		);

		// Create Voice grant
		$voiceGrant = new VoiceGrant();
		$voiceGrant->setOutgoingApplicationSid($this->outgoingApplicationSid);

		// Optional: add to allow incoming calls
		$voiceGrant->setIncomingAllow(true);

		// Add grant to token
		$token->addGrant($voiceGrant);

		// render token to string
		$data = $token->toJWT();
		output_data($data);
	}


	//客户向骑手拨打电话
	public function membercallOp(){	
		$order_id = intval($_POST['order_id']);
		$this->getproxyIdentifier($order_id);
	}	

	//骑手向客户拨打电话
	public function waitercallOp(){		
		$order_id = intval($_POST['order_id']);
		$this->getproxyIdentifier($order_id);
	}

	//私有方式，处理为两个手机处理代理手机号,返回数据的映射关系
	private function getproxyIdentifier($order_id){	
		
		$model_order = Model('order');
        $condition = array();
        $condition['order_id'] =$order_id;
        $order_info = $model_order->getOrderInfo($condition);
	    
	    //临时测试
		//$order_info['distributor_mobile']="+61432775796";
		//$order_info['buyer_phone']="+61459666011";
		
		//获取订单的配送员电话
		if(empty($order_info['distributor_mobile']))
		{
			output_error('配送员手机号为空，无法拨号');			
		}
		//获取订单的购买人电话
		if(empty($order_info['buyer_phone']))
		{
			output_error('购买者手机号为空，无法拨号');			
		}

		$distributor_mobile = $order_info['distributor_mobile'];  
		$buyer_phone = $order_info['buyer_phone'];


		//处理 twilio
		//获取目前创建的proxy服务
		//不需要动态创建，直接获取目前的1个server，放配置中
		//$services = $this->client->proxy->v1->services->read(1);
		$services_sid=$this->services_sid;
		$proxy_phone = $this->proxy_phone;


		$map_w=array();
		$condition = array();
        $condition['order_id'] = $order_id;
        
        $order_twilio_map_info = model('order_twilio_map')->getordertwiliomap($condition);

		if(empty($order_twilio_map_info))
		{
			//1个订单创建1个session
			try {
				$session = $this->client->proxy->v1->services($services_sid)->sessions->create(["uniqueName" => "session_order_".$order_id]);
			} catch (Exception $e) {
				output_error('失败了，原因:'.$e->getMessage());			
			}
			$session_id = $session->sid;


			//建立参与方1
			try {
				$participant = $this->client->proxy->v1->services($services_sid)->sessions($session_id)->participants->create($distributor_mobile, ["friendlyName" => "distributor","proxyIdentifier"=>$proxy_phone]);
			} catch (Exception $e) {
				output_error('失败了，原因:'.$e->getMessage());			
			}

			//建立参与方2
			try {
				$participant = $this->client->proxy->v1->services($services_sid)->sessions($session_id)->participants->create($buyer_phone,["friendlyName" => "buyer","proxyIdentifier"=>$proxy_phone]);
			} catch (Exception $e) {
				output_error('失败了，原因:'.$e->getMessage());			
			}

			$data=array();
			$data['order_id']=$order_id;
			$data['services_sid']=$services_sid;
			$data['session_id']=$session_id;
			$data['distributor_mobile']=$distributor_mobile;
			$data['buyer_phone']=$buyer_phone;
			$data['proxyIdentifier']=$proxy_phone;

			model('order_twilio_map')->add_order_twilio_map($data);
			output_data($data);
		}
		else
		{
			output_data($order_twilio_map_info);
		}
	}


	//删除session
	public function deletesessionOp(){		
		$order_id = intval($_POST['order_id']);
		
		$condition = array();
        $condition['order_id'] = $order_id;
		$order_twilio_map_info = model('order_twilio_map')->getordertwiliomap($condition);
		
		if($order_twilio_map_info['session_id'])
		{
			//删除twilio的session
			$this->client->proxy->v1->services($this->services_sid)->sessions($order_twilio_map_info['session_id'])->delete();
		}

		//删除映射关系表记录
		model('order_twilio_map')->deleteorder_twilio_map($order_twilio_map_info['id']);

		$data=array();
		output_data($data);
	}
	



	/*
	//发送邮件
	public function sendemailOp(){	
		try {
			$verification = $this->client->verify->v2->services("VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
				   ->verifications
				   ->create("recipient@foo.com", // to
							"email", // channel
							[
								"channelConfiguration" => [
									"template_id" => "d-4f7abxxxxxxxxxxxx",
									"from" => "override@example.com",
									"from_name" => "Override Name"
								]
							]
				   );

			output_data('发送模板邮件成功');
		} catch (Exception $e) {

			//echo 'Error: ' . $e->getMessage();
			output_error('失败了，原因:'.$e->getMessage());			
		}
	}

	//验证邮件
	public function verificationmailOp(){	
		try {

			$verification_check = $this->client->verify->v2->services("VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
				 ->verificationChecks
				 ->create("123456", // code
						  ["to" => "recipient@foo.com"]
				 );

			output_data('邮件验证成功');
		} catch (Exception $e) {

			//echo 'Error: ' . $e->getMessage();
			output_error('失败了，原因:'.$e->getMessage());			
		}
	}


	//发送短信验证
	public function sendsmsOp(){	
		try {

			$verification = $this->client->verify->v2->services("VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                                   ->verifications
                                   ->create("+15017122661", "sms");

			output_data('发送短信验证成功');
		} catch (Exception $e) {

			//echo 'Error: ' . $e->getMessage();
			output_error('失败了，原因:'.$e->getMessage());			
		}
	}


	//发送短信验证
	public function verificationsmsOp(){	
		try {

			$verification_check = $this->client->verify->v2->services("VAXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                                         ->verificationChecks
                                         ->create("1234", // code
                                                  ["to" => "+15017122661"]
                                         );
			print($verification_check->status);


			output_data('验证短信成功');
		} catch (Exception $e) {

			//echo 'Error: ' . $e->getMessage();
			output_error('失败了，原因:'.$e->getMessage());			
		}
	}

	
	

	
	//发送信息到某个手机号
	public function send_smsOp(){				
		$phone= $_POST['phone']; 	//请求的手机号
		
		//var_dump($this->client);exit;
		$this->client->messages->create(
			// phone:  send a text message (your cell phone)
			$phone,
			array(
				'from' => $this->twilio_number,
				'body' => 'I sent this message in under 10 minutes!'
			)
		);

		output_data('发送短信成功');
	}	


	//获取录音通话记录
	public function calls_getOp(){	
		
		$sid= $_POST['sid']; 	//请求的手机号
		
		$call = $this->client->calls($sid)->fetch();
		
		//处理call记录

		$list=array();
		output_data(array('list'=>$list ));				
	}

	*/
}