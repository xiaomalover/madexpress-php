<?php
/**
 * 商家首页

 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_indexControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 商家中心
     */
    public function indexOp() {
  
	    $store_info = array();
        $store_info['store_id'] = $this->store_info['store_id'];
        $store_info['store_name_primary'] = $this->store_info['store_name_primary'];
		$store_info['store_name_secondary'] = $this->store_info['store_name_secondary'];
		$store_info['store_address'] = $this->store_info['store_address'];
		$store_info['store_phone'] = $this->store_info['store_phone'];
	
		$store_shophours = $this->store_info['store_shophours'];
		
		$store_info['store_shophours'] = unserialize($store_shophours);
		$store_info['store_shophours_type'] = $this->store_info['store_shophours_type'];
		$store_info['store_about'] = $this->store_info['store_about'];
		$store_info['store_notice'] = $this->store_info['store_notice'];
		$store_info['store_state'] = $this->store_info['store_state'];
		$store_info['store_online'] = $this->store_info['store_online'];
		$store_info['store_tag'] = $this->storeTagClass($this->store_info['store_id']);
	    $store_info['store_price'] = $this->store_info['available_predeposit'];
		$store_info['store_login_name'] = $this->store_info['store_login_name'];
		$store_info['store_taking_type'] = $this->store_info['store_taking_type'];
		
		//爆单开关
		$store_info['store_busy_protection'] = $this->store_info['store_busy_protection'];
		
		//店铺头像
        $store_info['store_avatar'] = getStoreLogo($store_info['store_avatar'], 'store_avatar');     
		
        output_data(array('store_info' => $store_info));
		
		
    }

	private function storeTagClass($store_id){		
		$where = array(
			'store_id' => $store_id
		);		
		$tag = model('store_bind_class')->getStoreBindClassList($where);
		$data = array();
		foreach($tag as $k=> $v){			
			$data[$k] = $v;
		}		
		return $data;
	}
	
	
	
	
	/*
	爆单开关按钮
	*/
	
	public function store_busy_protectionOp(){
		
		$where=  array();
		$where['store_id'] = $this->store_info['store_id'];
				
		$update = array();
		$update['store_busy_protection'] = $_POST['state'];
		$row = model('store')->editStore($where,$update);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}	
	
	
	
	
	//店铺语言
	public function storeLangOp(){
			
		$where = array(
			'store_id' => $this->store_info['store_id']
		);	
		
		$lang = model('store_language')->getStoreLangList($where);
		foreach($lang as $k=> $v){
			$lang[$k] =$v;
			$lang[$k]['language_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['language_icon'];			
			
		}
		
	     output_data(array('language_list'=>$lang));
		
		
	}
	
	public function storeLangSaveOp(){
		
			
		model('store_language')->editStoreLang(array('is_default'=> 0),array('store_id'=>$this->store_info['store_id']));
		
		$where = array(
			'store_id' => $this->store_info['store_id'],
			'language_id' => $_POST['language_id']
		);			
		$data = array(
			'is_default' => 1
		);		
		
		$row = model('store_language')->editStoreLang($data,$where);
		if($row){
			
			model('store')->editStore(array('is_default_lang'=> $_POST['language_id']),array('store_id'=>$this->store_info['store_id']));		
				
			output_data('ok');
		}else{
			output_error('操作失败');
		}
				
		
		
		
		
		
		
	}
	
	/*
		修改商户状态
	*/
	
	public function  storeOnlineStateOp(){
		
		$state = $this->store_info['online_state'];		
		$store_time = $this->store_info['store_shophours'];
		
//		if($state == 1){
//		
//			$time = explode(';',$store_time);
//			$store_time = array();
//			foreach($time as $v){
//				$store_time[] = explode('-',$v);
//			
//			}
//			
//			foreach($store_time as $v){
//				
//				if($v[0]){
//					
//					
//				}
//				
//			}
//			
//			
//		}
//	

//		print_r($store_time);
//		
//		exit;
//		
//		
//		
		
		
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		
		if($this->store_info['store_online'] == 0){
		    $state  = 1;
		    
		    $msg = '店铺上线成功';
		    
		}else{
		    $state  = 0;
		   
		    $msg = '店铺下线成功';
		}
		
		$update = array(
			'store_online' => $state 
		);
		

		$row = model('store')->editStore($update,$where);
		
		
		if($row){
			
			output_data($msg);
		}else{
			output_error('操作失败');
			
		}
				
		
		
	}
	
	public function editTimeOp(){
		
		$type =  $_POST['type'];
		
		$update = array(		
			'store_shophours' => $_POST['time'],
			'store_shophours_type' => $_POST['type']			
		);		
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		
		$row = model('store')->editStore($update,$where);	
			
		if($row){
			output_data('ok');
		}else{
			output_error('操作失败');
		}
	}
	
	
  
  	public function  bindOp(){	
  	    
			$stoket = model('stoket');
			$mid = $this->store_info['store_id'];		
			
			$cid = $_POST['cid'];				
			$stoket->bind($cid,$mid);
			
			$where = array(
				'client_id' => $cid
			);
			
			$data = array(
				'mid' => $mid
			);
			
			$row = $stoket->editStoket($where,$data);		
			if($row){
			    output_data('绑定成功');    
			}else{
			    output_error('绑定失败');
			}
			
	}
  
	
	
	//切换语言
	public function  system_langOp(){
        $where = array(
            'store_id' => $this->store_info['store_id']    
        );
        $update = array();
        $update['system_language'] = $_POST["lang"];
    	$update = model('store')->editStore($update,$where);	
		if($update){
			output_data('切换成功');					
		}else{
			output_error('切换失败');	
		}		
	    
	    
	}
	
	
	
		
		//绑定设备号
		public function bind_tokenOp(){		
			
			
			$data = array();
			
			if($_POST['ios_device_token']){
				$data['ios_device_token'] = $_POST['ios_device_token'];
			}
			
			if($_POST['android_device_token']){
				$data['android_device_token'] = $_POST['android_device_token'];
			}				
			
			$update = model('store')->editStore($data,array( 'store_id' => $this->store_info['store_id']    ));
			if($update){
				output_data('绑定成功');					
			}else{
				output_error('保存失败');	
			}		
		}
	
	
	
	
	
	
}
