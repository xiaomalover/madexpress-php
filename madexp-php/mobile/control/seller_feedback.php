<?php
/**
 * 我的反馈
 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_feedbackControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加反馈
     */
    public function feedback_addOp() {
		
        $model_feedback = Model('feedback');
      
		$param = array();
        $param['feedback_content'] = $_POST['content'];
        $param['user_type'] = 2;
        $param['feedback_code'] = $this->feedbackCode();
		$param['feedback_type'] = $_POST['feedback_type'];		
		$param['feedback_type_id'] = $_POST['feedback_type_id'];
        $param['feedback_time'] = TIMESTAMP;
        $param['user_id']   = $this->store_info['store_id'];
        $param['user_name'] = $this->store_info['store_name_primary'];
		$param['feedback_file'] = $_POST["feedback_file"];	
		
	//	print_r($this->store_info);
	//	print_r($param);
        $result = $model_feedback->addFeedback($param);
        
        if($result) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }
	
	private function feedbackCode(){
		
		$length = 7;
		$count = model('feedback')->getFeedBackCount(TRUE);
        $len = $length - strlen($count+1);
        $code = strtoupper('MEO').str_pad($count+1,$len,0,STR_PAD_LEFT);
        return $code;
	   
		
	}
	
	public function feedback_typeOp(){
	
	        $model_feedback = Model('feedback');
	        
	        $type_list = $model_feedback->getFeedBackType(array('type_user' => 2,'type_parent_id' => 0));
	        
	        foreach($type_list as $v){
	         
	            $v['child'] = $model_feedback->getFeedBackType(array('type_parent_id' => $v['type_id']));
	            
	            $type[] =  $v;
	            
	        }
	    
			
			output_data($type);
		
	}
	
	
	
	public function updateFileOp(){
		
		
			$complain_pic = array();
	        $complain_pic[1] = 'file1';
	        $complain_pic[2] = 'file2';
	        $complain_pic[3] = 'file3';
	    	$complain_pic[4] = 'file4';
	    	$complain_pic[5] = 'file5';
	    	$complain_pic[6] = 'file6';
	    	$complain_pic[7] = 'file7';
	    	$complain_pic[8] = 'file8';
	    	$complain_pic[9] = 'file9';
	      
	        $pic_name = array();
	        $upload = new UploadFile();
	        $uploaddir = ATTACH_FEEDBACK;
	        $upload->set('default_dir',$uploaddir);
	        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
	        $count = 1;
	        foreach($complain_pic as $pic) {
	            if (!empty($_FILES[$pic]['name'])){
	                $result = $upload->upfile($pic);
	                if ($result){
	                    $pic_name[$count] = $upload->file_name;
	                    $upload->file_name = '';
	                } else {
	                    $pic_name[$count] = '';
	                }
	            }
	            $count++;
	        }
	        
	        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'));
	        
	        
				
	}
	
	
	
}
