<?php
/**
 * 商家商品管理
 *
 *

 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_goodsControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }
	
	
	public function indexOp(){		
		$this->goodsListOp();
	}
	
	
	
	
	

    public function goods_addOp() {		
		
		
		 $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["goods_name"],"require"=>"true","message"=>'商品名称不能为空'),
                array("input"=>$_POST["gc_id"],"require"=>"true","validator"=>"Number","message"=>'店铺分类不能为空'),
               
   
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                 output_error($error);
            }
		
		
		
		$data = array();
		$data['goods_name'] 	= $_POST['goods_name'];
		$data['gc_id'] 			= $_POST['gc_id'];
		$data['gc_name'] 		= $_POST['gc_name'];
		$data['store_id'] 		= $this->store_info['store_id'];
		$data['store_name'] 	= $this->store_info['store_name'];
		$data['goods_body'] 	= $_POST['goods_body'];		
		$data['goods_attr'] 	= serialize($_SESSION['goods_attr']);	//获取属性
		$data['goods_spec'] 	= serialize($_SESSION['goods_spec']); //获取规格		
		
		
		$data['goods_stock'] 	= $_POST['goods_stock']; //每日库存
		
		$data['goods_addtime'] 	=  time();
		$data['goods_price'] 	= $_SESSION['goods_spec'][0]['shop_price'];
		$data['goods_image'] 	= $_POST['goods_image'];
		$data['is_recommend'] 	= $_POST['is_recommend'];
		
		$icon = $_SESSION['goods_icon'];
		$goods_icon = '';
		foreach($icon as $v){
			$goods_icon .= $v.',';
		}				
		$data['goods_icon'] = $goods_icon;		
		//print_r($data);
		
		$row = model('goods')->addGoods($data);
		
		if($row){	
			
			$_SESSION['goods_attr'] = array();
			$_SESSION['goods_spec'] = array();
			$_SESSION['goods_icon'] = array();			
			
		     output_data(array('goods_id' => $row));
		}else{
			output_error('新增失败');
		}
		
     
    }

	//格式化当前规格数据
	public function setSpecSessionOp(){
		
		$data = array();		
		
		$data[] = array(
			'type' => $_POST['type_one_value'],
			'stock_id' => $_POST['foodbox_one_value'],
			'shop_price' => ncPriceFormat($_POST['shop_price_one'])
		);		
		
		$data[] =array(			
			'type' => $_POST['type_two_value'],
			'stock_id' => $_POST['foodbox_two_value'],
			'shop_price' => ncPriceFormat($_POST['shop_price_two'])		
		
		);		
		
		$_SESSION['goods_spec'] = $data;
		output_data('ok');
		
	}
	
	public function getSpecSessionOp(){		
		$spec = $_SESSION['goods_spec'];		
		output_data($spec);		
	}
	
	
	//记录口味种类，	
	public function setAttrSessionOp(){
		
		if( is_array($_SESSION['goods_attr'])){			
				$data  = $_SESSION['goods_attr'];
		}else{
				$data = array();			
		}		
		array_push($data,$_POST['attr_name']);		
		$_SESSION['goods_attr'] = array_unique($data);		
		output_data('ok');
	}
	
	
	public function getAttrSessionOp(){		
		$attr = $_SESSION['goods_attr'];		
		output_data($attr);		
	}
	
	
	
	//记录商品ICon
	public function setGoodsIconSessionOp(){
		
		
		if( is_array($_SESSION['goods_icon'])){			
				$data  = $_SESSION['goods_icon'];
		}else{
				$data = array();			
		}
		
		if(in_array($_POST['attr_id'],$data)){
			foreach( $data as $k=>$v) {
				if($_POST['attr_id'] == $v) unset($data[$k]);
			}
		}else{
			array_push($data,$_POST['attr_id']);		
		}
	
		$_SESSION['goods_icon'] = array_unique($data);	
		
		output_data('ok');
		
		
		
	}
	
	
	public function getGoodsIconSessionOp(){
		
		$icon = $_SESSION['goods_icon'];		
		foreach($icon as $v){
			$icon_id .= $v.',';
		}
		$data = $this->goods_icon($icon_id);		
		output_data($data);		
		
		
	}
	
	
	
	

	
	
	 /**
     * 店铺商品
     */
    public function goodsListOp()
    {
		
		$store_id =  $this->store_info['store_id'];		
		$store_class = $this->storeGoodsClass($store_id);	
		
		$data = array();
		foreach($store_class as $k=> $v){
			$data[$k] =$v;			
			if($v['class_id'] == 9997){
				$data[$k]['goods_list'] = $this->hot($store_id);
			}elseif($v['class_id'] == 9998){
				$data[$k]['goods_list'] = $this->recommend($store_id);	
			}elseif($v['class_id'] == 9999){
				$data[$k]['goods_list'] = $this->discount($store_id);	
			}elseif($v['class_id'] == 9996){
				$data[$k]['goods_list'] = $this->setmeal($store_id);	
			}else{
				$data[$k]['goods_list'] = $this->goodsList($v['class_id'],$store_id);
			}
		}	
		output_data($data);
       
	   
	   
    }
	
	
	
	/**
     * 店铺商品分类
     */
    private function storeGoodsClass($store_id)
    {

	   
       $store_goods_class = Model('store_goods_class')->getStoreGoodsClassPlainList($this->store_info['store_id'],$this->store_info['store_default_lang']);
	   $data = array();
	   foreach($store_goods_class as $k => $v){		   
		   $data[$k] = $v;	
		   
		   	 
		  // $data[$k]['cart_num'] =  $this->cart_count($v['class_id'],$store_id,1);		   
	  }	
      return $data;
    }
	
	private function hot($store_id){ //热销
	
		$where = array();
		$where['store_id'] = $store_id;
		$where['is_sales'] = 1;		     
	 	$data = $this->goods_list_un($where,$store_id);	
		return $data;
		
	}

	private function recommend($store_id){ //tuiujian
	
		$where = array();
		$where['is_recommend'] = 1;
		$where['store_id'] = $store_id;
        $data = $this->goods_list_un($where,$store_id);		
		return $data;
		
	}


	private function discount($store_id){ //优惠
	
		$where = array();
		$where['store_id'] = $store_id;
		$where['is_discount'] = 1;		
        $data = $this->goods_list_un($where,$store_id);			
		return $data;
		 
	}
	
	 private function setmeal($store_id){
        
        $where = array();
		$where['store_id'] = $store_id;
		$where['goods_is_set_meal'] = 1;		  
		$where['is_old'] =  0;   
	 	$data = $this->goods_list_un($where,$store_id);	
		return $data;
        
    }
	
	
	
	private function goodsList($class_id,$store_id){
		
		$where = array();
		//当前分类的ID
		$ext_goods = $this->extGoods($class_id,$store_id);	
		
		//筛选出来的 ingrid
		$ingr = $_POST['icon_ingr'];
	
		if(!empty($ingr)){
			
			$ext_ingr_goods = $this->extIngrGoods($ingr,$store_id);		
			if(count($ext_ingr_goods) > 0){
					foreach($ext_ingr_goods as $ingr){
						foreach($ext_goods as $ext){
							if($ext == $ingr){						
								$search_goods[] = $ingr;						
							}
						}
					}
					if(count($search_goods) > 0){
					  $ext_goods =  implode(",", $search_goods);	
					}else{
					  $ext_goods = '';
					}
			
			}else{
				$ext_goods = '';
				
			}
			
		}else{
			 $ext_goods =  implode(",", $ext_goods);	
		}
		
//		print_r($ext_goods);
		
		$where['goods_code'] = array('in',$ext_goods);		
		$where['is_delete'] = 0;
		$where['is_old'] = 0;	
		$where['goods_state'] = 1;	
	    /*
		if(!empty($_POST['keyword'])){
		    $where['goods_name'] = array('LIKE','%'.$_POST['keyword'].'%');
		}
		*/
      	$data = $this->goods_list_un($where,$store_id);			
		return $data;
	}
		
	
	
	
	//格式化商品数据
	private function goods_list_un($where,$store_id){
		
		$model_goods = Model('goods');			
		
		
	//	print_r($where);

		
		$goods_list = $model_goods->getGoodsList($where,'*','','goods_sort asc');
		$data = array();
		foreach($goods_list as $k=> $v){			
			$data[$k] = $v;
			$lang = $this->goods_language($v['goods_id']);
		//	print_r($lang);
			if(!empty($lang)){
				$data[$k]['goods_name'] = $lang['goods_name'];	
				$data[$k]['goods_description'] = $lang['goods_description'];	
			
				$data[$k]['goods_specs'] = $this->getSpecsFormat($lang['goods_code'],$lang['lang_name']);
				$data[$k]['goods_sizeprice'] = $this->getSizeFormat($lang['goods_code'],$lang['lang_name']) ;
				
			//	$data[$k]['goods_optional'] = unserialize($lang['goods_optional']);	
				$data[$k]['goods_lang'] = $lang['lang_name'];	
			}else{
				
				$data[$k]['goods_name'] = $v['goods_name'];	
				$data[$k]['goods_description'] = $v['goods_description'];	
				
				$data[$k]['goods_specs'] = $this->getSpecsFormat($lang['goods_code'],$lang['lang_name']);
				$data[$k]['goods_sizeprice'] = $this->getSizeFormat($lang['goods_code'],$lang['lang_name']) ;
				
			//	$data[$k]['goods_optional'] = unserialize($v['goods_optional']);	
				$data[$k]['goods_lang'] = $v['lang_name'];	
			
			}	
			
			$data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image'],$store_id) ;
			$data[$k]['goods_ingr'] = $this->goods_ingr($v['goods_ingr']);
			$data[$k]['goods_optional'] = $this->goodsOptional($v['goods_optional'],$lang['lang_name']);
		//	$data[$k]['cart_num'] = $this->cart_count($v['goods_id'],$store_id);			
			
		}		
		return $data;
	}
	
	
	//获取商品语言
	private function goods_language($goods_id){		
		
		//验证当前用户的所选语言是否存在
		$where = array(
			'goods_id' =>$goods_id,
			'lang_id' => $this->store_info['store_default_lang']
		);		
		
	
		$data = model('goods_language')->getGoodsLangInfo($where);
		
	
		
		
		switch ($data['lang_id']) {
			case 1:
				$data['spec_attr_name'] = "(必选)";
				break;
			case 2:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
			case 3:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
		}	
		
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
		
	}
	
	
	//获取配件商品
private function goodsOptional($code,$lang ='CHN-S'){
		   
		   $where = array();
		   $where['goods_code'] = $code;
		   $where['is_old'] = 0;
		   $where['goods_code_num'] = 0;
		   $options_list = model('store_goods_options')->getOptionsList($where);
		   $data = array();	   
		   $size_model = model('store_goods_size');
		   $specs_model = model('store_goods_specs');
		   $goods_lang_model = model('goods_language');
		   foreach($options_list as $k =>$v){			  
			   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang));
			   $data[$k]['size'] = $size;		   
			   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
			   $data[$k]['specs'] = $specs;		   
			   $data[$k]['price'] = $v['options_price'];		   
			   $goods_name = $goods_lang_model->getGoodsLangList(array('goods_code' => $v['options_goods_code']),'lang_id asc','goods_name,lang_name');		   
			   $data[$k]['goods_name'] = $goods_name;
			   $data[$k]['goods_code'] = $v['options_goods_code'];
		   }
		   
		   return $data;
		   
		   
	}

	//格式化配件商品的语言包
	private function optionsLang($data,$storeId){
		
	    $langInfo = model('store_language')->getStoreLangInfo(array('store_id'=> $storeId,'language_id'=> $this->store_info['store_default_lang']));
		$data = unserialize($data);	
		foreach($data as $v){
			$list[$v['lang_name']] = $v['options_name'];
		}			
		return $list[$langInfo['language_en']];
		
		
	}

	//格式化图片
	private function goodsImageFormat($image,$store_id){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['url'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS .$v;		
		}
		return $list;		
	}
	
		//格式化 size
	private function getSizeFormat($goods_code,$lang = 'CHN-S'){
		
		$model = model('store_goods_size');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSizeList($where);
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['name'] 		= $v['size_value'];
			$data[$k]['price'] 		= $v['size_price'];
			$data[$k]['sale_price'] = $v['size_sale_price'];
			$data[$k]['box_id'] 	= $v['size_box_id'];
			$data[$k]['size_id'] 	= $v['size_id'];
		}		
		return $data;
		
	}
	
	
	//格式化 specs
	private function getSpecsFormat($goods_code,$lang = 'CHN-S'){		
		
		$model = model('store_goods_specs');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['specs_type'] = 0;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){	
			$data[$k]['name'] = $v['specs_value'];
			$data[$k]['options'] = $this->getSpecsChildFormat($v['specs_id'],$lang,$v['specs_value']);
		}		
		return $data;
		
	}
	
	
	private function getSpecsChildFormat($specs_id,$lang,$specs_name){
		
		$model = model('store_goods_specs');	
		$where = array();
		$where['specs_parent_id'] = $specs_id;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){						
			$data[$k]['name'] = $specs_name;			
			$data[$k]['value'] = $v['specs_value'];
			$data[$k]['price'] = $v['specs_price'];
			$data[$k]['specs_id'] = $v['specs_id'];
		}		
		return $data;
	}
	
	
	
	//格式化数组
	private function FormatData($format_data,$type = '',$attr_name){
		
		$data = array();
		
		$result = array(); 
		foreach ($format_data as $data) { 
			  isset($result[$data['name']]) || $result[$data['name']] = array(); 
			  $result[$data['name']]['name'] = $data['name'].$attr_name;
			  
			  
			 $arr['name'] = $data['name'];
				 if($type == 'specs'){ 
					 $arr['value'] = $data['value'];
				 }
			  $arr['price'] = $data['price'];
			  $result[$data['name']]['options'][] = $arr;
			  
			  
		}
		
		$data = array();
		foreach($result as $v){
			$data[] = $v;
		}		
		return $data;
	}
	
	
	//获取ingr成分列表 
	private function extIngrGoods($ingr,$store_id){		
		$where = array();
		$where['store_id'] = $store_id;		
		$where['ingr_id'] = array('in',$ingr);					
		// array('ingr_id'=> array('in',$ingr),'store_id'=> $store_id);
		
		$ext_goods =  model('goods_ingr')->getGoodsIngrList($where,'goods_code','goods_code');		
	
		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods[] = $v['goods_code'];
			}
		}else{
			$goods = array();	
		}
		return $goods;		
	}	
	
	
	//商品成分记录
	private function goods_ingr($icon_id){
		
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		
		foreach($row as $k=> $v){			
			$data[$k] = $v;
			$data[$k]['attr_icon'] =  UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];
			$data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_active_icon'];
		}
		return $data;
	}
	
	
	// 读取扩展分类的商品
	private function extGoods($stc_id,$store_id){					
	
		$ext_goods = model('goods')->getGoodsList(array('gc_id'=>$stc_id),'goods_code');
		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods[] = $v['goods_code'];
			}			
		}else{
			$goods = array();	
		}
		return $goods;

		/*
		$where = array('stc_id'=> $stc_id,'store_id'=> $store_id);
		$ext_goods =  model('store_goods_extended')->getGoodsExtList($where,'goods_code');	
		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods[] = $v['goods_code'];
			}			
		}else{
			$goods = array();	
		}
		return $goods;
		*/


	}
	
	
	
	
	
	private function goods_list_data($class_id){
		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['gc_id'] = $class_id;
		$goods_list = model('goods')->getGoodsList($where);
		$data = array();
		foreach($goods_list as $k => $v){
			$data[$k] = $v;
			$data[$k]['goods_image'] = cthumb($v['goods_image']);			
			$data[$k]['goods_price'] = ncPriceFormat($v['shop_price']);
			$data[$k]['goods_spec'] = $spec;
			$data[$k]['goods_attr'] = unserialize($v['goods_attr']);
		}	
		
		return $data;
	}
	
	
	
	
	
	
	
	
	

    /**************************************************************************************
     * 商品详细信息
     */
     public function goods_infoOp() {
		 	
			
			$where = array(
				'goods_id' => $_POST['goods_id'],
				'store_id' => $this->store_info['store_id']
			);
			// 商品详细信息
			$model_goods = Model('goods');
			$goods_detail = $model_goods->getGoodsInfo($where);		
		
			$goods_detail = $this->_goods_detail_extend($goods_detail);
							
							
			if (empty($goods_detail)) {
				output_error('商品不存在');
			}
			
			
			output_data($goods_detail);
			
			
		 /*	$where = array(
				'goods_id' => $_POST['goods_id']
			);
			$goods_info = model('goods')->getGoodsInfo($where);
			$goods_info['goods_image'] = cthumb($goods_info['goods_image']);
		 	$goods_info['goods_attr'] = unserialize($goods_info['goods_attr']);
			$goods_info['goods_spec'] = unserialize($goods_info['goods_spec']);		
		 	$goods_info['goods_icon'] = $this->goods_icon($goods_info['goods_icon']);		 
			output_data($goods_info);*/
      
    }
	
	
	
	  /**
     * 商品详细信息处理
     */
    private function _goods_detail_extend($goods_detail) {
        
				$store_id = $goods_detail['store_id'];			
				$lang = $this->goods_language($goods_detail['goods_id']);		
				$goods_detail['goods_name'] = $lang['goods_name'];	
				$goods_detail['goods_description'] = $lang['goods_description'];	
				$goods_detail['goods_specs'] = $this->getSpecsFormat($lang['goods_code'],$lang['lang_name']);
				$goods_detail['goods_sizeprice'] =  $this->getSizeFormat($lang['goods_code'],$lang['lang_name']);
			// $data[$k]['goods_optional'] = unserialize($lang['goods_optional']);	
				$goods_detail['goods_lang'] = $lang['lang_name'];				
				$goods_detail['goods_image'] = $this->goodsImageFormat($goods_detail['goods_image'],$store_id) ;		
				$goods_detail['goods_ingr'] = $this->goods_ingr($goods_detail['goods_ingr']);
				$goods_detail['goods_optional'] = $this->goodsOptional($goods_detail['goods_optional'],$lang['lang_name']);
			//$goods_detail['cart_num'] = $this->cart_count($v['goods_id'],$store_id);					
		
     		   return $goods_detail;
    }
	
	
	
	
	
	//
	private function goods_icon($icon){
		
		$where = array(
			'attr_id' => array('in',$icon)
		);
		
		$list = model('goods_attr')->getGoodsAttrList($where);
		
		
		return $list;
		
	}
	
	
	
	
	
	
	//获取商品ICON
	public function goods_icon_listOp(){		
	
	/*
		$action = $_SESSION['goods_icon'];		
		$list = model('goods_attr')->getGoodsAttrList(TRUE);	
		$data =array();
		foreach($list as $k => $v){			
			$data[$k] = $v;	
			$data[$k]['action'] = @in_array($v['attr_id'],$action)  ? 1 : 0;	
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];		
			$data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_active_icon'];					
		}		
		output_data($data);		
		
	*/
        	$lang_name = $_POST['lang_name'];
    		$class = model('goods_attr')->getClassList(TRUE);
    		foreach($class as $k => $v){
    			$lang = unserialize($v['gc_lang']);
    			foreach($lang as $l){
    				if($l['lang_name'] == $_POST['lang_name']){
    					$name = $l['class_name'];
    				}
    			}
    			$data[$k]['name'] = $name;
    			$data[$k]['child'] = $this->get_ingr_child($v['gc_id'],$lang_name);			
    		}
    				
    		output_data($data);				
    		
		
	}
	
	
	private function get_ingr_child($class_id,$lang_name){
		$where = array();
		$where['attr_class_id'] = $class_id;			
		$list =model('goods_attr')->getGoodsAttrList($where);
		foreach($list as $k=> $v){
			$data[$k]['attr_id'] = $v['attr_id'];
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
			$data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
			$data[$k]['disabled'] = $v['disabled'];
			switch ($lang_name) {
				case 'CHN-S':
					$data[$k]['attr_name'] = $v['attr_name'];
					break;
				case 'CHN-T':
					$data[$k]['attr_name'] = $v['attr_name_chnt'];
					break;
				case 'ENG':
					$data[$k]['attr_name'] = $v['attr_name_eng'];
					break;
				default:
					$data[$k]['attr_name'] = $v['attr_name'];					
			}			
		}		
		return $data;
	}
		
	
	
	
	
	
	
    /**
     * 商品编辑保存
     */
    public function goods_editOp() {
       
	   
	   $where = array(
	   	'goods_id' => $_POST['goods_id'],
		'store_id' => $this->store_info['store_id']
	   );
	   
	   $data = array(
	   		'goods_state' => $_POST['goods_state'],
			'goods_stock' => $_POST['goods_stock'],
			'is_recommend' => $_POST['is_recommend'],
			'is_discount' => $_POST['is_discount']
	   );
	   
	   $row = model('goods')->editGoodsNew($data,$where);
	   if($row){
		   	output_data('ok');		
			   
		}else{
		   	output_data('保存失败');		
		}
	   
	   
	   
    }
    
    public function goods_sold_outOp(){
        
         $where = array(
	   	'goods_id' => $_POST['goods_id'],
		'store_id' => $this->store_info['store_id']
	   );
	   
	   $data = array(
			'is_sold_out' => $_POST['sold_out']
	   );
	   
	   $row = model('goods')->editGoodsNew($data,$where);
	   if($row){
		   	output_data('ok');		
			   
		}else{
		   	output_data('保存失败');		
		}
	   
    }
  
	


	
    /**
     * 商品删除
     */
    public function goods_dropOp() {
				
		$where = array(			
			'store_id' => $this->store_info['store_id'],
			'goods_id' => $_POST['goods_id']
		);		
		$update = array(
			'goods_state' => 2
		);		
        $result = model('goods')->editGoodsNew($update,$where);		
        if ($result) {
            output_data('ok');
        }else{	
        	output_error('删除失败');
		}
    }


	//商品ICON
	
	public function goodIconOp(){	
		$attr = model('goods_attr')->getGoodsAttrList(TRUE);		
		output_data($attr);	
	}
	
	
	
	//选中select
	public function selectIconOp(){
		
		
	}
	
	
	//获取餐盒
	public function foodboxOp(){		
		$foodbox = model('foodbox_class')->getClassList(TRUE);
		$data = array();
		foreach($foodbox as $k =>$v){
			$data[$k] = $v;
			$data[$k]['child'] = $this->foodbox_goods($v['class_id']);
		}
		
		output_data($data);
		
		
		
	}
	
	private  function foodbox_goods($class_id){
		
		$where = array(
			'class_id' => $class_id
		);
		$goods = model('foodbox_goods')->getGoodsList($where);		
		return $goods;
	}
	
	
	
	
	 public function updateFileOp()
    {
		
		$store_id = $this->store_info['store_id'];
		
		
        // 判断图片数量是否超限
        $model_album = Model('album');    

        $class_info = $model_album->getOne(array('store_id' => $store_id, 'is_default' => 1), 'album_class');
        // 上传图片
        $upload = new UploadFile();
        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
        $upload->set('max_size', C('image_max_filesize'));
        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
        $upload->set('fprefix', $store_id);
        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
        $result = $upload->upfile('file');
        if (!$result) {
            return callback(false, $upload->error);
        }
        $img_path = $upload->getSysSetPath() . $upload->file_name;
        // 取得图像大小
        if (!C('oss.open')) {
            list($width, $height, $type, $attr) = getimagesize(BASE_UPLOAD_PATH . '/' . ATTACH_GOODS . '/' . $store_id . DS . $img_path);
        } else {
            list($width, $height, $type, $attr) = getimagesize(C('oss.img_url') . '/' . ATTACH_GOODS . '/' . $store_id . DS . $img_path);
        }
        // 存入相册
        $image = explode('.', $_FILES[$image_name]["name"]);
        $insert_array = array();
        $insert_array['apic_name'] = $image['0'];
        $insert_array['apic_tag'] = '';
        $insert_array['aclass_id'] = $class_info['aclass_id'];
        $insert_array['apic_cover'] = $img_path;
        $insert_array['apic_size'] = intval($_FILES[$image_name]['size']);
        $insert_array['apic_spec'] = $width . 'x' . $height;
        $insert_array['upload_time'] = TIMESTAMP;
        $insert_array['store_id'] = $store_id;
        $model_album->addPic($insert_array);

        $data = array ();
        $data ['thumb_name'] = cthumb($img_path, 240, $store_id);
        $data ['name']      = $img_path;

		output_data($data);
		
		//output_data(array('url'=> UPLOAD_SITE_URL.'/'.ATTACH_SHOP.'/','file_name'=>$upload->file_name ));                  

       
    }
		
	
	
	
	public function goods_sortOp(){				
		
		 $file  = 'seller_log.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
		 $content = json_encode($_POST);
		 if($f  = file_put_contents($file, $content,FILE_APPEND)){// 这个函数支持版本(PHP 5) 
		 	
		 }
		
		 	

		
		$data =  json_decode($_POST['goods_data']);	
		$model_class = model('store_goods_class');
		$model_goods = model('goods');
		foreach($data as $k => $v){
						$model_class->editStoreGoodsClass(array('stc_sort'=>$k),array('store_id' => $this->store_info['store_id'],'stc_id'=>$v['class_id']));						
						foreach($v['goods_list'] as  $g => $vg){
								$model_goods->editGoodsNew(array('goods_sort'=>$g),array('store_id'=>$this->store_info['store_id'],'goods_id'=>$vg['goods_id']));							
						}
		}
		output_data('ok');
	}
	
	
}
