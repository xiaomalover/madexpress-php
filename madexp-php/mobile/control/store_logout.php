<?php
/**
 * 注销
 */



defined('InMadExpress') or exit('Access Invalid!');

class store_logoutControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 注销
     */
    public function indexOp(){

        $model_mb_seller_token = Model('mb_seller_token');
        $condition = array();
        $condition['seller_id'] = $this->store_info['store_id'];        
        $model_mb_seller_token->delSellerToken($condition);
        output_data('1');
        
    }

}
