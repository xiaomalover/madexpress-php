<?php
/**
 * 前台品牌分类
 */


defined('InMadExpress') or exit('Access Invalid!');
class member_documentControl extends mobileMemberControl {
    public function __construct() {
        parent::__construct();
    }

    public function indexOp() {
      
		$doc = Model('document')->getDocumentList(TRUE);
		
        output_data($doc);
    }
	
	
	
	
	public function showOp(){
		
		$where = array(
			'doc' => $_GET['id']
		);
		
		$doc = model('document')->getDocumentInfo($where);
		
		output_data($doc);
		
		
	}
	
}
