<?php
/**
 * 专员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_memoControl extends mobileSellerControl{
 
	public function __construct(){
        parent::__construct();   
    }

	
	public function get_classOp(){
		
		$model = model('notepad');
		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		
		$data = $model->getNotepadClassList($where);
		$list = array();
		foreach($data as $k=>$v){
			$v['child'] = $this->get_list($v['class_id']);			
			$list[$k] = $v;
		}
		
		output_data(array('list'=>$list));
		
		
	}
	
	private function get_list($class_id){
		
		$where = array();
		$where['class_id'] = $class_id;
		$where['store_id'] = $this->store_info['store_id'] ;
		$data = model('notepad')->getNotepadList($where);
		$list = array();
		foreach($data as $k => $v){
			$v['notepad_time'] = date('d/m/y',$v['notepad_time']);
			$list[$k] = $v;
		}
		
		return $list;
	}
	
	
	public function class_saveOp(){		
		$data = array();
		$data['class_name'] = $_POST['class_name'];
		$data['store_id'] = $this->store_info['store_id'];
		if(intval($_POST['class_id']) > 0){
			$where = array();
			$where['class_id'] = $_POST['class_id'];
			$row = model('notepad')->editNotepadClass($where,$data);
			
		}else{			
			$row = model('notepad')->addNotepadClass($data);
		}
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
		
	}
	
	
	public function memo_infoOp(){
		
		$where = array();
		$where['notepad_id'] = $_POST['id'];		
		$row = model('notepad')->getNotepadInfo($where);
		if($row){			
			$data =array();
			$data['content'] = $row['notepad_content'];
			$data['title'] = $row['notepad_title'];
			$data['class_id'] = $row['class_id'];
			$data['store_id'] = $row['store_id'];
			$data['notepad_id'] = $row['notepad_id'];
			
			output_data($data);			
		}else{			
			output_error('获取失败');
		}
	}
	
	
	public function memo_saveOp(){		
		$data = array();
		$data['notepad_title'] = $_POST['title'];
		$data['notepad_content'] = $_POST['content'];
		$data['notepad_time'] = time();
		$data['notepad_file'] = $_POST['file'];
		$data['class_id'] = $_POST['class_id'];
		$data['store_id'] = $this->store_info['store_id'];
		
		if(intval($_POST['notepad_id']) > 0){
			$where = array();
			$where['notepad_id'] = $_POST['notepad_id'];
			$row = model('notepad')->editNotepad($where,$data);
			
		}else{			
			$row = model('notepad')->addNotepad($data);
		}
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}
	
	
	public function memo_delOp(){
		
		$where = array();		
		$where['notepad_id'] = $_POST['id'];		
		$row = model('notepad')->delNotepad($where);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}

	
	public function memo_class_delOp(){
		
		$where = array();		
		$where['class_id'] = $_POST['class_id'];		
		$row = model('notepad')->delNotepad($where);
		model('notepad')->delNotepadClass($where);
		if($row){
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
	}


	
}
