<?php
/**
 * 商家财务
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_financeControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	
	//流水类型
	
	
	public function get_typeOp(){
		
		$positive_activities = array();		
		$positive_activities[0]['finance_project']  = 'All Positive Activities' ;
		$positive_activities[0]['finance_project_method']  = 'All' ;
		
		$positive = model('finance')->getFinanceList(array('finance_user' => 0 ,'finance_type' => 0));				
		$i = 1;
		foreach($positive as $v){
			$positive_activities[$i] = $v;
			$i++;
		}
		
		$negative_activities = array();		
		$negative_activities[0]['finance_project']  = 'All Negative Activities' ;	
		$negative_activities[0]['finance_project_method']  = 'All' ;
		$negative = model('finance')->getFinanceList(array('finance_user' => 0 ,'finance_type' => 1));				
		$i = 1;
		foreach($negative as $v){
			$negative_activities[$i] = $v;
			$i++;
		}
		
		
		output_data(array('positive' => $positive_activities,'negative'=>$negative_activities));
				
	}
	
	
	//获取流水
	public function get_activitiesOp(){
		
		$model = model('store_wallet');
		
		$condition = array();		
		if($_POST['type'] != 'All'){			
			$condition['lg_type'] = $_POST['type'];
		}
		$condition['lg_type_activities'] = $_POST['type_act'];
				
		$order = 'lg_id desc';	
		
			  
		$wallet_list = $model->getPdLogList($condition, '*', $this->page, $order);
		
		$list = array();		
		
		foreach ($wallet_list as $v) {
			
		   			
			$v['lg_add_time'] =  date('d/m/Y',$v['lg_add_time']);		
			$list[] = $v;
		}		
		
		$page_count = $model->gettotalpage();
		$list_count = $model->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
		
	}
	
	
	
	
	
		
		
	private function status(){
			$data = array();
			$data[0] = '待审核';
			$data[1] = '已审核';
			$data[2] = '已驳回';
			return $data;
	}
		
		
		
		
	public function examine_stateOp(){
			
	
			$examine_id = $_POST['examine_id'];
			$type = $_POST['state']; //1 同意  2驳回
			
			
			if($type == 1){				
				$row = model('store_examine')->storeExamine($examine_id);
			}else{				
				$where = array(
					'store_id' => $this->store_info['store_id'],
					'examine_id' => $examine_id
				);
				$updata = array(
					'examine_state' => 2
				);						
				$row = model('store_examine')->editStoreExamine($updata,$where);
			}
			
			if($row){
				output_data('操作成功');				
			}else{
				output_error('操作失败');				
			}
			
		}
		
		
	
	
}
