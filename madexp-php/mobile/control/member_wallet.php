<?php
/**
 * 
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_walletControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 我的商城
     */
    public function indexOp() {
        $member_wallet = array();
        $member_wallet['available'] = $this->member_info['available_predeposit'];
        $member_wallet['bank_list'] = $this->bankList();
		output_data(array('wallet' => $member_wallet));
	}
    
	
	
	
	private function bankList(){		
		$where = array(
			'member_id' => $this->member_info['member_id']
		);
		$list= model('member_bank')->getMemberBankList($where);		
		$data = array();
		foreach($list as $k=> $v){			
			$data[$k] = $v;
		}
		
		return $data;
		 //output_data(array('bank_list' => $bank_list));
		
	}
	
	
	
   
}
