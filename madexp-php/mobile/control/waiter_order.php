<?php
/**
 * 骑手订单
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_orderControl extends mobileWaiterControl
{
    public function __construct()
    {
        parent::__construct();
    }

    public function order_listOp()
    {
        $model_order = Model('order');
        
        $fields = 'order_id,order_sn,store_id,store_name,store_phone,order_state,store_address,store_region_id,store_region_sm_id,store_coordinate,store_region_name,store_region_color,buyer_id,buyer_code,buyer_name,buyer_phone,buyer_address,buyer_region_id,buyer_is_new,buyer_coordinate,buyer_region,delivery_fee,order_estimate_time,buyer_region_color,add_time,finnshed_time';
        
        $order_list = array();
        $order_list = $model_order->getWaiterOrderList(
            $this->waiter_info['distributor_id'],
            $_POST['order_sn'],
            $_POST['keyword'],
            $_POST['order_state'],
            $_POST['start_date'],
            $_POST['end_date'],
            $_POST['skip_off'],
            $fields,
            $_POST['start_income'],
            $_POST['end_income'],
            array()
        );
        $page_count = $model_order->gettotalpage();
        output_data(array('order_list'=>$order_list), mobile_page($page_count));
    }
    

    
    //订单信息
    public function order_infoOp()
    {
        $order_id = intval($_POST['order_id']);
        if (!$order_id) {
            output_error('订单编号有误');
        }
        $region = $this->region();
        $model_order = Model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['distributor_id'] = $this->waiter_info['distributor_id'];
        $order = $model_order->getOrderInfo($condition, array('order_common','order_goods','member','order_log','store'));
        // print_r($order);
        if (empty($order)) {
            output_error('订单信息不存在');
        }
        
        //用户信息
        $order_info  =array();
        $buyer_info = array();
        $buyer_info['buyer_id'] = $order['buyer_id'];
        $buyer_info['buyer_code'] = $order['buyer_code'];
        $buyer_info['buyer_name'] = $order['buyer_name'];
        $buyer_info['buyer_phone'] = $order['buyer_phone'];
        $buyer_info['buyer_address'] = $order['buyer_address'];
        $buyer_info['buyer_region_id'] = $order['buyer_region_id'];
        $buyer_info['buyer_is_new'] = $order['buyer_is_new'];
        $buyer_info['buyer_coordinate'] = $order['buyer_coordinate'];
        $buyer_info['buyer_region'] = $order['buyer_region'];
        $buyer_info['buyer_region_color'] = $order['buyer_region_color'];
        $buyer_info['buyer_status_code'] = $order['buyer_status_code'];
        $order_info['buyer'] = $buyer_info;
        
        
        $store = $order['extend_store'];
        $store_info = array();
        $store_info['store_id'] = $store['store_id'];
        $store_info['store_name_primary'] = $store['store_name_primary'];
        $store_info['store_name_secondary'] = $store['store_name_secondary'];
        $store_info['store_code'] = $store['store_code'];
        $store_info['store_address'] = $store['store_address'];
        $store_info['store_lat'] = $store['store_lat'];
        $store_info['store_lng'] = $store['store_lng'];
        $store_info['region_name'] = $store['region_name'];
        $store_info['region_color'] = $order['store_region_color'];
        $store_info['store_phone'] = $store['store_phone'];
        $store_info['store_status_code'] =$order['store_status_code'];
        $order_info['store'] = $store_info;
        //距离
        
        $distance = array();
        $distance['store_buyer_distance'] = $order['store_buyer_distance'];
        $distance['distributor_store_distance'] = $order['distributor_store_distance'];
        
        $order_info['distance'] = $distance;
        
        //订单lOG
        $order_info['order_log'] = $order['extend_order_log'];
                
        //商品
        $order_info['goods_list'] = $order['extend_order_goods'];
        
        //配送员信息
        
        $delivery = array();
        
        $delivery['distributor_id'] = $order['distributor_id'];
        $delivery['distributor_name'] = $order['distributor_name'];
        $delivery['distributor_mobile'] = $order['distributor_mobile'];
        $delivery['distributor_start_time'] = $order['distributor_start_time'];
        $delivery['distributor_end_time'] = $order['distributor_end_time'];
        $delivery['distributor_code'] = $order['distributor_code'];
        $delivery['distributor_duration'] = $order['distributor_duration'];
        $delivery['distributor_coordinate'] = $order['distributor_coordinate'];
        
        
        //预计送达时间计算 骑手到商家 + 商家备餐时间 + 商家到客户的距离
        
        
        $delivery['order_estimate_time'] = 300  ;// $order['order_estimate_time'];
        $order_info['delivery'] = $delivery;
        //订单信息
        
        
        $info = array();
        $info['order_sn'] = $order['order_sn'];
        $info['delivery_time'] = $order['delivery_time'];
        $info['delivery_type'] = $order['delivery_type'];
        $info['order_state'] = $order['order_state'];
        $info['delivery_fee'] = $order['delivery_fee'];
        $info['delivery_comment'] = $order['deliver_comment'];
     
        $info['delivery_state'] = $this->waiter_info['delivery_state'];
        $info['order_id'] = $order['order_id'];
        $order_info['order'] = $info;
        
        
        
        
        
        $problem = null;
      
        $problem_info = model('order_problem')->getProblemInfo(array('order_id' => $order['order_id'],'problem_publicher_type' => 2,'problem_publisher_id'=>$this->waiter_info['distributor_id']));
        if (!empty($problem_info)) {
            $problem = $problem_info;
            $problem['problem_image'] = $this->order_problem_image($problem_info['problem_image']);
        }
        
        $order_info['problem'] = $problem;
        
        output_data(array('order_info' => $order_info));
    }



        
    //格式化图片
    private function order_problem_image($image)
    {
        $list = array();
        if (!empty($image)) {
            $image  =	explode(",", $image);
            foreach ($image as $v) {
                $list[] = UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.$v;
            }
            return $list;
        } else {
            return $list;
        }
    }
    
    
    
    


    
    private function region()
    {
        $list = model('region')->getRegionList(true);
        $data= array();
        foreach ($list as $v) {
            $data[$v['region_id']] = $v;
        }
        return $data;
    }
    
    
    

    
    
    
    //取消订单
    public function cancelOp()
    {
        $order_sn = $_POST['order_sn'];
        $deliver_id = $this->waiter_info['distributor_id'];
        $batch_code = $this->waiter_info['batch_code'];
        $sch_id = $this->waiter_info['scheduling_id'];
        $allocation = model('order_allocation')->refuse($order_sn, $delivery_id, $batch_code, $sch_id);
    }
    
    
    //扫码取货
    public function scanOrderOp()
    {
        $where = array(
            'order_sn' => $_POST['order_sn'],
            'distributor_id' => $this->waiter_info['distributor_id']
        );
        $order_info = model('order')->getOrderInfo($where);
        if (empty($order_info)) {
            output_error('不是您的订单哦~');
        }
        $allocation = model('order_allocation')->takeOrder($_POST['order_sn'], $this->waiter_info['distributor_id']);
    }
    
    
    
    /*接单新
    public function robOrderOp()
    {
        $model = model('order_allocation');
        $order_sn = $_POST['order_sn'];
        $delivery_id = $this->waiter_info['distributor_id'];
        $model->accept($delivery_id, $order_sn);
    }
    
    
    //拒绝接单
    public function robCancelOrderOp()
    {
        $order_sn = $_POST['order_sn'];
        $deliver_id = $this->waiter_info['distributor_id'];
        $batch_code = $this->waiter_info['batch_code'];
        $sch_id = $this->waiter_info['scheduling_id'];
            
        $allocation = model('order_allocation')->refuse($order_sn, $delivery_id, $batch_code, $sch_id);
    }
    */
    
    
    //确认送达
    public function confirmOrderOp()
    {
        $order_sn = $_POST['order_sn'];
        $deliver_id = $this->waiter_info['distributor_id'];
        model('order_allocation')->giveOrder($order_sn, $deliver_id);
        
    }
    
    //手动开始配送
    
    public function startDeliveryOp()
    {
        $where = array(
            'distributor_id' => $this->waiter_info['distributor_id'],
            'order_state' => 30
        );
        $count = model('order')->getOrderCount($where);
        if ($count == 0) {
            output_error('您当前没有可取餐的订单');
        }
        
        $where = array(
            'distributor_id' => $this->waiter_info['distributor_id']
        );
        $data = array(
            'delivery_state' => $_POST['state']
        );
        $row = model('waiter')->editWaiter($where, $data);
        if ($row) {
            output_data('ok');
        } else {
            output_error('失败');
        }
    }

    
    public function getPollQueueOp()
    {
        $where = array(
            'waiter_id' => $this->waiter_info['distributor_id'],
            'state' => 0,
            'send_state' => 2
        );
        $list = model('order_polling')->getPollingList($where);
        output_data(array('queue_list'=>$list));
    }
    
    
        
    
    
    /**
     * 判断一个坐标是否在圆内
     * 思路：判断此点的经纬度到圆心的距离  然后和半径做比较
     * 如果此点刚好在圆上 则返回true
     * @param $point ['lng'=>'','lat'=>''] array指定点的坐标
     * @param $circle array ['center'=>['lng'=>'','lat'=>''],'radius'=>'']  中心点和半径
     */
    public function is_point_in_circle($point, $circle)
    {
        $distance = $this->distance($point['lat'], $point['lng'], $circle['center']['lat'], $circle['center']['lng']);
        if ($distance <= $circle['radius']) {
            return 1;
        } else {
            return 0;
        }
    }
    /**
         *  计算两个点之间的距离
         * @param $latA  第一个点的纬度
         * @param $lonA  第一个点的经度
         * @param $latB  第二个点的纬度
         * @param $lonB  第二个点的经度
         * @return float
         */
    public function distance($latA, $lonA, $latB, $lonB)
    {
        $earthR = 6371000.;
        $x = cos($latA * $this->PI / 180.) * cos($latB * $this->PI / 180.) * cos(($lonA - $lonB) * $this->PI / 180);
        $y = sin($latA * $this->PI / 180.) * sin($latB * $this->PI / 180.);
        $s = $x + $y;
        if ($s > 1) {
            $s = 1;
        }
        if ($s < -1) {
            $s = -1;
        }
        $alpha = acos($s);
        $distance = $alpha * $earthR;
        return $distance;
    }

    
    //毫秒级时间戳
    private function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
    
    
        

    //问题类型
    public function problemOrderTypeOp()
    {
        $model = model('problem_type');
        $where = array();
        $where['type_parent_id'] = 0;
        $parent = $model->getTypeList($where);
     
        $list = array();
        foreach ($parent as $k => $v) {
            $list[$k] = $v;
            $list[$k]['child'] =  $model->getTypeList(array('type_parent_id' => $v['type_id']));
        }
        
        output_data($list);
    }
    
    
    
    //上报问题单
    public function problemOrderOp()
    {
		
		$where = array(
			'order_id' => $_POST['order_id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);		
		$order_info = model('order')->getOrderInfoN($where);					
		if(empty($order_info)){				
			 output_error('订单参数错误');
		}	
			
		$problem_data = array(
			    'order_id'          => $_POST['order_id'],
			    //'order_sn'          => $_POST['order_sn'],
			    'problem_content'   => $_POST['problem_content'],
			    'problem_image'     => $_POST['problem_image'],
			    'problem_type_id'   => $_POST['problem_type_id'],
			    'problem_type_name' => $_POST['problem_type_name'],
			    'problem_publisher_id'      =>  $this->waiter_info['distributor_id'],
			    'problem_publisher_name'    =>  $this->waiter_info['distributor_name'],
			    'problem_publicher_type'    => 2,
			    'problem_state' => 0
		);
		$row = model('order_problem')->addProblem($problem_data);
		if ($row) {
			    $where = array(
			            'order_id' => $_POST['order_id'],
			            'distributor_id' => $this->waiter_info['distributor_id']
			    );
			    $data = array(
			            'is_problem' => 1
			    );
			    model('order')->editOrder($data, $where);
			    
				//写入配送员上报
				$record_data = array();
				$record_data['action_required_type'] = 'Courier Report';
				$record_data['action_required_time'] =  TIMESTAMP;	
				$record_data['action_required_id'] = $row;
				$record_data['order_id'] =  $order_info['order_id'];
				model('order_record')->addRecord('action_required',$record_data);
					
			    output_data('问题已上报');
			} else {
			    output_error('上报问题失败');
			}
		
    }
    
    public function uploadFileOp()
    {
        $complain_pic = array();
        $complain_pic[1] = 'file1';
        $complain_pic[2] = 'file2';
        $complain_pic[3] = 'file3';
        $complain_pic[4] = 'file4';
        $complain_pic[5] = 'file5';
        $complain_pic[6] = 'file6';
        $complain_pic[7] = 'file7';
        $complain_pic[8] = 'file8';
        $complain_pic[9] = 'file9';
          
        $pic_name = array();
        $upload = new UploadFile();
        $uploaddir = ATTACH_FEEDBACK;
        $upload->set('default_dir', $uploaddir);
        $upload->set('allow_type', array('jpg','jpeg','gif','png'));
        $count = 1;
        foreach ($complain_pic as $pic) {
            if (!empty($_FILES[$pic]['name'])) {
                $result = $upload->upfile($pic);
                if ($result) {
                    $pic_name[$count] = $upload->file_name;
                    $upload->file_name = '';
                } else {
                    $pic_name[$count] = '';
                }
            }
            $count++;
        }
            
        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'));
    }
}
