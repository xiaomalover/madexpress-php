<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class loginControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 登录
     */
    public function loginOp(){
      
		
		
		if(empty($_POST['username']) || empty($_POST['password'])) {
			output_error('登录失败');
        }
		
	
        $model_member = Model('member');
        $login_info = array();
        $login_info['user_name'] = $_POST['username'];
        $login_info['password'] = $_POST['password'];
		
		
		
        $member_info = $model_member->login($login_info);
		
        if(isset($member_info['error'])) {
			
            output_error($member_info['error']);
       
		} else {
			
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], 'mobile');
            if($token) {


				
				$grader = $this->getGrade($member_info['member_id']);
				output_data(array('username' => $member_info['member_name'], 'membeid' => $member_info['member_id'], 'key' => $token,'grade'=>$grader));
            } else {
                output_error('登录失败');
            }
        }
    }
	
	
	private function getGrade($member_id){
		$where = array(
			'member_id' => $member_id
		);
		$row = model('admin')->getAdminInfoNew($where);
		if(!empty($row)){			
			if($row['admin_type'] == 0){
				return 1; //超管
			}else{
				return 2; //专员账号
			}
		}else{
				return 0;
		}		
	}

    /**
     * 登录生成token
     */
    private function _get_token($member_id, $member_name, $client) {
        $model_mb_user_token = Model('mb_user_token');

        //重新登录后以前的令牌失效
        //暂时停用
        //$condition = array();
        //$condition['member_id'] = $member_id;
        //$condition['client_type'] = $client;
        //$model_mb_user_token->delMbUserToken($condition);

        //生成新的token
        $mb_user_token_info = array();
        $token = md5($member_name . strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_user_token_info['member_id'] = $member_id;
        $mb_user_token_info['member_name'] = $member_name;
        $mb_user_token_info['token'] = $token;
        $mb_user_token_info['login_time'] = TIMESTAMP;
        $mb_user_token_info['client_type'] = $client;

        $result = $model_mb_user_token->addMbUserToken($mb_user_token_info);

        if($result) {
            return $token;
        } else {
            return null;
        }

    }

    /**
     * 注册
     */
    public function registerOp(){
		
		if (process::islock('reg')){
			output_error('您的操作过于频繁，请稍后再试');
		}
        $model_member   = Model('member');
        $register_info = array();
		
		if(!preg_match('/^0?(13|15|17|18|14)[0-9]{9}$/i',   $_POST['username'])){		 	
			output_error('请输入正确手机号');			
		}
		
		
        $register_info['username'] = $_POST['username'];
        $register_info['password'] = $_POST['password'];
		
	//	$register_info['smscode'] = $_POST['smscode'];
		
        $register_info['password_confirm'] = $_POST['password'];
        $register_info['email'] = $_POST['username'].'@gmail.com';
        
        $member_info = $model_member->register($register_info);
		
		
        if(!isset($member_info['error'])) {
			process::addprocess('reg');
            $token = $this->_get_token($member_info['member_id'], $member_info['member_name'], 'app');
            if($token) {
				
				$model_lang = model('language');
				
				//写入语言
				$list = $model_lang->getLangList(TRUE);
				$lang_data = array();
				foreach($list as $k => $v){
					$data['language_id']   = $v['language_id'];
					$data['language_name'] = $v['language_name'];
					$data['language_icon'] = $v['language_icon'];
					$data['language_sort'] = $k;
                    $data['language_code'] = $v['language_flag'];
					$data['member_id'] = $member_info['member_id'];
					$lang_data[] = $data;
				};				
				
				 model('member_language')->addMemberLangAll($lang_data);				
				
				
				
				//更新所有分类到排序里。				
				model('member_bind_class')->delMemberBindClass(['member_id' => $member_info['member_id']]);
				
				
                output_data(array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $token));
            } else {
                output_error('注册失败');
            }
        } else {
            output_error($member_info['error']);
        }

    }
	
	public function consigneeAttrOp(){
		
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);
		$data = array();
		foreach($list as $v){
			$data[] = $v;
			$data[]['action'] = 0;
			$data[]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];
			
		}
		
		 output_data(array('list'=>$data));
		
	}
	

	
	//找回密码
	public function savePasswordOp(){
		
		
		
		
		
		
		
		
		
	}
	 
	
	
	
}
