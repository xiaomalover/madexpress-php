<?php
/**
 * 商户银行卡管理
 */


defined('InMadExpress') or exit('Access Invalid!');

class seller_walletControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();      
    }

    /**
     * 钱包首页
     */
    public function indexOp() {

		$data= array();
		$data['balance'] = $this->store_info['available_predeposit'];
		$data['freeze_balance'] = $this->store_info['freeze_predeposit'];
		
		output_data($data);
		
    }
	
	//钱包流水记录
	
	public function pdLogOp(){
		
		$model = model('store_wallet');
		$where = array();

		$where['lg_type_activities'] = $_POST['type'];
		
		
		if($_POST['start_date'] && $_POST['end_date']){
	        $where['lg_add_time'] = array('between',array(strtotime($_POST['start_date']),strtotime($_POST['end_date'])));
		}
		
		
		$result = $model->getPdLogList($where,$this->page,'*','lg_id desc');
		$data = array();
		foreach($result as $k => $v){			
			$data[$k] = $v;
			$data[$k]['lg_add_time'] = date('d M H:i');
		}
		
		 $page_count = $model->gettotalpage();
		 output_data(array('list' => $data), mobile_page($page_count));
		
	
	}
	
	
	
	
	//提现记录
	public function cashListOp(){
		
		
		
		
	}
	
	

}
