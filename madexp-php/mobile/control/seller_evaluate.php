<?php
/**
 * 我的评价

 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_evaluateControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }
    
	//评价页
	
	public function indexOp(){
		
		$store_id  = $this->store_info['store_id'];
		$data = array();
		
		
		$model = model('evaluate_store');	
		
		
	
		$total_count	= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id));
		$total_one		= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'1'));
		$total_two		= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'2'));
		$total_three	= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'3'));
		
		
		//获取是否有留言的。 
		$total_no_message	= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_content'=>''));
	
	
		$data['store_rating'] =  round(($total_three / $total_count) * 100,2) ;  //商家好评率
		
		
		$returning_cus	= $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_user_isreturn'=> 1 ));
	
		$data['member_returning_cus'] = $returning_cus.'/'.$total_count ;
		$data['member_returning_cus_rating'] = round(($returning_cus / $total_count  )  * 100,2) ; //回头点单率客		
		$data['message_rating'] = round((($total_count - $total_no_message) / $total_count) * 100,2);  //留言率
		
		
		
		
		
	/*	$data['ontime_no_num'] = 3;  //未按时订单
		$data['ontime_yes_num'] = 10; //已按时订单
		$data['message_no_num'] = 2; //未留言
		$data['message_yes_num'] = 10; //已留言
		*/
		

		
	
		$data['evaluate_total_all'] = $total_count;
		$data['evaluate_total_1'] = $total_one;
		$data['evaluate_total_2'] = $total_two;
		$data['evaluate_total_3'] = $total_three;
		
		output_data($data);
		
		
	}
	
	//评价详情
	
	public function evaluateInfoOp(){
		
		
			$score = array(
				1 =>'不喜欢',
				2 => '一般',
				3 => '喜欢'
			);

			$model = model('evaluate_store');		
			$where = array(
				'store_evaluate_id' => $_POST['id'],
				'store_evaluate_storeid' => $this->store_info['store_id']
			);		
			$info = $model->getEvaluateStoreInfo($where);		

		
			$evalue['eva_id'] = $info['store_evaluate_id'];
			$evalue['eva_addtime'] = date('d/m/y',$info['store_evaluate_addtime']);
			$evalue['eva_content'] = $info['store_evaluate_content'];
			$evalue['eva_username'] = $info['store_evaluate_username'];
			$evalue['eva_isnew'] = $info['store_evaluate_user_isnew'];
			$evalue['eva_userid'] = $info['store_evaluate_userid'];
		    $evalue['eva_usercode'] = $info['store_evaluate_usercode'];
			$evalue['eva_score'] = $score[$info['store_evaluate_score']];
			$evalue['eva_avatar'] = getMemberAvatarForID($info['store_evaluate_userid']);
				
			$evalue['eva_image'] = $this->evaluateImage($info['store_evaluate_image']);
				
			$evalue['eva_reply_time'] = date('d/m/y',$info['store_evaluate_reply_time']);
			$evalue['eva_reply'] = $info['store_evaluate_explain'];		
				
				
				
			output_data($evalue);
			
		
	}
	
	
    /**
     * 评论列表
     */
    public function evaluateListOp() {
      
		$model = model('evaluate_store');
		
		$where = array();
		
		$where['store_evaluate_storeid'] = $this->store_info['store_id'];
		$type = $_POST['type'];
	    switch ($type) {
            case '1':
                $where['store_evaluate_score'] = array('in', '1');
                break;
            case '2':
                $where['store_evaluate_score'] = array('in', '2');
                break;
            case '3':
                $where['store_evaluate_score'] = array('in', '3');
                break;
        	case '0':
                $where['store_evaluate_score'] = array('in', '1,2,3');
                break;
        }
		
        $list = $model->getEvaluateStoreList($where);
		$score = array(
			1 => '不喜欢',
			2 => '一般',
			3 => '喜欢'
		);			
		
		$evalue = array();		
		foreach($list as $k => $v){		
			$evalue[$k]['eva_id'] = $v['store_evaluate_id'];
			$evalue[$k]['eva_addtime'] = date('d/m/y',$v['store_evaluate_addtime']);
			$evalue[$k]['eva_content'] = $v['store_evaluate_content'];
			$evalue[$k]['eva_username'] = $v['store_evaluate_username'];
			$evalue[$k]['eva_isnew'] = $v['store_evaluate_user_isnew'];
			$evalue[$k]['eva_userid'] = $v['store_evaluate_userid'];
			$evalue[$k]['eva_score'] = $score[$v['store_evaluate_score']];
			$evalue[$k]['eva_avatar'] = getMemberAvatarForID($v['store_evaluate_userid']);;	
			
			$evalue[$k]['eva_reply_time'] = date('d/m/y',$v['store_evaluate_reply_time']);
			$evalue[$k]['eva_reply'] = $v['store_evaluate_explain'];
			
			$evalue[$k]['eva_image'] = $this->evaluateImage($v['store_evaluate_image']);
			
			
		}	
		
		$page_count = $model->gettotalpage();
     	
        output_data(array('evalue' => $evalue), mobile_page($page_count));
    }
	
	
	//格式化评价图片
	private function evaluateImage($image){
		
		if(!empty($image)){
			
			$image =   explode(",", $image);	
			
			$images = array();
			foreach($image as $v){
				$images[] =  UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.$v;
				
			}
			
			return $images;
		}else{
			
			return array();
			
			
		}
		
		
		
	}
	
	
	public function evaluateReplyOp(){		
		$where = array();		
		$where['store_evaluate_storeid'] = $this->store_info['store_id'];
		$where['store_evaluate_id'] = $_POST['id'];
		
		$data = array(
			'store_evaluate_explain' => $_POST['content'],
			'store_evaluate_reply_time' => time()
		);		
		$row = model('evaluate_store')->editEvaluateStore($where,$data);		
		if($row){
			output_data($row);
		}else{
			output_error('回复失败');
		}
	}
	
	public function evaluateQuickReplyOp(){
		
		$quick = array(
			0 => '感谢您的支持，欢迎再次惠顾！',
			1 => '亲爱的顾客，十分抱歉本次服务没有让您获得最佳体验，我们会不断改善服务，期待您再次惠顾！',
			2 => '感谢您的认可，我们会继续努力，祝您生活愉快！',
			3 => '亲爱的顾客，非常抱歉影响您的用餐，感谢您的反馈，我们会在今后服务中改进，期待您再次惠顾！'
		);
	
	
		output_data(array('list'=>$quick));
		
		
		
		
	}
    
    
    
}
