<?php
/**
 * 商品管理
 */
defined('InMadExpress') or exit ('Access Invalid!');
class store_mealControl extends mobileSellerControl {
    public function __construct() {
        parent::__construct ();       
    }
	
	//套餐列表
	public function get_meal_listOp(){
		
		$model_goods = Model('goods');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['is_delete'] = 0;
		$where['is_old'] = 0;	
		$where['goods_is_set_meal'] = 1;
		
		//搜索关键词
		if(!empty($_POST['keyword'])){
				$ext_goods = $this->extSearchGoods($_POST['keyword']);				
				$where['goods_id'] = array('in',$ext_goods);
		}
		
		
		
		$goods_list = $model_goods->getGoodsList($where,'*');		
		$page_count = $model_goods->gettotalpage();
		$list_count = $model_goods->gettotalnum();
		
			
		foreach($goods_list as $k=> $v){
			$goods_data[$k] = $v;
			$goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
			$goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
			//$goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);			
		}				
		output_data(array('list' => $goods_data), mobile_page($page_count,$list_count));			
	}
	
	
	
	
	
	
	private function goodsImageFormat($image){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['file_name'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS .$v;
			$list[$k]['file'] = $v;
			$list[$k]['type'] ='url';
		}
		
		return $list;		
	}
	
	
	
	
	
	/**
	 * 新增
	 */
	public function get_goods_initOp() {
	 
		$goods_data = array();
					
		$goods_data['store_goods_class'] = $this->jsonStoreClass();		
		$foodbox = Model('foodbox_goods')->getGoodsList(TRUE);    			
		$goods_data['foodbox'] = $foodbox;
	
		$ingr = model('goods_attr')->getGoodsAttrList(TRUE);
		$ingr_data = array();
		foreach($ingr as $k=> $v){
			$ingr_data[$k]['attr_id'] =  $v['attr_id'];
			if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);				
				$ingr_data[$k]['attr_name'] = $new[0];
				$ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];			
			}else{
				$ingr_data[$k]['attr_name'] =  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];					
			}
			$ingr_data[$k]['active'] = 0;								
		}
		
		$goods_data['ingr'] = $ingr_data;
		
		
		//语言包。
		$lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));		
		foreach($lang as $k=> $v){
			
				$goods_name[$k]['lang'] = $v['language_en'];
				$goods_name[$k]['goods_name'] = '';
				$goods_description[$k]['lang'] = $v['language_en'];
				$goods_description[$k]['goods_description'] = '';
				
				$data_meal_specs['goods_list'] = array();
				$data_meal_specs['lang'][$k]=array(
					'lang' => $v['language_en'],
					'class_name' => '',					
				);	
				
				$store_language[$k]['lang'] = $v['language_en'];
		}
		
		
		$goods_data['store_language'] = $store_language;
		
	
	
		//初始化数据		
		$goods = array();
		$goods['goods_name'] = $goods_name;
		$goods['goods_description'] = $goods_description;
		$goods['goods_meal_specs'][] = $data_meal_specs;
	
				
		$goods['goods_ingr'] = array();
		$goods['is_old'] = 0;
		$goods['goods_code'] = $this->getGoodsCode();	
		$goods['goods_optional'] = array(array(
				'options_id' => ''				
		));		
		$goods['goods_state'] = '是' ;
		$goods['is_recommend'] = '否';
		$goods['goods_stock'] = 66 ;		
	
		$goods_data['goods'] = $goods;
	
	
		$options = model('goods_options')->getOptionsList(array('store_id' => $this->store_info['store_id'],'state'=> 0 ));
		
		foreach($options as  $k=>$v){
			$options_list[$k] = $v;
			$options_list[$k]['lang']  = unserialize($v['options_lang']);
			$options_list[$k]['options_name'] =$options_list[$k]['lang'][0]['options_name'];
		}
				
		$goodscode = array(
			array(
				'goods_code' => $goods['goods_code'],
				'goods_code_num' => 0
			)
		);
		
		$goods_data['goodscode'] = $goodscode;
		$goods_data['goods_image'] = array();
		$goods_data['options_list'] = $options_list;
		
		output_data($goods_data);
	
	}
	
	
	
	private function jsonStoreClass($type = ''){
					
		$class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
	
		if($type == 'list'){
			$store_goods_class[] = array(
				'stc_id' => 'all',
				'stc_name' =>'全部'
			);
		}
		
		foreach($class as $v){
			$data = $v;
			$data['stc_id'] = $v['stc_id'];
			$data['active'] = 0;
			$data['stc_lang_name'] = unserialize($v['stc_lang_name']);						
		//	$data['goods_count'] = $this->goodsCount($v['stc_id']);
			$store_goods_class[] = $data;
		}
	
		return $store_goods_class;
	
	}
	
	
	//生成商品编号
	private function getGoodsCode(){
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		$count = model('goods')->getGoodsCount($where);
		
		$code = $this->store_info['store_code'].'_i'.($count + 1);
	
		return $code;
		
	}
	
	
	
	public function get_meal_infoOp(){
		
		$goods_id = $_POST['goods_id'];		
		$goods_info = model('goods')->getGoodsInfo(array('store_id'=> $this->store_info['store_id'],'goods_id'=> $goods_id));
		
		if(!empty($goods_info)){
		
			$goods_info['goods_optional'] = count(unserialize($goods_info['goods_optional']))> 0 ? unserialize($goods_info['goods_optional']) : array(array('options_id'=>'')) ;
			$goods_info['goods_image'] = $this->goodsImageFormat($goods_info['goods_image']);
			$goods_info['is_recommend'] = $goods_info['is_recommend']  == 1 ? '是':'否';
			$goods_info['goods_state'] = $goods_info['goods_state']  == 1 ? '是':'否';
		
			//语言包。
			$lang = model('store_language')->getStoreLangList(array('store_id'=> $this->store_info['store_id']));		
			foreach($lang as $k=> $v){	
					
					$store_language[$k]['lang'] = $v['language_en'];
			}
			
			
			$goods_data['store_language'] = $store_language;
			
		
		
		
			$lang_goods =  $this->getLangGoods($goods_info['goods_id']);
			$data  = array();
			foreach($lang_goods as $k => $v){
			
				$goods_name[$k]['lang'] = $v['lang_name'];
				$goods_name[$k]['goods_name'] = $v['goods_name'];	
							
				$goods_description[$k]['lang'] = $v['lang_name'];
				$goods_description[$k]['goods_description'] = $v['goods_description'];
				
		
				
			}
			
			
			$goods = array();
			$goods['goods_name'] = $goods_name;
			$goods['goods_description'] = $goods_description;
			$goods['goods_meal_specs'][] = $data_meal_specs;
				
					
			$goods['goods_ingr'] = array();
			$goods['is_old'] = 0;
			$goods['goods_code'] = $this->getGoodsCode();	
			$goods['goods_optional'] = array(array(
					'options_id' => ''				
			));		
			$goods['goods_state'] = '是' ;
			$goods['is_recommend'] = '否';
			$goods['goods_stock'] = 66 ;		
		
			$goods_data['goods'] = $goods;
					
		
		
			$options = model('goods_options')->getOptionsList(array('store_id' => $this->store_info['store_id'],'state'=> 0));
			foreach($options as  $k=>$v){			
				$options_list[$k] = $v;
				$options_list[$k]['lang']  = unserialize($v['options_lang']);
				$options_list[$k]['options_name'] =$options_list[$k]['lang'][0]['options_name'];
			}
			
			$goods_data['options_list'] = $options_list;
			$goods_data['goods_image'] = $goods_info['goods_image'];
			$goods_data['goods'] = $goods;
				
			
			output_data($goods_data);
			
			
		}		
		
		print_r($goods_info);
		exit;
		
	}
	
	
	private function getLangGoods($goods_id){
		$list = model('goods_language')->getGoodsLangList(array('goods_id' => $goods_id));
		
		$data = array();
		foreach($list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['lang_name'] =  $v['lang_name'];	
			$data[$k]['goods_set_meal_specs'] = unserialize($v['goods_set_meal_specs']);					
		}
		return $data;
	}
	
	

	
	
	
	
	//保存套餐
	public function save_mealOp(){
		
		
		$model= model('goods');
		
		//获取商品
		$where = array(
			'goods_id' => $_POST['goods_id'],
			'store_id' => $this->store_info['store_id']
		);
		
		$goods_info = $model->getGoodsInfo($where);
		if(!empty($goods_info)){
			$goods_code = $goods_info['goods_code'];
			$goods_code_num = $goods_info['goods_code_num'] + 1;			
			//编辑
			$data = array('is_old' => 1);
			$where = array('goods_id' => $goods_info['goods_id']);			
			$model->editGoodsNew($data,$where);			
		}else{			
			$goods_code  = $_POST['goods_code'];
			$goods_code_num = 0;
		}
				
		$data=  array();	
		$data['goods_name'] = $_POST['goods_name'];
		$data['gc_id'] = $_POST['gc_id'];
		$data['store_name'] = $_POST['store_name'];		
		
		
		//获取商品分类名称
		$gc_name = model('store_goods_class')->getOneById($_POST['gc_id']);
		
		
		$data['gc_name'] = $gc_name['stc_name'];
		$data['store_name'] = $this->store_info['store_name_primary'];
		
		
		$data['goods_price'] = $_POST['goods_price'];
		$data['goods_code'] = $goods_code;
		$data['goods_addtime'] = time();
		$data['goods_ingr'] = $_POST['goods_ingr'];
				
		$goods_image = $this->imageAll($_POST['goods_image']);	
		$data['goods_image'] = $goods_image;	
		$data['goods_optional'] = serialize($_POST['goods_optional']);
		$data['goods_stock'] = $_POST['goods_stock'];
		$data['goods_state'] = $_POST['goods_state'] =='是' ? 1 : 0;
		$data['store_id'] = $this->store_info['store_id'];
		$data['is_recommend'] = $_POST['is_recommend'] == '是' ? 1: 0;
		$data['goods_code_num'] = $goods_code_num;
		$data['goods_is_set_meal'] = 1;
		
		$row = $model->addGoods($data);	
		
		if($row){	
		
			//更新分类表
			model('store_goods_extended')->delGoodsExt(array('goods_code'=>$goods_info['goods_code'],'store_id'=> $this->store_info['store_id']));
			
			$data_goods_ext = array(
				'stc_id' => $_POST['gc_id'],
				'store_id' => $this->store_info['store_id'],
				'goods_code' => $goods_code
			);		
			model('store_goods_extended')->addGoodsExt($data_goods_ext);
			
				
		
			$lang = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));
		
			foreach($lang as $v){
				
				$data_sub = array();	
				$data_sub['goods_id'] = $row;
				$goods_name = $_POST['goods_name'];
				foreach($goods_name as $vv){
					if($v['language_en'] == $vv['lang']){
						$data_sub['goods_name'] = $vv['goods_name'];
					}
				}	
				$data_sub['store_id'] 	 = $v['store_id'];				
			
			
				$goods_set_meal_specs = $_POST['goods_meal_specs'];
				$specs_item = array();
				foreach($goods_set_meal_specs as $k => $vv){					
					
					foreach($vv['lang'] as $lang){
						if($v['language_en'] == $lang['lang']){
							
							$specs_item[$k]['class_name'] = $lang['class_name'];							
						
							
						}
					}
					
					foreach($vv['goods_list'] as $g => $goods){
						foreach($goods['lang'] as $lang){
							if($v['language_en'] == $lang['lang_name']){
								$goods_list[$g] = $lang;
							}
						}						
						
					}
					
					$specs_item[$k]['goods_list'] = $goods_list;	
					
					
					
				}				
				
				$data_sub['goods_set_meal_specs']     = serialize($specs_item);				
			
			
				$goods_description = $_POST['goods_description'];
				foreach($goods_description as $vv){
					if($v['language_en'] == $vv['lang']){
						$data_sub['goods_description'] = $vv['goods_description'];
					}
				}
	
				$data_sub['lang_id']       = $v['language_id'];			
				$data_sub['lang_name']       = $v['language_en'];			
				
			//	print_r($data_sub);
				
				
				model('goods_language')->addGoodsLang($data_sub);				
			
			}
			output_data(array('message' => '编辑成功'));			
		}else{
			output_error('编辑失败');			
		}
		
		
		
		
	}
	
	//删除套餐
	public function del_mealOp(){
		
		
	}
	
	
	
	//上传商品图片
	
	public function  upload_imageOp(){
		
		$store_id = $this->store_info['store_id'];
	    $upload = new UploadFile();
	    $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath()); 
	    $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
	    $result = $upload->upfile('file');
	    if (!$result) {
			output_error($upload->error);
			
	    }else{
			$img_path = $upload->getSysSetPath() . $upload->file_name;
			output_data(array('image' => $img_path));			
		}
		
	}
	
	
	//保存裁剪的图片
	public function save_imageOp(){
	
	   $upload = new UploadFile();
	   $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();		
	   $file_name = $this->base64_image_content($_POST['image'],$path);
	
			output_data(array('image' => $file_name));		
		
	}
	
	private function imageAll($imagelist){
		$upload = new UploadFile();
	    $path = BASE_UPLOAD_PATH .DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS . $upload->getSysSetPath();		
	
		$file_name = array();
		foreach($imagelist as $v){
			if($v['type'] == 'url'){
				$file_name[] = $v['file'];
			}else{
				$file_name[] = $this->base64_image_content($v['file_name'],$path);
			}
		}	
		
		return implode(",", $file_name);		
	}
	
	
	private function  base64_image_content($base64_image_content,$path){
	//匹配出图片的格式
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
			$type = $result[2];
			$new_file = $path;
			if(!file_exists($new_file)){
				//检查是否有该文件夹，如果没有就创建，并给予最高权限
				mkdir($new_file, 0700);
			}
			$file_name = time().rand(10000,99999).".{$type}";
			$new_file = $new_file.$file_name ;
			if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
				return $file_name;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	
	
	
	
}

?>