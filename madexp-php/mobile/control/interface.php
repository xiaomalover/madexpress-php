<?php
/**
 * 界面操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class interfaceControl extends mobileAdminControl {
  
    
    public function __construct(){
        parent::__construct();
	
    }

	
	//列表
	public function interface_listOp(){		
		
		$model_adv  = Model('adv_new');
		$condition  = array();
		
		if ($_POST['adv_title'] != '') {
			$condition["adv_title"] = $_POST['adv_title'];
		}
		
		if ($_POST['query'] != '' && in_array($_POST['qtype'],array('ap_name'))) {
		    $condition[$_POST['qtype']] = $_POST['query'];
		}
		$sort_fields = array('ap_class','ap_display','is_use','adv_start_date','adv_end_date','click_num');
		if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
		    $order = $_POST['sortname'].' '.$_POST['sortorder'];
		}
			
		$condition['ap_id'] = $_POST['ap_id'] >0 ? $_POST['ap_id'] : 1;
			
		$adv_list  = $model_adv->getAdvList($condition,'*',$this->page,$order);   
		
		$page_count = $model_adv->gettotalpage();
		$list_count = $model_adv->gettotalnum();
	
		foreach ($adv_list as $k => $v) {
		   	$data = array();		
			$data['adv_code'] = $v['adv_code'];
			$data['adv_id'] = $v['adv_id'];
			$data['ap_name'] = '首页banner';			
			$data['adv_title'] = $v['adv_ap_title'];
		    $data['adv_title'] = $v['adv_title'];
		    $data['adv_start_date'] = date('Y-m-d',$v['adv_start_date']);
		    $data['adv_end_date'] = date('Y-m-d',$v['adv_end_date']);
		    $data['is_show'] = $v['is_show'] == 0 ? false : true;
		    $data['click_num'] = $v['click_num'];	
				
		    $list[] = $data;
		}
				
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
			
	}


	 public function adv_saveOp(){
	 		$adv  = Model('adv_new');
	         $upload     = new UploadFile();
			 $gdurl = 'http://'.$_SERVER['SERVER_NAME'];
	         /**
	          * 验证
	          */
	 		$_POST = json_decode(file_get_contents('php://input'),true);
			
			//var_dump($_POST["adv_content"]);die;
			
			if($_POST["adv_content"]){
				$base64_image = str_replace(' ', '+', $_POST["adv_content"]);
				//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
				$img_url = $_POST["adv_id"]?str_replace($gdurl,"",$_POST["adv_content"]):'';//编辑时没上传图片，不更新这个值
				if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image, $result)){
					//定义图片储存文件目录
					$dir = '../data/upload/interface/'.date('ymd');
					//定义文件名称
					$picname = date("his") . '_' . rand(10000, 99999);
					if (!is_dir($dir)){
						//如果不存在就创建该目录
						mkdir($dir,0777,true);  
					}  
					//获取图片后缀
					if($result[2] == 'jpeg'){
						$picdir=$picname.'.jpg';
					}else{
						$picdir=$picname.'.'.$result[2];
					}
					//图片名称
					$image_url = $dir.'/'.$picdir;
					//储存图片
					if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image)))){
						$img_url = $image_url;
						$img_url = str_replace("..","",$img_url);
					}
				}
			}
		
			
			if($_POST["adv_content2"]){
				$base64_image2 = str_replace(' ', '+', $_POST["adv_content2"]);
				//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
				$img_url2 = $_POST["adv_id"]?str_replace($gdurl,"",$_POST["adv_content2"]):'';//编辑时没上传图片，不更新这个值
				if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image2, $result)){
					//定义图片储存文件目录
					$dir = '../data/upload/interface/'.date('ymd');
					//定义文件名称
					$picname = date("his") . '_' . rand(10000, 99999);
					if (!is_dir($dir)){
						//如果不存在就创建该目录
						mkdir($dir,0777,true);  
					}  
					//获取图片后缀
					if($result[2] == 'jpeg'){
						$picdir2=$picname.'.jpg';
					}else{
						$picdir2=$picname.'.'.$result[2];
					}
					//图片名称
					$image_url2 = $dir.'/'.$picdir2;
					//储存图片
					if (file_put_contents($image_url2, base64_decode(str_replace($result[1], '', $base64_image2)))){
						$img_url2 = $image_url2;
						$img_url2 = str_replace("..","",$img_url2);
					}
				}
			}
		
			
			if($_POST["adv_content3"]){
				$base64_image3 = str_replace(' ', '+', $_POST["adv_content3"]);
				//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
				$img_url3 = $_POST["adv_id"]?str_replace($gdurl,"",$_POST["adv_content3"]):'';//编辑时没上传图片，不更新这个值
				if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image3, $result)){
					//定义图片储存文件目录
					$dir = '../data/upload/interface/'.date('ymd');
					//定义文件名称
					$picname = date("his") . '_' . rand(10000, 99999);
					if (!is_dir($dir)){
						//如果不存在就创建该目录
						mkdir($dir,0777,true);  
					}  
					//获取图片后缀
					if($result[2] == 'jpeg'){
						$picdir3=$picname.'.jpg';
					}else{
						$picdir3=$picname.'.'.$result[2];
					}
					//图片名称
					$image_url3 = $dir.'/'.$picdir3;
					//储存图片
					if (file_put_contents($image_url3, base64_decode(str_replace($result[1], '', $base64_image3)))){
						$img_url3 = $image_url3;
						$img_url3 = str_replace("..","",$img_url3);
					}
				}
			}
		
		
		
			
	         $obj_validate = new Validate();
	         $validate_arr = array();
	         $validate_arr[] = array("input"=>$_POST["adv_title"], "require"=>"true", "message"=>$lang['adv_can_not_null']);         
	         $validate_arr[] = array("input"=>$_POST["ap_id"], "require"=>"true", "message"=>$lang['must_select_ap']);
	         $validate_arr[] = array("input"=>$_POST["adv_start_time"], "require"=>"true", "message"=>$lang['must_select_start_time']);
	         $validate_arr[] = array("input"=>$_POST["adv_end_time"], "require"=>"true", "message"=>$lang['must_select_end_time']);
	      //   $validate_arr[] = array("input"=>$_FILES['adv_pic']['name'], "require"=>"true", "message"=>$lang['picadv_null_error']);
	         $obj_validate->validateparam = $validate_arr;
	         $error = $obj_validate->validate();
	         if ($error != ''){
	               exit(json_encode(array('code'=> 400,'msg'=>$error))); 
	 			  
	         }else {				
	             $insert_array['adv_title']       = trim($_POST['adv_title']);
	             $insert_array['ap_id']           = intval($_POST['ap_id']);
	             $insert_array['adv_start_date']  = strtotime($_POST['adv_start_time']);
	             $insert_array['adv_end_date']    = strtotime($_POST['adv_end_time']);				
	 			if($insert_array['adv_end_date'] <   $insert_array['adv_start_date']){					
	 				 exit(json_encode(array('code'=> 400,'msg'=>'结束时间不得小于开始时间'))); 
	 		    }
	             $insert_array['is_show']         = '1';
	 			$insert_array['adv_code']           = $_POST['adv_code'];	
	 			$insert_array['adv_pic_url']        = $_POST['adv_pic_url'];	
	 			$insert_array['adv_text']           = $_POST['adv_text'];
				
				$insert_array['adv_content']         = $img_url;
				$insert_array['adv_content2']         = $img_url2;
				$insert_array['adv_content3']         = $img_url3;
					
	 		    if($_POST['adv_id'] > 0){
	 			   $where = array(
	 			  	 	'adv_id' => $_POST['adv_id']
	 			   );
	 			   $result = $adv->editAdv($where,$insert_array);
	 		   }else{
	 			   $result = $adv->addAdv($insert_array);				   
	 			}
	             //广告信息入库
	             if ($result){
	                  output_data($result);
	             }else {
	                  output_error($result);
	             }
	 		}
	 	
	 }
	 
	 //上传图片
	 
	 public function uploadAdsOp(){
	 	  $upload     = new UploadFile();		  
	 	  $upload->set('default_dir',ATTACH_ADV);
	       $result = $upload->upfile('file');
	       if (!$result){
	 			echo json_encode(array('code'=> 40000,'msg'=>$upload->error));
	        }else{
	 		    echo json_encode(array('code'=> 20000,'url'=>$upload->file_name)); 
	 	 }
	 		
	 		
	 	
	 }
	
	public function adv_editOp(){
		$adv  = Model('adv_new');
		
		$where = $data = array();
		$where["adv_id"] = $adv_id = $_POST['adv_id'];
		$data["is_show"] = $is_show = ($_POST['is_show']) ? 1 : 0;

		$result = $adv->editAdv($where,$data);
		
		output_data($result);
	}
		
	public function adv_delOp(){
		$adv  = Model('adv_new');
		
		$where = array();
		$where["adv_id"] = $adv_id = $_POST['adv_id'];

		$result = $adv->delAdv($where);
		
		output_data($result);
	}
	
	public function ap_manageOp(){
			
	
		$model  = Model('adv_ap');	   
		$list = $model->getAdvApList(TRUE);		
		$data = array();
		foreach($list as $k => $v){		
			$v['adv_num'] = $this->getAdvNum($v['ap_id']);		
			$data[$k] = $v;
		}	
		output_data($data);
	
	}
	
	private function getAdvNum($ap_id){
			$adv  = Model('adv_new');			
			$where = array();
			$where['ap_id'] = $ap_id;
			return $adv->getAdvCount($where);
	}
	
	
	public function getAdvInfoOp(){
			$adv  = Model('adv_new');			
			$where = array();
			$where['adv_id'] = $_POST['adv_id'];
			$res = $adv->getAdvInfo($where);
			$res["adv_start_time"] = date("Y-m-d",$res["adv_start_date"]);
			$res["adv_end_time"] = date("Y-m-d",$res["adv_end_date"]);
			
			
			$res['adv_content'] = 'http://'.$_SERVER['SERVER_NAME'].$res['adv_content'];//必填项
			
			if($res['adv_content2']){
				$res['adv_content2'] = 'http://'.$_SERVER['SERVER_NAME'].$res['adv_content2'];
			}
			
			if($res['adv_content3']){
				$res['adv_content3'] = 'http://'.$_SERVER['SERVER_NAME'].$res['adv_content3'];
			}

			output_data($res);
			
	}
	
	
}
