<?php
/**
 *店铺合同这类的东西
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_fileControl extends mobileSellerControl
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     *
     *
     */
    public function indexOp()
    {
        $data = array();
        
        $where = array(
            'store_id' => $this->store_info['store_id'],
            'is_default' => 1
        );
        $contract = model('store_contract')->getContractInfo($where);
        $contract['start_time'] = date('d/m/y', $contract['contract_start_time']);
        $contract['end_time'] = date('d/m/y', $contract['contract_end_time']);
        $contract['contract_sole'] = $contract['contract_sole'] == 1 ? 'ME平台独家入驻' : '非独家' ;
        $contract['contract_lunchbox'] = $contract['contract_lunchbox'] == 1 ? '收费' : '免费' ;
        $data['contract'] = $contract;
        
        $license = model('store_license')->getLicenseInfo($where);
        $license['start_time'] = date('d/m/y', $license['license_start_time']);
        $license['end_time'] = date('d/m/y', $license['license_end_time']);
        $data['license'] = $license;
            
        
        $hygiene = model('store_hygiene')->getHygieneInfo($where);
        $hygiene['start_time'] = date('d/m/y', $hygiene['hygiene_start_time']);
        $hygiene['end_time'] = date('d/m/y', $hygiene['hygiene_end_time']);
        $data['hygiene'] = $hygiene;
        
		
		$bill['money'] = $this->store_info['available_predeposit']; //账号余额
		$bill['bill_type'] = $this->store_info['bill_type'];
		$bill['bill_date'] = 'N/A';
		$data['bill'] = $bill;
		
		
		
        output_data($data);
    }
    
    
    
    //保存合同
    
    public function contract_editOp()
    {
        $model = model('store_contract');
        if (chksubmit()) {
            $contract_id = $_POST['contract_id'];
            $data = array(
                'contract_start_time' => strtotime($_POST['date'][0]),
                'contract_end_time' =>  strtotime($_POST['date'][1]),
                'contract_rate' => $_POST['contract_rate'],
                'contract_sole' => $_POST['contract_sole'],
                'contract_lunchbox' => $_POST['contract_lunchbox']
             );
            if ($contract_id > 0) {
                $where = array(
                    'contract_id' => $contract_id,
                    'store_id' => $this->store_info['store_id']
                );
                $result = $model->editContract($where, $data);
            } else {
                $result = $model->addContract($data);
            }
            if ($result) {
                output_data('操作成功');
            } else {
                output_error('操作失败');
            }
        }
        $where = array(
                    'store_id' => $this->store_info['store_id'],
                    'is_default' => 1
        );
            
        $contract = $model->getContractInfo($where);
        $data['form_submit'] = 'ok';
        $data['contract_id'] = $contract['contract_id'];
        $data['store_id'] = $contract['store_id'];
        $data['contract_file'] = $contract['contract_file'];
        $data['contract_lunchbox'] = $contract['contract_lunchbox'];
        $data['contract_sole'] = $contract['contract_sole'];
        $data['contract_rate'] = $contract['contract_rate'];
        $data['date'] =array(
                date('Y-m-d', $contract['contract_start_time']),date('Y-m-d', $contract['contract_end_time'])
            );
            
        output_data(array('contract'=>$data));
    }
    
    
    //列表
    
    public function store_file_listOp()
    {
        $type = $_POST['type'];
        if ($type == 'contract') { //合同
            $this->contract_listOp();
        }
        
        if ($type == 'license') { //营业执照
            $this->licenseOp();
        }
        
        if ($type == 'hygiene') {
            $this->hygieneOp();
        }
    }
    
    //删除
    public function store_file_delOp()
    {
        $type = $_POST['type'];
        if ($type == 'contract') { //合同
            $this->contract_delOp();
        }
        
        if ($type == 'license') { //营业执照
            $this->license_delOp();
        }
        
        if ($type == 'hygiene') {
            $this->hygiene_delOp();
        }
    }
    
    
    
    
    
    //合同列表
    public function contract_listOp()
    {
        $model_contract = Model('store_contract');
        $condition = array();
        if (!empty($_POST['keyword'])) {
            $condition['contract_filename'] = array('like', '%'.$_POST['keyword'].'%');
        }
        $condition['store_id'] = $this->store_info['store_id'];
        $list = $model_contract->getContractList($condition, $this->page, 'contract_id desc');
        $page_count = $model_contract->gettotalpage();
        $list_count = $model_contract->gettotalnum();
        $data = array();
        foreach ($list as $k => $v) {
            //$data[$k] = $v;
            $data[$k]['filename'] =$v['contract_filename'];
            $data[$k]['is_default'] = $v['is_default'] == 1 ? '生效中':'---';
            $data[$k]['start_time'] = date('d M y H:i:s', $v['contract_start_time']);
            $data[$k]['end_time'] = date('d M y H:i:s', $v['contract_end_time']);
            $data[$k]['addtime'] = date('d M y H:i:s', $v['addtime']);
            $data[$k]['id'] = $v['contract_id'];
        }
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
    }
    
    //删除合同
    public function contract_delOp()
    {
        $where = array();
        $where['contract_id'] = intval($_POST['id']);
        $where['store_id'] = $this->store_info['store_id'];
        $row = model('store_contract')->delContract($where);
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    
    
    
    
    ///营业执照
    public function license_editOp()
    {
        $model = model('store_license');
        if (chksubmit()) {
            $license_id = $_POST['license_id'];
            $data = array(
                'license_start_time' => strtotime($_POST['date'][0]),
                'license_end_time' =>  strtotime($_POST['date'][1]),
                'license_acn' => $_POST['license_acn'],
                'license_abn' => $_POST['license_abn'],
                'store_id' => $this->store_info['store_id']
             );
            if ($license_id > 0) {
                $where = array(
                    'license_id' => $license_id,
                    'store_id' => $this->store_info['store_id']
                );
                
                $result = $model->editLicense($where, $data);
            } else {
                $result = $model->addLicense($data);
            }
            
            if ($result) {
                output_data('操作成功');
            } else {
                output_error('操作失败');
            }
        }
        
        
        $where = array(
            'store_id' => $this->store_info['store_id'],
            'is_default' => 1
          );
        $license_data = $model->getLicenseInfo($where);
        $license = array();
        $license['form_submit'] = 'ok';
        $license['store_id'] = $license_data['store_id'] ;
        $license['license_id'] = $license_data['license_id'] ;
        $license['license_acn'] = $license_data['license_acn'] ;
        $license['license_abn'] = $license_data['license_abn'] ;
        $license['license_file'] = $license_data['license_file'];
        $license['date'] =array(
                date('Y-m-d', $license_data['license_start_time']),date('Y-m-d', $license_data['license_end_time'])
          );
          
        output_data(array('license' => $license));
    }
    
    
    //营业执照
    public function licenseOp()
    {
        $model_license= Model('store_license');
        $condition = array();
        if (!empty($_POST['keyword'])) {
            $condition['license_filename'] = array('like', '%'.$_POST['keyword'].'%');
        }
       
        $condition['store_id'] = $this->store_info['store_id'];
        $list = $model_license->getLicenseList($condition, 10, 'license_id desc');
        $page_count = $model_license->gettotalpage();
        $list_count = $model_license->gettotalnum();
        
        $data = array();
        foreach ($list as $k => $v) {
            $data[$k]['id'] =$v['license_id'];
            $data[$k]['is_default'] = $v['is_default'] == 1 ? '生效中':'---';
            $data[$k]['filename'] =$v['license_filename'];
            $data[$k]['start_time'] = date('d M y H:i:s', $v['license_start_time']);
            $data[$k]['end_time'] = date('d M y H:i:s', $v['license_end_time']);
            $data[$k]['addtime'] = date('d M y H:i:s', $v['addtime']);
        }
        
        
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
    }
    
    //删除合同
    public function license_delOp()
    {
        $where = array();
        $where['license_id'] = intval($_POST['id']);
        $where['store_id'] = $this->store_info['store_id'];
        $row = model('store_license')->delLicense($where);
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    
    //卫生许可证
    
    public function hygiene_editOp()
    {
        $model = model('store_hygiene');
        if (chksubmit()) {
            $hygiene_id = $_POST['hygiene_id'];
            $data = array(
                'hygiene_start_time' => strtotime($_POST['date'][0]),
                'hygiene_end_time' =>  strtotime($_POST['date'][1]),
                'hygiene_level' => $_POST['hygiene_level'],
                'store_id' => $this->store_info['store_id']
             );
            
            if ($hygiene_id > 0) {
                $where = array(
                    'hygiene_id' => $hygiene_id,
                    'store_id' => $this->store_info['store_id']
                );
                $result = $model->editHygiene($where, $data);
            } else {
                $result = $model->addHygiene($data);
            }
            
            if ($result) {
                output_data('操作成功');
            } else {
                output_error('操作失败');
            }
        }
        
        $where = array(
            'store_id' => $this->store_info['store_id'],
            'is_default' => 1
          );
         
        $hygiene = $model->getHygieneInfo($where);
        $data['form_submit'] = 'ok';
        
        $data['hygiene_level'] = $hygiene['hygiene_level'];
        $data['hygiene_id'] = $hygiene['hygiene_id'];
        $data['hygiene_file'] = $hygiene['hygiene_file'];
        $data['date'] =array(
                date('Y-m-d', $hygiene['hygiene_start_time']),date('Y-m-d', $hygiene['hygiene_end_time'])
          );
          
        output_data(array('hygiene'=>$data));
    }
    
    
    //卫生许可证
    public function hygieneOp()
    {
        $model_hygiene= Model('store_hygiene');
        $condition = array();
        if (!empty($_POST['keyword'])) {
            $condition['hygiene_filename'] = array('like', '%'.$_POST['keyword'].'%');
        }
       
        $condition['store_id'] = $this->store_info['store_id'];
        $list = $model_hygiene->getHygieneList($condition, 10, 'hygiene_id desc');
        $page_count = $model_hygiene->gettotalpage();
        $list_count = $model_hygiene->gettotalnum();
        
        
        
        $data = array();
        foreach ($list as $k => $v) {
            $data[$k]['id'] =$v['hygiene_id'];
            $data[$k]['is_default'] = $v['is_default'] == 1 ? '生效中':'---';
            $data[$k]['filename'] 	= $v['hygiene_filename'];
            $data[$k]['start_time'] = date('d M y H:i:s', $v['hygiene_start_time']);
            $data[$k]['end_time'] = date('d M y H:i:s', $v['hygiene_end_time']);
            $data[$k]['addtime'] = date('d M y H:i:s', $v['addtime']);
        }
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
    }
    
    //删除卫生许可
    public function hygiene_delOp()
    {
        $where = array();
        $where['hygiene_id'] = intval($_POST['id']);
        $where['store_id'] = $this->store_info['store_id'];
        $row = model('store_hygiene')->delHygiene($where);
        if ($row) {
            output_data('删除成功');
        } else {
            output_error('删除失败');
        }
    }
    
    
    
    
    
    
    public function bill_editOp()
    {
		
		 if (chksubmit()) {			 
			 $where = array();
			 $where['store_id'] = $this->store_info['store_id'];
			 $update = array();
			 $update['bill_type'] = $_POST['bill_type'];
			 
			
			 $row = model('store')->editStore($update,$where);	
			 if ($row) {
			     output_data('修改成功');
			 } else {
			     output_error('修改失败');
			 }
		 }
				
		$data = array();
		$data['bill_type'] = $this->store_info['bill_type'];
		output_data(array('bill'=>$data));
        
    }
}
