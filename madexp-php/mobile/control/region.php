<?php
/**
 * 
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class regionControl extends mobileAdminControl {

    public function __construct(){
        parent::__construct();
    }


	public function get_regionOp(){
		$data= array();
		
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1));		
	//	$region_data = array();
		$feature = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
			//$region_data[$v['region_id']] = $v;
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		$region_data_list = model('region')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		output_data(array('map_region'=>$result,'region_list'=> $this->getRegionList(),'region_data'=>$region_data));
		//echo json_encode(array('code' => 200,'data'=>$data));		
		
	}

	private function getRegionList(){
		$model = model('region');
		$region = $model->getRegionList(array('region_parent_id' => 0,'region_enable'=> 1));
		$list = array();
		foreach($region as $v){
			
			$child_data = $model->getRegionList(array('region_parent_id' => $v['region_id']));
			$child = array();
			foreach($child_data as $c){
				$c['is_money'] = $c['is_money'] == 1 ? true : false;
				$child[] = $c;
			}
			
			$v['child'] = $child;
			$v['region_small_num'] = count($v['child']);
			$list[] =$v;
		}
		
		return $list;
	}

	public function get_old_regionOp(){
				
		$model = model('region');
		$region = $model->getRegionList(array('region_parent_id' => 0),'*','region_version');
		$list = array();
		foreach($region as $v){		
			$v['region_version_date'] = date('d/m/y',$v['region_version_date']);
			$v['region_version_time'] = $v['region_version_date'];
			$list[] =$v;
		}
		
		output_data(array('list' => $list));
		
		
	}


	//保存REGION //保留
	/*
	public function save_regionOp(){
		
		$model = model('region');
		$post = json_decode($_POST['region_data'],true);	
		$i = 0;
		foreach($post as  $v){
			if($v['region_parent_id'] == 0 ){
				
				$child= $this->childRegion($v['region_id'],$post);	
				
				$v['region_version'] = $_POST['region_version'];
				$v['region_version_text']= $_POST['region_version_text'];
				$v['region_version_date']= $_POST['region_version_date'];
	
				$data = array(
					'region_name' =>  $v['region_name'],
					'region_coordinate' => $v['region_coordinate'],
					'region_color' => $v['region_color'],
					'region_type' => $v['region_type'],
					'region_parent_id' => $v['region_parent_id'],
					'region_en' => $v['region_en'],
					'region_version' => $v['region_version'],
					'region_version_text' => $v['region_version_text'],
					'region_version_date' => time()					
				);								
				$row = $model->addArea($data);	
							
				foreach($child as $c){					
					$data = array(
						'region_name' =>  $c['region_name'],
						'region_coordinate' => $c['region_coordinate'],
						'region_color' => $c['region_color'],
						'region_type' => $c['region_type'],
						'region_parent_id' => $row,
						'region_version' => $v['region_version'],
						'region_version_text' => $v['region_version_text'],
						'region_version_date' => time()				
						
					);						
					$model->addArea($data);
					
				}
				
			}			
		}		
		///写入数据库
		output_data('保存成功');	
	}
	
	*/
	
	//保存编辑
	public function save_regionOp(){
			$model = model('region');		
			$data =  json_decode($_POST['region_data'],true);
			//print_r($data);
			foreach($data as $v){				
				if($v['options'] == 'add'){			
					$point = $this->point($v['region_coordinate']);
					$data = array(
						'region_name' =>  $v['region_name'],
						'region_coordinate' =>$v['region_coordinate'],
						'region_color' => $v['region_color'],
						'region_type' => $v['region_type'],
						'region_parent_id' => $v['region_parent_id'],
						'region_en' => $v['region_en'],
						'region_nav_coordinate' => $point['nav'], //钱标导航点
						'region_money_coordinate' => $point['money'], //钱标围栏
						'region_sector_coordinate' => $point['sector'], //扇形点			
						'region_city_id' => 1,
						'region_enable'  => 1,
					);				
						
					//print_r($data);
					$model->addArea($data);						
				}
				
				if($v['options'] == 'update'){					
					$update = array(
						'region_name' =>  $v['region_name'],
						'region_coordinate' => $v['region_coordinate'],
						'region_color' => $v['region_color'],
						'region_parent_id' => $v['region_parent_id'],
					);	
					$model->editArea($update,array('region_id' => $v['region_id']));	
				}
				
				if($v['options'] == 'del'){
										
					
				}
				
				
			}
			
			output_data('操作成功');
		
	}
	
	//格式坐标
	private function point($point){
		
		$point = explode('|',$point);		
		$data['nav'] = $point[0];
		$data['sector'] = $point[0];		
		$money = array();
		$money[] = $point[0];
		$money[] = $point[1];
		$money[] = $point[2];
		$money[] = $point[3];
		$money[] = $point[0];		
		$data['money'] = implode('|',$money);		
		return $data;		
	}
	
	
	
	 
	private function childRegion($id, $data){
		
		$list = array();
		$i = 0;
		foreach($data as $v){			
			if($v['region_parent_id'] == $id){				
				$list[$i] = $v;
				$i++;
			}
		}
		
		return $list;
	}
	 
	 
	
	//启用版本
	public function region_enableOp(){
		
		$version = $_POST['version'];
		$date = $_POST['version_date'];
		$model = model('region');		
		$row = $model->editArea(array('region_enable' => 0),TRUE);
		if($row){			
			$where = array(
				'region_version' => $version,
				'region_version_date' => $date
			);
		
			/*
			增加触发
			*/
			
		
			/*
			
			*/
		
			$model->editArea(array('region_enable' => 1),$where);									
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
		
		
	}
	 
	
	
	//新增大区
	public function add_big_nameOp(){
		
		$model = model('region');
		
		$info = $model->getRegionInfo(array('region_enable' => 1));
		if(empty($info)){			
			output_error('缺少基础数据支持');			
		}
		
			
			
		$data=  array(
			'region_name' =>  $_POST['big_region_name'],		
			'region_type' => 1,
			'region_parent_id' => 0,			
			'region_version' => $info['region_version'],
			'region_version_text' => $info['region_version_text'],
			'region_version_date' => $info['region_version_date'],
			'region_enable' => 1
		);
	
		
		
		$row = $model->addArea($data);
		
		if($info){
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}	
		
	}
	
	//批量编辑名称
	public function edit_bigOp(){
		
		
		$data =  json_decode($_POST['region_data'],true);
		//print_r($data);
		if(empty($data)){			
			output_error('参数错误');
		}
		$model = model('region');		
		foreach($data as $v){		
			
			$model->editArea(array('region_name'=>$v['region_name']),array('region_id'=>$v['region_id']));
		}				
		output_data('操作成功');
		
		
	}
	
	//删除
	public function del_bigOp(){
		
		$model = model('region');
		$id = intval($_POST['region_id']);
		
		$where = array(
			'region_id' => $id
		);
		$row = $model->delArea($where);
		
		if($row){			
			$where = array(
				'region_parent_id' => $id
			);
			$model->delArea($where);
			output_data('删除成功');
		}else{
			output_error('删除失败');			
		}
		
				
		
	}
	
	
}
