<?php
/**
 * 
 *
 *
 */



defined('InMadExpress') or exit('Access Invalid!');
class brandControl extends mobileHomeControl {
    public function __construct() {
        parent::__construct();
    }


	public function indexOp(){
		
		$list = model('store')->getStoreList(true);
		
		echo count($list);
		foreach($list as $v){
			
			$points = array(
				'lat' => $v['store_lat'],
				'lng' => $v['store_lng']
			);				
		    $this->zuobiao($points,$v['store_id']);			
		}
		
		
	}









   
  	private function zuobiao($points,$store_id){
		 //145.003877,
		$model = model('store');
		$region =  model('region')->getRegionList(array('region_type'=> 2));		
		foreach($region as $k =>  $v){		 
		 	
		   $abc = $this->points_array($v['region_coordinate']);
		   $row = 	$this->is_point_in_polygon($points,$abc);			
			if($row === true){
				
				$data = array(
					'region_name' => $v['region_name'],
					'region_sm_id' => $v['region_id'],
					'region_id' => $v['region_parent_id'],
					'region_color' => $v['region_color']
					
				);
				
				$where = array(
					'store_id' => $store_id
				);
				
				print_r($data);
				print_r($where);
				
				$model->editStore($data,$where);
				
				break;
				
			}
			
			
		}
	   
  }
   
	
	private function points_array($points){
		
		$points_array = explode('|',$points);
		$data = array();
		foreach($points_array as $k => $v){
			$bs = explode(',',$v);
			
			$data[$k]['lng'] = $bs[0];
			$data[$k]['lat'] = $bs[1];
		}
		
		return $data;
		
	}
	
	
	
	private function is_point_in_polygon($point, $pts) {
		$N = count($pts);
		$boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
		$intersectCount = 0;//cross points count of x 
		$precision = 2e-10; //浮点类型计算时候与0比较时候的容差
		$p1 = 0;//neighbour bound vertices
		$p2 = 0;
		$p = $point; //测试点
	 
		$p1 = $pts[0];//left vertex        
		for ($i = 1; $i <= $N; ++$i) {//check all rays
			// dump($p1);
			if ($p['lng'] == $p1['lng'] && $p['lat'] == $p1['lat']) {
				return $boundOrVertex;//p is an vertex
			}
			 
			$p2 = $pts[$i % $N];//right vertex            
			if ($p['lat'] < min($p1['lat'], $p2['lat']) || $p['lat'] > max($p1['lat'], $p2['lat'])) {//ray is outside of our interests
				$p1 = $p2; 
				continue;//next ray left point
			}
			 
			if ($p['lat'] > min($p1['lat'], $p2['lat']) && $p['lat'] < max($p1['lat'], $p2['lat'])) {//ray is crossing over by the algorithm (common part of)
				if($p['lng'] <= max($p1['lng'], $p2['lng'])){//x is before of ray
					if ($p1['lat'] == $p2['lat'] && $p['lng'] >= min($p1['lng'], $p2['lng'])) {//overlies on a horizontal ray
						return $boundOrVertex;
					}
					 
					if ($p1['lng'] == $p2['lng']) {//ray is vertical                        
						if ($p1['lng'] == $p['lng']) {//overlies on a vertical ray
							return $boundOrVertex;
						} else {//before ray
							++$intersectCount;
						}
					} else {//cross point on the left side
						$xinters = ($p['lat'] - $p1['lat']) * ($p2['lng'] - $p1['lng']) / ($p2['lat'] - $p1['lat']) + $p1['lng'];//cross point of lng
						if (abs($p['lng'] - $xinters) < $precision) {//overlies on a ray
							return $boundOrVertex;
						}
						 
						if ($p['lng'] < $xinters) {//before ray
							++$intersectCount;
						} 
					}
				}
			} else {//special case when ray is crossing through the vertex
				if ($p['lat'] == $p2['lat'] && $p['lng'] <= $p2['lng']) {//p crossing over p2
					$p3 = $pts[($i+1) % $N]; //next vertex
					if ($p['lat'] >= min($p1['lat'], $p3['lat']) && $p['lat'] <= max($p1['lat'], $p3['lat'])) { //p.lat lies between p1.lat & p3.lat
						++$intersectCount;
					} else {
						$intersectCount += 2;
					}
				}
			}
			$p1 = $p2;//next ray left point
		}
	 
		if ($intersectCount % 2 == 0) {//偶数在多边形外
			return false;
		} else { //奇数在多边形内
			return true;
		}
	}
		
	
   
   
}
