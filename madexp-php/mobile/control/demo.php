<?php
/**
 * 前台登录 退出操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class demoControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }


	public function apiaoOp(){

		
		model('order_bill')->store_bill_full(846);


	}

	public function bpiaoOp(){

		
		model('order_bill')->store_bill_partial(847);


	}

	public function cpiaoOp(){

		$order_id = $_GET['order_id'];

		model('order_bill')->bill($order_id);


	}


	public function ptOp(){

		 //生成票据所需
		 $invoice_data = array();
		 $invoice_data['store_id'] = 6;		 
		 $invoice_data['me']  = 1;					 
		 //	print_r($invoice_data);			
		 $pdf = model('pdf')->createPdf($invoice_data,'me_platform_invoice_merchant');


	}



	public function pt2Op(){

		$invoice_data = array();
		$invoice_data['store_id'] =6;
		$invoice_data['order_id'] = 852;
		$invoice_data['me'] = 1;	
		//	print_r($invoice_data);			
		$pdf =  model('pdf')->createPdf($invoice_data,'me_platform_credit_note_merchant');


   }



	public function sendstoresmsOp(){
		
		
		$param = array();
		$param['code'] = 'new_order';
		$param['store_id'] = $order_info['store_id'];
		$param['param'] = array(
		        'order_sn' => $order_info['order_sn']
		);
		QueueClient::push('sendStoreMsg', $param);
		
		
	}
	
	public function sendusersmsOp(){
		
		$param = array();
		$param['code'] = 'order_delivery_received';
		$param['member_id'] = 1;
		$param['param'] = array(
		        'order_sn' => 'AUS0100023107211376',
				'id' => 680
		);
		QueueClient::push('sendMemberMsg', $param);
		
		
		
	}



	
	public function goodsOp(){
		
		$store_id = $_GET['store_id'];
		
		$goods_list = model('goods')->getGoodsList(['store_id' => $store_id ]);		
		foreach($goods_list as $v){			
			$ingr = explode(',',$v['goods_ingr']);			
			$data = array();
			foreach($ingr as $vv){				
				$data[]= array(
					'ingr_id' => $vv,
					'goods_code' => $v['goods_code']
				);				
			}			
			model('goods_ingr')->addGoodsIngrAll($data);			
		}		
		
	}



	public function orderOp(){
		
		
		/*$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');				
		$data = $stripe->balance->retrieve();		
		print_r($data);*/

		
		
		$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');	
		
		$stripe->topups->create([
		  'amount' => 2000,
		  'currency' => 'usd',
		  'description' => 'Top-up for Jenny Rosen',
		  'statement_descriptor' => 'Top-up',
		]);
				
		
	}

	
	public function refundOp(){
		
		
		$inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'stripe/stripe.php';
		if(!is_file($inc_file)){
		    output_error('支付接口不存在');
		}		
		require($inc_file);
			
		\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');		
		$re = \Stripe\Refund::create([
		  'payment_intent' => 'ch_3JQa7VB39zEGewFt09mApnqF',
		]);
		
		print_r($re);
		
		
	}








    //订单完成后的操作
   public function billOp(){
   	//print_r($order_id);
   	$pdf_model = model('pdf');
   	$where = array();
   	$where['order_id'] = 3;		
   //	$where['order_bill_state'] = 0;
   	$order_info = model('order')->getOrderInfoN($where);
 
   	$store_wallet = model('store_wallet');
   	
   	//检查有没有1，2,3阶段退款的单子。生成退款记录单
   	$refund_record = model('order_refund')->getRefundList(array('order_id' => $order_info['order_id'],'refund_stage' => 0,'refund_state' => 0));
   	
	
   	$refund_amount = 0;
   	
   	if(!empty($refund_record)){
   		
   		$refund_recode_data = array();
   		$refund_recode_data['order_id'] = $order_info['order_id'];
   		$refund_recode_data['refund_amount'] = $refund_record['refund_amount'];
   		$refund_recode_data['refund_payment_amount'] = $refund_record['refund_payment_amount'];
   		$refund_recode_data['refund_wallet_amount'] = $refund_record['refund_wallet_amount'];
   		$refund_recode_data['voucher_re'] = 'No';			
   		
   		$refund_amount +=  $refund_record['refund_amount'];
   		
   		$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
   		
   	}		
   	
   	
   	//检查有没有3阶段退款的商品 生成退款单
   	$refund_record = model('order_refund')->getRefundList(array('order_id' => $order_info['order_id'],'refund_stage' => 1,'refund_state' => 0));
   	if(!empty($refund_record)){
   		
   		$refund_recode_data = array();
   		$refund_recode_data['order_id'] = $order_info['order_id'];
   		$refund_recode_data['refund_amount'] = $refund_record['refund_amount'];
   		$refund_recode_data['refund_payment_amount'] = $refund_record['refund_payment_amount'];
   		$refund_recode_data['refund_wallet_amount'] = $refund_record['refund_wallet_amount'];
   		$refund_recode_data['voucher_re'] = 'No';			
   	
   		$refund_amount +=  $refund_record['refund_amount'];
   		
   		$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
   		
   	}		
   
   
   
   
   			
   	//Online Sales Income
   	//线上销售收入		
   	$online_sales_amount = $order_info['goods_amount']  - $order_info['store_coupon_price'] + $order_info['extxnfee'] + $order_info['foodbox_amount'] - $refund_amount;
	
	

   	if($online_sales_amount > 0 ){   		
   		//写入商家钱包
   		$data = array();
   		$data['amount'] = $online_sales_amount;
   		$data['order_sn'] = $order_info['order_sn'];
   		$data['order_id'] = $order_info['order_id'];
   		$data['store_id'] = $order_info['store_id'];
   		$data['store_name'] = $order_info['store_name'];
   		$store_wallet->changePd('online_sales_income',$data);
   		
   		//生成票据所需
   		$invoice_data = array();
   		$invoice_data['order_id'] = $order_info['order_id'];
   		$invoice_data['store_id'] = $order_info['store_id'];
   		$invoice_data['user_id'] = $order_info['buyer_id'];		
   		
   	//	print_r($invoice_data);			
   	//	$pdf = $pdf_model->createPdf($invoice_data,'online_sales_invoice_customer');
   		
   	}
   			
   			
   	//Platform Charges
   	//销售抽成&附加费		
   	$platform_charges_amount = $online_sales_amount *(20/100) + $order_info['extxnfee'] ;				
   	if($platform_charges_amount > 0){
   		
   		//写入商家钱包
   		$data = array();
   		$data['amount'] = $platform_charges_amount;
   		$data['order_sn'] = $order_info['order_sn'];
   		$data['order_id'] = $order_info['order_id'];
   		$data['store_id'] = $order_info['store_id'];
   		$data['store_name'] = $order_info['store_name'];
   		$store_wallet->changePd('platform_charges',$data);
   
   
   		//写入ME钱包		
   		$me_wallet = model('me_wallet');
   		$data = array();
   		$data['amount'] = $platform_charges_amount;
   		$data['order_sn'] = $order_info['order_sn'];
   		$data['order_id'] = $order_info['order_id'];
   		$me_wallet->changePd('platform_charges',$data);
   		
   		//生成平台服务集成发票
   	/*	$invoice_data = array();
   		$invoice_data['order_id'] = $order_info['order_id'];
   		$invoice_data['store_id'] = $order_info['store_id'];
   		$invoice_data['me'] = 1;
   		//$invoice_data['user_id'] = $order_info['buyer_id'];			
   		//$invoice_data['delivery_id'] = $order_info['distributor_id'];	
   		//	print_r($invoice_data);				
   		$pdf = $pdf_model->createPdf($invoice_data,'me_platform_invoice_merchant');		
   	*/	
   	}		
   			
   		
   	//写入配送员钱包
   	if($order_info['delivery_fee'] > 0){		
   		$waiter_wallet = model('waiter_wallet');
   		$data = array();
   		$data['amount'] = $order_info['delivery_fee'];
   		$data['order_sn'] = $order_info['order_sn'];
   		$data['order_id'] = $order_info['order_id'];
   		$data['distributor_id'] = $order_info['distributor_id'];
   		$data['distributor_name'] = $order_info['distributor_name'];		
   		$waiter_wallet->changePd('delivery_income',$data);
   		
   		
   		//生成配送票据所需
   		$invoice_data = array();
   		$invoice_data['order_id'] = $order_info['order_id'];
   	//	$invoice_data['store_id'] = $order_info['store_id'];
   		$invoice_data['user_id'] = $order_info['buyer_id'];			
   		$invoice_data['delivery_id'] = $order_info['distributor_id'];	
   		//	print_r($invoice_data);				
   		//$pdf = $pdf_model->createPdf($invoice_data,'delivery_invoice');			
   	
   	}
   	
   	
   	
   }


	//商家操作退款，部分退款或全单退款
	public function store_refund_billOp(){
		
		$pdf_model = model('pdf');
		$where = array();
		$where['order_id'] = 2;		
		//	$where['order_bill_state'] = 0;
		$order_info = model('order')->getOrderInfoN($where);
		
		//检查有没有3阶段退款的商品 生成退款单
		$refund_record = model('order_refund')->getRefundList(array('order_id' => $order_info['order_id'],'refund_stage' => 2,'refund_state' => 0));
		
		if($refund_record){
		
		
		}
		
	
		if(!empty($refund_record)){
			
			$refund_recode_data = array();
			$refund_recode_data['order_id'] = $order_info['order_id'];
			$refund_recode_data['refund_amount'] = $refund_record['refund_amount'];
			$refund_recode_data['refund_payment_amount'] = $refund_record['refund_payment_amount'];
			$refund_recode_data['refund_wallet_amount'] = $refund_record['refund_wallet_amount'];
			$refund_recode_data['voucher_re'] = 'No';		
			$refund_amount +=  $refund_record['refund_amount'];
			//$pdf = $pdf_model->createPdf($refund_recode_data,'sales_credit_note_partial');
			
		}	
		
		
		
		
		
		
		
		
	}

	
}
