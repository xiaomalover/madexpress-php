<?php
/**
 * 我的评价

 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_evaluateControl extends mobileWaiterControl {

    public function __construct(){
        parent::__construct();
    }
    
	//评价页
	
	public function indexOp(){
		
		$model = model('evaluate_waiter');
	
		$count =  $model->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$this->waiter_info['distributor_id']));
		
		$data = array();
		
		$s3 =  $model->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$this->waiter_info['distributor_id'],'waiter_evaluate_score'=> 3));
		
	
		if($s3 == 0 && $count == 0){
		    $rate = '0%';
		}else{
		    $rate = round($s3/$count*100,2)."%";;
		}
		
		$data['favorable_rate'] =  $rate;
		
		$data['ontime_rate'] = '0%';		 
		$data['ontime_no_num'] = 0;  //未按时订单
		$data['ontime_yes_num'] = $count; //已按时订单
		
		
	
		$mn =  $model->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$this->waiter_info['distributor_id'],'waiter_evaluate_content'=> array('eq','')));;
		
		if($mn == 0 && $count == 0){
		    $rate = '0%';
		    
		}else{
		    $rate = round($mn/$count*100,2)."%";;
		}
		
		
		$data['message_rate'] =  $rate; //留言率
		
		$data['message_no_num'] = $mn ;//未留言
		$data['message_yes_num'] = $count; //已留言
		
		
		output_data($data);
		
		
	}
	
	
	
	
	
    /**
     * 评论列表
     */
    public function evaluateListOp() {
      
      
   //     print_r($this->waiter_info);
		$model = model('evaluate_waiter');
		
		$where = array();
		
		$where['waiter_evaluate_waiterid'] = $this->waiter_info['distributor_id'];
		
		if($_POST['type'] > 0 && $_POST['type'] <= 3 ){			
			$where['waiter_evaluate_score'] = $_POST['type'];			
		}
		if($_POST['type'] == 4){			
			$where['waiter_evaluate_content']= array('neq','');
		}
		
	//	print_r($where);
		
        $list = $model->getEvaluateWaiterList($where);
		$score = array(
			0 => '全部',
			1 => '不满意',
			2 => '一般',
			3 => '满意',
			4 => '留言'
		);			
		
		$evalue = array();		
		foreach($list as $k => $v){		
			$evalue[$k]['eva_id'] = $v['waiter_evaluate_id'];
			$evalue[$k]['eva_addtime'] = date('d/m/y H:i',$v['waiter_evaluate_addtime']);
			$evalue[$k]['eva_content'] = $v['waiter_evaluate_content'];
			$evalue[$k]['eva_username'] = $v['waiter_evaluate_username'];
			$evalue[$k]['eva_usercode'] = $v['waiter_evaluate_usercode'];
			$evalue[$k]['eva_isnew'] = $v['waiter_evaluate_user_isnew'];
			$evalue[$k]['eva_userid'] = $v['waiter_evaluate_userid'];
			$evalue[$k]['eva_image'] = $this->evaluateImage($v['waiter_evaluate_image']);
			$evalue[$k]['eva_score'] = $score[$v['waiter_evaluate_score']];
			$evalue[$k]['eva_avatar'] = getMemberAvatarForID($v['waiter_evaluate_userid']);;	
			$evalue[$k]['eva_reply'] = $this->getLogList($v['waiter_evaluate_id']);
			$evalue[$k]['eva_state'] = $v['waiter_evaluate_state'];
			$evalue[$k]['eva_user_nickname'] = $v['waiter_evaluate_nickname'];
			
		}	
		
		$page_count = $model->gettotalpage();  
		
		$count = array();
		
		
	
		$delivery_id = $this->waiter_info['distributor_id'];
		
	//	print_r($model->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id)));
		
		
		$count = array();
		//全部
		$count['all_num'] = model('evaluate_waiter')->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id));
	
		//满意
		$count['satisfied_num'] = model('evaluate_waiter')->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id,'waiter_evaluate_score'=>3));
	
		//一般
		$count['commonly_num'] = model('evaluate_waiter')->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id,'waiter_evaluate_score'=>2));
		
		//不满意
		
		$count['dissatisfied_num'] = model('evaluate_waiter')->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id,'waiter_evaluate_score'=>1));
		
		
		//有留言的评价
		$count['mess_num'] = model('evaluate_waiter')->getEvaluateWaiterCount(array('waiter_evaluate_waiterid'=>$delivery_id,'waiter_evaluate_content'=>array('neq','')));
		$count['type'] = $score;
		
		
        output_data(array('evalue' => $evalue,'count'=>$count),mobile_page($page_count));
    }
	
	
	public function infoOp(){
	    
	    
	    
			$score = array(
				1 =>'不喜欢',
				2 => '一般',
				3 => '喜欢'
			);

			$model = model('evaluate_waiter');		
			$where = array(
				'waiter_evaluate_id' => $_POST['id'],
				'waiter_evaluate_waiterid' => $this->waiter_info['distributor_id']
			);		
			$info = $model->getEvaluateWaiterInfo($where);		

		
			$evalue['eva_id'] = $info['waiter_evaluate_id'];
			$evalue['eva_addtime'] = date('d/m/y',$info['waiter_evaluate_addtime']);
			$evalue['eva_content'] = $info['waiter_evaluate_content'];
			$evalue['eva_username'] = $info['waiter_evaluate_username'];
			$evalue['eva_isnew'] = $info['waiter_evaluate_user_isnew'];
			$evalue['eva_userid'] = $info['waiter_evaluate_userid'];
		    $evalue['eva_usercode'] = $info['waiter_evaluate_usercode'];
			$evalue['eva_score'] = $score[$info['waiter_evaluate_score']];
			$evalue['eva_avatar'] = getMemberAvatarForID($info['waiter_evaluate_userid']);
				
			$evalue['eva_image'] = $this->evaluateImage($info['waiter_evaluate_image']);
				
		//	$evalue['eva_reply_time'] = date('d/m/y',$info['waiter_evaluate_reply_time']);
			$evalue['eva_reply'] = $this->getLogList($info['waiter_evaluate_id']);
				
			$evalue['quick_reply'] = $this->quick_reply();
				
			output_data($evalue);
	    
	}
	
	private function quick_reply(){
	    
	    $data = array(
	        array(
	            'text'=>'感谢您的支持。'    
	        ),
	        array(
	            'text'=>'亲爱的顾客，十分抱歉本次服务没有让您获得最佳体验，我们会不断改善服务，期待您再次惠顾！'    
	        ),
	        array(
	            'text'=>'感谢您的认可，我们会继续努力，祝您生活愉快！'    
	        ),
	   );
	   return $data;
	    
	}
	
	
	
	private function getLogList($eva_id){
	    
	    $where = array();
	    $where['eva_id'] = $eva_id;
	    $list = model('evaluate_waiter')->getLogList($where);
	    $data = array();
	    foreach($list as $k=> $v){
	        $v['log_file'] = $this->evaluateImage($v['log_file']);
			$v['log_avatar'] = getMemberAvatarForID($v['reply_user_id']);
	        $v['log_addtime'] = date('d/m/y H:i',$v['log_addtime']);
	        $data[$k] = $v;
	    }
	    
	    return $data;
	    
	}
	
	//评价回复
	public function replyOp(){
	    
	    $data = array(
                'eva_id'	 	=> $_POST['id'],
                'log_content' 	=> $_POST['content'],
                'log_file' 		=> $_POST['image'],
                'log_addtime'	=> time(),
                'reply_user_id' => $this->waiter_info['distributor_id'],
                'reply_user_name' => $this->waiter_info['distributor_name']
        );
        
        
        
        
        $row = model('evaluate_waiter')->addLog($data);
        
        if ($row) {
            
            $model = model('evaluate_waiter');	
            
			$where = array(
				'waiter_evaluate_id' => $_POST['id'],
				'waiter_evaluate_waiterid' => $this->waiter_info['distributor_id']
			);		
			
			$info = $model->getEvaluateWaiterInfo($where);	
			
            if($info['waiter_evaluate_state'] ==0 ) {
                $data = array();
                $data['waiter_evaluate_explain'] = $_POST['content'];
                $data['waiter_evaluate_state'] = 1;
                $model->editEvaluateWaiter($where,$data);
            }
            
            output_data('保存成功');
        } else {
            output_error('保存失败');
        }
	    
	}
	
	
	
	public function evaluateQuickReplyOp(){
	    
	    	$quick = array(
			0 => '感谢您的支持！',
			1 => '亲爱的顾客，十分抱歉本次服务没有让您获得最佳体验，我们会不断改善服务，期待您再次惠顾！',
			2 => '感谢您的认可，我们会继续努力，祝您生活愉快！'
			
		);
	
	
		output_data(array('list'=>$quick));
		
	    
	}
	
	
	
	
		//格式化评价图片
	private function evaluateImage($image){
		
		if(!empty($image)){
			
			$image =   explode(",", $image);	
			
			$images = array();
			foreach($image as $v){
				$images[] = $v;
				
			}
			
			return $images;
		}else{
			
			return array();
			
			
		}
		
		
		
	}
	
	
	
	
    
}
