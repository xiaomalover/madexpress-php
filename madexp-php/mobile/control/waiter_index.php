<?php
/**
 * 我的商城
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_indexControl extends mobileWaiterControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 骑手个人信息
     */
    public function indexOp() {
      
	
		
		$waiter_info = array();
        $waiter_info['nickname'] = $this->waiter_info['distributor_name'];
		$waiter_info['truename'] = $this->waiter_info['distributor_truename'];
		
        $waiter_info['avatar'] = getWaiterAvatar($this->waiter_info['distributor_avatar']);
		$waiter_info['distributor_code'] = $this->waiter_info['distributor_code'];
        $waiter_info['distributor_mobile'] = $this->waiter_info['distributor_mobile'];
        $waiter_info['distributor_login_name'] = $this->waiter_info['login_name'];
		
		$waiter_info['online_state'] = $this->waiter_info['distributor_online'];
		$waiter_info['distributor_state'] = $this->waiter_info['distributor_status'];
		
		$waiter_info['delivery_goback_money'] = $this->waiter_info['delivery_return_money'];
		
		$waiter_info['scheduling_id'] = $this->waiter_info['scheduling_id'];
		
		
		$waiter_info['is_scheduling'] = $this->waiter_info['scheduling_is'];
		$waiter_info['distributor_email'] = $this->waiter_info['distributor_email'];
	
		if($this->waiter_info['distributor_online'] == 30){
		   	$where = array(
				'scheduling_id' => $this->waiter_info['scheduling_id']
			);
			$sch_info = model('scheduling')->getSchedulingInfo($where);		
		    if($sch_info['rest_id'] > 0 ){
		      	$rest = model('scheduling_log')->getSchedulingLogInfo(array('log_id'=>$sch_info['rest_id']));
		      	$rest_time = $rest['log_start_time'] + 900 -  time();
		      	if($rest_time <=0){
		      	    $rest_time = 0;
		      	}
		    	$waiter_info['scheduling_rest_time']   =  $rest_time;
		    }
		}
		
		
		
		$waiter_info['is_lock'] = $this->waiter_info['is_lock'];
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'file_type' => array('in','1,2,3'),
			'file_state' => 1
		);
		
		
		$waiter_file = model('waiter_file')->getWaiterFileList($where);
		$file =array();
		foreach($waiter_file as $v){
		    
			$file[$v['file_type']]['file_name'] = $v['file_name'];
			$file[$v['file_type']]['file_end_time'] = date('m/d',$v['file_end_time']);
			$file[$v['file_type']]['file_code']= $v['file_code'];
			
		}
				
		//护照
		$waiter_info['passport'] = $file['1'];
		//签证
		$waiter_info['visa'] = $file['2'];		
		//驾照
		$waiter_info['driver'] = $file['3'];		
		
		if($this->waiter_info['is_vehicle'] == 1){
		    
		    $waiter_info['vehicle'] = array(
		            'number' => $this->waiter_info['vehicle_number'],
		            'type' => $this->waiter_info['vehicle_type'],
		            'brand' => $this->waiter_info['vehicle_brand'],
		        )    ;
		}else{
		    $waiter_info['vehicle'] = '暂无';    
		}
		//车辆信息
		
		
	    
	   $waiter_info['delivery_state'] = $this->waiter_info['delivery_state'];
	    
      
		$waiter_info['today_order'] = $this->stat_order(); //今日订单
		$waiter_info['today_income'] = $this->stat_order_income(); //今日收入
		$waiter_info['today_online'] = 0; //今日在线时长 分钟
		$waiter_info['vehicle_num']  = 0;	//车辆信息气泡
	
	
		$waiter_info['evaluate_num']    = $this->eva_num(); //今日收到的评价气泡		
		$waiter_info['scheduling_num']  = $this->sch_num();	//车辆信息气泡
		
		
		
		
		
		
        output_data(array('waiter_info' => $waiter_info));
    }
    
    
    
    //统计今日订单
    
    private function stat_order(){
        $where = array();
       // $where['order_date'] = date('Y-m-d',time());
        $where['order_state'] = array('in','60');
        $where['distributor_id'] = $this->waiter_info['distributor_id'];
        
        $start_unixtime =  strtotime(date('Y-m-d',time()));
        $end_unixtime = $start_unixtime +86400 ;
     
	    if ($start_unixtime || $end_unixtime) {
            $where['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
        
        
        $count = model('order')->getOrderCount($where);
        return $count;
        
        
    }
    
    //统计今日收入
    private function stat_order_income(){
        $where = array();
        $where['order_state'] = array('in','60');
        $where['distributor_id'] = $this->waiter_info['distributor_id'];
        $start_unixtime =  strtotime(date('Y-m-d',time()));
        $end_unixtime = $start_unixtime +86400 ;
	    if ($start_unixtime || $end_unixtime) {
            $where['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
        $income = model('order')->getOrderInfoN($where,'SUM(delivery_fee) as total');
        return $income['total'];
        
    }
    
    //统计今日在线时长
    
    private function stat_online_time(){
        
        
        
        
        
        
    }
    
    
    
    
	
	//待回复评价数量
	
    private function eva_num(){
        
        $where = array();
        $where['waiter_evaluate_waiterid'] = $this->waiter_info['distributor_id'];
        
        $num = model('evaluate_waiter')->getEvaluateWaiterCount($where);
        
        return $num;
        
    }
    
    //进行中的排班数量
    private function sch_num(){
        
        $where = array();
      	$where['scheduling_status'] = array('in','10,20');
        $where['distributor_id'] = $this->waiter_info['distributor_id'];
        $num = Model('scheduling')->countScheduling($where);	
        
        
        return $num ;
    }
	

    //绑定设备号
    public function bind_tokenOp(){
        
         $where = array(
            'distributor_id' => $this->waiter_info['distributor_id']    
        );
        $update = array();
        $update['ios_device_token'] = $_POST["ios_device_token"];
        $update['android_device_token'] = $_POST["android_device_token"];
    	$update = Model('waiter')->editWaiter($where,$update);
		if($update){
			output_data('绑定成功');					
		}else{
			output_error('绑定失败');	
		}		
	    
        
        
    }


	
	//切换语言
	public function  system_langOp(){
        $where = array(
            'distributor_id' => $this->waiter_info['distributor_id']    
        );
        $update = array();
        $update['system_lang'] = $_POST["lang"];
    	$update = Model('waiter')->editWaiter($where,$update);
		if($update){
			output_data('切换成功');					
		}else{
			output_error('切换失败');	
		}		
	    
	    
	}
	
	
	//绑定stocket
	public function bindOp(){
		
		$stoket = model('stoket');
		$wid = $this->waiter_info['distributor_id'];		
		$cid = $_POST['cid'];		
		if(!$cid){
		    output_error('cid不可为空');
		}
		
		$stoket->bind($cid,$wid);
		$where = array(
			'client_id' => $cid
		);
		$data = array(
			'distributor_id' => $wid
		);
		$row = $stoket->editStoket($where,$data);
		$data = array(
						'distributor_online'    => 20,
						'stocket_last_time'     => 0,
						'stocket_error'         => 0
		);		
		$where = array('distributor_id'=>$this->waiter_info['distributor_id']);				
		$row = Model('waiter')->editWaiter($where,$data);
		
		if($row){
		    output_data('绑定成功');
		}else{
		    output_error('绑定失败');
		}
	}
	
	
	//编辑头像
	public function waiterAvatarOp(){
		
			$upload = new UploadFile();
            $upload->set('default_dir',ATTACH_WAITER_AVATAR);           
			//上传图片
            if (!empty($_FILES['file']['tmp_name'])){   			
                $result = $upload->upfile('file');
                if ($result){ 
									
					$data = array(
						'distributor_avatar' => $upload->file_name
					);		
					$where =array('distributor_id'=>$this->waiter_info['distributor_id']);				
					Model('waiter')->editWaiter($where,$data);
					
										                   
					output_data(array('url'=>  UPLOAD_SITE_URL.'/'.ATTACH_WAITER_AVATAR.'/'.$upload->file_name,'file_name' =>$upload->file_name ));
                }else {
                    output_error($upload->error);
                }	
            }else{
				output_error('上传失败');
			}

	}
	
	
	
	//编辑昵称
	public function saveNickNameOp(){
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
	
		$data = array(
			'distributor_name' => $_POST['distributor_name']
		);		
		$update = $model_waiter->editWaiter($where,$data);
	
		if($update){
			output_data('编辑成功');					
		}else{
			output_error('保存失败');	
		}
		
		
	}
		
	
	//编辑手机号
	public function saveMobileOp(){
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
		$data = array(
			'distributor_mobile' => $_POST['distributor_mobile']
		);		
		$update = $model_waiter->editWaiter($where,$data);
		if($update){
			output_data('1');					
		}else{
			output_error('保存失败');	
		}		
	}
	
	
	
	//编辑手机号
	public function saveEmailOp(){
		
		
		
		$pwd = $_POST['password'];
		if(md5($pwd) != $this->waiter_info['login_password']){			
			output_error('密码验证失败');		
		}
		
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
		$data = array(
			'distributor_email' => $_POST['distributor_email']
		);		
		$update = $model_waiter->editWaiter($where,$data);
		if($update){
			output_data('1');					
		}else{
			output_error('保存失败');	
		}		
	}
	
	//编辑密码
	public function savePassOp(){
		
		
		
		$old_pwd = $_POST['old_password'];
		if(md5($old_pwd) != $this->waiter_info['login_password']){			
			output_error('旧密码验证失败');		
		}
		
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
		$data = array(
			'login_password' => md5($_POST['password'])
		);		
		$update = $model_waiter->editWaiter($where,$data);
		if($update){
			output_data('密码修改成功');					
		}else{
			output_error('密码修改失败');	
		}		
	}
	
	
	
	
	
	
	
	
    /**
     * 我的信息
     */
    public function myAssetOp() {    
        $waiter_info = array();      
		$waiter_info['today_order'] = 0; //今日订单
		$waiter_info['today_income'] = 0.00; //今日收入
		$waiter_info['today_online'] = 0; //今日在线时长 分钟		
		$waiter_info['vehicle_num']  = 0;	//车辆信息气泡
		$waiter_info['evaluate_num'] = 0; //今日收到的评价气泡	
        output_data($waiter_info);
    }
	
	
	
	/*调试接口
	
	public function waiterStateTestOp(){
		
		$online_state = $_POST['state'];
		switch($online_state){		
			case 1: //下班
				
				output_data(array('state'=> 1,'msg'=>'下班成功'));	
				
			break;
			case 2: //上班
				output_error(array('state'=> 2,'msg'=>'未在城市内'));	
				
			break;	
			case 3: //上班
				
				output_error(array('state'=> 3,'msg'=>'没有车辆'));	
				
			break;				
			case 4: //上班
				
				output_error(array('state'=> 4,'msg'=>'未到达指定区域'));					
		
				
			break;	
				
			case 5: //上班				
				output_data(array('state'=> 5,'msg'=>'临时休息'));			
				
				
			break;	
				
			case 6: //上班				
				output_data(array('state'=> 6,'msg'=>'休息结束'));			
				
				
			break;	
				
				
		}
									   
	}
	
	*/
	
	
	//更改状态
	private function waiterOnline($state,$state_code){
			   
		
				$where = array(
					'distributor_id' => $this->waiter_info['distributor_id']
				);
				$data = array(
					'distributor_online' => $state
				);
				
				$row = $model->editWaiter($where,$data);
				
				if($row){					
					output_data(array('state_code'=>$state_code));
				}else{
					output_error('操作失败');
				}	
		
		
	}
	
	/*骑手状态*/
	
	public function waiterStateOp(){
					
		$online_state = $_POST['state'];
		
		switch($online_state){	
		
			case 'offwork': //下班
						
			
				if($this->waiter_info['scheduling_id'] > 0){
						
						
						
						//检查是否有未送完的订单						
						 if($this->checkWaiterOrder() >0){			
						     
						    //预约下班代码
						    if($this->waiter_info['appointment_offline'] ==  0 ){
						        
						         model('waiter')->editWaiter(array('distributor_id' => $this->waiter_info['distributor_id']),array('appointment_offline' => 1));
						         output_data(array('state_code'=> 11 ,'msg'=>'您已预约下线，配送完成后将自动下线')); 
						         
						    }else{
						        
						         output_data(array('state_code'=> 11 ,'msg'=>'您已预约下班')); 
						         
						    }
						 }						
						
						//修改排班的下班状态
						$where = array(					
							'scheduling_id' => $this->waiter_info['scheduling_id']					
						);		
						$info = model('scheduling')->getSchedulingInfo($where);
												
						//检查是否可以执行下班。
						/*if(time() <= $info['scheduling_end_time']){	
						    
								output_error(array('state_code'=> 11 ,'msg'=>'未到下班时间，无法进行下班')); 

						}
						*/
						
						
						$time = time();
						$updata = array(
							'scheduling_signout_time' => $time,
							'total_time' => ($time - $info['scheduling_sign_time']),							
							'scheduling_signout_coordinate' => $_POST['points'],					
							'scheduling_status' => 30
						);	
					
										
						model('scheduling')->editScheduling($updata,$where);	
										
						//修改当前骑手的状态					
						$where = array(
							'distributor_id' => $this->waiter_info['distributor_id']
						);
					
						$updata = array(
							'distributor_online' 	=> 10,
							'scheduling_id' 		=> 0,
							'delivery_state' 		=> 0
						);
					
						model('waiter')->editWaiter($where,$updata);
						$this->workHours(10);	
						output_data(array('state_code'=> 1 ,'msg'=>'已下班'));
					
					
				}else{					
								
								
						
						//检查是否有未送完的订单						
						 if($this->checkWaiterOrder() >0){	
						     //预约下班代码
						    if($this->waiter_info['appointment_offline'] ==  0 ){
						        
						         model('waiter')->editWaiter(array('distributor_id' => $this->waiter_info['distributor_id']),array('appointment_offline' => 1));
						        
						         output_data(array('state_code'=> 11 ,'msg'=>'您已成功预约下线，配送完成后将自动下线')); 
						         
						    }else{
						        					        
						         output_data(array('state_code'=> 11 ,'msg'=>'您已预约下线')); 

						    }
						 }		
										
						//修改当前骑手的状态					
						$where = array(
							'distributor_id' => $this->waiter_info['distributor_id']
						);
						$updata = array(
							'distributor_online' => 10,
							'scheduling_id' => 0,
							'delivery_state' 		=> 0
						);					
						model('waiter')->editWaiter($where,$updata);
					
						$this->workHours(10);	
						output_data(array('state_code'=> 1 ,'msg'=>'已下班'));					
					
				}
								
				
				
			break;				
			case 'onwork' : 
				
			
			    $latlng = explode(',',$this->waiter_info['region_coordinate']);
				$money = $this->polygon_money(array('lng'=>$latlng[0],'lat'=>$latlng[1]));
			    if($money == 0){
			        	output_error(array('state_code'=> 4 ,'msg'=>'未抵达上班区域'));		
			    }
			
			
				//请勿重复上班上班				
				
				if($this->waiter_info['distributor_online'] == 20){					
					output_error(array('state_code'=> 10,'msg'=>'请勿重复请求上班状态'));	
				}
				
				//查询是否有自己的车辆
				
			/*	if($this->waiter_info['is_vehicle'] == 0){ //没有自己的车辆就查询租车情况					
					
					$where = array(
						'lease_state' => 30,
						'lease_content' => array('like','%'.$this->waiter_info['distributor_code'].'%')
					);
					
					$lease_count = model('vehicle_lease')->getLeaseCount($where);
					
					if($lease_count <= 0){
						output_error(array('state_code'=> 3,'msg'=>'您还没有车辆哦'));						
					}
					
				}
            */
									
				//获取当前正在执行的排班状态，并上班
				
				$data = $this->scheduling();	
				
				if(!empty($data)){
					
				//	output_data($_POST);
					//查询是否已抵达上班地址
					$points = $_POST['points']; //获取当前骑手坐标
				
					if(!$points){						
						output_error(array('state_code'=> 9,'msg'=>'获取骑手坐标失败'));			
					}
					
					
					if($this->in_fences($data['region_id'],$points) == 0){						
						output_error(array('state_code'=> 4 ,'msg'=>'未抵达上班区域'));							
					}
					
					
					//检查相同租赁任务内。是否存在已上班的合租人					
					//if(!$this->checkWaiterLease(130)){						
					//	output_error(array('state_code'=> 4 ,'msg'=>'同一租赁计划内，同一时间两人无法同时上线'));			
					//}
					
					//修改骑手表上班状态
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 20,
						'scheduling_id' => $data['id'],
						'region_id' => $data['region_id'],
						'region_coordinate' => $points						
					);
										
					$row = model('waiter')->editWaiter($where,$updata);
									
					//修改排班的上班状态
					$where = array(					
						'scheduling_id' => $data['id']						
					);		
					
					$updata = array(
						'scheduling_sign_time' => time(),
						'scheduling_sign_coordinate' => $points,
						//'scheduling_sign_address' => '',
						'scheduling_status' => 20
					);					
					$row = model('scheduling')->editScheduling($updata,$where);	
					
					
					if($row){
						
						output_data(array('state_code'=> 5 , 'msg'=>'上班成功'));
						
					}else{
						
						output_error(array('state_code'=> 10 , 'msg'=>'上班失败'));
					}
					
					
				}else{ //执行普通上班
					
					//获取坐标点。计算所在区域
					
					$points = $_POST['points']; //获取当前骑手坐标										
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 20,				
						//'region' => $data['region_id'],
						'region_coordinate' => $points
					
					);

					$row = model('waiter')->editWaiter($where,$updata);
					
					if($row){	
						
						$this->workHours(20);
						output_data(array('state_code'=>5 , 'msg'=>'上班成功'));
						
					}
					
				}
				
				
				
			break;
			case 'onrest':  
				
					
			//开始休息	
				
				if($this->waiter_info['distributor_online'] != 20){
					output_error(array('state_code'=> 10,'msg'=>'请求状态错误'));
				}
				
				//本次排班休息次数。
				
				
				
				
				
				//高峰期休息提示
				
				
				
				//检查是否有未送完的订单
				if($this->checkWaiterOrder() >0){	
					output_error(array('state_code'=> 10,'msg'=>'您有正在未完成的订单，无法临时休息'));	    
				}
				
				
				
				
				
				
				
				
				
				//$this->workHours(30);				
				
				//如果有排班的状态可以点击休息。
				if($this->waiter_info['scheduling_id'] > 0){					
					
					$where = array(
						'scheduling_id' => $this->waiter_info['scheduling_id']
					);
					
					$row = model('scheduling')->getSchedulingInfo($where);					
					
					if($row['rest_id'] == 0){
						//写入临时休息表
						$data =array(
							'scheduling_id' => $row['scheduling_id'],
							'log_start_time' => time(),
							'addtime' => time(),
							'distributor_id' => $this->waiter_info['distributor_id']
						);
						
						$log_id = model('scheduling_log')->addSchedulingLog($data);
						//更新当前这个
						model('scheduling')->editScheduling(array('rest_id'=>$log_id),$where);
					
					
					
						//更新骑手状态
						model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>30,'rest_id' => $log_id));
						
						output_data(array('state_code'=> 6,'msg'=>'开始休息'));
						
					}else{
					    
					    output_data(array('state_code'=>10,'msg'=>'请勿重复请求临时休息'));
					    
					}
					
				}else{
					
					//非排班休息
					
					$this->workHours(30);
					model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>30));	
					output_data(array('state_code'=> 6,'msg'=>'开始休息'));
						
				}
				
				
				//休息
				
			break;	
			case 'offrest': //休息结束
			
				
				if($this->waiter_info['distributor_online'] != 30){
					output_error(array('state_code'=> 10,'msg'=>'请求状态错误'));
				}
				
				
				
				if($this->waiter_info['scheduling_id'] > 0){					
					
					$where = array(
						'scheduling_id' => $this->waiter_info['scheduling_id']
					);
					
					$row = model('scheduling')->getSchedulingInfo($where);					
					
					if($row['rest_id'] > 0){
					
						$rest = model('scheduling_log')->getSchedulingLogInfo(array('log_id'=>$row['rest_id']));
						//修改临时休息表
						$time = time();
						$data = array(
							'log_end_time' => $time,
							'total_time' => $time - $rest['log_start_time']
						);
						
						
						model('scheduling_log')->editSchedulingLog(array('log_id'=>$row['rest_id']),$data);
						
						//更新当前这个
						model('scheduling')->editScheduling(array('rest_id'=> 0 ),$where);
						
						//更新骑手状态
						model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>20,'rest_id' => 0));
						
						output_data(array('state_code'=> 7,'msg'=>'休息结束'));
						
					}else{
					    output_data(array('state_code'=> 10,'msg'=>'请勿重复请求休息结束'));
					    
					}
					
				}else{
					
					//非排班休息
										
					$this->workHours(40);
					model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>20));					
					output_data(array('state_code'=> 7,'msg'=>'休息结束'));
					
					
				}
								
				
			break;		
		}
				
		
	}
	
	
	
	
    //计算是否在围栏
	private function polygon_money($points){
		
				
		$polygon_model = model('polygon');		
	//	$polygon_list  = model('region_sector')->getRegionList(array('region_money_id' => $region_id));
		$region = model('region')->getRegionList(array('region_type'=> 2,'is_money_show' => 1));
		foreach($region as $k =>  $v){		 	
		   $polygon_points = $polygon_model->points_array($v['region_money_coordinate']);		   
		   $row = 	$polygon_model->is_point_in_polygon($points,$polygon_points);			   
			if($row === true){					
				return  1;
				break;
			}			
		}		
		
	}
	
	
	
	private function checkWaiterOrder(){
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'order_state' => array('in','30,40,50')
		);	
		
		$count = model('order')->getOrderCount($where);
		
		return $count;
	}
	
	//检查是否有排班
	private function scheduling(){		
		
		$day = strtotime(date('Y-m-d',time()));
		$time = time();			
		$where = array(
			'scheduling_date' => $day
		);	
		$data = model('scheduling')->getSchedulingList($where);				
		$result = array();
		foreach($data as $v){			
			if($v['scheduling_start_time'] < $time && $v['scheduling_end_time'] > $time && $v['scheduling_status'] == 10 ){			
				$result['id'] = $v['scheduling_id'];
				$result['region_id'] = $v['region_id'];
			}			
		}
	
		return $result;		
		
	}
	
	
	//验证是否存在合租人一上班
	private function checkWaiterLease($lease_id){
				
		$where = array(
			'lease_id' => $lease_id
		);
		$lease = model('vehicle_lease')->getLeaseInfo($where);
		$ids = $lease['distributor_id'].','; //主要租车人
		$leasecotenant = model('vehicle_lease_cotenant')->getLeaseCotenantList($where);
		foreach($leasecotenant as $v){
			$ids .= $v['distributor_id'].',';
		}
		
		$ids = substr($ids,0,-1);
		
		$where = array(
			'distributor_id' => array('in',$ids),
			'distributor_online' => array('in','20,30')
		);
		$count  =model('waiter')->getWaiterCount($where);
		
		if($count > 0){
			return false;
		}else{
			return true;
		}
		
		
		
		
		
		
		
		
		
	}
	
	
	//更新排班休息记录
	private function restlog($scheduling_id){
		
		$model = model('scheduling_log');
		
		$info = '';
			
		
	}
	
	//更新上下班记录,统计,工时等
	private function workHours($state){
		
		$model = model('waiter_workhours');		
		
		if($state == 10){			
			$data = array();			
			$data['work_end_time'] = time();
			$where = array(
				'work_id' => $this->waiter_info['work_id']				
			);
			$row = $model->editWorkhours($where,$data);
			if($row){				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				$data = array(
					'distributor_online' => 10,
					'work_id' => 0
				);
				model('waiter')->editWaiter($where,$data);		
			}
		}
			
		
		if($state == 20){
			
			$data = array();
			$data['distributor_id'] = $this->waiter_info['distributor_id'];
			$data['work_state_time'] = time();
			$data['addtime'] = time();
			$data['work_days'] = date('Y-m-d',time());
			
			$row = $model->addWorkhours($data);
			if($row){				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				$data = array(
					//'distributor_online' => 20,
					'work_id' => $row
				);
				model('waiter')->editWaiter($where,$data);				
			}
			
		}
		
		//更新普通休息记录，非排班休息记录		
		if($state == 30){
			
			$data = array();
			$data['distributor_id'] = $this->waiter_info['distributor_id'];			
			$data['rest_state_time'] = time();			
			$data['addtime'] = time();
			$data['rest_days'] = date('Y-m-d',time());
			$row = model('waiter_rest')->addRest($data);			
			
			if($row){				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				$data = array(
					//'distributor_online' => 20,
					'rest_id' => $row
				);
				model('waiter')->editWaiter($where,$data);				
			}
			
			
			
		}
		
		//结束休息		
		if($state == 40){
			$data = array();
			$data['rest_end_time'] = time();	
			
			$where = array(
				'rest_id' => $this->waiter_info['rest_id']				
			);
		
			$row = model('waiter_rest')->editRest($where,$data);			
			
			if($row){	
				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				
				$data = array(
					//'distributor_online' => 20,
					'rest_id' => 0
				);
				
				model('waiter')->editWaiter($where,$data);				
			}
		}
			
				
	}
	
	
	/*
	
	实时更新位置
	
	*/
	
	/*
	public function waiter_pointOp(){

	
		$lat = $_POST['latitude'];
		$lng = $_POST['longitude'];	
		$data = array();
		$data['region_coordinate'] = $lng.','.$lat;
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id']
		);	
		
		$money = $this->DeliverMoneyDistance($data['region_coordinate']);
	
	
		if($money == 1){
			$data['delivery_return_money'] = 1;
		}else{
			$data['delivery_return_money'] = 0;
		}
		
		
		
    	$row = model('waiter')->editWaiter($where,$data);
		if($row ){
			output_data(array('message'=> '上报成功'));			
		}else{
			output_error('更新失败');
		}
					
	}
	*/
	
	
	//坐标实时上报
	public function waiter_pointOp(){
	
	/*	$lat = $_POST['latitude'];
		$lng = $_POST['longitude'];	
		$data = array();
		$data['region_coordinate'] = $lng.','.$lat;
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id']
		);			
		
		$row = model('waiter')->editWaiter($where,$data);
	*/	
		output_data('成功');
		/*
		//是否在围栏里
		$money = $this->DeliverMoneyDistance($data['region_coordinate']);		
		if($money == 1){			
			$data['delivery_is_enclosure'] = 1;
		}else{
			$data['delivery_is_enclosure'] = 0;		
		}
		
		//当前骑手状态
		if($this->waiter_info['distributor_online'] == 10){ //不在线的时候导航过来的。			
		
			if($this->waiter_info['is_nav'] == 1 && $money == 1){ //导航中且已到了围栏里，执行上线
				$data['distributor_online'] = 20; //修改状态为上线状态
			}
			
		}elseif($this->waiter_info['distributor_online'] == 20){										
			
			$where = array(
				'distributor_id' => $this->waiter_info['distributor_id'],
				'order_state' => array('in','30,40,50')
			);			
			$list = model('order')->getOrderListNew($where);										
			//未在钱圈围栏,且手里没有订单，也不是刚送完返回中，那还不给你踢下线
			if($this->waiter_info['delivery_is_enclosure'] == 0 && $this->waiter_info['delivery_return_money'] == 0 && $this->waiter_info['delivery_state'] == 0 && count($list) == 0){				
				$data['distributor_online'] = 10;
				$state = 404; //被踢下线了
			}			
		}
		
		$row = model('waiter')->editWaiter($where,$data);
		
		if($row){		    
		    
		    //有排班的另算
			if($state == 404 && $this->waiter_info['scheduling_id'] > 0){	
				
				$data = array();				
				$update = array();
				$update['value'] = 2;							
				$update['content'] =  '没有订单情况下，处了围栏被下线，排班失败';
				
				$where = array();
				$where['sch_id'] = $this->waiter_info['scheduling_id'];
				$where['code'] = 'task_offline';							
				
				model('scheduling_task_log')->editTaskLog($update,$where);
			
			}			
			output_data(array('message'=> '上报成功'));			
		}else{
			output_error('更新失败');
		}*/
					
	}
	
	
	
	/**
	 * @name 围栏算法，判断一个坐标，是否在围栏里面.如：['113.664673,34.810146','113.681667,34.796896','113.69231,34.794711','113.702009,34.809159']
	 * @author macnie <mac@lenmy.com>
	 * @param array $fences 围栏，是一组坐标数组 如：113.674458,34.804719
	 * @param string $point
	 * @return bool
	 */
	
	private function in_fences($region_id,$point) {
	
		$region = model('region')->getRegionInfo(array('region_id'=> $region_id));		
		$fences = explode('|',$region['region_coordinate']);
		
		$point = $point;		
		
		$nvert = count($fences);
	
		$vertx = [];
		$verty = [];
		list($testy, $testx) = explode(',', $point);
		foreach ($fences as $r) {
			list($lng, $lat) = explode(',', $r);
			$vertx[] = $lat;
			$verty[] = $lng;
		}
		$i = $j = $c = 0;
		for ($i = 0, $j = $nvert - 1; $i < $nvert; $j = $i++) {
			if (( ($verty[$i] > $testy) != ($verty[$j] > $testy) ) &&
				($testx < ($vertx[$j] - $vertx[$i]) * ($testy - $verty[$i]) / ($verty[$j] - $verty[$i]) + $vertx[$i]))
				$c = !$c;
		}
		
		return $c;
	}
	
	
	//返回钱的坐标接口。
	public function homeMapOp(){
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'order_state' => array('in','30,40,50')
		);
		
		$list = model('order')->getOrderListNew($where);
	
		$data = array();
		$i = 0;
		foreach($list as $k => $v){
			$store = $this->getStore($v['store_id']);
		//	$data[$k]  = $v;
		//    $data[$k] = $v;
		
    		$buyer_info = array();
    		$buyer_info['buyer_id'] = $v['buyer_id'];
    		$buyer_info['buyer_code'] =$v['buyer_code'];
    		$buyer_info['buyer_name'] = $v['buyer_name'];
    		$buyer_info['buyer_phone'] = $v['buyer_phone'];
    		$buyer_info['buyer_address'] = $v['buyer_address'];
    		$buyer_info['buyer_region_id'] = $v['buyer_region_id'];
    		$buyer_info['buyer_is_new'] = $v['buyer_is_new'];
    		$buyer_info['buyer_coordinate'] = $v['buyer_coordinate'];
    		$buyer_info['buyer_region'] = $v['buyer_region'];		
    		$buyer_info['buyer_region_color'] = $v['buyer_region_color'];
    		$buyer_info['buyer_status_code'] = $v['buyer_status_code'];
    		$buyer_info['buyer_address_id'] = $v['buyer_address_id'];
    	
		 
		    $data[$k]['buyer'] = $buyer_info;
		 
	    
	      	$store_info = array();
    		$store_info['store_id'] = $store['store_id'];
    		$store_info['store_name_primary'] = $store['store_name_primary'];
    		$store_info['store_name_secondary'] = $store['store_name_secondary'];
    		$store_info['store_code'] = $store['store_code'];
    		$store_info['store_address'] = $store['store_address'];
    		$store_info['store_lat'] = $store['store_lat'];
    		$store_info['store_lng'] = $store['store_lng'];
    		$store_info['region_name'] = $store['region_name'];
    		$store_info['region_color'] = $store['region_color'];
    		$store_info['store_phone'] = $store['store_phone'];
    		$store_info['store_status_code'] = $v['store_status_code'];
    		 
		    
		    $data[$k]['store'] =$store_info;
		
		    
		    $order = array();
		    $order['order_sn'] = $v['order_sn'];
    		$order['delivery_time'] = $v['delivery_time'];
    		$order['delivery_type'] = $v['delivery_type'];
    		$order['order_state'] = $v['order_state'];
    		$order['delivery_fee'] = $v['delivery_fee'];
    		$order['delivery_comment'] = $v['deliver_comment'];
    	    $order['delivery_state'] = $this->waiter_info['delivery_state'];
		    $order['order_id'] = $v['order_id'];
		    $data[$k]['order'] = $order;
		
		
		
		
		    $delivery = array();
		    $delivery['distributor_id'] = $v['distributor_id'];
            $delivery['distributor_name'] = $v['distributor_name'];
            $delivery['distributor_mobile'] = $v['distributor_mobile'];
            $delivery['distributor_start_time'] = $v['distributor_start_time'];
            $delivery['distributor_end_time'] = $v['distributor_end_time'];
            $delivery['order_estimate_time'] = $v['order_estimate_time'];
            $delivery['distributor_code'] = $v['distributor_code'];
            $delivery['distributor_duration'] = $v['distributor_duration'];
            $delivery['distributor_coordinate'] = $v['distributor_coordinate'];
		    $data[$k]['delivery'] = $delivery;
		
		    
		    $buyer_remarks = array();
		    $buyer_remarks['order_comment'] = $v['order_comment'];
		    $buyer_remarks['buyer_name'] = $v['buyer_name'];
		    $buyer_remarks['buyer_avatar'] =  UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.'06545327115832600.jpg';
		    
		    
		    $data[$k]['buyer_remarks'] = $buyer_remarks;
		
		    $store_remarks = array();
		    $store_remarks['list'] = array(
		        array(
		          'image'=>  UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.'06545327115832600.jpg',
		          'desc'=>'阿巴巴巴巴巴'   
		        ),
		        array(
		            'image'=>  UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.'06545327115832600.jpg',
		         'desc'=>'阿巴巴巴巴巴'   
		        ),
		        array(
		         'image'=>  UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.'06545327115832600.jpg',
		         'desc'=>'阿巴巴巴巴巴'   
		        ),
		    );
		    
		    $data[$k]['store_remarks'] = $store_remarks;
		
		    
		
									
		}
		
		
		
		
		//导航点
		$nav_data = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));	
	    $nav_list = array();
	    $sector_list = array();
		foreach($nav_data as $k => $v){
		    
		    // 导航点
		    $nav_coordinate = explode(',',$v['region_nav_coordinate']);
		    $nav_list[$k]['name'] = $v['region_name'];
		    $nav_list[$k]['color'] = $v['region_color'];
		    $nav_list[$k]['lng'] = $nav_coordinate[0];
		    $nav_list[$k]['lat'] = $nav_coordinate[1];
		    $nav_list[$k]['desc'] = '测试测试';
		    $nav_list[$k]['image'] = UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.'06545327115832600.jpg';
		    
		    //
		    $sector_list[$k]['name'] = $v['region_name'];
		    $sector_list[$k]['color'] = $v['region_color'];
		 
		 
		 	$sector_coordinate = explode("|", $v['region_money_coordinate']);
		 	foreach($sector_coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk]['lng'] = floatval($geo_c[0]);
				$geo[$kk]['lat'] = floatval($geo_c[1]);				
			}		
		    $sector_list[$k]['sector_list'] = 	$geo;
		    
		    
		}
		
	
		
		output_data(array('list'=>$data,'money'=>$nav_list,'sector' => $sector_list));
		
	}
	
	
	//计算配送员于钱标的距离
	
	private function DeliverMoneyDistance($deliver_coordinate){
		
		$deliver_coordinate = explode(',',$deliver_coordinate);			
		$points = array();
		$points['lng'] = $deliver_coordinate[0];
		$points['lat'] = $deliver_coordinate[1];		       
		
		$money_data = model('region')->getRegionList(array('region_enable' => 1,'region_money_state' =>1));
		$money = array();
		foreach($money_data as  $k => $v){			

			$coordinate = explode("|", $v['region_money_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk]['lng'] = floatval($geo_c[0]);
				$geo[$kk]['lat'] = floatval($geo_c[1]);	
							
				$row = $this->is_point_in_polygon($points, $geo);	
				if($row == true){
					return true;
					break;
				}
			}
						
		}	
			
	}

	/*
	private function DeliverMoneyDistance($deliver_coordinate){
	
		$money = array();
		$money[0] =array(		
			'lat' => '32.03533764550808',
			'lng' => '118.77069084008008',
			'color' => '#FFA4A4'	
		);
		$money[1] =array(		
			'lat' => '32.022971582635336',
			'lng' => '118.84574467387733',
			'color' => '#FED38E'	
		);
		$money[2] =array(		
			'lat' => '31.99103693828563',
			'lng' => '118.8293017844943',
			'color' => '#A2CDF3'	
		);	
		$money[3] =array(		
			'lat' => '31.999134097971847',
			'lng' => '118.73966151656555',
			'color' => '#A2CDF3'	
		);
		$money[4] =array(		
			'lat' => '36.6224366391192',
			'lng' => '114.5074487981824',
			'color' => '#A2CDF3'	
		);	
		$money[5] =array(		
			'lat' => '36.61993123074936',
			'lng' => '114.47021224795043',
			'color' => '#A2CDF3'	
		);	
		$money[6] =array(		
			'lat' => '36.59630480615505',
			'lng' => '114.47221900814253',
			'color' => '#A2CDF3'	
		);	
		$money[7] =array(		
			'lat' => '36.6020330888346',
			'lng' => '114.50432717121708',
			'color' => '#A2CDF3'	
		);	
	
		$deliver_coordinate = explode(',',$deliver_coordinate);
		$result = false;
		foreach($money as  $k => $v){					
			$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $v['lat'], $v['lng']);
			if($distance < 3000){				
				$result = true;				
			}				
		}				
		return $result;
	}
	
	*/
	
	
	private function getStore($store_id){
		return  model('store')->getStoreInfo(array('store_id' => $store_id));
	}
	//获取语言排序
	
	public function waiter_langOp(){
		
		
		$where = array(
			'member_id' => $this->waiter_info['member_id']
		);
		$list = model('member_language')->getMemberLangList($where);	
		$data = array();
		foreach($list as $k => $v){
			
			$data[$k] = $v;
			$data[$k]['language_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['language_icon'];
			
		}	
		output_data($data);
		
	}
	
	
	public function waiter_lang_sortOp(){
		
		$ids = explode(',',$_POST['id']);
		foreach($ids as $k=> $v){	
			$where = array(
				'language_id' => $v,
				'member_id' => $this->waiter_info['member_id']
			);		
			$update = array(
				'language_sort' => $k+1
			);
			$row = model('member_language')->editMemberLang($update,$where);
		}
		
		if($row){			
			output_data('ok');			
		}else{			
			output_error('排序失败');
		}
	
	
	}
	
	//导航的时间上报
	public function nav_timeOp(){
	    
	    $where = array(
	        'distributor_id'   => $this->waiter_info['distributor_id']
	    );
	    
	    $update = array(
	        'time' => strtotime($_POST['time'])
	    );
	    
	    $row = model('waiter')->editWaiter($where,$update);
	    if($row){
	        output_data('上报成功');
	    }else{
	        output_error('上报失败');
	    }
	    
	}
	
	
	
	private function getDistance($lat1, $lng1, $lat2, $lng2) {
      
	    $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }
    
    
    private function is_point_in_polygon($point, $pts) {
		$N = count($pts);
		$boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
		$intersectCount = 0;//cross points count of x 
		$precision = 2e-10; //浮点类型计算时候与0比较时候的容差
		$p1 = 0;//neighbour bound vertices
		$p2 = 0;
		$p = $point; //测试点
	 
		$p1 = $pts[0];//left vertex        
		for ($i = 1; $i <= $N; ++$i) {//check all rays
			// dump($p1);
			if ($p['lng'] == $p1['lng'] && $p['lat'] == $p1['lat']) {
				return $boundOrVertex;//p is an vertex
			}
			 
			$p2 = $pts[$i % $N];//right vertex            
			if ($p['lat'] < min($p1['lat'], $p2['lat']) || $p['lat'] > max($p1['lat'], $p2['lat'])) {//ray is outside of our interests
				$p1 = $p2; 
				continue;//next ray left point
			}
			 
			if ($p['lat'] > min($p1['lat'], $p2['lat']) && $p['lat'] < max($p1['lat'], $p2['lat'])) {//ray is crossing over by the algorithm (common part of)
				if($p['lng'] <= max($p1['lng'], $p2['lng'])){//x is before of ray
					if ($p1['lat'] == $p2['lat'] && $p['lng'] >= min($p1['lng'], $p2['lng'])) {//overlies on a horizontal ray
						return $boundOrVertex;
					}
					 
					if ($p1['lng'] == $p2['lng']) {//ray is vertical                        
						if ($p1['lng'] == $p['lng']) {//overlies on a vertical ray
							return $boundOrVertex;
						} else {//before ray
							++$intersectCount;
						}
					} else {//cross point on the left side
						$xinters = ($p['lat'] - $p1['lat']) * ($p2['lng'] - $p1['lng']) / ($p2['lat'] - $p1['lat']) + $p1['lng'];//cross point of lng
						if (abs($p['lng'] - $xinters) < $precision) {//overlies on a ray
							return $boundOrVertex;
						}
						 
						if ($p['lng'] < $xinters) {//before ray
							++$intersectCount;
						} 
					}
				}
			} else {//special case when ray is crossing through the vertex
				if ($p['lat'] == $p2['lat'] && $p['lng'] <= $p2['lng']) {//p crossing over p2
					$p3 = $pts[($i+1) % $N]; //next vertex
					if ($p['lat'] >= min($p1['lat'], $p3['lat']) && $p['lat'] <= max($p1['lat'], $p3['lat'])) { //p.lat lies between p1.lat & p3.lat
						++$intersectCount;
					} else {
						$intersectCount += 2;
					}
				}
			}
			$p1 = $p2;//next ray left point
		}
	 
		if ($intersectCount % 2 == 0) {//偶数在多边形外
			return false;
		} else { //奇数在多边形内
			return true;
		}
	}
	
	
	
}
