<?php
/**
 * 商家订单
 * 1
 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_orderControl extends mobileSellerControl
{
    public function __construct()
    {
        parent::__construct();
    }

    public function order_listOp()
    {
        $model_order = Model('order');
       
       
        $fils = "order_id,order_sn,store_id,store_name,store_phone,store_address,store_region_id,store_region_sm_id,store_coordinate,store_region_name,store_region_color,buyer_id,buyer_code,buyer_name,buyer_phone,buyer_address,buyer_region_id,buyer_is_new,buyer_coordinate,buyer_region,buyer_region_color,buyer_comment,order_comment,store_comment,order_amount,order_state,distributor_id,distributor_name,distributor_mobile,distributor_start_time,distributor_end_time,order_estimate_time,distributor_code,distributor_duration,distributor_coordinate,is_delay,order_type";
       
        $order_list = array();
        $order_list = $model_order->getStoreOrderList(
            $this->store_info['store_id'],
            $_POST['order_sn'],
            $_POST['buyer_name'],
            $_POST['state'],
            $_POST['start_date'],
            $_POST['end_date'],
            $_POST['skip_off'],
            $fils,
            array('order_goods'),
            $_POST['is_problem']
        );
        
        
        //	   sort($order_list);
        
        $page_count = $model_order->gettotalpage();
        $order_count = $this->order_count();
        output_data(array('order_list' => $order_list,'order_count'=>$order_count), mobile_page($page_count));
    }

    
    //统计单数
    private function order_count()
    {
        $model =model('order');
        $data = array();
        $data['order_problem'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'is_problem'=> 1)); //问题单
        $data['order_disposed'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'is_problem'=> 2)); //已解决
                
        $data['order_10'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 10));
        $data['order_20'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 20));
        $data['order_30'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 30));
        $data['order_40'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 40));
        $data['order_50'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 50));
        $data['order_60'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 60));
        $data['order_70'] = $model->getOrderCount(array('store_id'=> $this->store_info['store_id'],'order_state'=> 70));
        return $data;
    }
    
    
    
    
    //
        
    public function order_infoOp()
    {
        $order_id = intval($_POST['order_id']);
        
        if (!$order_id) {
            output_error('订单编号有误');
        }
        $model_order = Model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['store_id'] = $this->store_info['store_id'];

        $order = $model_order->getOrderInfo($condition, array('order_common','order_goods','member','store','one_stage_refund','two_stage_refund','three_stage_refund','replace','goods_give'));
        
        
        //print_r($order);
        
        
        if (empty($order)) {
            output_error('订单信息不存在');
        }
        
        
        $order_item = array();
        
        //从新格式化
                
        
        $order_user = array();
        $order_user['buyer_code'] = $order['buyer_code'];
        $order_user['buyer_name'] = $order['buyer_name'];
        $order_user['buyer_phone'] = $order['buyer_phone'];
        $order_user['buyer_comment'] = $order['buyer_comment'];
        $order_user['buyer_is_new'] = $order['buyer_is_new'];
        $order_user['distributor_name'] = $order['distributor_name'];
        $order_user['distributor_mobile'] = $order['distributor_mobile'];
        $order_user['state'] = $order['order_state'];
        $order_user['evaluation_state'] = $order['evaluation_state'];
        
        
        if ($order['evaluation_state'] == 1) {
            $model = model('evaluate_store');
            $where = array(
                'store_evaluate_orderid' => $order['order_id']
            );
            $eva = $model->getEvaluateStoreInfo($where);
            $order['store_evaluate_addtime'] = date('Y-m-d', $eva['store_evaluate_addtime']);
            $order['evaluation_text'] = $eva['store_evaluate_score_text'];
            $order['evaluation_content'] = $eva['store_evaluate_score_content'];
        }
                    
        $order_item['order_user'] = $order_user;
                
                
                
        //订单商品,原始商品，不做任何修改
        $order_goods = array();
        $goods =  $order['extend_order_goods'];
        $goods_num = 0;
        $goods_list = array();
        foreach ($goods as $k => $v) {
            $goods_list[] = $v;
            $goods_num += $v['goods_num'];
        }
        
        $order_goods['goods_lang']  = $goods_list[0]['goods_lang'];
        $order_goods['goods_list']  = $goods_list;
        $order_goods['goods_num']   = $goods_num;
        $order_item['order_goods']  = $order_goods;
        

        //金额信息
        $order_amount = array();
        //商品小计
        $order_amount['goods_amount'] = $order['goods_amount'];
        //商品折扣
        $order_amount['sales_amount'] = $order['sales_amount'];
        //餐盒费
        $order_amount['foodbox_amount'] = $order['foodbox_amount'];
        //配送费
        $order_amount['delivery_amount'] = $order['delivery_fee'];
        //支付手续费
        
        $order_amount['pay_commission_amount'] = $order['pay_commission_amount'];
        
        //平台抽成
        $order_amount['commission_amount'] = $order['commission_amount'];
        //订单价格
        $order_amount['order_amount'] = $order['order_amount'];
        //商家优惠券
        $order_amount['store_coupon_price'] = $order['store_coupon_price'];
        //平台优惠券
        $order_amount['platform_coupon_price']  = $order['platform_coupon_price'];
        //订单实付
        $order_amount['pay_order_amount'] = $order['order_amount'];
        
        $order_item['order_amount'] = $order_amount;
        
        
        
        
        
        
        //一阶段退款
        $one_stage_refund = array();
        $refund = $order['extend_one_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        $one_stage_refund['goods_num'] 	= 	$goods_num;
        $one_stage_refund['goods_amount'] = $goods_amount;
        $one_stage_refund['goods_list'] = $order['extend_one_stage_refund'];
        $order_item['one_stage_refund'] = $one_stage_refund;
                
        
        //二阶段退款
        $two_stage_refund = array();
        $refund = $order['extend_two_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
                
        $two_stage_refund['goods_num'] = 	$goods_num;
        $two_stage_refund['goods_amount'] = 	$goods_amount;
        $two_stage_refund['goods_list'] = $order['extend_two_stage_refund'];
        $order_item['two_stage_refund'] = $two_stage_refund;
        
        
        //三阶段退款
        $three_stage_refund = array();
        $refund = $order['extend_three_stage_refund'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        $three_stage_refund['goods_num']    = 	$goods_num;
        $three_stage_refund['goods_amount'] = 	$goods_amount;
        $three_stage_refund['goods_list'] = $order['extend_three_stage_refund'];
        $order_item['three_stage_refund'] = $three_stage_refund;
        
        
        
        //全单退款
        
    
    
        //替换商品
        $order_replace = array();
    
            
        
        $order_replace['goods_num'] = 	$order['extend_order_goods_replace_num'];
        $order_replace['goods_amount'] = $order['extend_order_goods_replace_amount'];
        $order_replace['goods_list'] = $order['extend_order_goods_replace'];
        $order_item['order_replace'] = $order_replace;
        
        
        
        
        
        //商家赠送商品
        $order_goods_give = array();
        $give = $order['extend_order_goods_give'];
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($give)) {
            foreach ($give as $k => $v) {
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        
        
        
        $order_goods_give['goods_num'] = 	$goods_num;
        $order_goods_give['goods_amount'] = 	$goods_amount;
        $order_goods_give['goods_list'] = $order['extend_order_goods_give'];
        $order_item['order_give'] = $order_goods_give;
        
        
        //print_r($order);
        
        //商家赠送的优惠券
        $order_coupon_give = array();
        $order_coupon = model('order_coupon')->getCouponList(array('order_id' => $order['order_id']));
        //print_r($order_coupon);
        
        $order_item['order_coupon_give'] = $order_coupon;
        
    
        
        //订单信息
        $order_info = array();
        $order_info['order_sn'] = $order['order_sn'];
        $order_info['add_time'] = date('H:i:s d/m/y', $order['add_time']);
        $order_info['store_comment'] = $order['store_comment']?$order['store_comment']:'--';
        $order_info['payment_name'] = $order['payment_name'];
        $order_info['countdown'] =  120;
        $order_item['order_info'] = $order_info;
        
    
    
    
            
        //支付方式
        $payment_info = array();
        $payment_info['wallet_fee'] = $order['pd_amount']; //余额支付
        $payment_info['external_payment'] = array(
            'bank_name' => $order['bank_name'],
            'bank_pay_fee' => $order['order_amount'] - $order['pd_amount']
        );
        
        $order_item['payment_info'] = $payment_info;
        
        
        
        //收入信息
        $order_income = array();
        $order_income['commission_amount'] = $order['commission_amount'];
        $order_income['order_amount'] = $order['order_amount'];
        $order_item['order_income'] = $order_income;
        
        
            
        if ($order['distributor_id'] > 0) {
            $delivery = array();
        
            $delivery['delivery_id'] = $order['distributor_id'];
            $delivery['delivery_name'] = $order['distributor_name'];
            $delivery['delivery_mobile'] = $order['distributor_mobile'];
            $delivery['delivery_start_time'] = $order['distributor_start_time'];
            $delivery['delivery_end_time'] = $order['distributor_end_time'];
            $delivery['order_estimate_time'] = $order['order_estimate_time'];
            $delivery['delivery_code'] = $order['distributor_code'];
            $delivery['delivery_duration'] = $order['distributor_duration'];
            $delivery['delivery_coordinate'] = $order['distributor_coordinate'];
            $delivery['delivery_comment'] = $order['deliver_comment'];
            
            $order_item['delivery'] = $delivery;
        }
        
        
        
        //按钮信息
        $order_btn = array();
        $order_btn['state'] = $order['order_state'];
        $order_btn['order_id'] = $order['order_id'];
        
        $order_item['order_btn'] = $order_btn;
        $order_item['state'] = $order['order_state'];
        $order_item['refund_state'] =$order['refund_state'];
        $order_item['is_update'] =$order['is_update'];
        $order_item['is_problem'] =$order['is_problem'];
        
        $order_item['problem_type'] =$order['problem_type'];
        $order_item['is_delay'] =$order['is_delay'];
            
        $order_item['order_type'] =$order['order_type'];
        
        $problem = array();
        $problem['problem_content'] = $order['problem_content'];
        $problem['problem_type_name'] = $order['problem_type_name'];
        $problem['problem_image'] = $this->order_problem_image($order['problem_image']);//empty($order['problem_image']) ? array() : unserialize($order['problem_image']);
        $problem['problem_type_id'] = $order['problem_type_id'];
        $problem['problem_type'] = $order['problem_type'];
        $problem['problem_state'] = $order['problem_state'];
        $problem['problem_title'] = $order['problem_title'];
        $order_item['problem'] = $problem;
        
        if ($order['evaluation_state'] == 1) {
            $eva_model = model('evaluate_store');
            $evaluate = array();
            $eva_info = $eva_model->getEvaluateStoreInfo(array('store_evaluate_orderid'=> $order['order_id']));
        
            if (!empty($eva_info)) {
                $eva_info['store_evaluate_image'] = $this->order_evaluate_image($eva_info['store_evaluate_image']);
    
    
                $eva_info['store_evaluate_addtime'] = date('H:i d M y', $eva_info['store_evaluate_addtime']);
                
                $eva_info['store_evaluate_score'] = $eva_info['store_evaluate_score_text'];
                
                
                $evaluate = $eva_info;
            }
            
                
            $order_item['evaluate'] = $evaluate;
        }
        
        
        
        output_data($order_item);
    }

    
    //格式化图片
    private function order_problem_image($image)
    {
        $list = array();
        if (!empty($image)) {
            $image  =	explode(",", $image);
            foreach ($image as $v) {
                $list[] = $v;
            }
            return $list;
        } else {
            return $list;
        }
    }
    
    
    //格式化图片
    private function order_evaluate_image($image)
    {
        $list = array();
        
        if (!empty($image)) {
            $image  =	explode(",", $image);
            //print_r($image);
            foreach ($image as $v) {
                $list[] = UPLOAD_SITE_URL.DS.ATTACH_EVALUATE.DS.$v;
            }
            return $list;
        } else {
            return $list;
        }
    }
    
    private function order_replace_goods($rec_id)
    {
        $list =    model('order')->getOrderGoodsList(array('replace_rec_id'=>$rec_id));
        $data = array();
        foreach ($list as $v) {
            $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
            $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
            $data[] = $v;
        }
        
        return $data;
    }

    
    
    
    
    
    
    /**
    * 接单
    */
    public function order_acceptOp()
    {
        $order_id = intval($_POST['order_id']);
        $model_order = Model('order');
     //   $stoket =  model('stoket');
    
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['store_id'] = $this->store_info['store_id'];
        $order_info = $model_order->getOrderInfo($condition);

        if (!empty($order_info)) {      
           
            $row = $model_order->editOrder(['order_state' => 40], $condition);

            if ($row) {
                
				//商家接单
				$record_data = array();
				$record_data['order_id'] = $order_info['order_id'];
				$record_data['store_taking_time']       =  time(); //配送员编号
				$record_data['store_taking_type']          = 'Manual';//当前批次第几单			
				model('order_record')->addRecord('order_confirmed_courier',$record_data);
				   
								
				
                //更新商家配餐商品数量      
       
                $update = array();
                $update['order_state_peican'] = array('exp','order_state_peican + 1');
                model('store')->editStore($update,['store_id' => $order_info['store_id']]);


                
                // 发送买家消息
                $param = array();
                $param['code'] = 'order_store_received';
                $param['member_id'] = $order_info['buyer_id'];
                $param['param'] = array(
                    'order_sn' => $order_info['order_sn'],
                    'id' => $order_info['order_id']
                );
                QueueClient::push('sendMemberMsg', $param);
                    
                
                
                
                $data = array();
                $data['order_id'] = $order_info['order_id'];
                $data['log_role'] = 'seller';
                $data['log_msg'] = '商家已接单';
                $data['log_user'] = $order_info['store_id'];
              
                $data['log_orderstate'] = 40;
                $model_order->addOrderLog($data);

                /*
                //推送给用户
                $data = array();
                $data['type'] = 'order_update';
                $data['data'] = $order_info;
                $data = json_encode($data);
                $stoket->sendUser($order_info['buyer_id'], $data);
                */
                               
							
                if ($order_info['order_type'] == 1) { //自提单不需要推送给骑手
                    /*
                    $data = array();
                    $data['type'] = 'order_update';
                    $data['data'] = $order_info;
                    //print_r($order_info);
                    $data = json_encode($data);
                    $stoket->sendDelivery($order_info['distributor_id'], $data);
                    */
                }
                
                //且慢恢复
                model('store')->editStore(['store_is_delay' => 0], ['store_id' => $this->store_info['store_id']]);
                
                //恢复标记且慢的单子
                model('order')->editOrder(['is_delay' => 0 ], ['store_id' => $this->store_info['store_id'],'order_state' => 30, 'is_delay' => 1]);
                
                
                output_data('ok');
            }
        } else {
            output_error('订单不存在');
        }
    }
    

    //拒绝接单
    public function refuse_orderOp(){

        $order_id = intval($_POST['order_id']);
        $model_order = Model('order');
     //   $stoket =  model('stoket');
    
        $condition = array();
        $condition['order_id'] = $order_id;
        $condition['store_id'] = $this->store_info['store_id'];
        $order_info = $model_order->getOrderInfo($condition);

        if(!empty($order_info)){
            $row = $model_order->editOrder(['order_state' => 0], $condition);

            //拒绝后直接退款
            



        }
    
      


    }

    

    
    
    
    //且慢按钮[且慢功能已取消]
    public function order_delayOp()
    {
        $model = model('store');
        $where = array(
            'store_id' => $this->store_info['store_id'],
        );
        $update = array(
            'store_is_delay' => 1
        );
        $row = $model->editStore($update, $where);
        if ($row) {
            $order_list = model('order')->getOrderListNew(['order_state' => 30,'store_id' => $this->store_info['store_id']]);
            $order_list_sn = '';
            foreach ($order_list as $v) {
                $order_list_sn .= $v['order_sn'].'|';
            }
            
            foreach ($order_list as $v) {
                $data_record = array();
                $data_record['order_id'] 			  = $v['order_id'];
                $record_data['order_pending_ids']     =  $order_list_sn; //
                $record_data['order_pending_time']  =  time();//
                model('order_record')->addRecord('order_pending', $data_record);
                    
                model('order')->editOrder(['is_delay' => 1 ], ['order_id' => $v['order_id']]);
            }
                    
            
            output_data('操作成功');
        } else {
            output_error('操作失败');
        }
    }
    
    
    
    
    
    public function goods_revokeOp()
    {
        $where = array(
          //  'rec_id' => $rec_id,
            'store_id' => $this->store_info['store_id']
        );
        $rec_info = model('order')->getOrderGoodsInfo($where);
        if ($_POST['type'] == 1) {
            $update['goods_num'] = $rec_info['goods_num'] + $rec_info['old_goods_num'];
            $update['old_goods_num'] = 0;
            $update['state'] = 0;
        }
        
        if ($_POST['type'] == 2) {
            $update['goods_name'] = $rec_info['old_goods_name'];
            $update['goods_price'] = $rec_info['old_goods_price'];
            $update['goods_num'] = $rec_info['old_goods_num'];
            $update['goods_id'] = $rec_info['old_goods_id'];
            $update['state'] = 0;
        }
        
        $row = model('order')->editOrderGoods($update, $where);
        if ($row) {
            output_data('修改成功');
        } else {
            output_error('操作失败');
        }
    }
    
    
    
    
    
    //从新计算订单价格
    
    private function order_amount($order_id)
    {
        $model = model('order');
        
        $order_info = $model->getOrderInfo(array('order_id'=> $order_id,'store_id'=>$this->store_info['store_id']));
        
        $where = array(
            'order_id' => $order_id
        );
        $order_goods = model('order')->getOrderGoodsList($where);
        $goods_amount = 0;
        foreach ($order_goods as $v) {
            $goods_amount +=  $v['goods_price'] * $v['goods_num'];
        }
        
        
        $update = array();
        $update['goods_amount'] = $goods_amount;
        $update['order_amount'] = $goods_amount + $order_info['delivery_fee'] - $order_info['sales_amount'];
        
        
        $where = array('order_id'=> $order_id,'store_id'=>$this->store_info['store_id']);
        
        $model->editOrder($update, $where);
    }
    
    
    
    
    
    
    
    /*
    * 打单接口
    */
    
    
    public function order_printOp()
    {
        $model = model('order');
                
        $order_id = $_POST['order_id'];
        
        $where = array();
        $where['store_id'] = $this->store_info['store_id'];
        $where['order_id'] = $order_id;
        
        $order_info = $model->getOrderInfo($where, array('order_print_goods','member','store'));
        
        $data = array();
        if ($order_info) {
            $deliver = array();
            $delivery['delivery_code'] 		= $order_info['distributor_code'];
            $delivery['delivery_id'] 		= $order_info['distributor_id'];
            $delivery['delivery_name'] 		= $order_info['distributor_name'];
            $delivery['delivery_mobile'] 	= $order_info['distributor_mobile'];
                        
            $data['delivery'] = $delivery;
            $store_info = $order_info['extend_store'];
            
            $store = array();
            $store['store_code']            = $store_info['store_code'];
            $store['store_name_primary']    = $store_info['store_name_primary'];
            $store['store_name_secondary'] = $store_info['store_name_secondary'];
            $data['store'] = $store;
                
                
            $goods_list = array();
            
            foreach ($order_info['extend_print_order_goods'] as $k=> $v) {
                $goods_list[$k] = $v;
            }
            
            
            
                        
            $data['item_list'] = $goods_list;
            $data['order_note'] = $order_info['order_comment'];
            $data['delivery_note'] = $order_info['deliver_comment'];
            
            
            
            $user = array();
            $user['buyer_code'] = $order_info['buyer_code'];
            $user['buyer_name'] = $order_info['buyer_name'];
            $user['buyer_phone'] = $order_info['buyer_phone'];
            $user['buyer_address'] = $order_info['buyer_address'];
            $data['user'] = $user;
            $data['order_id'] = $order_info['order_id'];
            $data['order_sn'] = $order_info['order_sn'];
        }
            
    
        output_data($data);
    }
    
    
    //驳回【此功能已弃用】
        
    public function order_rejectOp()
    {
        $order_id = $_POST['order_id'];
        $where = array();
        $where['order_id'] = $order_id;
        $where['store_id'] = $this->store_info['store_id'];
        
        $data = array();
        $data['problem_state'] = 2; //驳回
        $data['is_problem'] = 0;
        $row = model('order')->editOrder($data, $where);
        if ($row) {
            $order_info =model('order')->getOrderInfoN($where);
            
            //判断是否有赠送店铺优惠券
            if ($_POST['coupon_id'] > 0) {
                $t_id = intval($_POST['coupon_id']);
                if ($t_id <= 0) {
                    output_error('店铺优惠券信息错误');
                }
                $model_voucher = Model('voucher');
                //验证是否可领取代金券
                $data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
                if ($data['state'] == false) {
                    output_error($data['msg']);
                }
                try {
                    $model_voucher->beginTransaction();
                    //添加代金券信息
                    $data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
                    if ($data['state'] == false) {
                        throw new Exception($data['msg']);
                    }
                    $model_voucher->commit();
                } catch (Exception $e) {
                    $model_voucher->rollback();
                    output_error($e->getMessage());
                }
            }
            
            $give_list = model('order_goods_give')->getGiveList(array('order_id'=>$order_info['order_id'],'store_id'=>$order_info['store_id'],'give_type'=>0));
            //判断是否有赠送商品
            if (!empty($give_list)) {
                foreach ($give_list as $v) {
                    $goods_info = array();
                    $goods_info['order_id'] = $v['order_id'];
                    $goods_info['goods_id'] = $v['goods_id'];
                    $goods_info['goods_name'] = $v['goods_name'];
                    $goods_info['goods_price'] = $v['goods_price'];
                    $goods_info['goods_num'] = $v['goods_num'];
                    $goods_info['goods_image'] = $v['goods_image'];
                    $goods_info['goods_pay_price'] = 0;
                    $goods_info['store_id'] = $v['store_id'];
                    $goods_info['goods_spec'] = $v['goods_spec'];
                    $goods_info['goods_size'] = $v['goods_size'];
                    $goods_info['state'] = 3;
                    $goods_info['goods_lang'] = $v['goods_lang'];
                    $goods_info['goods_optional'] = $v['goods_optional'];
                    $goods_info['replace_rec_id'] = $v['rec_id'];
                    $goods_info['is_take_ffect'] = 1;
                    model('order')->addOrderGood($goods_info);
                    
                    //删除赠送商品记录
                }
                model('order_goods_give')->delGive(array('order_id'=>$order_info['order_id'],'store_id'=>$order_info['store_id'],'give_type'=>0));
            }
                    
                
                
                
            output_data('已驳回');
        } else {
            output_error('操作失败');
        }
    }
    

    //通过【此功能已弃用】
    
    public function order_adoptOp()
    {
        $model = model('order');
        $order_id = $_POST['order_id'];
        $where = array();
        $where['order_id'] = $order_id;
        $where['store_id'] = $this->store_info['store_id'];
        
        $data = array();
        $data['problem_state'] = 1; //通过
        $data['is_problem'] = 0;
                
        
        $row = $model->editOrder($data, $where);
        
        if ($row) {
            $order_info = $model->getOrderInfoN($where);
            
            
            //判断是否有赠送店铺优惠券
            if ($_POST['coupon_id'] > 0) {
                $t_id = intval($_POST['coupon_id']);
                if ($t_id <= 0) {
                    output_error('店铺优惠券信息错误');
                }
                $model_voucher = Model('voucher');
                //验证是否可领取代金券
                $data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
                if ($data['state'] == false) {
                    output_error($data['msg']);
                }
                try {
                    $model_voucher->beginTransaction();
                    //添加代金券信息
                    $data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
                    if ($data['state'] == false) {
                        throw new Exception($data['msg']);
                    }
                    $model_voucher->commit();
                } catch (Exception $e) {
                    $model_voucher->rollback();
                    output_error($e->getMessage());
                }
            }
            
            $give_list = model('order_goods_give')->getGiveList(array('order_id'=>$order_info['order_id'],'store_id'=>$order_info['store_id'],'give_type'=>0));
            //判断是否有赠送商品
            if (!empty($give_list)) {
                foreach ($give_list as $v) {
                    $goods_info = array();
                    $goods_info['order_id'] = $v['order_id'];
                    $goods_info['goods_id'] = $v['goods_id'];
                    $goods_info['goods_name'] = $v['goods_name'];
                    $goods_info['goods_price'] = $v['goods_price'];
                    $goods_info['goods_num'] = $v['goods_num'];
                    $goods_info['goods_image'] = $v['goods_image'];
                    $goods_info['goods_pay_price'] = 0;
                    $goods_info['store_id'] = $v['store_id'];
                    $goods_info['goods_spec'] = $v['goods_spec'];
                    $goods_info['goods_size'] = $v['goods_size'];
                    $goods_info['state'] = 3;
                    $goods_info['goods_lang'] = $v['goods_lang'];
                    $goods_info['goods_optional'] = $v['goods_optional'];
                    $goods_info['replace_rec_id'] = $v['rec_id'];
                    $goods_info['is_take_ffect'] = 1;
                    $model->addOrderGood($goods_info);
                    
                    //删除赠送商品记录
                }
                model('order_goods_give')->delGive(array('order_id'=>$order_info['order_id'],'store_id'=>$order_info['store_id'],'give_type'=>0));
            }
                
                
            
            //退款
            if ($order_info['problem_type'] == 1) {
                
                //执行退款部分退款
                if ($order_info['refund_state'] == 1) {
                }
                
                //执行退款全部
                if ($order_info['refund_state'] == 2) {
                    
                    
                    //全部退款执行完毕后更新订单未已取消状态。
                }
            }
            
            //补送
            if ($order_info['problem_type'] == 2) {
            }
            output_data('通过');
        } else {
            output_error('操作失败');
        }
    }
    
    
    //确认已自提
    
    public function order_confirmOp()
    {
        $order_id = $_POST['order_id'];
        $model = model('order');
        $where = array();
        $where['order_id'] = $order_id;
        $where['store_id'] = $this->store_info['store_id'];
        $where['order_state'] = array('in','30,40,50');
        $where['order_type'] = 2; //自提单
        $data = array();
        $data['order_state'] =  60;
        $row = $model->editOrder($data, $where);
        if ($row) {
            model('order_bill')->bill($order_id);
            
            output_data('取餐成功');
        } else {
            output_error('取餐失败');
        }
    }
}
