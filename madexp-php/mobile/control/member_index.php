<?php
/**
 * 我的商城
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_indexControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 我的商城
     */
    public function indexOp() {
        $member_info = array();
        $member_info['user_name'] = $this->member_info['member_name'];
        $member_info['avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
		$member_info['member_nickname'] = $this->member_info['member_truename'];
        $member_info['member_mobile'] = $this->member_info['member_mobile'];
		$member_info['member_emaile'] = $this->member_info['member_emaile'];
		$member_info['member_code'] = $this->member_info['member_code'];
		
		
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);
		$attr = array();
		foreach($list as $v){
			$attr[$v['attr_id']] = $v;		
		}
		
		$member_info['member_attr'] = $attr[$this->member_info['member_attr']]['attr_name'];
		
        output_data(array('member_info' => $member_info));
    }
    
	
	
	
    
    
	private function getMemberAvatar($image){
	  //  print_r( UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image);
		if(file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !=''){
		
			return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
			
		}else{
			return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
		}
	}
	
	
	//绑定用户品类
	public function member_bind_cateOp(){
	
	   model('member_bind_class')->delMemberBindClass(array('member_id'=>$this->member_info['member_id']));
		
		
		$cateIds	 = $_POST['cateIds'];
		$where = array(
			'small_id' => array('in',$cateIds)
		);
		$data_list = model('store_small_class')->getStoreSmallClassList($where);
		
		
		foreach($data_list as  $v){
			$in_data[] = array(
				'member_id' => $this->member_info['member_id'],
				'bind_class_name' => $v['small_name'],
				'bind_class_id' => $v['small_id'],
				'bind_sort' => 0						
			);
		}
		
		$row = model('member_bind_class')->addMemberBindClassAll($in_data);
		
		if($row){
			  output_data('ok');
		}else{
			 output_error('保存失败');
		}
		
			
	}
	
	//编辑昵称
	public function saveTruenameOp(){
		
		 $model_member = Model('member');
		$where =array('member_id'=>$this->member_info['member_id']);
		$data = array(
			'member_truename' => $_POST['member_nickname']
		);		
		$update = $model_member->editMember($where,$data);
		if($update){
			output_data('ok');					
		}else{
			output_error('保存失败');	
		}
		
		
	}
	
	//编辑属性
	public function saveAttrOp(){
		
		$model_member = Model('member');
		$where =array('member_id'=>$this->member_info['member_id']);
		
		$data = array(
			'member_attr' => $_POST['attr_id']
		);		
		
		$update = $model_member->editMember($where,$data);
		if($update){
			output_data('1');					
		}else{
			output_error('保存失败');	
		}		
		
	}
	
	public function memberAttrOp(){		
		
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);
		$data = array();
		foreach($list as $k => $v){
			$data[$k] = $v;
			$data[$k]['action'] = $v['attr_id'] == $id ? 1: 0;
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];
		}
		
		$data = array(
			'attr_list' => $data,
			'member_attr' => $this->member_info['member_attr']
			
		);
		output_data($data);	
		
	}
	
	
	//编辑手机号
	public function saveMobileOp(){
		
		$state_data =  Logic('connect_api')->checkSmsCaptcha($_POST['mobile'], $_POST['captcha'], 4);//再次进行验证码验证
        $state = $state_data['state'];
        if($state == false){
			output_error('验证码错误');
		}
		$model_member = Model('member');		
		$where =array('member_id'=>$this->member_info['member_id']);
		$data = array(
			'member_mobile' => $_POST['mobile'],
			'member_name' => $_POST['mobile']
		);		
		$update = $model_member->editMember($where,$data);
		if($update){			
			$member['member_id'] = $this->member_info['member_id'];
			$member['member_name'] = $this->member_info['member_name'];
			$token =  Logic('connect_api')->getUserToken($member, $_POST['client']);	
			if($token){
					$state_data = array();
				 	$state_data['key'] = $token;
                    $state_data['username'] =$this->member_info['member_name'];
                    $state_data['memberid'] = $this->member_info['member_id'];
					output_data($state_data);		
			}else{
				output_error('修改失败');	
			}
			
					
					
		}else{
			output_error('保存失败');	
		}
		
				
	}
	
	
	//编辑手机号
	public function saveEmailOp(){
		
		$model_member = Model('member');
		
		$where = array(
			'member_id'=>$this->member_info['member_id'],
			'member_passwd' => md5($_POST['passwd'])
		);
		$info = $model_member->getMemberInfo($where);
		if(empty($info)){
			output_error('密码错误');
		}
		
		$where =array('member_id'=>$this->member_info['member_id']);
		$data = array(
			'member_email' => $_POST['member_email']
		);		
		$update = $model_member->editMember($where,$data);
		if($update){
			output_data('1');					
		}else{
			output_error('保存失败');	
		}		
	}
	
	
	
  
	
	//编辑头像
	public function saveAvatarOp(){
		
				
			$member_id = $this->member_info['member_id'];
			$upload = new UploadFile();
		//	$upload->set('thumb_width', 500);
		//	$upload->set('thumb_height',499);
		//	$ext = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
			//$upload->set('file_name',"avatar_$member_id.$ext");
		//	$upload->set('thumb_ext','_new');
		//	$upload->set('ifremove',true);
			$upload->set('default_dir',ATTACH_AVATAR);
			if (!empty($_FILES['file']['tmp_name'])){
				$result = $upload->upfile('file');
				if ($result){						
					$where = array(
						'member_id' => $this->member_info['member_id']
					);					
					$data = array(
						'member_avatar' => $upload->file_name
					);					
					$row = model('member')->editMember($where,$data);
					if($row){
						output_data(array('url'=> UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$upload->file_name,'filename'=>$upload->file_name));
					}else{
						output_error('上传失败');						
					}
				}else{
					
				  output_error($upload->error);	
				}
			}else{
				output_error('头像更新失败');
			}
		
		
		
			
		
	}
	
	
    /**
     * 我的资产
     */
    public function my_assetOp() {
        $param = $_GET;
        $fields_arr = array('point','predepoit','available_rc_balance','redpacket','voucher');
        $fields_str = trim($param['fields']);
        if ($fields_str) {
            $fields_arr = explode(',',$fields_str);
        }
        $member_info = array();
        if (in_array('point',$fields_arr)) {
            $member_info['point'] = $this->member_info['member_points'];
        }
        if (in_array('predepoit',$fields_arr)) {
            $member_info['predepoit'] = $this->member_info['available_predeposit'];
        }
        if (in_array('available_rc_balance',$fields_arr)) {
            $member_info['available_rc_balance'] = $this->member_info['available_rc_balance'];
        } 
        if (in_array('redpacket',$fields_arr)) {
            $member_info['redpacket'] = Model('redpacket')->getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        }
        if (in_array('voucher',$fields_arr)) {
            $member_info['voucher'] = Model('voucher')->getCurrentAvailableVoucherCount($this->member_info['member_id']);
        }
        output_data($member_info);
    }
	
	
	
	
	//绑定socket
	
	public function  bindOp(){	
			$stoket = model('stoket');
			$mid = $this->member_info['member_id'];		
			$cid = $_POST['cid'];				
			$stoket->bind($cid,$mid);
			$where = array(
				'client_id' => $cid
			);
			$data = array(
				'mid' => $mid,
				'type' => 0
				
			);
			$row = $stoket->editStoket($where,$data);		
			output_data('绑定成功');
	}
	
	//绑定设备号
	public function bind_tokenOp(){		
		
		$data = array();
		
		if($_POST['ios_device_token']){
			$data['ios_device_token'] = $_POST['ios_device_token'];
		}
		
		if($_POST['android_device_token']){
			$data['android_device_token'] = $_POST['android_device_token'];
		}				
		
		$update = $model_member->editMember(array('member_id' => $this->member_info['member_id']),$data);
		if($update){
			output_data('绑定成功');					
		}else{
			output_error('保存失败');	
		}		
	}
	
		
		
		
    public function sendOp(){
		
		$uid = $_POST['uid'];
		$order_sn = $_POST['order_sn'];
		
		$data = array(
			'order_state' => $_POST['state']
		);
		
		$row = model('order')->editOrder($data,array('order_sn'=> $order_sn));
		
				
		$stoket =  model('stoket');	
		
		$stoket->sendUser($uid,$order_sn);	
		
	}		
	
	
	//切换语言
	public function  system_langOp(){
	    $model_member = model('member');
        $where = array(
            'member_id' => $this->member_info['member_id']    
        );
        $update = array();
        $update['member_language'] = $_POST["lang"];
    	$update = $model_member->editMember($where,$update);
		if($update){
			output_data('切换成功');					
		}else{
			output_error('切换失败');	
		}		
	    
	    
	}
	
		
		
		
		

}
