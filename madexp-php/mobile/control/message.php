<?php
/**
 * 消息界面操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class messageControl extends mobileAdminControl {
  
    
    public function __construct(){
        parent::__construct();
	
    }

	public function indexOp(){
		
			$type = $_POST['type'];
			$list = [];
			switch($type){
				case 'member':				
					$list["data"] = $this->member_msg_tpl();			
					break;				
				case 'store':				
					$list["data"] = $this->store_msg_tpl();
					break;				
				case 'delivery':				
					$list["data"] = $this->delivery_msg_tpl();							
					break;				
				default:
					$list["data"] = $this->member_msg_tpl();
				
			}
			$list["count_member"] = count($this->member_msg_tpl());
			$list["count_store"] = count($this->store_msg_tpl());
			$list["count_delivery"] = count($this->delivery_msg_tpl());
			output_data($list);	
	}
		
	//获取消息模板
	public function get_msg_infoOp(){
		$type = $_POST['type'];
		$msg_code = $_POST['msg_code'];
		switch($type){
			case 'member':			
				$info = $this->get_member_msg_tpl_info();						
				break;	
			case 'store':			
				$info = $this->get_store_msg_tpl_info();
				break;		
			case 'delivery':
				$info = $this->get_delivery_msg_tpl_info();
		}
		
		if($info['code'] == 200){			
			output_data($info['data']);			
		}else{
			output_error('消息模板不存在');
		}
	}
	
	//列表保存状态
	public function save_stateOp(){		
		$type= $_POST['ajax_type'];
		$admin_state= $_POST['admin_state'];
		switch($type){
			case 'member':			
				$where = array();
				$where['mmt_code'] = $_POST['code'];
				$update = array();
				$update['mmt_message_switch'] = $_POST['admin_state'];
				$info = model('member_msg_tpl')->editMemberMsgTpl($where,$update);
				break;	
			case 'store':			
				$where = array();
				$where['smt_code'] = $_POST['code'];
				$update = array();
				$update['smt_message_switch'] = $_POST['admin_state'];
				$info = model('store_msg_tpl')->editStoreMsgTpl($where,$update);
				break;											
			case 'delivery':
				$where = array();
				$where['dmt_code'] = $_POST['code'];
				$update = array();
				$update['dmt_message_switch'] = $_POST['admin_state'];
				$info = model('delivery_msg_tpl')->editDeliveryMsgTpl($where,$update);
				break;
		}		
		if($info){			
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}		
	}
		
	//保存	
	public function save_msgOp(){		
		$type= $_POST['type'];
		$msg_code = $_POST['msg_code'];
		switch($type){
			case 'member':			
				$info = $this->save_member_tpl();						
				break;	
			case 'store':			
				$info = $this->save_store_tpl();
				break;											
			case 'delivery':
				$info = $this->save_delivery_tpl();
				break;
		}		
		if($info['code'] == 200){			
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}		
	}
	
	
	
	
	//调用语言配置
	private function get_system_lang(){		
		$system_lang = model('language')->getLangList(array('language_system' => 1));		
		return $system_lang;
	}
	
	//用户列表
	private function member_msg_tpl(){				
		$mmtpl_list = Model('member_msg_tpl')->getMemberMsgTplList(array());	
		$list = array();
		foreach($mmtpl_list as $k => $v){
			$list[$k]['ajax_type'] = "member";
			$list[$k]['code'] = $v['mmt_code'];
			$list[$k]['name'] = $v['mmt_name'];
			$list[$k]['message_switch'] = $v['mmt_message_switch'];
			$list[$k]['code_new'] = $v['mmt_code_new'];
			$list[$k]['name_lang'] = unserialize($v['mmt_name_lang']);
			$list[$k]['message_lang'] = unserialize($v['mmt_message_content_lang']);						
		}
			
			
		return $list;	
	}
	
	//用户模板默认
	private function get_member_msg_tpl_info(){			
		$where = array();
		$where['mmt_code'] = $_POST['code'];
	
		$mmtpl_info = Model('member_msg_tpl')->getMemberMsgTplInfo($where);
		if(!empty($mmtpl_info)){			
			$data = array();
			$data['ajax_type'] = "member";
			$data['code'] = $mmtpl_info['mmt_code'];
			$data['name'] = $mmtpl_info['mmt_name'];
			$data['message_switch'] = $mmtpl_info['mmt_message_switch'];
			$data['short_switch'] = $mmtpl_info['mmt_short_switch'];
			$data['mail_switch'] = $mmtpl_info['mmt_mail_switch'];
			$data['code_new'] = $mmtpl_info['mmt_code_new'];			
			$data['name_lang'] = unserialize($mmtpl_info['mmt_name_lang']);			
			$data['message_content_lang'] = unserialize($mmtpl_info['mmt_message_content_lang']);			
			return array('code'=> 200 ,'data'=> $data);
		}else{			
			return array('code'=> 400);
		}	
	}
		
	//保存用户模板
	private function save_member_tpl(){		
		
		$where = array();
		$where['mmt_code'] = $_POST['code'];
		
		$update = array();
		$update['mmt_name_lang'] 			= serialize($_POST['name_lang']);
		$update['mmt_message_content_lang'] = serialize($_POST['message_content_lang']);
		$update['mmt_message_switch'] 		= $_POST['message_switch'];		
		$update['mmt_short_switch'] 		= $_POST['short_switch'];		
		$update['mmt_mail_switch'] 		= $_POST['mail_switch'];		
		
		$row = model('member_msg_tpl')->editMemberMsgTpl($where,$update);		
		if($row){
			return array('code'=> 200);
		}else{
			return array('code' => 400);			
		}
	}

	
	//商家的消息模板
	private function store_msg_tpl(){
		$msgtpl_list = Model('store_msg_tpl')->getStoreMsgTplList(array());
		$list = array();
		foreach($msgtpl_list as $k => $v){			
			$list[$k]['ajax_type'] = "store";
			$list[$k]['code'] = $v['smt_code'];
			$list[$k]['name'] = $v['smt_name'];
			$list[$k]['message_switch'] = $v['smt_message_switch'];
			$list[$k]['short_switch'] = $v['smt_short_switch'];
			$list[$k]['mail_switch'] = $v['smt_mail_switch'];
			$list[$k]['code_new'] = $v['smt_code_new'];
			$list[$k]['name_lang'] = unserialize($v['smt_name_lang']);
			$list[$k]['message_lang'] = unserialize($v['smt_message_content_lang']);						
		}		
		return $list;
	}
	
	
	
	//商家模板默认
	private function get_store_msg_tpl_info(){	
		
		$where = array();
		$where['smt_code'] = $_POST['code'];
		$msgtpl_info = Model('store_msg_tpl')->getStoreMsgTplInfo($where);
		if(!empty($msgtpl_info)){			
			
			$data = array();
			$data['ajax_code'] = "store";
			$data['code'] = $msgtpl_info['smt_code'];
			$data['name'] = $msgtpl_info['smt_name'];
			$data['message_switch'] = $msgtpl_info['smt_message_switch'];
			$data['short_switch'] = $msgtpl_info['smt_short_switch'];
			$data['mail_switch'] = $msgtpl_info['smt_mail_switch'];
			$data['code_new'] = $msgtpl_info['smt_code_new'];			
			$data['name_lang'] = unserialize($msgtpl_info['smt_name_lang']);			
			$data['message_content_lang'] = unserialize($msgtpl_info['smt_message_content_lang']);			
			
						
			return array('code'=> 200 ,'data'=> $data);
		}else{			
			return array('code'=> 400);
		}		
	}
	
	//保存用户模板
	private function save_store_tpl(){		
		
		$where = array();
		$where['smt_code'] = $_POST['code'];
		
		$update = array();
		$update['smt_name_lang'] 			= serialize($_POST['name_lang']);
		$update['smt_message_content_lang'] = serialize($_POST['message_content_lang']);
		$update['smt_message_switch'] 		= $_POST['message_switch'];		
		$update['smt_short_switch'] 		= $_POST['short_switch'];		
		$update['smt_mail_switch'] 		= $_POST['mail_switch'];		
		
		$row = model('store_msg_tpl')->editStoreMsgTpl($where,$update);		
		if($row){
			return array('code' => 200);
		}else{
			return array('code' => 400);			
		}
	}
	
	
	


	private function delivery_msg_tpl(){
		$msgtpl_list = Model('delivery_msg_tpl')->getDeliveryMsgTplList(array());
		$list = array();
		foreach($msgtpl_list as $k => $v){			
			$list[$k]['ajax_type'] = "delivery";
			$list[$k]['code'] = $v['dmt_code'];
			$list[$k]['name'] = $v['dmt_name'];
			$list[$k]['message_switch'] = $v['dmt_message_switch'];
			$list[$k]['short_switch'] = $v['dmt_short_switch'];
			$list[$k]['mail_switch'] = $v['dmt_mail_switch'];
			$list[$k]['code_new'] = $v['dmt_code_new'];
			$list[$k]['name_lang'] = unserialize($v['dmt_name_lang']);
			$list[$k]['message_lang'] = unserialize($v['dmt_message_content_lang']);						
		}
		return $list;	
	}
	
	private function get_delivery_msg_tpl_info(){
		
		
		$where = array();
		$where['dmt_code'] = $_POST['code'];
		$msgtpl_info = Model('delivery_msg_tpl')->getDeliveryMsgTplInfo($where);
		if(!empty($msgtpl_info)){			
			
			$data = array();
			$data['ajax_type'] = "delivery";
			$data['code'] = $msgtpl_info['dmt_code'];
			$data['name'] = $msgtpl_info['dmt_name'];
			$data['message_switch'] = $msgtpl_info['dmt_message_switch'];
			$data['short_switch'] = $msgtpl_info['dmt_short_switch'];
			$data['mail_switch'] = $msgtpl_info['dmt_mail_switch'];
			$data['code_new'] = $msgtpl_info['dmt_code_new'];			
			$data['name_lang'] = unserialize($msgtpl_info['dmt_name_lang']);			
			$data['message_content_lang'] = unserialize($msgtpl_info['dmt_message_content_lang']);			
			
						
			return array('code'=> 200 ,'data'=> $data);
		}else{			
			return array('code'=> 400);
		}		
		
		
	}
	
	//保存
	private function save_delivery_tpl(){
		
		$where = array();
		$where['dmt_code'] = $_POST['code'];
		
		$update = array();
		$update['dmt_name_lang'] 			= serialize($_POST['name_lang']);
		$update['dmt_message_content_lang'] = serialize($_POST['message_content_lang']);
		$update['dmt_message_switch'] 		= $_POST['message_switch'];		
		$update['dmt_short_switch'] 		= $_POST['short_switch'];		
		$update['dmt_mail_switch'] 		= $_POST['mail_switch'];		
		
		$row = model('delivery_msg_tpl')->editDeliveryMsgTpl($where,$update);		
		if($row){
			return array('code' => 200);
		}else{
			return array('code' => 400);			
		}
	}

	
	
}
