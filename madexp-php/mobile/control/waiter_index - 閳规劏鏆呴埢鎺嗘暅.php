<?php
/**
 * 我的商城
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_indexControl extends mobileWaiterControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 骑手个人信息
     */
    public function indexOp() {
      
	
		
		$waiter_info = array();
        $waiter_info['nickname'] = $this->waiter_info['distributor_name'];
        $waiter_info['avatar'] = getMemberAvatarForID($this->waiter_info['distributor_avatar']);
		$waiter_info['distributor_code'] = $this->waiter_info['distributor_code'];
        $waiter_info['distributor_mobile'] = $this->waiter_info['distributor_mobile'];
		$waiter_info['online_state'] = $this->waiter_info['distributor_online'];
		$waiter_info['distributor_state'] = $this->waiter_info['distributor_status'];
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'file_type' => array('in','1,2,3'),
			'file_state' => 1
		);
		
		
		$waiter_file = model('waiter_file')->getWaiterFileList($where);
		$file =array();
		foreach($waiter_file as $v){
			$file[$v['file_type']]['file_name'] = $v['file_name'];
			$file[$v['file_type']]['file_end_time'] = date('m/d',$v['file_end_time']);
			$file[$v['file_type']]['file_code']= $v['file_code'];
			
		}
				
		//护照
		$waiter_info['passport'] = $file['1'];
		//签证
		$waiter_info['visa'] = $file['2'];		
		//驾照
		$waiter_info['driver'] = $file['3'];		
		//车辆信息
		$waiter_info['vehicle'] = '暂无';
		
		
		
        output_data(array('waiter_info' => $waiter_info));
    }
    
	
	//绑定stocket
	public function bindOp(){
		
		$stoket = model('stoket');
		$wid = $this->waiter_info['distributor_id'];
		$cid = $_POST['cid'];		
		$stoket->bind($cid,$wid);
		output_data('绑定成功');
		
	}
	
	
	//编辑头像
	public function editAvatarOp(){
		
		
		
		
		
		
		
	}
	
	
	
	//编辑昵称
	public function saveNickNameOp(){
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
	
		$data = array(
			'distributor_name' => $_POST['distributor_name']
		);		
		$update = $model_waiter->editWaiter($where,$data);
	
		if($update){
			output_data('编辑成功');					
		}else{
			output_error('保存失败');	
		}
		
		
	}
		
	
	//编辑手机号
	public function saveMobileOp(){
		
		$model_waiter = Model('waiter');
		$where =array('distributor_id'=>$this->waiter_info['distributor_id']);
		$data = array(
			'distributor_mobile' => $_POST['distributor_mobile']
		);		
		$update = $model_waiter->editWaiter($where,$data);
		if($update){
			output_data('1');					
		}else{
			output_error('保存失败');	
		}		
	}
	
	
	
	
	
	
    /**
     * 我的信息
     */
    public function myAssetOp() {
    
        $waiter_info = array();
      
		$waiter_info['today_order'] = 0; //今日订单
		$waiter_info['today_income'] = 0.00; //今日收入
		$waiter_info['today_online'] = 192; //今日在线时长 分钟
		
		$waiter_info['vehicle_num'] = 12;	//车辆信息气泡
		$waiter_info['evaluate_num'] = 22; //今日收到的评价气泡		
		
        output_data($waiter_info);
    }
	
	
	/*调试接口*/
	
	public function waiterStateTestOp(){
		
		$online_state = $_POST['state'];
		switch($online_state){		
			case 1: //下班
				
				output_data(array('state'=> 1,'msg'=>'下班成功'));	
				
			break;
			case 2: //上班
				output_error(array('state'=> 2,'msg'=>'未在城市内'));	
				
			break;	
			case 3: //上班
				
				output_error(array('state'=> 3,'msg'=>'没有车辆'));	
				
			break;				
			case 4: //上班
				
				output_error(array('state'=> 4,'msg'=>'未到达指定区域'));					
		
				
			break;	
				
			case 5: //上班				
				output_data(array('state'=> 5,'msg'=>'临时休息'));			
				
				
			break;	
				
			case 6: //上班				
				output_data(array('state'=> 6,'msg'=>'休息结束'));			
				
				
			break;	
				
				
		}
									   
	}
	
	
	
	
	//更改状态
	private function waiterOnline($state,$state_code){
			   
		
				$where = array(
					'distributor_id' => $this->waiter_info['distributor_id']
				);
				$data = array(
					'distributor_online' => $state
				);
				
				$row = $model->editWaiter($where,$data);
				
				if($row){					
					output_data(array('state_code'=>$state_code));
				}else{
					output_error('操作失败');
				}	
		
		
	}
	
	/*骑手状态*/
	
	public function waiterStateOp(){
						
		$model = model('distributor');
		$online_state = $_POST['state'];

		switch($online_state){	
		
			case 'offwork': //下班
						
				
				//是否有排班的下班状态修改
				
				if($this->waiter_info['scheduling_id'] > 0){
										
						//修改排班的下班状态
						$where = array(					
							'scheduling_id' => waiter_info['scheduling_id']					
						);		

						$updata = array(
							'scheduling_signout_time' => time(),
							'scheduling_signout_coordinate' => $points,					
							'scheduling_status' => 30
						);					
						model('scheduling')->editScheduling($where,$updata);	
					
					
						//修改当前骑手的状态					
						$where = array(
							'distributor_id' => $this->waiter_info['distributor_id']
						);
						$updata = array(
							'distributor_online' => 10,
							'scheduling_id' => 0
						);
					
						model('waiter')->editWaiter($where,$updata);
					//	$this->workHours(10);	
						output_data(array('state_code'=> 1 ,'msg'=>'已下班'));
					
					
				}else{					
										
						//修改当前骑手的状态					
						$where = array(
							'distributor_id' => $this->waiter_info['distributor_id']
						);
						$updata = array(
							'distributor_online' => 10,
							'scheduling_id' => 0
						);					
						model('waiter')->editWaiter($where,$updata);
					
						//$this->workHours(10);	
						output_data(array('state_code'=> 1 ,'msg'=>'已下班'));					
					
				}
				
			break;				
			case 'onwork' : 
				
			
				//上班				
				
				//查询是否有自己的车辆
				
				if($this->waiter_info['is_vehicle'] == 0){ //没有自己的车辆就查询租车情况					
					
					$where = array(
						'lease_state' => 30,
						'lease_content' => array('like','%'.$this->waiter_info['distributor_code'].'%')
					);
					
					$lease_count = model('vehicle_lease')->getLeaseCount($where);
					
					if($lease_count <= 0){
						output_error(array('state_code'=> 3,'msg'=>'您还没有车辆哦'));						
					}
					
					
				}
				
				
									
				//获取当前正在执行的排班状态，并上班
				
				$data = $this->scheduling();	
				
				if(!empty($data)){
					
					//查询是否已抵达上班地址
					$points = $_POST['points']; //获取当前骑手坐标
					
					
					if($points){						
						output_error(array('state_code'=> 9,'msg'=>'获取骑手坐标失败'));			
					}
					
					
					if($this->in_fences($data['region_id'],$points) == 0){						
						output_error(array('state_code'=> 4 ,'msg'=>'未抵达上班区域'));							
					}
					
					
					//修改骑手表上班状态
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 20,
						'scheduling_id' => $data['id'],
						'region' => $data['region_id'],
						'region_coordinate' => $points						
					);

					$row = model('waiter')->editWaiter($where,$updata);
					
					
					//修改排班的上班状态
					$where = array(					
						'scheduling_id' => $data['id']						
					);		
					
					$updata = array(
						'scheduling_sign_time' => time(),
						'scheduling_sign_coordinate' => $points,
						//'scheduling_sign_address' => '',
						'scheduling_status' => 20
					);					
					model('scheduling')->editScheduling($where,$updata);	
					
					if($row){
						
						output_data(array('state_code'=>5 , 'msg'=>'上班成功'));
						
					}
					
					
				}else{ //执行普通上班
					
					
					//获取坐标点。计算所在区域
					
					$points = $_POST['points']; //获取当前骑手坐标
					
					
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 20,				
						//'region' => $data['region_id'],
						'region_coordinate' => $points,
						'login_points' => $points
					);
					$row = model('waiter')->editWaiter($where,$updata);
					if($row){						
						output_data(array('state_code'=>5 , 'msg'=>'上班成功'));
					}
					
				}
				
				
				
				//$this->workHours(20);
				
				
				
			break;
				
			case 'onrest':  
				
				//开始休息	
				
				//$this->workHours(30);				
				
				//如果有排班的状态可以点击休息。
				if($this->waiter_info['scheduling_id'] > 0){					
					
					$where = array(
						'scheduling_id' => $this->waiter_info['scheduling_id']
					);
					
					$row = model('scheduling')->getSchedulingInfo($where);					
					
					if($row['rest_id'] == 0){
						//写入临时休息表
						$data =array(
							'scheduling_id' => $row['scheduling_id'],
							'log_start_time' => time(),
							'addtime' => time(),
							'distributor_id' => $this->waiter_info['distributor_id']
						);
						
						$log_id = model('scheduling_log')->addSchedulingLog($data);
						//更新当前这个
						model('scheduling')->editScheduling($where,array('rest_id'=>$log_id));
						//更新骑手状态
						model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>30));
						
						output_data(array('state_code'=> 6,'msg'=>'开始休息'));
						
					}
					
				}else{
					
					//非排班休息
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 30,	
					);
					$row = model('waiter')->editWaiter($where,$updata);
					
					
					
					
					output_data(array('state_code'=> 6,'msg'=>'开始休息'));
					
				}
				
				
				
				
				//休息
				
			break;	
			case 'offrest': //休息结束
			
				
				
				if($this->waiter_info['scheduling_id'] > 0){					
					$where = array(
						'scheduling_id' => $this->waiter_info['scheduling_id']
					);
					
					$row = model('scheduling')->getSchedulingInfo($where);					
					
					if($row['rest_id'] > 0){
					
						//修改临时休息表
						$data =array(
							'log_end_time' => time()
						);
						
						model('scheduling_log')->editSchedulingLog(array('rest_id'=>$row['rest_id']),$data);
						
						//更新当前这个
						model('scheduling')->editScheduling($where,array('rest_id'=>0));
						
						//更新骑手状态
						model('waiter')->editWaiter(array('distributor_id'=> $this->waiter_info['distributor_id']),array('distributor_online'=>20));
						
						output_data(array('state_code'=> 7,'msg'=>'休息结束'));
						
					}
					
				}else{
					
					//非排班休息
					
					
					$where = array(
						'distributor_id' => $this->waiter_info['distributor_id']
					);
					
					$updata = array(
						'distributor_online' => 20,	
					);
					$row = model('waiter')->editWaiter($where,$updata);
					
										
					output_data(array('state_code'=> 7,'msg'=>'休息结束'));
					
					
				}
				
				
				

				
				
				
				
			break;		
		}
				
		
		
		
				
		
	}
	
	
	
	//检查是否有排班
	private function scheduling(){		
		
		$day = strtotime(date('Y-m-d',time()));
		$time = time();			
		$where = array(
			'scheduling_date' => $day
		);	
		$data = model('scheduling')->getSchedulingList($where);				
		$result = array();
		foreach($data as $v){			
			if($v['scheduling_start_time'] < $time && $v['scheduling_end_time'] > $time && $v['scheduling_status'] == 10 ){			
				$result['id'] = $v['scheduling_id'];
				$result['region_id'] = $v['region_id'];
			}			
		}
	
		return $result;		
		
	}
	
	//更新排班休息记录
	private function restlog($scheduling_id){
		
		$model = model('scheduling_log');
		
		$info = '';
				
		
	}
	
	//更新上下班记录,统计,工时等
	private function workHours($state){
		
		$model = model('waiter_workhours');		
		
		if($state == 10){			
			$data = array();			
			$data['work_end_time'] = time();
			$where = array(
				'work_id' => $this->waiter_info['work_id']
			);
			$row = $model->editWorkhours($where,$data);
			if($row){				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				$data = array(
					'distributor_online' => 10,
					'work_id' => 0
				);
				model('waiter')->editWaiter($where,$data);		
			}
		}
			
		
		if($state == 20){
			
			$data = array();
			$data['distributor_id'] = $this->waiter_info['distributor_id'];
			$data['work_state_time'] = time();
			$data['addtime'] = time();
			$data['work_days'] = date('Y-m-d',time());
			$row = $model->addWorkhours($data);
			if($row){				
				$where = array(
					'distributor_id' =>  $this->waiter_info['distributor_id']					
				);
				$data = array(
					'distributor_online' => 20,
					'work_id' => $row
				);
				model('waiter')->editWaiter($where,$data);				
			}
			
		}
		
		if($state == 30){
			
			$data = array();
			$data['distributor_id'] = $this->waiter_info['distributor_id'];			
			$data['rest_state_time'] = time();			
			$data['addtime'] = time();
			$data['rest_days'] = date('Y-m-d',time());
			model('waiter_rest')->editWaiter($where,$data);		
			
			
			
			
		}
		
				
	}
	
	
	
	
	/**
	 * @name 围栏算法，判断一个坐标，是否在围栏里面.如：['113.664673,34.810146','113.681667,34.796896','113.69231,34.794711','113.702009,34.809159']
	 * @author macnie <mac@lenmy.com>
	 * @param array $fences 围栏，是一组坐标数组 如：113.674458,34.804719
	 * @param string $point
	 * @return bool
	 */
	
	
	
	

	private function in_fences($region_id,$point) {
	
		$region = model('region')->getRegionInfo(array('region_id'=> $region_id));		
		$fences = explode('|',$region['region_coordinate']);
		
			
		$point = $point;
		
		
		$nvert = count($fences);
	
		$vertx = [];
		$verty = [];
		list($testy, $testx) = explode(',', $point);
		foreach ($fences as $r) {
			list($lng, $lat) = explode(',', $r);
			$vertx[] = $lat;
			$verty[] = $lng;
		}
		$i = $j = $c = 0;
		for ($i = 0, $j = $nvert - 1; $i < $nvert; $j = $i++) {
			if (( ($verty[$i] > $testy) != ($verty[$j] > $testy) ) &&
				($testx < ($vertx[$j] - $vertx[$i]) * ($testy - $verty[$i]) / ($verty[$j] - $verty[$i]) + $vertx[$i]))
				$c = !$c;
		}
		
		return $c;
	}
	
	
	
	
	
	
}
