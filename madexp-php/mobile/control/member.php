<?php
/**
 * 会员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class memberControl extends mobileAdminControl{
    const EXPORT_SIZE = 1000;

	public function __construct(){
        parent::__construct();   
    }

	private function checkMemberName($memberName)
	{
	    // 判断store_joinin是否存在记录
	    $count = (int) Model('member')->getMemberCount(array(
	        'member_name' => $memberName,
	    ));
	    if ($count > 0){
	        return false;
		}
	    return true;
	}
	
	public function member_saveOp(){
		
		
			$model = model('member');
			$member_name = $_POST['member_name'];
			
			if (!$this->checkMemberName($member_name)){
				output_error('账号已被占用');				
			}
				
			$data = array(			
					
				'member_name' => $_POST['member_name'],
				'member_code' => $_POST['member_name'],
				'member_passwd' => md5($_POST['member_passwd']),
				'member_truename' => $_POST['member_truename'],
				'member_mobile' => $_POST['member_mobile'],
				'member_time'=>time(),
				
			);
				
			$store_id = $model->addMember($data);
			if($store_id){
					output_data(array('store_id'=>$store_id));
			}else{
					output_error('操作失败');
	      	}
		
	}

	public function member_listOp(){
			
			
		
		$model_member = Model('member');
        $condition = array();
		
		if(is_array($_POST['search_ing'])){
			$condition['order_count'] = array('between',$_POST['search_ing']);
		}
		if(is_array($_POST['search_complate'])){
			$condition['order_week_svg'] = array('between',$_POST['search_complate']);
		}
		if(is_array($_POST['search_cancel'])){
			$condition['order_week_price'] = array('between',$_POST['search_cancel']);
		}
		
        if ($_POST['query'] != '') {
            $condition[$_POST['qtype']] = array('like', '%' . $_POST['query'] . '%');
        }		
		if ($_POST['keyword'] != '') {         
			$condition = array(
				'member_name' => array('like', '%' . $_POST['keyword'] . '%'), 
				'member_truename'=> array('like', '%' . $_POST['keyword'] . '%'), 
				'member_code'=> array('like', '%' . $_POST['keyword'] . '%'),
				'member_email'=> array('like', '%' . $_POST['keyword'] . '%'), 
				'_op' => 'or'
			);
        }			
		
        $order = 'member_id desc';			
        $param = array('member_id','member_name','order_count','order_week_svg','order_week_price','order_last_time');
        
		if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
		        	
        $member_list = $model_member->getMemberList($condition, '*', $this->page, $order);		      
		
		$list = array();		
		
        foreach ($member_list as $value) {
			
            $param = array();					
			$param['member_id'] = $value['member_id'];
            $param['member_code'] = $value['member_code'];
            $param['member_truename'] = ($value['member_truename']=='' ? '--': $value['member_truename']);
            $param['member_mobile'] = $value['member_mobile'];
		    $param['member_email'] = $value['member_email'];	
            $param['order_count'] =  $value['order_count'];
            $param['order_week_svg'] = $value['order_week_svg'];
            $param['order_week_price'] = $value['order_week_price'];
            $param['order_last_time'] =   $value['order_last_time'] > 0 ? date('Y-m-d', $value['order_last_time']) : 'N/A';
			$param['member_time'] = date('d M y H:i:s',$value['member_time']);
			$list[] = $param;
        }		
		
		$page_count = $model_member->gettotalpage();
		$list_count = $model_member->gettotalnum();
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
	
		
		
	}
	
	
	//用户详情
	public function member_infoOp(){
		
		
		$member_data  =  array();
		$member_id = intval($_POST['id']);
		
		
		if(empty($member_id)){
			output_error('参数错误');
		}	
		
		$where  = array(
			'member_id' => $member_id 
		);		
		
		$member_info = model('member')->getMemberInfo($where);
		
				
		//加载全部地址属性		
		$address_attr = $this->address_attr();		
		
		$member_data['address_attr'] = $address_attr;
	
		$consignee_attr = $this->consignee_attr();		
	
		$member_data['consignee_attr'] = $consignee_attr;
		
		$region = $this->region();
	
	
		//用户基础信息
		$info = array();
		$info['member_id'] = $member_info['member_id'];
		$info['member_time'] = date('Y-m-d H:i:s',$member_info['member_time']);
		$info['member_avatar'] = getMemberAvatarForID($member_info['member_id']);
		$info['member_attr'] =$consignee_attr[$member_info['member_attr']]['attr_name'];		
		$info['member_code'] = $member_info['member_code'];
		$info['member_name'] = $member_info['member_name'];
		$info['member_truename'] = $member_info['member_truename'];
		$info['member_mobile'] = $member_info['member_mobile'];
		$info['member_wallet'] = $member_info['available_predeposit'];
		
		
		$member_data['info'] = $info;
		



	
		//获取收货人
		$consignee_list = model('consignee')->getConsigneeList($where);	
		$member_data['consignee_list'] = $consignee_list;
	
	
	
		//获取收货地址
		$address_list = model('address')->getAddressList($where);			
		$member_data['address_list'] = $address_list;
	
	
	
	
		$bank_list = model('member_bank')->getMemberBankList($where);		
		$bank_data = array();
		foreach($bank_list as $k=> $v){
			$bank_data[$k] =$v;
			$bank_data[$k]['bank_code'] =  '**** **** **** '.substr($v['bank_code'],-4);			
		}		
	
		$member_data['bank_list'] = $bank_data;		
		$member_bind_class = model('member_bind_class')->getMemberBindClassList($where);		
	
		$member_data['bind_class'] = $member_bind_class;
	
		output_data($member_data);
		
	}
	
	
	
	
	/*
	用户的优惠券
	*/
	public function member_couponOp(){
		
		$model_redpacket = Model('redpacket');
		$state = array(
			1=> '未使用',
			2=> '已使用',
			3=> '已过期'
		);
		$sort_fields = array('rpacket_code','rpacket_title','rpacket_start_date','rpacket_state','rpacket_active_date');
		if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
		    $order = $_POST['sortname'].' '.$_POST['sortorder'];
		}		
		if ($_POST['keyword'] != '') {
		  //  $condition['member_name'] = array('like', '%' . $_POST['member_name'] . '%');			
			$where['rpacket_code'] = array('like', '%' . $_POST['keyword'] . '%');
		}	
		
		if($_POST['state'] != ''){
			$where['rpacket_state'] = $_POST['state'];
		}
		
		$where['rpacket_owner_id'] = $_POST['member_id'];		
		$data = $model_redpacket->getRedpacketList($where, '*', 0,$this->page, $order);
			
		foreach ($data as $val) {
		   	$i=array();	
			$i['rpacket_t_id'] = $val['rpacket_t_id'];            	
		    $i['rpacket_code'] = $val['rpacket_code'];                  
		    $i['rpacket_title'] = $val['rpacket_title'];                  
		    $i['rpacket_active_date'] = date('Y-m-d H:i', $val['rpacket_active_date']);                  
		    $i['rpacket_state'] = $state[$val['rpacket_state']];
			$i['rpacket_order_id'] = $val['rpacket_order_id'] == 0 ? 'N/A' :  $val['rpacket_order_id']; 
		    $i['rpacket_active_datetext'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_active_date']):'';
			$platform_list[] = $i;
		}
	
	
			$page_count = $model_redpacket->gettotalpage();
			$list_count = $model_redpacket->gettotalnum();
		
			output_data(array('platform_list' => $platform_list), mobile_page($page_count,$list_count));
			
	}
	
	
	
	
	
	//我的收藏	
	public function  member_likeOp(){		
		$model_favorites = Model('favorites');		
		$where =array();		
		$state = array(
			0 => '下架',
			1 => '上线',
			2 => '下线'
		);		
		$order = '';		
		$sort_fields = array('store_id','store_name','store_score','store_day_avg');      
		if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
		    $order = $_POST['sortname'].' '.$_POST['sortorder'];
		}
		
		if ($_POST['keyword'] != '') {         
		  //  $condition['member_name'] = array('like', '%' . $_POST['member_name'] . '%');			
			$where = array('store_name' => array('like', '%' . $_POST['keyword'] . '%'));
		}			
		$where['member_id'] = $_POST['member_id'];				
		$data = $model_favorites->getFavoritesList($where, '*', $this->page,$order);	
		$list = array();		
		foreach ($data as $value) {			
			$store = $this->getStoreInfo($value['store_id']);				
		   	$i=array();	
		    $i['store_id'] = $value['store_id'];            
			$i['store_code'] = $store['store_code'];
		    $i['store_name_primary'] = $store['store_name_primary'];     
			$i['store_name_secondary'] = $store['store_name_secondary'];     
			$i['store_region'] = $store['region_name'];  
		    $i['store_class'] = $this->store_bind_class($value['store_id']);     
			$i['store_phone'] = $store['store_phone'];   
			$i['store_day_avg'] = $store['store_day_avg'];     
			$i['store_score'] = $store['store_score'];  
			$i['store_state'] = $state[$store['store_state']]; 						
			$list[] = $i;
		}
		
		if($_POST['sort'] != ''){
			$sort = $_POST['sort'];
			$sorttype = $_POST['sorttype'];
			switch($sort){
				case 'store_score':
					if($_POST['sorttype'] =='desc'){
						array_multisort(array_column($list,'store_score'),SORT_DESC,$list);
					}else{
						array_multisort(array_column($list,'store_score'),SORT_ASC,$list);
					}
					
				break;						
				case 'store_day_avg': 	
					if($_POST['sorttype'] =='desc'){
						array_multisort(array_column($list,'store_day_avg'),SORT_DESC,$list);					
					}else{
						array_multisort(array_column($list,'store_day_avg'),SORT_ASC,$list);						
					}
				break;					
			}
		}
		
		
		$page_count = $model_favorites->gettotalpage();
		$list_count = $model_favorites->gettotalnum();
				
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
		
		
	}
	
	
	
	//反馈记录
	public function member_feedbackOp(){
		
	
		$model_feedback = Model('feedback');
		// 设置页码参数名称        
	   if ($_POST['keyword'] != '') {         
		  //  $condition['member_name'] = array('like', '%' . $_POST['member_name'] . '%');			
			$condition = array('feedback_code' => array('like', '%' . $_POST['keyword'] . '%'));
	    }	
		
		$condition['user_id'] = $_POST['member_id'];        
		$condition['user_type'] = 1;  
	    $order = 'feedback_time desc';
	    $param = array('id','feedback_time','last_reply_time');
	    if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
	        $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
	    }	   
	   	$usertype = array(
		    0=>'客户',
			1=>'商户',
			2=>'配送员'
		);
	   
	   	$state = array(
		    0=>'待处理',
			1=>'处理中',
			2=>'已完成'
		);
	
	     if($_POST['feedback_type']){			
			$condition['feedback_type'] = $_POST['feedback_type'];
	     }
	
	     if($_POST['choose_date']){
			 if(strlen($_POST['choose_date'])==10){
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400  ));//日
				if($_POST['choose_week_end']){//周
					$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']),strtotime($_POST['choose_date'])+86400*7  ));
				}
			 }else if(strlen($_POST['choose_date'])==7){//月
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01"),strtotime($_POST['choose_date']."-31")  ));
			 }else if(strlen($_POST['choose_date'])==4){//年
				$condition['feedback_time'] = array('between',array( strtotime($_POST['choose_date']."-01-01"),strtotime($_POST['choose_date']."-12-31")  ));
			 }
	     }
		 
		$feedback_list = $model_feedback->getFeedBackList($condition, '*', $this->page ,$order);
		
		$data = array();
		$list = array();
		   
	    foreach ($feedback_list as $value) {
	        $param = array();			  
			$param['feedback_id'] = $value['id']; 
			$param['feedback_code'] = $value['feedback_code']; 
			$param['feedback_type'] = $value['feedback_type']; 
			$param['feedback_content'] = $value['feedback_content'];  			
			$param['feedback_time'] = date('Y-m-d H:i:s',$value['feedback_time']);
	        $param['last_reply_time'] = $value['last_reply_time'] > 0 ?  date('Y-m-d H:i:s',$value['last_reply_time']) : 'N/A';			
			$param['feedback_state'] =  $state[$value['feedback_state']];			  
	        $list[] = $param;
			
	    }
		
			

		$arr = array('投诉配送员','投诉商家','意见反馈','配送问题','其他');
		foreach($arr as $v){
			$filterTypeData[] = array(
				'text' => $v,
				'value' => $v
			);
		}
		
		$arr = array('待处理','处理中','已完成');
		foreach($arr as $v){
			$filterStateData[] = array(
				'text' => $v,
				'value' => $v
			);
		}
		
		$page_count = $model_feedback->gettotalpage();
		$list_count = $model_feedback->gettotalnum();
				
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
		
		
		
		
	
	}
	
	
	
	
	//用户订单
	
	public function member_orderOp(){
		
		$model_order = Model('order');
		$sort_fields = array('order_sn','store_name','buyer_name','goods_amount','delivery_fee','sales_amount','order_amount','distributor_end_time');
		if ($_POST['sortorder'] != '' && in_array($_POST['sortname'],$sort_fields)) {
		    $order = $_POST['sortname'].' '.$_POST['sortorder'];
		}
		if ($_POST['keyword'] != '') {         
		  //  $condition['member_name'] = array('like', '%' . $_POST['member_name'] . '%');			
			$condition['order_sn'] = array('like', '%' . $_POST['keyword'] . '%');
		}	
		$condition['buyer_id'] =  $_POST['member_id'];	
		
		if(is_array($_POST['goods_amount'])){
			$condition['goods_amount'] = array('between',$_POST['goods_amount']);
		}
		if(is_array($_POST['delivery_fee'])){
			$condition['delivery_fee'] = array('between',$_POST['delivery_fee']);
		}
		if(is_array($_POST['sales_amount'])){
			$condition['sales_amount'] = array('between',$_POST['sales_amount']);
		}
		if(is_array($_POST['order_amount'])){
			$condition['order_amount'] = array('between',$_POST['order_amount']);
		}
		
		$order_list = $model_order->getOrderList($condition,$this->page,'*',$order);	
		
		$list = array(); 
		foreach ($order_list as $order_id => $order_info) {
			$data = array();
		    $data['order_sn'] = $order_info['order_sn'];       
		   	$data['store_name'] = $order_info['store_name'];
			$data['region_name'] = $order_info['store_region_name'] ;			
			$data['region_color'] = $order_info['store_region_color'] ;			
			$data['buyer_name'] = $order_info['buyer_name'];
			$data['goods_amount'] = ncPriceFormat($order_info['goods_amount']);
			$data['delivery_fee'] = ncPriceFormat($order_info['delivery_fee']);
			$data['sales_amount'] = ncPriceFormat($order_info['sales_amount']);
			$data['order_amount'] = ncPriceFormat($order_info['order_amount']);
			$data['distributor_end_time'] = $order_info['distributor_end_time'] > 0 ? date('Y-m-d H:i:s',$order_info['distributor_end_time']) : 'N/A';
			$list[] = $data;
		}	
		
		$page_count = $model_order->gettotalpage();
		$list_count = $model_order->gettotalnum();				
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
					
	}
	
	
	//订单统计
	public function get_order_statOp(){
		
		$member_id = $_POST['member_id'];
		
		$data =array();
		$data['order_stat_count'] = $this->state_count($member_id);
		$data['week_stat_count'] = 0;
		$data['order_ave'] = 0;
		$data['order_gross_profit'] = 0;
		
		output_data(array('stat' => $data));
	}
	
	
	private function state_count($member_id)
	{
	    $model = Model('order');
	    $where = array();
		$where['buyer_id'] = $member_id;
	    $count = $model->getOrderCount($where);
	    return $count;
	}
	
	
	
	
	
	
	//店铺绑定的子类
	private function store_bind_class($store_id){
		$where = array(
			'store_id' =>  $store_id
		);
		$bind_class = model('store_bind_class')->getStoreBindClassList($where);
		$list = '';
		foreach($bind_class as $v){
			$list .= $v['class_name'].'·';
		}
		return $list;
		
	}
	
	
	private function member_favorites($member_id){
		
		$model_fav = Model('favorites');
		$condition  = array(
			'member_id' => $member_id,
			'fav_type' => 'store'
		);		
		$fav_list=$model_fav->getFavoritesList($condition);		
		foreach($fav_list as $k=> $v){			
			$data[$k] = $v;
			$data[$k]['store'] = $this->getStoreInfo($v['store_id']);
		}
		Tpl::output('fav_list',$data);
		
		
	}
	
	private function getStoreInfo($store_id){		
		$where = array(
			'store_id' => $store_id
		);
		$row = model('store')->getStoreInfo($where);		
		return $row;	
	}
	
	private function address_attr(){		
		$list = model('address_attr')->getAddressAttrList(TRUE);	
		$data = array();
		foreach($list as $v){
			$data[$v['attr_id']] = $v;			
		}
		return $data;
		
	}
	
	private function consignee_attr(){
		$data = array();
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);	
		foreach($list as $v){
			$data[$v['attr_id']] = $v;			
		}		
		return $data;
	}
	
	private function region(){		
		$data = array();
		$list = model('region')->getRegionList(TRUE,'region_id,region_name,region_color');	
		foreach($list as $v){
			$data[$v['region_id']] = $v;			
		}		
		return $data;
		
	}
	
	
}
