<?php
/**
 * 会员管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class deliveryControl extends mobileAdminControl
{
    const EXPORT_SIZE = 1000;

    public function __construct()
    {
        parent::__construct();
    }


    //配送员统计
    public function delivery_statOp()
    {
        $stat = array();
        
        //总人数
        $stat['delivery_total'] = model('waiter')->getWaiterCount(true);
		
        //列表人数
        $stat['delivery_total_list'] = model('waiter')->getWaiterCount(array('distributor_status'=>array('in','1,2,3')));
		
        //列表人数 审核
        $stat['delivery_total_check'] = model('waiter')->getWaiterCount(array('distributor_status'=>array('in','0,5,6')));
        
        //在线人数
        $stat['delivery_online_total'] = model('waiter')->getWaiterCount(array('distributor_online'=>array('in','20,30')));
        
        //均送餐金额
        $stat['delivery_mean_fee'] = 0;
        
        //均时长单
        $stat['delivery_mean_time'] = 0;
        
        //均距离
        $stat['delivery_mean_distance'] = 0;
        
        //均到店
        $stat['delivery_arrival_store'] = 0;
        
        //均配送
        $stat['delivery_arrival_user'] = 0;
            
        //均完成时间
        $stat['delivery_success'] = 0;
                
        output_data(array('stat' => $stat));
    }



    public function delivery_listOp()
    {
        if (!$_POST['state_type']) {
            $_POST['state_type'] = 'state_all';
        }
        
        $model_waiter = Model('waiter');
        $condition = '1=1 ';
        if ($_POST['keyword'] != '') {
            $condition .= " and distributor_name like '%" . $_POST['keyword'] . "%'";
            $condition .= " or distributor_truename like '%" . $_POST['keyword'] ."%'";
            $condition .= " or distributor_code like '%" . $_POST['keyword'] . "%'";
            $condition .= " or distributor_mobile like '%" . $_POST['keyword'] . "%'";
            
            //array('distributor_name' => array('like', '%' . $_POST['keyword'] . '%'), 'distributor_truename'=> array('like', '%' . $_POST['keyword'] . '%'), 'distributor_code'=> array('like', '%' . $_POST['keyword'] . '%'),'distributor_mobile'=> array('like', '%' . $_POST['keyword'] . '%'), '_op' => 'or');
        }
               
        $order = '';
        $param = array('distributor_id','order_state_take','order_state_send','order_state_success','order_state_canal','order_state_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        
        if ($_POST['state_type'] != '') {
            if ($_POST['state_type'] == 'state_all') {
                $condition .= ' and distributor_status in (1,2,3)';
            }
            if ($_POST['state_type'] == 'state_examine') {
                $condition .= ' and distributor_status in (0,5,6)';
                // $condition['distributor_status'] = array('in','0,2');
            }
            if ($_POST['state_type'] == 'state_doubt') {
                $condition .= ' and distributor_status in (4)';
                // $condition['distributor_status'] = array('in','3');
            }
        }
        if ($_POST['distributor_status'] != '') {
            $condition .= ' and distributor_status = '.$_POST['distributor_status'];
        }
		
		
		if(is_array($_POST['take_num'])){				
			$condition .= ' and order_state_take between '.$_POST['take_num'][0] ." and ".$_POST['take_num'][1];
		}
		if(is_array($_POST['send_num'])){
			$condition .= ' and order_state_send between '.$_POST['send_num'][0] ." and ".$_POST['send_num'][1];
		}
		if(is_array($_POST['success_num'])){
			$condition .= ' and order_state_success between '.$_POST['success_num'][0] ." and ".$_POST['success_num'][1];
		}
 		if(is_array($_POST['cancel_num'])){
			$condition .= ' and order_state_canal between '.$_POST['cancel_num'][0] ." and ".$_POST['cancel_num'][1];
		}
 		if(is_array($_POST['time_num'])){
			$condition .= ' and order_state_time between '.$_POST['time_num'][0] ." and ".$_POST['time_num'][1];
		}
        
        
        
    
        $waiter_list = $model_waiter->getWaiterList($condition, '*', $this->page, $order);
        $page_count = $model_waiter->gettotalpage();
        $list_count = $model_waiter->gettotalnum();
            
        $state_array = $this->waiter_state();
        $list = array();
        
        foreach ($waiter_list as $value) {
            $param = '';
            $param['distributor_state'] = $state_array[$value['distributor_status']];
            $param['distributor_state_code'] = 	$value['distributor_status'];
            $param['distributor_id'] = $value['distributor_id'];
            $param['distributor_avatar'] = getWaiterAvatar($value['distributor_avatar']);
            $param['distributor_code'] = $value['distributor_code'];
            $param['distributor_name'] = $value['distributor_name'];
            $param['region'] = '';
            $param['order_state_take'] = $value['order_state_take'];
            $param['order_state_send'] = $value['order_state_send'];
            $param['order_state_success'] = $value['order_state_success'];
            $param['order_state_canal'] = $value['order_state_canal'];
            $param['order_state_time'] = $value['order_state_time']; //均是单
            $param['waiter_region'] = 0; //均是单
            $param['evaluate_rate'] = $value['evaluate_rate'] . '%';
            $param['region_color'] = $value['region_color'];
            $param['region_name'] = $value['region_name'];
            $param['region_id'] = $value['region_id'];
            $list[] = $param;
        }
                    
        $arr = array('待审核','OnlineS','Manual','Offline','Cage','待面试');
        foreach ($arr as $v) {
            $waiterState[] = array(
                'text' => $v,
                'value' => $v
            );
        }
            
        output_data(array('list' => $list), mobile_page($page_count, $list_count));
    }
    
    //搜索框搜索配送员
    
    public function delivery_search_listOp()
    {
        $condition = array();
        $condition['distributor_name|distributor_code|distributor_truename|distributor_mobile'] = array('LIKE', '%'.$_POST['keyword'].'%');
        $condition['distributor_online'] = 20;
            
        $waiter_list = Model('waiter')->getWaiterList($condition);
        foreach ($waiter_list as $v) {
            $param = array();
            $param['distributor_id'] = $v['distributor_id'];
            $param['distributor_avatar'] = getWaiterAvatar($v['distributor_avatar']);
            $param['distributor_code'] = $v['distributor_code'];
            $param['distributor_name'] = $v['distributor_name'];
            $list[] = $param;
        }
            
        output_data(array('list' => $list));
    }
    
    
    //配送员详情
    public function delivery_infoOp()
    {
        $where = array(
            'distributor_id' =>intval($_POST['delivery_id'])
        );
        $delivery_info = model('waiter')->getWaiterInfo($where);
        $delivery = array();
        
        $delivery['distributor_name']  = $delivery_info['distributor_name'];
        $delivery['distributor_truename']  = $delivery_info['distributor_truename'];
        $delivery['distributor_code'] = $delivery_info['distributor_code'];
        $delivery['avatar'] = getWaiterAvatar($delivery_info['distributor_avatar']);
        
        //自有车辆
        $delivery['vehicle']  = array(
            'vehicle_number' 	=> $delivery_info['vehicle_number'],
            'vehicle_type' 		=> $delivery_info['vehicle_type'],
            'vehicle_brand' 	=> $delivery_info['vehicle_brand'],
            'is_vehicle' 		=> $delivery_info['is_vehicle']
        );
        
        $delivery['distributor_mobile'] = $delivery_info['distributor_mobile'];// 联系手机
        $delivery['evaluate_rate'] = $delivery_info['evaluate_rate'];// 侧重区域
        $delivery['evaluate_rate'] = $delivery_info['evaluate_rate'];// 好评
        $delivery['addtime'] = date('d M y H:i:s', $delivery_info['addtime']); //注册时间
        $delivery['order_last'] = $delivery_info['order_last'] > 0 ? date(' d M y H:i:s', $delivery_info['order_last']) : 'N/A';//最后接单
        
        $file_list= $this->delivery_file($delivery_info['distributor_id']);
                
        output_data(array('delivery_info'=>$delivery,'file_list' => $file_list ));
    }
    
    
    //保存基础编辑
    public function delivery_base_saveOp()
    {
        $model_waiter = Model('waiter');
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
                array("input" => $_POST["distributor_mobile"], "require" => "true", "message" => '请填写配送员手机'),
                array("input" => $_POST["distributor_name"], "require" => "true",  "message" => '请填写配送员名称'),
                array(
                    "input" => $_POST["distributor_name"],
                    "max" => "20",
                    "min" => "3",
                    "validator" => "length",
                    "message" => "配送员名称长度要在6~20个字符"
                ),
                
                
            );
        $error = $obj_validate->validate();
        if ($error != '') {
            output_error($error);
        } else {
            $update_array = array();
            $update_array['distributor_name'] = $_POST['distributor_name'];
            $update_array['distributor_mobile'] = $_POST['distributor_mobile'];
            $update_array['distributor_avatar'] = $_POST['distributor_avatar'];
            $result = $model_waiter->editWaiter(array('distributor_id' => intval($_POST['distributor_id'])), $update_array);
            if ($result) {
                output_data('保存成功');
            } else {
                output_error('保存失败');
            }
        }
    }
    
    
    
    private function delivery_file($waiter_id)
    {
        $model_file = model('waiter_file');
        $condition = array();
        $condition['distributor_id'] = $waiter_id;
        if ($_POST['file_type'] != '') {
            $condition['file_type'] = $_POST['file_type'];
        }
        
        $param = array('file_id','file_end_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
        
        
        $file_list = $model_file->getWaiterFileList($condition, '*', 50, $order);
        
        $list = array();
        foreach ($file_list as $k => $v) {
            $list[$k] = $v;
            $list[$k]['addtime'] = date('d/m/y', $v['addtime']);
            $list[$k]['state_time'] = date('d/m/y', $v['file_state_time']);
            $list[$k]['end_time'] = date('d/m/y', $v['file_end_time']);
			$list[$k]['file_url'] = $v['file_url'] ? 'http://'.$_SERVER['SERVER_NAME'].$v['file_url'] : "";
			
        }
        return $list;
    }
    
    

    
    
    
    /*
    订单列表
    */
    
    public function delivery_orderOp()
    {
        $model_order = Model('order');
        $condition = array(
                'distributor_id' => $_POST['delivery_id']
            );
            
        $order = '';
        $sort_fields = array('order_sn','store_name','buyer_name','goods_amount','delivery_fee','sales_amount','order_amount','distributor_end_time');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'], $sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
            
        if ($_POST['keyword'] != '') {
            $condition['order_sn'] = $_POST['keyword'];
        }
            
        if ($_POST['order_state'] != '') {
            $condition['order_state'] = $_POST['order_state'];
        }
		
		
		    
        if(is_array($_POST['delivery_fee'])){				
			    $condition['delivery_fee'] = array('between',$_POST['delivery_fee']);
		}
		if(is_array($_POST['order_amount'])){
			  $condition['order_amount'] = array('between',$_POST['order_amount']);
		}
		if(is_array($_POST['sales_amount'])){
			 $condition['sales_amount'] = array('between',$_POST['sales_amount']);
		}
		
                                    
        $order_list = $model_order->getOrderList($condition, $this->page, '*', $order);
        $data = array();
                    
        foreach ($order_list as $order_id => $order_info) {
            $list = array();
            $list['order_id'] = $order_info['order_id'];
            $list['order_sn'] = $order_info['order_sn'];
            $list['add_times'] = date('i"s"', $order_info['add_time']);
            $list['order_amount'] = ncPriceFormat($order_info['order_amount']);
            $list['delivery_fee'] = ncPriceFormat($order_info['delivery_fee']);
            $list['sales_amount'] = ncPriceFormat($order_info['sales_amount']);
            $list['goods_amount'] = ncPriceFormat($order_info['goods_amount']);
            $list['order_state'] = $order_info['state_desc'];
            $list['buyer_id'] = $order_info['buyer_id'];
            $list['buyer_name'] = $order_info['buyer_name'];
                
            $list['store_id'] = $order_info['store_id'];
                
            $list['store_name'] = $order_info['store_name'];
            $list['send_time'] = date('d/m/y H:i:s', $order_info['distributor_end_time']);
            $data[] = $list;
        }
                    
        $page_count = $model_order->gettotalpage();
        $list_count = $model_order->gettotalnum();
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
    }
    
    
    
    
    /*租赁*/
    public function delivery_leaseOp()
    {
        $model_vehicle = model('vehicle_lease');
        $where = array();
        $where['distributor_id'] = $_POST['delivery_id'];
        
        //$where['lease_id'] =   array('in',);
        $order = '';
        $sort_fields = array('order_sn','lease_start_time','lease_end_time','lease_days','lease_state');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'], $sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
        if ($_POST['keyword'] != '') {
            //  $condition['member_name'] = array('like', '%' . $_POST['member_name'] . '%');
            $where['lease_sn'] = array('like', '%' . $_POST['keyword'] . '%');
        }
                
        $list = $model_vehicle->getLeaseList($where, '*', 10, $order);
        
        $page_count = $model_vehicle->gettotalpage();
        $list_count = $model_vehicle->gettotalnum();
    
        $state = array(
            0 => '待支付',
            10 => '待确认',
            20 => '待取车',
            30 => '进行中',
            40 => '待付款',
            50 => '已完成',
            60 => '已评价',
            70 => '无效'
        );
                        
        $data =array();
        foreach ($list as $k => $v) {
            $list[$k] = $v;
            $list[$k]['cotenant'] =  $this->_getWaiterCotenant($v['lease_id']);
            $list[$k]['lease_start_time'] = date('d/m/y', $v['lease_start_time']);
            $list[$k]['lease_end_time']  = date('d/m/y', $v['lease_end_time']);
            $list[$k]['lease_state']  = $state[$v['lease_state']];
        }
                
                
        output_data(array('list' => $list), mobile_page($page_count, $list_count));
    }
    
    
    
    private function _getWaiterCotenant($lease_id)
    {
        $lease_list = Model('vehicle_lease_cotenant')->getLeaseCotenantList(array('lease_id'=>$lease_id), 'lease_id,distributor_name', 100);
        $lease_id ='';
        foreach ($lease_list as $v) {
            $waiter_name .= $v['distributor_name'].',';
        }
        
        return $waiter_name;
    }
    
    
    
    /*排班*/
    public function delivery_shedulingOp()
    {
        $model =  model('scheduling');
        $state = array(
             10 => '待上班',
             20 => '进行中',
             30 => '已完成',
             40 => '已取消',
             50 => '排班未达标',
             60 => '迟到'
        );
        
        
        $where = array(
            'distributor_id' => $_POST['delivery_id'],
            'scheduling_status' => array('in','10,20')
        );
        
        $order = '';
        $sort_fields = array('scheduling_id','scheduling_date');
        if ($_POST['sortorder1'] != '' && in_array($_POST['sortname1'], $sort_fields)) {
            $order = $_POST['sortname1'].' '.$_POST['sortorder1'];
        }
        
        $new_sch = $model->getSchedulingList($where, 5, $order);
        $data_sch =array();
        foreach ($new_sch as  $k => $v) {
            $data_sch[$k] = $v;
            $data_sch[$k]['scheduling_date'] = date('d/m/y', $v['scheduling_date']);
            $data_sch[$k]['scheduling_work'] = $this->work($v['scheduling_date']);
            $data_sch[$k]['region'] = $region[$v['region_id']];
            $data_sch[$k]['state'] =  $state[$v['scheduling_status']];
        }
        
        
        
        $where = array(
            'distributor_id' => $_POST['delivery_id'],
            'scheduling_status' => array('in','30,40,50')
        );
        
        $order = '';
        $sort_fields = array('scheduling_id','scheduling_date');
        if ($_POST['sortorder'] != '' && in_array($_POST['sortname'], $sort_fields)) {
            $order = $_POST['sortname'].' '.$_POST['sortorder'];
        }
                
        $old_sch = $model->getSchedulingList($where, $this->page, $order);
        $data_old_sch = array();
        foreach ($old_sch as  $k => $v) {
            $data_old_sch[$k] = $v;
            $data_old_sch[$k]['scheduling_date'] = date('d/m/y', $v['scheduling_date']);
            $data_old_sch[$k]['scheduling_work'] = $this->work($v['scheduling_date']);
            $data_old_sch[$k]['region'] = $region[$v['region_id']];
            $data_old_sch[$k]['state'] =  $state[$v['scheduling_status']];
        }
        
        $page_count = $model->gettotalpage();
        $list_count = $model->gettotalnum();
        
        output_data(array('list' => $data_sch,'old_list' => $data_old_sch), mobile_page($page_count, $list_count));
        
        
        
        /*


            $arr = array('待上班','进行中','已完成','已取消','排班未达标','迟到');
            foreach($arr as $v){
                $filterStateData[] = array(
                    'text' => $v,
                    'value' => $v
                );
            }

            $arr = array('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
            foreach($arr as $v){
                $filterWorkData[] = array(
                    'text' => $v,
                    'value' => $v
                );
            }


            $arr = array('T1','T2');
            foreach($arr as $v){
                $filterTypeData[] = array(
                    'text' => $v,
                    'value' => $v
                );
            }
        */
    }
    
    
    private function work($date)
    {
        $arr = array('Sun','Mon','Tues','Wed','Thur','Fri','Sat');
        
        return $arr[date('w', $date)];
    }
        
    
    /*
    反馈
    */
    public function delivery_feedback_typeOp()
    {
        $model_feedback = Model('feedback');
                
        // 设置页码参数名称
        $condition = array(
            'user_id' => $_POST['delivery_id'],
            'user_type' => 2
        );
		$feedback_list = $model_feedback->getFeedBackListGroup($condition, 'count(feedback_type_id) as count,feedback_type as name,feedback_type_id as value',0,"",0,"feedback_type_id");
		
		$all_count = 0;
		foreach($feedback_list as $k=>$v){
			$all_count += $v["count"];
		}
		$all["name"] = "全部";
		$all["count"] = $all_count;
		array_unshift($feedback_list,$all);
		
		output_data(array('list' => $feedback_list), mobile_page($page_count, $list_count));
    }
    
    /*
    反馈
    */
    public function delivery_feedbackOp()
    {
        $model_feedback = Model('feedback');
                
        // 设置页码参数名称
        $condition = array(
            'user_id' => $_POST['delivery_id'],
            'user_type' => 2
        );
            
        $order = '';
        $param = array('id','feedback_time','last_reply_time');
        if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
            $order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
        }
              
        $state = array(
                    0=>'待处理',
                    1=>'处理中',
                    2=>'已完成'
        );
        
        if ($_POST['feedback_type'] !='') {
            $condition['feedback_type'] = $_POST['feedback_type'];
        }
        if ($_POST['feedback_type_id'] !='') {
            $condition['feedback_type_id'] = $_POST['feedback_type_id'];
        }
        if ($_POST['feedback_state'] !='') {
            $condition['feedback_state'] = $_POST['feedback_state'];
        }
        if ($_POST['keyword'] != '') {
            $condition['feedback_code'] = array('like', '%' . $_POST['keyword'] . '%');
        }
        $feedback_list = $model_feedback->getFeedBackList($condition, '*', $this->page, $order);
        
        $data = array();
        foreach ($feedback_list as $value) {
            $param = array();
            $param['feedback_id'] = $value['id'];
            $param['feedback_code'] = $value['feedback_code'];
            $param['feedback_type'] = $value['feedback_type'];
            $param['feedback_content'] = $value['feedback_content'];
            $param['feedback_time'] = date('Y-m-d H:i:s', $value['feedback_time']);
            $param['last_reply_time'] = $value['last_reply_time'] > 0 ?  date('Y-m-d H:i:s', $value['feedback_time']) : 'N/A';
            $param['feedback_state'] = $state[$value['feedback_state']];
            $data[] = $param;
        }
                
        $page_count = $model_feedback->gettotalpage();
        $list_count = $model_feedback->gettotalnum();
        output_data(array('list' => $data), mobile_page($page_count, $list_count));
    
                

                
                
        $arr = array('投诉客户','投诉商家','意见反馈','配送问题','其他');
        foreach ($arr as $v) {
            $filterTypeData[] = array(
                        'text' => $v,
                        'value' => $v
                    );
        }
            
                
        $arr = array('待处理','处理中','已完成');
        foreach ($arr as $v) {
            $filterStateData[] = array(
                        'text' => $v,
                        'value' => $v
                    );
        }
    }
    
    
    
    
    
    
    
    private function waiter_state()
    {
        $array = array();
        
        $array[0] = '待审核';
        $array[1] = 'OnlineS';
        $array[2] = 'Manual';
        $array[3] = 'Offline';
        $array[4] = 'Cage';
        $array[5] = '待面试';
    
        return $array;
    }
    
    
    
    
    
    //编辑证件
    public function delivery_file_editOp()
    {
        if (chksubmit()) {
            $type = array(
                    1 => '护照',
                    2 => '签证',
                    3 => '驾照',
                    4 => '健康证',
                    5 => 'ABM',
                    6 => '车辆',
                );
                
            $data = array();
            $data['file_name'] = $type[$_POST['file_type']];
            $data['file_type'] = $_POST['file_type'];
			$gdurl = 'http://'.$_SERVER['SERVER_NAME'];
			
			if($_POST["file_url"]){
				$base64_image2 = str_replace(' ', '+', $_POST["file_url"]);
				//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
				$file_url = $_POST["file_url"]?str_replace($gdurl,"",$_POST["file_url"]):'';//编辑时没上传图片，不更新这个值
				if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image2, $result)){
					//定义图片储存文件目录
					$dir = '../data/upload/waiter/'.date('ymd');
					//定义文件名称
					$picname = date("his") . '_' . rand(10000, 99999);
					if (!is_dir($dir)){
						//如果不存在就创建该目录
						mkdir($dir,0777,true);  
					}  
					//获取图片后缀
					if($result[2] == 'jpeg'){
						$picdir=$picname.'.jpg';
					}else{
						$picdir=$picname.'.'.$result[2];
					}
					//图片名称
					$image_url = $dir.'/'.$picdir;
					//储存图片
					if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image2)))){
						$file_url = $image_url;
						$file_url = str_replace("..","",$file_url);
					}
				}
			}
			
			
            $data['file_url'] = $file_url;
            $data['file_state_time'] = strtotime($_POST['file_date'][0]);
            $data['file_end_time'] = strtotime($_POST['file_date'][1]);
            $data['addtime'] = time();
            $data['file_text'] = $_POST['file_text'];
            $data['distributor_id'] = $_POST['distributor_id'];
            $data['file_code'] = $_POST['file_code'];
            $data['file_state'] = 1;
    
            if ($_POST['file_id'] > 0) {
                $where = array(
                        'file_id' => $_POST['file_id']
                    );
                $row = Model('waiter_file')->editWaiterFile($where, $data);
            } else {
                $row = Model('waiter_file')->addWaiterFile($data);
            }
                
            if ($row) {
                output_data('保存成功');
            } else {
                output_error('保存失败');
            }
        }
                
            
        $where = array();
        $where['file_id'] = $_POST['file_id'];
        $info = model('waiter_file')->getWaiterFileInfo($where);
        if ($info) {
            $info['file_date'][0] = date('Y-m-d', $info['file_state_time']) ;
            $info['file_date'][1] = date('Y-m-d', $info['file_end_time']) ;
            $info['form_submit'] = 'ok';
			$info['file_url'] = $info['file_url'] ? 'http://'.$_SERVER['SERVER_NAME'].$info['file_url'] : "";
        }
            
        output_data(array('info' => $info));
    }
    
    //上传文件。
    
    public function file_uploadOp()
    {
        $upload     = new UploadFile();
        $upload->set('default_dir', ATTACH_WAITER_FILE);
        $result = $upload->upfile('file');
        if (!$result) {
            output_error($upload->error);
        } else {
            output_data($upload->file_name);
        }
    }
    
    
    
    //获取mini详情
    public function delivery_basicOp()
    {
        $state = $this->waiter_state();
        $where = array(
                'distributor_id' => $_POST['id']
            );
        $delivery_info = model('waiter')->getWaiterInfo($where);
        $delivery_info['avatar'] = getWaiterAvatar($delivery_info['distributor_avatar']);
        $delivery_info['state'] = $state[$delivery_info['distributor_status']];
       
       
       
       
        //	exit(json_encode(array('code'=> 200 ,'waiterInfo' => $waiter_info )));
      
        output_data($delivery_info);
    }
    
    
    //添加新的配送员
    public function delivery_saveOp()
    {
        $model_waiter = Model('waiter');
        $obj_validate = new Validate();
		
		if(isset($_POST["vehicle"]["is_vehicle"]) && !empty($_POST["vehicle"]["is_vehicle"])){
			$data = array();
				$data['distributor_truename'] = $_POST['distributor_truename'];
				$data['distributor_mobile'] = $_POST['distributor_mobile'];
				
				
				if($_POST["distributor_avatar"]){
					$base64_image2 = str_replace(' ', '+', $_POST["distributor_avatar"]);
					//post方式接收的数据, 加号会被替换为空格, 需要重新替换回来, 若不是post数据, 不需要执行
					//$distributor_avatar = $_POST["adv_id"]?str_replace($gdurl,"",$_POST["distributor_avatar"]):'';//编辑时没上传图片，不更新这个值
					if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image2, $result)){
						//定义图片储存文件目录
						$dir = '../data/upload/waiter/avatar';
						//定义文件名称
						$picname = date("his") . '_' . rand(10000, 99999);
						if (!is_dir($dir)){
							//如果不存在就创建该目录
							mkdir($dir,0777,true);  
						}  
						//获取图片后缀
						if($result[2] == 'jpeg'){
							$picdir=$picname.'.jpg';
						}else{
							$picdir=$picname.'.'.$result[2];
						}
						//图片名称
						$image_url = $dir.'/'.$picdir;
						//储存图片
						if (file_put_contents($image_url, base64_decode(str_replace($result[1], '', $base64_image2)))){
							//$distributor_avatar = $image_url;
							//$distributor_avatar = str_replace("..","",$distributor_avatar);
						}
					}
					$data['distributor_avatar'] = $picdir;
				}
			
				$where["distributor_id"] = $_POST["vehicle"]["is_vehicle"];
				$result = $model_waiter->editWaiter($where,$data);
				if ($result) {
					output_data(array('delivery_id'=>$result));
				} else {
					output_error('保存失败');
				}
		}else{
			$obj_validate->validateparam = array(
						array("input" => $_POST["code"], "require" => "true", "message" => '编号不能为空'),
						array("input" => $_POST["name"], "require" => "true", "message" => '昵称不能为空'),
						array("input" => $_POST["truename"], "require" => "true", "message" => '姓名不能为空'),
						array("input" => $_POST["mobile"], "require" => "true", "message" => '手机号不能为空'),
					);
			$error = $obj_validate->validate();
			if ($error != '') {
				output_error($error);
			} else {
				$data = array();
				$data['distributor_name'] = $_POST['name'];
				$data['distributor_truename'] = $_POST['truename'];
				$data['distributor_code'] = $_POST['code'];
				$data['distributor_mobile'] = $_POST['mobile'];
				$data['login_name'] = $_POST['mobile'];
				if ($this->checkMobile($_POST['mobile']) > 0) {
					output_error('此手机号已被注册');
				}
				$data['login_password'] = md5($_POST['password']);
				$data['addtime'] = time();
				$result = $model_waiter->addWaiter($data);
				if ($result) {
					output_data(array('delivery_id'=>$result));
				} else {
					output_error('保存失败');
				}
			}
		}
    }
	
	
	private function checkMobile($mobile){
		
		$where = array();
		$where['distributor_mobile'] = $mobile;
		$delivery_info = model('waiter')->getWaiterInfo($where);
		if($delivery_info){
			return 1;
		}else{
			return 0;
		}
		
		
	}
}
