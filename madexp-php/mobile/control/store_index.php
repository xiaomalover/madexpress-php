<?php
/**
 * 商户订单管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_indexControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	public function indexOp(){
		
		$seller_info = array();
		$seller_info['roles'] = ['admin'];
		$seller_info['introduction'] = 'I am a super administrator';
		$seller_info['avatar'] = 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif';
		$seller_info['name'] = $this->store_info['store_name_primary'];
		//$admin_info['']
		output_data($seller_info);
		
	}
	
	//数据统计
	public function statOp(){
		
		
	}
	
	
	//热销商品
	public function hotgoods_foodboxOp(){
		
		
		$where = array(
			'store_id' => $this->store_info['store_id'],
			'is_old' => 0,
			'is_delete' => 0
		);
		$sales_list = model('goods')->getGoodsList($where, '*','','goods_sales_month desc', 10);
		
		$data = array();
		foreach($sales_list as $k => $v){
				$lang = $this->goods_language($v['goods_id']);
				$data[$k] = $v;
				$data[$k]['goods_name'] = $lang['goods_name'];
				$data[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);   
		}			
		 
		 
		$foodbox_list  = model('foodbox_class')->getClassList(TRUE);
		foreach($foodbox_list as $k => $v){
		 			$foodbox[$k]['class_name'] = $v['class_name'];
		 			$foodbox[$k]['class_id'] = $v['class_id'];
		 			$foodbox[$k]['foodbox_num'] = $this->foodboxNum($v['class_id']);
		 			
		}		 
		
		output_data(array('hot_list'=> $data,'foodbox' => $foodbox));
	}
	

	
	//店铺信息
	public function store_infoOp(){
		
		
		$model = model('store');
		$store_info = $model->getStoreInfo(array('store_id'=> $this->store_info['store_id']));
		$store_info['store_avatar'] =  $store_info['store_avatar']
		    ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_info['store_avatar']
		    : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
		
		$store_info['store_banner'] != '' ?  explode(",", $store_info['store_banner']) : array();		
		
		
		$bindclass= model('store_bind_class')->getStoreBindClassList(array('store_id'=> $this->store_info['store_id'] ));
		$class = '';
		foreach($bindclass as $v){
			$class .= $v['class_name'].'; ';			
		}		
		$store_info['store_class'] = $class;
		
		//分组的店铺分类
		
		$cuisine_data = model('cuisine')->getCuisineList(TRUE);		
		$data = array();
		foreach($cuisine_data as $k => $v){
			$data[$k] = $v;
			$data[$k]['child'] = $this->getSmallChild($v['cuisine_id']);
		}
		$store_info['smallClass'] = $data;
		
		
		//绑定的语言报
		$bindlang = model('store_language')->getStoreLangList(array('store_id'=>$this->store_info['store_id']));				
		$data = array();
		foreach($bindlang as $v){
			$data[] = $v['language_name'];
		}
		$bindlang = implode(",", $data);		
		$store_info['store_lang'] = $bindlang;
		
		//日期
		$dateTime = array(
		 	'day' => date('d',time()),
			'month' => date('M',time()),
			'year' => date('y',time())
		);			 
		
		$store_info['store_shophours'] = unserialize($store_info['store_shophours']);	
		
		
		$store_info['store_estimated_time'] = !empty($store_info['store_estimated_time']) ? unserialize($store_info['store_estimated_time']) : array('start_time'=> 20,'end_time'=>40);
		
		$store_info['store_stripe_info'] = $this->get_stripe_info();
		
		$store_info['store_taking_type'] = $store_info['store_taking_type'];
		
		
		
		//平台所有语言
		$language = model('language')->getLangList(TRUE);	
		
		
		
		
		
		output_data(array('store_info' => $store_info,'language' => $language));
		
	}
	
	
	//获取子类
	private function getSmallChild($cuisine_id){
		
		$where = array(
			'cuisine_id' => $cuisine_id
		);
		$list= model('store_small_class')->getStoreSmallClassList($where);
		$data = array();
		foreach($list as $k => $v){
			$data[$k] = $v;
		}
		
		return $data;
	}
	
	
		
	private function foodboxNum($class_id){		
			$where = array(
				'class_id' => $class_id,
				'store_id' => $this->store_info['store_id']
			);
			$data = model('store_foodbox_goods')->getStoreFoodboxGoodsInfo($where,'SUM(goods_num) as num' );		
			return $data['num'];
	}
		
		
	private function goodsImageUrl($image,$store_id){
			
			if(empty($image)){
				return '';
			}
			$image = explode(",", $image);			
			return UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS . $image[0];
			
	}
	
	//查询语言参数
	private function goods_language($goods_id){		
			$where = array(
				'goods_id' =>$goods_id,
				'lang_id' => $this->store_info['store_default_lang']
			);
			$data = model('goods_language')->getGoodsLangInfo($where);
			if(!empty($data)){
				return $data;
			}else{
				return false;
			}
			
	}
		
	
	
	
	
	//修改营业时间	
	public function time_saveOp(){
			$model = model('store_examine');
			if(empty($_POST['time'])){
				output_error('请选择时间');					
			}	
			if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){					
				$update = array(
					'store_shophours' =>  serialize($_POST['time'])
				);
				$where = array(
					'store_id' => $this->store_info['store_id']
				);			
				$result = model('store')->editStore($update,$where);							
			}else{
			
				$data = array(
					'examine_type' => 'shophours',
					'examine_title' => '修改经营时间',
					'examine_old_data' => $this->store_info['store_shophours'],
					'examine_new_data' => serialize($_POST['time']),
					'store_id' => $this->store_info['store_id'],
					'addtime' => time(),
					'examine_sn' => 'MEO'.date('YmdHis',time())
					
				);			
				
				$result = $model->addStoreExamine($data);					
			}
		
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');				
			}
	}
	
	
	
	
	
	//修改经营品类
	public function class_saveOp(){
			$model = model('store_examine');			
			if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){				
				model('store_bind_class')->delStoreBindClass(array('store_id'=> $this->store_info['store_id']));				
							
				$where = array(
					'small_id' => array('in',implode(",", $_POST['class_first']))
				);			
				$small_class = model('store_small_class')->getStoreSmallClassList($where);		
				
				foreach($small_class as $v){
					$bind_class[] = array(
						'store_id' => $this->store_info['store_id'],
						'class_id' => $v['small_id'],
						'class_name' => $v['small_name'],
						'class_type' => 1
					);
				}
						
			
				$where = array(
					'small_id' => array('in',implode(",", $_POST['class_last']))
				);			
				$small_class = model('store_small_class')->getStoreSmallClassList($where);				
				foreach($small_class as $v){
					$bind_class[] = array(
						'store_id' => $this->store_info['store_id'],
						'class_id' => $v['small_id'],
						'class_name' => $v['small_name'],
						'class_type' => 0
					);
				}
				$result = model('store_bind_class')->addStoreBindClassAll($bind_class);
				
				
				
				
			}else{						
				$data = array(
					'examine_type' => 'class',
					'examine_title' => '修改经营类目',
					'examine_old_data' => $_POST['store_old_class'],
					'examine_new_data' => $_POST['store_new_class'],
					'store_id' => $this->store_info['store_id'],
					'addtime' => time()
				 );		
				$result = $model->addStoreExamine($data);				
			}
						
			if($result){	
					output_data('操作成功');
			}else{
					output_error('操作失败');	
			}
	}
	
	
	
	//修改LOGO	
	public function avatar_saveOp(){
		
		
		$upload = new UploadFile();
		
		$path = BASE_UPLOAD_PATH .DS. ATTACH_STORE . DS . $upload->getSysSetPath();
		$file_name = $this->base64_image_content($_POST['image'],$path);
		
		output_data(array('image' => $file_name));		
		
		/*
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
		*/
	}
	
	
	private function  base64_image_content($base64_image_content,$path){
	//匹配出图片的格式
		if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){
			$type = $result[2];
			$new_file = $path;
			if(!file_exists($new_file)){
				//检查是否有该文件夹，如果没有就创建，并给予最高权限
				mkdir($new_file, 0700);
			}
			$file_name = time().rand(10000,99999).".{$type}";
			$new_file = $new_file.$file_name ;
			if (file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_image_content)))){
				return $file_name;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	//修改公告
	public function notice_saveOp(){		
			$model = model('store');		
			$update = array(
				'store_notice' => $_POST['notice']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	
	//修改联系电话
	public function tel_saveOp(){		
			$model = model('store');		
			$update = array(
				'store_phone' => $_POST['tel']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}		
	}
	
	
	
	//修改简介
	public function about_saveOp(){
		
			$model = model('store');						
			$update = array(
				'store_about' => $_POST['about']
			);
			$where = array(
				'store_id' => $this->store_info['store_id']
			);			
			$result = $model->editStore($update,$where);						
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
	}
	
	//获取mapbox地址
	public function get_mapbox_addressOp(){
		
		
		
		
		
	}
	
	//修改地址	
	public function address_saveOp(){		
		
			$model = model('store_examine');	
			if($this->admin_info['admin_id'] > 0  && $this->store_info['store_examine'] == 0){			
				
				$address = $_POST['address'];			
			
				if($row['status'] == 'OK'){
					$poi = $row['results'][0]['geometry']['location'];					
				}
				
				$update = array(
					'store_address' => $_POST['address'],
					'store_lat' => $poi['lat'],
					'store_lng' => $poi['lng']
				);
				
				$where = array(
					'store_id' => $this->store_info['store_id']
				);			
				
				$result = model('store')->editStore($update,$where);	
				
				
			}else{
				$data = array(
					'examine_type' => 'address',
					'examine_title' => '修改商户地址',
					'examine_old_data' => $this->store_info['store_address'],
					'examine_new_data' => $_POST['address'],
					'examine_code' => 'ME'.date('YmdHis',time()),
					'store_id' => $this->store_info['store_id'],
					'addtime' => time()
				 );						
				$result = $model->addStoreExamine($data);			
			}
			if($result){	
				output_data('操作成功');
			}else{
				output_error('操作失败');	
			}
		
	}
	
	
	//保存预计送达时间
	public function save_estimated_timeOp(){	
		
		$update = array(
			'store_estimated_time' => serialize($_POST['time'])			
		);
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);			
		
		$result = model('store')->editStore($update,$where);	
		
		if($result){
			output_data('操作成功');
		}else{
			output_error('操作失败');	
		}		
	}
	
	
	//获取基本信息
	private function get_stripe_info(){
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);		
		$row = model('store_stripe')->getStoreStripeInfo($where);		
		return $row;		
	}
	
	
	
	//修改专员
	public function  save_mavinOp(){		
		
		$update = array(
			'admin_id' => $_POST['admin_id'],
			'admin_name' => $_POST['admin_name']
		);
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);					
		$result = model('store')->editStore($update,$where);			
		if($result){
			output_data('操作成功');
		}else{
			output_error('操作失败');	
		}			
	}
	
	
	//专员列表
	public function mavin_listOp(){
		
		$model_admin = model('admin');
		$where = array();
		$where['admin_type'] = 1;
		$admin_list = Model('admin')->getAdminList($where,1);
		foreach($admin_list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['examine_num'] = 0;
			$data[$k]['store_up'] = 0;
			$data[$k]['store_num'] = 0;			
			$data[$k]['admin_state_name'] = $v['admin_state'] == 1 ? '启用' : '禁用';
			
		}
				
		output_data(array('list' => $data));
		
		
		
		
	}
	
	//修改
	public function lang_saveOp(){
		
		$lang =  implode(',',$_POST['lang']);
		$where = array();
		$where['language_name'] = array('in',$lang);		
		$list = model('language')->getLangList($where);
		
		
		//删除以往设置的
		model('store_language')->delStoreLang(array('store_id' => $this->store_info['store_id']));
		
		
		foreach($list as $k=> $v){
			$data = array();			
			$data['language_id'] = $v['language_id'];
			$data['store_id'] 	= $this->store_info['store_id'];
			$data['language_name'] = $v['language_name'];
			$data['language_en'] = $v['language_flag'];
			$data['language_icon'] = $v['language_icon'];
			$data['is_default'] = $k == 0 ? 1 :0 ;
			$inst_data[] = $data;
		}
		
		$row = model('store_language')->addStoreLangAll($inst_data);
		
		if($row){			
			output_data('修改完成');
		}else{
			output_error('修改失败');
		}
		
	}
	
	
	
}
