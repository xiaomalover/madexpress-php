<?php
/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class seller_order_give_gongjuControl extends mobileHomeControl {

    public function __construct() {
        parent::__construct();     
		   	$_POST = json_decode(file_get_contents('php://input'),true);
    }

	
	
	
	//记录替换商品
	public function save_giveOp(){
		
		
		$goods_list = model('goods')->getGoodsList(array('store_id' => $_POST['store_id']));
		
	
		
		$goods_list_index = array_rand($goods_list,2);
	
		$cart_list = array();
		$goods_amount = 0;
		foreach($goods_list_index  as $k => $v){
			$goods_info = $goods_list[$v];
		}
		
		
	
		
		
		$_POST['goods_lang'] = 'CHN-S';
		
		
		//$goods_id = intval($_POST['goods_id']);
		$quantity = 1;		
	
	
			
		  		
		$param = array();		
		$param['store_id']  = $goods_info['store_id'];
		$param['goods_id']  = $goods_info['goods_id'];
		$param['goods_name'] = $goods_info['goods_name'];
		$param['goods_price'] = $goods_info['goods_price'];
		$param['goods_image'] = $goods_info['goods_image'];
		//$param['store_name'] = $goods_info['store_name'];
		$param['goods_spec'] = '';
		$param['goods_spec_price'] = 0;
		$param['goods_size'] = 'DefaultSize';
		$param['goods_size_price'] =  $goods_info['goods_price'];		
		$param['goods_optional'] = '';	
		$param['goods_optional_price'] = '';	
		//$param['goods_comment'] = $_POST['goods_comment'];		
		$param['goods_lang'] = 'CHN-S';
		$param['goods_class_id'] = $goods_info['gc_id'];		
		$param['order_id'] = $_POST['order_id'];						
		//$param['rec_id'] = $_POST['rec_id'];		
		$param['goods_num'] = 1;
			
		$param['give_type'] = 0;			
			
		$condition = array();		
		$condition['goods_id'] = $goods_info['goods_id'];
		//$condition['rec_id'] = $_POST['rec_id'];			
		$condition['order_id'] = $_POST['order_id'];
		$condition['goods_size'] ='DefaultSize';  		

		
	    //print_r($param);
		//print_r($condition);			
					
		$check_cart = $this->check_cart($condition);		
    	//print_r($check_cart);
		
		if($check_cart){			
			$update = array();
			$update['goods_num'] = $check_cart['goods_num'] + $quantity;						
			$where = array();
			$where['rec_id'] =  $check_cart['rec_id'];
			$row = model('order_goods_give')->editGive($where,$update);
		}else{			
			$row = model('order_goods_give')->addGive($param);
		}
		
		if($row){			
			output_data('ok');			
		}else{
			output_error('保存失败');
		}
	}
	
	
	
		
	private function goodsOptional($ids){			
			$where = array();
			$where['options_id'] = array('in',$ids);
			$where['state'] = 0 ;
			$where['store_id'] = $this->store_info['store_id'];	
			$list = model('goods_options')->getOptionsList($where);		
			$optional = array();
			foreach($list as  $k=>$v){
				$optional[$k]['id'] = $v['options_id'];
				$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$store_id);
				$optional[$k]['price'] = $v['options_price'];
			}
			return $optional;
	}
		
		
		//格式化配件商品的语言包
		private function optionsLang($data,$storeId){			
		    $langInfo = model('language')->getLangInfo(array('language_id'=>$this->store_info['store_default_lang']));
			$data = unserialize($data);
			foreach($data as $k => $v){
				$list[$v['lang_name']] = $v['options_name'];
			}	
			return $list[$langInfo['language_flag']];		
		}
	
	
		//获取当前值的KEY	
		private function find_by_array_spec($array,$find)
		{
			 foreach ($array as $key => $v)
			{
				if($v['value']==$find)
				{
					return $key;
				}
			}
		}
		
		
		private function find_by_array_size($array,$find)
		{
			foreach ($array as $key => $v)
			{
				if($v['name']==$find)
				{
					return $key;
				}
			}
		}
	
	
	
	
	
	//验证是否存在
	private function check_cart($where){
		
		//	print_r($where);
			$info  = model('order_goods_give')->getGiveInfo($where);		
		
		//	print_r($info);
			return $info;
	}
	
	
	
	
	/**
	 * 更新数量
	 */
	public function giveNumOp() {
	    
	    
	 //  print_r(234);
	    
	    $rec_id = intval($_POST['rec_id']);
	    $quantity = $_POST['quantity'];
	    
		$store_id = $this->store_info['store_id'];
		$order_id = intval($_POST['order_id']);
		
	     if(empty($rec_id) ) {
	        output_error('参数错误');
	    }
	
	    $model_give = Model('order_goods_give');	
           $where = array();
    	   $where['rec_id'] = $rec_id;
    	   $where['order_id'] = $order_id;
    	   $where['store_id'] = $store_id; 
    	   
	    if($quantity > 0 ){
	        
    	   $data = array();
    	   $data['goods_num'] = $quantity;	
    	   $row = $model_give->editGive($where,$data); //$this->editGive($data, array('rec_id'=>$rec_id,'order_id' => $order_id));
	    }else{
	        $row = $model_give->delGive($where);
	    }
	    if($row) {	
	        output_data('修改成功');
	    } else {
	        output_error('修改失败或商品不存在');
	    }
	    
	    
	    
	}
	
	
	//更改数量
	private function editGive($data,$where){
		
		
		
		$model_give = Model('order_goods_give');
		
		$row = $model_give->editGive($where,$data);
        
        return $row;
		
		
	}
	
	
	//删除所有赠送商品
	public function delGiveOp(){		
	
		$order_id = intval($_POST['order_id']);		
		
		$where = array();
		$where['order_id'] = $order_id;
		//$where['store_id'] = $this->store_info['store_id'];
		if($_POST['rec_id'] > 0){
			$where['rec_id'] = intval($_POST['rec_id']);
		}
		$row = model('order_goods_give')->delGive($where);		
		if($row){
			output_data('ok');			
		}else{
			output_error('失败');			
		}		
	}
	
	
	
	//校验传过来的属性和规格是否正确
	
	private function checkSpec($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_id' => $goods_id,
			'lang_name' => $lang_name
		);
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);
	
		$data['goods_sizespec'] ='';
		$data['goods_specs'] = '';

		
		return $data;
			
		
	}
	
	
	
}
