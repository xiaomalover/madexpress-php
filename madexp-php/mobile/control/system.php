<?php
/**
 * 系统设置
 */
defined('InMadExpress') or exit('Access Invalid!');

class systemControl extends mobileAdminControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	public function indexOp(){
		
		$model_setting = Model('setting');		
		$list_setting = $model_setting->getListSetting();		
		output_data($list_setting);	
			
			
	}
	
	//基本设置
	public function save_meOp(){
		$model_setting = Model('setting');
		$update_array = array();
		$update_array['site_name'] = $_POST['site_name'];
		$update_array['site_phone'] = $_POST['site_phone'];
		$update_array['site_email'] = $_POST['site_email'];
		$update_array['time_zone'] = $_POST['time_zone'];
		$update_array['site_status'] = $_POST['site_status'] ? 1 :0 ;
		$update_array['closed_reason'] = $_POST['closed_reason'];
		
		$result = $model_setting->updateSetting($update_array);
		if($result){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
	}
	
	
	//保存短信
	public function save_smsOp(){
		$model_setting = Model('setting');
		$update_array = array();
		$update_array['hao_sms_tgs'] = $_POST['hao_sms_tgs'];
		$update_array['hao_sms_zh'] = $_POST['hao_sms_zh'];
		$update_array['hao_sms_pw'] = $_POST['hao_sms_pw'];
		$update_array['hao_sms_key'] = $_POST['hao_sms_key'];
		$update_array['hao_sms_signature'] = $_POST['hao_sms_signature'];
		$update_array['hao_sms_bz'] = $_POST['hao_sms_bz'];
		
		$result = $model_setting->updateSetting($update_array);
		
		if($result){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
	}
	
	
	//保存upload
	public function save_uploadOp(){
		$model_setting = Model('setting');
		$update_array = array();
		$update_array['image_max_filesize'] = $_POST['image_max_filesize'];
		$update_array['image_allow_ext'] = $_POST['image_allow_ext'];
		$result = $model_setting->updateSetting($update_array);
		if($result){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
	}
	
	//保存email
	public function save_emailOp(){
		$model_setting = Model('setting');
		$update_array = array();
		$update_array['email_host'] = $_POST['email_host'];
		$update_array['email_port'] = $_POST['email_port'];
		$update_array['email_addr'] = $_POST['email_addr'];
		$update_array['email_id'] = $_POST['email_id'];
		$update_array['email_pass'] = $_POST['email_pass'];
		$update_array['email_test'] = $_POST['email_test'];
		$result = $model_setting->updateSetting($update_array);
		if($result){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
	}
	
	//保存twilio
	public function save_twilioOp(){
		$model_setting = Model('setting');
		$update_array = array();
		$update_array['account_sid'] = $_POST['account_sid'];
		$update_array['auth_token'] = $_POST['auth_token'];
		$update_array['twilio_number'] = $_POST['twilio_number'];
		$update_array['twilioApiKey'] = $_POST['twilioApiKey'];
		$update_array['twilioApiSecret'] = $_POST['twilioApiSecret'];
		$update_array['outgoingApplicationSid'] = $_POST['outgoingApplicationSid'];
		$update_array['identity'] = $_POST['identity'];
		$update_array['services_sid'] = $_POST['services_sid'];
		$update_array['proxy_phone'] = $_POST['proxy_phone'];
		$update_array['sms_verify'] = $_POST['sms_verify'];
		$update_array['email_verify'] = $_POST['email_verify'];
		$result = $model_setting->updateSetting($update_array);
		if($result){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
	}
	
	
	
	//语言包
	public  function system_langOp(){		
		$lang_list = model('language')->getLangList($where);		
		output_data($lang_list);		
	}
	
	//保存语言
	public function save_langOp(){
		
		$where = array();
		$where['language_id'] = $_POST['id'];
		$data = array();
		$data['language_name'] = $_POST['name'];
		$data['language_icon'] = $_POST['icon'];
		$data['language_flag'] = $_POST['code'];
		$data['language_system'] = $_POST['system'];
	
		$row = model('language')->editLang($data,$where);
		if($row){
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
		
		
	}
	

    /**
     * 管理员列表日志
     *
     * @param array $condition 检索条件
     * @param obj $obj_page 分页对象
     * @return array 数组类型的返回结果
     */
    public function getAdminListLogOp(){
		
        
	    $list = Model()->table('admin_log')->page($this->page)->select();
		
		$page_count = Model()->table('admin_log')->gettotalpage();
		$list_count = Model()->table('admin_log')->count();
		
		
		foreach($list as $k => $v){
				$v['createtime'] = date('Y-m-d',$v['createtime']);
				
				$data[$k] = $v;
		}
		output_data(array('list' => $data), mobile_page($page_count,$list_count));

    }
	
	
}
