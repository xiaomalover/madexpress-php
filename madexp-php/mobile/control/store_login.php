<?php
/**
 * 商家登录
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class store_loginControl extends mobileHomeControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 登录
     */
    public function indexOp(){
				
        if(empty($_POST['username']) || empty($_POST['password'])) {
            output_error('用户名密码不能为空');
        }

        $model_store = Model('store');
        $store_info = $model_store->getStoreInfo(array('store_code' => $_POST['username']));   
		
		
		if(!$store_info) {           	
			output_error('没有这个账号哦');
        }		
				
		if($store_info['store_login_password'] != md5($_POST['password'])){ 
			output_error('账号和密码不匹配哦');
		}
		
		
        //读取店铺信息
        $model_store = Model('store');
        $store_info = $model_store->getStoreInfoByID($store_info['store_id']);

        //更新卖家登陆时间
        $model_store->editStore(array('last_login_time' => TIMESTAMP), array('store_id' => $store_info['store_id']));

      
		//生成登录令牌  
        $token = $this->_get_token($store_info['store_id'], $store_info['store_login_name'], 'pc');
        if($token) {
		
			//	$member_token = $this->_get_member_token($member_info['member_id'], $member_info['member_name'], 'wap');
         //   $memberinfo=array('username' => $member_info['member_name'], 'userid' => $member_info['member_id'], 'key' => $member_token);
			
			output_data(array('seller_name' => $store_info['store_login_name'], 'store_name_primary' => $store_info['store_name_primary'], 'token' => $token));
        } else {
            output_error('登录失败');
        }
    }
	
	
	
	public function autologinOp(){
		
		$where = array();
		$where['token_id'] = $_POST['token_id'];
		$model_mb_seller_token = Model('mb_seller_token');
		$token_info =$model_mb_seller_token->getSellerTokenInfo($where);
		if(!empty($token_info)){
			$model_store = Model('store');
			$store_info = $model_store->getStoreInfoByID($token_info['store_id']);
			
			output_data(array('seller_name' => $store_info['store_login_name'], 'store_name_primary' => $store_info['store_name_primary'], 'token' => $token_info['token']));
						
		}else{
			output_error('登陆失败');
		}
		
	}
	
	

    /**
     * 登录生成token
     */
    private function _get_token($seller_id, $seller_name, $client) {
        $model_mb_seller_token = Model('mb_seller_token');

        //重新登录后以前的令牌失效
        $condition = array();
        $condition['seller_id'] = $seller_id;
        $model_mb_seller_token->delSellerToken($condition);

        //生成新的token
        $mb_seller_token_info = array();
        $token = md5($seller_name. strval(TIMESTAMP) . strval(rand(0,999999)));
        $mb_seller_token_info['seller_id'] = $seller_id;
        $mb_seller_token_info['seller_name'] = $seller_name;
        $mb_seller_token_info['token'] = $token;
        $mb_seller_token_info['login_time'] = TIMESTAMP;
        $mb_seller_token_info['client_type'] = $client;

        $result = $model_mb_seller_token->addSellerToken($mb_seller_token_info);

        if($result) {
            return $token;
        } else {
            return null;
        }
    }
	
	
	
	
	
	//
	public function logoutOp(){		
		output_data('安全登出');		
		
	}
	
	
}
