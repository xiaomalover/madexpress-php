<?php
/**
 * 我的银行卡

 */



defined('InMadExpress') or exit('Access Invalid!');

class member_languageControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
  
    }

    public function indexOp() {
		
		$where = array(
			'member_id' => $this->member_info['member_id']
		);
		$list = model('member_language')->getMemberLangList($where);	
		$data = array();
		foreach($list as $k => $v){
			
			$data[$k] = $v;
			$data[$k]['language_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['language_icon'];
			
		}	
		output_data($data);
		
    }
	
	//排序
	public function sortOp(){		
		
		$ids = explode(',',$_POST['id']);		
		
		foreach($ids as $k=> $v){	
			
			$where = array(
				'id' => $v,
				'member_id' => $this->member_info['member_id']
			);		
			$update = array(
				'language_sort' => $k+1
			);
			$row = model('member_language')->editMemberLang($update,$where);
		}
		
		
		
		
		if($row){			
			output_data('ok');			
		}else{			
			output_error('排序失败');
		}
	}


}
