<?php
/**
 * 我的收藏

 *
 */



defined('InMadExpress') or exit('Access Invalid!');
class member_collectionControl extends mobileMemberControl{

    public function __construct() {
        parent::__construct();
        $this->model_favorites = Model('favorites');
    }

    /**
     *收藏列表
     */
    public function indexOp(){
    	$collection_list = $this->model_favorites->getStoreFavoritesList($this->member_info['member_id']);    	
    	output_data(array('collection_list' => $collection_list));
    }
	
	public function favoritesOp(){
		
		$model =  Model('favorites');
		$where = array(
			'fav_type' => 'store',
			'store_id' =>$_POST['store_id'],
			'member_id' => $this->member_info['member_id']
		);
		
		
		$log =  $model->getOneFavorites($where);

		
		if(!empty($log)){			
			
			$result = $model->delFavorites($where);			
			$msg  ='取消收藏成功';
		}else{
			
			$data = array(
				'fav_type' 	=> 'store',
				'store_id' 	=>$_POST['store_id'],
				'fav_id' 	=>$_POST['store_id'],
				'member_id'	=> $this->member_info['member_id'],
				'member_name'=> $this->member_info['member_name'],
				'fav_time' 	=> time()				
			);
			
			$result =  $model->addFavorites($data);
			$msg = '收藏成功';
		}
		
		if($result){			
			output_data($msg);			
		}else{
			output_error('操作失败');			
			
		}
		
	}
	

	
}