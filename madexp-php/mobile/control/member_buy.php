<?php
/**
 * 下单流程
 */

defined('InMadExpress') or exit('Access Invalid!');

class member_buyControl extends mobileMemberControl
{
    public function __construct()
    {
        parent::__construct();
    }
    
    //新增？号提示内容
    
    public function payment_tipsOp(){
        $data = array();
        $data['tips'] = '后台配置文本';
        output_data($data);
    }

    public function delivery_tipsOp(){
        $data = array();
        $data['tips'] = '后台配置文本';
        output_data($data);
    }


    //获取购物车详情
    public function getCartList($store_id)
    {
        $model_cart = Model('cart');
        $condition = array(
            'buyer_id' => $this->member_info['member_id'],
            'store_id' => $store_id
        );
        $cart_list  = $model_cart->listCart('db', $condition);
        //print_r($cart_list);
        $store_cart_list = array();
        $goods_amount = 0;
        $foodbox_amount = 0;
        $goods_original_amount = 0;
        $lang = $this->member_info['menu_language'];
        foreach ($cart_list as $k=> $cart) {
         
			$lang_data = $this->goodsLang($cart, $lang);
          
			if (empty($lang_data)) {
				output_error('购物车商品数据异常');
			}
            $cart_list[$k]['goods_name'] = $lang_data['goods_name']; 						
            $cart_list[$k]['goods_lang'] = $lang_data['goods_lang'];          
			if($cart['set_meal'] == 0 ){
				$cart_list[$k]['goods_spec'] = $lang_data['goods_spec'];
				$cart_list[$k]['goods_size'] = $lang_data['goods_size'];				
			}else{				
				$cart_list[$k]['goods_spec'] = $this->_getMealList($cart['set_meal_spec'],$lang);
			}
			
			
            $cart_list[$k]['goods_image_url'] = $this->goodsImageUrl($cart['goods_image'], $cart['store_id']);
            $cart_list[$k]['goods_optional'] = $this->goodsOptional($cart['goods_optional'], $cart['goods_lang']);
                
				
				
				
				
            $goods_amount += $cart['goods_num'] * $cart['goods_price'];
            $foodbox_amount += ($cart['foodbox_num'] * $cart['foodbox_price']) * $cart['goods_num'];
            $goods_original_amount += $cart['goods_num']  * $cart['goods_original_price'];
        }
        
        return array('cart_list' => $cart_list, 'goods_amount' => $goods_amount ,'foodbox_amount' => $foodbox_amount ,'goods_original_amount' => $goods_original_amount);
    }
    
    
	//获取套餐商品
	private function _getMealList($meal_ids,$lang){		
		$model = model('store_goods_meal');
		$where = array();		    
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_id'] = array('in',$meal_ids);
		$list = $model->getMealList($where);
		$data = array();
		foreach($list as $k => $v){			
			$specs_name[] = $v['meal_value'];			 
		}			
		$specs_name = implode('/',$specs_name);	
		
		return $specs_name;
		
	}
	
	
	
    //格式化商品URL
    
    private function goodsImageUrl($image, $store_id)
    {
        if (empty($image)) {
            return '';
        }
        return UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS . $image;
    }
    
    
    
    
    
    
 
    //格式化当前数据根据现有值匹配出英文值
    private function goodsLang($data, $lang)
    {
            
        //根据现有数据获得规格的key
        $where = array(
        	'goods_id' => $data['goods_id'],
        	'lang_name' => $lang
        );		
        $lang_goods = model('goods_language')->getGoodsLangInfo($where);	
        $cart['goods_name'] = $lang_goods['goods_name'];
        //size			
        $size_row = 	model('store_goods_size')->getSizeInfo(array('size_id' => $data['goods_size_id'],'lang' => $lang,'is_old' => 0));
        				
        $cart['goods_size'] = $size_row['size_value'];
        $specs_row =  model('store_goods_specs')->getSpecsList(array('specs_id'=>array('in',$data['goods_specs_id']),'lang' => $lang,'is_old' =>0));
        $specs_name = [];
        foreach($specs_row as $v){
        	$specs_name[] = $v['specs_value'];
        }
        $specs_name = implode('/',$specs_name);			
        $cart['goods_spec'] = $specs_name;			
        $cart['goods_lang']	 = $lang;
        return $cart;
    }
    
    
    


    //查询语言参数
    private function goods_language($goods_id)
    {
        $where = array(
            'goods_id' =>$goods_id,
            'lang_id' => $this->member_info['menu_language']
        );
        $data = model('goods_language')->getGoodsLangInfo($where);
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }
    
    //获取菜品配件
   
   private function goodsOptional($ids,$lang ='CHN-S'){
   	   
   	   $where = array();
   	   $where['options_id'] = array('in',$ids);
   	   $where['is_old'] = 0;		   
   	   $options_list = model('store_goods_options')->getOptionsList($where);
   	   $data = array();	   
   	   $size_model = model('store_goods_size');
   	   $specs_model = model('store_goods_specs');
   	   $goods_lang_model = model('goods_language');
   	   foreach($options_list as $k =>$v){			  
   		   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang,'is_old' => 0));
   		   $data[$k]['size'] = $size['size_value'];		   
   		   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
   		   $specs_name = array();
   		   foreach($specs as $vs){
   			   $specs_name[] = $vs['specs_value'];
   		   }
   		   $specs_name = implode(',',$specs_name);			   
   		   
   		   $data[$k]['specs'] = $specs_name;		   
   		   $data[$k]['price'] = $v['options_price'];		   
   		   $goods_name = $goods_lang_model->getGoodsLangInfo(array('goods_code' => $v['options_goods_code'],'lang_name' => $lang,'is_old' => 0 ));		   
   		   $data[$k]['name'] = $goods_name['goods_name'];
   	//	   $data[$k]['goods_code'] = $v['options_goods_code'];
   		   $data[$k]['id'] = $v['options_id'];
   	   }
   	   
   	   return $data;
   	   
   	   
   }
    
    
    
    
    
    //格式化配件商品的语言包
    private function optionsLang($data, $storeId)
    {
        $langInfo = model('language')->getLangInfo(array('language_id'=> $this->member_info['menu_language']));
        $data = unserialize($data);
        foreach ($data as $k => $v) {
            $list[$v['lang_name']] = $v['options_name'];
        }
        return $list[$langInfo['language_flag']];
    }
    
    
    
    
    //获取当前值的KEY
    
    private function find_by_array_spec($array, $find)
    {
        foreach ($array as $key => $v) {
            if ($v['value']==$find) {
                return $key;
            }
        }
    }
    
    
    //获取商品尺寸
    
    private function find_by_array_size($array, $find)
    {
        foreach ($array as $key => $v) {
            if ($v['name']==$find) {
                return $key;
            }
        }
    }

    
    
    //获取可用优惠券数量
    private function coupon_num()
    {
        $num = model('redpacket') -> getCurrentAvailableRedpacketCount($this->member_info['member_id']);
        return $num;
    }
    
    
    
    //从新计算配送费
    public function checkDeliveryOp($store_id=0, $address_id = 0, $type = 0)
    {
        $store_id = intval($store_id) == 0 ? intval($_POST['store_id']) : intval($store_id);
        $address_id = intval($address_id) == 0 ? intval($_POST['address_id']) : intval($address_id);
        if ($store_id == 0 || $address_id == 0) {
            if ($type == 0) {
                output_error('参数错误');
            } else {
                return 0;
            }
        }
        
        $store =  model('store')->getStoreInfo(array('store_id'=>$store_id));
        $origin = $store['store_lat'].','.$store['store_lng'];
        
        
        
        $address =  model('address')->getAddressInfo(array('address_id'=>$address_id));
        
        $ng =  explode(",", $address['coordinate']);
    
        $destination =$ng[1].','.$ng[0];
        
        //	$lbs = new lbs();
        //$latlng = '118.787187,31.978779'; //模拟一个虚拟坐标。可以下单。否则无法下单，正式环境注释掉即可
        //$result = $lbs->direction($origin, $destination);
        //接口未返回距离开启自己计算
        $price  = 6;
        $order_allocation = model('order_allocation');
        //if($result == false){
        $km = $order_allocation->getDistance($store['store_lat'], $store['store_lng'], $ng[1], $ng[0]);
        $km = number_format(($km / 1000), 2);
        switch ($km) {
            case $km < 3:
                $price = 7;
            break;
            case  $km > 3:
                $price = round(($km - 3 + $price), 0);
             break;
        }
        if ($type == 0) {
            output_data(array('delivery_fee'=>$price));
        } else {
            return 	$price;
        }
    }
    
    
    
    
    //获取当前用户的银行卡。
    private function bank($bank_id = 0)
    {
        $list =  model('member_bank')->getMemberBankList(array('member_id'=> $this->member_info['member_id']));
        $bank = array();
        
        foreach ($list as $k=> $v) {
            if ($bank_id == $v['bank_id']) {
                $bank_default = $v;
            } elseif ($v['is_default'] == 1 && $bank_id == 0) {
                $bank_default = $v;
            }
                
            $bank_validity =  explode("/", $v['bank_validity']);
            $v['mm']  = $bank_validity[0];
            $v['yy']  = $bank_validity[1];
            $bank[$k] = $v;
        }
        return array('bank_list'=>$bank , 'bank_default' => $bank_default );
    }




    //收货人
    private function consignee($consignee_id = 0)
    {
        $attr = $this->consigneeAttr();
        $list =  model('consignee')->getConsigneeList(array('member_id'=> $this->member_info['member_id']));
        $consignee = array();
        foreach ($list as $k=> $v) {
            $consignee[$k] = $v;
            $consignee[$k]['attr_name'] = $attr[$v['attr_id']]['attr_name'];
            $consignee[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['attr_id']]['attr_icon'];
            
            if ($consignee_id == $v['consignee_id']) {
                $consignee_default = $consignee[$k];
            } elseif ($v['is_default'] == 1 && $consignee_id == 0) {
                $consignee_default = $consignee[$k];
            }
        }
        return array('consignee_list'=>$consignee , 'consignee_default' => $consignee_default );
    }
    
    
    
    //获取收货人的标签
    private function consigneeAttr()
    {
        $attr = model('consignee_attr')->getConsigneeAttrList(true);
        foreach ($attr as $v) {
            $attr[$v['attr_id']] = $v;
        };
        return $attr;
    }
    
    
    
    //地址
    private function address($address_id = 0)
    {
        $attr = $this->addressAttr();
        $list =  model('address')->getAddressList(array('member_id'=> $this->member_info['member_id']));
        $address = array();
        foreach ($list as $k=> $v) {
            $address[$k] = $v;
            $address[$k]['address_nickname'] = $v['address_nickname'];
            //	$address[$k]['attr_icon'] =  UPLOAD_SITE_URL.'/icon/'.$attr[$v['address_icon']]['attr_icon'];
            $address[$k]['address_icon_black'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['address_icon']]['address_icon_url'].'black@2x.png';
            $address[$k]['address_icon_white'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['address_icon']]['address_icon_url'].'white@2x.png';
            
            if ($address_id == $v['address_id']) {
                $address_default = $address[$k];
            } elseif ($v['is_default'] == 1) {
                $address_default = $address[$k];
            }
        }
        return array('address_list'=>$address , 'address_default' => $address_default );
    }
    
    
    
    //获取地址自定义标签。
    private function addressAttr()
    {
        $attr = model('address_attr')->getAddressAttrList(true);
        foreach ($attr as $v) {
            $attr[$v['address_icon']] = $v;
        };
        return $attr;
    }
    
    
    
    
    /**
     * 购物车、直接购买第一步:选择收获地址和配置方式
     */
    public function buyInfoOp()
    {
        $store_model = model('store');
        $store_id = intval($_POST['store_id']);
        
    
        $cart = $this->getCartList($store_id);
        
		if(empty($cart['cart_list'])){			
			output_error('购物车无菜品');
		}
		
		
        //print_r($cart);
        
        //收货地址
        $address = $this->address(intval($_POST['address_id']));
        
        //	print_r($address);
        $data['address'] = $address['address_list'];
        
        $data['address_default'] = $address['address_default'];
        
        
        //获取当前可用的优惠券数量
        //$data['coupon_num'] = $this->coupon_num();
        
        $data['coupon_price'] = ncPriceFormat(0);
        $data['platform_coupon_price'] = ncPriceFormat(0);
        //平台优惠券
        
        
        if (!empty($_POST['platform_coupon_id'])) {
            $platform_coupon = $this->getPlatformCoupon(intval($_POST['platform_coupon_id']));
            
            //print_r($platform_coupon);
            
            $data['platform_coupon_price'] = ncPriceFormat($platform_coupon['rpacket_price']);
            $data['platform_coupon_code'] = $platform_coupon['rpacket_code'];
            $data['platform_coupon_id'] = $platform_coupon['rpacket_id'];
        }
        
        $data['store_coupon_price'] = ncPriceFormat(0);
        //店铺优惠券
        if (!empty($_POST['store_coupon_id'])) {
            $store_coupon = $this->getStoreCoupon(intval($_POST['store_coupon_id']));
            //print_r($store_coupon);
            $data['store_coupon_id'] 	= $store_coupon['voucher_id'];
            $data['store_coupon_price'] = ncPriceFormat($store_coupon['voucher_price']);
            $data['store_coupon_code'] 	= $store_coupon['voucher_code'];
        }
        
        
        $data['coupon_price'] =ncPriceFormat($data['platform_coupon_price'] + $data['store_coupon_price']);
        $data['order_type'] = $_POST['order_type'] ? $_POST['order_type'] : 'Delivery';
    
        if ($data['order_type'] == 'Delivery') {
          
          $address_id = $_POST['address_id'] > 0 ?  $_POST['address_id'] :  $address['address_default']['address_id'];
		  
            $delivery_original_fee  = $this->checkDeliveryOp($store_id, $address_id, 1);
           /*  
           $ship_type = array(
                'type_id' => $_POST['delivery_type'] > 0 ? $_POST['delivery_type'] : 1
            );
            
           $shipping_type = Model('order_shipping_type')->getTypeInfo($ship_type);
            
            
            
            if (empty($shipping_type)) {
                output_error('配送类型参数错误');
            }
            if ($shipping_type['type_amount'] > 0) {
                $delivery_fee = $delivery_original_fee + $shipping_type['type_amount'];
            } elseif ($shipping_type['type_amount'] < 0) {
                $delivery_fee = $delivery_original_fee + $shipping_type['type_amount'];
            }
           */ 
             $delivery_fee = $delivery_original_fee ;
            
        } else {
            $delivery_fee = ncPriceFormat(0);
        }
        
        //	print_r($delivery_fee);
                    
        $data['delivery_fee'] = ncPriceFormat($delivery_fee); //配送费
        $data['goods_amount'] = ncPriceFormat($cart['goods_amount']); //商品费
 //       $data['sales_amount'] =  ($cart['goods_original_amount'] > $cart['goods_amount'])  ?  ncPriceFormat($cart['goods_original_amount'] -  $cart['goods_amount'])  : ncPriceFormat(0);
        $data['foodbox_amount'] = ncPriceFormat($cart['foodbox_amount']);//餐盒费
        $data['goods_original_amount'] = ncPriceFormat($cart['goods_original_amount']);//原商品总价
		


         //获取用户钱包金额
         $member_wallet_amount = $this->member_info['available_predeposit'];
        
         $data['sales_amount'] =  $cart['goods_original_amount'] > $cart['goods_amount']  ?  ncPriceFormat($cart['goods_original_amount'] -  $cart['goods_amount'])  : ncPriceFormat(0);//商品折扣
         
         $data['coupon_price'] = ncPriceFormat($data['platform_coupon_price'] + $data['store_coupon_price']);
 
         $order_amount = ncPriceFormat($data['goods_amount'] + $data['delivery_fee'] + $data['foodbox_amount'] - $data['coupon_price']);
            
         $data['pay_amount'] = ncPriceFormat(0);
         $data['wellet_amount']   = ncPriceFormat(0);
         
         
         if($member_wallet_amount > 0 ){ //有钱包金额
 
             if($member_wallet_amount >= $order_amount){ //钱包余额可以抵扣全部订单金额
 
                 $pay_commission_amount = ncPriceFormat(0);
 
                 $data['wellet_amount']  = ncPriceFormat($order_amount);
 
 
             }elseif($member_wallet_amount < $order_amount && $member_wallet_amount > 0){ //有钱，但不够足额支付的。
 
               
                 $pay_commission_amount = ncPriceFormat(($order_amount - $member_wallet_amount) * 0.03 + 0.3);
 
                 $data['pay_amount'] = ncPriceFormat($order_amount - $member_wallet_amount +  $pay_commission_amount) ;
 
                 $data['wellet_amount'] = ncPriceFormat($member_wallet_amount);
                 
             }
 
         }else{ //钱包没有钱
 
             $pay_commission_amount  = ncPriceFormat($order_amount * 0.03 + 0.3);
 
             $data['pay_amount'] = ncPriceFormat($order_amount + $pay_commission_amount);
            
        }
        
         $data['pay_mession_amount'] =ncPriceFormat($pay_commission_amount); //支付手续费
      

        $data['order_amount'] = ncPriceFormat($data['goods_amount'] + $data['delivery_fee'] + $data['foodbox_amount'] + $pay_commission_amount); //订单总价值
        
            
        $data['cart_list'] = $cart['cart_list'];
                    
        //店铺信息
        $data['store'] = $store_info = model('store')->getStoreInfo(array('store_id'=> $store_id), 'store_id,store_name_primary,store_name_secondary,store_code,store_lat,store_lng');
    
        $consignee = $this->consignee(intval($_POST['consignee_id']));
                    
        //收货人
        $data['consignee'] = $consignee['consignee_list'];
        $data['consignee_default'] = $consignee['consignee_default'];
        
        //银行卡
        $bank = $this->bank(intval($_POST['bank_id']));
        $data['bank'] = $bank['bank_list'];
        $data['bank_default'] = $bank['bank_default'];
        $data['delivery_time'] = $_POST['delivery_time'];
        
        //用户信息
        $data['member'] = array();
        $data['member']['member_id'] = $this->member_info['member_id'];
        $data['member']['member_name'] = $this->member_info['member_name'];
        $data['member']['member_truename'] = $this->member_info['member_truename'];
        $data['member']['member_avatar'] = $this->member_info['member_avatar'];
        $data['member']['available_predeposit'] = $this->member_info['available_predeposit'];
        $data['member']['member_code'] = $this->member_info['member_code'];
        
        //验证当前是否有可用配送员。
        $allocation = model('order_allocation')->getStoreDeliver($store_info);
        
        
        
        $shipping_type_list = model('order_shipping_type')->getTypeList(true);
       
        foreach ($shipping_type_list as $v) {
            if ($shipping_type['type_id'] == $v['type_id']) {
                $amount = 0;
            } else {
                if ($shipping_type['type_amount'] > 0) {
                    $amount = $v['type_amount'] - $shipping_type['type_amount'] ;
                } else {
                    $amount = $v['type_amount'] + abs($shipping_type['type_amount']) ;
                }
            }
            
            $v['type_amount'] = $amount;
            
            
            //	print_r($amount.'|');
            
            $shipping_method[] = $v;
        }
        
        
        //print_r($shipping_method);
        
        
      $data['shipping_method'] = $shipping_method ;//'Delivery to lobby/street';// $shipping_method;
       
        
        $h = date('H', time()) + 1;
        
        for ($i = 0 ; $i <= 10; $i++) {
            $list[] =  date('H:i', strtotime(date('Y-m-d ') .' '.$h.':00:00') +  ($i * 900));
        }
        $data['delivery_time_list'] = $list;
      
        if ($allocation['count'] <= 0 &&  $_POST['order_type'] == 'Delivery') {
            //	output_error('暂无可用配送员');
        }
        
        $data['is_deliver'] = $allocation['count'] > 1 ? 1 :0;
        
        output_data($data);
    }
    
    
    /*
     *独立验证是否有可用配送员 
     
     */
    
    public function check_deliveryOp(){ 
        
        $store_id = intval($_POST['store_id']);
        $store_info = model('store')->getStoreInfo(array('store_id'=> $store_id), 'store_id,store_name_primary,store_name_secondary,store_code,store_lat,store_lng');
        
         //验证当前是否有可用配送员。
        $allocation = model('order_allocation')->getStoreDeliver($store_info);
        $data = array();
        $data['is_deliver'] = $allocation['count'] > 1 ? 1 :0;
        
        output_data($data);
        
        
    }
    
    
    
    /**
     * 购物车、直接购买第二步:保存订单入库，产生订单号，开始选择支付方式
     *
     */
    public function buySaveOp()
    {
        $order = array();
        $store_id 		= intval($_POST['store_id']);
        $address_id 	= intval($_POST['address_id']);
        $bank_id 		= intval($_POST['bank_id']);
        $store_coupon_id= intval($_POST['store_coupon_id']);
        $platform_coupon_id = intval($_POST['platform_coupon_id']);
        $consignee_id 	= intval($_POST['consignee_id']);
        $order_from 	= 2;
        $cart = $this->getCartList($store_id);
        //
        if (empty($cart['cart_list'])) {
            output_error('购物车没有商品');
        }
                
        if (empty($store_id) && empty($address_id) && empty($bank_id) && empty($consignee_id)) {
            output_error('下单参数不全');
        }
                
        $store_info = model('store')->getStoreInfo(array('store_id'=> $store_id), '*');
            


            
        if ($_POST['order_type'] == 'Taking') {
            $order['order_type']  = 2;
        } elseif ($_POST['order_type'] == 'Delivery') {
            $order['order_type']  = 1;
        } else {
            $order['order_type']  = 1;
        }
                            
                
        //自提订单运费为零；
        if ($order['order_type'] == 1) {
            $delivery_fee = $this->checkDeliveryOp($store_id, $address_id, 1);
            $allocation = model('order_allocation')->getStoreDeliver($store_info);
            if ($allocation['count'] <= 0) {
                //output_error('暂无可用配送员');
            }
        } else {
            $delivery_fee = 0;
        }
                        
        if($allocation['count'] > 0){
            $order['delivery_capacity'] = 1;
        }else{
            $order['delivery_capacity'] = 0;
        }
    
                        
                        
                    
        $member_id = $this->member_info['member_id'];
        $member_name = $this->member_info['member_name'];
        $member_email = $this->member_info['member_email'];
        $member_level = $this->member_info['member_level'];
                
        $model_order = Model('order');
        $pay_sn = $this->makePaySn($member_id);
        $order_pay = array();
        $order_pay['pay_sn'] = $pay_sn;
        $order_pay['buyer_id'] = $member_id;
        $order_pay_id = $model_order->addOrderPay($order_pay);
                            
            
        $order_goods = array();
                
        $order['order_sn'] = $this->makeOrderSn($store_info['store_code'], $order['order_type']);
        $order['pay_sn'] = $pay_sn;
        $order['store_id'] = $store_info['store_id'];
        $order['store_name'] = $store_info['store_name_primary'];
        $order['store_phone'] = $store_info['store_phone'];
        $order['store_address'] = $store_info['store_address'];
            
            
        //获取商户的坐标信息，和地区信息。
        $region_where = array('region_id'=> $store_info['region_sm_id']);
        //	print_r($region_where);
                
        //	$reigon = model('region')->getRegionInfo($region_where,'region_color');
            
        //	print_r($region);
                
                
        $order['store_region_sm_id'] = $store_info['region_sm_id'];
        $order['store_region_name']  = $store_info['region_name'];
        $order['store_region_color'] = $store_info['region_color'];
        $order['store_region_id']  = $store_info['region_id'];
        $order['store_coordinate'] = $store_info['store_lng'].','.$store_info['store_lat'];
        
        
        //获取用的信息。
        $order['buyer_id'] 		= $this->member_info['member_id'];
        $order['buyer_code'] 	= $this->member_info['member_code'];
            
        if ($_POST['order_type'] == 'Delivery') {
       
			$consignee =  model('consignee')->getConsigneeInfo(array('member_id'=> $this->member_info['member_id'],'consignee_id'=>$consignee_id));
                    
            $address =  model('address')->getAddressInfo(array('member_id'=> $this->member_info['member_id'],'address_id'=>$address_id));
                    
                    
            $order['buyer_name'] 	= $consignee['consignee_name'];
            $order['buyer_phone'] 	= $consignee['consignee_mobile'];
                
            $order['buyer_address'] = $address['address'];
            $order['buyer_region_id'] = $address['region_parent_id'];
            $order['buyer_coordinate'] = $address['coordinate'];
            $order['buyer_region_color'] = $address['region_color'];
            $order['buyer_region'] = $address['region_name'];
                    
            //新增
            $order['member_address'] = serialize($address);
            $order['member_consignee'] = serialize($consignee);
            $order['consignee_attr_id'] = $consignee_id;
            $order['address_attr_id'] = $address_id;
        }
                
                
                
                        
                        
        $order['add_time'] = TIMESTAMP;
        // $order['payment_code'] = $store_pay_type_list[$store_id];
        $order['order_state'] = 10;
                
        $data['platform_coupon_price'] = 0;
        //平台优惠券
                
        
        if (!empty($_POST['platform_coupon_id'])) {
            $platform_coupon = $this->getPlatformCoupon(intval($_POST['platform_coupon_id']));
            if (!$platform_coupon) {
                output_error('平台优惠券无效');
            }
            //print_r($platform_coupon);
                    
            $order['platform_coupon_price'] = $platform_coupon['rpacket_price'];
            $order['platform_coupon_code'] 	= $platform_coupon['rpacket_code'];
            $order['platform_coupon_id'] 	= $platform_coupon['rpacket_id'];
        }
                
        $order['store_coupon_price'] = 0;
        //店铺优惠券
        if (!empty($_POST['store_coupon_id'])) {
            $store_coupon = $this->getStoreCoupon(intval($_POST['store_coupon_id']));
            if (!$store_coupon) {
                output_error('店铺优惠券无效');
            }
                    
            //print_r($store_coupon);
            $order['store_coupon_id'] 		= $store_coupon['voucher_id'];
            $order['store_coupon_price'] 	= $store_coupon['voucher_price'];
            $order['store_coupon_code'] 	= $store_coupon['voucher_code'];
        }
                
                
        $order['order_from']   = $order_from;
                         
        //查询当前店铺的平台抽成
        $contract = model('store_contract')->getContractInfo(array('store_id'=> $store_id,'is_default' => 1));
                
                
        $order['goods_amount'] = ncPriceFormat($cart['goods_amount']); //商品费
        $order['delivery_fee'] = ncPriceFormat($delivery_fee); //配送费
        $order['foodbox_amount'] = ncPriceFormat($cart['foodbox_amount']);//餐盒费
        $order['goods_original_amount'] = ncPriceFormat($cart['goods_original_amount']);//原商品总价
     
        //获取用户钱包金额
        $member_wallet_amount = $this->member_info['available_predeposit'];
        
        $order['sales_amount'] =  $cart['goods_original_amount'] > $cart['goods_amount']  ?  ncPriceFormat($cart['goods_original_amount'] -  $cart['goods_amount'])  : ncPriceFormat(0);//商品折扣
        
        $order['coupon_price'] = ncPriceFormat($order['platform_coupon_price'] + $order['store_coupon_price']);

		$order_amount = ncPriceFormat($order['goods_amount'] + $order['delivery_fee'] + $order['foodbox_amount'] - $order['coupon_price']);
           
        $order['pay_amount'] = ncPriceFormat(0);
        
        $order['pd_amount']  = ncPriceFormat(0);
        
        if($member_wallet_amount > 0 ){ //有钱包金额

            if($member_wallet_amount >= $order_amount){ //钱包余额可以抵扣全部订单金额

                $pay_commission_amount = ncPriceFormat(0);

                $order['pd_amount']    = ncPriceFormat($order_amount);


            }elseif($member_wallet_amount < $order_amount && $member_wallet_amount > 0){ //有钱，但不够足额支付的。

              
                $pay_commission_amount = ncPriceFormat(($order_amount - $member_wallet_amount) * 0.03 + 0.3);

                $order['pay_amount'] = ncPriceFormat($order_amount - $member_wallet_amount + $pay_commission_amount);

                $order['pd_amount'] = ncPriceFormat($member_wallet_amount);
                
              
                
            }

        }else{ //钱包没有钱

            $pay_commission_amount  = ncPriceFormat($order_amount * 0.03 + 0.3);

            $order['pay_amount'] = ncPriceFormat($order_amount + $pay_commission_amount) ;

        }

      //  $order_amount = $order_amount + $pay_commission_amount;


        $order['pay_commission_amount'] = $pay_commission_amount; //支付手续费
          
      
        $order['order_amount'] = ncPriceFormat($order['goods_amount'] + $order['delivery_fee'] + $order['foodbox_amount'] + $pay_commission_amount) ; //订单总价

      
        $order['rpt_amount'] = 0 ;

        $order['commission_amount'] = ($contract['contract_rate'] / 100) * $order['goods_amount'] + $pay_commission_amount ; //平台抽成
      
        $order['pay_order_amount'] =  $order['pay_amount'] ;
        

        
        $order['delivery_time'] = $_POST['delivery_time'];
        $order['delivery_type'] = $_POST['delivery_type'];
                        
        //$order['rcb_amount'] = 0 ;
        $order['order_note'] = $_POST['order_note']; //'无备注';
        $order['store_note'] = $_POST['store_note']; //给商家的备注；
        $order['delivery_note'] = $_POST['delivery_note']; //配送备注
        
        $order['store_capacity'] = $store_info['store_taking_type'];
        
       
        
        
		$order_allocation = model('order_allocation');                
                
        $address_coordinate = explode(',', $address['coordinate']);
                
        $order['store_buyer_distance'] = $order_allocation->getDistance($store_info['store_lat'], $store_info['store_lng'], $address_coordinate[1], $address_coordinate[0]);
		
        $order['distributor_store_distance'] = 0;
                                                
                        
        $bank_info = model('member_bank')->getMemberBankInfo(array('member_id'=>$this->member_info['member_id'],'bank_id'=>$bank_id));
        $order['bank_id']  	 = $bank_info['bank_id'];
        $order['bank_name']  = $bank_info['bank_name'];
        $order['bank_code']  = $bank_info['bank_code'];
        $order['payment_code']  = 'stripe';
        $order['payment_name']  = 'stripe支付';
        //是否启用组合支付。
        
		$order['store_bind_sector_id'] =  $store_info['bind_money_nav'];
			  
        $order['order_date']  = date('Y-m-d',time());
        $order['snapshot_time'] = time();
        $order['system_lang'] = $this->member_info['member_language'];
        $order['menu_lang'] = $this->member_info['menu_language'];
        


              //计算16变形
        $buyer_coordinate = explode(',', $order['buyer_coordinate']);
        $points = array(
            'lat' => $buyer_coordinate[1],
            'lng' => $buyer_coordinate[0]
        );
        $row =    model('order_allocation')->polygon16_check($store_info['bind_money_nav'], $points);
        if($row != 1){
            output_error('收货地址未在可配送区域内');
        }   
              
   
        /**
         * 新增优惠券超过订单总额后。多出的金额要写入到用户的钱包。
         * 优惠券折现
         */



        //    print_r($order);             
        $order_id = $model_order->addOrder($order);
        if ($order_id > 0) {
			
            foreach ($cart['cart_list'] as $k=> $val) {
                $goods_data[] = array(
                        'order_id' => $order_id,
                        'goods_id' => $val['goods_id'],
                        'goods_name'=> $val['goods_name'],
                        'goods_price'=> $val['goods_price'],
                        'goods_num'=> $val['goods_num'],
                        'goods_image'=> $val['goods_image'],
                        'goods_pay_price'=> $val['goods_price'],
                        'store_id'=> $store_info['store_id'],
                        'buyer_id'=> $this->member_info['member_id'],
                        'gc_id' => $val['goods_class_id'],
                        'goods_spec'=> $val['goods_spec'],
                        'goods_size'=> $val['goods_size'],
                        'goods_optional'=> serialize($val['goods_optional']),
                        'goods_comment' => $val['goods_comment'],
                        'goods_lang' => $val['goods_lang'],
                        'foodbox_price' => $val['foodbox_price'],
                        'foodbox_id' => $val['foodbox_id'],
                        'foodbox_name' => $val['foodbox_name'],
                        'foodbox_num' => $val['foodbox_num'],
                        'goods_usable_num' => $val['goods_num'],
                        'goods_specs_id' => $val['goods_specs_id'],
                        'goods_size_id' => $val['goods_size_id'],
                        'goods_code' => $val['goods_code'],
                        'goods_optional_ids' => $val['goods_optional_ids'],
                        'set_meal' => $val['set_meal'],
                        'set_meal_spec' => $val['set_meal_spec']

                    );
            }
                        
                
            //扣余额,如果订单未完成，剩余部分付款。订单取消余额退还 10分钟后退还
                
            if ($order['pd_amount'] > 0) {
                $available_rcb_amount = floatval(ncPriceFormat($this->member_info['available_predeposit']));
                $model_pd = Model('predeposit');
                $order_amount = floatval($order['order_amount']);
                $data_pd = array();
                $data_pd['member_id'] = $this->member_info['member_id'];
                $data_pd['member_name'] = $this->member_info['member_name'];
                $data_pd['amount'] = $order['pd_amount'];
                $data_pd['order_sn'] = $order['order_sn'];
                $data_pd['order_id'] = $order['order_id'];
                
                if ($available_rcb_amount >= $order['pd_amount']) {                    
                    //立即支付，订单支付完成
                    $model_pd->changePd('balance_deduction', $data_pd);                                    
                               
                }
            }
                
            //如果支付余额已抵扣全部金额，则订单直接显示已付款
            $model_order->addOrderGoods($goods_data);


            if (floatval($order['pay_order_amount']) <= 0) {
				
				//增加LOG记录
				$record_data = array();
				$record_data['order_id'] = $order_id;
				$record_data['order_addtime'] = TIMESTAMP;
				model('order_record')->addRecord('order_placed', $record_data);        
				
				
                if ($order['order_type'] == 1) {
                    $order['order_state'] = 20; //
                } else {
                    $order['order_state'] = 30; //
                }
                
                $data = array();
                $data['order_id'] = $order_id;
                $data['log_role'] = 'buyer';
                $data['log_msg'] = '支付订单';
                $data['log_orderstate'] = 20;
                $model_order->addOrderLog($data);
                                        
                    
                $data_order = array();
                $data_order['order_state'] =  $order['order_state'];
                $data_order['payment_time'] = TIMESTAMP;
                $data_order['payment_code'] = 'stripe';
                $data_order['payment_name'] = 'stripe';
                    
                if ($order['order_type'] == 2 && $store_info['store_taking_type'] == 1) { //自动接单
                        $data_order['order_state'] = 40; //自动接自提单
                }
                
                $model_order->editOrder($data_order, array('order_id'=>$order_id));
            }
             
            //print_r($goods_data);
                
            $condition = array(
                    'store_id' => $store_info['store_id'],
                    'buyer_id' => $this->member_info['member_id']
            );
            
            //清空购物车
            model('cart')->delCart('db', $condition);
                                
            //用掉优惠券
            if (!empty($order['platform_coupon_id'])) {
                $this->checkRedPacket($order['platform_coupon_id'], $order['order_sn']);
            }
            if (!empty($order['store_coupon_id'])) {
                $this->checkStoreCoupon($order['platform_coupon_id'], $order['order_sn']);
            }
                
            //	$data = array();
                
            //	$model_order->addOrderLog($data);
                
                
            //改变默认
            if ($order['order_type'] == 1) {
                $this->editAddressConsigneeDefault($address_id, $consignee_id, $bank_id);
            }
            
            $pay_info = array();
            $pay_info['pay_amount'] = $order['pay_order_amount'];
            $pay_info['order_id'] = $order_id;
            $pay_info['payment_code'] = $order['payment_code'];
            $pay_info['pay_sn'] = $order['pay_sn'];
            $pay_info['order_sn'] = $order['order_sn'];
                
            //判断自提单还是配送单，然后执行不同的推送
            $model_alloction = model('order_allocation');
                    
            //计算16变形
            $buyer_coordinate = explode(',', $order['buyer_coordinate']);
            $points = array(
                    'lat' => $buyer_coordinate[1],
                    'lng' => $buyer_coordinate[0]
            );
          //  print_r($points);
		//	print_r($store_info['bind_money_nav']);            
		//	print_r($order_id);
		//	            
			$model_alloction->polygon16($store_info['bind_money_nav'], $points, $order_id);
            
		//	print_r(3333);			
			
            if ($order['pay_order_amount'] == 0 &&  $order['order_type'] == 1) {
           //     $model_alloction->sendDeliveryOrder($order['order_sn'], 1);	//推送给骑手
            }
			
			
            if ($order['pay_order_amount'] == 0 &&  $order['order_type'] == 2) {
             //   $model_alloction->sendStoreOrder($order['order_sn'], 1);	//推送给商家
            }
            
            //更新当前店铺所属类目的订单数量
            
            
            
            //更新点单偏好preference		        
            $store_class = model('store_bind_class')->getStoreBindClassList(['store_id' => $order['store_id']]);
			foreach($store_class as $v){				
				$cate[] = array(
					'cate_name' => $v['class_name'],
					'cate_id'	=> $v['class_id'],
					'member_id' => $this->member_info['member_id']
				);
			}		           
            model('member_preference')->addMemberPreferenceAll($cate);
			
         //   print_r(444);
            //更新用户订单信息

            $update = array();
            $update['order_count'] = array('exp','order_count + 1');
            $update['order_week_svg'] = $this->member_week_svg(); //周均订单
        //    $update['order_week_price'] = $this->member_price_svg(); //均单消费
            model('member')->editMember(['member_id' => $this->member_info['member_id']],$update);

                
            output_data($pay_info);
        } else {
            output_error('下单失败');
        }
    }

    //计算周均订单
    private function member_week_svg(){

        $order = model('order')->getOrderCount(['member_id' => $this->member_info['member_id']]);
        $data = $order / 52; 
        return  $data > 0 ? $data : 0;

    }




    //计算均单消费金额
    private function member_price_svg(){

        $order = model('order')->getOrderInfoN(['member_id' => $this->member_info['member_id']],'SUM(order_amount) as amount, COUNT(order_id) as num ');
        $amount = $order['amount'] > 0 ?  $order['amount'] : 0;
        $num = $order['num'] >0 ? $order['num'] : 0;
      
        $data = $amount / $num;
        return $data > 0 ? $data : 0 ;

    }






    
    //修改 地址，收货人默认
    
    private function editAddressConsigneeDefault($address_id, $consignee_id, $bank_id)
    {
        model('address')->editAddress(array('is_default'=> 0), array('member_id'=> $this->member_info['member_id']));
        
        model('address')->editAddress(array('is_default'=> 1), array('member_id'=> $this->member_info['member_id'],'address_id'=>$address_id));
        
     //   model('consignee')->editConsignee(array('member_id'=> $this->member_info['member_id']), array('is_default'=> 0));
        
    //    model('consignee')->editConsignee(array('member_id'=> $this->member_info['member_id'],'consignee_id'=>$consignee_id), array('is_default'=> 1));
        
        model('member_bank')->editMemberBank(array('is_default'=> 0), array('member_id'=> $this->member_info['member_id']));
        
        model('member_bank')->editMemberBank(array('is_default'=> 1), array('member_id'=> $this->member_info['member_id'],'bank_id'=>$bank_id));
		
		//更新最后下单时间		
		model('member')->editMember(['member_id' => $this->member_info['member_id']],['order_last_time' => time()]);
		
    }
    
    
    /*
    更新类目销量
    */
    private function sale_cate_num($store_id){
        
        //获取商家的类目
        
        
        
        
        
        
    }
    

    //验证优惠券
    private function checkRedPacket($id, $sn)
    {
        $model = model('redpacket');
        $where = array(
            'rpacket_id' =>$id,
            'rpacket_owner_id' => $this->member_info['member_id'],
            'rpacket_state' => 1
        );
        $info = $model->getRedpacketInfo($where);
        if (!empty($info)) {
            $data = array(
                'rpacket_order_id' => $sn,
                'rpacket_state' => 2
            );
            $row = $model->editRedpacket($where, $data, $this->member_info['member_id']);
            return $info;
        } else {
            return array();
        }
    }
    
    private function checkStoreCoupon($id, $sn)
    {
        $model = model('voucher');
        
        $where = array(
            'voucher_id' => $id,
            'voucher_owner_id' => $this->member_info['member_id'],
            'voucher_state' => 1
        );
        
        $info = model('voucher')->getVoucherInfo($where);
        if (!empty($info)) {
            $update = array(
                'voucher_order_id' => $sn,
                'voucher_state' => 2
            );
            $row = $model->editVoucher($update, $where, $this->member_info['member_id']);
            return $info;
        } else {
            return array();
        }
    }
    
    
    
    
    //查询平台优惠券
    
    private function getPlatformCoupon($id)
    {
        $where = array(
            'rpacket_id' =>$id,
            'rpacket_owner_id' => $this->member_info['member_id'],
            'rpacket_state' => 1
        );
        $info = model('redpacket')->getRedpacketInfo($where);
        if (!empty($info)) {
            return $info;
        } else {
            return array();
        }
    }
    
    
    //查询店铺优惠券
    private function getStoreCoupon($id)
    {
        $where = array(
            'voucher_id' => $id,
            'voucher_owner_id' => $this->member_info['member_id'],
            'voucher_state' => 1
        );
        $info = model('voucher')->getVoucherInfo($where);
        if (!empty($info)) {
            return $info;
        } else {
            return array();
        }
    }
    
    
    
    
    //可用平台优惠券列表
    public function coupon_listOp()
    {
        $where = array(
            'rpacket_owner_id' => $this->member_info['member_id'],
            'rpacket_state' => 1
        );
        
        $platform = Model('redpacket')->getRedpacketList($where);
        $platform_coupon_list = array();
        foreach ($platform as  $k => $v) {
            $platform_coupon_list[$k]['platform_coupon_id'] = $v['rpacket_id'];
            $platform_coupon_list[$k]['platform_coupon_title'] = $v['rpacket_title'];
            $platform_coupon_list[$k]['platform_coupon_price'] = $v['rpacket_price'];
            $platform_coupon_list[$k]['platform_start_date'] = $v['rpacket_start_date_text'];
            $platform_coupon_list[$k]['platform_end_date'] = $v['rpacket_end_date_text'];
            $platform_coupon_list[$k]['platform_coupon_code'] = $v['rpacket_code'];
        }
        
        $store_id = intval($_POST['store_id']);
        if (!empty($store_id)) {
            $model_voucher = Model('voucher');
            $list = $model_voucher->getMemberVoucherList($this->member_info['member_id'], 1);
            $store_coupon_list = array();
            foreach ($list as $k => $v) {
                $store_coupon_list[$k]['store_coupon_id'] = $v['voucher_id'];
                $store_coupon_list[$k]['store_coupon_title'] = $v['store_name_primary'];
                $store_coupon_list[$k]['store_coupon_price'] = $v['voucher_price'];
                $store_coupon_list[$k]['store_start_date'] = $v['voucher_start_date_text'];
                $store_coupon_list[$k]['store_end_date'] = $v['voucher_end_date_text'];
                $store_coupon_list[$k]['store_coupon_code'] = $v['voucher_code'];
            }
        }
        output_data(array('platform_coupon_list' => $platform_coupon_list,'store_coupon_list'=>$store_coupon_list));
    }
    


    


    public function makePaySn($member_id)
    {
        return mt_rand(10, 99)
              . sprintf('%010d', time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $member_id % 1000);
    }

    /**
     * 订单编号生成规则，n(n>=1)个订单表对应一个支付表，
     * 生成订单编号(年取1位 + $pay_id取13位 + 第N个子订单取2位)
     * 1000个会员同一微秒提订单，重复机率为1/100
     * @param $pay_id 支付表自增ID
     * @return string
     */
    public function makeOrderSn($store_code, $order_type)
    {
        //记录生成子订单的个数，如果生成多个子订单，该值会累加
        return $store_code . date('dmy', time()). $order_type. rand(100, 999);
    }




    /**
     * 更换收货地址
     */
    public function change_addressOp()
    {
        $logic_buy = Logic('buy');
        if (empty($_POST['city_id'])) {
            $_POST['city_id'] = $_POST['area_id'];
        }
        $data = $logic_buy->changeAddr($_POST['freight_hash'], $_POST['city_id'], $_POST['area_id'], $this->member_info['member_id']);
        if (!empty($data) && $data['state'] == 'success') {
            output_data($data);
        } else {
            output_error('地址修改失败');
        }
    }



    /**
     * 实物订单支付(新接口)
     */
    public function payOp()
    {
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d{18}$/', $pay_sn)) {
            output_error('该订单不存在');
        }
        //查询支付单信息
        $model_order= Model('order');
        $pay_info = $model_order->getOrderPayInfo(array('pay_sn'=>$pay_sn,'buyer_id'=>$this->member_info['member_id']), true);
        if (empty($pay_info)) {
            output_error('该订单不存在');
        }
    
        //取子订单列表
        $condition = array();
        $condition['pay_sn'] = $pay_sn;
        $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
        $order_list = $model_order->getOrderList($condition, '', '*', '', '', array(), true);
        if (empty($order_list)) {
            output_error('未找到需要支付的订单');
        }

        //定义输出数组
        $pay = array();
        //支付提示主信息
        //订单总支付金额(不包含货到付款)
        $pay['pay_amount'] = 0;
        //充值卡支付金额(之前支付中止，余额被锁定)
        $pay['payed_rcb_amount'] = 0;
        //预存款支付金额(之前支付中止，余额被锁定)
        $pay['payed_pd_amount'] = 0;
        //还需在线支付金额(之前支付中止，余额被锁定)
        $pay['pay_diff_amount'] = 0;
        //账户可用金额
        $pay['member_available_pd'] = 0;
        $pay['member_available_rcb'] = 0;
        $logic_order = Logic('order');
        //计算相关支付金额
        foreach ($order_list as $key => $order_info) {
            if (!in_array($order_info['payment_code'], array('offline','chain'))) {
                if ($order_info['order_state'] == ORDER_STATE_NEW) {
                    $pay['payed_rcb_amount'] += $order_info['rcb_amount'];
                    $pay['payed_pd_amount'] += $order_info['pd_amount'];
                    $pay['pay_diff_amount'] += $order_info['order_amount'] - $order_info['rcb_amount'] - $order_info['pd_amount'];
                }
            }
        }
        if ($order_info['chain_id'] && $order_info['payment_code'] == 'chain') {
            $order_list[0]['order_remind'] = '下单成功，请在'.CHAIN_ORDER_PAYPUT_DAY.'日内前往门店提货，逾期订单将自动取消。';
            $flag_chain = 1;
        }

        //如果线上线下支付金额都为0，转到支付成功页
        if (empty($pay['pay_diff_amount'])) {
            output_error('订单重复支付');
        }

        $payment_list = Model('mb_payment')->getMbPaymentOpenList();
        if (!empty($payment_list)) {
            foreach ($payment_list as $k => $value) {
                if ($value['payment_code'] == 'wxpay') {
                    unset($payment_list[$k]);
                    continue;
                }
                unset($payment_list[$k]['payment_id']);
                unset($payment_list[$k]['payment_config']);
                unset($payment_list[$k]['payment_state']);
                unset($payment_list[$k]['payment_state_text']);
            }
        }
        //显示预存款、支付密码、充值卡
        $pay['member_available_pd'] = $this->member_info['available_predeposit'];
        $pay['member_available_rcb'] = $this->member_info['available_rc_balance'];
        $pay['member_paypwd'] = $this->member_info['member_paypwd'] ? true : false;
        $pay['pay_sn'] = $pay_sn;
        $pay['payed_amount'] = ncPriceFormat($pay['payed_rcb_amount']+$pay['payed_pd_amount']);
        unset($pay['payed_pd_amount']);
        unset($pay['payed_rcb_amount']);
        $pay['pay_amount'] = ncPriceFormat($pay['pay_diff_amount']);
        unset($pay['pay_diff_amount']);
        $pay['member_available_pd'] = ncPriceFormat($pay['member_available_pd']);
        $pay['member_available_rcb'] = ncPriceFormat($pay['member_available_rcb']);
        $pay['payment_list'] = $payment_list ? array_values($payment_list) : array();
        output_data(array('pay_info'=>$pay));
    }
}
