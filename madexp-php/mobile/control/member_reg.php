<?php
/**
 * 我的商城
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_regControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 
     */
	
	public function registerOneOp(){
		
		$list = model('consignee_attr')->getConsigneeAttrList(TRUE);
		$data = array();
		foreach($list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['action'] = 0;
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['attr_icon'];			
		}
		
		output_data(array('list'=>$data));
		
		
	}
	
	public function registerOneSaveOp(){
		
		
		$where = array(
			'member_id' => $this->member_info['member_id']
		);
		$update = array(
			'member_truename' => $_POST['member_truename'],
			'member_attr' => $_POST['member_attr']
		);
		
		$result = model('member')->editMember($where,$update);
		if($result){
		    
		    //更新到收货人本人里
		    
		    $data = array();
            $data['member_id'] = $this->member_info['member_id'];
            $data['consignee_name'] = $_POST['member_truename'];
            $data['consignee_mobile'] = $this->member_info['member_mobile']; 
            $data['attr_id'] = $_POST['member_attr'];    
    	//	$data['attr_name'] = $_POST['attr_name'];    
    		$data['is_default'] = 1; 
		    Model('consignee')->addConsignee($data);
		    
		    
		    
		    
		    
			output_data('ok');					
		}else{
			output_error('保存失败');	
		}
			
		
	}
	
	
	//获取所有子类
	public function registerTwoOp(){
		
		$type = $_POST['type'];
		if($type == 1){
			$where = array(
				'cuisine_id' => 1
			);	
		}elseif($type == 2){
			$where = array(
				'cuisine_id' => array('in','2,3,4')
			);	
		}elseif($type == 3){
			$where = array(
				'cuisine_id' => array('in','1,2,3,4')
			);	
		}		
		$cuisine = model('cuisine')->getCuisineList($where);		
		foreach($cuisine as $k=> $v){
			$list[$k] = $v;
			$list[$k]['child'] = $this->small_class($v['cuisine_id']);
		}
		output_data(array('small_class' => $list));
		
	}
	
	public function registerTwoSaveOp(){
		
		
		$cateIds  = $_POST['cateIds'];
		$where = array(
			'small_id' => array('in',$cateIds)
		);
		$data_list = model('store_small_class')->getStoreSmallClassList($where);
		foreach($data_list as  $v){
			$in_data[] = array(
				'member_id' => $this->member_info['member_id'],
				'bind_class_name' => $v['small_name'],
				'bind_class_id' => $v['small_id'],
				'bind_sort' => 0						
			);
		}
		
		
		$row = model('member_bind_class')->addMemberBindClassAll($in_data);		
		if($row){
			  output_data('1');
		}else{
			 output_error('保存失败');
		}
		
		
	}
	
	
	private function small_class($id){
		$where = array(
			'cuisine_id' =>  $id
		);
		
		
		$list = model('store_small_class')->getStoreSmallClassList($where);
		foreach($list as $k => $v){
			$data[$k] = $v;
			$data[$k]['select'] = 0;
			$data[$k]['small_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['small_icon'];	
		}
		return $data;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
