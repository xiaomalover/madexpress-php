<?php
/**
 * 商家优惠券
 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_couponsControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }


	//店铺优惠券种类
	
	
	public function indexOp(){	
		
						//店铺优惠券
		                $condition = array();
		                $condition['voucher_t_gettype'] = 3;
		                $condition['voucher_t_state'] 	= 1;
		                $condition['voucher_t_end_date'] = array('gt', time());
		                $condition['voucher_t_store_id'] = array('in', $this->store_info['store_id']);
		                $voucher_template = Model('voucher')->getVoucherTemplateList($condition);
						$voucher = array();
		                if (!empty($voucher_template)) {
							
		                    foreach ($voucher_template as $val) {
								
		                        $param = array();
		                        $param['coupons_id'] = $val['voucher_t_id'];
		                        $param['coupons_price'] = $val['voucher_t_price'];
								$param['coupons_title'] = $val['voucher_t_title'];
		                        //$param['voucher_t_limit'] = $val['voucher_t_limit'];
								//$param['voucher_t_end_date'] = date('Y年m月d日', $val['voucher_t_end_date']);
		                      
		                        $voucher[] = $param;
								
		                    }					
							
		                }
						 
							
						output_data(array('coupons'=>$voucher));	   
								
						
								
	}
	
	
}
?>