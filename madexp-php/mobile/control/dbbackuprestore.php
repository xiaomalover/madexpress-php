<?php
/*
	数据库备份与还原
*/
defined('InMadExpress') or exit('Access Invalid!');

class dbbackuprestoreControl extends mobileAdminControl {
   
    private $client;
    public function __construct(){
		parent::__construct();	
    }

	//备份数据库
	public function backupOp(){	
		if(!file_exists("database_bakeup"))
		{
			 mkdir("database_bakeup",0777);//创建目录
		}
		
		$fileName    = './database_bakeup/'.date('Y-m-d-H-i-s').'_'.rand(100,999).'.sql';

		$list = Model()->query(' show table status ');
		$tables=array();
		foreach($list as $key=>$val)
		{
			 $tables[] = $val['Name'];
		}


		//枚举所有表的创建语句
		foreach ($tables as $val) {
			 $res = Model()->query("show create table $val");
			 $isDropInfo   = "DROP TABLE IF EXISTS `" . $val . "`;\r\n";
			 $tableStructure = $isDropInfo . $res[0]['Create Table'] . ";\r\n";

			 file_put_contents($fileName, $tableStructure, FILE_APPEND);	 
		}


		// 枚举所有表的INSERT语句
		foreach ($tables as $val) {
			$res = Model()->query("select * from $val");
			$sqlStr = "";
			if(!empty($res)){
				foreach($res as $kk=>$vv)
				{
					 $keys = array_keys($vv);
					 $keys = array_map('addslashes', $keys);
					 $keys = join('`,`', $keys);
					 $keys = "`" . $keys . "`";

					 $vals = array_values($vv);
					 $vals = array_map('addslashes', $vals);
					 $vals = join("','", $vals);
					 $vals = "'" . $vals . "'";
						
					 $sqlStr .= "insert into `$val` ($keys) values($vals);\r\n";	 
				}
				file_put_contents($fileName, $sqlStr, FILE_APPEND);
			}			
	   }

	   output_data('数据库备份成功');
	}	

	
	//还原数据库
	public function restoreOp(){	
				
		//$restore_file= "2021-04-04-17-06-03_997.sql";

		if(!file_exists("database_bakeup/".$restore_file))
		{
			output_error('备份文件不存在');
		}

		$str = file_get_contents("database_bakeup/".$restore_file);
		$allsqls = explode(";\r\n", $str);//所以... 这里拆分sql

		if($allsqls)
		{
			foreach($allsqls as $sql)
			{
				Model()->query($sql);//逐条执行
			}
		}
		output_data('还原数据库成功');
	}	


	
	//获取录音通话记录
	public function allbackupOp(){	

		  $path    = './database_bakeup/';

		  if(!is_dir($path)){
				output_error('备份目录不存在');
		  }
 
		  //scandir方法
		  $filelist = array();
		  $data = scandir($path);
		  foreach ($data as $value){
			if($value != '.' && $value != '..'){
			  $filelist[] = $value;
			}
		  }
		  
		  //备份文件，不需要放在数据库中
		  $list=array();
		  foreach($filelist as $key=>$file)
		  {
			 $list[$key]['backup_url']=$path.$file;
			 $list[$key]['backup_time']=substr($file,0,19);
			 $list[$key]['backup_name']=substr($file,0,19)."备份文件";  
		  }
		
		  output_data(array('list'=>$list ));				
	}
}