<?php 

defined('InMadExpress') or exit('Access Invalid!');

class cacheControl extends mobileAdminControl
{
    protected $cacheItems = array(
        'setting',          // 基本缓存
        'seo',              // SEO缓存   
        'store_class',      // 店铺分类        
        'store_msg_tpl',    // 店铺消息
        'member_msg_tpl',   // 用户消息
        'consult_type',     // 咨询类型                
        'area',              // 地区
        
    );

    public function __construct() {
        parent::__construct();
        Language::read('cache');
    }

    public function indexOp() {
        $this->clearOp();
    }

    /**
     * 清理缓存
     */
    public function clearOp() {
       
        $lang = Language::getLangContent();

        // 清理所有缓存
        if ($_POST['cls_full'] == 1) {
            foreach ($this->cacheItems as $i) {
                dkcache($i);
            }

            // 商品分类
            dkcache('gc_class');
            dkcache('all_categories');
            dkcache('goods_class_seo');
            dkcache('class_tag');
            if (C('cache_open')) {
                dkcache('index/article');
            }

        } else {
			
            $todo = (array) $_POST['cache'];
            foreach ($this->cacheItems as $i) {
                if (in_array($i, $todo)) {
                    dkcache($i);
                }
            }

            // 商品分类
            if (in_array('goodsclass', $todo)) {
                dkcache('gc_class');
                dkcache('all_categories');
                dkcache('goods_class_seo');
                dkcache('class_tag');
            } 
			
        }

     
     
    }
}


?>