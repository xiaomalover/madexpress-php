<?php
/**
 * 店铺
 *
 */


defined('InMadExpress') or exit('Access Invalid!');

class storeControl extends mobileMemberControl
{
    public function __construct()
    {
        parent::__construct();
    }

	
	
    /**
     * 店铺信息
     */
    public function store_infoOp()
    {
			
        $store_id = $_POST['store_id'];
        if ($store_id <= 0) {
            output_error('参数错误');
        }
			
		
		$store_online_info = model('store')->getStoreInfo(['store_id' => $store_id]);
		
		
			//切换菜单语言
		if($_POST['lang_id'] > 0){
			
			//修改商户当前选中的菜单ID		    
		    $info = model('store_language')->getStoreLangInfo(['store_id' => $store_id,'language_id' => $_POST['lang_id']]);
			model('member')->editMember(['member_id' => $this->member_info['member_id']],['menu_language' => $info['language_en']]);
			$this->member_info['menu_language'] = $info['language_en'];	
					

		}else{		
			
			$lang = $this->storeLanguage($store_id);
			$where = array(
				'member_id' => $this->member_info['member_id']
			);
			$updata = array(
				'menu_language' => $lang[0]['language_code']			
			);
			model('member')->editMember($where,$updata);			
			$this->member_info['menu_language'] = $lang[0]['language_code'];
			
						
		}
//		print_r($lang);
		
		
	//	print_r($this->member_info['menu_language']);
	
	
        $store_info = array();
        $store_info['store_id'] = $store_online_info['store_id'];
        $store_info['store_name_primary'] = $store_online_info['store_name_primary'];
		$store_info['store_name_secondary'] = $store_online_info['store_name_secondary'];
		$store_info['store_address'] = $store_online_info['store_address'];
		$store_info['store_phone'] = $store_online_info['store_phone'];
		$store_info['store_shophours'] = unserialize($store_online_info['store_shophours']);
		$store_info['store_about'] = $store_online_info['store_about'];
		$store_info['store_notice'] = $store_online_info['store_notice'];		
		$store_info['store_tag'] = $this->storeTagClass($store_online_info['store_id']);	
		$store_info['store_lat'] = $store_online_info['store_lat'];
		$store_info['store_lng'] = $store_online_info['store_lng'];
		$store_info['store_score'] = $store_online_info['store_score'];		
		$store_info['min_consume'] = $store_online_info['min_consume'];
		
		$storeLang = $this->storeLanguage($store_online_info['store_id']);
		
	    $store_info['language_id'] =   $info['language_id'];

		
	
	
		
        // 店铺头像
        $store_info['store_avatar'] = $store_online_info['store_avatar']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_online_info['store_avatar']
            : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
		
        // 如果已登录 判断该店铺是否已被收藏
        if ($memberId = $this->member_info['member_id']) {
            $c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($store_id, $memberId);
            $store_info['is_favorate'] = $c > 0;
        } else {
            $store_info['is_favorate'] = false;
        }

		
      

        // 页头背景图
        $store_info['mb_title_img'] = $store_online_info['mb_title_img']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_online_info['mb_title_img']
            : '';

		
        // 轮播
        $store_info['mb_sliders'] = array();
        $mbSliders = @unserialize($store_online_info['mb_sliders']);
        if ($mbSliders) {
            foreach ((array) $mbSliders as $s) {
                if ($s['img']) {
                    $s['imgUrl'] = UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.$s['img'];
                    $store_info['mb_sliders'][] = $s;
                }
            }
        }
		
        output_data(array(
            'store_info' => $store_info,
            'store_class' => $this->storeGoodsClass($store_online_info['store_id']),
            'store_goods' => $this->storeGoods($store_online_info['store_id']),
			'store_language' => $this->storeLanguage($store_online_info['store_id'])
        )); 	
    }




	//获取当前店铺的语言
	private function storeLanguage($store_id){
		
			
		$where = array(
			'store_id' => $store_id
		);		
		
		$store_lang = model('store_language')->getStoreLangList($where);
		if(!empty($store_lang)){
					
			foreach($store_lang as $v){
				$ids[] = $v['language_id'];
			}
			$ids = implode(",",$ids);		
			$where = array(
				'language_id' => array('in',$ids),
				'member_id' => $this->member_info['member_id']
			);		
			$language = model('member_language')->getMemberLangList($where,'language_id,language_name,language_icon,language_sort,language_code');
			$data = array();
			foreach($language as $k => $v){
				
				$data[$k] = $v;
				$data[$k]['language_icon'] = UPLOAD_SITE_URL.'/icon/'.$v['language_icon'];
				
			}	
			
		}else{
			$data = array();	
		}
		
		return $data;
	}
	
	
	//店铺绑定的分类
	private function storeTagClass($store_id){		
		$where = array(
			'store_id' => $store_id
		);		
		$tag = model('store_bind_class')->getStoreBindClassList($where);
		
		$data = array();
		foreach($tag as $k=> $v){		
		    $info = model('category')->getGoodsClassInfoById($v['class_id']);
			$data[$k] = $v;		
			
			$data[$k]['class_icon'] = UPLOAD_SITE_URL.'/icon/'.$info['cate_icon'];
		}		
		return $data;
	}
	
	
    /**
     * 店铺商品分类
     */
    private function storeGoodsClass($store_id)
    {
         
	   $lang = $this->member_info['menu_language'];
       $store_goods_class = Model('store_goods_class')->getStoreGoodsClassPlainList($store_id,$lang);
	   $data = array();
	   foreach($store_goods_class as $k => $v){		   
		   $data[$k] = $v;		 
		   $data[$k]['cart_num'] =  $this->cart_count($v['class_id'],$store_id,1);		   
	  }	
      return $data;
    }

	
	
    /**
     * 店铺商品
     */
    private function storeGoods($store_id,$param=array())
    {
		
		$store_class = $this->storeGoodsClass($store_id);		
		$data = array();
		foreach($store_class as $k=> $v){
			$data[$k] =$v;
			
			if($v['class_id'] == 9997){		
				
				$data[$k]['goods_list'] = $this->hot($store_id);	
						
			}elseif($v['class_id'] == 9998){
				
				$data[$k]['goods_list'] = $this->recommend($store_id);			
				
			}elseif($v['class_id'] == 9999){
				
				$data[$k]['goods_list'] = $this->discount($store_id);			
		
			}elseif($v['class_id'] == 9996){
				
				$data[$k]['goods_list'] = $this->meal($store_id);		
			}else{
				
				$data[$k]['goods_list'] = $this->goodsList($v['class_id'],$store_id);			
				
			}
		}
	
       return $data;
    }

    
    //套餐
    private function meal($store_id){
        
        $where = array();
		$where['store_id'] = $store_id;
		$where['goods_is_set_meal'] = 1;		  
		$where['is_old'] =  0;   
	 	$data = $this->goods_list_un($where,$store_id);	
		return $data;
    }

	private function hot($store_id){ //热销
	
		$where = array();
		$where['store_id'] = $store_id;
		$where['is_sales'] = 1;		  
		$where['is_old'] =  0;   
	 	$data = $this->goods_list_un($where,$store_id);	
		return $data;
		
	}

	private function recommend($store_id){ //tuiujian
	
		$where = array();
		$where['is_recommend'] = 1;
		$where['is_old'] =  0;
		$where['store_id'] = $store_id;
        $data = $this->goods_list_un($where,$store_id);		
		return $data;
		
	}


	private function discount($store_id){ //优惠
	
		$where = array();
		$where['store_id'] = $store_id;
		$where['is_discount'] = 1;		
		$where['is_old'] =  0;
        $data = $this->goods_list_un($where,$store_id);			
		return $data;
		
	}
	
	
	
	private function goodsList($class_id,$store_id){
		$where = array();
		//当前分类的ID
		$ext_goods = $this->extGoods($class_id,$store_id);	
		
		//筛选出来的 ingrid
		$ingr = $_POST['icon_ingr'];
		
		if(!empty($ingr)){
			
			$ext_ingr_goods = $this->extIngrGoods($ingr,$store_id);		
			
			
			//print_r($ext_ingr_goods);
			
			if(count($ext_ingr_goods) > 0){
				
					foreach($ext_ingr_goods as $ingr){
						
						foreach($ext_goods as $ext){
							
							if($ext == $ingr){						
								$search_goods[] = $ingr;						
							}
							
						}
					}
					if(count($search_goods) > 0){
					  $ext_goods =  implode(",", $search_goods);	
					}else{
					  $ext_goods = '';
					}
			
			}else{
				$ext_goods = '';				
			}
		}else{
			 $ext_goods =  implode(",", $ext_goods);	
		}
		
	//	print_r($ext_goods);
		$where['goods_code'] = array('in',$ext_goods);		
		$where['is_delete'] = 0;
		$where['is_old'] = 0;	
		$where['goods_state'] = 1;	
	
			
      	$data = $this->goods_list_un($where,$store_id);			
		return $data;
	}
	
	
	
	// 读取扩展分类的商品
	private function extGoods($stc_id,$store_id){					
		//$where = array('stc_id'=> $stc_id,'store_id'=> $store_id);
		//去除扩展分类功能
		//$ext_goods =  model('store_goods_extended')->getGoodsExtList($where,'goods_code');	
		
		$ext_goods = model('goods')->getGoodsList(array('gc_id'=>$stc_id),'goods_code');
		
		
		
		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods[] = $v['goods_code'];
			}			
		}else{
			$goods = array();	
		}
		return $goods;
	}
	
	
	/*
	
	//获取ingr成分列表 
	private function extIngrGoods($ingr,$store_id){		
		//$where = array();
		//$where['store_id'] = $store_id;		
		$where = 'store_id = '.$store_id;
		
		//拆分成数组				
		$ingr = explode(",", $ingr);		
		
		$not_ingr_data = model('goods_attr') -> getGoodsAttrList(array('attr_type' => 1));
		foreach($not_ingr_data as $v){
			$not_ingr_id[] = $v['attr_id'];
		}
	
		foreach($ingr as $v){
			if(in_array($v,$not_ingr_id)){
			    if($v == 29){
					$notin_ingr[] = 6;
				}else{
				    $notin_ingr[] = $v;
				}
			}else{
				$in_ingr[] = $v;
			}
		}
			
		if(count($notin_ingr) > 0 && count($in_ingr) > 0){				
			$where .=' and ingr_id in ('. implode(",", $in_ingr).') and  ingr_id not in ('.implode(",", $notin_ingr).')';
			// = array('ingr_id' =>  array('in', implode(",", $in_ingr)),'ingr_id' =>array('not in',implode(",", $notin_ingr)),'_op' => 'and') ;				
			//$where['ingr_id'] =  array('not in',implode(",", $notin_ingr));
		}elseif(count($notin_ingr) > 0 && empty($in_ingr)){
			
			$where .=' and ingr_id not in ('. implode(",", $notin_ingr).')';				
			
		}else{
			
			$where .=' and ingr_id in ('. implode(",", $in_ingr).')';				
			
		}
		
	//	print_r($where);
		
	
		$ext_goods =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');	
		
		if(!empty($ext_goods)){			
			foreach($ext_goods as $v){			
				$goods[$v['goods_code']][] = $v['goods_code'];
			}
			
			$ingr_count = count($ingr);
			$data = array();
			foreach($goods as $k=> $v){
			//	if($ingr_count <= count($v)){
					$data[] = $k;
			//	}
			}
			$goods =  $data;
		}else{
			$goods = array();	
		}		
		return $goods;		
	}	
	*/
	
	
	//获取ingr成分列表 
	private function extIngrGoods($ingr,$store_id){		
		//$where = array();
		//$where['store_id'] = $store_id;		
		$where = 'store_id = '.$store_id;
		
		//拆分成数组				
		$ingr = explode(",", $ingr);		
		
		$not_ingr_data = model('goods_attr') -> getGoodsAttrList(array('attr_type' => array('in','1,2')));
		foreach($not_ingr_data as $v){
			$not_ingr_id[] = $v['attr_id'];
		}	
		$notin_ingr = array();
		$in_ingr = array();
		foreach($ingr as $v){
			if(in_array($v,$not_ingr_id)){								
				if($v == 29){
					$notin_ingr[] = 6;
				}else{
					$notin_ingr[] = $v;
				}				
			}else{
				$in_ingr[] = $v;
			}
			
		}		
	
		//获取包含的商品
		$in_goods_list = array();
		if(count($in_ingr) > 0){
			$where = 'store_id = '.$store_id.' and ingr_id in ('. implode(",", $in_ingr).')';									
			$in_goods_data =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');
			
			foreach($in_goods_data as $v){				
				$in_goods_list[$v['goods_code']][] = $v['goods_code'];				
			}	
			//print_r($in_goods_list);
			$ingr_count = count($in_ingr);
			$in_goods = array();
			foreach($in_goods_list as $k=> $v){
				if($ingr_count <= count($v)){
					$in_goods[] = $k;
				}
			}	
		//	print_r($in_goods);
		}
		
	//	print_r($in_goods);
	
		//获取要剔除的商品
		$not_goods_list = array();
		if(count($notin_ingr) > 0){
			$where = 'store_id = '.$store_id.' and ingr_id  in ('. implode(",", $notin_ingr).')';					
			//print_r($where);
			$not_goods_data =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');			
			foreach($not_goods_data as $v){
				$not_goods_list[$v['goods_code']][] = $v['goods_code'];				
			}
		//	print_r($not_goods_list);
			
			$ingr_count = count($notin_ingr);
			$not_goods = array();
			foreach($not_goods_list as $k=> $v){				
					$not_goods[] = $k;				
			}
			
			
		}	
		

		
		
		if(!empty($in_ingr) && !empty($notin_ingr)){ //获取和剔除同时存在的时候		
			
			//1.先获取剔除的商品列表 			
			$ids = '';
			if(count($not_goods) > 0){
				foreach($not_goods as $v){
					$ids .= "'".$v."'," ;
				}
				$ids = substr($ids,0,-1);			
			}			
			$where = "store_id = ".$store_id." and goods_code not in (".$ids.")";		
			$not_goods_data =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');
			$not_goods = array();
			foreach($not_goods_data as $v){				
				$not_goods[$v['goods_code']] = $v['goods_code']; 
			}
			
			//这里是剔除后的列表						
		//	print_r($not_goods);
			
			
			//2. 从剔除的商品列表里提取需要抓取的商品。
		//	print_r($in_goods);
			$ids = '';
			if(count($not_goods) > 0){
				foreach($not_goods as $v){
					$ids .= "'".$v."'," ;
				}
				$ids = substr($ids,0,-1);			
			}			
			
			
				
			$where = 'store_id = '.$store_id." and goods_code in (".$ids.") and ingr_id in (". implode(",", $in_ingr).')';
			
			//print_r($where);
			$in_goods_data =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');
			$in_goods_list = array();
			foreach($in_goods_data as $v){				
				$in_goods_list[$v['goods_code']][] = $v['goods_code'];				
			}	
			//print_r($in_goods_list);
			$ingr_count = count($in_ingr);
			$in_goods = array();
			foreach($in_goods_list as $k=> $v){
				if($ingr_count <= count($v)){
					$in_goods[] = $k;
				}
			}		
				
			//print_r($in_goods);
			$goods = $in_goods;
		
		//	print_r($goods);
			
			
		}elseif(!empty($in_ingr) && empty($notin_ingr)){	 //当筛选只存在抓取的时候
			
			
			$goods = $in_goods;
						
						
		}elseif(empty($in_ingr) && !empty($notin_ingr)){    //当筛选只存在剔除的时候
			
			
			//获取全部分类然后剔除商品
			$ids = '';
			if(count($not_goods) > 0){
				foreach($not_goods as $v){
					$ids .= "'".$v."'," ;
				}
				$ids = substr($ids,0,-1);			
			}			
			$where = "store_id = ".$store_id." and goods_code not in (".$ids.")";		
			$not_goods_data =  model('goods_ingr')->getGoodsIngrList($where,'goods_code');
			foreach($not_goods_data as $v){				
				$goods[$v['goods_code']] = $v['goods_code']; 
			}
		}	
		
		//print_r($goods);
			
		if(!empty($goods)){				
			$goods =  $goods;		
							
		}else{
			
			$goods = array();	
		}		
		return $goods;		
	}	
	
	
	
	
	//单独筛选商品列表
	
	public function goodsListOp(){
		
		$store_id = $_POST['store_id'];		
		$icon_ingr = $_POST['icon_ingr'];				
		
		$store_goods  = $this->storeGoodsScreen($store_id);
				
		output_data(array('store_goods' => $store_goods));
	}
	
	
	
	
	//ICON筛选
    private function storeGoodsScreen($store_id,$param=array())
    {
		
		$store_class = $this->storeGoodsClass($store_id);
		
		$data = array();
		foreach($store_class as $k=> $v){			
			$data[$k] = $v;
			$data[$k]['goods_list'] = $this->goodsList($v['class_id'],$store_id);				
			
		}
	
       return $data;
    }
	
	
	
	
	//格式化商品数据
	private function goods_list_un($where,$store_id){
			
		
		$model_goods = Model('goods');					
		$goods_list = $model_goods->getGoodsList($where);
	
		
		$data = array();
		foreach($goods_list as $k=> $v){			
			$data[$k] = $v;
			$lang = $this->goods_language($v['goods_id']);
			if(!empty($lang)){
				$data[$k]['goods_name'] = $lang['goods_name'];	
				$data[$k]['goods_description'] = $lang['goods_description'];	
				$data[$k]['goods_specs'] = $this->getSpecsFormat($lang['goods_code'],$lang['lang_name']);
				$data[$k]['goods_sizeprice'] = $this->getSizeFormat($lang['goods_code'],$lang['lang_name']) ;
			//	$data[$k]['goods_optional'] = unserialize($lang['goods_optional']);	
				$data[$k]['goods_lang'] = $lang['lang_name'];	
			}else{
				
				$data[$k]['goods_name'] = $v['goods_name'];	
				$data[$k]['goods_description'] = $v['goods_description'];	
			//	$data[$k]['goods_specs'] = $this->FormatData(unserialize($v['goods_specs']),'specs');	
			//	$data[$k]['goods_sizeprice'] = unserialize($v['goods_sizeprice']);	
			//	$data[$k]['goods_optional'] = unserialize($v['goods_optional']);	
				$data[$k]['goods_lang'] = $v['lang_name'];	
			
			}	
						
			$data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image'],$store_id) ;
			$data[$k]['goods_ingr'] = $this->goods_ingr($v['goods_ingr']);
			$data[$k]['goods_optional'] = $this->goodsOptional($v['goods_code'],$lang['lang_name']);
			$data[$k]['cart_num'] = $this->cart_count($v['goods_id'],$store_id);
			
		}		
		
		return $data;
		
	}
	
	
	//格式化 size
	private function getSizeFormat($goods_code,$lang = 'ENG'){
		
		$model = model('store_goods_size');
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSizeList($where);
		$data = array();
		foreach($list as $k => $v){			
			$data[$k]['name'] 		= $v['size_value'];
			$data[$k]['price'] 		= $v['size_price'];
			$data[$k]['sale_price'] = $v['size_sale_price'];
			$data[$k]['box_id'] 	= $v['size_box_id'];
			$data[$k]['size_id'] 	= $v['size_id'];
		}		
		return $data;
		
	}
	
	
	//格式化 specs
	private function getSpecsFormat($goods_code,$lang = 'ENG'){		
		
		$model = model('store_goods_specs');		
		$where = array();
		$where['goods_code'] = $goods_code;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['specs_type'] = 0;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){	
			$data[$k]['name'] = $v['specs_value'];
			$data[$k]['buy_num'] = $v['specs_buy_num'];
			$data[$k]['options'] = $this->getSpecsChildFormat($v['specs_id'],$lang,$v['specs_value']);
		}		
		return $data;
		
	}
	
	private function getSpecsChildFormat($specs_id,$lang,$specs_name){
		
		$model = model('store_goods_specs');	
		$where = array();
		$where['specs_parent_id'] = $specs_id;
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$list = $model->getSpecsList($where);
		$data = array();
		foreach($list as $k => $v){						
			$data[$k]['name'] = $specs_name;			
			$data[$k]['value'] = $v['specs_value'];
			$data[$k]['price'] = $v['specs_price'];
			$data[$k]['specs_id'] = $v['specs_id'];
		}		
		return $data;
	}
	
	
	
	
	
	
	private function goodsOptional($code,$lang ='ENG'){
		   
		   $where = array();
		   $where['goods_code'] = $code;
		   $where['is_old'] = 0;
		   $where['goods_code_num'] = 0;
		   $options_list = model('store_goods_options')->getOptionsList($where);
		   $data = array();	   
		   $size_model = model('store_goods_size');
		   $specs_model = model('store_goods_specs');
		   $goods_lang_model = model('goods_language');
		   foreach($options_list as $k =>$v){			  
			   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang));
			   $data[$k]['size'] = $size;		   
			   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
			   $data[$k]['specs'] = $specs;		   
			   $data[$k]['price'] = $v['options_price'];		   
			   $goods_name = $goods_lang_model->getGoodsLangList(array('goods_code' => $v['options_goods_code']),'lang_id asc','goods_name,lang_name');		   
			   $data[$k]['goods_name'] = $goods_name;
			   $data[$k]['goods_code'] = $v['options_goods_code'];
		   }
		   
		   return $data;
		   
		   
	}
	
	/*
	//获取配件商品
	private function goodsOptional($optional,$store_id){
		
		$data = unserialize($optional);
		foreach($data as $v){
			$ids .=$v['options_id'].',';		
		}
		$ids = substr($ids,0,-1);		
		$where = array();
		$where['options_id'] = array('in',$ids);
		$where['state'] = 0 ;
		$where['store_id'] = $store_id;		
		
		$list = model('goods_options')->getOptionsList($where);
		
		$optional = array();
		foreach($list as  $k=>$v){
			$optional[$k]['id'] = $v['options_id'];
			$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$store_id);
			$optional[$k]['price'] = $v['options_price'];
			
		}
		
		return $optional;
		
	}
	

	//格式化配件商品的语言包
	private function optionsLang($data,$storeId){
		
	    $langInfo = model('store_language')->getStoreLangInfo(array('store_id'=> $storeId,'language_id'=> $this->member_info['menu_language']));
		
		$data = unserialize($data);
	
		foreach($data as $v){
			$list[$v['lang_name']] = $v['options_name'];
		}	
		return $list[$langInfo['language_en']];
		
		
	}
	
	*/
	
	
	

	//格式化图片
	private function goodsImageFormat($image,$store_id){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['url'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS .$v;		
		}
		return $list;		
	}
	
	
	
	//格式化数组 弃用
	private function FormatData($format_data,$type = '',$attr_name){
		
	
		$data = array();		
		$result = array(); 
		if(!is_array($format_data)){			
			return array();			
		}		
		foreach ($format_data as $data) { 			
			if($data['name'] != ''){
			  isset($result[$data['name']]) || $result[$data['name']] = array(); 
			  $result[$data['name']]['name'] = $data['name'].$attr_name;			  			  
			 $arr['name'] = $data['name'];
				 if($type == 'specs'){ 
					 $arr['value'] = $data['value'];
				 }
			  $arr['price'] = $data['price'];
			  $result[$data['name']]['options'][] = $arr;
			} 
		}		
		$data = array();
		foreach($result as $v){
			$data[] = $v;
		}		
		return $data;		
	}
	
	
	//统计购物车数量
	private function cart_count($goods_id,$store_id,$type = 0){
		
		$where = array();
		$where['buyer_id'] = $this->member_info['member_id'];
		if($type == 0){
			$where['goods_id'] = $goods_id;
		}
		if($type == 1){
			$where['goods_class_id'] = $goods_id;
		}
		$where['store_id'] = $store_id;
		
		$count = model('cart')->countCart($where,'SUM(goods_num) as count');
		return $count['count'] > 0 ?  $count['count'] :0 ;		
	}
	
	
	//获取商品语言
	private function goods_language($goods_id){		
		
		
		//验证当前用户的所选语言是否存在
		
		$where = array(
			'goods_id' =>$goods_id,
			'lang_name' => $this->member_info['menu_language'] ? $this->member_info['menu_language'] : 'ENG'
		);		
	
		$data = model('goods_language')->getGoodsLangInfo($where);
		
		switch ($data['lang_id']) {
			case 1:
				$data['spec_attr_name'] = "(必选)";
				break;
			case 2:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
			case 3:
				$data['spec_attr_name'] = "(Mandatory)";
				break;
		}	
		
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
		
	}
	
	//商品成分记录
	private function goods_ingr($icon_id){
				
		$data = array();		
		$where = array(
			'attr_id' => array('in',$icon_id)
		);		
		$row = model('goods_attr')->getGoodsAttrList($where);
		
		foreach($row as $v){
			
			
			
			if($v['attr_type'] == 1){			
				$content = explode('|',$v['attr_content']);
				$v['attr_name'] = $content[0];
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[1];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[2];
			}else{
				$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
				$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
				
			}
			
			
			$data[$v['attr_class_id']]['name'] = $v['attr_class_name'];
			$data[$v['attr_class_id']]['child'][] = $v;
		}		
		
		$list = array();
		foreach($data as $v){
			$list[] = $v;
		}		
		return $list;		
	}
	
	
	
	
	
    private function getGoodsFields()
    {
        return implode(',', array(
            'goods_id',
            'goods_commonid',
            'store_id',
            'store_name',
            'goods_name',
            'goods_price',
            'goods_promotion_price',
            'goods_promotion_type',
            'goods_marketprice',
            'goods_image',
            'goods_salenum',
            'evaluation_good_star',
            'evaluation_count',
            'is_virtual',
            'is_presell',
            'is_fcode',
            'have_gift',
            'goods_addtime',
        ));
    }

    /**
     * 处理商品列表(抢购、限时折扣、商品图片)
     */
    private function _goods_list_extend($goods_list) {
        //获取商品列表编号数组
        $goodsid_array = array();
        foreach($goods_list as $key => $value) {
            $goodsid_array[] = $value['goods_id'];
        }

        $sole_array = Model('p_sole')->getSoleGoodsList(array('goods_id' => array('in', $goodsid_array)));
        $sole_array = array_under_reset($sole_array, 'goods_id');

        foreach ($goods_list as $key => $value) {
            $goods_list[$key]['sole_flag']      = false;
            $goods_list[$key]['group_flag']     = false;
            $goods_list[$key]['xianshi_flag']   = false;
            if (!empty($sole_array[$value['goods_id']])) {
                $goods_list[$key]['goods_price'] = $sole_array[$value['goods_id']]['sole_price'];
                $goods_list[$key]['sole_flag'] = true;
            } else {
                $goods_list[$key]['goods_price'] = $value['goods_promotion_price'];
                switch ($value['goods_promotion_type']) {
                    case 1:
                        $goods_list[$key]['group_flag'] = true;
                        break;
                    case 2:
                        $goods_list[$key]['xianshi_flag'] = true;
                        break;
                }

            }

            //商品图片url
            $goods_list[$key]['goods_image_url'] = cthumb($value['goods_image'], 360, $value['store_id']);

            unset($goods_list[$key]['goods_promotion_type']);
            unset($goods_list[$key]['goods_promotion_price']);
            unset($goods_list[$key]['goods_commonid']);
            unset($goods_list[$key]['nc_distinct']);
        }

        return $goods_list;
    }

    /**
     * 商品评价
     */
    public function store_creditOp() {
        $store_id = intval($_GET['store_id']);
        if ($store_id <= 0) {
            output_error('参数错误');
        }
        $store_online_info = Model('store')->getStoreOnlineInfoByID($store_id);
        if (empty($store_online_info)) {
            output_error('店铺不存在或未开启');
        }

        output_data(array('store_credit' => $store_online_info['store_credit']));
    }
    /**
     * 店铺商品排行
     */
    public function store_goods_rankOp()
    {
        $store_id = (int) $_REQUEST['store_id'];
        if ($store_id <= 0) {
            output_data(array());
        }
        $ordertype = ($t = trim($_REQUEST['ordertype']))?$t:'salenumdesc';
        $show_num = ($t = intval($_REQUEST['num']))>0?$t:10;

        $where = array();
        $where['store_id'] = $store_id;
        // 默认不显示预订商品
        $where['is_book'] = 0;
        // 排序
        switch ($ordertype) {
            case 'salenumdesc':
                $order = 'goods_salenum desc';
                break;
            case 'salenumasc':
                $order = 'goods_salenum asc';
                break;
            case 'collectdesc':
                $order = 'goods_collect desc';
                break;
            case 'collectasc':
                $order = 'goods_collect asc';
                break;
            case 'clickdesc':
                $order = 'goods_click desc';
                break;
            case 'clickasc':
                $order = 'goods_click asc';
                break;
        }
        if ($order) {
            $order .= ',goods_id desc';
        }else{
            $order = 'goods_id desc';
        }
        $model_goods = Model('goods');
        $goods_fields = $this->getGoodsFields();
        $goods_list = $model_goods->getGoodsListByColorDistinct($where, $goods_fields, $order, 0, $show_num);
        $goods_list = $this->_goods_list_extend($goods_list);
        output_data(array('goods_list' => $goods_list));
    }
    /**
     * 店铺商品上新
     */
    public function store_new_goodsOp(){
        $store_id = (int) $_REQUEST['store_id'];
        if ($store_id <= 0) {
            output_data(array('goods_list'=>array()));
        }
        $show_day = ($t = intval($_REQUEST['show_day']))>0?$t:30;
        $where = array();
        $where['store_id'] = $store_id;
        $where['is_book'] = 0;//默认不显示预订商品
        $stime = strtotime(date('Y-m-d',time() - 86400*$show_day));
        $etime = $stime + 86400*($show_day+1);
        $where['goods_addtime'] = array('between',array($stime,$etime));
        $order = 'goods_addtime desc, goods_id desc';
        $model_goods = Model('goods');
        $goods_fields = $this->getGoodsFields();
        $goods_list = $model_goods->getGoodsListByColorDistinct($where, $goods_fields, $order, $this->page);
        $page_count = $model_goods->gettotalpage();
        if ($goods_list) {
            $goods_list = $this->_goods_list_extend($goods_list);
            foreach($goods_list as $k=>$v){
                $v['goods_addtime_text'] = $v['goods_addtime']?@date('Y年m月d日',$v['goods_addtime']):'';
                $goods_list[$k] = $v;
            }
        }
        output_data(array('goods_list' => $goods_list),mobile_page($page_count));
    }
    /**
     * 店铺简介
     */
    public function store_introOp()
    {
        $store_id = (int) $_REQUEST['store_id'];
        if ($store_id <= 0) {
            output_error('参数错误');
        }
        $store_online_info = Model('store')->getStoreOnlineInfoByID($store_id);
        if (empty($store_online_info)) {
            output_error('店铺不存在或未开启');
        }
        $store_info = $store_online_info;
        //开店时间
        $store_info['store_time_text'] = $store_info['store_time']?@date('Y-m-d',$store_info['store_time']):'';
        // 店铺头像
        $store_info['store_avatar'] = $store_online_info['store_avatar']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_online_info['store_avatar']
            : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
        //商品数
        $store_info['goods_count'] = (int) $store_online_info['goods_count'];
        //店铺被收藏次数
        $store_info['store_collect'] = (int) $store_online_info['store_collect'];
        //店铺所属分类
        $store_class = Model('store_class')->getStoreClassInfo(array('sc_id' => $store_info['sc_id']));
        $store_info['sc_name'] = $store_class['sc_name'];
        //如果已登录 判断该店铺是否已被收藏
        if ($member_id = $this->getMemberIdIfExists()) {
            $c = (int) Model('favorites')->getStoreFavoritesCountByStoreId($store_id, $member_id);
            $store_info['is_favorate'] = $c > 0?true:false;
        } else {
            $store_info['is_favorate'] = false;
        }
        // 是否官方店铺
        $store_info['is_own_shop'] = (bool) $store_online_info['is_own_shop'];
        // 页头背景图
        $store_info['mb_title_img'] = $store_online_info['mb_title_img'] ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_online_info['mb_title_img'] : '';
        // 轮播
        $store_info['mb_sliders'] = array();
        $mbSliders = @unserialize($store_online_info['mb_sliders']);
        if ($mbSliders) {
            foreach ((array) $mbSliders as $s) {
                if ($s['img']) {
                    $s['imgUrl'] = UPLOAD_SITE_URL.DS.ATTACH_STORE.DS.$s['img'];
                    $store_info['mb_sliders'][] = $s;
                }
            }
        }
        output_data(array('store_info' => $store_info));
    }
	
	
	
	
    /**
     * 店铺促销活动
     */
    public function store_promotionOp(){
        $param = $_REQUEST;
        $store_id = (int) $param['store_id'];
        if ($store_id <= 0) {
            output_error('参数错误');
        }
        $fields_arr = array('mansong','xianshi');
        $fields_str = trim($param['fields']);
        if ($fields_str) {
            $fields_arr = explode(',',$fields_str);
        }
        $promotion_arr = array();
        if (in_array('mansong',$fields_arr)) {
            //满就送
            $mansong_info = Model('p_mansong')->getMansongInfoByStoreID($store_id);
            if ($mansong_info) {
                $mansong_info['start_time_text'] = date('Y-m-d',$mansong_info['start_time']);
                $mansong_info['end_time_text'] = date('Y-m-d',$mansong_info['end_time']);
                foreach($mansong_info['rules'] as $rules_k=>$rules_v){
                    $rules_v['goods_image_url'] = cthumb($rules_v['goods_image'], 60);
                    $rules_v['price'] = ncPriceFormat($rules_v['price']);
                    if ($rules_v['discount']) {
                        $rules_v['discount'] = ncPriceFormat($rules_v['discount']);
                    }
                    $mansong_info['rules'][$rules_k] = $rules_v;
                }
                $promotion_arr['mansong'] = $mansong_info;
            }
        }
        if (in_array('xianshi',$fields_arr)) {
            //限时折扣
            $where = array();
            $where['store_id'] = $store_id;
            $where['state'] = 1;
            $where['start_time'] = array('elt', TIMESTAMP);
            $where['end_time'] = array('egt', TIMESTAMP);
            $xianshi_list = Model('p_xianshi')->getXianshiList($where, 0, 'xianshi_id asc', '*', 1);
            if ($xianshi_list) {
                $xianshi_info = $xianshi_list[0];
                $xianshi_info['start_time_text'] = date('Y-m-d',$xianshi_info['start_time']);
                $xianshi_info['end_time_text'] = date('Y-m-d',$xianshi_info['end_time']);
                $promotion_arr['xianshi'] = $xianshi_info;
            }
        }
        output_data(array('promotion' => $promotion_arr));
    }
	
	
	
	
	public function store_evaluateOp() {
        $store_id = intval($_POST['store_id']);
        $type = intval($_POST['type']);
        $condition = array();
        $condition['store_evaluate_storeid'] = $store_id;
        switch ($type) {
            case '1':
                $condition['store_evaluate_score'] = array('in', '1');
                break;
            case '2':
                $condition['store_evaluate_score'] = array('in', '2');
                break;
            case '3':
                $condition['store_evaluate_score'] = array('in', '3');
                break;
        	 case '0':
                $condition['store_evaluate_score'] = array('in', '1,2,3');
                break;
        }
		
		$score = array(
			1 => '不喜欢',
			2 => '一般',
			3 => '喜欢'
		);
        
        //查询商品评分信息
        $model_evaluate = Model("evaluate_store");
        $data_list = $model_evaluate->getEvaluateStoreList($condition, 20);
		$store_evaluate = array();
		$member =model('member');
		foreach($data_list as $k => $v){
			$member_avatar = $member->getMemberInfo(array('member_id'=> $v['store_evaluate_userid']),'member_avatar');
			
			$store_evaluate[$k] = $v;
			$store_evaluate[$k]['store_evaluate_addtime'] = date('Y/m/d',$v['store_evaluate_addtime']);
			$store_evaluate[$k]['store_evaluate_score_name'] = $score[$v['store_evaluate_score']];
			$store_evaluate[$k]['store_evaluate_useravatar'] = $this->getMemberAvatar($member_avatar);
			
			$store_evaluate[$k]['store_evaluate_image'] = $this->evaluateImage($v['store_evaluate_image']);
			
		}
		
      //  $goods_eval_list = Logic('member_evaluate')->evaluateListDity($goods_eval_list);

        $page_count = $model_evaluate->gettotalpage();
     	
		$count = $this->evaluateCount($store_id);
		
	 
	 
	    output_data(array('store_evaluate' => $store_evaluate,'store_evaluate_count'=>$count), mobile_page($page_count));
    }
	
	
		//格式化评价图片
	private function evaluateImage($image){
		
		if(!empty($image)){
			
			$image =   explode(",", $image);	
			
			$images = array();
			foreach($image as $v){
				$images[] =  UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.$v;
				
			}
			
			return $images;
		}else{
			
			return array();
			
			
		}
		
		
		
	}
	
	
	
	
	private function getMemberAvatar($image){
		if(file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !=''){
			return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
		}else{
			return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
		}
	}
	
	private function evaluateCount($store_id){
		
		$model = model('evaluate_store');
		$total_count = $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id));
		$total_one = $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'1'));
		$total_two = $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'2'));
		$total_three = $model->getEvaluateStoreCount(array('store_evaluate_storeid'=> $store_id,'store_evaluate_score'=>'3'));
		
		
		return array(
			'evaluate_total_all' => $total_count,
			'evaluate_total_1' => $total_one,
			'evaluate_total_2' => $total_two,
			'evaluate_total_3' => $total_three
		);
		
		
	}
	
	//icon筛选菜单
	public function iconSearchOp(){		
		
		$where = array(
			'is_show' => 1
		);

		$lang_name = $this->member_info['member_language']; // $_POST['lang_name'];

			
		$class = model('goods_attr')->getClassList(TRUE);
		foreach($class as $k => $v){
			
			$lang = unserialize($v['gc_lang']);
			foreach($lang as $l){
				if($l['lang_name'] == $lang_name){
					$name = $l['class_name'];
				}
			}
			$data[$k]['name'] = $name;
			$data[$k]['child'] = $this->get_ingr_child($v['gc_id'],$lang_name);			
		}
				
		output_data($data);				
	}
	
	
	private function get_ingr_child($class_id,$lang_name){
		$where = array();
		$where['attr_class_id'] = $class_id;	
		$where['is_show'] = 1;
		$list =model('goods_attr')->getGoodsAttrList($where);
		foreach($list as $k=> $v){
			$data[$k]['attr_id'] = $v['attr_id'];
			$data[$k]['attr_icon'] = UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
			$data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
			$data[$k]['disabled'] = $v['disabled'];
			switch ($lang_name) {
				case 'CHN-S':
					$data[$k]['attr_name'] = $v['attr_name'];
					break;
				case 'CHN-T':
					$data[$k]['attr_name'] = $v['attr_name_chnt'];
					break;
				case 'ENG':
					$data[$k]['attr_name'] = $v['attr_name_eng'];
					break;
				default:
					$data[$k]['attr_name'] = $v['attr_name'];					
			}			
		}		
		return $data;
	}
		
}
