<?php
/**
 * 商家审批管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_examineControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	//数据统计

	public function examine_statOp() {	       
		$model_examine = Model('store_examine');	 
		$examine_count = array(
				'state_0' => $model_examine->getStoreExamineCount(array('store_id'=> $this->store_info['store_id'],'examine_state'=> 0)),
				'state_1' => $model_examine->getStoreExamineCount(array('store_id'=> $this->store_info['store_id'],'examine_state'=> 1)),
				'state_2' => $model_examine->getStoreExamineCount(array('store_id'=> $this->store_info['store_id'],'examine_state'=> 2))
		);
	
		output_data(array('stat' => $examine_count));
	
	}
		
	//审核列表
	public function examine_listOp(){			
			
			$model_examine = Model('store_examine');	
	        $condition = array();
	        if(!empty($_POST['keyword'])) {
	            $condition['examine_title|examine_old_data'] = array('like', '%'.$_POST['keyword'].'%');
	        }	       
	        $condition['store_id'] = $this->store_info['store_id'];	    
			
			$param = array('addtime');
			if (in_array($_POST['sortname'], $param) && in_array($_POST['sortorder'], array('asc', 'desc'))) {
				$order = $_POST['sortname'] . ' ' . $_POST['sortorder'];
			}
			
			$list = $model_examine->getStoreExamineList($condition, $this->page,$order);
			$page_count = $model_examine->gettotalpage();
			$list_count = $model_examine->gettotalnum();
			$status = $this->status();
			$data = array();
			foreach($list as $k => $v){				
				$data[$k] = $v;
				$data[$k]['addtime'] = date('Y-m-d H:i:s',$v['addtime']);
				$data[$k]['examine_state'] = $status[$v['examine_state']];			
			}			
			output_data(array('list' => $data), mobile_page($page_count,$list_count));
		}
		
		
		
	public function examine_infoOp(){
		$status = $this->status();
	    $model_examine = Model('store_examine');
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['examine_id'] = $_POST['examine_id'];
		$info = $model_examine->getStoreExamineInfo($where);
		if($info){
			$info['examine_state_name'] =  $status[$info['examine_state']];
		}
		
		output_data(array('info'=>$info));
	}
		
		
		
	private function status(){
			$data = array();
			$data[0] = '待审核';
			$data[1] = '已审核';
			$data[2] = '已驳回';
			return $data;
	}
		
		
		
		
	public function examine_stateOp(){
			
	
			$examine_id = $_POST['examine_id'];
			$type = $_POST['state']; //1 同意  2驳回
			
			
			if($type == 1){				
				$row = model('store_examine')->storeExamine($examine_id);
			}else{				
				$where = array(
					'store_id' => $this->store_info['store_id'],
					'examine_id' => $examine_id
				);
				$updata = array(
					'examine_state' => 2
				);						
				$row = model('store_examine')->editStoreExamine($updata,$where);
			}
			
			if($row){
				output_data('操作成功');				
			}else{
				output_error('操作失败');				
			}
			
		}
		
		
	
	
}
