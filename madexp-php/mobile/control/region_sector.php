<?php
/**
 * 
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class region_sectorControl extends mobileAdminControl {

    public function __construct(){
        parent::__construct();
    }


	//返回各种16变行。
	public function get_sector_listOp(){
		
		
		
		$data= array();		
		$region = model('region_sector')->getRegionList(array('region_enable' => 1));		
	//	$region_data = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
		//	$region_data[$v['region_id']] = $v;
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		
		$region_data_list = model('region_sector')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		output_data(array('map_region'=>$result,'region_list'=> $this->getRegionList(),'region_data'=>$region_data));
	}
	

	private function getRegionList(){
		$model = model('region');
		$region = $model->getRegionList(array('is_money'=> 1,'region_enable'=> 1));
		$list = array();
		foreach($region as $v){			
			$list[] =$v;
		}		
		return $list;
	}
	
	

	public function get_old_regionOp(){
		
		$model = model('region_sector');
		$region = $model->getRegionList(array('region_parent_id' => 0),'*','region_version');
		$list = array();
		foreach($region as $v){		
			$v['region_version_date'] = date('d/m/y',$v['region_version_date']);
			$v['region_version_time'] = $v['region_version_date'];
			$list[] =$v;
		}		
		output_data(array('list' => $list));
	}

	
	
	//获取扇形点 和 钱标区域
	public function  get_sector_point_listOp(){
		
			$data= array();			
			$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));		
		//	$region_data = array();
			$nav_data = array();
			$sector_data = array();
			foreach($region as $k => $v){	
				
				$feature[$k]['type'] = 'Feature';
				$feature[$k]['id'] = $v['region_id'];	
				$feature[$k]['properties']['color'] = $v['region_color'];
				$feature[$k]['properties']['name'] = $v['region_name'];			
				$coordinate = explode("|", $v['region_money_coordinate']);
				$geo = array();
				foreach($coordinate as  $kk => $vv){
					$geo_c = explode(",",$vv);
					$geo[$kk][0] = floatval($geo_c[0]);
					$geo[$kk][1] = floatval($geo_c[1]);				
				}		
				$feature[$k]['geometry']['type'] = 'Polygon';
				$feature[$k]['geometry']['coordinates'][0] = $geo;			
						
						
						
				//扇形点	 //显示打开的扇形点
				if($v['is_sector_point_show'] == 1){	
					$sector_point['properties']['iconSize'] = array(250,250);
					$sector_point['properties']['name'] = $v['region_name'];			
					$sector_point['properties']['id'] = $v['region_id'];
					$sector_point['properties']['icon'] = '';
					$sector_point['geometry']['type'] = 'Point';
					$sector_coordinate = explode(',',$v['region_sector_coordinate']);
					$sector_point['geometry']['coordinates'][0] = $sector_coordinate[0];						
					$sector_point['geometry']['coordinates'][1] = $sector_coordinate[1];	
					$sector_data[] = $sector_point;
				}
				
						
			}
					
			$data['type'] = 'FeatureCollection';
			$data['features'] = $feature;		
			
			$result['type'] = 'geojson';
			$result['data'] = $data;
			
			$region_data_list = model('region')->getRegionList(array('region_enable'=> 1));		
			$region_data = array();
			foreach($region_data_list as $v){			
				$v['options'] = '';
				$region_data[$v['region_id']] = $v;			
			}	
						
			output_data(array('area_list'=> $result,'sector_list'=>$sector_data,'area_data'=>$region_data));		
	}


	//切换扇形点显示
	public function switch_point_showOp(){		
		$where= array();
		$where['region_id'] = $_POST['region_id'];
		
		$data = array();
		$data['is_sector_point_show'] = $_POST['show'];
		
		$row =model('region')->editArea($data,$where);
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}

	public function switch_sector_showOp(){
		
		$where= array();
		$where['region_id'] = $_POST['region_id'];		
		$data = array();
		$data['is_sector_show'] = $_POST['show'];
		$row =model('region')->editArea($data,$where);
		
		
		
		$where= array();
		$where['region_money_id'] = $_POST['region_id'];		
		$data = array();
		$data['region_enable'] = $_POST['show'];
		$row =model('region_sector')->editArea($data,$where);
		
		
		
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
	}
	
	public function switch_area_showOp(){
		
		$where= array();
		if($_POST['region_id'] == 'all'){
			$where['region_city_id'] = 2;
			$where['region_type'] = 2;
		}else{
			$where['region_id'] = $_POST['region_id'];		
		}
		
		$data = array();
		$data['is_region'] = $_POST['show'];
		
	
		
		$row = model('region')->editArea($data,$where);
		
		if($row){
			output_data('保存成功');
		}else{
			output_error('保存失败');
		}
		
	}





	//调用大区
	public function get_area_regionOp(){
		
		
		$data= array();		
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_region' => 1));		
	//	$region_data = array();
		$feature = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
			//$region_data[$v['region_id']] = $v;
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		$region_data_list = model('region')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		//获取大区列表		
		output_data(array('map_region'	=>	$result, 'region_data'	=>	$region_data,'region_list'	=> 	$this->getProRegionList()));
	
	}
	
	
	private function getProRegionList(){
		$model = model('region');
		
		$where = array();
		$where['region_parent_id'] = 0;
		$where['region_enable'] = 1;
		
		
		$region = $model->getRegionList($where);
		$list = array();
		foreach($region as $v){	
			$v['child'] = $model->getRegionList(array('region_parent_id' => $v['region_id']));
			$v['region_small_num'] = count($v['child']);
			$list[] =$v;
		}
		
		return $list;
	}


	//保存编辑
	public function save_regionOp(){
			$model = model('region_sector');		
			$data =  json_decode($_POST['region_data'],true);
			//print_r($data);
			foreach($data as $k=>$v){				
				if(strlen($k) == 32){					
					$data = array(
						'region_name' =>  $v['region_name'],
						'region_coordinate' => $v['region_coordinate'],
						'region_color' => $v['region_color'],						
						'region_money_id' => $v['region_money_id'],
						'region_enable' => 1
					);							
					$model->addArea($data);						
				}else{
					$update = array(
						'region_name' =>  $v['region_name'],
						'region_coordinate' => $v['region_coordinate'],
						'region_color' => $v['region_color'],
						'region_money_id' => $v['region_money_id'],
						'region_enable' => 1
					);	
					
					$model->editArea($update,array('region_id' => $v['region_id']));	
				}
				
				if($v['options'] == 'del'){
					
									
					
				}
			}
			
			output_data('操作成功');
		
	}
	
	
	 
	private function childRegion($id, $data){
		
		$list = array();
		$i = 0;
		foreach($data as $v){			
			if($v['region_parent_id'] == $id){				
				$list[$i] = $v;
				$i++;
			}
		}
		
		return $list;
	}
	 
	 
	
	
	
	//新增大区
	public function add_big_nameOp(){
		
		$model = model('region_sector');
		
		$info = $model->getRegionInfo(array('region_enable' => 1));
		if(empty($info)){			
			output_error('缺少基础数据支持');			
		}
		
			
			
		$data=  array(
			'region_name' =>  $_POST['big_region_name'],		
			'region_type' => 1,
			'region_parent_id' => 0,			
			'region_version' => $info['region_version'],
			'region_version_text' => $info['region_version_text'],
			'region_version_date' => $info['region_version_date'],
			'region_enable' => 1
		);
	
		
		
		$row = $model->addArea($data);
		
		if($info){
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}	
		
	}
	
	//批量编辑名称
	public function edit_bigOp(){
		
		
		$data =  json_decode($_POST['region_data'],true);
		//print_r($data);
		if(empty($data)){			
			output_error('参数错误');
		}
		$model = model('region_sector');		
		foreach($data as $v){		
			
			$model->editArea(array('region_name'=>$v['region_name']),array('region_id'=>$v['region_id']));
		}				
		output_data('操作成功');
		
		
	}
	
	//删除
	public function del_sectorOp(){
		
		$model = model('region_sector');
		$id = intval($_POST['region_id']);
		
		$where = array(
			'region_id' => $id
		);
		$row = $model->delArea($where);
		
		if($row){		
			output_data('删除成功');
		}else{
			output_error('删除失败');			
		}
		
				
		
	}
	
	
}
