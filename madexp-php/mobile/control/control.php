<?php
/**
 * mobile父类

 */



defined('InMadExpress') or exit('Access Invalid!');

/********************************** 前台control父类 **********************************************/

class mobileControl{

    //客户端类型
    protected $client_type_array = array('android', 'wap', 'wechat', 'ios', 'windows');
    //列表默认分页数
    protected $page = 5;


    public function __construct() {
        Language::read('mobile');

        //分页数处理
        $page = intval($_POST['page']);
        
        if($page > 0) {
            $this->page = $page;
        }
        
           $post = serialize($_POST);
        Log::record($post,Log::POST);
        
        
    }
    
    
    //log
    
    
}

class mobileHomeControl extends mobileControl{
    public function __construct() {
        parent::__construct();
    }

    protected function getMemberIdIfExists()
    {
        $key = $_POST['key'];
        if (empty($key)) {
            $key = $_GET['key'];
        }

        $model_mb_user_token = Model('mb_user_token');
        $mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($key);
        if (empty($mb_user_token_info)) {
            return 0;
        }

        return $mb_user_token_info['member_id'];
    }
}

class mobileMemberControl extends mobileControl{

    protected $member_info = array();

    public function __construct() {
        parent::__construct();
        
		$agent = $_SERVER['HTTP_USER_AGENT']; 
		
		if (strpos($agent, "MicroMessenger") && $_GET["act"]=='auto') {	
		    
			$this->appId = C('app_weixin_appid');
			$this->appSecret = C('app_weixin_secret');		
			
        }else{
                        
			$model_mb_user_token = Model('mb_user_token');
			$key = $_POST['key'];
			
			if(empty($key)) {
				$key = $_GET['key'];
			}
			
			$mb_user_token_info = $model_mb_user_token->getMbUserTokenInfoByToken($key);
			
			
			if(empty($mb_user_token_info)) {
				output_error('请登录', array('login' => '0'));
			}

            $model_member = Model('member');
            $this->member_info = $model_member->getMemberInfoByID($mb_user_token_info['member_id']);

            if(empty($this->member_info)) {
                
                 output_error('请登录', array('login' => '0'));
                 
			} else {
			    
				$this->member_info['client_type'] = $mb_user_token_info['client_type'];
				$this->member_info['openid'] = $mb_user_token_info['openid'];
				$this->member_info['token'] = $mb_user_token_info['token'];
				$level_name = $model_member->getOneMemberGrade($mb_user_token_info['member_id']);
				$this->member_info['level_name'] = $level_name['level_name'];
				
				$this->member_info['grade'] = $this->getGrade($mb_user_token_info['member_id']);
				
			
			}
        }
    }
	
	
	private function getGrade($member_id){
		$where = array(
			'member_id' => $member_id
		);
		$row = model('admin')->getAdminInfoNew($where);
		if(!empty($row)){			
			if($row['admin_type'] == 0){
				return 1;
			}else{
				return 2;
			}
		}else{
				return 0;
		}		
	}
	

    public function getOpenId()
    {
        return $this->member_info['openid'];
    }

    public function setOpenId($openId)
    {
        $this->member_info['openid'] = $openId;
        Model('mb_user_token')->updateMemberOpenId($this->member_info['token'], $openId);
    }
}

class mobileSellerControl extends mobileControl{

    protected $seller_info = array();
    protected $store_info = array();


    public function __construct() {
        parent::__construct();

        $model_mb_seller_token = Model('mb_seller_token');

        $key = $_POST['key']?$_POST['key']:$_GET['key'];
        if(empty($key)) {
            output_error('请登录', array('login' => '0'));
        }

        $mb_seller_token_info = $model_mb_seller_token->getSellerTokenInfoByToken($key);
		
      //  print_r($mb_seller_token_info);		
		
        if(empty($mb_seller_token_info)) {
            output_error('请登录', array('login' => '0'));
        }
 
        $model_store = Model('store');        
    		
		$this->store_info = $model_store->getStoreInfo(array('store_id' => $mb_seller_token_info['seller_id']));
   
       
    }
}




class mobileWaiterControl extends mobileControl{

    protected $waiter_info = array();
    protected $store_info = array();


    public function __construct() {
        parent::__construct();

        $model_mb_waiter_token = Model('mb_waiter_token');
		
        $key = $_POST['key']?$_POST['key']:$_GET['key'];
        if(empty($key)) {
            output_error('请登录', array('login' => '0'));
        }

        $mb_waiter_token_info = $model_mb_waiter_token->getMbWaiterTokenInfoByToken($key);
		
		
		
        if(empty($mb_waiter_token_info)) {
            output_error('请登录', array('login' => '0'));
        } 
        $model_waiter = Model('waiter');
        
        $this->waiter_info = $model_waiter->getWaiterInfo(['distributor_id'=>$mb_waiter_token_info['waiter_id']]);
        $this->waiter_common_info = $model_waiter->getDeliveryCommInfo(['delivery_id' => $mb_waiter_token_info['waiter_id']]);

     //   print_r($this->waiter_info);
      //  print_r(array('distributor_id'=>$mb_waiter_token_info['waiter_id']));
    //	print_r(array('distributor_id'=>$mb_waiter_token_info['waiter_id']));
		
	
   
       
    }
}
