<?php
/**
 * 商家注销
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_logoutControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }

    /**
     * 注销
     */
    public function indexOp(){
        if(empty($_POST['seller_name']) || !in_array($_POST['client'], $this->client_type_array)) {
            output_error('参数错误');
        }
        $model_mb_seller_token = Model('mb_seller_token');
        if($this->store_info['store_login_name'] == $_POST['seller_name']) {
            $condition = array();
            $condition['seller_id'] = $this->store_info['store_id'];
            $model_mb_seller_token->delSellerToken($condition);
            $a = new stdclass;
		    output_data( $a );
        } else {
            output_error('参数错误');
        }
    }

}
