<?php
/**
 * 所有店铺分类

 */


defined('InMadExpress') or exit('Access Invalid!');

class shop_classControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }

    /*
     * 首页显示
     */
    public function indexOp(){
        $this->_get_shop_class_List();

    }

    private  function  _get_shop_class_List(){
        //获取自营店列表
        $model_store_class = Model("store_class");
	//如果只想显示自营店铺，把下面的//去掉即可
        //$condition = array(
         //   'is_own_shop' => 1,
        //);

        $lst = $model_store_class->getStoreClassList($condition);
        $new_lst = array();
        foreach ($lst as $key => $value) {

            $new_lst[$key]['sc_id'] = $lst[$key]['sc_id'];
            $new_lst[$key]['sc_name'] = $lst[$key]['sc_name'];
            $new_lst[$key]['sc_bail'] = $lst[$key]['sc_bail'];
            $new_lst[$key]['sc_sort'] = $lst[$key]['sc_sort'];
        }	
        output_data(array('class_list' => $new_lst));
    }
	
	
	//获取所有子类
	public function cuisineOp(){
		$cuisine = model('cuisine')->getCuisineList(TRUE);
		foreach($cuisine as $k=> $v){
			$list[$k] = $v;
			$list[$k]['child'] = $this->small_class($v['cuisine_id']);
		}
		output_data(array('small_class' => $list));
		
		
	}
	
	private function small_class($id){
		$where = array(
			'cuisine_id' =>  $id
		);
		
		
		$member_bind_class = model('member_bind_class')->getMemberBindClassList(array('member_id'=> $this->member_info['member_id']));
		
		
	
		$list = model('store_small_class')->getStoreSmallClassList($where);
		foreach($list as $k => $v){
			$data[$k] = $v;
			foreach($member_bind_class as $val){
				if($v['small_id'] == $val['bind_class_id']){
					$data[$k]['select'] = 1;
				}else{
					$data[$k]['select'] = 0;
				}
			}
						
		}
		return $data;
		
	}
	
}