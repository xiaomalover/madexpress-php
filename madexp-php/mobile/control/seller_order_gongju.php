<?php
/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class seller_order_gongjuControl extends mobileHomeControl
{
    public function __construct()
    {
        parent::__construct();		
		$_POST = json_decode(file_get_contents('php://input'),true);
		
    }

    
    //
    public function order_infoOp()
    {
        $order_id = intval($_POST['order_id']);
        
        if (!$order_id) {
            output_error('订单编号有误');
        }
        $model_order = Model('order');
        $condition = array();
        $condition['order_id'] = $order_id;
       // $condition['store_id'] = $_POST['store_id'];
    
        $order = $model_order->getOrderInfo($condition, array('order_common','order_goods','member','store'));
        
                
        if (empty($order)) {
            output_error('订单信息不存在');
        }
                
        $order_item = array();
        //从新格式化
        
        $order_user = array();
        $order_user['buyer_code'] 			= $order['buyer_code'];
        $order_user['buyer_name'] 			= $order['buyer_name'];
        $order_user['buyer_phone'] 			= $order['buyer_phone'];
        $order_user['buyer_comment'] 		= $order['buyer_comment'];
        $order_user['buyer_is_new'] 		= $order['buyer_is_new'];
        $order_user['distributor_name'] 	= $order['distributor_name'];
        $order_user['distributor_mobile'] = $order['distributor_mobile'];
        $order_user['state'] 			= $order['order_state'];
        $order_user['evaluation_state'] = $order['evaluation_state'];
        
        if ($order['evaluation_state'] == 1) {
            $model = model('evaluate_store');
            $where = array(
                'store_evaluate_orderid' => $order['order_id']
            );
            $eva = $model->getEvaluateStoreInfo($where);
            $order['evaluation_text'] = $eva['store_evaluate_score_text'];
            $order['evaluation_content'] = $eva['store_evaluate_score_content'];
        }
        
                    
        $order_item['order_user'] = $order_user;
                
        
        
                
                
        //订单商品,原始商品，不做任何修改
        $order_goods = array();
        $goods =  $order['extend_order_goods'];
        $goods_num = 0;
        foreach ($goods as $k => $v) {
            $goods_num += $v['goods_num'];
        }
        
        $order_goods['goods_list'] = $order['extend_order_goods'];
        $order_goods['goods_num'] = $goods_num;
        $order_item['order_goods'] = $order_goods;
        
        
        
    
    
        //获取当前订单的退款商品
        
            
        
        //退款商品	[售前]
        $order_presale_refund = array();
        $where = array();
        $where['order_id'] = $order['order_id'];
        $refund  = model('order_goods_refund')->getRefundList($where);
        
        $goods_num = 0;
        $goods_amount = 0;
        $refund_list = array();
        if (!empty($refund)) {
            foreach ($refund as $k => $v) {
                $v['goods_optional']= unserialize($v['goods_optional']);
                $refund_list[] = $v;
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        $order_presale_refund['goods_num'] = 	$goods_num;
        $order_presale_refund['goods_amount'] = $goods_amount;
        $order_presale_refund['goods_list'] = $refund_list;
        $order_item['order_presale_refund'] = $order_presale_refund;
        
            
        
        //替换商品
        $order_replace = array();
        $where = array();
        $where['order_id'] = $order['order_id'];
        $replace =  model('order_goods_replace')->getGoodsList($where);//$order['extend_order_goods_replace'];
        
        $replace_ids = array();
        foreach ($replace as $v) {
            $replace_ids[] = $v['rec_id'];
        }
        
        $replace_ids = 	implode(',', $replace_ids);
        
        $goods_num = 0;
        $goods_amount = 0;
		$replace_list = array();
        if (!empty($replace)) {
            $replace_data = $model_order->getOrderGoodsList(array('rec_id'=> array('in',$replace_ids)));
            foreach ($replace_data as $k => $v) {
                $v['goods_optional'] =  $v['goods_optional'] !='' ? unserialize($v['goods_optional']) : array();
                $replace_list[$k] = $v;
                $replace_list[$k]['replace_goods'] =  $this->order_replace_goods($v['rec_id']);
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        
        $order_replace['goods_num'] 	= 	$goods_num;
        $order_replace['goods_amount'] = 	$goods_amount;
        $order_replace['goods_list'] = $replace_list;
        $order_item['order_replace'] = $order_replace;
        
        
        
            
        //商家赠送商品
        $order_goods_give = array();
        $where = array();
        $where['order_id'] = $order['order_id'];
        $where['give_type'] = 0;
        $give =  model('order_goods_give')->getGiveList($where); //$order['extend_order_goods_give'];
        
        $give_list = array();
        $goods_num = 0;
        $goods_amount = 0;
        if (!empty($give)) {
            foreach ($give as $k => $v) {
                $v['goods_optional'] = unserialize($v['goods_optional']);
                $give_list[] = $v;
                $goods_num += $v['goods_num'];
                $goods_amount +=$v['goods_num'] * $v['goods_price'];
            }
        }
        
        $order_goods_give['goods_num'] = 	$goods_num;
        $order_goods_give['goods_amount'] = 	$goods_amount;
        $order_goods_give['goods_list'] 	= $give_list;
        $order_item['order_give'] = $order_goods_give;
                
        
                
        //商家赠送的优惠券
        $order_coupon_give = array();
        $order_coupon_give['order_coupon_give_id']		 = $order['order_coupon_give_id'];
        $order_coupon_give['order_coupon_give_price']	 = $order['order_coupon_give_price'];
        $order_coupon_give['order_coupon_give_name']	 = $order['order_coupon_give_name'];
        $order_item['order_coupon_give'] = $order_coupon_give;
        
        


        output_data($order_item);
    }
    

    private function order_replace_goods($rec_id)
    {
        $list =    model('order_goods_replace')->getGoodsList(array('rec_id'=>$rec_id));
        $data = array();
        foreach ($list as $v) {
            $v['goods_optional'] =  $v['goods_optional'] !='' ? unserialize($v['goods_optional']) : array();
            $data[] = $v;
        }
        return $data;
    }


    
    
    
    
    /*
    商家保存编辑
    */
    public function save_orderOp()
    {
        $model = model('order');
        $snapshot_model = model('order_snapshot');
        
        $order_id = $_POST['order_id'];
        
        $where = array();    
        $where['order_id'] = $order_id;
        $order_info = $model->getOrderInfo($where);


        $snapshot_amendment = array();
        //判断是否有赠送店铺优惠券
        if ($_POST['coupon_id'] > 0) {
            $t_id = intval($_POST['coupon_id']);
            if ($t_id <= 0) {
                output_error('店铺优惠券信息错误');
            }
            $model_voucher = Model('voucher');
            //验证是否可领取代金券
            $data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
                    
            //  print_r($data);
                    
            if ($data['state'] == false) {
                output_error($data['msg']);
            }
            try {
                $model_voucher->beginTransaction();
                //添加代金券信息
                $data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
                      
                      
                if ($data['state'] == false) {
                    throw new Exception($data['msg']);
                }
                        
                $voucher = $data['voucher'];
                        
                //更新订单优惠券
                //优惠券写入到优惠券表
                                
                $coupon = array();
                $coupon['order_id'] =  $order_id;
                $coupon['store_id'] = $_POST['store_id'];
                $coupon['coupon_code'] = $voucher['voucher_code'];
                $coupon['add_time'] = time();
                $coupon['coupon_price'] = $voucher['voucher_price'];
                $coupon['coupon_name'] = $voucher['voucher_title'];
                $coupon['coupon_id'] = $voucher['voucher_id'];
                $row = model('order_coupon')->addCoupon($coupon);
                if ($row) {
                    $snapshot_amendment[] ='赠送优惠券';
                }
                        
                $model_voucher->commit();
            } catch (Exception $e) {
                $model_voucher->rollback();
                output_error($e->getMessage());
            }
        }
        



        //生成订单快照
        $sphot = $snapshot_model->createSphot($order_info['order_id']);
        if ($sphot) {
            $order_num = $snapshot_model->getSnaOrderCount(array('order_id' => $order_info['order_id']));
        }
        
        
        //替换商品
        $replace_list = model('order_goods_replace')->getGoodsList(array('is_take_ffect'=> 0, 'order_id'=>$order_id));
        //判断是否有商品替换
        if (!empty($replace_list)) {
            $where = array();
            $where['is_take_ffect'] = 0;
            $where['order_id'] = $order_id;
          
            $update = array();
            $update['is_take_ffect'] = 1;
            $row = model('order_goods_replace')->editGoods($where, $update);
            if ($row) {
                $snapshot_amendment[] ='替换商品';
            }
        }
        
        
        //	print_r($where);
        
        
        //1阶段退款
        $refund_list = model('order_goods_refund')->getRefundList(array('is_take_ffect'=> 0, 'refund_stage'=>0, 'order_id'=>$order_id));
		
        //判断是否有退款
        if (!empty($refund_list)) {
			$refund_amount = 0;
			foreach($refund_list as $v){
				$refund_amount += $v['goods_price'] * $v['goods_num'];
			}
							//录入退款单			
			$data = array();
			$data['order_id'] = $order_info['order_id'];
			$data['order_sn'] = $order_info['order_sn'];
			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			$data['member_id'] = $order_info['buyer_id'];		
			$data['refund_type'] 	= 1;
			$data['refund_amount'] 	= $refund_amount;
		//	$data['refund_comment'] = $_POST['refund_comment'];
			$data['refund_stage'] = 0;
			//$data['refund_reason'] 	= $_POST['refund_reason'];	
			$data['role'] = 1; //用户申请的退款
			$data['add_time'] = time();							
			$refund_id = model('order_refund')->addRefund($data);	
			
			
			
			$where = array();
			$where['is_take_ffect'] = 0;
			$where['order_id'] = $order_id;        
			
			$update = array();
			$update['is_take_ffect'] = 1;
			$update['refund_id'] = $refund_id;
			$row = model('order_goods_refund')->editRefund($where, $update);
						
            if ($row) {
                $snapshot_amendment[] ='一阶段退款';
            }
			
			
			
			
        }
		
        $give_list = model('order_goods_give')->getGiveList(array('is_take_ffect'=> 0,'order_id'=>$order_id,'give_type'=>0));
        //判断是否有赠送商品
        if (!empty($give_list)) {
            $where = array();
            $where['is_take_ffect'] = 0;
            $where['order_id'] = $order_id;
             
            $update = array();
            $update['is_take_ffect'] = 1;                
            $row = model('order_goods_give')->editGive($where, $update);
            if ($row) {
                $snapshot_amendment[] ='赠送商品';
            }
        }
                            
        //生成退款记录
        $where = array();
        $where['order_id'] = $_POST['order_id'];      
        $update = array();
        $update['order_update_time'] = time();
        $update['is_old_num'] = $order_num + 1;
        $update['snapshot_amendment'] = implode(',', $snapshot_amendment); //当前新订单做了哪些处理
		



		//更新订单数据，从新计算商品金额。
					
					
					
					
					
					
        $row = model('order')->editOrder($update, $where);
            
        if ($row) {
            output_data('编辑成功');
        } else {
            output_error('编辑失败');
        }
    }
    
    
    private function checkGoods($goods, $state)
    {
        $where = array();
        $where['goods_id']          = $goods['goods_id'];
        $where['state']             = $state;
        $where['goods_spec']        = $goods['goods_spec'];
        $where['goods_size']        = $goods['goods_size'];
        $where['goods_optional']    = $goods['goods_optional'];
        
        return model('order')->getOrderGoodsInfo($where);
    }
    
    
    //编辑订单商品信息
    public function goodsUpdateOp()
    {
        $type = $_POST['type'];
        $rec_id = $_POST['rec_id'];
        $order_id = $_POST['order_id'];
        
        $where = array();
        $where['rec_id'] = $rec_id;
        $where['order_id'] = $order_id;
                
        $rec_info = model('order')->getOrderGoodsInfo($where);
        $goods_info = model('goods')->getGoodsInfo(array('goods_id'=>$rec_info['goods_id']));
        
        $model_refund = model('order_goods_refund');
        if ($rec_info) {
            $update  = array();
            //增加数量，撤回退款商品
            if ($type == 'plus') {
                $plus = $this->delGoodsRefund($rec_info);
                if (!$plus) {
                    output_error('已无可撤回商品');
                } else {
                    $this->editGoodsUsable($rec_info, $type);
                    output_data('操作成功');
                }
            }            
            //减少数量，增加退款商品
            if ($type == 'reduce') {
                $reduce = $this->addGoodsRefund($rec_info);
                if (!$reduce) {
                    output_error('可退款商品数量已达上限');
                } else {
                    $this->editGoodsUsable($rec_info, $type);
                    output_data('操作成功');
                }
            }
        } else {
            output_error('商品不存在或参数错误');
        }
    }
    
    
    //写入退款商品
    private function addGoodsRefund($data)
    {
        $model = model('order_goods_refund');
        //验证是否存在这个商品
        $check = $this->checkRefund($data);
        
        //print_r($check);
        
        if (!empty($check)) {
            if (($check['goods_num'] + 1) > $data['goods_num']) {
                return false;
            }            
            $where = array();
            $where['rec_id'] = $data['rec_id'];
            $where['order_id'] = $data['order_id'];
            $update = array();
            $update['goods_num'] = array('exp','goods_num + 1');
            $model->editRefund($where, $update);
            return true;
			
        } else {
			
			$data['goods_num'] = 1;            
            unset($data['goods_usable_num']);            
            $model->addRefund($data);        
            return true;
        }
    }
    
    
    //更新主订单商品的可用商品数数量
    private function editGoodsUsable($data, $type)
    {
        
        //更新主订单
        $where = array(
            'rec_id' => $data['rec_id'],
            'order_id' => $data['order_id']
        );
        
        $info = model('order')->getOrderGoodsInfo($where);
        
        $data = array();
        if ($type == 'plus') {
            $data['goods_usable_num'] =  $info['goods_usable_num'] + 1;
        } else {
            $data['goods_usable_num'] =  $info['goods_usable_num'] - 1;
        }

        model('order')->editOrderGoods($data, $where);
    }
    
    
    
    //回退商品
    private function delGoodsRefund($data)
    {
        $model = model('order_goods_refund');
        $check = $this->checkRefund($data);
        //print_r($check);
        //验证商品
        if (!empty($check)) {
            if ($check['goods_num'] > 1) {
                $where = array();
                $where['rec_id']        = $data['rec_id'];
                $where['order_id']      = $data['order_id'];
                $update = array();
                $update['goods_num']    = array('exp','goods_num - 1');
                $model->editRefund($where, $update);
            } else {
                $where = array();
                $where['order_id']  = $data['order_id'];
                $where['rec_id']    = $data['rec_id'];
                $row = $model->delRefund($where);
            }
            
            return true;
        } else {
            return false;
        }
    }
    
    
    //获取退款的商品
    private function checkRefund($data)
    {
        $model = model('order_goods_refund');
        
        $where = array();
        $where['order_id'] = $data['order_id'];
        $where['rec_id'] = $data['rec_id'];
        
        //	print_r($where);
        $info = $model->getRefundInfo($where);
        
        //print_r($info);
        return $info;
    }
    
    
    //回退，删除整个
    
    public function del_refund_goodsOp()
    {
        $model = model('order_goods_refund');
        $model_order = model('order');
        $where = array();
        $where['order_id'] = intval($_POST['order_id']);
        
        if (intval($_POST['rec_id']) > 0) {
            $where['rec_id'] = intval($_POST['rec_id']);
        }
        
        $result = $model->delRefund($where);
        
        if ($result) {
            
            //更新主商品数量
            $goods_list = $model_order->getOrderGoodsList($where);
            
            foreach ($goods_list as $v) {
                $model_order->editOrderGoods(array('goods_usable_num' => $v['goods_num']), array('rec_id'=>$v['rec_id']));
            }
            
            output_data('操作成功');
        } else {
            output_error('操作失败');
        }
    }
    
    
    //编辑订单备注
    
    public function update_store_commentOp()
    {
        $where = array(
            'order_id' => $_POST['order_id'],
            'store_id' => $_POST['store_id']
        );        
        $data = array(
            'store_comment' => $_POST['store_comment']
        );        
        $row  = model('order')->editOrder($data, $where);        
        if ($row) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }
    
    
    
    //取消编辑商品
    public function cancel_order_editOp()
    {
        $where = array(
            'order_id' => $_POST['order_id'],
            'store_id' => $_POST['store_id'],
            'is_take_ffect' => 0
        );
        
        //删除曾平
        $row = model('order_goods_give')->delGive($where);        
        //删除退款
        $row = model('order_goods_refund')->delRefund($where);
        //删除替换
        $row = model('order_goods_replace')->delGoods($where);        
        output_data('取消成功');
    }
}
