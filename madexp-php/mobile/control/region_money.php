<?php
/**
 * 
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class region_moneyControl extends mobileAdminControl {

    public function __construct(){
        parent::__construct();
    }





	public function get_regionOp(){
		$data= array();
		
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));		
	//	$region_data = array();
		$nav_data = array();
		$sector_data = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_money_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
		//	$region_data[$v['region_id']] = $v;
		
			//导航点
			
			
			$nav_data[$v['region_id']]['properties']['iconSize'] = array(30,30);
			$nav_data[$v['region_id']]['properties']['name'] = $v['region_name'];			
			$nav_data[$v['region_id']]['properties']['id'] = $v['region_id'];
			$nav_data[$v['region_id']]['properties']['icon'] = '';
			$nav_data[$v['region_id']]['geometry']['type'] = 'Point';
			$nav_coordinate = explode(',',$v['region_nav_coordinate']);
			$nav_data[$v['region_id']]['geometry']['coordinates'][0] = $nav_coordinate[0];						
			$nav_data[$v['region_id']]['geometry']['coordinates'][1] = $nav_coordinate[1];	
			
		
			
			//扇形点
			
			$sector_data[$v['region_id']]['properties']['iconSize'] = array(250,250);
			$sector_data[$v['region_id']]['properties']['name'] = $v['region_name'];			
			$sector_data[$v['region_id']]['properties']['id'] = $v['region_id'];
			$sector_data[$v['region_id']]['properties']['icon'] = '';
			$sector_data[$v['region_id']]['geometry']['type'] = 'Point';
			$sector_coordinate = explode(',',$v['region_sector_coordinate']);
			$sector_data[$v['region_id']]['geometry']['coordinates'][0] = $sector_coordinate[0];						
			$sector_data[$v['region_id']]['geometry']['coordinates'][1] = $sector_coordinate[1];	
			
					
			
		
		
		
		
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		
		
		//增加导航点，
	
		
		
		//扇形点
		
		
		
		
		
		
		$region_data_list = model('region')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		
		
		output_data(array('map_region'=>$result,'region_list'=> $this->getRegionList(),'region_data'=>$region_data,'nav_list'=>$nav_data,'sector_list'=>$sector_data));
		//echo json_encode(array('code' => 200,'data'=>$data));		
		
	}

	private function getRegionList(){
		$model = model('region');
		
		$where = array();
		$where['region_parent_id'] = 0;
		$where['region_enable'] = 1;
		
		
		$region = $model->getRegionList($where);
		$list = array();
		foreach($region as $v){
			$v['child'] = $model->getRegionList(array('region_parent_id' => $v['region_id'],'region_money_state' => 1));
			$v['region_small_num'] = count($v['child']);
			$list[] =$v;
		}
		
		return $list;
	}
	
	
	
	
	
	
	//单独触发多边形
	public function get_money_polygonOp(){		
			$data= array();			
			$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_money' => 1));		
		//	$region_data = array();
			foreach($region as $k => $v){			
				$feature[$k]['type'] = 'Feature';
				$feature[$k]['id'] = $v['region_id'];	
				$feature[$k]['properties']['color'] = $v['region_color'];
				$feature[$k]['properties']['name'] = $v['region_name'];			
				$coordinate = explode("|", $v['region_money_coordinate']);
				$geo = array();
				foreach($coordinate as  $kk => $vv){
					$geo_c = explode(",",$vv);
					$geo[$kk][0] = floatval($geo_c[0]);
					$geo[$kk][1] = floatval($geo_c[1]);				
				}		
				$feature[$k]['geometry']['type'] = 'Polygon';
				$feature[$k]['geometry']['coordinates'][0] = $geo;			
				
			//	$region_data[$v['region_id']] = $v;
			}
					
			$data['type'] = 'FeatureCollection';
			$data['features'] = $feature;
			
			$result['type'] = 'geojson';
			$result['data'] = $data;
		
			output_data(array('map_region'=>$result));
	}
	
	

	public function get_old_regionOp(){
				
		$model = model('region_money');
		$region = $model->getRegionList(array('region_parent_id' => 0),'*','region_version');
		$list = array();
		foreach($region as $v){		
			$v['region_version_date'] = date('d/m/y',$v['region_version_date']);
			$v['region_version_time'] = $v['region_version_date'];
			$list[] =$v;
		}		
		output_data(array('list' => $list));
	}


	//保存REGION //保留
	/*
	public function save_regionOp(){
		
		$model = model('region_money');
		$post = json_decode($_POST['region_data'],true);	
		$i = 0;
		foreach($post as  $v){
			if($v['region_parent_id'] == 0 ){
				
				$child= $this->childRegion($v['region_id'],$post);	
				
				$v['region_version'] = $_POST['region_version'];
				$v['region_version_text']= $_POST['region_version_text'];
				$v['region_version_date']= $_POST['region_version_date'];
	
				$data = array(
					'region_name' =>  $v['region_name'],
					'region_coordinate' => $v['region_coordinate'],
					'region_color' => $v['region_color'],
					'region_type' => $v['region_type'],
					'region_parent_id' => $v['region_parent_id'],
					'region_en' => $v['region_en'],
					'region_version' => $v['region_version'],
					'region_version_text' => $v['region_version_text'],
					'region_version_date' => time()					
				);								
				$row = $model->addArea($data);	
							
				foreach($child as $c){					
					$data = array(
						'region_name' =>  $c['region_name'],
						'region_coordinate' => $c['region_coordinate'],
						'region_color' => $c['region_color'],
						'region_type' => $c['region_type'],
						'region_parent_id' => $row,
						'region_version' => $v['region_version'],
						'region_version_text' => $v['region_version_text'],
						'region_version_date' => time()				
						
					);						
					$model->addArea($data);
					
				}
				
			}			
		}		
		///写入数据库
		output_data('保存成功');	
	}
	
	*/
	
	//保存编辑
	public function save_regionOp(){
			$model = model('region');		
			$data =  json_decode($_POST['region_data'],true);	
			
			//print_r($data);
			foreach($data as $v){	
				
				$point = $this->point($v['region_money_coordinate']);
				
				if($v['options'] == 'update'){					
					$update = array(						
						'region_money_coordinate' 	=> $v['region_money_coordinate'],
						'region_nav_coordinate' 	=> $point['nav'], //钱标导航点
						'region_sector_coordinate' 	=> $point['sector'], //扇形点		
						'region_money_state' 		=> 1,
						'is_money' 					=> 1
					);
										
										
										
				//	print_r($update);
					$where  = array(
						'region_id' => $v['region_id']
					);
					
					//print_r($where);
					
				//	print_r($v);
					
					$model->editArea($update,$where);	
					
					
				}
				
				if($v['options'] == 'add'){		
					
					//print_r($v);
					
					$data = array(
						'region_money_coordinate' 	=> $v['region_money_coordinate'],
						'region_nav_coordinate' 	=> $point['nav'], //钱标导航点
						'region_sector_coordinate' 	=> $point['sector'], //扇形点		
						'region_money_state' 		=> 1,
						'is_money' 					=> 1
					);
					$model->addArea($data);
				}
				
				
				
			}
			
			output_data('操作成功');
		
	}
	

	//格式坐标
	private function point($point){
		
		$point = explode('|',$point);		
		$data['nav'] = $point[0];
		$data['sector'] = $point[0];
		return $data;		
	}
	
	
	 
	private function childRegion($id, $data){
		
		$list = array();
		$i = 0;
		foreach($data as $v){			
			if($v['region_parent_id'] == $id){				
				$list[$i] = $v;
				$i++;
			}
		}
		
		return $list;
	}
	 
	 
	
	//启用版本
	public function region_enableOp(){
		
		$version = $_POST['version'];
		$date = $_POST['version_date'];
		$model = model('region_money');		
		$row = $model->editArea(array('region_enable' => 0),TRUE);
		if($row){			
			$where = array(
				'region_version' => $version,
				'region_version_date' => $date
			);
		
			/*
			增加触发
			*/
			
		
			/*
			
			*/
		
			$model->editArea(array('region_enable' => 1),$where);									
			output_data('修改成功');
		}else{
			output_error('修改失败');
		}
		
		
		
	}
	 
	
	
	//新增大区
	public function add_big_nameOp(){
		
		$model = model('region_money');
		
		$info = $model->getRegionInfo(array('region_enable' => 1));
		if(empty($info)){			
			output_error('缺少基础数据支持');			
		}
		
		
		$data=  array(
			'region_name' =>  $_POST['big_region_name'],		
			'region_type' => 1,
			'region_parent_id' => 0,			
			'region_version' => $info['region_version'],
			'region_version_text' => $info['region_version_text'],
			'region_version_date' => $info['region_version_date'],
			'region_enable' => 1
		);
	
		
		$row = $model->addArea($data);
		
		if($info){
			output_data('保存成功');
		}else{
			output_error('保存失败');			
		}	
		
	}
	
	//批量编辑名称
	public function edit_bigOp(){
		
		
		$data =  json_decode($_POST['region_data'],true);
		//print_r($data);
		if(empty($data)){			
			output_error('参数错误');
		}
		$model = model('region_money');		
		foreach($data as $v){		
			
			$model->editArea(array('region_name'=>$v['region_name']),array('region_id'=>$v['region_id']));
		}				
		output_data('操作成功');
		
		
	}
	
	//删除,其实就是修改一下。
	
	public function del_bigOp(){
		
		$model = model('region');		
		$id = intval($_POST['region_id']);		
		$where = array(
			'region_id' => $id
		);		
		$update = array(
			'region_money_coordinate' => '',
			'region_money_state' => 0,
			'is_money' => 0
			
		);
		$row = $model->editArea($update,$where);		
		if($row){						
			output_data('删除成功');
		}else{
			output_error('删除失败');			
		}
		
		
	}
	
	
	
	//操作这个区域是否在后台显示
	
	public function region_admin_showOp(){
		
		$where = array();
	
		if($_POST['region_id'] == 'all'){
			$where['region_city_id'] = 2;
			$where['region_type'] = 2;
		}else{
			$where['region_id'] = $_POST['region_id'];		
		}
		
		$data = array();
		$data['is_money'] = $_POST['show'];		
		$row = model('region')->editArea($data,$where);		
		if($row){
			output_data('成功');
		}else{
			output_error('失败');
		}
	}
	
	//操作这个区域是否在后台显示
	
	public function region_pro_showOp(){
		
		$where = array();
		if($_POST['region_id'] == 'all'){
			$where['region_city_id'] = 2;
			$where['region_type'] = 2;
		}else{
			$where['region_id'] = $_POST['region_id'];		
		}
		$data = array();
		$data['is_region'] = $_POST['show'];		
		$row = model('region')->editArea($data,$where);		
		if($row){
			output_data('成功');
		}else{
			output_error('失败');
		}
	}
	
	
	
	//保存导航
	public function region_nav_saveOp(){
		
		$where = array();
		$where['region_id'] = $_POST['region_id'];
		$data = array();
		$data['region_nav_coordinate'] = $_POST['region_nav_coordinate'];
		$data['region_nav_desc'] = $_POST['region_nav_desc'];
		$data['region_nav_image'] = $_POST['region_nav_image'];
		
		$row = model('region')->editArea($data,$where);
		if($row){
			output_data('成功');
		}else{
			output_error('失败');
		}
 		
		
	}
	//保存XX点
	public function region_sector_saveOp(){
		
		$where = array();
		$where['region_id'] = $_POST['region_id'];
		
		$data = array();
		$data['region_sector_coordinate'] = $_POST['region_sector_coordinate'];
		$row = model('region')->editArea($data,$where);
		if($row){
			output_data('成功');
		}else{
			output_error('失败');
		
		}
	}

	
	//调用大区
	public function get_area_regionOp(){
		$data= array();
		
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'is_region' => 1));		
	//	$region_data = array();
		$feature = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
			//$region_data[$v['region_id']] = $v;
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		$region_data_list = model('region')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		//获取大区列表
		
				
		
		output_data(array(
			'map_region'	=>	$result, //地图用
			'region_list'	=> 	$this->getProRegionList(), //列表用
			'region_select' => 	$this->getStateRegionList(), //下拉菜单
			'region_data'	=>	$region_data
		));
		//echo json_encode(array('code' => 200,'data'=>$data));		
		
	}
	
	
	
	private function getProRegionList(){
		$model = model('region');
		
		$where = array();
		$where['region_parent_id'] = 0;
		$where['region_enable'] = 1;
		
		
		$region = $model->getRegionList($where);
		$list = array();
		foreach($region as $v){	
			$v['child'] = $model->getRegionList(array('region_parent_id' => $v['region_id']));
			$v['region_small_num'] = count($v['child']);
			$list[] =$v;
		}
		
		return $list;
	}
	
	private function getStateRegionList(){
		$model = model('region');
		
		$where = array();
		$where['region_parent_id'] = 0;
		$where['region_enable'] = 1;		
		
		$region = $model->getRegionList($where);
		$list = array();
		foreach($region as $v){
			$where = array();		
			$where['region_money_state'] = 0;		
			$where['region_parent_id'] = $v['region_id'];			
			$v['child'] = $model->getRegionList($where);
			$v['region_small_num'] = count($v['child']);
			$list[] =$v;
		}
		
		return $list;
	}
	
	
	
}
