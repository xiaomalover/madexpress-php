<?php




defined('InMadExpress') or exit('Access Invalid!');
class mapControl extends mobileHomeControl{
  
    public function __construct() {      
        parent::__construct();
    }

	
	
	
	public function saveOp(){
		
		$data = array(
			'map_name' => $_POST['map_name'],
			'map_color' => $_POST['map_color'],
			'map_point' => $_POST['map_point'],
			'map_en' => $_POST['map_en'],
		);
		
		$map_id = $_POST['map_id'] ? $_POST['map_id'] : 0;
		if($map_id > 0){
			$where =array(
				'map_id'=> $map_id
			);
			$row = model('map')->editMap($where,$data);
			
		}else{
			
			$row = model('map')->addMap($data);
			
		}
		
		if($row > 0){
			output_data('ok'); 
		}else{
			output_error('no'); 
		}
		
		
	}
	
	public function map_listOp(){
		
			$map = model('region')->getRegionList(TRUE);			
		
			foreach($map as $k=> $v){
				$list[$k]["map_id"] = $v['region_id'];
				$list[$k]["map_name"] = $v['region_name'];
				$list[$k]["map_color"] = $v['region_color'];				
				$list[$k]["map_point"] = $this->pointsOp($v['region_coordinate']);
				$list[$k]["map_en"] =  $v['region_end'];
			}		
			
			output_data($list); 
		
	}
	
	public function pointsOp($points){
		
		$points= substr($points,0,-1);
		
		$list = explode("|", $points);
		
		$data = array();
		
		foreach($list as $k => $v){
			
			$p = explode(",",$v);
			
			$data[$k]['l']=$p[0];
			$data[$k]['r']=$p[1];			
		}
		
		return $data;
		
	}
	
	
	public function map_infoOp(){
		$map_id = $_POST['map_id'];
		$where = array(
			'map_id'=> $map_id
		);	
		$map = model('map')->getMapInfo($where);
				
		$map['map_point_old'] = $map['map_point'];
		$map['map_point'] = $this->pointsOp($map['map_point']);
	
		
		output_data($map); 		
	}
	
	
	public function delOp(){
		$map_id = $_POST['map_id'];
		$row = model('map')->delMap($map_id);
		 output_data($row); 		
	}
	
	
}
