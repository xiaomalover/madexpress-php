<?php
/**
 * 商户设置
 */


defined('InMadExpress') or exit('Access Invalid!');

class seller_settingControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();

    }

    //商户上下线
	public function  storeOnlineStateOp(){
		
		
		
		
		
		
	}
	
	//修改密码
	
	public  function storeEditPwdOp(){
		
		
			$old_pwd = $_POST['old_password'];
			$new_pwd = $_POST['new_password'];
		
			$obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["old_password"],"require"=>"true","message"=>'当前密码不能为空'),
                array("input"=>$_POST["new_password"],"require"=>"true","message"=>'新密码不能为空'),             
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                 output_error($error);
            }
		
			$where = array(
				'store_id' => $this->store_info['store_id']			
			);
			$row = model('store')->getStoreInfo($where);
			if($row){
				$data = array(
					'store_login_password' => md5($new_pwd)
				);			
				model('store')->editStore($data,$where);			
				output_data('ok');
			}else{
				output_error('当前密码错误');
			}

		
		
		
		
		
		
		
	}
	
	
	//修改营业时间 
	
	public function storeShopTimeOp(){
		
		
		
		
	}
	
	
	
	

}
