<?php
/**
 * 我的反馈
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_feedbackControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 添加反馈
     */
    public function feedback_addOp() {
		
		
		
		
        $model_feedback = Model('feedback');
      
		
		$param = array();
        $param['feedback_content'] = $_POST['content'];
        $param['user_type'] = 3;
		$param['feedback_code'] = $this->feedbackCode();
		$param['feedback_type'] = $_POST['feedback_type'];		
		$param['feedback_type_id'] = $_POST['feedback_type_id'];		
        $param['feedback_time'] = TIMESTAMP;
        $param['user_id']   = $this->waiter_info['distributor_id'];
        $param['user_name'] = $this->waiter_info['distributor_name'];
		$param['feedback_file'] = $_POST["feedback_file"];	
		
        $result = $model_feedback->addFeedback($param);

        if($result) {
            output_data('1');
        } else {
            output_error('保存失败');
        }
    }
	
	private function feedbackCode(){
		
		$length = 7;
		$count = model('feedback')->getFeedBackCount(TRUE);
        $len = $length - strlen($count+1);
        $code = strtoupper('MEO').str_pad($count+1,$len,0,STR_PAD_LEFT);
        return $code;
	   
		
	}
	
	
	public function updateFileOp(){
		
			$complain_pic = array();
	        $complain_pic[1] = 'file1';
	        $complain_pic[2] = 'file2';
	        $complain_pic[3] = 'file3';
	    	$complain_pic[4] = 'file4';
	    	$complain_pic[5] = 'file5';
	    	$complain_pic[6] = 'file6';
	    	$complain_pic[7] = 'file7';
	    	$complain_pic[8] = 'file8';
	    	$complain_pic[9] = 'file9';
	      
	        $pic_name = array();
	        $upload = new UploadFile();
	        $uploaddir = ATTACH_FEEDBACK;
	        $upload->set('default_dir',$uploaddir);
	        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
	        $count = 0;
	        foreach($complain_pic as $pic) {
	            if (!empty($_FILES[$pic]['name'])){
	                $result = $upload->upfile($pic);
	                if ($result){
	                    $pic_name[$count] = $upload->file_name;
	                    $upload->file_name = '';
	                } else {
	                    $pic_name[$count] = '';
	                }
	            }
	            $count++;
	        }
	        
	        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'));
	        
		
		
		
		
	}
	
	public function feedback_typeOp(){
	
	
	
	        $model_feedback = Model('feedback');
	        
	        $type_list = $model_feedback->getFeedBackType(array('type_user' => 2,'type_parent_id' => 0));
	        
	        foreach($type_list as $v){
	         
	            $v['child'] = $model_feedback->getFeedBackType(array('type_parent_id' => $v['type_id']));
	            
	            $type[] =  $v;
	            
	        }
	    
			
			output_data($type);
	
	/*
	        $data = array(
	          array(  'name'=>'商家不配合',
	            'child' => array('店铺关门','商家联系不上','商家有老鼠','商家不知道怎么了')
	            ),
	          array(  'name'=>'客户不配合',
	            'child' => array('客户不接电话','客户态度差')
	            ),
	           array(  'name'=>'其他问题',
	            'child' => array('车坏了','天气差')
	            ),
	       );
	
	
			
			output_data($data);
		*/
	}
	
	
	//反馈列表
	public function feedback_listOp(){
	    
	    $model_feedback = Model('feedback');
	    $condition = array();
	
	    $condition['user_id'] = $this->waiter_info['distributor_id'];    
		$condition['user_type'] = 3;
		
		$list = $model_feedback->getFeedBackList($condition, $this->page);	
		
		$page_count = $model_feedback->gettotalpage();
		$list_count = $model_feedback->gettotalnum();
		
		
		$data = array();
		foreach($list as $k => $v){
		    
		    $v['feedback_time'] = date('d M y H:i:s',$v['feedback_time']);
		    
			$data[$k] = $v;
		}
		output_data(array('list' => $data), mobile_page($page_count,$list_count));
	}
	
	//反馈详情
	
	public function feedback_infoOp(){
	    
	    $model_feedback = Model('feedback');
	    
	    $where = array();
	    $where['user_id'] = $this->waiter_info['distributor_id'];
	    $where['user_type'] = 3;
	    $where['id'] = $_POST['feedback_id'];
        $info = $model_feedback->getFeedBackInfo($where);
        
        
        
        if(!empty($info)){
            $info['feedback_time'] = date('d M y H:i:s',$info['feedback_time']); 
            $info['feedback_file'] = $this->feedback_image($info['feedback_file']);
            $info['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
            //
            $log_list = array();
            $log = $model_feedback->getFeedBackListLog(array('feedback_id' => $info['id']));
            
        
            foreach($log as $v){
                $v['log_file'] =  $this->feedback_image($v['log_file']);
                $v['log_addtime'] =  date('d M y H:i:s',$v['log_addtime']);
                $v['user_avatar'] = $this->getMemberAvatar($this->member_info['member_avatar']);
                $log_list[] = $v;
                
            }
            
            
            $info['log'] = $log_list;
            
            output_data(array('info' => $info));    
        }else{
            output_error('未获取到数据');
        }
	    
	}
	
		//格式化图片
	private function feedback_image($image){		
		$list = array();
		if(!empty($image)){			
			$image  =	explode(",", $image);			
			foreach($image as $v){				
				$list[] = UPLOAD_SITE_URL.'/'.ATTACH_FEEDBACK.'/'.$v;
			}			
			return $list;			
		}else{
			return $list;			
		}
	}
	
		//继续反馈
	public function feedback_add_logOp(){
	      $model_feedback = Model('feedback');
	    
	       $data = array();
	       $data['log_content'] = $_POST['log_content'];
	       $data['log_file'] = $_POST['log_file'];
	       $data['feedback_id'] = $_POST['feedback_id'];
	       $data['log_addtime'] = time();
	       $data['user_id'] = $this->waiter_info['distributor_id'];
	       $data['user_name'] = $this->waiter_info['distributor_name'];
	       $row = $model_feedback->addFeedBackLog($data);
	    
	     if($row){
            output_data('保存成功');    
        }else{
            output_error('保存失败');
        }
	    
	    
	    
	    
	}
	
	private function getMemberAvatar($image){
		if(file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_AVATAR.'/'.$image) && $image !=''){
			return UPLOAD_SITE_URL.'/'.ATTACH_AVATAR.'/'.$image;
		}else{
			return UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_user_portrait');
		}
	}
	
}
