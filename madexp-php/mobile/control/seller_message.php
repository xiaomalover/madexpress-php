<?php
/**
 * 我的消息
 */

defined('InMadExpress') or exit('Access Invalid!');
class seller_messageControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();
        $this->model_msg = Model('store_msg');
    }
	
	
    /**
     * 消息列表
     */
    public function indexOp() {
		
		$where = array('store_id'=>$this->store_info['store_id']);
		
	    $where['msg_readids'] = $_POST['msg_read'];
		
        $data = $this->model_msg->getStoreMsgList($where,$this->page);	
		
		$page_count = $this->model_msg->gettotalpage();
		$list_count = $this->model_msg->gettotalnum();
		
		
		
		
		$msg_list = array();
		foreach($data as $k => $v){
			$msg_list[$k]['msg_id'] = $v['msg_id'];
			$msg_list[$k]['msg_code'] = $v['msg_code'];
			$msg_list[$k]['msg_code_name'] = $v['msg_code_name'];
			$msg_list[$k]['msg_content'] = $v['msg_content'];	
			$msg_list[$k]['msg_readids'] = $v['msg_readids'];	
			$msg_list[$k]['msg_addtime'] = date('Y/m/d',$v['msg_addtime']);
		}
		
        output_data(array('msg_list' => $msg_list), mobile_page($page_count,$list_count));        
    }
	
	
	
	
	//消息详情
	public function infoMsgOp(){
		
		
		$where = array(
			'store_id' => $this->store_info['store_id'],
			'msg_id' => $_POST['msg_id'],
		);
		$info =  $this->model_msg->getStoreMsgInfo($where);	
			
		$data = array(
				'msg_readids' => 1			
			
		);
		  $this->model_msg->editStoreMsg($where,$data);		
			
		  output_data(array('info' => $info));        
	}
	
	
	//全部已读
	public function allStateOp(){
		
		$where = array();
		
		$where['store_id'] = $this->store_info['store_id'];
		if($_POST['msg_id'] > 0){
			$where['msg_id'] = $_POST['msg_id'];
		}
		$where['store_id'] = $this->store_info['store_id'];
		 $data = array(
				'msg_readids' => 1
		);
		$row = $this->model_msg->editStoreMsg($where,$data);		
			
		 output_data('ok');      
		
		
	}
	
	
	public function delMsgOp(){
		$where = array(
			'msg_id' => $_POST['msg_id'],
			'store_id' => $this->store_info['store_id']
		);
		$result = $this->model_msg->delStoreMsg($where);
		if($result){
			  output_data('ok');			
		}else{
		  output_data('删除失败');	
		}
		
	}
	
	public function readidsMsgOp(){
		
		$where = array(
			'store_id' => $this->store_info['store_id'],
			'msg_readids' => 0
		);
		$num = $this->model_msg->getStoreMsgCount($where);
		  output_data(array('readids_num' => $num));		
	}
	
	
	
	


}
