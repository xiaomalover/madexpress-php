<?php
/**
 * 商家收款

 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_collectioncontrol extends mobileSellerControl {

    private $payment_code;
    private $payment_config;

    public function __construct(){
        parent::__construct();
		
		$this->payment_code = 'stripe';
    }

	/**
	 * 写充值信息
	 */
	public function indexOp(){
	    $pdr_amount = abs(floatval($_POST['amount']));
		if ($pdr_amount <= 0) {		    
			output_error('金额不正确!');
		}
		else{
			$model_pdr = Model('predeposit');
			$data = array();
			$data['pdr_sn'] = $pay_sn = $model_pdr->makeSn();
		//	$data['pdr_member_id'] = $this->member_info['member_id'];
		//	$data['pdr_member_name'] = $this->member_info['member_name'];
			$data['pdr_amount'] = $pdr_amount;
			$data['pdr_add_time'] = TIMESTAMP;
			$data['store_id'] = $this->store_info['store_id'];
			$data['store_name'] = $this->store_info['store_name_primary'];
			$insert = $model_pdr->addPdRecharge($data);
			if ($insert) {
				output_data(array('pay_sn' => $pay_sn));
			}
			else{
				output_error('创建失败!');
			}
		}
		
	}
	
	//扫码收款
	public function scan_codeOp(){
		
		$model_pdr = Model('predeposit');		
		$member_code = $_POST['code'];		
		$where = array();
		$where['member_code'] = $member_code;
		$member_info = model('member')->getMemberInfo($where);
		if(empty($member_info)){
			output_error('会员不存在');
		}
		
		$where = array();
		$where['pdr_sn'] = $_POST['pay_sn'];
		$update = array();
		$update['pdr_member_id'] = $member_info['member_id'];
		$update['pdr_member_name'] = $member_info['member_name'];		
		$row = $model_pdr->editPdRecharge($update,$where);
		if($row){			
			
			$where = array();
			$where['pdr_sn'] = $_POST['pay_sn'];
			$order_info = $model_pdr->getPdRechargeInfo($where);
			
			$model = model('stoket');
			$data = array();
			$data['type'] = 'payment_notice';
			$data['data'] = $order_info;		
			$data = json_encode($data);	
			$model->sendUser($user_id,$data);
			
			output_data('等待付款');			
		}else{			
			output_error('操作失败');			
		}
		
	}


	public function listOp(){
		
		$model_pdr = Model('predeposit');	
		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['pdr_payment_state'] = 1;
		
		$data = $model_pdr->getPdRechargeList($where,$this->page,'*','pdr_id desc');
		foreach($data as $k => $v){
			$v['pdr_payment_time'] = date('d/m/y H:i:s',$v['pdr_payment_time']);
			$v['pdr_add_time'] = date('d/m/y H:i:s',$v['pdr_add_time']);
			$list[$k] = $v;			
		}	
		$page_count = $model_pdr->gettotalpage();
		output_data(array('list' => $list), mobile_page($page_count));	
		
	}

	public function infoOp(){		
		
		$model_pdr = Model('predeposit');	
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		$where['pdr_id'] = $_POST['id'];
		$row = $model_pdr->getPdRechargeInfo($where);
		
	
		if(!$row){
		    
			output_error('订单不存在');
		}
		
		$row['refund_list'] = $this->getRefundList($row['pdr_id']);
		$row['pdr_add_time'] = date('d/m/y H:i:s',$row['pdr_add_time']);
		
		
		output_data(array('info'=>$row));
			
	}
	
	
	public function getRefundList($id){
	    
	    $model_pdr = Model('predeposit');	
	    
	    $list = $model_pdr->getPdRechargeRefundList(array('pdr_id' => $id));
	    $data = array();
	    foreach($list as $k => $v){
	        $v['refund_add_time'] = date('d/m/y H:i:s',$v['refund_add_time']);
	        $v['refund_time'] = date('d/m/y H:i:s',$v['refund_time']);
	        $data[$k] = $v;
	    }
	    
	    
	    
	    return $data;
	    
	}
	
	
	
	
	    //退款操作
	    public function refundOp(){
	        
	        $model_pdr = Model('predeposit');	
			$where = array();
			$where['store_id'] = $this->store_info['store_id'];
			$where['pdr_id'] = $_POST['pdr_id'];
			$order_info = $model_pdr->getPdRechargeInfo($where);
			if(!$order_info){
				output_error('订单不存在');
			}
	        
	        $refund_amount = $_POST['amount'];
	        if($refund_amount > ($order_info['pdr_amount'] - $order_info['refund_amount']) ){
	            output_error('退款金额不能大于最大退款金额');
	        }
	        
	        
			
			//判断当前退款方式。
			
		
			//写入退款信息
			$refund_id = $this->_refund($order_info,$refund_amount);
									
	
		/*	if($order_info['pdr_payment_type'] == 0){ //客服用的钱包支付
				
				$this->_wallet_refund($order_info,$refund_amount,$refund_id);
								
			}
			
			if($order_info['pdr_payment_type'] == 1){ //客户用的stripe支付
				
				$this->_api_refund($order_info,$refund_amount,$refund_id);
				
			}
			
			if($order_info['pdr_payment_type'] == 2){ //客户用的组合支付。
				
				$this->_wallet_refund($order_info,$refund_amount,$refund_id);
				
				
			}        
	        */
	        if($refund_id){
	            output_data('退款已生成');    
	        }else{
	            output_error('退款失败');
	        }
	        
	        
	        
	        
	        
	    }
		
		
		//offline_payment_refund   predeposit 客户钱包操作
		//offline_sales_income_refund  store_wallet 商户钱包操作
		
		
		private function _wallet_refund($order_info,$refund_amount){ //钱包退款
			
			$store_wallet = model('store_wallet');
			$member_wallet = model('predeposit');
			
		
			//事务
		try {
		//	    
			    $store_wallet->beginTransaction();	
			
				//扣除商户钱包
				$data_store = array();
				$data_store['store_id'] = $order_info['store_id'];
				$data_store['store_name'] = $order_info['store_name'];
				$data_store['amount'] = $refund_amount;
				$data_store['order_sn'] = $order_info['pdr_sn'];
				$data_store['order_id'] = $order_info['pdr_id'];
				$store_wallet->changePd('offline_sales_income_refund',$data_store);
			    
			 //   print_r($data_store);	
				
				//写入用户钱包
				$data_member = array();
				$data_member['member_id'] = $order_info['pdr_member_id'];
				$data_member['member_name'] = $order_info['pdr_member_name'];
				$data_member['amount'] = $refund_amount;
				$data_member['order_sn'] = $order_info['pdr_sn'];
				$data_member['order_id'] = $order_info['pdr_id'];
				$member_wallet->changePd('offline_payment_refund',$data_member);
							
			//	 print_r($data_member);	
				
			
				$store_wallet->commit();
				return callback(true,'',$order_info);
				
			}catch (Exception $e){
			    $store_wallet->rollback();
			    return callback(false, $e->getMessage());
			}			
			
			//第一步扣除商家钱包，
			//写入用户钱包，
			//第三步生成退款记录
			//更新订单状态
					
		}
		
		
		
	
		public function _api_refundOp($order_info,$refund_amount){//第三方退款
			
			$inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
			if(!is_file($inc_file)){
			    output_error('支付接口不存在');
			}
			//print_r($order_info);
			require($inc_file);	       
			// Set your secret key. Remember to switch to your live secret key in production.
			// See your keys here: https://dashboard.stripe.com/account/apikeys
			/*\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');
			
			$charge = \Stripe\Refund::create([
			  'amount' => 1000,
			  'payment_intent' => $_POST['sn'],
			]);
			
			print_r($charge);
			*/
			\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');
			
			$refund = \Stripe\Refund::create([
			  'charge' => $order_info['pdr_trade_sn'],
			   'amount' => $refund_amount * 100,
			]);
		    	
		
		}
		
		
		public function _refund($order_info,$refund_amount){ //生成退款记录
		
			$model = model('predeposit');
			$data = array();
			$data['pdr_id'] = $order_info['pdr_id'];
			$data['pdr_sn'] = $order_info['pdr_sn'];
			$data['pdr_member_id'] = $order_info['pdr_member_id'];
			$data['pdr_member_name'] = $order_info['pdr_member_name'];
			$data['refund_amount'] = $refund_amount;			
			$data['refund_add_time'] = time();			
			$data['refund_time'] = time();			
			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			$data['refund_type'] = $order_info['pdr_payment_type'];
			
			
			//判断当前退款方式
			//0 钱包 1 stripe 2 钱包+stripe
			$data['refund_wallet_amount'] = 0;
			$data['refund_stripe_amount'] = 0;
			
			
			if($order_info['pdr_payment_type'] == 0){
			    
			    
			    	$data['refund_wallet_amount'] = $refund_amount;
			    	
			}elseif($order_info['pdr_payment_type'] == 1){
			    
			        $data['refund_stripe_amount'] = $refund_amount;
			        
			}elseif($order_info['pdr_payment_type'] == 2){
			    
			        if($refund_amount > $order_info['pdr_wallet_amount']){
			           
			            $data['refund_wallet_amount'] = $order_info['pdr_wallet_amount']; //优先退款
		            	$data['refund_stripe_amount'] = $refund_amount - $order_info['pdr_wallet_amount'];
		        	
			        }else{
			            
			            $data['refund_wallet_amount'] = $order_info['pdr_wallet_amount']; //优先退款
		            	$data['refund_stripe_amount'] = 0;
			            
			        }
			}
			$data['refund_reason'] = $_POST['reason'];
			$data['reason_content'] = $_POST['content'];	
			$refund_id = $model->addPdRechargeRefund($data);
			
			if($refund_id){
			    
			    
			    
			    if($data['refund_wallet_amount'] > 0){
			    //执行钱包退款
			      $wallet =   $this->_wallet_refund($order_info,$data['refund_wallet_amount']);
			    }
			    
			    //执行stripe 退款
			    if($data['refund_stripe_amount'] > 0){
			       $stripe =  $this->_api_refund($order_info,$data['refund_stripe_amount']);
			    }
			    
			    
			    
			    
			}
			
			
			
			
			
			
			return $refund_id;
		}






}
?>