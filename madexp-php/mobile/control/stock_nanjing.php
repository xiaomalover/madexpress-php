<?php 
class stock_nanjingControl extends SystemnewControl {
   
    public function __construct(){
        parent::__construct();      
		$_POST = json_decode(file_get_contents('php://input'),true);	   	
		  
    }


    public function indexOp() {


		
		Tpl::setDirquna('shop');
        Tpl::showpage('stocknew.nanjing');
    
    }

	//拆分出的地图
	public function mapOp(){
		
		
	}
	
	
	
	public function memberOp(){
		
		
		
		Tpl::setDirquna('shop');
		Tpl::showpage('stocknew.member');
		    
		
	}
	
	public function get_memberOp(){
		
		$member = model('ceshi_user')->getAddressList(TRUE);
		foreach($member as $k => $v ){
		
			$v['order_list'] = $this->get_member_order($v['member_id']);
			$member_data[$k] = $v;
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['name'] = $v['address_nickname'];			
			$feature[$k]['properties']['id'] = $v['weizhi_id'];			
			$feature[$k]['geometry']['type'] = 'Point';
		
			$feature[$k]['geometry']['coordinates'][0] = $v['lng'];			
			$feature[$k]['geometry']['coordinates'][1] = $v['lat'];		
		
		}
		
		$member_list = array();
		$member_list['type'] = 'FeatureCollection';
		$member_list['features'] = $feature;	
			
		
		
		output_data(array('member_point' => $member_list,'member_list' => $member_data));
		
	}
	
	public function get_member_order($id){
		
		$where = array();
		$where['buyer_id'] = $id;
		$where['order_state'] =  array('in','20,30,40,50');		
		$order = model('order')->getOrderListNew($where,'order_sn,order_id,store_name','order_id asc');		
		
		return $order;
		
	}
	
	
	public function edit_memberOp(){
		
		$_POST = json_decode(file_get_contents('php://input'),true);
		$where= array(
			'member_id' => $_POST['member_id']
		);
		$data = array(
			'lat' => $_POST['lat'],
			'lng' => $_POST['lng']
		);
		$row = model('ceshi_user')->editAddress($data,$where);		
		if($row){				
		
			output_data('ok');
		}else{
			output_error('更新失败');
		}
		
		
	}
	
	
	

	public function get_sector_listOp(){
		
		
		
		$data= array();		
		$region = model('region_sector')->getRegionList(array('region_enable' => 1));		
	//	$region_data = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
		//	$region_data[$v['region_id']] = $v;
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		
		$region_data_list = model('region_sector')->getRegionList(array('region_enable'=> 1));		
		$region_data = array();
		foreach($region_data_list as $v){			
			$v['options'] = '';
			$region_data[$v['region_id']] = $v;			
		}
		
		output_data(array('map_region'=>$result,'region_data'=>$region_data));
	}




	public function cancelOp(){
	
	
			$model = model('order');
	
			$data = array(
				'order_state'=> 60
			);			
			$where = array(
				'order_sn' => $_POST['order_sn']
			);			
			$row = $model->editOrder($data,$where);		
			
			if($row){
				output_data('ok');
			}else{
				output_error('错误');
			}	
	}

	




	public function editStoreOp(){
	
		$_POST = json_decode(file_get_contents('php://input'),true);	   				
		$where= array(
			'store_id' => $_POST['store_id']
		);
		$data = array(
			'store_lat' => $_POST['lat'],
			'store_lng' => $_POST['lng']
		);
		$row = model('store')->editStore($data,$where);		
		if($row){
		
			//更新所有商家的关系。
		
		    	//录入一个坐标
    	/*	
    		$data = array(
    		    'lat' => $_POST['lat'],
    		    'lng' => $_POST['lng']
    		);
    		
    		model('ceshi_user')->addAddress($data);
    		
    		*/
			
			
			$this->editStoreDj();
		
					
			output_data('ok');
		}else{
			output_error('更新失败');
		}
	
	}
	
	
	private function editStoreDj(){		
		$store = model('store')->getStoreList(TRUE);
		foreach($store as $v){
			$die = array();
			foreach($store as $b){
			
				$distance = $this->getDistance($v['store_lat'], $v['store_lng'], $b['store_lat'], $b['store_lng']);	
				
				if($distance <= 1500 && $v['store_id'] != $b['store_id']){
					$die[] = $b['store_id'];			
				}
				
			}
			$where = array(
				'store_id' => $v['store_id']
			);
			$update = array(
				'store_superpose' => implode(',',$die)
			);
			model('store')->editStore($update,$where);
		}
		

		
	
	}
	
	
	public function storeJiedanOp(){
	    
	    	$data = array(
				'order_state' => 40
			);			
			$where = array(
				'order_state' => 30,
				'store_id' => $_POST['store_id']
			);
			
			$row = model('order')->editOrder($data,$where);		
			
			output_data('操作成功');
	    
	}
	
	
	public function storeJudanOp(){
	    
	    	$data = array(
				'order_state' => 0
			);			
			$where = array(
				'order_state' => 30,
				'store_id' => $_POST['store_id']
			);
			
			$row = model('order')->editOrder($data,$where);		
			output_data('操作成功');
	    
	}
	
	
	
	
	//更改配送员坐标
	public function editDeliverOp(){
	
		$_POST = json_decode(file_get_contents('php://input'),true);	   		
		
	
		$where= array(
			'distributor_id' => $_POST['deliver_id']
		);
		$data = array();
		$data['region_coordinate'] = $_POST['lng'].','.$_POST['lat'];

		$money = $this->DeliverMoneyDistance($data['region_coordinate']);
		if($money == 1){
			$data['delivery_return_money'] = 1;
		}else{
			$data['delivery_return_money'] = 0;
		}
		
		
		$row = model('waiter')->editWaiter($where,$data);		
		if($row){		
		
		    $latlng = $_POST['lng'].','.$_POST['lat'];
		    
		    //判断手里的单子是否有进入商家附近的
		    $this->orderDStore($_POST['deliver_id'],$latlng);
		    
		    
		    //判断手里的单子是否有进入用户附近的。
		     $this->orderDUser($_POST['deliver_id'],$latlng);
		    
		    
		
		
			//判断进入围栏后是否推送订单池订单			
			model('order_polling')->delPolling(array('waiter_id'=>$_POST['deliver_id']));			
			$this->orderReload($_POST['deliver_id']);	
								
			
			output_data('ok');
		
		}else{
			output_error('更新失败');
		}
		
	}
	
	
	//抵达商家附近 现在计算为 200米
	private function orderDStore($deliver_id,$latlng){
	    
	    
	    $where = array();
	    $where['distributor_id'] = $deliver_id;
	    $where['order_state'] = 40;
	    
	  	$order = model('order')->getOrderListNew($where,'*','order_id asc');		
		foreach($order as $v){
		    
		        $store_lng = explode(',',$v['store_coordinate']);
		    	$distance = $this->getDistance($latlng[1], $latlng[0], $store_lng[1], $store_lng[0]);
	            if($distance < 200){
	                    
	                    $where = array();
	                    $where['order_id'] = $v['order_id'];
	                    $where['record_msg'] = '配送员到达商家附近';
	                    $record_info = model('order_record')->getRecordInfo($where);
	                    if(empty($record_info)){
                		       $data_record = array();
                			   $data_record['order_id'] = $v['order_id'];
                			   $data_record['record_msg'] = '配送员到达商家附近';
                			   $data_record['record_time'] = time();
                			   model('order_record')->addRecord($data_record);
	                    }
	                    
	                    
	                    $where = array();
	                    $where['order_id'] = $v['order_id'];
	                    $where['log_orderstate'] = 41;
	                    $log_info = model('order')->getOrderLogInfo($where);
	                    
	                    if(empty($log_info)){
                				$data = array();
                				$data['order_id'] = $v['order_id'];
                				$data['log_role'] = 'waiter';
                				$data['log_msg'] = '配送员到达商家附近';
                				$data['log_user'] = $v['distributor_id'];
                			
                				if ($msg) {
                					$data['log_msg'] .= ' ( '.$msg.' )';
                				}
                				
                				$data['log_orderstate'] = 41;
                				 model('order')->addOrderLog($data);
        				
	                    }
        				
	                
	            }
	            
		    
		    
		    
		}
		
	    
	    
	    
	    
	    
	    
	    
	    
	    
	     
	    
	}
	
	private function orderDUser(){
	    
	    
	    $where = array();
	    $where['distributor_id'] = $deliver_id;
	    $where['order_state'] = 50;
	    
	  	$order = model('order')->getOrderListNew($where,'*','order_id asc');		
		foreach($order as $v){
		    
		        $store_lng = explode(',',$v['buyer_coordinate']);
		    	$distance = $this->getDistance($latlng[1], $latlng[0], $store_lng[1], $store_lng[0]);
	            if($distance < 200){
	                    
	                    $where = array();
	                    $where['order_id'] = $v['order_id'];
	                    $where['record_msg'] = '配送员到达顾客位置';
	                    $record_info = model('order_record')->getRecordInfo($where);
	                    if(empty($record_info)){
                		           $data_record = array();
                    			    $data_record['order_id'] = $order['order_id'];
                    			    $data_record['record_msg'] = '配送员到达顾客位置';
                    			    $data_record['record_time'] = time();
                    			    model('order_record')->addRecord($data_record);
	                    }
	                    
	                    
	                    $where = array();
	                    $where['order_id'] = $v['order_id'];
	                    $where['log_orderstate'] = 52;
	                    $log_info = model('order')->getOrderLogInfo($where);
	                    
	                    if(empty($log_info)){
                				$data = array();
                				$data['order_id'] = $v['order_id'];
                				$data['log_role'] = 'waiter';
                				$data['log_msg'] = '配送员到达顾客位置';
                				$data['log_user'] = $v['distributor_id'];
                			
                				if ($msg) {
                					$data['log_msg'] .= ' ( '.$msg.' )';
                				}
                				
                				$data['log_orderstate'] = 41;
                				 model('order')->addOrderLog($data);
        				
	                    }
        				
	            }
	            
		    
		}
		
	}
	
	
	
	
	//检查订单池遗留下来的订单并推送
	private function orderPool(){
		$where = array(
			'order_polling' => array('in','0,1,2'),
			'order_polling_show' => 0
		);
		$order = model('order')->getOrderListNew($where,'*','order_id asc');		
		
		
		
		foreach($order as $v){	
				
				
				model('order_polling')->delPolling(array('order_sn'=>$v['order_sn']));
			
				$this->sendOrder($v['order_sn'],1);
		}
	}
	
	

	
	

	//配送员排序条件
	
	
	private function deliverySort($deliver_id,$store_id){		
		//有两单
		$count = $this->deliverOrderCount($deliver_id);
		
		switch ($count) {		
		   case 2: 
				
				$where = array(
					'distributor_id' => $deliver_id,		
					'store_id' => $store_id,			
					'order_state' => array('in','30,40')				
				);
				
				$count = model('order')->getOrderCount($where);
				
				if($count  == 2){
				
					return  1 ;	
				
				}elseif($count == 1){
				
					return  2 ;
				
				}elseif($count == 0){
				
					return  4;
				
				}					
			 break;
		   case 1:		   
				$where = array(
					'distributor_id' => $deliver_id,	
					'store_id' => $store_id,						
					'order_state' => array('in','30,40')				
				);
				$count = model('order')->getOrderCount($where);
				//有一单
				if($count  == 1){
					return 3;					
				}elseif($count == 0){					
					return 5;
				}			
			 break;			 
		   case 0:			   	   
				return 6;									
		   break;		  
		}
	}
	
	
	private function deliverOrderCount($id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40,50')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	

	
	//订单
	public function getOrderOp(){
		
		$where = array(
			'order_state' => array('in','20')
		);
		$list = model('order')->getOrderListNew($where,'*', 'daycode asc');
		$data = array();	
		foreach($list as $k => $v ){				
			$data[$k]['order_sn'] = $v['order_sn'];
			$data[$k]['order_id'] = $v['order_id'];
			$data[$k]['store_name'] = $v['store_name'];
			$data[$k]['order_polling'] = $v['order_polling'];
			$data[$k]['add_time'] = $this->orderTime($v['add_time']);
			
			$store =  explode(',',$v['store_coordinate']) ;
			
			$data[$k]['deliver'] = $this->getOrderDeliver(array('store_lat'=> $store[1],'store_lng'=>$store[0]),$v);

			
		}		
		output_data(array('list'=>$data));
	}
	
	
	//计算等待时间
	
	private function orderTime($orderTime){
	
		$time = time();		
		return ($time - $orderTime) ;
	
	}

	
	//上线
	public function upLineOp(){
	
		$_POST = json_decode(file_get_contents('php://input'),true);	   		
		
	
		$where = array();
		$where['distributor_id'] = $_POST['deliver_id'];
		
		$data = array();
		$data['distributor_online'] = 20;
		$data['delivery_state'] = 0;
		$data['last_online_time'] = time();
		$row = model('waiter')->editWaiter($where,$data);
		if($row){
			//写入socket 时间 ，记录空闲时间			
			$data = array(
				'client_id' => date('YmdHis',time()),
				'stoket_state' => 0,
				'updatetime' => time(),
				'distributor_id' => $_POST['deliver_id']
			);
			model('stoket')->addStoket($data);			
			//检查周边5公里内延迟等待订单。
				
			
			$this->orderReload($_POST['deliver_id']);	
				
						
//			$this->orderPool();
			
			output_data('ok');
		
		}else{
			output_error('失败');
		}
	
	
	}
	
	//下线
	public function downLineOp(){		
		$_POST = json_decode(file_get_contents('php://input'),true);	   						
		$where = array();
		$where['distributor_id'] = $_POST['deliver_id'];	
					
		$data = array();
		$data['distributor_online'] = 10;
		$row = model('waiter')->editWaiter($where,$data);
		if($row){
		
		
				//清空当前配送员的列表库		
			
					$where = array(
						'waiter_id' => $_POST['deliver_id']
					);
					$order_list = model('order_polling_show')->getShowList($where);		
					$order_id = array();			
					foreach($order_list as $v){
						$order_id[] = $v['order_id'];
					}		
					
					if(is_array($order_id)){
						$order_ids = implode(',',$order_id);
						$where = array(
							'order_id' => array('in',$order_ids)
						);
						model('order')->editOrder(array('order_polling_show'=> 0),$where);					
					}
					
					//删除展示
					model('order_polling_show')->delShow(array('waiter_id'=>$_POST['deliver_id']));
					
					//删除
					model('order_polling')->delPolling(array('waiter_id'=>$_POST['deliver_id']));	
					
					
					
		
			output_data('ok');
		
		}else{
			output_error('失败');
		}
		
	
	}
	
	//钱的标志
	public function getMoneyOp(){
	
			
				//导航点
		$region = model('region')->getRegionList(array('region_type'=> 2,'region_enable' => 1,'region_money_state' => 1));	
	   	$region_data = array();
		$nav_data = array();
		$sector_data = array();
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['id'] = $v['region_id'];	
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_money_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo_c = explode(",",$vv);
				$geo[$kk][0] = floatval($geo_c[0]);
				$geo[$kk][1] = floatval($geo_c[1]);				
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
			
		//	$region_data[$v['region_id']] = $v;
		
			//导航点
			
			
			$nav_data[$v['region_id']]['properties']['iconSize'] = array(30,30);
			$nav_data[$v['region_id']]['properties']['name'] = $v['region_name'];			
			$nav_data[$v['region_id']]['properties']['id'] = $v['region_id'];
			$nav_data[$v['region_id']]['properties']['icon'] = '';
			$nav_data[$v['region_id']]['geometry']['type'] = 'Point';
			$nav_coordinate = explode(',',$v['region_nav_coordinate']);
			$nav_data[$v['region_id']]['geometry']['coordinates'][0] = $nav_coordinate[0];						
			$nav_data[$v['region_id']]['geometry']['coordinates'][1] = $nav_coordinate[1];	
			
		
			
		
			
		
		
		
		
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;		
		$result['type'] = 'geojson';
		$result['data'] = $data;
		
		

		
	
		
		
		output_data(array('map_region'=>$result));	
			
			
	
	}
	
		//计算配送员于钱标的距离
	
	private function DeliverMoneyDistance($deliver_coordinate){
	
		$data = array(
					0 => array(
					'lat' => '32.03533764550808',
					'lng' => '118.77069084008008',
					'name' => '钱1',
					'id' => 1
				),
				
				1 => array(
					'lat' => '32.022971582635336',
					'lng' => '118.84574467387733',
					'name' => '钱2',
					'id' => 2		
				),
				
				2 => array(
					'lat' => '31.99103693828563',
					'lng' => '118.8293017844943',
					'name' => '钱3',
					'id' => 3
				),
				
				3 => array(
					'lat' => '31.999134097971847',
					'lng' => '118.73966151656555',
					'name' => '钱4',
					'id' => 4
				)
			);
	
			$deliver_coordinate = explode(',',$deliver_coordinate);

			$result = false;
			foreach($data as  $k => $v){		
			
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $v['lat'], $v['lng']);
				if($distance < 3000){
				
						$result = true;
				
				}
				
			}	
			
			
			return $result;
			
	}
	
	
	
	
	//后去配送员列表
	public function getDeliveryOp(){
		
			$where['distributor_status'] = 1;
			$where['weizhi_id'] = 0;
			
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			$state = array(
				'10' => '已下线',
				'20' => '在线中'
			);
			$deliveryState = array(
				0 => '接单中',
				1 => '取餐中',
				2 => '配送中'
			);
			$deliveryReturnMoney = array(
				0 => '未在钱圈',
				1 => '已在钱圈'
			);
			
			
			foreach($deliver_data as $k => $v){	
				$deliver[$k]['distributor_id'] = $v['distributor_id'];			
				$deliver[$k]['distributor_name'] = $v['distributor_name'];
				$deliver[$k]['order_list'] =  $this->getDeliverOrder($v['distributor_id']);
				$deliver[$k]['distributor_state'] =  $state[$v['distributor_online']];
				
				$deliver[$k]['delivery_state_name'] =  $deliveryState[$v['delivery_state']];
				$deliver[$k]['delivery_state'] = $v['delivery_state'];
				$deliver[$k]['distributor_online'] = $v['distributor_online'];
				
				$deliver[$k]['delivery_return_money'] =  $deliveryReturnMoney[$v['delivery_return_money']];
				
				//推送过来订单。展示中的
				$show_order = $this->getStockSendOrder($v['distributor_id']);;
				
				$deliver[$k]['distributor_show_order'] = $show_order;
				
				$deliver[$k]['order_time_num'] = $show_order ?  ($show_order['addtime'] - time() + 50)   : 'N/A';				
				
				//等待推送的订单
				$deliver[$k]['distributor_stock_order'] = $this->getStockShowOrder($v['distributor_id']);
				
				
				
				
				
				
				
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(28,28);
					$feature[$k]['properties']['name'] = $v['distributor_name'];			
					$feature[$k]['properties']['id'] = $v['distributor_id'];
					$feature[$k]['properties']['state'] = $v['distributor_online'];
					$feature[$k]['geometry']['type'] = 'Point';
					$deliver_coordinate = explode(',',$v['region_coordinate']);
					$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];			
			}
		
		
			$deliver_list = array();
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;		
			output_data(array('deliver'=>$deliver,'deliver_list'=>$deliver_list));
		
	}
	
	
	//获取当前给他推送的订单
	private function getStockSendOrder($deliver_id){	
		
		$model = model('order_polling_show');				
		$where = array(
			'waiter_id' => $deliver_id		
		);		
		$info = $model->getShowInfo($where);		
		return $info;	
	}
	
	
	
	private function getStockShowOrder($deliver_id){
		
		
		$model = model('order_polling_show');				
		$where = array(
			'waiter_id' => $deliver_id		
		);		
		$info = $model->getShowInfo($where);	
		
		
		
		
		$model = model('order_polling');		
		$where = array();
		$where['waiter_id'] = $deliver_id;
		$where['send_state'] = 1;
		
		if(!empty($info)){
			$where['order_sn'] =array('not in ',$info['order_sn']);
		}
			
		
		$list = $model->getPollingList($where,'','order_sort desc,order_id asc');	
		
		
		
		
		return $list;			
	}
	
	
	
	
	//获取配送员现在手里的订单
	private function getDeliverOrder($deliver_id){
		
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);
		$order_data = model('order')->getOrderList($where);
		$order_list = array();
		foreach($order_data as $k=>$v){
			$order_list[$k]['name'] = $v['store_name'].'-'.$v['order_id'];
			$order_list[$k]['order_id'] = $v['order_id'];
			$order_list[$k]['order_state'] = $v['order_state'];
		}
				
		return $order_list;
		
	}
	
	
	
	
	//获取订单周边范围的可用配送员
	private function getOrderDeliver($store,$order){
		
			$where = array();
			$where['distributor_status'] = 1;
			$where['distributor_online'] = 20;
			$where['delivery_state'] = 0;
			$where['weizhi_id'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			foreach($deliver_data as $v){
				$deliver_coordinate = explode(',',$v['region_coordinate']);

				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
			
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$store_id);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order['store_id']);
				
				//距离小于5000的可以加入
			
				//是否拒绝过此单
				$refuse = $this->checkRefuse($order['order_sn'],$v['distributor_id']);
				
			
				if(($superposition == 1)  ){	
				
					$deliver[] = array(
						'distributor_name' => $v['distributor_name'],
						'deliver_sort' => $deliver_sort,//订单排序
						'deliver_order' => '',//最早的订单
						'deliver_freetime' => ''//空闲时间
					);	
						
					
				}elseif($distance < 5000  && ( $superposition == 1 || $superposition == 0)   ){
				
					$deliver[] = array(
						'distributor_name' => $v['distributor_name'],
						'deliver_sort' => $deliver_sort,//订单排序
						'deliver_order' => '',//最早的订单
						'deliver_freetime' => '',//空闲时间
						'is_refuse' => $refuse > 0 ? 1 : 0
					);	
						
				}
				
				
				
				
				
			}	
			
			return $deliver;	
	
	}
	
	
	//获取配送员可接的叠加商户
	private function getOrderSuperpositionStore($deliver_id,$store_id){
		
		$count = 0;
		
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);
		
		$order = model('order')->getOrderInfoN($where,'store_id,store_superpose','distributor_start_time desc');		
	
		if($order['store_id'] == $store_id){					
			return 1; //店铺本身.
		}
		
		if($order['store_superpose'] == $store_id){
			return 1;
		}
		
		//print_r($order);
		if(!empty($order)){		
			//直接读取当前商户是否可以叠加其他的店铺
			$store = explode(',',$order['store_superpose']);
			if(in_array($store_id,$store)){  			
				return 1;				
			}else{			
				return 2;				
			}						
		}else{		
			return 0; //没有单子
		}
		
	}
	
	
	
	//获取商家周边范围的可用配送员
	private function getStoreDeliver($store){
			$where = array();
			$where['distributor_status'] = 1;			
			$where['distributor_online'] = 20;			
			$where['delivery_state'] = 0;
			$where['weizhi_id'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			$count = 0;
			foreach($deliver_data as $k =>$v){
			
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				
				
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$store['store_id']);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$store['store_id']);
				

				
				if($superposition == 1 ){						
						
						$deliver[$k]['name'] = $v['distributor_name'];	
						$deliver[$k]['distance'] = $distance;
						$deliver[$k]['sort'] = $deliver_sort;
						$deliver[$k]['distributor_id'] = $v['distributor_id'];
						$deliver[$k]['superposition'] = $superposition;
						
						$count += $this->getDeliveryCount($v['distributor_id']);
						
						
				}else if($distance < 5000 &&  $superposition == 0 ){
				
						$deliver[$k]['name'] = $v['distributor_name'];	
						$deliver[$k]['distance'] = $distance;
						$deliver[$k]['sort'] = $deliver_sort;
						$deliver[$k]['distributor_id'] = $v['distributor_id'];	
						$deliver[$k]['superposition'] = $superposition;
						$count += $this->getDeliveryCount($v['distributor_id']);
						
				}
				
			}
			
			
			return array('deliver'=>$deliver,'count' => $count);	
	
	}
	
	
	//店铺状态
	public function storeStateOp(){
			
			$_POST = json_decode(file_get_contents('php://input'),true);
			
			$where = array(
				'store_id' => $_POST['store_id']
			);
			
			$data = array(
				'store_state' => $_POST['state']
			);
			
			
			$row = model('store')->editStore($data,$where);
			
			if($row){
				output_data('ok');
			}else{
				output_error('操作失败');
			}
			
			
	
	
	}
	
	
	//获取商家
	public function getStoreOp($type = 0){
	
		$state= array(
			0 => '未上架',
			1 => '营业中',
			2 => '已打烊',
		);
	
		$color = array('#f40606','#0000ff','#009245','#ff00ff','#754c24','#f7931e');		
		$where = array();
		$where['store_state'] = array('in','1,2');
		$where['weizhi_id'] = 0;
		$store = model('store')->getStoreList($where);
		foreach($store as $k => $v){
			
			$data[$k]['store_id'] = $v['store_id'];
					
			$data[$k]['store_lat'] = $v['store_lat'];
			$data[$k]['store_lng'] = $v['store_lng'];			
			$data[$k]['store_superpose'] = $this->getStoreOverlap($v['store_superpose']);		
			$data[$k]['store_state_name'] = $state[$v['store_state']];
			$data[$k]['store_state'] = $v['store_state'];
			$data[$k]['store_name_primary'] = '['.$v['xuhao'].']'.$v['store_name_primary'];			
			
			
			$delivery = $this->getStoreDeliver(array('store_lat'=>$v['store_lat'],'store_lng'=> $v['store_lng'],'store_id'=>$v['store_id']));
			
			$data[$k]['store_delivery'] = $delivery['deliver'];
			
			$data[$k]['store_delivery_num'] = $delivery['count'];
			
			$data[$k]['store_color'] = $color[$k];				 
			
			
			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['name'] = $v['store_name_primary'];	
			$feature[$k]['properties']['id'] = $v['store_id'];	
			$feature[$k]['properties']['store_state'] = $v['store_state'];	
			$feature[$k]['properties']['xuhao'] = $v['xuhao'];	
			$feature[$k]['geometry']['type'] = 'Point';
			
			$feature[$k]['geometry']['coordinates'][0] = $v['store_lng'];			
			$feature[$k]['geometry']['coordinates'][1] = $v['store_lat'];			
			
			$circle_feature[$k]['type'] = 'Feature';		
			$circle_feature[$k]['geometry']['type'] = 'Point';			
			$circle_feature[$k]['geometry']['coordinates'][0] = $v['store_lng'];			
			$circle_feature[$k]['geometry']['coordinates'][1] = $v['store_lat'];				
			
		}
		
		
		$store_list = array();
		$store_list['type'] = 'FeatureCollection';
		$store_list['features'] = $feature;
						
		$circle_list = array();
		$circle_list['type'] = 'FeatureCollection';
		$circle_list['features'] = $circle_feature;
				
		
		if($type == 0){
			output_data(array('store'=>$data,'store_list' => $store_list,'circle_list' => $circle_list));
		}else{
		
			return $data;
			
		}
	//	exit(json_encode(array('store'=>$data)));
	
	}
	
	//叠加商户名称 
	private function getStoreOverlap($store_id){
		
		if(empty($store_id)){
			return '';
		}
				
		$where = array(
			'store_id' => array('in',$store_id)
		);
		
		$store = model('store')->getStoreList($where);		
		
		$store_name = array();
		
		foreach($store as $v){
			
			$store_name[] = $v['store_name_primary'];
			
		}
				
		return implode(',',$store_name);
		
	}
	
	
	//计算运力
	public function getDeliveryCount($deliver_id){
		$count = 3;				
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);		
		$order_count = model('order')->getOrderCount($where);
		return $count - $order_count;	
	}
	
	
	
	//获取配送员手里的单子
	public function getDeliveryOrderNum($delivery_id){
		
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);		
		$order_count = model('order')->getOrderCount($where);		
		return $order_count;
	
	}
	
	
	
	
	
	//接单
	public function acceptOp(){
		
		$_POST = json_decode(file_get_contents('php://input'),true);	   				
		$stoket =  model('stoket');		
		$model = model('order');		
		
		$deliver_id = $_POST['deliver_id'];
		
		$deliver_info = model('waiter')->getWaiterInfo(array('distributor_id'=>$deliver_id));
		
		$where = array(
		
			'order_sn' => $_POST['order_sn']
			
		);	
		
		if(empty($_POST['order_sn'])){
		    
		    	output_error('没有可接的单子');
		    
		}	
			
			
		$order_info = $model->getOrderInfo($where); 			
		
		if($order_info['order_state'] == 30 && $order_info['distributor_id'] > 0  ){	
				
			output_error('异常');	
					
		}else{
		
			$count = $this->deliverOrderCount($deliver_info['distributor_id']);		
			$data = array(
				'distributor_id'		=> $deliver_info['distributor_id'],
				'distributor_name'		=> $deliver_info['distributor_name'],
				'distributor_mobile'	=> $deliver_info['distributor_mobile'],	
				'distributor_start_time'=> time(),
				'order_state' => 30,
				'distributor_code'      => $deliver_info['distributor_code']
			);			
			$where = array(
				'order_sn' => $_POST['order_sn']
			);
			
			$row = $model->editOrder($data,$where);		
				
				
			if($row){	
				
				
				
				
				
				
				
				
				
				//推送给当前商户
				
				
				
    			require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
        		$gateway = new \GatewayClient\Gateway();
        		$gateway::$registerAddress = '150.158.195.68:1238';	
        		
        		$data = array();
        		$data['type'] = 'order_notification';
        		$data['data'] = $order_info;
                
              
               // print_r($order_info);
                $data = json_encode($data);	
                $gateway::sendToUid($order_info['store_id'],$data);
            		
    				
				  
		        	  $data_record = array();
			        $data_record['order_id'] = $order['order_id'];
			        $data_record['record_msg'] = '配送员已接单';
			        $data_record['record_time'] = time();
			        model('order_record')->addRecord($data_record);
			
			
				
				
				$data = array();
				$data['order_id'] = $order_info['order_id'];
				$data['log_role'] = 'waiter';
				$data['log_msg'] = '送餐员已接单';
				$data['log_user'] = $order_info['distributor_id'];
				if ($msg) {
					$data['log_msg'] .= ' ( '.$msg.' )';
				}
				$data['log_orderstate'] = 30;
				$model->addOrderLog($data);
								
				//写入测试				
			     $log = array(
			   		'order_sn' => $order_info['order_sn'],
					'log_msg' => '已接单：'.$store_info['distributor_name'].'接单:'.$order['order_sn'],
					'log_time' => time()
			     );
				 
				 model('stoket_log')->addSlog($log);
				 //删掉当前接的单子
				 model('order_polling')->delPolling(array('order_sn' => $order_info['order_sn']));	
				 $deliver_list = array();
				
				 //查询当前跟本配送员的所有订单
				 $deliver_order = model('order_polling')->getPollingList(array('waiter_id'=>$deliver_id));
					 
				//print_r($deliver_list);
				 //删除推送记录
				 model('order_polling_show')->delShow(array('order_sn' => $order_info['order_sn']));
				 
				$deliver_ordernew = array();
			//	print_r($deliver_order);
				foreach($deliver_order as $v){
				 
				 	$deliver_order_new = model('order_polling')->getPollingList(array('order_sn'=>$v['order_sn']));
				 
				//	print_r($deliver_order_new);	
					$deliver_ordernew[$v['order_sn']] = $v['order_sn'];
				 
				 	foreach($deliver_order_new as $n){
				 		
				 		 $deliver_list[$n['waiter_id']] = $n['waiter_id'];
				 	
				 		 
				 	}
				 	
				 	$is_show = model('order_polling_show')->getShowInfo(array('order_sn' => $v['order_sn']));
				 	if(empty($is_show)){
				 	
				 	//把其他订单恢复到初始状态
						model('order')->editOrder(array('order_polling_show'=> 0),array('order_sn' => $v['order_sn']));	
					
				 	}
					
					model('order_polling')->delPolling(array('order_sn' => $v['order_sn']));	
					
				 }
				 
			
				 
				//查询当前接了接了几单
			 	$count = $this->deliverOrderCount($deliver_id);		
				
				if($count == 3){
					
					
					$where = array(
						'waiter_id' => $deliver_id
					);
					$order_list = model('order_polling_show')->getShowList($where);	
					foreach($order_list as $v){
						$order_id[] = $v['order_id'];
					}					
					
					if(is_array($order_id)){
						
						$order_ids = implode(',',$order_id);
						$where = array(
							'order_id' => array('in',$order_ids)
						);
						
						model('order')->editOrder(array('order_polling_show'=> 0),$where);					
					}
					
					
					
					
						//清空本单列表
					model('order_polling')->delPolling(array('waiter_id' => $deliver_id));					 
					 //删除推送记录
					model('order_polling_show')->delShow(array('waiter_id' =>  $deliver_id));
					
					
					
					//配送员状态改成配送中
					$where = array(
						'distributor_id' => $deliver_id
					);
					
					$data =  array(
						'delivery_state' => 1
					);		
					
					model('waiter')->editWaiter($where,$data);
					
					
					/*
					//订单修改成派送中
					$where = array(
						'distributor_id' => $deliver_id,
						'order_state'=> 30
					);		
					$data = array(
						'order_state' => 40
					);					
					model('order')->editOrder($data,$where);						
					
					*/
					
				}
				
			
			//	$this->orderReload($deliver_id);	
		
			
			
				//$oder_list = model('')->
				$this->orderPool();	
				
			//	print_r($deliver_ordernew);
			//	print_r($deliver_list);
			
		/*	if(count($deliver_ordernew) > 0 ){ //有其余订单。就更新其余订单
				
				foreach($deliver_ordernew as $v){
					$this->sendOrder($v,1);
				}
				
			}
			
			if(count($deliver_list) > 0 ){ //有其他配送员。就更新配送员
				
				foreach($deliver_list as $v){
						$this->orderReload($v);	
					
				}	
					
				
			}else{ //都没有就刷新一下订单池
				
				$this->orderPool();
				
			}
			
			*/
			
			
			
				
				
				
					
			//	$this->orderReload($deliver_id);		
						
			//	print_r($deliver_list);		
			/*	if(count($deliver_list) > 0 ){
					
					
				}*/
				
				
				//查询订单池里是不是有订单
				
				
				
				
				
				
					//刷新他的所有订单					
				//	$order = model('order_polling')->getPollingList(TRUE);	
					//foreach($order as $v){
					//	model('order_polling')->delPolling(TRUE);	
					//	$this->sendOrder($v['order_sn']);						
				//	}
					
							
					//刷新一次订单池。					
				//	$this->orderPool();
					
					output_data('接单成功');
		
			}else{
				
				output_error('接单失败');
			}
		}
				
		output_data('ok');
		
	}
	
	
	

	
	
	//拒绝
	public function refuseOp(){
	
			$_POST = json_decode(file_get_contents('php://input'),true);	
			
			$stoket =  model('stoket');		
			
			$order_sn = $_POST['order_sn'];		
			$deliver_id = $_POST['deliver_id'];

		
			//$this->getWaiterNew($order_sn);
			//写入拒绝表
			$data = array(
				'waiter_id' => $deliver_id,
				'order_sn' => $order_sn
			);
			
			model('polling_refuse')->addRefuse($data);	
			
			
			
			
			$where = array(			
				'order_sn' => $order_sn
			);
			model('order_polling')->delPolling($data);		
			
			//删除推送记录
			
			model('order_polling_show')->delShow(array('order_sn' => $order_sn,'waiter_id' => $deliver_id));									
			model('order')->editOrder(array('order_polling_show'=> 0),array('order_sn'=>$order_sn));
			
			
									
			//无可用列队后，进入池订单。
	//		$where = array(
			//	'state'=> 0	,
		//		'order_sn' => $order_sn
		//	);			
		 //   $count = model('order_polling')->getPollingCount($where);
		//	print_r('liedui :'.$count);
			
		  
		/* 	 $order = model('order_polling')->getPollingList(array('send_state'=> 1));	
			 foreach($order as $v){					
					model('order_polling')->delPolling(array('order_sn' =>$v['order_sn']));
					$this->sendOrder($v['order_sn']);
			 }*/
			
			
			 			
			$this->orderReload($deliver_id);		
						
					  
		  
		  /*  if($count == 0){			
					$data = array(   
					   'order_polling' => 2
					);			
					$where = array(
						'order_sn' => $_POST['order_sn']
					);
					model('order')->editOrder($data,$where);	
					 
					//刷新一下订单池。
					$this->orderPool();
				
			 }else{			
							
											
			//	$this->sendOrder($order_sn);
					
				
					
			}		
			*/		
		
			output_data('ok');
	}
		
		
		
		
		
		
		
	
	public function huanyuanOp(){
			//删除排队
			model('order_polling')->delPolling(TRUE);
			//删除
			model('polling_refuse')->delRefuse(TRUE);
			$data = array(
				'store_id' => 0,
				'store_name' => '',
				'store_phone' => '',
				'store_address' => '',
				'store_region_id' => '',
				'store_region_sm_id' => '',
				'store_coordinate' => '',
				'daycode' => '',
				'distributor_id' => '',
				'distributor_name' => '',
				'distributor_mobile' => '',
				'order_state'=> 10,
				'order_polling' => 0,
				'store_superpose' => '',
				'order_polling_show' => 0
			);	
			model('order')->editOrder($data,TRUE);
			
			
			$data = array(
				'delivery_state' => 0,
				'distributor_online' => 10
				
			);
			
			model('waiter')->editWaiter(TRUE,$data);
			
			model('stoket')->delStoket(TRUE);			
			model('order_polling_show')->delShow(TRUE);
			
			output_data('ok');
		
	}
	
	
	
	//新增一笔订单
	public function addorderOp(){	
				
		$_POST = json_decode(file_get_contents('php://input'),true);	   
	  
		$where = array(
			'store_id' =>  0
		);	
		$order = model('order')->getOrderInfoN($where,'*','order_id asc');
		
		
		if($order){		
				
			$store_info = model('store')->getStoreInfo(array('store_id'=> $_POST['store_id']));					
		
			$data = array();
			
			$data['store_id'] = $store_info['store_id'];
            $data['store_name'] = $store_info['store_name_primary'];
			$data['store_phone'] = $store_info['store_phone'];
			$data['store_address'] = $store_info['store_address'];
			$data['store_region_id'] = $store_info['region_id'];
			$data['store_region_sm_id'] = $store_info['region_sm_id'];			
		    $data['store_coordinate'] = $store_info['store_lng'].','.$store_info['store_lat'];	
			$data['store_superpose'] = $store_info['store_superpose'];
			$data['order_sn'] = $store_info['store_code'].date('dmy',time()).'01'.rand(1000,9999);
			$data['order_estimate_time'] = time() + 40 * 60;
			
			$member_id = $_POST['member_id'];
		
			$user = model('ceshi_user')->getAddressInfo(array('member_id' => $member_id));
	        
	        
			
			$data['buyer_id']       = $user['member_id'];
		    $data['buyer_name']     = $user['address_nickname'];
			$data['buyer_phone']    = '152310'.rand(10000,99999);
			$data['buyer_address']  = $user['address'];
		    $data['buyer_coordinate']   = $user['lng'].','.$user['lat'];
		    $data['buyer_region']       = $user['region_name'];
		    $data['buyer_region_color'] = $user['region_color'];
	    	$data['buyer_region_id']    = $user['region_id'];
	    		    	
			$data['order_date'] = date('Y-m-d',time());
			$data['order_state']	 = 20;
			$data['daycode'] = time();			
			$data['add_time'] = time(); //$this->getMillisecond();					   
			$data['weizhi_id'] = 0;
			
			
		    $where = array(
			   	'order_id' => $order['order_id']
		    );		
					   
		   $row = model('order')->editOrder($data,$where);		
		   if($row){
			   
				 
			  
			        $data_record = array();
			        $data_record['order_id'] = $order['order_id'];
			        $data_record['record_msg'] = '客户支付订单';
			        $data_record['record_time'] = time();
			        model('order_record')->addRecord($data_record);
			
			  	
					$data = array();
					$data['order_id'] = $order['order_id'];
					$data['log_role'] = 'user';
					$data['log_msg'] = '支付订单';
					$data['log_user'] = '用户';
					if ($msg) {
						$data['log_msg'] .= ' ( '.$msg.' )';
					}
					$data['log_orderstate'] = 20;
					$model->addOrderLog($data);
			  			  
			    //更新他的订单商品
			    
			  
			   //锁定运力。
			  // $this->lockWaiter($order['order_sn']);
				
			   //执行推送 这是新订单
				$this->sendOrder($data['order_sn'],1);
			   						   
			   	output_data('ok');
			   
			}else{
				 output_error('订单添加失败');
		    }
			
		}		
		
		
	}
	
	
	

	
	
	
	
	//获取商家周边范围的可用配送员
	private function sendOrderDeliver($store,$order_info){
	
			
			$where = array();
			
			$where['distributor_status'] = 1;			
			$where['distributor_online'] = 20;			
			$where['delivery_state'] = 0;		
			$where['weizhi_id'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);		
			$deliver = array();
			foreach($deliver_data as $v){
			
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store[1], $store[0]);
			
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
			//	print_r($deliver_coordinate);
				//print_r($store);
			//	print_r($distance.',');
			
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order_info['store_id']);
				
				//顶级排序
				if( ($superposition == 1 )){	
					
					$deliver[] = array(
						'distributor_id' => $v['distributor_id'],
						'distributor_name' => $v['distributor_name']
					);					
				}else{
					if($distance < 5000 &&  ($superposition == 1 || $superposition == 0 )){
						$deliver[] = array(
							'distributor_id' => $v['distributor_id'],
							'distributor_name' => $v['distributor_name']
						);	
					}
				
				}
				
			}
			
			
			
			
			
			return $deliver;	
	
	}
	
	
	
	//生成订单推送排序
	private function sendOrder($order_sn = '',$is_new = 0){		
	
		$stoket =  model('stoket');			
	
		$order_info = model('order')->getOrderInfo(array('order_sn'=>$order_sn));
		
		if(empty($order_info)){
			output_error('订单不存在');
		}
		
		$store_latlng = explode(',',$order_info['store_coordinate']);
		
		//print_r($store_latlng);
		
		$delivery = $this->sendOrderDeliver($store_latlng,$order_info);		
		
	//	print_r($delivery);
		foreach($delivery as $v){
			
				
				
				$deliver_sort = $this->deliverySort($v['distributor_id'],$order_info);		
						
			
				$order_sort = $this->deliverOrderSort($order_info,$v['distributor_id']);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order_info['store_id']);
				
				//print_r($superposition);
				
				//echo '单量'.$count;				
				//检查当前 配送员 是否有单			
				//没有订单，则分配个商户围栏内的配送员			
				//如果有一单，检查是否为本商家的订单，			
				//如果有一单，检查是否为叠加范围内商户，并判断他是否在叠加商户记录内。	
				//检查本单是否与上一单超过6分钟。			
				$diffTime = $this->checkDiffTime($v['distributor_id'],$order_info);
			
			
				
				//检查是否已拒绝过本单	
				$refuse = $this->checkRefuse($order_info['order_sn'],$v['distributor_id']);
				//echo '拒绝'.$refuse;
					
			
				
				//检查本单是否为列队等待单
				
			/*	echo $count;
				echo '|';
				echo $refuse;
				echo '|';						
				echo $overlying;
				echo '|';
				echo $diffTime;
				echo '|';			
				*/
				
				//	print_r($delivery_sort.'|');
				//	print_r($superposition.'|');
				//	print_r($refuse.'|');
				//	print_r($diffTime.'|');
				
				$count = $this->getDeliveryOrderNum($v['distributor_id']);
				
				
			
				
				if($refuse == 0  && $diffTime == 0 && ($superposition == 1 || $superposition == 0)){
				
					$polling_data[] = array(
						'order_id' => $order_info['order_id'],
						'order_sn' => $order_info['order_sn'],
						'waiter_id' => $v['distributor_id'],
						'state' => 0,
						'addtime' => time(),	
						'last_time' => $this->deliver_last_time($count ,$v['distributor_id']),
						'deliver_sort' => $deliver_sort,
						'order_sort' => $order_sort,
						'store_name' => $order_info['store_name']
					);	
				
								
					//print_r($polling_data);
									
					//写入lOG
					$log = array(
						'order_sn' => $order_info['order_sn'],
						'log_msg' => '轮询：'.$v['distributor_name'].'，单号：'.$order_info['order_sn'],
						'log_time' => time()
					);
					
					model('stoket_log')->addSlog($log);
					
				}			
			}
			
			$row = model('order_polling')->addPollingAll($polling_data);			
			
			
			if(count($polling_data) > 0){
			//如无可用运力。直接到订单池
				$data = array(
					'order_polling' => 1
				);	
				
			}else{
			
				$data = array(
					'order_polling' => 2
				);	
				
			}
			
			
			
			
			$where = array(
				'order_sn' => $order_info['order_sn']
			);
			model('order')->editOrder($data,$where);

			if($row){	
								
				$this->stocketSend($order_info['order_sn'],$is_new);			
								
				//开始轮询
				//$stoket->send($order_info['order_sn']);	
			}
	}
	
	
	
	
	
	
	
	//更新订单排序
	private function deliverOrderSort($order,$deliver_id){
		
		$where = array(
			'store_id' => $order['store_id'],
			'order_state' => array('in','30,40'),
			'distributor_id' => $deliver_id
		);
		
		$count = model('order')->getOrderCount($where);
		
		if($count > 0 ){	
				
			return 1;	
				
		}else{
		
			return 0 ;
			
		}
		
	}
	
	
	
	//模拟功能
	private function orderReload($deliver_id = 0,$is_new = 0){
	/*
				 $order = model('order_polling')->getPollingList(array('send_state'=> 1));	 
				 foreach($order as $v){						 
						model('order_polling')->delPolling(array('order_sn' =>$v['order_sn']));	
						$this->sendOrder($v['order_sn']);					
				 }
				*/
				//$deliver_id = $_GET['id'];

				
				$store = $this->getStoreOp(1);				
				$data = array();				
				foreach($store as $s){				
						foreach($s['store_delivery'] as $d){									
							if($d['distributor_id'] == $deliver_id){															
								$data[] = $s['store_id'];								
							}
						}
				}					
			
				
				$store_id  = implode(',',$data);		
				
				//print_r($store_id);			
				
				//刷新这些订单
				$order = model('order')->getOrderListNew(array('order_polling'=>array('in','0,1,2'),'store_id'=>array('in',$store_id),'order_state'=> 20,'order_polling_show'=> 0),'order_id,order_sn,store_id','order_id asc');	
				
			
				
				foreach($order as $v){	
					
							model('order_polling')->delPolling(array('order_sn' =>$v['order_sn']));	
							
							$this->sendOrder($v['order_sn'],$is_new);	
							
														
				}
							
							
			    foreach($order as $v){
				
					
						$model = model('order_polling');	
						$where = array(
							'order_sn' => $v['order_sn']
							//'send_state' => 1
						);	
						$list = $model->getPollingList($where,'*','order_sort desc,order_id asc');	
						
				//	print_r($list);
						
						//print_r($list);
						
						//print_r($list);	
					
						$this->stocketShow($list[0]);
						
						
				}
					
				
					
					
					
					/*
		
					$where = array(					
								'state' 	=> 0,
								'waiter_id' => $deliver_id							
					);					
					
				//	print_r($where);									
					$polling = model('order_polling')->getPollingInfo($where,'*');				
				//	print_r($polling);
					$this->stocketShow($polling);
					//写入推送列表				
			
			
				*/
						 
	}
	
    private function stocketShow($polling){
	
				
					//如果正在展示的订单就不写入到展示页面里去
					$count = model('order_polling_show')->getShowCount(array('waiter_id'=> $polling['waiter_id']));
				
					if($count > 0){
						
						$where = array(
							'waiter_id' => $polling['waiter_id'],
							'order_sn' => $polling['order_sn']
						);	
						$data = array(
							'send_state' =>  1
						);									
						model('order_polling')->editPolling($data,$where);	
						
						
					}else{		
					
					
						$data = array();
						$data['order_id'] 	= $polling['order_id'];
						$data['order_sn'] 	= $polling['order_sn'];
						$data['waiter_id'] 	= $polling['waiter_id'];
						$data['addtime'] 	= $polling['addtime'];
						$data['store_name'] = $polling['store_name'];								
						model('order_polling_show')->addShow($data);
						
						$where = array(
							'waiter_id' => $polling['waiter_id'],
							'order_sn' => $polling['order_sn']
						);	
						
														
						$data = array(
							'send_state' =>  1
						);			
						
						model('order_polling')->editPolling($data,$where);	
						
						
						$where = array(
							'order_id' => $polling['order_id']
						);
						$data = array(
							'order_polling_show' => 1
						);
						model('order')->editOrder($data,$where);
					}
	}
	
	
	
	
	
	//模拟推送
	private function stocketSend($order_sn = '',$is_new = 0){	
		
		
		    require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
    		$gateway = new \GatewayClient\Gateway();
    		$gateway::$registerAddress = '150.158.195.68:1238';	
    		
    
		
		
				$where = array(
					'order_sn'=>$order_sn,
					'state' => 0,
				//	'send_state' => 0 
				);			
				
				$polling = model('order_polling')->getPollingInfo($where,'*');
				
				$where = array(
						'waiter_id' => $polling['waiter_id'],
						'order_sn' => $polling['order_sn']
				);	
				
				$data = array(
						'send_state' =>  1
				);	
				
				model('order_polling')->editPolling($data,$where);	
				
				if($is_new == 1){	
					
					
				
					$orders = model('order')->getOrderInfoN(array('order_sn' => $order_sn));
					$store = $this->getStore($orders['store_id']);
					$data = array();
                	$data['order_sn'] = $order_sn;
                	$data['type'] = 'rob';
                
					    
					
						$buyer_info = array();
                		$buyer_info['buyer_id'] = $orders['buyer_id'];
                		$buyer_info['buyer_code'] =$orders['buyer_code'];
                		$buyer_info['buyer_name'] = $orders['buyer_name'];
                		$buyer_info['buyer_phone'] = $orders['buyer_phone'];
                		$buyer_info['buyer_address'] = $orders['buyer_address'];
                		$buyer_info['buyer_region_id'] = $orders['buyer_region_id'];
                		$buyer_info['buyer_is_new'] = $orders['buyer_is_new'];
                		$buyer_info['buyer_coordinate'] = $orders['buyer_coordinate'];
                		$buyer_info['buyer_region'] =$orders['buyer_region'];		
                		$buyer_info['buyer_region_color'] = $orders['buyer_region_color'];
                		$buyer_info['buyer_status_code'] = $orders['buyer_status_code'];
    		 
		 
		                $order_info['buyer'] = $buyer_info;
		 
	    
            	      	$store_info = array();
                		$store_info['store_id'] = $store['store_id'];
                		$store_info['store_name_primary'] = $store['store_name_primary'];
                		$store_info['store_name_secondary'] = $store['store_name_secondary'];
                		$store_info['store_code'] = $store['store_code'];
                		$store_info['store_address'] = $store['store_address'];
                		$store_info['store_lat'] = $store['store_lat'];
                		$store_info['store_lng'] = $store['store_lng'];
                		$store_info['region_name'] = $store['region_name'];
                		$store_info['region_color'] = $store['region_color'];
                		$store_info['store_phone'] = $store['store_phone'];
                		$store_info['store_status_code'] = $orders['store_status_code'];
		    
		                $order_info['store'] =$store_info;
		
		    
            		    $order = array();
            		    $order['order_sn'] = $orders['order_sn'];
                		$order['delivery_time'] = $orders['delivery_time'];
                		$order['delivery_type'] = $orders['delivery_type'];
                		$order['order_state'] = $orders['order_state'];
                		$order['delivery_fee'] = $orders['delivery_fee'];
                		$order['delivery_comment'] = $orders['deliver_comment'];
                		
                		$time = time();
                		if($time > $orders['order_estimate_time']){
                		    $estimate_time = 0;
                		}else{
                		    $estimate_time = $orders['order_estimate_time'] - time();
                		}
                		
                		
                		$order['order_estimate_time'] = $estimate_time;
                	 //   $order['delivery_state'] = $this->waiter_info['delivery_state'];
            		    $order['order_id'] = $orders['order_id'];
            		    $order_info['order'] = $order;
		
				    	$data['order_info'] = $order_info;
					
					    
					    $where = array(
							'order_id' => $polling['order_id']
						);
					
						model('order')->editOrder(array('socket_send_time' => time()),$where);
					
					
						$data['closetime'] =  (time() + 30) - time();
                	
    					$data = json_encode($data);	
    					$gateway::sendToUid($polling['waiter_id'],$data);
        		
    					
    					$this->stocketShow($polling);
					
				}
	}
	
	
	
	
	
	
	
	private function getStore($store_id){
		
		
		return  model('store')->getStoreInfo(array('store_id' => $store_id));
		
	}
	
	
	
	
	//距离上一单是否超过6分钟
	private function checkDiffTime($waiter_id,$order){
			   $where = array(
					'distributor_id' => $waiter_id,
					'daycode' => 1,
					'order_state' => array('in','30,40')				
				);
				$info = model('order')->getOrderInfoN($where);
				if(!empty($info)){
					$old_time = $info['add_time'];
					$new_time = $order['add_time'];
					$diff = $new_time - $old_time;					
					if($diff > 360){
						return 1;	
					}else{
						return 0;
					}				
				}else{					
					return 0;
				}		
	}
	
	
	
		//获取等待时间最长下单时间最久的那个
	private function deliver_last_time($count,$waiter_id){
		
		if($count > 0){		
			//获取下单时间最久的
			$where = array(
				'distributor_id' => $waiter_id,
				'order_state' => array('in','30,40')
			);
			$row = model('order')->getOrderInfoN($where,'*','add_time asc');
			return $row['add_time'];
						
		}else{
			
			//获取上线时间
			$where = array(
				'distributor_id' => $waiter_id,
				'stoket_state' => 0
			);
			$row = model('stoket')->getStoketInfo($where);
			return $row['updatetime'];
			
		}		
	}
	
	
	//检查此订单是否被该用户拒绝过
	private function checkRefuse($order_sn,$waiter_id){
		
		$where = array(
			'order_sn' => $order_sn,
			'waiter_id' => $waiter_id
		);
		$count = model('polling_refuse')->getRefuseCount($where);
		return $count;
	}
	
	
	
	
	
	
	
	//配送员状态
	public function deliverStateOp(){		
		$_POST = json_decode(file_get_contents('php://input'),true);	  	
		$where = array(
			'distributor_id' => $_POST['deliver_id'],
			'order_state' => 30			
		);
		$count = model('order')->getOrderCount($where);		
		if($count == 0 && $_POST['state'] == 1){
			output_error('您当前没有可取餐的订单');
		}		
		$where = array(
			'distributor_id' => $_POST['deliver_id']
		);		
		$data = array(
			'delivery_state' => $_POST['state']
		);		
		$row = model('waiter')->editWaiter($where,$data);
		if($row){
			
			//确认送达
			if($_POST['state'] == 2){
			
				//修改手里的订单为 已送达
				$where = array(
					'distributor_id' => $_POST['deliver_id'],
					'order_state' => 40
				);
				
				$data = array(
					'order_state' => 50
				);
				model('order')->editOrder($data,$where);
				//更新状态为返回中
				
				
				
				$where = array();
				$where['distributor_id'] = $_POST['deliver_id'];
					
				$data = array();
				$data['delivery_state'] = 0;	
								
				model('waiter')->editWaiter($where,$data);
			
			}
			
			if($_POST['state'] == 1){
			
				//修改手里的订单为 去取餐
				$where = array(
					'distributor_id' => $_POST['deliver_id'],
					'order_state' => 30
				);				
				$data = array(
					'order_state' => 40
				);
				model('order')->editOrder($data,$where);
				//更新状态为返回中
			
			
			}
			
		
			output_data('ok');
		}else{
		
			output_error('失败');
		}
		
	
	
	} 
	
	
	//取餐	
	public function takeOrderOp(){	
			$_POST = json_decode(file_get_contents('php://input'),true);	
			$where = array(
					'order_id' => $_POST['order_id']
			);				
			$data = array(
					'order_state' => 50
			);
			$row =  model('order')->editOrder($data,$where);
			if($row){						
			
			      
			        $data_record = array();
			        $data_record['order_id'] = $_POST['order_id'];
			        $data_record['record_msg'] = '配送员已取餐';
			        $data_record['record_time'] = time();
			        model('order_record')->addRecord($data_record);
			
			
			
			
			
			    	
			    $order_info = model('order')->getOrderInfoN($where);
				
    			require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
        		$gateway = new \GatewayClient\Gateway();
        		$gateway::$registerAddress = '150.158.195.68:1238';	
        		
        		$data = array();
        		$data['type'] = 'order_take';
        		$data['data'] = $order_info;
                
              
               // print_r($order_info);
                $data = json_encode($data);	
                $gateway::sendToUid($order_info['store_id'],$data);
            		
			
        		$data = array();
        		$data['type'] = 'order_update';
        		$data['data'] = $order_info;
                
              
               // print_r($order_info);
                $data = json_encode($data);	
                $gateway::sendToUid($order_info['distributor_id'],$data);
            		
			
			
			
			
			
			
				//查询是否还有可以取的餐				
					$where = array(
						'distributor_id' => $_POST['deliver_id'],
						'order_state' => 40			
					);
					$count = model('order')->getOrderCount($where);		
					if($count == 0){
							//如果没有。状态变成配送中
							$where = array();
							$where['distributor_id'] = $_POST['deliver_id'];								
							$data = array();
							$data['delivery_state'] = 2;												
							model('waiter')->editWaiter($where,$data);						
					}		
			
				output_data('ok');								
			}else{							
				output_error('失败');							
			}
			
	
		
	
	}
	
	//送达
	public function giveOrderOp(){	
			$_POST = json_decode(file_get_contents('php://input'),true);	
			$where = array(
					'order_id' => $_POST['order_id']
			);				
			$data = array(
					'order_state' => 60
			);
			$row =  model('order')->editOrder($data,$where);
			if($row){						
			
			     //更新操作工具
			
			        
			        $data_record = array();
			        $data_record['order_id'] = $_POST['order_id'];
			        $data_record['record_msg'] = '配送员已送达';
			        $data_record['record_time'] = time();
			        model('order_record')->addRecord($data_record);
			
			
			
			
			
			
			        $order_info = model('order')->getOrderInfoN($where);
				
        			require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
            		$gateway = new \GatewayClient\Gateway();
            		$gateway::$registerAddress = '150.158.195.68:1238';	
            		
            		$data = array();
            		$data['type'] = 'order_arrive';
            		$data['data'] = $order_info;
                    
                  
                    // print_r($order_info);
                    $data = json_encode($data);	
                    $gateway::sendToUid($order_info['store_id'],$data);
                		
			  
        		
            		$data = array();
            		$data['type'] = 'order_update';
            		$data['data'] = $order_info;
                    
                  
                    // print_r($order_info);
                    $data = json_encode($data);	
                    $gateway::sendToUid($order_info['distributor_id'],$data);
                		
			
			        
			
				    //查询是否还有可以正在配送中的				
					$where = array(
						'distributor_id' => $_POST['deliver_id'],
						'order_state' => 50			
					);
					$count = model('order')->getOrderCount($where);		
					if($count == 0){
							//如果没有。状态变成配送中
							$where = array();
							$where['distributor_id'] = $_POST['deliver_id'];								
							$data = array();
							$data['delivery_state'] = 0;												
							model('waiter')->editWaiter($where,$data);						
					}		
			
				output_data('ok');								
			}else{							
				output_error('失败');							
			}
			
	
		
	
	}
	
	
	
	
	public function getRegionOp(){
		$data= array();
		$region = model('region')->getRegionList(array('region_type'=> 2));		
		
		foreach($region as $k => $v){			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['color'] = $v['region_color'];
			$feature[$k]['properties']['name'] = $v['region_name'];			
			$coordinate = explode("|", $v['region_coordinate']);
			$geo = array();
			foreach($coordinate as  $kk => $vv){
				$geo[$kk] = explode(",",$vv);
			}		
			$feature[$k]['geometry']['type'] = 'Polygon';
			$feature[$k]['geometry']['coordinates'][0] = $geo;			
						
		}
				
		$data['type'] = 'FeatureCollection';
		$data['features'] = $feature;
		$result['type'] = 'geojson';
		$result['data'] = $data;
		output_data($result);
		//echo json_encode(array('code' => 200,'data'=>$data));		
		
		
	}
	
	
	
	
	
	
	
	
	
	private function getDistance($lat1, $lng1, $lat2, $lng2) {
      
	    $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }

	

}



?>