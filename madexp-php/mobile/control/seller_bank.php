<?php
/**
 * 商户银行卡管理
 */


defined('InMadExpress') or exit('Access Invalid!');

class seller_bankControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();
        $this->model_bank = Model('store_bank');
    }

    /**
     * 银行卡列表
     */
    public function bankListOp() {
       $condition = array();
       $condition['store_id'] = $this->store_info['store_id'];
       $bank_list =  $this->model_bank->getStoreBankList($condition);		
       output_data(array('bank_list' => $bank_list));

    }

    /**
     * 地址详细信息
     */
    public function bankInfoOp() {
        $bank_id = intval($_POST['bank_id']);
        if ($bank_id <=  0) {
            output_error('地址参数错误！');
        }
        $condition = array();
        $condition['bank_id'] = $bank_id;
        $bank_info = $this->model_bank->getStoreBankInfo($condition);
        if(!empty($bank_id) && $bank_info['store_id'] ==$this->store_info['store_id']) {
            output_data(array('bank_info' => $bank_info));
        } else {
            output_error('银行卡不存在');
        }
    }

	
	
    /**
     * 删除银行卡
     */
    public function bankDelOp() {

		$bank_id = intval($_POST['bank_id']);
        if ($bank_id <=  0) {
            output_error('删除地址失败！');
        }
        $condition = array();
        $condition['bank_id'] = $bank_id;
        $condition['store_id'] = $this->store_info['store_id'];
        $delete = $this->model_bank->delStoreBank($condition);
        if ($delete){
            output_data('删除地址成功！');
        }else {
            output_error('删除地址失败！');
        }
		
		
    }

    /**
     * 新增地址
     */
    public function bankAddOp() {

        $model_daddress = Model('daddress');
 //保存 新增/编辑 表单
            $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["bank_code"],"require"=>"true","message"=>'请输入银行卡卡号'),
                array("input"=>$_POST["bank_validity"],"require"=>"true","message"=>'请输入有效期'),
                array("input"=>$_POST["bank_cvv"],"require"=>"true","message"=>'请输入CVV码'),
                array("input"=>$_POST["bank_country"],"require"=>"true","message"=>'请选择地址'),
                array("input"=>$_POST["bank_name"],"require"=>"true","message"=>'请选择银行卡类型')             
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                 output_error($error);
            }
            $data = array(
                'store_id' => $this->store_info['store_id'],
                'bank_code' => $_POST['bank_code'],
                'bank_validity' => $_POST['bank_validity'],
                'bank_cvv' => $_POST['bank_cvv'],
                'bank_country' => $_POST['bank_country'],
                'bank_name' => $_POST['bank_name'],            
                'is_default'=>intval($_POST['is_default'])
            );
		
            $bank_id = intval($_POST['bank_id']);
            if ($address_id > 0){
                $condition = array();
                $condition['bank_id'] = $bank_id;
                $condition['store_id'] = $this->store_info['store_id'];
                $update = $this->model_bank->editBank($data,$condition);
                if (!$update){
                     output_error('修改失败！');
                }
            } else {
				
                $insert = $this->model_bank->addStoreBank($data);
                if (!$insert){
                    output_error('增加失败！');
                }
			
            }
             output_data('操作成功！');
    }

}
