<?php
/**
 * 支付
 *

 */



defined('InMadExpress') or exit('Access Invalid!');


class member_paymentControl extends mobileMemberControl {

    private $payment_code;
    private $payment_config;

    public function __construct() {
        parent::__construct();

        if($_GET['op'] != 'payment_list' && !$_POST['payment_code']) {
            $payment_code = 'alipay';

            if(in_array($_GET['op'], array('wx_app_pay', 'wx_app_pay3', 'wx_app_vr_pay', 'wx_app_vr_pay3'), true)) {
                $payment_code = 'wxpay';
            }
            else if (in_array($_GET['op'],array('alipay_native_pay','alipay_native_vr_pay'),true)) {
                $payment_code = 'alipay_native';
            }
            else if (isset($_GET['payment_code'])) {
                $payment_code = $_GET['payment_code'];
            }

            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $payment_code;
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
            if(!$mb_payment_info) {
                output_error('支付方式未开启');
            }

            $this->payment_code = $payment_code;
            $this->payment_config = $mb_payment_info['payment_config'];

        }
		
		
	//	$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
	//	require($stripe_url);		
	//	$stripe_new = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');		
			
		
		
    }

    /**
     * 实物订单支付 新方法
     */
    public function pay_newOp() {
        @header("Content-type: text/html; charset=".CHARSET);
        $pay_sn = $_POST['pay_sn'];
        if (!preg_match('/^\d{18}$/',$pay_sn)){
            output_error('支付单号错误');
        }
        //if (in_array($_GET['payment_code'],array('alipay','wxpay_jsapi'))) {
		//修复订单列表无法使用预付款 
		
		if (isset($_POST['payment_code'])) {
            $model_mb_payment = Model('mb_payment');
            $condition = array();
            $condition['payment_code'] = $_POST['payment_code'];
            $mb_payment_info = $model_mb_payment->getMbPaymentOpenInfo($condition);
		
            if(!$mb_payment_info) {
                output_error('支付方式未开启');
            }

            $this->payment_code = $_POST['payment_code'];
            $this->payment_config = $mb_payment_info['payment_config'];            
        } else {
            output_error('支付方式提交错误');
        }
	
	
		
		$order_info = model('order')->getOrderInfo(array('pay_sn' => $pay_sn,'buyer_id' => $this->member_info['member_id']));
		 
		if($order_info['order_state'] == 0){
			output_error('此订单已取消');
		} 
		
        if($order_info['order_state'] > 10) {
            output_error('此订单已支付');
        }
		
        //第三方API支付
        $this->_api_pay($order_info);

    }

   
	
    /**
     * 第三方在线支付接口
     *
     */
    private function _api_pay($order_info) {
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.$this->payment_code.DS.$this->payment_code.'.php';
        if(!is_file($inc_file)){
            output_error('支付接口不存在');
        }		
        require($inc_file);
        
        if ($this->payment_code == 'stripe') {			
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here: https://dashboard.stripe.com/account/apikeys				
			
			//通过前端传值来判断
			//正常通道		
					
		 	\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');
			$token = $_POST['stripeToken'];
			if($token == ''){				
				output_error('支付卡token错误');		
			}
							 
			$charge = \Stripe\Charge::create([
				'amount' => $order_info['pay_amount'] * 100,
				'currency' => 'aud',
				'description' => '支付订单号：'.$order_info['order_sn'],
				'source' => $token,
			]);		
						
			$model_order = Model('order');	
			if($charge['status'] == 'succeeded'){
			//	$post = serialize($charge);
			//	Log::record($post,Log::POST);
				
			
				$data = array();
                $data['order_id'] = $order_info['order_id'];
                $data['log_role'] = 'buyer';
                $data['log_msg'] = '支付订单';
                $data['log_orderstate'] = 20;
                $model_order->addOrderLog($data);             
               
			   
			   //增加LOG记录
			   $record_data = array();
			   $record_data['order_id'] = $order_info['order_id'];
			   $record_data['order_addtime'] = TIMESTAMP;
			   model('order_record')->addRecord('order_placed', $record_data);        
			   			   
			   
               
                //订单状态 置为已支付
                 $data_order = array();
                 
                 $data_order['order_state'] = $order_info['order_type'] == 2 ? 30 : 20; //如果是自提订单。则直接显示为带商家接单
                 
                 $data_order['payment_time'] = TIMESTAMP;
                 $data_order['payment_code'] = 'stripe';
				 $data_order['payment_name'] = 'stripe';	
				 $data_order['trade_no']	 = $charge['id'];				
				 $data_order['balance_transaction'] = $charge['balance_transaction']; 
				 
				
				//更新stripe扣的点				
			//	$retrieve_data = $stripe_new->balanceTransactions->retrieve($charge['balance_transaction'],[]);
				
			//	$data_order['extxnfee_stripe'] = $retrieve_data['fee'];		
						
				$result = $model_order->editOrder($data_order,array('order_id'=>$order_info['order_id']));
								
				
				output_data(array('order_id'=>$order_info['order_id']));
				
			}else{
				
				output_error($charge['error']);
				
			}
			
        }
		
    }
	
	
	
	
	
	
	
  
	
	
	

    /**
     * 可用支付参数列表
     */
    public function payment_listOp() {
        $model_mb_payment = Model('mb_payment');

        $payment_list = $model_mb_payment->getMbPaymentOpenList();

        $payment_array = array();
        if(!empty($payment_list)) {
            foreach ($payment_list as $value) {
                $payment_array[] = $value['payment_code'];
            }
        }

        output_data(array('payment_list' => $payment_array));
    }

    

}
