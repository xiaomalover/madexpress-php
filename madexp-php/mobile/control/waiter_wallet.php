
<?php
/**
 * 
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_walletControl extends mobileWaiterControl {

	public function __construct(){
		parent::__construct();
		
		
	
	}

	
	//钱包首页信息
	public function indexOp(){
		
		$wallet=array();
		
		
		//手续费
		
		//
		
		
		$wallet['money'] 	= $this->waiter_info['available_predeposit'];
		$wallet['frozen'] 	= $this->waiter_info['freeze_predeposit'];	
		$wallet['service_charge'] = '1%';
		$wallet['acctno'] 	= $this->waiter_info['delivery_acctno'];	
		$wallet['bsb'] 		= $this->waiter_info['delivery_bsb'];	
		
		output_data($wallet);
		
	}
	
	
	//提现记录
	public function cashLogOp(){
		
		$where = array(
			'pdc_distributor_id' => $this->waiter_info['distributor_id']
		);
		
		$result = model('waiter_wallet')->getPdCashList($where);
		output_data($result);		
	}
	
	
	//申请提现
	public function cashAddOp(){
		
		
		
		if($this->waiter_info['is_problem'] == 1 || $this->waiter_info['is_lock'] == 1){			
			output_error('您的账号异常或账号已被禁用，暂时无法提现，请联系平台客服。');			
		}
		
		
		
		
		$obj_validate = new Validate();
		$pdc_amount = abs(floatval($_POST['pdc_amount']));
		$validate_arr[] = array("input"=>$pdc_amount, "require"=>"true",'validator'=>'Compare','operator'=>'>=',"to"=>'0.01',"message"=>'提现金额不正确');
	//	$validate_arr[] = array("input"=>$_POST["pdc_bank_name"], "require"=>"true","message"=>'请输入收款银行');
		//$validate_arr[] = array("input"=>$_POST["pdc_bank_no"], "require"=>"true","message"=>'请输入收款账号');	
		$obj_validate -> validateparam = $validate_arr;
		$error = $obj_validate->validate();
		
			if ($error != ''){
				output_error($error);
			}

			$model_pd = Model('waiter_wallet');			
			$pdc_fee =sprintf("%.2f",($pdc_amount * 0.01));
			//验证金额是否足够
			if (floatval($this->waiter_info['available_predeposit']) < $pdc_amount + ($pdc_fee)){
				output_error('余额不足以支付本次提现，其中包含提现手续费'.$pdc_fee);
			}				
	
			$pdc_sn = $model_pd->makeSn();
			$data = array();
			$data['pdc_sn'] = $pdc_sn;
			$data['pdc_distributor_id'] = $this->waiter_info['distributor_id'];
			$data['pdc_distributor_name'] =  $this->waiter_info['distributor_name'];
			$data['pdc_amount'] = $pdc_amount;			
		//	$data['pdc_bank_id'] = $_POST['bank_id'];
			$data['pdc_add_time'] = TIMESTAMP;
			$data['pdc_payment_state'] = 0;	
			$model_pd->addPdCash($data);						
			$row = $this->transfers($pdc_sn);
			if($row['code'] == 200){
				output_data(array('status'=>'ok'));
			}else{		
				output_error('提现失败');
			}
		}
		
		
	
	
	//向子账号划拨金额
	private function transfers($sn){
		
		$model_pd = Model('waiter_wallet');
		$inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'stripe/stripe.php';
		if(!is_file($inc_file)){
		    output_error('支付接口不存在');
		}
		require($inc_file);		
		$pd_info = Model('waiter_wallet')->getPdCashInfo(['pdc_sn' => $sn,'pdc_distributor_id' => $this->waiter_info['distributor_id']]);					
		$stripe_info = model('store_stripe')->getStoreStripeInfo(['delivery_id' => $this->waiter_info['distributor_id']]);
		
		if(!empty($stripe_info)){	
			
			\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');			
			$transfer = \Stripe\Transfer::create([
			  'amount' => $pd_info['pdc_amount'] * 100,
			  'currency' => 'aud',
			  'destination' => $stripe_info['stripe_id'],
			 // 'transfer_group' => '{ORDER10}',
			]);				
			if($transfer['id']){					
			
			
				$row = Model('waiter_wallet')->editPdCash(['stripe_sn' => $transfer['id'],'pdc_payment_time' => time(),'pdc_payment_state' => 1],['pdc_sn' => $sn,'pdc_distributor_id' => $this->waiter_info['distributor_id']]);								
				
				//扣款存款
				$data = array();
				$data['distributor_id'] = $this->waiter_info['distributor_id'];
				$data['distributor_name'] = $this->waiter_info['distributor_name'];
				$data['amount'] = $pd_info['pdc_amount'];
				$data['order_sn'] = $pd_info['pdc_sn'];			
				$model_pd->changePd('withdraw',$data);	
				
				
				$pdc_fee =sprintf("%.2f",($pd_info['pdc_amount'] * 0.01));
				//扣除提现手续费
				$data = array();
				$data['distributor_id'] = $this->waiter_info['distributor_id'];
				$data['distributor_name'] = $this->waiter_info['distributor_name'];
				$data['amount'] = $pdc_fee;
				$data['order_sn'] = $pd_info['pdc_sn'];
				$model_pd->changePd('withdraw_fee',$data);				
								
				return ['code' => 200];
			}else{				
				return ['code' => 400];
			}
		}else{
			
			return ['code' => 400];
			
		}
		
	}
	
	
	
		//收支记录
	 public function pdLogListOp(){
			
	        $model_pd = Model('waiter_wallet');
	        $condition = array();
	        $condition['lg_distributor_id'] = $this->waiter_info['distributor_id'];
			if($_POST['type'] == 0){
				$condition['lg_av_amount'] = array('gt',0);
			}else{
				$condition['lg_av_amount'] = array('lt', 0);
			}
			
			if($_POST['activities']){
			    $condition['lg_type'] = $_POST['activities'];
			}
			
			
			$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['start_date']);
	        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['end_date']);
	        $start_unixtime = $if_start_date ? strtotime($_POST['start_date']) : null;
	        $end_unixtime = $if_end_date ? strtotime($_POST['end_date']): null;
	        if ($start_unixtime || $end_unixtime) {
	            $condition['lg_add_time'] = array('time',array($start_unixtime,$end_unixtime));
	        }
				
				
			if (floatval($_POST['start_price']) > 0 && floatval($_POST['end_price']) > 0) {
                 $condition['lg_av_amount'] = array('between',floatval($_POST['start_price']).','.floatval($_POST['end_price']));
            }	
			
			
	        $list = $model_pd->getPdLogList($condition,20,'*','lg_id desc');		
			
			$data = array();
			foreach($list as $k=> $v){	
					
					$data[$k]['log_id'] = $v['lg_id'];
					$data[$k]['distributor_id'] = $v['lg_distributor_id'];
					$data[$k]['distributor_name'] = $v['lg_distributor_name'];
					$data[$k]['admin_name'] = $v['lg_admin_name'];
					$data[$k]['type_name'] = $v['lg_desc'];
					$data[$k]['amount'] = $v['lg_av_amount'];
					$data[$k]['freeze_amount'] = $v['lg_freeze_amount'];
					$data[$k]['desc'] = $v['lg_desc'];
					$data[$k]['order_sn'] = $v['lg_order_sn'];
					$data[$k]['order_id'] = $v['lg_order_id'];
					$data[$k]['add_time'] = date('d M Y',$v['lg_add_time']);	
					$data[$k]['invoice_url'] = $v['invoice_url'];
			}	
			$page_count = $model_pd->gettotalpage();
	        output_data(array('bill_list' => $data), mobile_page($page_count));
	
	    }
	   
	   
	   public function pd_typeOp(){
	       
	       $shou = array();
	       $shou = array(
	           array(
	                'name' => 'ALL',
	                'value' => ''
	            ),
	            array(
	                'name' => 'Delivery Income',
	                'value' => 'delivery_income'
	            ),
	            array(
	                'name' => "'MPOIG' Incentive",
	                'value' => 'mpoig_incentive'
	            ),
	            array(
	                'name' => "'Schedule Urself' Incentive",
	                'value' => 'schedule_urself_incentive'
	            ),
	            array(
	                'name' => 'Delivery Fee Reimbursment',
	                'value' => 'delivery_fee_reimbursment'
	            ),
	            array(
	                'name' => 'ME Refund',
	                'value' => 'me_refund'
	            ),
	             array(
	                'name' => 'ME Wallet Top Up',
	                'value' => 'me_wallet_top_up'
	            )
	            
	       );
	       
	       $zhi = array(
	            array(
	                'name' => 'ALL',
	                'value' => ''
	            ),
	            array(
	                'name' => "Delivery Fee Refund",
	                'value' => 'delivery_fee_refund'
	            ),
	            array(
	                'name' => 'Liquidated Damage',
	                'value' => 'liquidated_damage'
	            ),
	            
	            array(
	                'name' => 'Withdrawn',
	                'value' => 'withdraw'
	            ),
	              array(
	                'name' => 'Withdrawn Fee',
	                'value' => 'withdraw_fee'
	            ),
	            
	            array(
	                'name' => 'Incentive Cancelation',
	                'value' => 'incentive_cancelation'
	            ),
	          //  array(
	         //       'name' => '保底收入取消',
	        //        'value' => 'incentive_cancelation'
	        //    ),
	        //    array(
	        //        'name' => 'ME钱包内部转账',
	      //          'value' => 'from_internal_wallet_txn'
	       //     ),
	       );
	       
	       if($_POST['type'] == 0 ){
	           output_data(array('type'=>$shou));
	       }else{
	           output_data(array('type'=>$zhi));
	           
	       }
	       
	       
	       
	       
	   }
	   
	   
	   //钱包流水导出类型的接口
	   public function export_typeOp(){
	       
	       $data = array();
	       $incomes= array(
	            'name' => 'Incomes',
	            'child' => array(
	                  array(
    	                'name' => 'Delivery Income',
    	                'value' => 'delivery_income'
    	            ),
    	            array(
    	                'name' => "'MPOIG' Incentive",
    	                'value' => 'mpoig_incentive'
    	            ),
    	            array(
    	                'name' => "'Schedule Urself' Incentive",
    	                'value' => 'schedule_urself_incentive'
    	            ),
    	            array(
    	                'name' => 'Delivery Fee Reimbursment',
    	                'value' => 'delivery_fee_reimbursment'
    	            ),
    	            array(
    	                'name' => 'ME Refund',
    	                'value' => 'me_refund'
    	            ),
    	             array(
    	                'name' => 'ME Wallet Top Up',
    	                'value' => 'me_wallet_top_up'
    	            )
	            )
	       );
	       
	       $expenditures = array(
	            'name' => 'Expenditures',
	             'child' => array(
	                array(
    	                'name' => "Delivery Fee Refund",
    	                'value' => 'delivery_fee_refund'
    	            ),
    	            array(
    	                'name' => 'Liquidated Damage',
    	                'value' => 'liquidated_damage'
    	            ),
    	            
    	            array(
    	                'name' => 'Withdrawn',
    	                'value' => 'withdraw'
    	            ),
    	              array(
    	                'name' => 'Withdrawn Fee',
    	                'value' => 'withdraw_fee'
    	            ),
    	            
    	            array(
    	                'name' => 'Incentive Cancelation',
    	                'value' => 'incentive_cancelation'
    	            ),
	            )
	       );
	       
	       $data['incomes'] = $incomes;
	       $data['expenditures'] = $expenditures;
	       
	       output_data($data);
	       
	       
	   }
	   
	    
	   public function exportOp(){
	       
		   
		   	       
		   
		   require_once BASE_DATA_PATH. '/resource/phpexcel/PHPExcel/IOFactory.php';		
		   $filename = BASE_UPLOAD_PATH.'/xls/WalletStatement.xlsx';			
		  
		   
		   $objPHPExcel  = PHPExcel_IOFactory::load($filename);
		       
		   $sheet = $objPHPExcel->getSheet(0); // 读取第一個工作表
		   $highestColumm = $sheet->getHighestColumn(); // 取得总列数
		   $highestRow = $sheet->getHighestRow(); // 取得总行数
		   
		   $exlCASArr = array();
		   		   	   
		   //abn 		   
		   $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'Dafa Restaurant');//调用ABN		   
		   //Activity Types		   
		   $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', $this->waiter_common_info['deliivery_abn']);//本次导出的那些数据类型 

		   $activity_types = $_POST['type'];		   	   
		   $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', $activity_types);//本次导出的那些数据类型
		   
		   $start_date = $_POST['start_time'];
		   $end_date   = $_POST['end_time'];

		   //Statement Period	
		   $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', $start_date.$end_date);//导出月份
		   
		     //D.O.I		
			$doi = date('d-M-Y',time());
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2',$doi);//生成日期
		   
			//Opening Balance	   		 
			$opening_balance = 0 ;
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', $opening_balance);//开始金额
			  
			//Ending Balance
			$ending_balance = 0;
		    $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', $ending_balance);//结束金额
		   
		    $where = array();
		    $where['lg_add_time'] = array('between',array(strtotime($_POST['start_time'].' 00:00:01'),strtotime($_POST['end_time'].' 23:59:59')));
		    $where['lg_type'] = array('in',$_POST['type']);
			$where['lg_distributor_id'] = $this->waiter_info['distributor_id'];

			$wallet_list = model('waiter_wallet')->getPdLogList($where);
		
			$num = 5;
			foreach($wallet_list as $v){	

				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$num, date('d-M-y',$v['lg_add_time']));//日期			
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B'.$num, $v['lg_name_of_activities']);//描述
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C'.$num, 'Order No. '.$v['lg_order_sn']);//订单编号
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D'.$num, ' ');//相关
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$num, $v['lg_type_activities'] == 0 ? '$'.$v['lg_av_amount'] : ' ');//收入
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$num, $v['lg_type_activities'] == 1 ? '$'.$v['lg_av_amount'] : ' ');//支出
				$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$num, '$'.$v['lg_balance']);//剩余						   
				$num ++ ;

			}
		   
		   		   
		   // Rename worksheet
		   $objPHPExcel->getActiveSheet()->setTitle('Simple');
		   // Set active sheet index to the first sheet, so Excel opens this as the first sheet
		   $objPHPExcel->setActiveSheetIndex(0);
		       
		   // Redirect output to a client’s web browser (Excel5)
		   header('Content-Type: application/vnd.ms-excel');
		   header('Content-Disposition: p_w_upload;filename="01simple.xlsx"');
		   header('Cache-Control: max-age=0');
		   // If you're serving to IE 9, then the following may be needed
		   header('Cache-Control: max-age=1');
		   // If you're serving to IE over SSL, then the following may be needed
		   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		   header('Last-Modified: '.date('D, d M Y H:i:s').' GMT'); // always modified
		   header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		   header('Pragma: public'); // HTTP/1.0
		   
		   $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		   
		   $url =  BASE_UPLOAD_PATH .'/delivery/wallet_xls/'.time().'.xlsx';
		   $http_url = UPLOAD_SITE_URL.'/delivery/wallet_xls/'.time().'.xlsx';
		   $objWriter->save($url);
		       
		   output_data(array('url' => $http_url));
		   
		   
	   }

    
}
