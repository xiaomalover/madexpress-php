<?php
/**
 * 通信服务
 */


defined('InMadExpress') or exit('Access Invalid!');

class stoketControl extends mobileHomeControl {
	private $PI = 3.14159265358979324;
    private $x_pi = 0;
 
    public function __construct(){
        parent::__construct();
		$this->x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    }

	


	/*
     * 测试发送接口
     */
    
	public function indexOp(){		

		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();
		$gateway::$registerAddress = '150.158.195.68:1238';	
		
		$type = $_POST['type'];
		$uid = $_POST['uid'];
		
		
		$order_sn = $_POST['order_sn'];
		$data = array(
			'order_sn' => $order_sn,
			'type' => $type
		);
		
		
			
		$data = json_encode($data);				
	    $gateway::sendToUid($uid,$data);
    }
	
	
	
	public function eeedOp(){		

		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();
		$gateway::$registerAddress = '150.158.195.68:1238';	
		
		$type = $_POST['type'];
		$uid = $_POST['uid'];
	    
	    /*
        	    
        order_update	收到订单变更通知
        lease_deduction	收到租车扣款通知
        lease_invitation	收到租车邀请通知
        delivery_begins	收到开始配送通知
        scheduling_fail	收到排班失败通知
        */
	
	
	    if($type == 1){
	        
	        	$sendData = array();
				$sendData['type'] = 'order_update';
				$sendData['data'] = array(
				    'order_id' => 1,
				    'order_state' => 40
				    
				);
	    }	
	    if($type == 2){
	        
	        	$sendData = array();
				$sendData['type'] = 'rental_fee_deduction';
				$sendData['data'] = array(
				    'lease_id' => 1,
				    'message' => '本次扣款100元'
				    
				    
				);
	    }
	    if($type == 3){
	        
	        	$sendData = array();
				$sendData['type'] = 'rental_invitation';
				$sendData['data'] = array(
				    'lease_id' => 1
				);
	        
	    }
	    if($type == 4){
	        
	        	$sendData = array();
				$sendData['type'] = 'delivery_phase_begin';
				$sendData['data'] = array(
				    'order_id' => $order_info['order_id'],
				    'message' => '请开始您的表演'
				    
				);
	        
	        
	    }
		
		  if($type == 5){
	        	$sendData = array();
				$sendData['type'] = 'scheduling_fail';
				$sendData['data'] = array(
				    'scheduling_id' => 1,
				    'message' => '本次排班失败'
				    
				);
	        
	        
	    }
		
		
	
			
		$data = json_encode($data);				
	    $gateway::sendToUid($uid,$data);
    }
	
	
	
	public function sendOp(){
		
			$stoket =  model('stoket');				
			$stoket->send($_POST['order_sn']);	
			
	}
	
public function sendStateOp(){
		$uid = $_POST['uid'];
		$stoket =  model('stoket');				
		$stoket->sendStatusWaiter($uid,'4');	
	
	}
	
   public function abcdeOp(){
		
		$order_sn = '2000000000008701';
		
	
		$where = array(
			'order_sn'=>$order_sn
		);
		$polling = model('order_polling')->getPollingInfo($where);	
			
			
		print_r($polling);
		
	}
	
	
	
	//绑定骑手ID
	public function binduidOp(){
		
		
		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();
		$gateway::$registerAddress = '192.168.0.106:1238';	
		
		
		$gateway::sendToUid(1,json_encode(array(
            'type'      => 'rob',
            'order_sn' => '1232141234'
        )));
		
		
		
		print_r($data);
		
		
		
		
	}

	//拾取的坐标 118.784007,31.97979
	
	public function xxOp(){
		
		$type = $_POST['type'];
		
		$lbs = new lbs();
		
	
		//$latitude = '31.982385';
		//$longitude = '118.771951';
		
		$lat =  $_POST['lat'];
		$lng = $_POST['lng'];
		$waiter_id = $_POST['waiter_id'];
		$row = $lbs->createPoi('waiter', $waiter_id, $lat, $lng, 3);
		
		if($row){
			
			$where = array(
				'waiter_id' => $waiter_id
			);
			
			$update = array(
				'lbs_id' => $row['id'],
				'region_coordinate' => $lat.','.$lng
			);
			
			model('waiter')->editWaiter($where,$update);
			
		}
		
		
		print_r($row);
		
		
		
		
		
	}
	
	public function bbbOp(){
		
		
			
		$lbs = new lbs();
		
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];
		$uid = $_POST['uid'];
		
		
		$row = $lbs->updatePoi('waiter', $uid , $lat, $lng, 3);
		
		print_r($row);
		
		
	}
	
	public function cccOp(){
		
			$points = '118.772524,31.974905';
			$lbs = new lbs();
			
		
		print_r($row);
	}
	
	public function luxianOp(){
		
		
		
			$lbs = new lbs();
			$origin = $_POST['origin'];
		    $destination = $_POST['destination'];
		
		
		
		
		    $row = $lbs->directionAbroad($origin,$destination);
			print_r($row);
		
		
	}
	
	
	
	
	public function getStoreOp(){
		
		
		$color = array('#f40606','#0000ff','#009245','#ff00ff','#754c24','#f7931e');		
		$where = array();
		$where['store_state'] = 1;
		$store = model('store')->getStoreList($where);
		foreach($store as $k => $v){
			
			$data[$k]['store_id'] = $v['store_id'];
			$data[$k]['store_name'] = $v['store_name_primary'];			
			$data[$k]['store_lat'] = $v['store_lat'];
			$data[$k]['store_lng'] = $v['store_lng'];		
			$data[$k]['store_yunli'] = $this->storeWaiter($v['store_lng'].','.$v['store_lat'],$v['store_name']);
			$data[$k]['store_color'] = $color[$k];				 
			
		}
		
		output_data(array('store'=>$data));
	//	exit(json_encode(array('store'=>$data)));
	
	}
	
	
	public function getUserOp(){
		
	
		
		
	}
	
	
	public function getWaiterOp(){
		
		$where = array();
		$where['distributor_status'] = 1;
		$where['distributor_online'] = 20;
		$store = model('waiter')->getWaiterList($where);
		foreach($store as $k => $v){
			$data[$k]['waiter_id'] = $v['distributor_id'];
			$data[$k]['waiter_name'] = $v['distributor_name'];			
			$point = explode(',',$v['region_coordinate']);
			$data[$k]['lat'] = $point[1];
			$data[$k]['lng'] = $point[0];
			$data[$k]['order_num'] =0;			
		}
		
		output_data(array('waiter'=>$data));
		
	}
	
	
	
	//计算运力
	
	
	public function capacity(){
		
		
		
	}
	
	
	
	public function getUser(){
	
		$where = array();		
		$order_list = model('order')->getOrderList($where);
		
	
		
	
		
	}
	
	
	
	
	//获取个个商家范围内的可用运力
	private function storeWaiter($latlng,$store_id){
							
		$lbs = new lbs();
		$result = $lbs->searchPoi('waiter', $latlng);						
		if($result['status'] == 0){
			//查询当前配送员是否有订单。
			$yunli_data = 2;
			$yunli = 0;		
			foreach($result['contents'] as $v){				
				$orderNum = $this->waiterOrderNum($v['waiter_id']);				
				if($orderNum < $yunli_data ){
					$yunli += $yunli_data - $orderNum;	
				}else{
					$yunli += 0;
				}
				
			
				
			}	
			return  $yunli ;
		}else{
			return 0 ;	
		}	
	}
	
	
	//检测本次抓取是否有锁定中的运力。
	private function checkLock($waiter_id){		
		$num = 0;
		$where = array(
			'waiter_id' => $waiter_id
		);
		$num = model('stoket_lock')->getLockCount($where);
		return $num;
	}
	
	
	
	
	
	
	//判断当前配送员有几个单子。	
	private function waiterOrderNum($waiter_id){		
	   $count = 0;
		$where = array(
			'distributor_id' =>$waiter_id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	
	
	//拖拽坐标更新配送员的新位置
	
	
	public function editWaiterPointOp(){
	
	
		$waiter_id = $_POST['waiter_id'];		
		$row = model('waiter')->getWaiterInfoByID($waiter_id,'lbs_id');
		
		$lbs_id = $row['lbs_id'];		
		$lat = $_POST['lat'];
		$lng = $_POST['lng'];	
		$lbs = new lbs();
		$result = $lbs->updatePoi('waiter', $lbs_id,$lat,$lng);		
		if($result['status'] == 0){
			$where = array(
				'distributor_id' => $_POST['waiter_id']
			);
			$data = array(
				'region_coordinate' => $lng.','.$lat
			);
			model('waiter')->editWaiter($where,$data);
			output_data('ok');			
		}else{
			output_error('更新失败');
		}
		
	}
	
	public function editStorePointOp(){
	
		$where = array(
			'store_id' => $_POST['store_id']
		);
		$row = model('store')->getStoreInfo($where,'lbs_id,store_lat,store_lng');
		
		
		
		$lbs_id = $row['lbs_id'];		
		$lat = $row['store_lat'];
		$lng = $row['store_lng'];	
		$lbs = new lbs();
		$result = $lbs->updatePoi('store', $lbs_id,$lat,$lng);		
		
		print_r($result);
		if($result['status'] == 0){
			output_data('ok');			
		}else{
			output_error('更新失败');
		}
		
	}
	
	
	
	
	
	

	
	
	public function shipOrderOp(){
		
		$where = array(
			'order_state' => array('in','30,40')
		);
		
		
		
		$list = model('order')->getOrderListNew($where);
		$data = array();
		$juli = 5000;
		foreach($list as $k => $v ){
			
			$data[$k]['truename'] = $v['buyer_name'].'-'.$v['order_sn'].'-'.$v['distributor_name'];			
			$buyer_points =  explode(',',$v['buyer_coordinate']);			
			$data[$k]['buyer_lat'] = $buyer_points[1];
			$data[$k]['buyer_lng'] = $buyer_points[0];			
			$waiter_coordinate	 = 	$this->waiterInfo($v['distributor_id']);
			$waiter_points =  explode(',',$waiter_coordinate);			
			$data[$k]['waiter_lat'] = $waiter_points[1];
			$data[$k]['waiter_lng'] = $waiter_points[0];

			$distance = $this->getDistance($waiter_points[0],$waiter_points[1],$buyer_points[0],$buyer_points[1],1);
			
			$data[$k]['distance'] = $distance;
			
			if($distance < 5000 ){
				
				$quan = 5000 - $distance;
				
			}else{
   			    $quan = 0;	
			}
			
			$data[$k]['quan'] = $quan;
			
			
		}		
		output_data(array('list'=>$data));
	}
	
	
	
	private function waiterInfo($waiter_id){		
		$where = array(
			'distributor_id' => $waiter_id
		);
		$row = model('waiter')->getWaiterInfo($where,'region_coordinate');
		return $row['region_coordinate'];
	}
	
	
	public function getLogOp(){
		
		$data = model('stoket_log')->getSlogList(TRUE);
		
		$list = array();
		foreach($data as  $k => $v){
			$list[$k] = $v;
			$list[$k]['log_time'] = date('Y-m-d H:i:s',$v['log_time']);
		}
		
		output_data(array('list'=>$list));		
		
	}
	

	//新增一笔订单
	
	public function addorderOp(){				
		$where = array(
			'store_id' => 0
		);
		$order = model('order')->getOrderInfoN($where,'*','order_id desc');
		if($order){		
		
			
			$store_info = model('store')->getStoreInfo(array('store_id'=> $_POST['store_id']));					
			$data= array();
			$data['store_id'] = $store_info['store_id'];
            $data['store_name'] = $store_info['store_name'];
			$data['store_phone'] = $store_info['store_phone'];
			$data['store_address'] = $store_info['store_address'];
			$data['store_region_id'] = $store_info['region_id'];
			$data['store_region_sm_id'] = $store_info['region_sm_id'];			
		    $data['store_coordinate'] = $store_info['store_lng'].','.$store_info['store_lat'];	
			$data['add_time'] = time(); //$this->getMillisecond();
					   
		    $where = array(
			   	'order_id' => $order['order_id']
		    );		
					   
		   $row = model('order')->editOrder($data,$where);		
		   if($row){
			   
			   $log = array(
			   		'order_sn' => $order['order_sn'],
					'log_msg' => $store_info['store_name'].'有新订单:'.$order['order_sn'],
					'log_time' => time()
			   );
			   model('stoket_log')->addSlog($log);
			  
			   //锁定运力。
			  // $this->lockWaiter($order['order_sn']);
				
			   //开始轮训
			   $this->polling($order['order_sn']);
			   
			
						   
			   	output_data('ok');
			   
			}else{
				 output_error('订单添加失败');
		    }
			
		}		
		
		
	}
	
		
	
	
	public function ceshiOp(){
		
		$order_sn = $_POST['order_sn'];
		
		$order_info = model('order')->getOrderInfo(array('order_sn'=>$order_sn));		
		
		print_r($order_info);
		
		$overlying = $this->checkOverlying(5,$order_info);
		 
		 print_r($overlying);
		 
		 
	}
	

	//生成轮询
	private function polling($order_sn = ''){		
		$stoket =  model('stoket');			
		$order_info = model('order')->getOrderInfo(array('order_sn'=>$order_sn));
		if(empty($order_info)){
			output_error('订单不存在');
		}
	
	
	/*	$lbs = new lbs();		
		$latlng = $order_info['store_coordinate'];
		
		
		
		$result = $lbs->searchPoi('waiter', $latlng);	
		foreach($result['contents'] as $v){
			$ids .= $v['waiter_id'].',';
		}		
		$where =array(
			'distributor_id' => array('in',$ids),
			'distributor_online' => 20
		);
		$waiter = model('waiter')->getWaiterList($where);	
	*/	
	
			$overlap = $this->overlyingStore($order_info['store_coordinate'],$order_info['store_id']);	
			foreach($overlap['waiter'] as $v){
				//检查运力			
				//$where = array(
				//	'waiter_id' => $v['distributor_id'],
				//	'order_sn' => $order_info['order_sn']
			//	);
			//	$num = model('stoket_lock')->getLockInfo($where);	
				
				$count = $this->waiter_order_count($v['distributor_id']);			
				
				//echo '单量'.$count;
				
				//检查当前 配送员 是否有单			
				//没有订单，则分配个商户围栏内的配送员			
				//如果有一单，检查是否为本商家的订单，			
				//如果有一单，检查是否为叠加范围内商户，并判断他是否在叠加商户记录内。
				
				
				//检查本单是否与上一单超过6分钟。			
				$diffTime = $this->checkDiffTime($v['distributor_id'],$order_info);
				
				//echo '时间差'.$diffTime;
				
				//检查叠加单范围	
				if($count > 0 ){		
					$overlying = $this->checkOverlying($v['distributor_id'],$overlap['store']);
				}else{
					$overlying = 1;	
				}
			
				//检查是否已拒绝过本单	
				$refuse = $this->checkRefuse($order_info['order_sn'],$v['distributor_id']);
				//echo '拒绝'.$refuse;
					
				
				//检查本单是否为列队等待单
				
			/*	echo $count;
				echo '|';
				echo $refuse;
				echo '|';						
				echo $overlying;
				echo '|';
				echo $diffTime;
				echo '|';			
				*/
				
				
				if($count  < 3  && $refuse == 0  && ($overlying == 1 && $diffTime == 0)){		
					$polling_data[] = array(
						'order_id' => $order_info['order_id'],
						'order_sn' => $order_info['order_sn'],
						'waiter_id' => $v['distributor_id'],
						'state' => 0,
						'addtime' => time(),					
						'order_number' => $count ,					
						'last_time' => $this->waiter_last_time($count ,$v['distributor_id']),
						'order_sort' => $this->polling_sort($v['distributor_id'],$order_info)
					);
					
					//写入lOG
					$log = array(
						'order_sn' => $order_info['order_sn'],
						'log_msg' => '轮询：'.$v['distributor_name'].'，单号：'.$order_info['order_sn'],
						'log_time' => time()
					);
					model('stoket_log')->addSlog($log);
					
				}			
			}
			
			$row = model('order_polling')->addPollingAll($polling_data);
			if(count($polling_data) > 0){
			//如无可用运力。直接到订单池
				$data = array(
					'order_polling' => 1
				);	
			}else{
				$data = array(
					'order_polling' => 2
				);	
			}
			$where = array(
				'order_sn' => $order_info['order_sn']
			);
			model('order')->editOrder($data,$where);			
			

			if($row){	
				//开始轮询
				$stoket->send($order_info['order_sn']);	
			}
	}
	
	
	//检查周边叠加商户
	private  function overlyingStore($latlng,$store_id){
		
		//$latlng = $_POST['latlng'];		
		//$store_id = $_POST['store_id']; //主
		$lbs = new lbs();		
		$result = $lbs->storePoi('store', $latlng);	
	//	print_r($result);


		foreach($result['contents'] as $v){			
			$ids[] = $v['store_id'].',';	
			$latlng = $v['location'][0].','.$v['location'][1];
			$store[$v['store_id']] = $this->getWaiterList($latlng);
			$diejia_store[] = $v['store_id']; //当前订单所有的叠加范围商户
		}	
		
		//print_r($store);
		$ben = array();
		$beiyong = array();		

			
		foreach($store as $k => $v){
			if($k == $store_id){
				$ben = $v;
			}else{
				foreach($v as $ss){					
					$beiyong[$ss['distributor_id']] =  $ss;
				}
			}			
		}
		
		$main = array();
		foreach($ben as $k => $v){
			$v['type'] = 1;
			$main[$k] = $v;
		}
				
		//叠加商户的运力，如果 配送员有当前商家的订单。那就可以启用。
		$spare =  array_diff_assoc($beiyong,$ben);
		$data_spare = array();
		foreach($spare as $k => $v){
			$v['type'] = 0; //叠加商户的配送员。
			$data_spare[$k] = $v;
		}
		$overlap = array();
		$overlap['waiter'] = array_merge($main,$data_spare);		
		$overlap['store'] = $diejia_store;
		return $overlap;	
	} 
	
	
	
	
	private function getWaiterList($latlng){
	
		$lbs = new lbs();	
		$result = $lbs->searchPoi('waiter', $latlng);	
		
		foreach($result['contents'] as $v){
			$ids .= $v['waiter_id'].',';
		}		
		$where =array(
			'distributor_id' => array('in',$ids),
			'distributor_online' => 20
		);
		$waiter = model('waiter')->getWaiterList($where,'distributor_id,distributor_name,distributor_code');	
		$data = array();
		foreach($waiter as $v){
			$data[$v['distributor_id']] = $v;
		}
		return $data;
	}
	
	
	
	
	//检查是否是叠加单配送员
	private function checkOverlying($waiter_id,$overlap_store){
			
			   $where = array(
					'distributor_id' => $waiter_id,
					'daycode' => 1,
					'order_state' => array('in','30,40')				
				);
				$info = model('order')->getOrderInfoN($where);				
				
				
				if(in_array($info['store_id'],$overlap_store)){		
					return 1;
				}else{
					return 0;	
				}
				
				 /*
				$old_point =  explode(",", $info['store_coordinate']);															
			//	print_r($info);
				
				//已接订单 
				$circle = array(
					'center'=>array('lng'=>$old_point[0],'lat'=>$old_point[1]),
					'radius'=> 3000
				);	
			
			
				$new_point =  explode(",", $order['store_coordinate']);						 			
				//第2单订单的商家坐标
				$point = array('lng'=>$new_point[0],'lat'=>$new_point[1]);
				
			//	print_r($old_point);
			//	print_r($point);
			//	echo '<br>';
			//	print_r(time());
			//	echo '<br>';
				return  $this->is_point_in_circle($point,$circle);	
			//	print_r('*******************');
			//	print_r($abc);
			
			*/
			
			
				
	}
	
	//距离上一单是否超过6分钟
	private function checkDiffTime($waiter_id,$order){		
		
			   $where = array(
					'distributor_id' => $waiter_id,
					'daycode' => 1,
					'order_state' => array('in','30,40')				
				);
				$info = model('order')->getOrderInfoN($where);
				if(!empty($info)){
					$old_time = $info['add_time'];
					$new_time = $order['add_time'];
					$diff = $new_time - $old_time;
					
					if($diff > 360){
						return 1;	
					}else{
						return 0;
					}
				
				}else{
					
					return 0;
				}
		
		
		
		
	}
	
	//检查此订单是否被该用户拒绝过
	private function checkRefuse($order_sn,$waiter_id){
		
		$where = array(
			'order_sn' => $order_sn,
			'waiter_id' => $waiter_id
		);
		$count = model('polling_refuse')->getRefuseCount($where);
		return $count;
	}
	
	
	
	//获取当前配送员人数
   private function  checkWaitercount($order_info){
		$lbs = new lbs();		
		$latlng = $order_info['store_coordinate'];
		$result = $lbs->searchPoi('waiter', $latlng);				
		return $result['total'];
	}
	
	
   //锁定运力
	private function lockWaiter($data){
		
	
				//锁定运力后跳出
				$data = array();
				$data['waiter_id'] = $data['distributor_id'];
				$data['order_sn'] = $data['order_sn'];
				$data['store_id'] = $data['store_id'];
				$data['addtime'] = time();				
				model('stoket_lock')->addLock($data);
				//写入lOG
			    $log = array(
			   		'order_sn' => $data['order_sn'],
					'log_msg' => '锁定运力：编号'.$data['distributor_id'].'_'.$data['distributor_name'].'，单号：'.$data['order_sn'],
					'log_time' => time()
			    );
			    model('stoket_log')->addSlog($log);
				
	}
	
	
	//解除运力锁定		
	private function unlock($order_sn){
		$where = array(
			'order_sn' => $order_sn
		);				
		model('stoket_lock')->delLock($where);
	}
		
		
	
	//获取等待时间最长下单时间最久的那个
	private function waiter_last_time($count,$waiter_id){
		
		if($count > 0){		
			//获取下单时间最久的
			$where = array(
				'distributor_id' => $waiter_id,
				'order_state' => array('in','30,40')
			);
			$row = model('order')->getOrderInfoN($where,'*','add_time asc');
			return $row['add_time'];
						
		}else{
			
			//获取上线时间
			$where = array(
				'distributor_id' => $waiter_id,
				'stoket_state' => 0
			);
			$row = model('stoket')->getStoketInfo($where);
			return $row['updatetime'];
			
		}		
	}
	
	
	//排序	
	private function polling_sort($waiter_id,$order){	
		$count = $this->waiter_order_count($waiter_id);
	
		switch ($count) {
		   case 2: 
				$where = array(
					'distributor_id' => $waiter_id,
					'daycode' => 2,
					'order_state' => array('in','30,40')				
				);
				$info = model('order')->getOrderInfoN($where);
				
				//有两单，第二单本商家配送员
				if($info['store_id'] == $order['store_id']){
					return  1 ;
				}
						
				$old_point =  explode(",", $info['store_coordinate']);															
				//当前订单坐标 
				$circle = array(
					'center'=>array('lng'=>$old_point[0],'lat'=>$old_point[1]),
					'radius'=> 3000
				);	
				$new_point =  explode(",", $order['store_coordinate']);						 			
				//第2单订单的商家坐标
				$point = array('lng'=>$new_point[0],'lat'=>$new_point[1]);
				
				$bool = $this->is_point_in_circle($point,$circle);
				//有两单，在第二单商家叠加单围栏范围
				if($bool == 1){
					return 2;			
				}
		   		
		
			 break;
		   case 1:
		   
				$where = array(
					'distributor_id' => $waiter_id,
					'daycode' => 1,
					'order_state' => array('in','30,40')				
				);
				$info = model('order')->getOrderInfoN($where);	
				
				//首单为本商家的
				if($info['store_id'] == $order['store_id']){
					return 3;
				}	
				
					
				$old_point =  explode(",", $info['store_coordinate']);															
				//当前订单坐标 
				$circle = array(
					'center'=>array('lng'=>$old_point[0],'lat'=>$old_point[1]),
					'radius'=> 3000
				);	
				$new_point =  explode(",", $order['store_coordinate']);						 			
				//第2单订单的商家坐标
				$point = array('lng'=>$new_point[0],'lat'=>$new_point[1]);
				
				$bool = $this->is_point_in_circle($point,$circle);
				//有两单，在第二单商家叠加单围栏范围	
						
				//本商家在配送员首单商家叠加单范围内的
				if($bool == 1){					
					return 4;
				}		
			
			 break;
		   case 0:
		   		   
				return 5;	
				
		   break;		  
		}

		
	}
	
	
	
	
	private function waiter_order_count($id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}



	private function getDistance($lng1, $lat1, $lng2, $lat2, $unit = 2, $decimal = 2)
	{
	
		$EARTH_RADIUS = 6370.996; // 地球半径系数
		$PI           = 3.1415926535898;
	
		$radLat1 = $lat1 * $PI / 180.0;
		$radLat2 = $lat2 * $PI / 180.0;
	
		$radLng1 = $lng1 * $PI / 180.0;
		$radLng2 = $lng2 * $PI / 180.0;
	
		$a = $radLat1 - $radLat2;
		$b = $radLng1 - $radLng2;
	
		$distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
		$distance = $distance * $EARTH_RADIUS * 1000;
	
		if ($unit === 2) {
			$distance /= 1000;
		}
	
		return round($distance, $decimal);
	}

	//取消订单后触发再次生成轮训
	public function startPollingOp(){
			$stoket =  model('stoket');			
			$stoket->send($_POST['order_sn']);	
	}
	


	//检查订单池
	public function  order_poolOp(){
		
		$where = array(
			'order_polling' => 2
		);
		$data = model('order')->getOrderListNew($where);
		
		//$this->polling($v['order_sn']);
			
		
		output_data(array('count'=> count($data)));
		
		
		
	}




	




	//批量添加订单
	public function order_batchOp(){
		


		
	}


	
	
	/**
     * 判断一个坐标是否在圆内
     * 思路：判断此点的经纬度到圆心的距离  然后和半径做比较
     * 如果此点刚好在圆上 则返回true
     * @param $point ['lng'=>'','lat'=>''] array指定点的坐标
     * @param $circle array ['center'=>['lng'=>'','lat'=>''],'radius'=>'']  中心点和半径
     */
    function is_point_in_circle($point, $circle){
 
        $distance = $this->distance($point['lat'],$point['lng'],$circle['center']['lat'],$circle['center']['lng']);
		print_r($distance);
		print_r($circle);
        if($distance <= $circle['radius']){
            return 1;
        }else{
            return 0;
        }
    }
/**
     *  计算两个点之间的距离
     * @param $latA  第一个点的纬度
     * @param $lonA  第一个点的经度
     * @param $latB  第二个点的纬度
     * @param $lonB  第二个点的经度
     * @return float
     */
    function distance($latA, $lonA, $latB, $lonB)
    {
        $earthR = 6371000.;
        $x = cos($latA * $this->PI / 180.) * cos($latB * $this->PI / 180.) * cos(($lonA - $lonB) * $this->PI / 180);
        $y = sin($latA * $this->PI / 180.) * sin($latB * $this->PI / 180.);
        $s = $x + $y;
        if ($s > 1) $s = 1;
        if ($s < -1) $s = -1;
        $alpha = acos($s);
        $distance = $alpha * $earthR;
        return $distance;
    }

	
	//毫秒级时间戳
	 private function getMillisecond() {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }
   
	
	public function huanyuanOp(){
			//删除排队
			model('order_polling')->delPolling(TRUE);
		
			//删除
			model('polling_refuse')->delRefuse(TRUE);
			
		
			$data = array(
				'store_id' => 0,
				'store_name' => '',
				'store_phone' => '',
				'store_address' => '',
				'store_region_id' => '',
				'store_region_sm_id' => '',
				'store_coordinate' => '',
				'daycode' => '',
				'distributor_id' => '',
				'distributor_name' => '',
				'distributor_mobile' => '',
				'order_state'=> 20,
				'order_polling' => 0
			);	
			model('order')->editOrder($data,TRUE);
			
			
			$data = array(
				'distributor_online' => 10
			);
			model('waiter')->editWaiter(TRUE,$data);
			
			
			
			
			
			
			model('stoket')->delStoket(TRUE);
			
			output_data('ok');
		
	}
	
	

	
	
	public function ceshiOrderOp(){
		
		$model = model('order');
		$where = array();
		$where['order_sn'] = $_POST['order_sn'];
		
		
	
		
		$data = array();		
		$data['order_state'] = $_POST['order_state'];
		
		
		if($_POST['order_state'] == 30){				
				$data['distributor_id']	= 1;
				$data['distributor_name'] = '1号配送员';
				$data['distributor_mobile']	= '17327080106' ;	
				$data['distributor_start_time'] = time();
		}
		
		
		
		
		$row = $model->editOrder($data,$where);
		if($row){
			
			$order_info = $model->getOrderInfo($where); 	
		
			
			if($_POST['order_state'] == 30){		
					
					$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = 'waiter';
					$data['log_msg'] = '送餐员已接单';
					$data['log_user'] = $order_info['distributor_id'];					
					$data['log_orderstate'] = 30;
					$model->addOrderLog($data);
					
			}	
			
				if($_POST['order_state'] == 40){		
					
					$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = 'store';
					$data['log_msg'] = '商家已接单';
					$data['log_user'] = $order_info['store_id'];					
					$data['log_orderstate'] = 40;
					$model->addOrderLog($data);
					
			}	
			
			
			
			
			
			if($_POST['order_state'] == 50){		
					
					$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = 'waiter';
					$data['log_msg'] = '送餐员已到店取货';
					$data['log_user'] = $order_info['distributor_id'];					
					$data['log_orderstate'] = 50;
					$model->addOrderLog($data);
					
			}	
			
			if($_POST['order_state'] == 60){
				$data = array();
				$data['order_id'] = $order_info['order_id'];
				$data['log_role'] = 'waiter';
				$data['log_msg'] = '送餐员已确认送达';
				$data['log_user'] = $order_info['distributor_id'];
				if ($msg) {
					$data['log_msg'] .= ' ( '.$msg.' )';
				}
				$data['log_orderstate'] = 60;
				$model->addOrderLog($data);		
			
			}			
			
			output_data('ok');
		}else{
			output_error('操作失败');
		}
			
			
			
		
		
	}
	
	
}