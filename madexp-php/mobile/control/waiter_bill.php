<?php
/**
 * 
 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_billControl extends mobileWaiterControl {

	public function __construct(){
		parent::__construct();
	}

	
	//钱包首页信息
	public function indexOp(){
		
		$model = model('bill');		
		$where = array();
	    
	    $where['from_id'] = $this->waiter_info['distributor_id'];
	    $where['form_type'] = 2;
	
		$query_start_price  = $_POST['start_price'];
		$query_end_price 	= $_POST['end_price'];
		
		if($_POST['type']){
		    $where['bill_type'] = $_POST['type'];    
		}
		
		
		$price_from = preg_match('/^[\d.]{1,20}$/',$query_start_price) ?$query_start_price : null;		
		$price_to = preg_match('/^[\d.]{1,20}$/',$query_end_price) ? $query_end_price : null;
		
		if ($price_from && $price_to) {
		    $where['bill_price'] = array('between',"{$price_from},{$price_to}");
		} elseif ($price_from) {
		    $where['bill_price'] = array('egt',$price_from);
		} elseif ($price_to) {
		    $where['bill_price'] = array('elt',$price_to);
		}
	
	    
	    $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['query_end_date']);
        $start_unixtime = $if_start_date ? strtotime($_POST['query_start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_POST['query_end_date']): null;
     
	    if ($start_unixtime || $end_unixtime) {
            $where['bill_addtime'] = array('time',array($start_unixtime,$end_unixtime));
        }
        
		
		$list = $model->getBillList($where,'*',$this->page);
	    foreach($list as $k => $v){
	        
	        $v['pdf'] = UPLOAD_SITE_URL.'/'.$v['bill_pdf'];
	        $list_data[$k] = $v;
	        
	    }
	
		$page_count = $model->gettotalpage();
		output_data(array('bill_list' => $list_data), mobile_page($page_count));
		
		
	}
	
	
	public function bill_typeOp(){
		
		$data = array(
			
			array(
    			'name' => 'TAX SUMMARY',
    			'value' => 'tax_summary'
		    ),
		    array(
    			'name' => 'DELIVERY INVOICE',
    			'value' => 'delivery_invoice'
		    ),
		    array(
    			'name' => 'CREDIT NOTE',
    			'value' => 'credit_note'
		    ),
		    array(
    			'name' => 'ME INVOICE',
    			'value' => 'me_invoice'
		    ),
		    array(
    			'name' => 'ME RCTI',
    			'value' => 'me_rcti'
		    ),
			
		    array(
    			'name' => 'BOND RECEIPT',
    			'value' => 'bond_receipt'
		    ),
			
			array(
    			'name' => 'BOND RELEASE',
    			'value' => 'bond_release'
		    ),
		    array(
		        'name' => 'OTHERS',
		        'value' => 'others'
		    )
		);
		
		output_data(array('type' => $data));

		
	}
	

    
}
