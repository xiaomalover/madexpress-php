<?php
/**
 * 会员评价
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_evaluateControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }
    
    /**
     * 评论
     */
    public function indexOp() {
        $order_id = intval($_POST['order_id']);
        $return = Logic('member_evaluate')->validation($order_id, $this->member_info['member_id']);
        if (!$return['state']) {
            output_error($return['msg']);
        }
        extract($return['data']);
        $store = array();
        $store['store_id'] = $store_info['store_id'];
        $store['store_name'] = $store_info['store_name'];      
		$store['store_avatar'] = $store_info['store_avatar']
            ? UPLOAD_SITE_URL.'/'.ATTACH_STORE.'/'.$store_info['store_avatar']
            : UPLOAD_SITE_URL.'/'.ATTACH_COMMON.DS.C('default_store_avatar');
		
		$waiter= array();
		if($waiter_info){
		
	
    		$waiter['waiter_id'] = $waiter_info['distributor_id'];
    		$waiter['waiter_name'] = $waiter_info['distributor_name'];
    		$waiter['waiter_code'] = $waiter_info['distributor_code'];
    		$waiter['waiter_mobile'] = $waiter_info['distributor_mobile'];
    		$waiter['waiter_avatar'] = getWaiterAvatar($waiter_info['distributor_avatar']);
    		$waiter['waiter_duration'] = $order_info['distributor_duration'];
    		
		}
		
		
        output_data(array('store_info' => $store, 'order_info' => $order_info,'waiter_info' => $waiter));
    }
    
    /**
     * 评论保存
     */
    public function saveEvaluateOp() {
        $order_id = intval($_POST['order_id']);
        $return = Logic('member_evaluate')->validation($order_id, $this->member_info['member_id']);
        if (!$return['state']) {
            output_error($return['msg']);
        }
      
        extract($return['data']);
        $return = Logic('member_evaluate')->save($_POST, $order_info, $store_info, $waiter_info,$order_goods, $this->member_info['member_id'], $this->member_info['member_name'],$this->member_info['member_code']);

        if(!$return['state']) {
            output_data($return['msg']);
        } else {
            output_data('评价成功');
        }
    }
	
	
	
    public function upFileOp(){
		
			$upload = new UploadFile();
            $upload->set('default_dir',ATTACH_EVALUATE);           
			//上传图片
            if (!empty($_FILES['file']['tmp_name'])){   
            	
				$thumb_width    = '200';
                $thumb_height   = '200';
                $upload->set('thumb_width', $thumb_width);
                $upload->set('thumb_height',$thumb_height);	
				$upload->set('thumb_ext',   '_small');
                $upload->set('file_name', '');
                $result = $upload->upfile('file');
                if ($result){     
					 output_data(array('url'=> UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'.$upload->file_name,'file_name'=>$upload->file_name ));               
					//output_data($upload->file_name);
                }else {
                    output_error($upload->error);
                }	
                
            }else{
				output_error('上传失败');
			}
		
		
		
		
	}
	
	
	public function upFileAllOp(){
		
		
			$evaluate_pic = array();
	        $evaluate_pic[1] = 'file1';
	        $evaluate_pic[2] = 'file2';
	        $evaluate_pic[3] = 'file3';
	    	$evaluate_pic[4] = 'file4';
	    	$evaluate_pic[5] = 'file5';
	    	$evaluate_pic[6] = 'file6';
	    	$evaluate_pic[7] = 'file7';
	    	$evaluate_pic[8] = 'file8';
	    	$evaluate_pic[9] = 'file9';
	      
	        $pic_name = array();
	        $upload = new UploadFile();
	        $uploaddir = ATTACH_EVALUATE;
	        $upload->set('default_dir',$uploaddir);
	        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
	        	
	        $thumb_width    = '200';
            $thumb_height   = '200';
	    	$upload->set('thumb_width', $thumb_width);
            $upload->set('thumb_height',$thumb_height);	
			$upload->set('thumb_ext',   '_small');   
			
	        $count = 1;
	        foreach($evaluate_pic as $pic) {
	            if (!empty($_FILES[$pic]['name'])){
	                $result = $upload->upfile($pic);
	                if ($result){
	                    $pic_name[$count] = $upload->file_name;
	                    $upload->file_name = '';
	                } else {
	                    $pic_name[$count] = '';
	                }
	            }
	            $count++;
	        }
	        
	        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.ATTACH_EVALUATE.'/'));
	        
	        
	      
			
		
				
	}
	
	


    
 
}
