<?php
/**
 * 我的银行卡

 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_bankControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
        $this->model_bank = Model('waiter_bank');
    }
	
	
	
	
    /**
     * 银行卡列表
     */
    public function listBankOp() {
        $bank_list = $this->model_bank->getWaiterBankList(array('distributor_id'=>$this->waiter_info['distributor_id']));	
        output_data(array('bank_list' => $bank_list));        
    }

	
	
    /**
     * 新增银行卡
     */
    public function addBankOp() {
        
        $where = array(
			'bank_code' => $_POST['bank_code'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		
		$result = $this->model_bank->getWaiterBankInfo($where);
        if($result){
            output_error('此银行卡已存在');
        }
        
        
        
        $bank_info = $this->_bank_valid();
        $result = $this->model_bank->addWaiterBank($bank_info);
        if($result) {
            output_data('添加成功');
        } else {
            output_error('保存失败');
        }
    }
	
	
	public function infoBankOp(){
		
		$where = array(
			'bank_id' => $_POST['bank_id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		
		$result = $this->model_bank->getWaiterBankInfo($where);
		if($result){
			
			output_data($result);
		}else{
			output_error('获取失败');
			
		}
		
		
		
	}
	
	
	//编辑银行卡
	public function editBankOp(){
		
		$data = $this->_bank_valid();
		
		$where = array(
			'bank_id' => $_POST['bank_id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);		
		$result = $this->model_bank->editWaiterBank($data,$where);
        if($result) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
		
		
		
		
	}
	
	
	
	
	public function delBankOp(){
		$where = array(
			'bank_id' => $_POST['bank_id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		$result = $this->model_bank->delWaiterBank($where);
		if($result){
			  output_data('ok');			
		}else{
		  output_data('删除失败');	
		}
		
	}
	
	

    /**
     * 验证数据
     */
    private function _bank_valid() {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array("input"=>$_POST["bank_code"],"require"=>"true","message"=>'银行卡号不能为空'),
            array("input"=>$_POST["bank_name"],"require"=>"true","message"=>'持卡人姓名不能为空'),
            array("input"=>$_POST["bank_validity"],"require"=>"true","message"=>'有效期不能为空'),
           // array("input"=>$_POST["bank_country"],"require"=>"true","message"=>'所属国家不能为空'),
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }

        $data = array();
        $data['distributor_id'] = $this->waiter_info['distributor_id'];
        $data['bank_code'] = $_POST['bank_code'];
        $data['bank_name'] = $_POST['bank_name'];
        $data['bank_cvv'] = $_POST['bank_cvv'];
        $data['bank_validity'] = $_POST['bank_validity'];
      //  $data['bank_country'] = $_POST['bank_country'];
        return $data;
    }

}
