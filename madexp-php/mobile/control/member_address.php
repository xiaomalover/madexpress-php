<?php
/**
 * 我的地址

 */



defined('InMadExpress') or exit('Access Invalid!');

class member_addressControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
        $this->model_address = Model('address');
    }

    /**
     * 地址列表
     */
	
	//定位查询地址
	public function search_addressOp(){	
		
		
		$lbs = new lbs();
		
		$keyword = $_POST['keyword'];
		
		$row = $lbs->place_abroad($keyword);		
		output_data($row);
		
	}
	
	
	
	
    public function address_listOp() {
        
        $order_allocation = model('order_allocation');
	
		$list = model('address_attr')->getAddressAttrList(TRUE);
		$attr = array();
		foreach($list as $v){
			$attr[$v['address_icon']] = $v;
		}
		
		$address_list = $this->model_address->getAddressList(array('member_id'=>$this->member_info['member_id']));		
       	$data = array();
		foreach($address_list as $k=> $v){
			
			$data[$k] = $v;
			$data[$k]['address_nickname'] = $v['address_nickname'];
			$data[$k]['address_icon_black'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['address_icon']]['address_icon_url'].'black@2x.png';
			$data[$k]['address_icon_white'] = UPLOAD_SITE_URL.'/icon/'.$attr[$v['address_icon']]['address_icon_url'].'white@2x.png';
			
			if(!empty($_POST['coordinate'])){			
				
				$location = explode(',',$_POST['coordinate']);
				$address_location = explode(',',$v['coordinate']);				
				$distance = $order_allocation->getDistance($location[1],$location[0],$address_location[1],$address_location[0]);
				$data[$k]['distance'] = $distance;
				
			}
			
		}
		
		output_data(array('address_list' => $data));
    }
	
	
	
	
	public  function addressAttrOp(){
	    
		$list = model('address_attr')->getAddressAttrList(TRUE);
	//	print_r($list);
		$data = array();
	
		foreach($list as $k=> $v){
			$data[$k] = $v;			
			$data[$k]['address_icon_black'] =  UPLOAD_SITE_URL.'/icon/'.$v['address_icon_url'].'black@2x.png';			
			$data[$k]['address_icon_white'] =  UPLOAD_SITE_URL.'/icon/'.$v['address_icon_url'].'white@2x.png';	
		}
		output_data(array('list'=>$data));		
	}

    /**
     * 地址详细信息
     */
    public function address_infoOp() {
       
		$list = model('address_attr')->getAddressAttrList(TRUE);
		$attr = array();
		
		foreach($list as $v){			
			$attr[$v['address_icon']] = $v;
		}
		
		$address_id = intval($_POST['address_id']);
        $condition = array();
        $condition['address_id'] = $address_id;
        $condition['member_id'] = $this->member_info['member_id'];
        $address_info = $this->model_address->getAddressInfo($condition);
		
	
        if(!empty($address_id)) {
		//	$address_info['attr_name'] = $attr[$address_info['attr_id']]['attr_name'];
			
            output_data(array('address_info' => $address_info));
        } else {
            output_error('地址不存在');
        }
    }

    /**
     * 删除地址
     */
    public function address_delOp() {
        $address_id = intval($_POST['address_id']);
        $condition = array();
        $condition['address_id'] = $address_id;
        $condition['member_id'] = $this->member_info['member_id'];
        $row = $this->model_address->delAddress($condition);
		if($row){
        	output_data('ok');
		}else{			
			output_error('删除失败');
		}
    }

    /**
     * 新增地址
     */
    public function address_addOp() {
		
		
		/* $file  = 'add_log.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
		 $content = json_encode($_POST);
		 if($f  = file_put_contents($file, $content,FILE_APPEND)){// 这个函数支持版本(PHP 5) 
		 	
		 }*/
		
        $address_info = $this->_address_valid();		
		$where = array('member_id'=> $this->member_info['member_id']);		
		$count = $this->model_address->getAddressCount($where);		
		if($count > 0){
			$this->model_address->editAddress(array('is_default'=> 0),$where);
		}	
		
        $result = $this->model_address->addAddress($address_info);
        if($result) {
            output_data(array('address_id' => $result));
        } else {
            output_error('保存失败');
        }
    }

    /**
     * 编辑地址
     */
    public function addressSaveOp() {
      
	  	
	/*	 $file  = 'add_log.txt';//要写入文件的文件名（可以是任意文件名），如果文件不存在，将会创建一个
		 $content = json_encode($_POST);
		 if($f  = file_put_contents($file, $content,FILE_APPEND)){// 这个函数支持版本(PHP 5) 
		 	
		 }*/
		
	  
		$address_id = intval($_POST['address_id']);
		$address_info = $this->_address_valid();
		if($address_id > 0){
			$result = $this->model_address->editAddress($address_info, array('address_id'=>$address_id,'member_id'=>$this->member_info['member_id']));
			
		}else{
			$result = $this->model_address->addAddress($address_info);
		}
        if($result) {
            output_data('ok');
        } else {
            output_error('保存失败');
        }
    }

    /**
     * 验证地址数据
     */
    private function _address_valid() {
        $obj_validate = new Validate();
        $obj_validate->validateparam = array(       
            array("input"=>$_POST["address"],"require"=>"true","message"=>'地址不能为空'),
            array("input"=>$_POST["coordinate"],"require"=>"true","message"=>'坐标不能为空'),
            array("input"=>$_POST["address_nickname"],"require"=>"true","message"=>'地址昵称不能空'),
            array("input"=>$_POST["address_icon"],"require"=>"true","message"=>'地址ICON不能为空'),	
            array("input"=>$_POST["consignee_id"],"require"=>"true","message"=>'默认收货人必选'),	
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            output_error($error);
        }		

        $data = array();
        $data['member_id'] = $this->member_info['member_id']; 
        $data['address'] = $_POST['address'];
        $data['address_note'] = $_POST['address_note'];
        $data['address_icon'] = $_POST['address_icon'];   
		$data['address_nickname'] = $_POST['address_nickname'];   
		$data['address_ufn'] = $_POST['address_ufn'];   	
		$data['consignee_id'] = $_POST['consignee_id'];
		
		$region = $this->coordinate($_POST['coordinate']);
		
		$data['coordinate']   = $_POST['coordinate'];   
		if(!empty($region)){
			$data['region_id']    = $region['region_id'];
			$data['region_name']  = $region['region_name'];
			$data['region_color'] = $region['region_color'];		
			$data['region_parent_id'] = $region['region_parent_id'];		
		}else{
		    output_error('该地址不可配送');
		}
		
		$data['is_default'] = 1;   
		
        return $data;
    }
	
	
	
   
  	private function coordinate($points){
		 //145.003877,
		$bs = explode(',',$points);
		$point = array();
		$point['lng'] = $bs[0];
		$point['lat'] = $bs[1];			
		
		$region =  model('region')->getRegionList(array('region_type'=> 2));		
		$data = array();
		foreach($region as $k =>  $v){		 
		    $abc = $this->points_array($v['region_coordinate']);
		    $row = 	$this->is_point_in_polygon($point,$abc);			
			if($row === true){
				$data['region_id'] = $v['region_id'];
				$data['region_name'] = $v['region_name'];
				$data['region_color']  = $v['region_color'];
				$data['region_parent_id'] = $v['region_parent_id'];
				break;
			}			
		}
		
		return $data;
				
	   
  }
   
	
	private function points_array($points){
		
		$points_array = explode('|',$points);
		$data = array();
		foreach($points_array as $k => $v){
			$bs = explode(',',$v);			
			$data[$k]['lng'] = $bs[0];
			$data[$k]['lat'] = $bs[1];
		}
		return $data;
		
	}
	
	
	
	private function is_point_in_polygon($point, $pts) {
		$N = count($pts);
		$boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
		$intersectCount = 0;//cross points count of x 
		$precision = 2e-10; //浮点类型计算时候与0比较时候的容差
		$p1 = 0;//neighbour bound vertices
		$p2 = 0;
		$p = $point; //测试点
	 
		$p1 = $pts[0];//left vertex        
		for ($i = 1; $i <= $N; ++$i) {//check all rays
			// dump($p1);
			if ($p['lng'] == $p1['lng'] && $p['lat'] == $p1['lat']) {
				return $boundOrVertex;//p is an vertex
			}
			 
			$p2 = $pts[$i % $N];//right vertex            
			if ($p['lat'] < min($p1['lat'], $p2['lat']) || $p['lat'] > max($p1['lat'], $p2['lat'])) {//ray is outside of our interests
				$p1 = $p2; 
				continue;//next ray left point
			}
			 
			if ($p['lat'] > min($p1['lat'], $p2['lat']) && $p['lat'] < max($p1['lat'], $p2['lat'])) {//ray is crossing over by the algorithm (common part of)
				if($p['lng'] <= max($p1['lng'], $p2['lng'])){//x is before of ray
					if ($p1['lat'] == $p2['lat'] && $p['lng'] >= min($p1['lng'], $p2['lng'])) {//overlies on a horizontal ray
						return $boundOrVertex;
					}
					 
					if ($p1['lng'] == $p2['lng']) {//ray is vertical                        
						if ($p1['lng'] == $p['lng']) {//overlies on a vertical ray
							return $boundOrVertex;
						} else {//before ray
							++$intersectCount;
						}
					} else {//cross point on the left side
						$xinters = ($p['lat'] - $p1['lat']) * ($p2['lng'] - $p1['lng']) / ($p2['lat'] - $p1['lat']) + $p1['lng'];//cross point of lng
						if (abs($p['lng'] - $xinters) < $precision) {//overlies on a ray
							return $boundOrVertex;
						}
						 
						if ($p['lng'] < $xinters) {//before ray
							++$intersectCount;
						} 
					}
				}
			} else {//special case when ray is crossing through the vertex
				if ($p['lat'] == $p2['lat'] && $p['lng'] <= $p2['lng']) {//p crossing over p2
					$p3 = $pts[($i+1) % $N]; //next vertex
					if ($p['lat'] >= min($p1['lat'], $p3['lat']) && $p['lat'] <= max($p1['lat'], $p3['lat'])) { //p.lat lies between p1.lat & p3.lat
						++$intersectCount;
					} else {
						$intersectCount += 2;
					}
				}
			}
			$p1 = $p2;//next ray left point
		}
	 
		if ($intersectCount % 2 == 0) {//偶数在多边形外
			return false;
		} else { //奇数在多边形内
			return true;
		}
	}
	
	
	
	

}
