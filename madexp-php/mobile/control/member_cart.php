<?php
/**
 * 我的购物车
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_cartControl extends mobileMemberControl {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 购物车列表
     */
	
    public function cart_listOp() {		
        $model_cart = Model('cart');
        $condition = array(
			'buyer_id' => $this->member_info['member_id'],
			'store_id' => $_POST['store_id']
		);
        $cart_list  = $model_cart->listCart('db', $condition);     
        $store_cart_list = array();
        $sum = 0;
		
		$lang = $this->member_info['menu_language'];
		//print_r($this->member_info);
		
        foreach ($cart_list as $k=> $cart) {
            $cart_list[$k] = $cart;						
			$lang_goods = $this->goodsLang($cart,$lang);					
			$cart_list[$k]['goods_name'] = $lang_goods['goods_name'];		
			if($cart['set_meal'] == 0 ){		
				$cart_list[$k]['goods_spec'] = $lang_goods['goods_spec'];
				$cart_list[$k]['goods_size'] = $lang_goods['goods_size'];				
			}else{				
				$cart_list[$k]['goods_spec'] = $this->_getMealList($cart['set_meal_spec'],$lang);
			}
						
			$cart_list[$k]['goods_optional'] = $this->goodsOptional($cart['goods_optional'],$lang);
						
            $sum += $cart['goods_num'] * $cart['goods_price'];
        }        
        output_data(array('cart_list' => $cart_list, 'total_price' => ncPriceFormat($sum), 'cart_count' => count($cart_list)));
    }
	
	
	
	//获取套餐商品
	private function _getMealList($meal_ids,$lang){		
		$model = model('store_goods_meal');
		$where = array();		    
		$where['is_old'] = 0;
		$where['lang'] = $lang;
		$where['meal_id'] = array('in',$meal_ids);
		$list = $model->getMealList($where);
		$data = array();
		if(!empty($list)){
			foreach($list as $k => $v){			
				$specs_name[] = $v['meal_value'];			 
			}			
			
			$specs_name = implode('/',$specs_name);	
		}else{
			$specs_name = '';
		}
		return $specs_name;
		
	}
	
	
	
	//格式化当前数据根据现有值匹配出英文值	
	private function goodsLang($data,$lang){		
			
			 //根据现有数据获得规格的key
			$where = array(
				'goods_id' => $data['goods_id'],
				'lang_name' => $lang
			);		
			$lang_goods = model('goods_language')->getGoodsLangInfo($where);	
			$cart['goods_name'] = $lang_goods['goods_name'];
			//size			
			$size_row = 	model('store_goods_size')->getSizeInfo(array('size_id' => $data['goods_size_id'],'lang' => $lang,'is_old' => 0));
							
			$cart['goods_size'] = $size_row['size_value'];
			$specs_row =  model('store_goods_specs')->getSpecsList(array('specs_id'=>array('in',$data['goods_specs_id']),'lang' => $lang,'is_old' =>0));
			$specs_name = [];
			foreach($specs_row as $v){
				$specs_name[] = $v['specs_value'];
			}
			$specs_name = implode('/',$specs_name);			
			$cart['goods_spec'] = $specs_name;			
			$cart['goods_lang']	 = $lang;
			return $cart;
	}
	
	//查询语言参数
   private function goods_language($goods_id){		
		$where = array(
			'goods_id' =>$goods_id,
			'lang_id' => $this->member_info['menu_language']
		);
		$data = model('goods_language')->getGoodsLangInfo($where);
		if(!empty($data)){
			return $data;
		}else{
			return false;
		}
		
	}
	
	
	
	
	
	
	//格式化配件商品的语言包
	private function optionsLang($data,$storeId){
		
	    $langInfo = model('language')->getLangInfo(array('language_id'=> $this->member_info['menu_language']));
		$data = unserialize($data);
		foreach($data as $k => $v){
			$list[$v['lang_name']] = $v['options_name'];
		}	
		return $list[$langInfo['language_flag']];		
	}
	
	//获取当前值的KEY
	
	private function find_by_array_spec($array,$find)
	{
 	 foreach ($array as $key => $v)
    {
        if($v['value']==$find)
        {
            return $key;
        }
    }
	}
	
	private function find_by_array_size($array,$find)
	{
 	 foreach ($array as $key => $v)
    {
        if($v['name']==$find)
        {
            return $key;
        }
    }
	}

    /**
     * 购物车列表
     */
    public function cart_list_oldOp() {
        $model_cart = Model('cart');
    
        $condition = array('buyer_id' => $this->member_info['member_id']);
        $cart_list  = $model_cart->listCart('db', $condition);
    
        // 购物车列表 [得到最新商品属性及促销信息]
        $cart_list = logic('buy_1')->getGoodsCartList($cart_list, $jjgObj);
        $sum = 0;
        foreach ($cart_list as $key => $value) {
            $cart_list[$key]['goods_image_url'] = cthumb($value['goods_image'], $value['store_id']);
            $cart_list[$key]['goods_sum'] = ncPriceFormat($value['goods_price'] * $value['goods_num']);
            $sum += $cart_list[$key]['goods_sum'];
        }
    
        output_data(array('cart_list' => $cart_list, 'total_price' => ncPriceFormat($sum)));
    }

    /**
     * 购物车添加
     */
    public function cart_addOp() {
        
		if(!$this->member_info['is_buy']) output_error('您没有商品购买的权限,如有疑问请联系客服人员');
       
		$goods_id = $_POST['goods_code'];
        $quantity = intval($_POST['quantity']);		
        if(empty($goods_id)|| $quantity <= 0) {
            output_error('参数错误');
        }
		
		
        $model_goods = Model('goods');
        $model_cart = Model('cart');
		$model_size = model('store_goods_size');
		$model_specs = model('store_goods_specs');
		$model_options = model('store_goods_options');
	  
		$goods_info = $model_goods->getGoodsInfo(array('goods_code'=> $goods_id,'is_old' => 0));
		$checkSpec = $this->getLangGoods($goods_id,$_POST['goods_lang']);
		
	
	
	
	
	
	    if($_POST['goods_set_meal'] == 0){
	
		
    		//新规格
    		if(!empty($_POST['goods_size'])){			
    			$size_row = $model_size->getSizeInfo(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
    			if(empty($size_row)){				
    				output_error('尺寸不存在');
    			}
    			if($size_row['size_sale_price'] > 0){			
    				$goods_size_price = $size_row['size_sale_price'];
    				$goods_original_price = $size_row['size_price'];				
    			}else{
    				
    				$goods_size_price = $size_row['size_price'];
    				$goods_original_price = 0;			
    			}
    		
    			
    			$size_name = $size_row['size_value'];
    			$box_id = $size_row['size_box_id'];
    		}else{			
    			output_error('尺寸不存在');			
    		}
						
						
						
    		//新规格 ,入参可能为多个specsid
    		$spec_price = 0;
    		$spec_name = array();
    		if(!empty($_POST['goods_spec'])){			
    			$specs_row = $model_specs->getSpecsList(array('specs_id' => array('in',$_POST['goods_spec']),'lang' =>$_POST['goods_lang'],'is_old' => 0 ));			
    			foreach($specs_row as $v){
    				$spec_price += $v['specs_price'];
    				$spec_name[] = $v['specs_value'];								
    			}			
    			$spec_name = implode('/',$spec_name);
    			
    		}	
	
	
	
    		//新配件
    		$optional_price = 0;
    		if(!empty($_POST['goods_optional'])){
    			$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
    			foreach($options_row as $v){				
    				$optional_price += $v['options_price'];
    			}
    		}
    		
        
        	$goods_price =  $goods_size_price + $optional_price + $spec_price ; //折扣价
	    	$goods_original_price = $goods_original_price + $optional_price + $spec_price ;	 //原价
        
        
	    }else{
	        
	        
    	   //新配件
    		$optional_price = 0;
    		if(!empty($_POST['goods_optional'])){
    			$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
    			foreach($options_row as $v){				
    				$optional_price += $v['options_price'];
    			}
    		}
    	    $goods_price = 0;	
    	     
    		$model = model('store_goods_meal');		
    		$where = array();
    
    		$where['is_old'] = 0;
    		$where['lang'] = $_POST['goods_lang'];
    		$where['meal_id'] = array('in',$_POST['goods_set_meal_spec']);
    		$list = $model->getMealList($where);
    		$data = array();
    		foreach($list as $k => $v){
    	    	 $goods_price +=$v['meal_price'];
    		}				
	
    		
	      
	        $goods_size_price = 0;
	        $spec_price = 0;
	        $goods_original_price = 0;
	        
	        
	    }





	
	
	
	 //  print_r($goods_price.'|');	
//	   print_r($goods_original_price);
    
		
        //验证是否可以购买
        if(empty($goods_info)) {
            output_error('商品已下架或不存在');
        }
       
        if(intval($goods_info['goods_stock']) < 1 || intval($goods_info['goods_stock']) < $quantity) {
            output_error('库存不足');
        }

      	if(!empty($goods_info['goods_image'])){
			$data = explode(",",$goods_info['goods_image']);
			$image = $data[0];
		}else{
			$image = '';	
		}	  
	  	
        $param = array();
        $param['buyer_id']  = $this->member_info['member_id'];
        $param['store_id']  = $goods_info['store_id'];
        $param['goods_id']  = $goods_info['goods_id'];
        $param['goods_name'] = $checkSpec['goods_name'];
        $param['goods_price'] 		= ncPriceFormat($goods_price);
        $param['goods_image'] 		= $image;
        $param['store_name'] 		= $goods_info['store_name'];
		$param['goods_spec'] = 		$spec_name;
		$param['goods_spec_price'] 	= ncPriceFormat($spec_price);
		$param['goods_size'] 		= $size_name;
		$param['goods_size_price'] 	= ncPriceFormat($goods_size_price);		
		$param['goods_optional'] 	= $_POST['goods_optional'];			
		$param['goods_optional_price'] = ncPriceFormat($optional_price);	
		$param['goods_comment'] 	= $_POST['goods_comment'];		
		$param['goods_lang'] 		= $_POST['goods_lang'];
		$param['goods_class_id'] 	= $goods_info['gc_id'];		
		$param['goods_original_price'] = $goods_original_price;		
		$param['goods_specs_id'] 		= $_POST['goods_spec'];
		$param['goods_size_id'] 		= $_POST['goods_size'];
		$param['goods_code'] 		= $goods_info['goods_code'];
		$param['goods_optional_ids'] = $_POST['goods_optional'];
		

		$param['set_meal'] = 0;
		if($_POST['goods_set_meal'] == 1){
		 
		    $param['set_meal'] = 1;
		    $param['set_meal_spec'] = $_POST['goods_set_meal_spec'];		    
	
		}
		
		//检查包装
		if($box_id > 0){
			$where  = array(
				'goods_id' => $box_id,
				'store_id' => $goods_info['store_id']
			);
				
			$box_info = model('store_foodbox_goods')->getStoreFoodboxGoodsInfo($where);	
		//	print_r($box_info);
			
			$param['foodbox_price']	 	= $box_info['goods_price'];
			$param['foodbox_id'] 		= $box_info['goods_id'];
			$param['foodbox_name'] 		= $box_info['goods_name'];
			$param['foodbox_num'] 		= 1;			
		}
		
		
	//	print_r($param);
		
		
	 
	
		$result = $model_cart->addCart($param, 'db', $quantity);
        if($result) {
            output_data('添加成功');
        } else {
            output_error('添加失败');
        }
    }
	
	private function goodsOptional($ids,$lang ='CHN-S'){
		   
		   $where = array();
		   $where['options_id'] = array('in',$ids);
		   $where['is_old'] = 0;		   
		   $options_list = model('store_goods_options')->getOptionsList($where);
		   $data = array();	   
		   $size_model = model('store_goods_size');
		   $specs_model = model('store_goods_specs');
		   $goods_lang_model = model('goods_language');
		   foreach($options_list as $k =>$v){			  
			   $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang,'is_old' => 0));
			   $data[$k]['size'] = $size['size_value'];		   
			   $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
			   $specs_name = array();
			   foreach($specs as $vs){
				   $specs_name[] = $vs['specs_value'];
			   }
			   $specs_name = implode(',',$specs_name);			   
			   
			   $data[$k]['specs'] = $specs_name;		   
			   $data[$k]['price'] = $v['options_price'];		   
			   $goods_name = $goods_lang_model->getGoodsLangInfo(array('goods_code' => $v['options_goods_code'],'lang_name' => $lang,'is_old' => 0 ));		   
			   $data[$k]['name'] = $goods_name['goods_name'];
		//	   $data[$k]['goods_code'] = $v['options_goods_code'];
			   $data[$k]['id'] = $v['options_id'];
		   }
		   
		   return $data;
		   
		   
	}
	

	//获取语言包属性
	private function getLangGoods($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_code' => $goods_id,
			'lang_name' => $lang_name,
			'is_old' => 0
			
		);		
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);		
		return $goods_lang;		
	}
	



    /**
     * 购物车删除
     */
    public function cart_delOp() {
        $cart_id = intval($_POST['cart_id']);

        $model_cart = Model('cart');

        if($cart_id > 0) {
            $condition = array();
            $condition['buyer_id'] = $this->member_info['member_id'];
            $condition['cart_id'] = $cart_id;
		

            $model_cart->delCart('db', $condition);
        }

        output_data('1');
    }
	
	
	public function cartEmptyOp(){
		
	    $store_id = intval($_POST['store_id']);
        $model_cart = Model('cart');
        if($store_id > 0) {
            $condition = array();
			$condition['store_id'] = $store_id;
            $condition['buyer_id'] = $this->member_info['member_id'];
            $model_cart->delCart('db', $condition);
        }

        output_data('1');
		
		
		
	}
	
	

    /**
     * 更新购物车购买数量
     */
    public function cart_edit_quantityOp() {
        $cart_id = intval(abs($_POST['cart_id']));
        $quantity = intval(abs($_POST['quantity']));
		$store_id = intval(abs($_POST['store_id']));
		$goods_comment = $_POST['goods_comment'];
		
         if(empty($cart_id) || empty($quantity) || empty($store_id)) {
            output_error('参数错误');
        }

        $model_cart = Model('cart');

        $cart_info = $model_cart->getCartInfo(array('cart_id'=>$cart_id, 'store_id'=> $store_id, 'buyer_id' => $this->member_info['member_id']));

        //检查是否为本人购物车
        if($cart_info['buyer_id'] != $this->member_info['member_id']) {
            output_error('参数错误');
        }

      
        $data = array();
        $data['goods_num'] = $quantity;
		if($goods_comment != ''){
			$data['goods_comment'] = $goods_comment;
		}
        $update = $model_cart->editCart($data, array('cart_id'=>$cart_id));
        if ($update) {
			
			
			
            $return = array();
            $return['quantity'] = $quantity;
            $return['goods_price'] = ncPriceFormat($cart_info['goods_price']);
            $return['total_price'] = ncPriceFormat($cart_info['goods_price'] * $quantity);
            output_data($return);
        } else {
            output_error('修改失败');
        }
    }

    /**
     * 检查库存是否充足
     */
    private function _check_goods_storage(& $cart_info, $quantity, $member_id) {
        $model_goods= Model('goods');
        $model_bl = Model('p_bundling');
        $logic_buy_1 = Logic('buy_1');

        if ($cart_info['bl_id'] == '0') {
            //普通商品
            $goods_info = $model_goods->getGoodsOnlineInfoAndPromotionById($cart_info['goods_id']);

            //手机专享
            $logic_buy_1->getMbSoleInfo($goods_info);
            
            //抢购
            $logic_buy_1->getGroupbuyInfo($goods_info);
            if ($goods_info['ifgroupbuy']) {
                if ($goods_info['upper_limit'] && $quantity > $goods_info['upper_limit']) {
                    return false;
                }
            }

            //限时折扣
            $logic_buy_1->getXianshiInfo($goods_info,$quantity);

            if(intval($goods_info['goods_storage']) < $quantity) {
                return false;
            }
            $goods_info['cart_id'] = $cart_info['cart_id'];
            $cart_info = $goods_info;
        } else {
            //优惠套装商品
            $bl_goods_list = $model_bl->getBundlingGoodsList(array('bl_id' => $cart_info['bl_id']));
            $goods_id_array = array();
            foreach ($bl_goods_list as $goods) {
                $goods_id_array[] = $goods['goods_id'];
            }
            $bl_goods_list = $model_goods->getGoodsOnlineListAndPromotionByIdArray($goods_id_array);

            //如果有商品库存不足，更新购买数量到目前最大库存
            foreach ($bl_goods_list as $goods_info) {
                if (intval($goods_info['goods_storage']) < $quantity) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public function cart_infoOp(){
        
        $where = array();
        $where['buyer_id']  = $this->member_info['member_id'];
        $where['store_id']  = $_POST['store_id'];
        $where['goods_id']  = $_POST['goods_id'];
        if(!empty($_POST['goods_comment'])){
           $where['goods_comment'] = $_POST['goods_comment'];
        }
        $where['set_meal']      = $_POST['set_meal'];
        
        if($_POST['set_meal'] == 0){
            
           if(!empty($_POST['goods_spec'])){  
            $where['goods_specs_id'] = $_POST['goods_spec'];
           } 
            $where['goods_size_id'] = $_POST['goods_size'];
        }else{
            $where['set_meal_spec'] = $_POST['set_meal_spec'];
        }
        
        $where['goods_lang']    = $_POST['goods_lang'];
      
        if(!empty($_POST['goods_optional'])){  
        $where['goods_optional'] = $_POST['goods_optional'];
        } 
        
        
     //   print_r($where);
        $result = model('cart')->getCartInfo($where);
        
        output_data(array('cart_info' => $result));
        
        
    }
    
    /**
     * 查询购物车商品数量
     */
    function cart_countOp() {
        $param['cart_count'] = Model('cart')->countCartByMemberId($this->member_info['member_id']);
        output_data($param);
    }

    /**
     * 批量添加购物车
     * cartlist 格式为goods_id1,num1|goods_id2,num2
     */
    public function cart_batchaddOp(){
        $param = $_POST;
        $cartlist_str = trim($param['cartlist']);
        $cartlist_arr = $cartlist_str?explode('|',$cartlist_str):array();
        if(!$cartlist_arr) {
            output_error('参数错误');
        }

        $cartlist_new =  array();
        foreach($cartlist_arr as $k=>$v){
            $tmp = $v?explode(',',$v):array();
            if (!$tmp) {
                continue;
            }
            $cartlist_new[$tmp[0]]['goods_num'] = $tmp[1];
        }
        Model('cart')->batchAddCart($cartlist_new, $this->member_info['member_id'], $this->member_info['store_id']);
        output_data('1');
    }
}
