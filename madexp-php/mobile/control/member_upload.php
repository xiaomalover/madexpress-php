<?php
/**
 * 客户端上传
 */



defined('InMadExpress') or exit('Access Invalid!');

class member_uploadControl extends mobileMemberControl {

    public function __construct(){
        parent::__construct();
    }
    
	
	public function upload_fileOp(){
		
		
			$file_type = $_POST['type'];
			
			if($file_type == ''){
				output_error('请传类型参数');
			}
			
		
			$evaluate_pic = array();
	        $evaluate_pic[1] = 'file1';
	        $evaluate_pic[2] = 'file2';
	        $evaluate_pic[3] = 'file3';
	    	$evaluate_pic[4] = 'file4';
	    	$evaluate_pic[5] = 'file5';
	    	$evaluate_pic[6] = 'file6';
	    	$evaluate_pic[7] = 'file7';
	    	$evaluate_pic[8] = 'file8';
	    	$evaluate_pic[9] = 'file9';
	      
	        $pic_name = array();
	        $upload = new UploadFile();
			
			if($file_type == 'evaluate'){
			    
				$uploaddir = ATTACH_EVALUATE;
				
			}elseif($file_type == 'feedback'){
			    
				$uploaddir = ATTACH_FEEDBACK;
				
			}elseif($file_type == 'complain'){
			    
				$uploaddir = 'complain';
			}elseif($file_type == 'avatar'){
			    
			    $uploaddir = ATTACH_AVATAR;
			    
			}elseif($file_type == 'refund'){
			    
			    $uploaddir = ATTACH_REFUND;
			    
			}
			
	        $upload->set('default_dir',$uploaddir);
	        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
	     
	     	
	     	if($file_type == 'avatar'){
	     	        
	     	        $result = $upload->upfile('file1');
	     	       
	     	      	if ($result){		 
    	     	        $where = array(
    						'member_id' => $this->member_info['member_id']
    					);					
    					$data = array(
    						'member_avatar' => $upload->file_name
    					);					
    					$row = model('member')->editMember($where,$data);
    	     	    
    	     	    	if($row){
    						output_data(array('pic_name'=>$upload->file_name,'url'=> UPLOAD_SITE_URL.'/'.$uploaddir.'/'));
    					}else{
    						output_error('上传失败');						
    					}
	     	      	}else{
	     	      	    
	     	      	    output_error('上传失败');
	     	      	}
	     	    
	     	    
	     	    
	     	    
	     	}else{
    	        $count = 1;
    	        foreach($evaluate_pic as $pic) {
    	            if (!empty($_FILES[$pic]['name'])){
    	                $result = $upload->upfile($pic);
    	                if ($result){
    	                    $pic_name[] = $upload->file_name;
    	                    $upload->file_name = '';
    	                } else {
    	                    $pic_name[] = '';
    	                }
    	            }
    	            $count++;
    	        }
    	        
    	        
    	        output_data(array('pic_name'=>$pic_name,'url'=> UPLOAD_SITE_URL.'/'.$uploaddir.'/'));
	        
	        
	     	}
	        
	        
	        
	      
			
		
				
	}
	
	


    
 
}
