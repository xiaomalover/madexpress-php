<?php
/**
 * 订单商品替换
 */

defined('InMadExpress') or exit('Access Invalid!');
class seller_order_replaceControl extends mobileSellerControl {

    public function __construct() {
        parent::__construct();      
    }


	//替换列表
	public function replace_listOp(){
		
		$order_id =  intval($_POST['order_id']);		
		
		$where = array();
		$where['order_id']  = $order_id;
		$where['store_id'] = $this->store_info['store_id'];			
		$where['rec_id'] = intval($_POST['rec_id']);
		$lang = $_POST['lang'] ?  $_POST['lang']  : 'CHN-S';
		$replace_list = model('order_goods_replace')->getGoodsList($where);
		foreach ($replace_list as $k=> $replace) {
		    $replace_list[$k] = $replace;						
		
			$lang_data = $this->goodsLang($replace,$lang);		
		
		
		
			if(!empty($lang_data)){
			    
				  $replace_list[$k]['goods_name'] = $lang_data['goods_name'];				
				  $replace_list[$k]['goods_spec'] = $lang_data['goods_spec'];				
				  $replace_list[$k]['goods_size'] = $lang_data['goods_size'];	
				  $replace_list[$k]['goods_lang'] = $lang_data['goods_lang'];				  				
				  
			}		
			
			$replace_list[$k]['goods_optional'] = $this->goodsOptional($replace['goods_optional']);			
		
		}
		
		
		output_data(array('replace_list' => $replace_list,  'replace_count' => count($replace_list)));
		
	}
		
	
	
	//格式化当前数据根据现有值匹配出英文值	
	private function goodsLang($data,$lang){		
			
			 //根据现有数据获得规格的key
			$where = array(
				'goods_id' => $data['goods_id'],
				'lang_name' => $lang
			);		
			$lang_goods = model('goods_language')->getGoodsLangInfo($where);	
			$cart['goods_name'] = $lang_goods['goods_name'];
			//size			
			$size_row = 	model('store_goods_size')->getSizeInfo(array('size_id' => $data['goods_size_id'],'lang' => $lang,'is_old' => 0));
							
			$cart['goods_size'] = $size_row['size_value'];
			$specs_row =  model('store_goods_specs')->getSpecsList(array('specs_id'=>array('in',$data['goods_specs_id']),'lang' => $lang,'is_old' =>0));
			$specs_name = [];
			foreach($specs_row as $v){
				$specs_name[] = $v['specs_value'];
			}
			$specs_name = implode('/',$specs_name);			
			$cart['goods_spec'] = $specs_name;			
			$cart['goods_lang']	 = $lang;
			return $cart;
	}
	
	
	
		
		//查询语言参数
	private function goods_language($goods_id){		
			$where = array(
				'goods_id' =>$goods_id,
				'lang_id' => $this->store_info['store_default_lang']
			);
			$data = model('goods_language')->getGoodsLangInfo($where);
			if(!empty($data)){
				return $data;
			}else{
				return false;
			}
			
	}
		
		
		
		
		
		
	private function goodsOptional($ids){			
			$where = array();
			$where['options_id'] = array('in',$ids);
			$where['state'] = 0 ;
			$where['store_id'] = $this->store_info['store_id'];	
			$list = model('goods_options')->getOptionsList($where);		
			$optional = array();
			foreach($list as  $k=>$v){
				$optional[$k]['id'] = $v['options_id'];
				$optional[$k]['name'] = $this->optionsLang($v['options_lang'],$store_id);
				$optional[$k]['price'] = $v['options_price'];
			}
			return $optional;
	}
		
		
		//格式化配件商品的语言包
		private function optionsLang($data,$storeId){			
		    $langInfo = model('language')->getLangInfo(array('language_id'=>$this->store_info['store_default_lang']));
			$data = unserialize($data);
			foreach($data as $k => $v){
				$list[$v['lang_name']] = $v['options_name'];
			}	
			return $list[$langInfo['language_flag']];		
		}
	
	
		//获取当前值的KEY	
		private function find_by_array_spec($array,$find)
		{
			 foreach ($array as $key => $v)
			{
				if($v['value']==$find)
				{
					return $key;
				}
			}
		}
		
		
		private function find_by_array_size($array,$find)
		{
			foreach ($array as $key => $v)
			{
				if($v['name']==$find)
				{
					return $key;
				}
			}
		}
	
	
	
	
	
	
		//商品信息
	public function order_goods_infoOp(){
			
			$where = array(
				'rec_id' => $_POST['rec_id'],
				'order_id' => $_POST['order_id'],
				'store_id' => $this->store_info['store_id']
			);		
			$rec_info = model('order')->getOrderGoodsInfo($where);	
			
			$rec_info['goods_optional'] = null;
			if($rec_info['goods_optional']){
				$rec_info['goods_optional'] = unserialize($rec_info['goods_optional']);
			}
			$rec_info['goods_spec'] = null;
			if($rec_info['goods_spec']){
			    $rec_info['goods_spec'] = $rec_info['goods_spec'];
			}
			
			
			
			
			output_data(array('goods_info'=>$rec_info));
	}

	
	//记录替换商品
	public function save_replaceOp(){
		
		
		$goods_code = $_POST['goods_code'];
		$quantity = intval($_POST['quantity']);		
		
		if(!$goods_code  || $quantity <= 0) {
		    output_error('参数错误');
		}		
	
		$model_goods = Model('goods');
		$model_size = model('store_goods_size');
		$model_specs = model('store_goods_specs');
		$model_options = model('store_goods_options');
		
		$goods_info = $model_goods->getGoodsInfo(array('goods_code'=> $goods_code,'is_old' => 0));
		$checkSpec = $this->getLangGoods($goods_code,$_POST['goods_lang']);
	
		
		
		//新规格
		if(!empty($_POST['goods_size'])){			
			$size_row = $model_size->getSizeInfo(array('size_id' => $_POST['goods_size'],'lang' =>$_POST['goods_lang'],'is_old' => 0 ));
			if(empty($size_row)){				
				output_error('尺寸不存在');
			}
			if($size_row['size_sale_price'] > 0){			
				$goods_size_price = $size_row['size_sale_price'];
				$goods_original_price = $size_row['size_price'];				
			}else{
				
				$goods_size_price = $size_row['size_price'];
				$goods_original_price = 0;			
			}
					
			$size_name = $size_row['size_value'];
			$box_id = $size_row['size_box_id'];
		}else{			
			output_error('尺寸不存在');			
		}
						
						
						
		//新规格 ,入参可能为多个specsid
		$spec_price = 0;
		$spec_name = array();
		if(!empty($_POST['goods_spec'])){			
			$specs_row = $model_specs->getSpecsList(array('specs_id' => array('in',$_POST['goods_spec']),'lang' =>$_POST['goods_lang'],'is_old' => 0 ));			
			foreach($specs_row as $v){
				$spec_price += $v['specs_price'];
				$spec_name[] = $v['specs_value'];								
			}			
			$spec_name = implode('/',$spec_name);			
		}	
			
			
			
			
		//新配件
		$optional_price = 0;
		if(!empty($_POST['goods_optional'])){
			$options_row = $model_options->getOptionsList(array('options_id' => array('in',$_POST['goods_optional'],'is_old' => 0)));					
			foreach($options_row as $v){				
				$optional_price += $v['options_price'];
			}
		}
		
		
		$goods_price =  $goods_size_price + $optional_price + $spec_price ; //折扣价
		$goods_original_price = $goods_original_price + $optional_price + $spec_price ;	 //原价
					
	
		
		//验证是否可以购买
		if(empty($goods_info)) {
		    output_error('商品已下架或不存在');
		}
		   
			   
		if(intval($goods_info['goods_stock']) < 1 || intval($goods_info['goods_stock']) < $quantity) {
		    output_error('库存不足');
		}
		
		
		if(!empty($goods_info['goods_image'])){
			$data = explode(",",$goods_info['goods_image']);
			$image = $data[0];
		}else{
			$image = '';	
		}
		
	
	  //  print_r($checkSpec['goods_lang']);
	  
	
		$param = array();		
		$param['store_id']      = $goods_info['store_id'];
		$param['goods_id']      = $goods_info['goods_id'];
		$param['goods_name']    = $checkSpec['goods_name'];
		$param['goods_price']   = $goods_price;
		$param['goods_image']   = $image;
		//$param['store_name']  = $goods_info['store_name'];
	
		$param['goods_spec'] = 		$spec_name;
		$param['goods_spec_price'] 	= ncPriceFormat($spec_price);
		$param['goods_size'] 		= $size_name;
		$param['goods_size_price'] 	= ncPriceFormat($goods_size_price);		
		$param['goods_optional'] 	= $goods_optional;			
		$param['goods_optional_price'] = ncPriceFormat($optional_price);	
		
		
		//$param['goods_comment'] = $_POST['goods_comment'];		
		$param['goods_lang']     = $_POST['goods_lang'];
		$param['goods_class_id'] = $goods_info['gc_id'];		
		$param['order_id']       = $_POST['order_id'];						
		$param['rec_id']         = $_POST['rec_id'];		
		$param['goods_num']      = $quantity;
		
		$param['goods_size_id']      = $_POST['goods_size'];
		$param['goods_specs_id']     = $_POST['goods_spec'];
		$param['goods_code']         = $_POST['goods_code'];
		$param['goods_optional_ids'] = $_POST['goods_optional'];

		
		
	    //print_r($param);
		
			
		$condition = array();		
		$condition['goods_id']      = $goods_info['goods_id'];
		$condition['rec_id']        = $_POST['rec_id'];					
		$condition['goods_size_id']    = $_POST['goods_size'];  	
		
		if(!empty($param['goods_spec'])){
			$condition['goods_specs_id'] = $param['goods_spec'];  	
		}
		
		if(!empty($param['goods_optional'])){
			$condition['goods_optional_ids'] = $param['goods_optional'];  	
		}
		
		//print_r($condition);			
					
		$check_cart = $this->check_cart($condition);		
		//print_r($check_cart);
		
		if($check_cart){			
		    
			$update = array();
			$update['goods_num'] = $check_cart['goods_num'] + $quantity;	
			$where = array();
			$where['replace_id'] =  $check_cart['replace_id'];
			$row = model('order_goods_replace')->editGoods($where,$update);
			
		}else{	
		    
			$row = model('order_goods_replace')->addGoods($param);
			
		}
		
		
		if($row){				    
		    
			output_data('ok');			
		}else{
			output_error('保存失败');
		}
	}
	
	//获取语言包属性
	private function getLangGoods($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_code' => $goods_id,
			'lang_name' => $lang_name,
			'is_old' => 0
			
		);		
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);		
		return $goods_lang;		
	}
	
		//更新主订单商品的可用商品数数量
	private function editGoodsUsable($data,$type){
		
		//更新主订单			
		$where = array(
			'rec_id' => $data['rec_id'],
			'order_id' => $data['order_id']
		);
		
		$info = model('order')->getOrderGoodsInfo($where);
		
		$data = array();
		
		if($type == 'plus'){
		    
		    $data['goods_usable_num'] =  $info['goods_usable_num'] + 1;
		    
		}else{
		    
		    $data['goods_usable_num'] =  $info['goods_usable_num'] - 1;
		    
		}
		
		model('order')->editOrderGoods($data,$where);	
		
	}
	
	
	
	//获取
	private function check_cart($where){
		
		//	print_r($where);
			$info  = model('order_goods_replace')->getGoodsInfo($where);		
		
		//	print_r($info);
			return $info;
	}


	
	
	/**
	 * 更新数量
	 */
	public function order_replace_numOp() {
	    $replace_id = intval(abs($_POST['replace_id']));
	    $quantity = intval(abs($_POST['quantity']));
		$store_id = $this->store_info['store_id'];
		
	     if(empty($replace_id) ||  empty($store_id)) {
	        output_error('参数错误');
	    }
	
	    $model_replace = Model('order_goods_replace');	
	    $replace_info = $model_replace->getGoodsInfo(array('replace_id'=>$replace_id, 'store_id'=> $store_id));
		   
	    $data = array();
	    $data['goods_num'] = $quantity;		
	    $update = $model_replace->editGoods(array('replace_id'=>$replace_id),$data);
	    if ($update) {
	        output_data('修改成功');
	    } else {
	        output_error('修改失败');
	    }
	}	
	
	//编辑商品	
	private function editReplace($data,$where){
		
				
		
	}
	
	
	//校验传过来的属性和规格是否正确	
	private function checkSpec($goods_id,$lang_name){
		
		$data = array();
		$where = array(
			'goods_id' => $goods_id,
			'lang_name' => $lang_name
		);
		$goods_lang = model('goods_language')->getGoodsLangInfo($where);
		
		$goods_sizespec = unserialize($goods_lang['goods_sizeprice']);
		
		$new_sizespec = array();
		foreach($goods_sizespec as $v){			
			$new_sizespec[$v['name']] = $v['price'];
		}
		
		
		$new_specs = array();
		$goods_specs = unserialize($goods_lang['goods_specs']);
		foreach($goods_specs as $v){			
			$new_specs[$v['value']] = $v['price'];			
		}
		
		
		
		$data['goods_sizespec'] = $new_sizespec;
		$data['goods_specs'] = $new_specs;
		$data['goods_lang'] = $goods_lang;
		
		return $data;
			
		
	}
	
	
	
	//回退商品和全部删除
	public function del_replaceOp(){
		$where= array();
		$where['order_id'] = $_POST['order_id'];
		if($_POST['rec_id'] > 0){
		    	$where['rec_id'] = $_POST['rec_id'];
		}
		$where['store_id'] = $this->store_info['store_id'];
		$row = model('order_goods_replace')->delGoods($where);
		
		$model_order = model('order');
		
		if($row){
		    
		     //更新主商品数量
		    $goods_list = $model_order->getOrderGoodsList($where);
		    foreach($goods_list as $v){
		            $model_order->editOrderGoods(array('goods_usable_num' => $v['goods_num']),array('rec_id'=>$v['rec_id']));
		    }
		    
			output_data('操作成功');
		}else{
			output_error('操作失败');
		}
				
		
		
	}
	
	
	//保存	
	public function save_order_replaceOp(){	
				
			$rec_id = intval($_POST['rec_id']);
			$order_id = intval($_POST['order_id']);
			$rec_num = intval($_POST['rec_num']);
			$where  =array(
				'rec_id' => $rec_id,
				'order_id' => $order_id,
				'store_id' => $this->store_info['store_id']
			);
			$update = array(
				'rec_num' => $rec_num
			);
			$row = model('order_goods_replace')->editGoods($where,$update);	
			
			if($row){
			    
			    
			    $where = array(
			        'rec_id' => $rec_id
			    );
			    
			    $update = array(
			        'goods_usable_num' => array('exp','goods_usable_num -'.$rec_num),
			        'replace_num' => array('exp','replace_num +'.$rec_num) 
			    );
			    
			    $model_order = model('order')->editOrderGoods($update,$where);
			    
			    
				output_data('保存成功');				
			}else{
				output_error('保存失败');				
			}
		
	}
	
	
	

}
