<?php
/**
 * 商家退款

 */
defined('InMadExpress') or exit('Access Invalid!');

class seller_refundControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }
    
	
	
	/*
	退款商品列表
	*/
	
	public function order_infoOp(){
		
		$order_id = intval($_POST['order_id']);
		if (!$order_id) {
		    output_error('订单编号有误');
		}
		$model_order = Model('order');
		$condition = array();
		$condition['order_id'] = $order_id;
		$condition['store_id'] = $this->store_info['store_id'];
		
		$order = $model_order->getOrderInfo($condition, array('order_common','order_goods'));
		
		
		if (empty($order)) {
		    output_error('订单信息不存在');
		}
		
		$order_item = array();		
		//从新格式化				
		
		$order_user = array();
		$order_user['buyer_code'] = $order['buyer_code'];
		$order_user['buyer_name'] = $order['buyer_name'];
		$order_user['buyer_phone'] = $order['buyer_phone'];
		$order_user['buyer_comment'] = $order['buyer_comment'];
		$order_user['buyer_is_new'] = $order['buyer_is_new'];
		$order_user['distributor_name'] = $order['distributor_name'];
		$order_user['distributor_mobile'] = $order['distributor_mobile'];
		$order_user['state'] = $order['order_state'];
		$order_user['evaluation_state'] = $order['evaluation_state'];
		
		if($order['evaluation_state'] == 1) {			
			$model = model('evaluate_store');		
			$where = array(			
				'store_evaluate_orderid' => $order['order_id']
			);		
			$eva = $model->getEvaluateStoreInfo($where);	
			$order['evaluation_text'] = $eva['store_evaluate_score_text'];
			$order['evaluation_content'] = $eva['store_evaluate_score_content'];	
		}
		
					
		$order_item['order_user'] = $order_user;
				
				
				
		//订单商品,原始商品，不做任何修改
		$order_goods = array();		
		$goods =  $order['extend_order_goods'];		
		$goods_num = 0;
		foreach($goods as $k => $v){			
			$goods_num += $v['goods_num'];	
		}
		
		$order_goods['goods_list'] = $order['extend_order_goods'];				
		$order_goods['goods_num'] = $goods_num;		
		$order_item['order_goods'] = $order_goods;
		
		
		//金额信息
		$order_amount = array();		
		//商品小计
		$order_amount['goods_amount'] = $order['goods_amount'];		
		//商品折扣
		$order_amount['sales_amount'] = $order['sales_amount'];		
		//餐盒费
		$order_amount['foodbox_amount'] = $order['foodbox_amount'];
		//配送费
		$order_amount['delivery_amount'] = $order['delivery_fee'];
		//支付手续费	
		
		$order_amount['pay_commission_amount'] = $order['pay_commission_amount'];		
		
		$order_item['order_amount'] = $order_amount;
		
		
		output_data($order_item);		
		
	}
	
	
	//保存退款信息
	public  function  save_refundOp(){
		
		$model = model('order');
		$order_id 	= $_POST['order_id'];
		$refund_type = $_POST['refund_state'];
		$coupon_id = $_POST['coupon_id'];		
		$order_info = $model->getOrderInfoN(array('order_id'=>$order_id,'store_id'=>$this->store_info['store_id']));
		if(empty($order_info)){			
			output_error('订单参数错误');
		}		
		if($order_info['refund_state'] == 2){
			output_error('此订单已退款');
		}

        //判断当前退款属于1,2,3阶段。
        //商家接单前直接取消
        //商家接单后要生成票据，部分退款refund code 
        //订单送达后，就是退款票据
        $refund_stage = 0;
        if($order_info['order_state'] == 40){
            $refund_stage = 1;
        }
        if($order_info['order_state'] == 50 || $order_info['order_state'] == 60){
            $refund_stage = 2;
        }
        

		$refund_amount = 0;
		        
        //部分退款
		if($refund_type == 1){			
		        
                
                $goods_ids = explode('|',$_POST['goods_ids']);			
                $refund_amount = 0;
                foreach($goods_ids as $v){	

                    $goods = explode(',',$v);	
                    $rec_info = model('order')->getOrderGoodsInfo(['rec_id' => $goods[0]]);
                    $rec_info['goods_num'] = $goods[1];
                    $refund_goods[] = $rec_info;
                    $refund_amount += $rec_info['goods_price'];

                }
                
                $data = array();       
                $data['order_id']    = $order_info['order_id'];
                $data['order_sn']    = $order_info['order_sn'];
                $data['store_id']    = $order_info['store_id'];
                $data['store_name']  = $order_info['store_name'];
                $data['member_id']   = $order_info['buyer_id'];        
                $data['role']       =  0;
                $data['refund_comment'] = $_POST['refund_comment'];    
                $data['add_time']   = time();
                $data['refund_type'] = $refund_type;
                $data['refund_amount'] = $refund_amount;
                $data['refund_stage'] = $refund_stage;
                $data['refund_log'] = '';// serialize($order_refund);
                $data['is_delivery'] = 0;
                $data['is_commission'] = 0;    
                $refund_id = model('order_refund')->addRefund($data);    
                if($refund_id){
                    
                    foreach($refund_goods as $v){

                        $data = array(); 
                        $data['refund_id'] = $refund_id;                        
                        $data['order_id'] = $v['order_id'];                        
                        $data['goods_id'] = $v['goods_id'];                             
                        $data['goods_name'] =  $v['goods_name'];                        
                        $data['goods_price'] = $v['goods_price'];                           
                        $data['goods_num'] = $v['goods_num'];                               
                        $data['goods_image'] = $v['goods_image'];                       
                        $data['goods_pay_price'] = $v['goods_pay_price'];                     
                        $data['store_id'] = $v['store_id'];                      
                        $data['buyer_id'] = $v['buyer_id'];                     
                        $data['gc_id'] = $v['gc_id'];                      
                        $data['goods_size_id'] = $v['goods_size_id'];                    
                        $data['goods_specs_id'] =$v['goods_specs_id'];                      
                        $data['goods_spec'] = $v['goods_spec'];                      
                        $data['goods_size'] = $v['goods_size'];                       
                        $data['original_goods_id'] = $v['original_goods_id'];                       
                        $data['original_goods_name'] = $v['original_goods_name'];                         
                        $data['original_goods_num'] = $v['original_goods_num'];                       
                        $data['original_goods_spec'] = $v['original_goods_spec'];                         
                        $data['original_goods_size'] = $v['original_goods_size'];                         
                        $data['original_goods_price'] = $v['original_goods_price'];                        
                        $data['goods_lang'] = $v['goods_lang'];                        
                        $data['goods_optional_ids'] = $v['goods_optional_ids'];                        
                        $data['goods_optional'] = $v['goods_optional'];                        
                        $data['original_goods_optional'] = $v['original_goods_optional'];                        
                        $data['foodbox_price'] = $v['foodbox_price'];                      
                        $data['foodbox_id'] = $v['foodbox_id'];                        
                        $data['foodbox_name'] = $v['foodbox_name'];                       
                        $data['foodbox_num'] =$v['foodbox_num'];                         
                        $data['rec_id'] = $v['rec_id'];     
                        $data['goods_code'] = $v['goods_code'];                    
                        $row = model('order_goods_refund')->addRefund($data);

                        //更新主商品信息的可操作商品数量
                        model('order')->editOrderGoods(['goods_usable_num'=>['exp','goods_usable_num -'.$v['goods_num']]],['rec_id' => $v['rec_id']]);		


                    }

                }		
		}


        //全单退款
        if($refund_type == 2){		
            

            $goods_list  = model('order')->getOrderGoodsList(['order_id' => $order_info['order_id']]);
            $data = array();       
            $data['order_id']    = $order_info['order_id'];
            $data['order_sn']    = $order_info['order_sn'];
            $data['store_id']    = $order_info['store_id'];
            $data['store_name']  = $order_info['store_name'];
            $data['member_id']   = $order_info['buyer_id'];        
            $data['role']       =  0;
            $data['refund_comment'] = $_POST['refund_comment'];    
            $data['add_time']   = time();
            $data['refund_type'] = $refund_type;
            $data['refund_amount'] = $order_info['order_amount'];
            $data['refund_stage'] =  $refund_stage;
            $data['refund_log'] = '';// serialize($order_refund);
            $data['is_delivery'] = 1;
            $data['is_commission'] = 1;    //商家操作是不退抽成的。
            $refund_id = model('order_refund')->addRefund($data);    
            if($refund_id){

             
                foreach($goods_list as $v){
                    $data = array(); 
                    $data['refund_id'] = $refund_id;                        
                    $data['order_id'] = $v['order_id'];                        
                    $data['goods_id'] = $v['goods_id'];                             
                    $data['goods_name'] =  $v['goods_name'];                        
                    $data['goods_price'] = $v['goods_price'];                           
                    $data['goods_num'] = $v['goods_num'];                               
                    $data['goods_image'] = $v['goods_image'];                       
                    $data['goods_pay_price'] = $v['goods_pay_price'];                     
                    $data['store_id'] = $v['store_id'];                      
                    $data['buyer_id'] = $v['buyer_id'];                     
                    $data['gc_id'] = $v['gc_id'];                      
                    $data['goods_size_id'] = $v['goods_size_id'];                    
                    $data['goods_specs_id'] =$v['goods_specs_id'];                      
                    $data['goods_spec'] = $v['goods_spec'];                      
                    $data['goods_size'] = $v['goods_size'];                       
                    $data['original_goods_id'] = $v['original_goods_id'];                       
                    $data['original_goods_name'] = $v['original_goods_name'];                         
                    $data['original_goods_num'] = $v['original_goods_num'];                       
                    $data['original_goods_spec'] = $v['original_goods_spec'];                         
                    $data['original_goods_size'] = $v['original_goods_size'];                         
                    $data['original_goods_price'] = $v['original_goods_price'];                        
                    $data['goods_lang'] = $v['goods_lang'];                        
                    $data['goods_optional_ids'] = $v['goods_optional_ids'];                        
                    $data['goods_optional'] = $v['goods_optional'];                        
                    $data['original_goods_optional'] = $v['original_goods_optional'];                        
                    $data['foodbox_price'] = $v['foodbox_price'];                      
                    $data['foodbox_id'] = $v['foodbox_id'];                        
                    $data['foodbox_name'] = $v['foodbox_name'];                       
                    $data['foodbox_num'] =$v['foodbox_num'];                         
                    $data['rec_id'] = $v['rec_id'];     
                    $data['goods_code'] = $v['goods_code'];                    
                    $row = model('order_goods_refund')->addRefund($data);
                    //更新主商品信息的可操作商品数量
                    model('order')->editOrderGoods(['goods_usable_num'=>['exp','goods_usable_num -'.$v['goods_num']]],['rec_id' => $v['rec_id']]);		
                }
            }

		}
		
		$where =  array(
			'order_id' => $order_id,
			'store_id' => $this->store_info['store_id']			
		);	
		
		$update = array();
		$update['refund_state'] 	= $refund_type;
		$update['refund_amount'] 	= $refund_amount;
		$update['refund_comment'] 	= $_POST['refund_comment'];
		$update['refund_reason'] 	= $_POST['refund_reason'];	
        if($refund_type == 2){
            $update['order_state'] = 60;
        }
		$row = $model->editOrder($update,$where);	
		if($row){		
			//获取订单ID
			if($coupon_id > 0){				
				$t_id = intval($coupon_id);
				if($t_id <= 0){
				    output_error('店铺优惠券信息错误');
				}
				$model_voucher = Model('voucher');
				//验证是否可领取代金券
				$data = $model_voucher->getCanChangeTemplateInfo($t_id, $order_info['buyer_id'], 0);
				if ($data['state'] == false){
				    output_error($data['msg']);
				}
				try {
				    $model_voucher->beginTransaction();
				    //添加代金券信息
				    $data = $model_voucher->exchangeVoucher($data['info'], $order_info['buyer_id'], $order_info['buyer_name']);
				    if ($data['state'] == false) {
				        throw new Exception($data['msg']);
				    }
				    $model_voucher->commit();			            
				} catch (Exception $e) {
				    $model_voucher->rollback();
				    output_error($e->getMessage());
				}
				
			}
			//调用退款接口。根据原支付退回。
			
			//
			$record_data = array();
			$record_data['order_id'] = $order_info['order_id'];
			$record_data['merchant_refund_type']       =  'Manual';//配送员编号
			$record_data['merchant_refund_time']          = time() ;//当前批次第几单			
			model('order_record')->addRecord('merchant_refund',$record_data);
			   
			
            //执行全单退款
			if($refund_type == 2 && $refund_stage == 2){

                //执行商家退款
                $store_wallet = model('store_wallet');		
                $data = array();
                $data['amount'] = $order_info['order_amount'];
                $data['order_sn'] = $order_info['order_sn'];
                $data['order_id'] = $order_info['order_id'];
                $data['store_id'] = $order_info['store_id'];
                $data['store_name'] = $order_info['store_name'];
                $store_wallet->changePd('order_refund',$data);     
                
                 
                $pdf_model = model('pdf');
                  //生成票据所需
                $invoice_data = array();
                $invoice_data['order_id'] = $order_info['order_id'];
                $invoice_data['store_id'] = $order_info['store_id'];
                $invoice_data['user_id'] = $order_info['buyer_id'];		
                    
                //	print_r($invoice_data);			
                $pdf = $pdf_model->createPdf($invoice_data,'sales_credit_note_full');

                //执行退回到用户            
                //检查退回钱包的金额
                $pd_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
                if($pd_amount > 0){

                    $member_wallet = model('predeposit');
                    $data = array();
                    $data['amount'] = $pd_amount;
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['member_id'] = $order_info['buyer_id'];
                    $data['member_name'] = $order_info['buyer_name'];
                    $member_wallet->changePd('order_refund',$data);

                }

                $pay_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
                if($pay_amount > 0){
                    //执行stripe 退款
                    



                }


                 //违约金
                 //扣除商家的违约金
                 $store_wallet = model('store_wallet');		
                 $data = array();
                 $data['amount'] = $order_info['delivery_fee'];
                 $data['order_sn'] = $order_info['order_sn'];
                 $data['order_id'] = $order_info['order_id'];
                 $data['store_id'] = $order_info['store_id'];
                 $data['store_name'] = $order_info['store_name'];
                 $store_wallet->changePd('liquidated_damage',$data);                   
 
                //扣除销售抽成
                 $store_wallet = model('store_wallet');		
                 $data = array();
                 $data['amount'] = $order_info['commission_amount'] + $order_info['extxnfee'];
                 $data['order_sn'] = $order_info['order_sn'];
                 $data['order_id'] = $order_info['order_id'];
                 $data['store_id'] = $order_info['store_id'];
                 $data['store_name'] = $order_info['store_name'];
                 $store_wallet->changePd('platform_charges',$data);  


                 //生成违约金发票                    
                 $invoice_data = array();
                 $invoice_data['order_id'] = $order_info['order_id'];
                 $invoice_data['store_id'] = $order_info['store_id'];
                 $invoice_data['me'] = 1;	
                 //	print_r($invoice_data);			
                 $pdf = $pdf_model->createPdf($invoice_data,'liquidated_damage_receipt_merchant');
     


                 //补贴给配送员
                 $waiter_wallet = model('waiter_wallet');	
                 $data = array();
                 $data['amount'] = $order_info['delivery_fee'];
                 $data['order_sn'] = $order_info['order_sn'];
                 $data['order_id'] = $order_info['order_id'];
                 $data['distributor_id'] = $order_info['distributor_id'];
                 $data['distributor_name'] = $order_info['distributor_name'];              
                 $waiter_wallet->changePd('delivery_fee_reimbursment',$data);

                 //开补贴发票给平台                    
                 $invoice_data = array();
                 $invoice_data['order_id'] = $order_info['order_id'];
                 $invoice_data['delivery_id'] = $order_info['distributor_id'];
                 $invoice_data['me'] = 1;	
                 //	print_r($invoice_data);			
                 $pdf = $pdf_model->createPdf($invoice_data,'delivery_fee_reimbursement_invoice');


            }

					
						
			
			output_data('退款操作成功');
		}else{
			
			output_error('退款操作失败');
			
		}
		
		
	}
	
	
	//获取商品信息
	private function getOrderGoods($rec_id){
		
		$where = array();
		$where['rec_id'] = $rec_id;
		$where['store_id'] = $this->store_info['store_id'];
		
		
		
		$goods_info = model('order')->getOrderGoodsInfo($where);
		return $goods_info;
	}
	
	
	
	
	
	/*
	获取退款的选项
	*/
	
	public function refund_reasonOp(){
		
		$list = model('refund_return')->getReasonList(TRUE);
		
		$data = array();
		foreach($list as $v){
		    
		    $data[] = $v;
		    
		}
		
		
		output_data($data);
		
	}
	
	
	public function order_replacementOp(){
		
		
		
		
		
	}
	
	
	
	
	
    /**
     * 全部退款获取订单信息
     */
    public function refund_all_formOp(){
       
		$where = array(
			'order_id' => $_POST['order_id'],
			'store_id' => $this->store_info['store_id']
		);
		$info = model('order')->getOrderInfo($where);
		
		if($info){
		
            output_data(array('order' => $info));
       
		} else {
			
            output_error('参数错误');
			
        }
    }

	
	
	
	
	
	
    /**
     * 全部退款保存数据
     */
    public function refund_all_postOp(){
		
        $model_refund = Model('refund_return');
		
        $store_id = $this->store_info['store_id'];
        $order_id = intval($_POST['order_id']);
            
        $model_order = Model('order');
        $condition = array();
        $condition['store_id'] = $store_id;
        $condition['order_id'] = $order_id;
        $condition['order_state'] = 10 ;
      
		$order_info = $model_order->getOrderInfo($condition);	
		
        $payment_code = $order_info['payment_code'];//支付方式
        
		
        $condition = array();
        $condition['store_id'] = $store_id;
        $condition['order_id'] = $order_id;
        $condition['goods_id'] = '0';
        $condition['seller_state'] = array('lt','3');
		
        $refund = $model_refund->getRefundReturnInfo($condition);
        
		
		
        if (empty($order_info) || $payment_code == 'offline' || $refund['refund_id'] > 0) {//检查数据,防止页面刷新不及时造成数据错误
          
			output_error('参数错误');
			
        } else {
        
            $allow_refund_amount = ncPriceFormat($order_info['order_amount']);//可退款金额
            
            $refund_array = array();
            $refund_array['refund_type'] = '1';//类型:1为退款,2为退货
            $refund_array['seller_state'] = '2';//状态:1为待审核,2为同意,3为不同意
            $refund_array['order_lock'] = '2';//锁定类型:1为不用锁定,2为需要锁定
            $refund_array['goods_id'] = '0';
            $refund_array['order_goods_id'] = '0';
            $refund_array['reason_id'] = '0';
            $refund_array['reason_info'] = '取消订单，全部退款';
            $refund_array['goods_name'] = '订单商品全部退款';
            $refund_array['refund_amount'] = ncPriceFormat($allow_refund_amount);
            $refund_array['seller_message'] = $_POST['seller_message'];
            $refund_array['add_time'] = time();
          
            $state = $model_refund->addRefundReturn($refund_array,$order_info);
           
			if ($state) {
				
                //启用余额退款
				if($order_info['payment_code'] == 'predeposit'){					
					
					 $member_id = $order_info['buyer_id'];
					 $member_name = $order_info['buyer_name'];    
					 //执行退款
					 $model_pd = Model('predeposit');
					 $data_pd = array();
					 $data_pd['member_id'] = $member_id;
					 $data_pd['member_name'] = $member_name;
					 $data_pd['amount'] = $order_info['order_amount'];
					 $data_pd['order_sn'] = $order_info['order_sn'];
					 $row = $model_pd->changePd('refund',$data_pd);
				
					 if($row){				 
						  //记录订单日志(取消)
						$data = array();
						$data['order_id'] = $order_info['order_id'];
						$data['log_role'] = 'seller';
						$data['log_msg'] = '订单退款';
						$data['log_orderstate'] = 60;
						$insert = $model_order->addOrderLog($data);
						 
						 
						 //订单状态 置为已取消
						$data_order = array();
						$data_order['order_state'] = 70;
						$data_order['refund_state'] = 2;
						$data_order['refund_amount'] = $order_info['order_amount'];
						$result = $model_order->editOrder($data_order,array('order_id'=>$order_info['order_id'])); 
						if($result){
							output_data('ok');							
						}else{							
							output_error('退款失败');	
						}						 
					 }else{						 
						output_error('退款失败'); 
					 }										
				}				
				//启用stripe 退款
				if($order_info['payment_code'] == 'stripe'){
					
										
					
					
				}
				
				
				
				
				$model_refund->editOrderLock($order_id);
               
				output_data(1);
            } else {
                output_error('退款失败');
            }
        }
    }

	
	
	
	
    /**
     * 部分退款获取订单信息
     */
    public function refund_formOp(){
        $model_refund = Model('refund_return');
        $condition = array();
        $reason_list = $model_refund->getReasonList($condition, '', '', 'reason_id,reason_info');//退款退货原因
        $new_reason_list = array();
        foreach ($reason_list as $key => $value) {
            $new_reason_list[] = $value;
        }

        $store_id = $this->store_info['store_id'];
        $order_id = intval($_POST['order_id']);
        $goods_id = intval($_POST['order_goods_id']);//订单商品表编号
        
        $model_order = Model('order');
        $condition = array();
        $condition['store_id'] = $store_id;
        $condition['order_id'] = $order_id;
        $order_info = $model_refund->getRightOrderList($condition, $goods_id);
        $refund_state = $model_refund->getRefundState($order_info);//根据订单状态判断是否可以退款退货
        if($refund_state == 1 && $goods_id > 0) {
            $order = array();
            $order['order_id'] = $order_info['order_id'];
            $order['order_type'] = $order_info['order_type'];
            $order['order_amount'] = ncPriceFormat($order_info['order_amount']);
            $order['order_sn'] = $order_info['order_sn'];
            $order['store_name'] = $order_info['store_name'];
            $order['store_id'] = $order_info['store_id'];
            
            $goods = array();
            $goods_list = $order_info['goods_list'];
            $goods_info = $goods_list[0];
            
            $goods['store_id'] = $goods_info['store_id'];
            $goods['order_goods_id'] = $goods_info['rec_id'];
            $goods['goods_id'] = $goods_info['goods_id'];
            $goods['goods_name'] = $goods_info['goods_name'];
            $goods['goods_type'] = orderGoodsType($goods_info['goods_type']);
            $goods['goods_img_360'] = thumb($goods_info,360);
            $goods['goods_price'] = ncPriceFormat($goods_info['goods_price']);
            $goods['goods_spec'] = $goods_info['goods_spec'];
            $goods['goods_num'] = $goods_info['goods_num'];
            
            $goods_pay_price = $goods_info['goods_pay_price'];//商品实际成交价
            $order_amount = $order_info['order_amount'];//订单金额
            $order_refund_amount = $order_info['refund_amount'];//订单退款金额
            if ($order_amount < ($goods_pay_price + $order_refund_amount)) {
                $goods_pay_price = $order_amount - $order_refund_amount;
            }
            $goods['goods_pay_price'] = ncPriceFormat($goods_pay_price);
            output_data(array('order' => $order,'goods' => $goods,'reason_list' => $new_reason_list));
        } else {
            output_error('参数错误');
        }
    }
    
    /**
     * 部分退款保存数据
     */
    public function refund_postOp(){
        $member_id = $this->waiter_info['member_id'];
        $order_id = intval($_POST['order_id']);
        $goods_id = intval($_POST['order_goods_id']);//订单商品表编号
        
        $model_order = Model('order');
        $model_refund = Model('refund_return');
        
        $condition = array();
        $condition['buyer_id'] = $member_id;
        $condition['order_id'] = $order_id;
        $order_info = $model_refund->getRightOrderList($condition, $goods_id);
        $refund_state = $model_refund->getRefundState($order_info);//根据订单状态判断是否可以退款退货
        $condition = array();
        $condition['buyer_id'] = $member_id;
        $condition['order_id'] = $order_id;
        $condition['order_goods_id'] = $goods_id;
        $condition['seller_state'] = array('lt','3');
        $refund = $model_refund->getRefundReturnInfo($condition);
        if($refund_state == 1 && $goods_id > 0 && empty($refund)) {
            $goods_list = $order_info['goods_list'];
            $goods_info = $goods_list[0];
            $refund_array = array();
            $goods_pay_price = $goods_info['goods_pay_price'];//商品实际成交价
            $order_amount = $order_info['order_amount'];//订单金额
            $order_refund_amount = $order_info['refund_amount'];//订单退款金额
            if ($order_amount < ($goods_pay_price + $order_refund_amount)) {
                $goods_pay_price = $order_amount - $order_refund_amount;
            }
            $refund_amount = floatval($_POST['refund_amount']);//退款金额
            if (($refund_amount < 0) || ($refund_amount > $goods_pay_price)) {
                $refund_amount = $goods_pay_price;
            }
            $goods_num = intval($_POST['goods_num']);//退货数量
            if (($goods_num < 0) || ($goods_num > $goods_info['goods_num'])) {
                $goods_num = 1;
            }
            $reason_list = $model_refund->getReasonList(array(), '', '', 'reason_id,reason_info');//退款退货原因
            $refund_array['reason_info'] = '';
            $reason_id = intval($_POST['reason_id']);//退货退款原因
            $refund_array['reason_id'] = $reason_id;
            $reason_array = array();
            $reason_array['reason_info'] = '其他';
            $reason_list[0] = $reason_array;
            if (!empty($reason_list[$reason_id])) {
                $reason_array = $reason_list[$reason_id];
                $refund_array['reason_info'] = $reason_array['reason_info'];
            }
            
            $pic_array = array();
            $pic_array['buyer'] = $_POST['refund_pic'];//上传凭证
            $info = serialize($pic_array);
            $refund_array['pic_info'] = $info;
            
            $model_trade = Model('trade');
            $order_shipped = $model_trade->getOrderState('order_shipped');//订单状态30:已发货
            if ($order_info['order_state'] == $order_shipped) {
                $refund_array['order_lock'] = '2';//锁定类型:1为不用锁定,2为需要锁定
            }
            $refund_array['refund_type'] = $_POST['refund_type'];//类型:1为退款,2为退货
            $refund_array['return_type'] = '2';//退货类型:1为不用退货,2为需要退货
            if ($refund_array['refund_type'] != '2') {
                $refund_array['refund_type'] = '1';
                $refund_array['return_type'] = '1';
            }
            $refund_array['seller_state'] = '1';//状态:1为待审核,2为同意,3为不同意
            $refund_array['refund_amount'] = ncPriceFormat($refund_amount);
            $refund_array['goods_num'] = $goods_num;
            $refund_array['buyer_message'] = $_POST['buyer_message'];
            $refund_array['add_time'] = time();
            
            $state = $model_refund->addRefundReturn($refund_array,$order_info,$goods_info);
            if ($state) {
                if ($order_info['order_state'] == $order_shipped) {
                    $model_refund->editOrderLock($order_id);
                }
                output_data(1);
            } else {
                output_error('退款退货申请保存失败');
            }
        } else {
            output_error('参数错误');
        }
    }
    
    /**
     * 上传凭证
     */
    public function upload_picOp() {
        $upload = new UploadFile();
        $dir = ATTACH_PATH.DS.'refund'.DS;
        $upload->set('default_dir',$dir);
        $upload->set('allow_type',array('jpg','jpeg','gif','png'));
        $result = 0;
        if (!empty($_FILES['refund_pic']['name'])){
            $result = $upload->upfile('refund_pic');
        }
        if ($result){
            $file_name = $upload->file_name;
            $pic = UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$file_name;
            output_data(array('file_name' => $file_name,'pic' => $pic));
        } else {
            output_error('图片上传失败');
        }
    }

    /**
     * 退款记录列表
     */
    public function get_refund_listOp() {
        $model_order = Model('order');
        $model_refund = Model('refund_return');
        $member_id = $this->waiter_info['member_id'];
        $refund_list = array();
        $condition = array();
        $condition['buyer_id'] = $member_id;
        $keyword_type = array('order_sn','refund_sn','goods_name');
        if (trim($_GET['k']) != '' && in_array($_GET['type'],$keyword_type)){
            $type = $_GET['type'];
            $condition[$type] = array('like','%'.$_GET['k'].'%');
        }
        if (trim($_GET['add_time_from']) != '' || trim($_GET['add_time_to']) != ''){
            $add_time_from = strtotime(trim($_GET['add_time_from']));
            $add_time_to = strtotime(trim($_GET['add_time_to']));
            if ($add_time_from !== false || $add_time_to !== false){
                $condition['add_time'] = array('time',array($add_time_from,$add_time_to));
            }
        }
        $list = $model_refund->getRefundList($condition,$this->page);
        $page_count = $model_refund->gettotalpage();
        if(!empty($list) && is_array($list)) {
            $seller_state = $model_refund->getRefundStateArray('seller');
            $admin_state = $model_refund->getRefundStateArray('admin');
            foreach($list as $k => $v) {
                $val = array();
                $val['refund_id'] = $v['refund_id'];
                $val['order_id'] = $v['order_id'];
                $val['refund_amount'] = ncPriceFormat($v['refund_amount']);
                $val['refund_sn'] = $v['refund_sn'];
                $val['order_sn'] = $v['order_sn'];
                $val['add_time'] = date("Y-m-d H:i:s",$v['add_time']);
                $val['seller_state_v'] = $v['seller_state'];
                $val['seller_state'] = $seller_state[$v['seller_state']];
                $val['admin_state_v'] = $v['refund_state'];
                $val['admin_state'] = $v['seller_state']==2 ? $admin_state[$v['refund_state']]:'无';
                $val['store_id'] = $v['store_id'];
                $val['store_name'] = $v['store_name'];
                $goods_list = array();
                if ($v['goods_id'] > 0){
                    $goods = array();
                    $goods['goods_id'] = $v['goods_id'];
                    $goods['goods_name'] = $v['goods_name'];
                    
                    $condition = array();
                    $condition['rec_id'] = $v['order_goods_id'];
                    $order_goods_list = $model_order->getOrderGoodsList($condition);
                    $goods['goods_spec'] = $order_goods_list[0]['goods_spec'];
                    
                    $goods['goods_img_360'] = thumb($v,360);
                    $goods_list[] = $goods;
                } else {
                    $condition = array();
                    $condition['order_id'] = $v['order_id'];
                    $order_goods_list = $model_order->getOrderGoodsList($condition);
                    foreach($order_goods_list as $key => $value) {
                        $goods = array();
                        $goods['goods_id'] = $value['goods_id'];
                        $goods['goods_name'] = $value['goods_name'];
                        $goods['goods_spec'] = $value['goods_spec'];
                        $goods['goods_img_360'] = thumb($value,360);
                        $goods_list[] = $goods;
                    }
                }
                $val['goods_list'] = $goods_list;
                $refund_list[] = $val;
            }
        }
        output_data(array('refund_list' => $refund_list), mobile_page($page_count));
    }

    /**
     * 查看退款信息
     *
     */
    public function get_refund_infoOp(){
        $model_refund = Model('refund_return');
        $member_id = $this->waiter_info['member_id'];
        $condition = array();
        $condition['buyer_id'] = $member_id;
        $condition['refund_id'] = intval($_GET['refund_id']);
        $refund_info = $model_refund->getRefundReturnInfo($condition);
        if(!empty($refund_info) && is_array($refund_info)) {
            $seller_state = $model_refund->getRefundStateArray('seller');
            $admin_state = $model_refund->getRefundStateArray('admin');
            $refund = array();
            $refund['refund_id'] = $refund_info['refund_id'];
            $refund['goods_id'] = $refund_info['goods_id'];
            $refund['goods_name'] = $refund_info['goods_name'];
            $refund['order_id'] = $refund_info['order_id'];
            $refund['refund_amount'] = ncPriceFormat($refund_info['refund_amount']);
            $refund['refund_sn'] = $refund_info['refund_sn'];
            $refund['order_sn'] = $refund_info['order_sn'];
            $refund['add_time'] = date("Y-m-d H:i:s",$refund_info['add_time']);
            $refund['goods_img_360'] = thumb($refund_info,360);
            $refund['seller_state'] = $seller_state[$refund_info['seller_state']];
            $refund['admin_state'] = $refund_info['seller_state']==2 ? $admin_state[$refund_info['refund_state']]:'无';
            $refund['store_id'] = $refund_info['store_id'];
            $refund['store_name'] = $refund_info['store_name'];
            $refund['reason_info'] = $refund_info['reason_info'];
            $refund['buyer_message'] = $refund_info['buyer_message'];
            $refund['seller_message'] = $refund_info['seller_message'];
            $refund['admin_message'] = $refund_info['admin_message'];
            
            $info['buyer'] = array();
            if(!empty($refund_info['pic_info'])) {
                $info = unserialize($refund_info['pic_info']);
            }
            $pic_list = array();
            if(is_array($info['buyer'])) {
                foreach($info['buyer'] as $k => $v) {
                    if(!empty($v)){
                        $pic_list[] = UPLOAD_SITE_URL.'/'.ATTACH_PATH.'/refund/'.$v;
                    }
                }
            }
            
            $detail_info = $model_refund->getDetailInfo(array('refund_id'=> $refund_info['refund_id']));
            $detail_array = array();
            if(!empty($detail_info) && $detail_info['refund_state'] == 2) {
                $detail_array['refund_code'] = orderPaymentName($detail_info['refund_code']);
                $detail_array['pay_amount'] = ncPriceFormat($detail_info['pay_amount']);
                $detail_array['pd_amount'] = ncPriceFormat($detail_info['pd_amount']);
                $detail_array['rcb_amount'] = ncPriceFormat($detail_info['rcb_amount']);
            }
            output_data(array('refund' => $refund,'pic_list' => $pic_list,'detail_array' => $detail_array));
        } else {
            output_error('参数错误');
        }
    }
}
