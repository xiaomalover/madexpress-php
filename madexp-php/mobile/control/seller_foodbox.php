<?php
/**
 * 商家餐盒
 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_foodboxControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }

	/*
	//餐盒种类
	
	public function foodClassOp(){	
		
		$data = model('foodbox_class')->getClassList(TRUE);		
		output_data($data);
		
	}
	
	
	//获取餐盒的数据
	
	public function foodboxGoodsOp(){		
		
		///$class_id = $_GET['class_id'] ? $_GET['class_id']: $class_id;		
	
		$cartList= $this->cartGoods();
		
		$where = array(
			'class_id' => $_POST['class_id']
		);		
		
		
		$data = model('foodbox_goods')->getGoodsList($where);	
		
		$cart=array();
		foreach($data as $k => $v){
			
			$cart[$k]= $v;
			$cart[$k]['goods_num'] = $cartList[$v['goods_id']]['goods_num'] ?  $cartList[$v['goods_id']]['goods_num'] : 0;
			$cart[$k]['cart_id'] = $cartList[$v['goods_id']]['cart_id'] ?  $cartList[$v['goods_id']]['cart_id'] : 0;
			$count += $cartList[$v['goods_id']]['goods_num'] ?  $cartList[$v['goods_id']]['goods_num'] : 0;
			
		}
		
		
		output_data(array('cartList'=>$cart,'cartCount'=>$count));
		

	}*/
	
	
  
  	//餐盒种类
	
	public function foodClassOp(){	
		
		$list = model('foodbox_class')->getClassList(TRUE);		
		foreach($list as $k => $v){
		
          $data = $this->foodboxGoods($v['class_id']);
            $v['class_child'] = $data['list'];
           $v['class_count'] = $data['count'];
			$foodbox[$k] =$v;
			
		}
		output_data($foodbox);
		
	}
	
	
	//获取餐盒的数据
	
	private function foodboxGoods($class_id){		
		
		///$class_id = $_GET['class_id'] ? $_GET['class_id']: $class_id;		
	
		$cartList= $this->cartGoods();
		
		$where = array(
			'class_id' => $class_id
		);		
		
		
		$data = model('foodbox_goods')->getGoodsList($where);	
		
		$cart=array();
		foreach($data as $k => $v){
			
			$cart[$k]= $v;
			$cart[$k]['goods_num'] = $cartList[$v['goods_id']]['goods_num'] ?  $cartList[$v['goods_id']]['goods_num'] : 0;
			$cart[$k]['cart_id'] = $cartList[$v['goods_id']]['cart_id'] ?  $cartList[$v['goods_id']]['cart_id'] : 0;
			$count += $cartList[$v['goods_id']]['goods_num'] ?  $cartList[$v['goods_id']]['goods_num'] : 0;
			
		}
		
		return array('list'=>$cart,'count'=>$count);
		//output_data(array('cartList'=>$cart,'cartCount'=>$count));
		

	}
	
	
	

	
	
	
	//查询当前商品是否存在于购物车
	private function cartGoods(){
		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		$cart = model('foodbox_cart') ->listCart($where);
		$data = array();
		foreach($cart as $v){
			$data[$v['goods_id']] = $v;
		}
		
		
		return $data;
		
		
		
	}
	
	
	
	
	
	
	
	//餐盒订单列表
	
	public function foodOrderListOp(){
		
		 $state = array(
			
			 0 =>'未审核',
			 10 => '待分配',
			 20 => '待配送',
			 30 => '配送中',
			 40 => '已完成',
			 50 =>'取消'
			 
		 );
		
		$model=Model('foodbox_order');	
		
		$where = array();
		$where['store_id'] = $this->store_info['store_id'];
		
		if($_POST['state'] == '0'){
			$where['order_status'] = array('in','0,10,20') ;
		}
		
		if($_POST['state'] == '1'){
			$where['order_status'] = array('in','30') ;
		}
		
		if($_POST['state'] == '2'){
			$where['order_status'] = array('in','40') ;
		}
		
		
		
		
		$data = $model->getOrderList($where);
		$order_list = array();
		foreach($data as $k => $v){			
			$order_list[$k]['order_id'] = $v['order_id'];
			$order_list[$k]['order_sn'] = $v['order_sn'];
			$order_list[$k]['order_notes'] = $v['order_notes'];
			$order_list[$k]['order_type_name'] =$v['order_type'] == 0 ? '配送':'自提';;
			$order_list[$k]['order_status'] = $v['order_status'];
			$order_list[$k]['order_status_name'] = $state[$v['order_status']];			
			$order_list[$k]['order_add_time'] = date('d M Y',$v['order_add_time']);
			$order_list[$k]['order_send_time'] = date('d M Y',$v['order_send_time']);
			$order_list[$k]['distributor_name'] = $v['distributor_name'];
			$order_list[$k]['distributor_mobile'] = $v['distributor_mobile'];		
			$time = $this->orderTime($v['order_end_time'],time());
			$order_list[$k]['dq'] = $time;	
			
			
		}
		
		$page_count = $model->gettotalpage();	
		output_data(array('order_list' => $order_list), mobile_page($page_count));
		
	}
	
	
	private function orderTime($second1,$second2){
		
	  if ($second1 > $second2) {
			return round(($second1 - $second2) / 86400);
	   }else{
		    return 0;  
	   }
	   
	 
			
	}
	
	
	
	//餐盒订单详情	
	public function  foodOrderInfoOp(){	
		
		 $state = array(
			 0 =>'未审核',
			 10 => '待分配',
			 20 => '待配送',
			 30 => '配送中',
			 40 => '已完成',
			 50 =>'取消'
		 );
		
		$model=Model('foodbox_order');	
		$where = array();
		$where['order_id'] = $_POST['order_id'];
		$where['store_id'] = $this->store_info['store_id'];		
		$info = $model->getOrderInfo($where);	
		if(empty($info)){
			output_error('订单不存在');
		}
		
		
		$basic = array();
		$basic['order_sn'] = $info['order_sn'];
		$basic['order_id'] = $info['order_id'];
		$basic['order_status'] = $state[$info['order_status']]; //状态
		
		$order_info['order_basic'] = $basic;
		
		
		//配送信息
		$delivery =array();
		$delivery['distributor_id'] = $info['distributor_id'];
		$delivery['distributor_name'] = $info['distributor_name'];
		$delivery['distributor_code'] = $info['distributor_code'];
		$delivery['distributor_mobile'] = $info['distributor_mobile'];
		$delivery['vehicle_code'] = $info['vehicle_code'];		
		$order_info['order_delivery'] = $delivery;
		
		$goods_list =  $this->orderGoods($info['order_id']);
		
		
		$new_data = array();
		if(!empty($goods_list['goods'])){
			foreach($goods_list['goods'] as $v){
				$goods_data[$v['class_id']]['class_id'] = $v['class_id'];
				$goods_data[$v['class_id']]['class_name'] = $v['class_name'];
				$goods_data[$v['class_id']]['child'][] = $v;
			}
			
			foreach($goods_data as $v){
				$new_data[] = $v;
			}
		}
		
		
		
		
		//正常商品
		$goods = array();
		$order_info['order_goods'] = $new_data;
		
		//取消的商品
		$cancel = array();
		$cancel = $goods_list['goods_cancel'];
		$order_info['order_goods_cancel'] = $cancel;
		
		
		//替换的商品
		$replace = array();
		$replace = $goods_list['goods_replace'];
		$order_info['order_goods_replace'] = $replace;
		
		
		
		//基本信息
		$detail = array();
		$detail['add_time'] = date('d/m/y',$info['order_add_time']);
		$detail['service_time'] = date('d/m/y',$info['order_send_time']);
		$detail['delivery_type'] =  $info['order_type'] == 0 ? '配送':'自提';
		$detail['store_comment'] = $info['order_notes'];
		$detail['platform_comment'] = '';
		
		$detail['goods_price'] = $info['goods_price'];
		$detail['order_amount'] = $info['order_amount'];
		$detail['dispatching_fee'] = $info['dispatching_fee'];
		$detail['order_store_type'] = $info['order_store_type'];
		$detail['order_type'] = $info['order_type'];
		
		$order_info['order_detail'] = $detail;
		
		
		
		
		output_data($order_info);
	}
	
	
	//订单商品
	
	private function orderGoods($order_id){
		
		//
		$model = model('foodbox_order_goods');
		$data = array();
		$where = array(
			'order_id' => $order_id			
		);
		
		$goods_list = $model->getOrderGoodsList($where);			
		$goods = array();		
		$goods['goods'] = array();
		$goods['goods_cancel'] = array();
		$goods['goods_replace'] = array();
		
		foreach($goods_list as $k => $v){
			if($v['type'] == 0){
				$goods['goods'][] = $v;
			}
			
			if($v['type'] == 1){
				$goods['goods_cancel'][] = $v;				
			}
			
			if($v['type'] == 2){
				$goods['goods_replace'][] = $v;				
			}
		}
		
		return $goods;
	}
	

	//购物车餐盒统计
	public function cartCountOp(){
		
		$model = model('foodbox_cart');
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		
		$goods_list = model('foodbox_cart')->listCart($where);
		$cart_num = array();
		foreach($goods_list as $v){
			
			$cart_num[$v['class_id']]['class_id'] =  $v['class_id'];
			$cart_num[$v['class_id']]['class_name'] =  $v['class_name'];
			$cart_num[$v['class_id']]['goods_num'] +=  $v['goods_num'];
			$cart_num[$v['class_id']]['goods_price'] +=  $v['goods_price'];
		}
		
		
		
		output_data($cart_num);
		
		

		
	}

	
	
	
	/*
	*  餐盒购物车
	*  
	*
	*/
	
	//餐盒写入餐盒购物车
	
	public function cartAddOp(){		
		
		$data = array();
		$data['store_id'] = $this->store_info['store_id'];
		$data['store_name'] = $this->store_info['store_name_primary'];
		
		$where = array(
			'goods_id' => $_POST['goods_id']
		);
	
		$goods = model('foodbox_goods')->getGoodsInfo($where);
		
		$cart = model('foodbox_cart')->getCartInfo(array('goods_id'=> $_POST['goods_id'],'store_id'=>$this->store_info['store_id']));
		
		if($cart){
				$where = array(
					'cart_id' => $cart['cart_id'],
					'store_id' => $this->store_info['store_id']
				);	
				$data = array(
					'goods_num' => $cart['goods_num'] + 1
				);		
			
				
				$row = model('foodbox_cart')->editCart($data,$where);
			
		}else{
		
			
				$data['goods_id'] = $goods['goods_id'];
				$data['goods_name'] = $goods['goods_name'];
				$data['goods_price'] = $goods['goods_price'];
				$data['goods_num'] = 1;
				$data['class_id'] = $goods['class_id'];
				$data['class_name'] = $goods['class_name'];			
				$data['goods_icon'] = $goods['goods_icon'];	
			
				$row = model('foodbox_cart')->addCart($data);
			
		}
		
		
		if($row){
			output_data(array('cart_id'=>$row));
		}else{
			output_error('写入失败');
		}
		
	}
	
	
	
	
	
	//编辑数量
	public function cartUpdateOp(){
		
		
		$cart = model('foodbox_cart')->getCartInfo(array('goods_id'=> $_POST['goods_id'],'store_id'=>$this->store_info['store_id']));
		
		if($cart['goods_num'] == 1){
			
			$where = array(
				'cart_id'=> $cart['cart_id'],
				'store_id' => $this->store_info['store_id']
			);
			$cart = model('foodbox_cart')->delCart($where);
		
		}else{
			
			$where = array(
				'cart_id' => $cart['cart_id'],
				'store_id' => $this->store_info['store_id']
			);
			
			$data = array(
				'goods_num' => $cart['goods_num'] - 1
			);			
			$cart = model('foodbox_cart')->editCart($data,$where);	

			
		}
		
	
		if($cart){
			output_data($cart);
		}else{
			output_error('写入失败');
		}	
		
		
	}
	
	
	
	/**
	订单流程开始
	
	
	*/
	public function foodBoxOrderOp(){
		
		
		$model = model('foodbox_cart');
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		
		$goods_list = model('foodbox_cart')->listCart($where,'class_id');
		
		$cart_list = array();
		foreach($goods_list as $k => $v){			
			$cart_list[$k]['class_id'] =  $v['class_id'];			
			$cart_list[$k]['class_name'] =  $v['class_name'];			
			$cart_list[$k]['child'] = $this->cartClass($v['class_id']);
		}		
		output_data($cart_list);		
		
	}
	
	//获取分类下的商品
	private function cartClass($class_id){
		
		$where = array(
			'class_id' => $class_id,
			'store_id' => $this->store_info['store_id']
		);
		$list = model('foodbox_cart')->listCart($where);
		
		foreach($list as $k => $v){
			$data[$k] = $v;
			$data[$k]['goods_amount'] += $v['goods_price'];
		}		
		return $data;
		
	}
	
	
	
	
	
	
	
	
	//保存餐盒申请
	
	public function foodBoxSaveOp(){		
		
		$goods_price = 0;
		$order_amount =0;
		$dispatching_fee =0;
		
		
		$order_model = model('foodbox_order');		
		$where = array(
			'store_id' => $this->store_info['store_id']
		);
		$list = model('foodbox_cart')->listCart($where);
		$order_goods = array();
		foreach($list as $v){			
			$goods_price += $v['goods_price'] * $v['goods_num'];
		}		
		$order_amount = $goods_price + $dispatching_fee;
		$data = array();
		$data['order_sn'] = $order_model->makeSn();
		$data['order_add_time'] = time();		
		$data['goods_price']  = $goods_price;
		$data['order_amount'] = $order_amount;
		$data['dispatching_fee'] = $dispatching_fee;
		$data['store_id'] = $this->store_info['store_id'];
		$data['store_name'] = $this->store_info['store_name'];
		
		$data['store_region_id'] = $this->store_info['region_id'];
		$data['store_region_sm_id'] = $this->store_info['region_sm_id'];
		$data['store_region_name'] = $this->store_info['region_name'];
		$data['store_region_color'] = $this->store_info['region_color'];
		
		
		
		
		
		$data['order_notes'] = $_POST['content'];
		$data['order_type'] = $_POST['order_type'];		
		$row = $order_model->addOrder($data);
		if($row){
			
			//写入订单商品
			foreach($list as $v){				
				$goods[] = array(
					'order_id' => $row,
					'store_id' => $this->store_info['store_id'],
					'goods_id' => $v['goods_id'],
					'goods_num' => $v['goods_num'],
					'goods_price' => $v['goods_price'],
					'class_id' => $v['class_id'],
					'class_name' => $v['class_name'],
					'total_price' => $v['goods_num'] * $v['goods_price'],
					'goods_name' => $v['goods_name'],
					'type' => 0
				);
			}
			
			model('foodbox_order_goods')->addOrderGoodsAll($goods);			
			output_data($row);
		}else{
			output_error('申请失败');	
        }
	}
	
	
}
?>