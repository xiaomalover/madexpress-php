<?php
/**
 * 支付
 *

 */



defined('InMadExpress') or exit('Access Invalid!');

class waiter_paymentControl extends mobileWaiterControl {

    private $payment_code;
    private $payment_config;

    public function __construct() {
        parent::__construct();
    }
	
	
		
  
	
	
	
	
	/**
     * 支付
     */
    public function pay_vehicleOp() {
       
        $lease_id = $_POST['lease_id'];

		$where = array();		
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		$where['lease_id'] = $lease_id;
		$order_info = model('vehicle_lease')->getLeaseInfo($where);
		
	//	print_r($order_info);
		if($order_info['payment_state'] == 2){
			output_error('订单已支付');
		}	 
		
		if($_POST['payment_code'] == 'wallet'){
		    
		    $this->_pd_pay($order_info);		
		    
		}
		
		if($_POST['payment_code'] == 'stripe'){
		    
		    $this->_api_pay($order_info);
		}
		
		
		
		
	

    }

   
	
	
	
	
	
	/**
     * 实物订单
     *
     */
    private function _pd_pay($order_info) {
       
	
        $model_waiter = Model('waiter');
        $waiter_info = $model_waiter->getWaiterInfoByID($this->waiter_info['distributor_id']);
      
	
        if ($waiter_info['available_predeposit'] == 0) {
            output_data('钱包余额不足');
        }
		
		
//        if (floatval($order_info['payment_state']) > 0 || floatval($order_info['pd_amount']) > 0) {
//            return $order_list;
//        }

		
        try {
            $model_waiter->beginTransaction();
           // $logic_buy_1 = Logic('buy_1');
           
    
			
        			
              
				
				//$order_list = $logic_buy_1->pdPay($order_list, $post, $buyer_info);
          
			
			    $waiter_id = $waiter_info['distributor_id'];
				$waiter_name = $waiter_info['distributor_name'];

				$available_pd_amount = floatval(ncPriceFormat($waiter_info['available_predeposit']));
				if ($available_pd_amount <= 0) return $order_info;

				$model_vehicle_lease = Model('vehicle_lease');
				
				$model_pd = Model('waiter_predeposit');
				
				
			
				if($order_info['payment_state'] == 0){			
					
					$order_amount = floatval(ncPriceFormat($order_info['lease_first_amount']));
					
				}
				
				if($order_info['payment_state'] == 1){					
					
					$order_amount = floatval(ncPriceFormat($order_info['lease_last_amount']));
					
				}
								
				
				
				$data_pd = array();
				$data_pd['waiter_id'] = $waiter_id;
				$data_pd['waiter_name'] = $waiter_name;
				$data_pd['amount'] = $order_amount;
				$data_pd['order_sn'] = $order_info['lease_sn'];

				if ($available_pd_amount >= $order_amount && $order_amount > 0) {
				

					//预存款立即支付，订单支付完成
					$model_pd->changePd('order_lease_pay',$data_pd);
								
					$available_pd_amount -= $order_amount;
					$available_pd_amount = floatval(ncPriceFormat($available_pd_amount));


					//记录订单日志(已付款)
					$data = array();
					$data['order_id'] = $order_info['lease_id'];
					$data['log_role'] = 'waiter';
					$data['log_msg'] = '支付订单';
					$data['log_orderstate'] = 10;
						
					$insert = $model_vehicle_lease->addOrderLog($data);
							
					if (!$insert) {
						output_error('记录订单预存款支付日志出现错误');
					}
				
					//订单状态 置为已支付
					$data_order = array();
					
					$update = array();
					if($order_info['payment_state']  == 0 && $_POST['payment_type'] == 'deposit'){		
						
						if($order_info['lease_cotenant'] == 0){
							$update['lease_state'] = 20;
						}else{
							$update['lease_state'] = 10;
						}
						
						$update['payment_code'] = 'wallet';
						$update['payment_name'] = '钱包支付';
						$update['payment_time'] = TIMESTAMP;
						$update['payment_state'] = 1;
						
					}
					
					
					if($order_info['payment_state'] == 1  && $_POST['payment_type'] == 'rent'){
						
						$update['lease_state'] = 50;
						$update['payment_code'] = 'wallet';
						$update['payment_name'] = '钱包支付';
						$update['payment_time'] = TIMESTAMP;
						$update['payment_state'] = 2;
						
					}
					
				
						$result = model('vehicle_lease')->editLease(array('lease_id'=>$order_info['lease_id']),$update);
					
						if (!$result) {
								output_error('订单更新失败');
						}	
					}else{					
						output_error('您已支付或者订单金额有误');
					}
    
				
	
            $model_waiter->commit();
        } catch (Exception $e) {
            $model_waiter->rollback();
            output_error($e->getMessage());
        }
	
	    output_data(array('message'=>'支付成功'));
		
    }

	
	
    /**
	
     * 第三方在线支付接口
     *
     */
    private function _api_pay($order_info) {
        $inc_file = BASE_PATH.DS.'api'.DS.'payment'.DS.'stripe'.DS.'stripe.php';
        if(!is_file($inc_file)){
            exit('支付接口不存在');
        }
        require($inc_file);
       

        	\Stripe\Stripe::setApiKey('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');
			$token = $_POST['stripeToken'];
			if($token == ''){				
				output_error('支付卡token错误');		
			}			 
			
			$amount = 0;
			if($_POST['payment_type'] == 'deposit'){
			   $amount = $order_info['lease_first_amount'] * 100;
			   $desc = '支付押金订单号：'.$order_info['lease_sn'];
			}
			
			if($_POST['payment_type'] == 'rent'){
			    
			    $amount = $order_info['lease_last_amount'] * 100;
			    $desc = '支付租金订单号：'.$order_info['lease_sn'];
			}
			
			
			
			$charge = \Stripe\Charge::create([
				'amount' => $amount,
				'currency' => 'usd',
				'description' => $desc ,
				'source' => $token,
			]);				
		
			if($charge['status'] == 'succeeded'){	
			
			    	if($order_info['payment_state']  == 0 && $_POST['payment_type'] == 'deposit'){		
						
						if($order_info['lease_cotenant'] == 0){
							$update['lease_state'] = 20;
						}else{
							$update['lease_state'] = 10;
						}
						
						$update['payment_code'] = 'Stripe';
						$update['payment_name'] = 'Stripe支付';
						$update['payment_time'] = TIMESTAMP;
						$update['payment_bank_id'] = $_POST['bank_id'];
						$update['payment_state'] = 1;
						
					}
					
					
					if($order_info['payment_state'] == 1 && $_POST['payment_type'] == 'rent'){
						
						$update['lease_state'] = 50;
						$update['payment_code'] = 'Stripe';
						$update['payment_name'] = 'Stripe支付';
						$update['payment_bank_id'] = $_POST['bank_id'];
						$update['payment_time'] = TIMESTAMP;
						$update['payment_state'] = 2;
						
					}
					
				    $result = model('vehicle_lease')->editLease(array('lease_id'=>$order_info['lease_id']),$update);
					
					
					output_data(array('message'=>'支付成功'));
					
			}else{
			    
			    	output_error($charge['error']);
			}



	
    }
	
	
	 /**
     * 可用支付参数列表
     */
    public function payment_listOp() {
		
        $model_mb_payment = Model('mb_payment');
        $payment_list = $model_mb_payment->getMbPaymentOpenList();
		
        $payment_array = array();
        if(!empty($payment_list)) {
           
			foreach ($payment_list as $k => $v) {           
			    $payment_array[$k]['payment_code'] = $v['payment_code'];
				if($v['payment_code'] == 'predeposit'){					
					$payment_array[$k]['available_predeposit'] = $this->waiter_info['available_predeposit'];
			    	$payment_array[$k]['payment_code'] = 'wallet';
				}
				
				if($v['payment_code'] == 'stripe'){				
					$payment_bank = model('waiter_bank')->getWaiterBankList(array('distributor_id'=>$this->waiter_info['distributor_id']));					
				}
				
				$payment_array[$k]['payment_name'] = $v['payment_name'];
			
				
            }			
        }
		
		
		//加载当前需要支付的费用		
		$order_type = $_POST['order_type'];
		$lease_id = $_POST['lease_id'];
		$amount = 0;
		if($order_type == 'vehicle'){ //租车付款信息
			
				$where = array();		
				$where['distributor_id'] = $this->waiter_info['distributor_id'];
				$where['lease_id'] = $lease_id;
				$order_info = model('vehicle_lease')->getLeaseInfo($where);
				
				if($order_info['payment_state']  == 0){
					$amount = $order_info['lease_first_amount'];												
				}
					
				if($order_info['payment_state'] == 1){					
					$amount = $order_info['lease_last_amount'];							
				}			
		}
			
		
        output_data(array('payment_list' => $payment_array,'amount'=>$amount,'payment_bank'=>$payment_bank));
    }

	
	
	
	

   
	

}
