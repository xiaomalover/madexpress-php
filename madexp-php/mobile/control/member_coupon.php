<?php
/**
 * 我的优惠券

 *
 */



  
defined('InMadExpress') or exit('Access Invalid!');
class member_couponControl extends mobileMemberControl{

    public function __construct() {
        parent::__construct();
        $this->model_coupons = Model('redpacket');
    }

    /**
     *优惠券列表
     */
    public function coupon_listOp(){
		
		$where = array(
			'rpacket_owner_id' => $this->member_info['member_id'],
			'rpacket_state' => $_POST['type']
		);
		
    	$platform_coupon_list = Model('redpacket')->getRedpacketList($where);    	
	
	
		$model_voucher = Model('voucher');
		
		$store_coupon_list = $model_voucher->getMemberVoucherList($this->member_info['member_id'],$_POST['type']);
        foreach($store_coupon_list as $v){
            $v['voucher_title'] = $v['store_name_primary'];
            $coupon_list[] = $v;

        }


		
		
    	output_data(array('platform_coupon_list' => $platform_coupon_list,'store_coupon_list' => $coupon_list));
    }

}