<?php
/**
 * 钱包操作
 *
 */



defined('InMadExpress') or exit('Access Invalid!');

class walletControl extends mobileAdminControl {
  //每次导出订单数量
    const EXPORT_SIZE = 1000;
    private $gettype_arr;
    private $templatestate_arr;
    private $redpacket_state_arr;
    private $member_grade_arr;
    
    public function __construct(){
        parent::__construct();
		$model_redpacket = Model('redpacket');
		$this->gettype_arr = $model_redpacket->getGettypeArr();
		$this->templatestate_arr = $model_redpacket->getTemplateState();
		$this->redpacket_state_arr = $model_redpacket->getRedpacketState();
		$this->member_grade_arr = Model('member')->getMemberGradeArr();
    }

	
	//使用列表
	public function coupon_order_listOp(){		
		
		
		$model_redpacket = Model('redpacket');		
		$where = array();
		$where['rpacket_state'] = 2;		
		$data = $model_redpacket->getRedpacketList($where, '*', 0, $this->page, 'rpacket_id desc');
		
		
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list = array();
		foreach ($data as $val) {
		    
			$i = $val;
		    $i['rpacket_code'] = $val['rpacket_code'];		
		    $i['rpacket_owner_name'] = $val['rpacket_owner_name']?$val['rpacket_owner_name']:'未领取';
		    $i['rpacket_active_datetext'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_active_date']):'';
			$i['rpacket_usage_time'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_usage_time']):'';
			
		    $list[] = $i;
		}
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));		
	}


	 
	
	//优惠券列表
	
	public function coupon_listOp(){
		$where = array();
		if ($_POST['advanced']) {
		    if (strlen($q = trim($_POST['rpt_title']))) {
		        $where['rpacket_t_title'] = array('like', '%' . $q . '%');
		    }
		    if (($q = (int) $_POST['rpt_POSTtype']) > 0) {
		        $where['rpacket_t_POSTtype'] = $q;
		    }
		    if (($q = (int) $_POST['rpt_state']) > 0) {
		        $where['rpacket_t_state'] = $q;
		    }
		    if (strlen($q = trim($_POST['rpt_recommend']))) {
		        $where['rpacket_t_recommend'] = (int) $q;
		    }
		
		    if (trim($_POST['sdate']) && trim($_POST['edate'])) {
		        $sdate = strtotime($_POST['sdate']);
		        $edate = strtotime($_POST['edate']);
		        $where['rpacket_t_updatetime'] = array('between', "$sdate,$edate");
		    } elseif (trim($_POST['sdate'])) {
		        $sdate = strtotime($_POST['sdate']);
		        $where['rpacket_t_updatetime'] = array('egt', $sdate);
		    } elseif (trim($_POST['edate'])) {
		        $edate = strtotime($_POST['edate']);
		        $where['rpacket_t_updatetime'] = array('elt', $edate);
		    }
		
		    $pdates = array();
		    if (strlen($q = trim((string) $_POST['pdate1'])) && ($q = strtotime($q . ' 00:00:00'))) {
		        $pdates[] = "rpacket_t_end_date >= {$q}";
		    }
		    if (strlen($q = trim((string) $_POST['pdate2'])) && ($q = strtotime($q . ' 00:00:00'))) {
		        $pdates[] = "rpacket_t_start_date <= {$q}";
		    }
		    if ($pdates) {
		        $where['pdates'] = array('exp',implode(' and ', $pdates));
		    }
		} else {
		    if (strlen($q = trim($_POST['query']))) {
		        switch ($_POST['qtype']) {
		            case 'rpt_title':
		                $where['rpacket_t_title'] = array('like', "%$q%");
		                break;
		        }
		    }
		}
		
		switch ($_POST['sortname']) {
		    case 'rpacket_t_price':
		    case 'rpacket_t_limit':
		        $sort = $_POST['sortname'];
		        break;
		    case 'rpacket_t_mgradelimittext':
		        $sort = 'rpacket_t_mgradelimit';
		        break;
		    case 'rpacket_t_updatetimetext':
		        $sort = 'rpacket_t_updatetime';
		        break;
		    case 'rpacket_t_start_datetext':
		        $sort = 'rpacket_t_start_date';
		        break;
		    case 'rpacket_t_end_datetext':
		        $sort = 'rpacket_t_end_date';
		        break;
		    case 'rpacket_t_statetext':
		        $sort = 'rpacket_t_state';
		        break;
		    case 'rpacket_t_recommend':
		        $sort = 'rpacket_t_recommend';
		        break;
		    default:
		        $sort = 'rpacket_t_id';
		        break;
		}
		if ($_POST['sortorder'] != 'asc') {
		    $sort .= ' desc';
		}
		
		$model_redpacket = Model('redpacket');
		$data = $model_redpacket->getRptTemplateList($where, '*', 0, $this->page, $sort);
		
	
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list  = array();
		foreach ($data as $val) {
		  
		    $i = array();
			$i['coupon_id'] = $val['rpacket_t_id'];
		    $i['coupon_title'] = $val['rpacket_t_title'];
		    $i['coupon_price'] = $val['rpacket_t_price'];
		    $i['coupon_limit'] = $val['rpacket_t_limit'];
			$i['coupon_total'] = $val['rpacket_t_total'];
			$i['coupon_amount'] = $val['rpacket_t_total'] * $val['rpacket_t_price'];
			
		    $i['coupon_mgradelimittext'] = $val['rpacket_t_mgradelimittext'];
		    $i['coupon_updatetimetext'] = date('Y-m-d', $val['rpacket_t_updatetime']);
		    $i['coupon_start_datetext'] = date('Y-m-d', $val['rpacket_t_start_date']);
		    $i['coupon_end_datetext'] = date('Y-m-d', $val['rpacket_t_end_date']);
		    $i['coupon_type_text'] = $val['rpacket_t_gettype_text'];
		    $i['coupon_statetext'] = $val['rpacket_t_state_text'];
		    $i['coupon_recommendtext'] = $val['rpacket_t_recommend'] == '1'
		        ? '<span class="yes"><i class="fa fa-check-circle"></i>是</span>'
		        : '<span class="no"><i class="fa fa-ban"></i>否</span>';
		
		    $list[] = $i;
		}
		
		output_data(array('list' => $list), mobile_page($page_count,$list_count));
			
		
		
	}
	
	//优惠券详情
	public function coupon_infoOp(){
		
		$model_redpacket = Model('redpacket');		
		$where = array(
			'rpacket_t_id' => $_POST['coupon_id']
		);		
		$info = $model_redpacket->getRptTemplateInfo($where);
		if($info){
			$info['coupon_start_datetext'] = date('Y-m-d', $info['rpacket_t_start_date']);
			$info['coupon_end_datetext'] = date('Y-m-d', $info['rpacket_t_end_date']);
			$info['rpacket_t_nogive'] = $info['rpacket_t_total'] - $info['rpacket_t_giveout'];
			$info['rpacket_t_noused'] = $info['rpacket_t_total'] - $info['rpacket_t_used'];
		}
		
		output_data(array('info' => $info));
	}
	
	
	
	public function coupon_info_listOp(){		
		$model_redpacket = Model('redpacket');
		$where = array(
			'rpacket_t_id' => $_POST['coupon_id']
		);		
		$data = $model_redpacket->getRedpacketList($where, '*', 0, $this->page, 'rpacket_id desc');
		$page_count = $model_redpacket->gettotalpage();
		$list_count = $model_redpacket->gettotalnum();
		$list = array();
		foreach ($data as $val) {			    
				$i = $val;
			    $i['rpacket_code'] = $val['rpacket_code'];		
			    $i['rpacket_owner_name'] = $val['rpacket_owner_name']?$val['rpacket_owner_name']:'未领取';
			    $i['rpacket_active_datetext'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_active_date']):'';
				$i['rpacket_usage_time'] = $val['rpacket_owner_id']>0?date('Y-m-d H:i', $val['rpacket_usage_time']):'';				
			    $list[] = $i;
		}
		output_data(array('list' => $list), mobile_page($page_count,$list_count));	
	}
	
	//搜索发送的用户
	public function coupon_member_listOp(){
		
		
		
	}
	
	//发放
	public function coupon_giveOp(){				
		$member_ids =$_POST['member_ids'];
		$tid = $_POST['tid'];
		$model = model('redpacket');

		if(!empty($member_ids)){	
			foreach($member_ids as $v){			
				$redpacket = $model->getCanChangeTemplateInfo($tid,$v);
				
				if($redpacket['state'] == true){					
						$model->exchangeRedpacket($redpacket['info'],$v);				
				}else{
					output_error($redpacket['msg']);
					
				}
			}
			output_data('发放成功');
		}else{
			output_error('请勾选用户');		
		}
			
	}
	
	
	
	//添加优惠券
	public function coupon_saveOp(){
		
		
		$obj_validate = new Validate();
		$obj_validate->validateparam = array(
		         array("input"=>$_POST['rpt_title'], "require"=>"true","validator"=>"Length","min"=>"1","max"=>"20","message"=>'优惠券名称不能为空且小于20个字符'),
		        // array("input"=>$_POST['rpt_gettype'], "require"=>"true","message"=>'请选择领取方式'),
		         array("input"=>$_POST['rpt_data'][0], "require"=>"true","message"=>'请选择有效期开始时间'),
		         array("input"=>$_POST['rpt_data'][1], "require"=>"true","message"=>'请选择有效期结束时间'),
		         array("input"=>$_POST['rpt_price'], "require"=>"true","validator"=>"Number","min"=>"1","message"=>'面额不能为空且为大于1的整数'),
		         array("input"=>$_POST['rpt_total'], "require"=>"true","validator"=>"Number","min"=>"1","message"=>'可发放数量不能为空且为大于1的整数'),
		         array("input"=>$_POST['rpt_orderlimit'], "require"=>"true","validator"=>"Double","min"=>"0","message"=>'单笔使用消费限额不能为空且必须是数字')
		         
		 );
		 $error = $obj_validate->validate();
		 //开始时间不能大于结束时间
		 $stime = strtotime($_POST['rpt_data'][0]);
		 $etime = strtotime($_POST['rpt_data'][1]);
		 if ($stime > $etime){
		     $error.= '开始时间不能大于结束时间';
		 }
		 //验证红包面额不能大于订单限额
		 $price = floatval($_POST['rpt_price'])>0?floatval($_POST['rpt_price']):0;
		 $limit = floatval($_POST['rpt_orderlimit'])>0?floatval($_POST['rpt_orderlimit']):0;
		 if($limit>0 && $price>=$limit) $error.= '面额不能大于消费限额';
		 //验证卡密红包发放数量
		 $gettype = trim(2);
		 if($gettype == 'pwd'){
		     if (intval($_POST['rpt_total']) > 10000){
		         $error.= '领取方式为卡密兑换的红包，发放总数不能超过10000张';
		     }
		 }
		 //验证积分
		 $points = intval($_POST['rpt_points']);
		 if($gettype == 'points' && $points < 1){
		     $error.= '兑换所需积分不能为空且为大于1的整数';
		 }
		 if ($error){
		     output_error($error);
		 }else {
		     $model_redpacket = Model('redpacket');
		     $insert_arr = array();
		     $insert_arr['rpacket_t_title'] = trim($_POST['rpt_title']);
		     $insert_arr['rpacket_t_desc'] = trim($_POST['rpt_title']);
		     $insert_arr['rpacket_t_start_date'] = $stime;
		     $insert_arr['rpacket_t_end_date'] = $etime;
		     $insert_arr['rpacket_t_price'] = $price;
		//     $insert_arr['rpacket_t_limit'] = $limit;
		
		
		     $insert_arr['rpacket_t_adminid'] = $this->admin_info['admin_id'];
		     $insert_arr['rpacket_t_state'] = $this->templatestate_arr['usable']['sign'];
		     $insert_arr['rpacket_t_total'] = intval($_POST['rpt_total']);
		     $insert_arr['rpacket_t_giveout'] = 0;
		     $insert_arr['rpacket_t_used'] = 0;
		     $insert_arr['rpacket_t_updatetime'] = time();
		     $insert_arr['rpacket_t_points'] = $points;
		     $insert_arr['rpacket_t_eachlimit'] = ($t = intval($_POST['rpt_eachlimit']))>0?$t:0;
		     $insert_arr['rpacket_t_recommend'] = 0;
		     $insert_arr['rpacket_t_gettype'] = in_array($gettype,array_keys($this->gettype_arr))?$this->gettype_arr[$gettype]['sign']:$this->gettype_arr[$model_redpacket::GETTYPE_DEFAULT]['sign'];
		     $insert_arr['rpacket_t_isbuild'] = 0;
		     $mgrade_limit = intval($_POST['rpt_mgradelimit']);
		     $insert_arr['rpacket_t_mgradelimit'] = in_array($mgrade_limit,array_keys($this->member_grade_arr))?$mgrade_limit:$this->member_grade_arr[0]['level'];
		  
			//print_r($insert_arr);
		  
		     $rs = $model_redpacket->addRptTemplate($insert_arr);
			 
		     if($rs){
		         //生成卡密红包
		         if($gettype == 'pwd'){
		             QueueClient::push('buildPwdRedpacket', $rs);
		         }
		        
				output_data(array('message' => '优惠券添加成功'));		         
		     }else{
				 output_error('添加失败');		         
		     }
		 }
		
	}
	
	
	
	
}
