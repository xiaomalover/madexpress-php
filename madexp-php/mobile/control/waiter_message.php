<?php
/**
 * 我的消息
 */

defined('InMadExpress') or exit('Access Invalid!');
class waiter_messageControl extends mobileWaiterControl {

    public function __construct() {
        parent::__construct();
        $this->model_msg = Model('waiter_msg');
    }
	
	
    /**
     * 消息列表
     */
    public function indexOp() {
		
	
		$where = array();		
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		
	
		$where['msg_read'] = $_POST['msg_read'];
	
		
		
		
		$if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['start_date']);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$_POST['end_date']);
        $start_unixtime = $if_start_date ? strtotime($_POST['start_date']) : null;
        $end_unixtime = $if_end_date ? strtotime($_POST['end_date']): null;
     
	    if ($start_unixtime || $end_unixtime) {
            $where['msg_addtime'] = array('time',array($start_unixtime,$end_unixtime));
        }
		
//	print_r($where);
		
		//	file_put_contents("log.txt", json_encode($where));
		
        $data = $this->model_msg->getWaiterMsgList($where,'*',$this->page);
		$msg_list = array();
		foreach($data as $k => $v){
			$msg_list[$k]['msg_id'] = $v['msg_id'];
			$msg_list[$k]['msg_code'] = $v['msg_code'];
			$msg_list[$k]['msg_code_name'] = $v['msg_code_name'];
			$msg_list[$k]['msg_content'] = $v['msg_content'];		
			$msg_list[$k]['msg_read'] = $v['msg_read'];					
			$msg_list[$k]['msg_addtime'] = date('d/m/y',$v['msg_addtime']);
			
			$msg_list[$k]['msg_data_id'] = $v['msg_data_id'];		
		}
		
		$page_count = $this->model_msg->gettotalpage();				
        output_data(array('msg_list' => $msg_list),mobile_page($page_count));        
    }
	
	
	//消息详情
	public function infoMsgOp(){
		
		
		
			
		
	}
	
	
	//全部已读
	public function allStateOp(){
		
		$data = array();
		$data['msg_read'] = 1;
	
		$where = array();
		$where['distributor_id'] = $this->waiter_info['distributor_id'];
		if($_POST['msg_id'] > 0){
			$where['msg_id'] = $_POST['msg_id'];
		}
		$row = $this->model_msg->editWaiterMsg($data,$where);
		if($row){
			 output_data('ok');
		}else{
			 output_error('操作失败');
		}
	}
	
	
	
	
	
	public function delMsgOp(){
		
		$where = array(
			'msg_id' => $_POST['msg_id'],
			'distributor_id' => $this->waiter_info['distributor_id']
		);
		
		$result = $this->model_msg->delWaiterMsg($where);
		
		if($result){
		  output_data('ok');			
		}else{
		  output_data('删除失败');	
		}
		
	}
	
	
	
	
	public function readidsMsgOp(){
		
		$where = array(
			'distributor_id' => $this->waiter_info['distributor_id'],
			'msg_read' => 0
		);
		
		$num = $this->model_msg->getWaiterMsgCount($where);
		output_data(array('readids_num' => $num));		
	}
	
	


}
