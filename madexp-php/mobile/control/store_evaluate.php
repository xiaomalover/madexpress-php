<?php
/**
 * 商户订单管理
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_evaluateControl extends mobileSellerControl{
  
	

	public function __construct(){
        parent::__construct();   
    }
	
	
	//数据统计
	public function statOp(){
		
		
	}
	
	
	//用户评价列表
	public function evaluate_listOp(){
		
		$model_evaluate = Model('evaluate_store');		
		$condition = array();
		if(!empty($_POST['goods_name'])) {
		    $condition['geval_goodsname'] = array('like', '%'.$_POST['goods_name'].'%');
		}
		$condition['store_evaluate_storeid'] = $this->store_info['store_id'];
		
		if($_POST['keyword']){
			$condition["store_evaluate_goods"] = array('like', '%'.$_POST['keyword'].'%');
		 }
		 
		$list = $model_evaluate->getEvaluateStoreList($condition, 10, 'store_evaluate_id desc');		
		$page_count = $model_evaluate->gettotalpage();
		$list_count = $model_evaluate->gettotalnum();
		
		$evaluate_name = $this->evaluate_name();
		
		$data = array();
		foreach($list as $k => $v){
			
			$data[$k] = $v;
			$data[$k]['store_evaluate_score'] =$evaluate_name[$v['store_evaluate_score']];
			$data[$k]['store_evaluate_addtime'] = date('Y-m-d H:i:s',$v['store_evaluate_addtime']);
			$data[$k]['store_evaluate_user_isnew'] = $v['store_evaluate_user_isnew'] == 1? '否':'是';
			
		}	
		
		output_data(array('list' => $data), mobile_page($page_count,$list_count));
		
	}
	
	//用户评价详情
	public function evaluate_infoOp(){
		
		$model_evaluate = Model('evaluate_store');
		$where = array();
		$where['store_evaluate_storeid'] = $this->store_info['store_id'];
		$where['store_evaluate_id'] = $_POST['eva_id'];
		
		$info = $model_evaluate -> getEvaluateStoreInfo($where);
		$info['store_evaluate_addtime'] = date('d/m/Y H:i:s',$info['store_evaluate_addtime']);
		
		$info['store_log'] = $this->getEvaLog($info['store_evaluate_id']);
		
		
		output_data(array('eva_info' => $info));
		
	}
	
	public function save_evaOp()
	{
	    $data = array(
	            'eva_id'	 	=> $_POST['eva_id'],
	            'log_content' 	=> $_POST['reply_content'],
	            'log_file' 		=> $_POST['reply_file'],
	            'log_addtime'	=> time(),
	            'reply_user_id' => $this->store_info['store_id'],
	            'reply_user_name' => $this->store_info['store_name_primary'],
				'reply_user_type' => 0
	        );
	    $row = model('evaluate_store')->addLog($data);
	    if ($row) {
	        output_data('保存成功');
	    } else {
	        output_error('保存失败');
	    }
	}
	
	private function getEvaLog($eva_id){
		
		$where = array();
		$where['eva_id'] = $eva_id;
		$list = model('evaluate_store')->getLogList($where);		
		foreach($list as $v){
			$v['log_addtime'] = date('d/m/y H:i',$v['log_addtime']);
			$eva_log[] = $v;
		}
		return $eva_log;		
		
	}
	
	private function evaluate_name(){
	
			 $data= array();
			 $data[1] = '不喜欢';
			 $data[2] = '一般';
			 $data[3] = '喜欢';
			return $data;
	
	
	}
		
		
	
	
	
}
