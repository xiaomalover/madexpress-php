<?php
/**
 * 商家商品管理
 *
 *

 */



defined('InMadExpress') or exit('Access Invalid!');

class seller_goodsControl extends mobileSellerControl {

    public function __construct(){
        parent::__construct();
    }
	
	
	public function indexOp(){		
		$this->goodsListOp();
	}
	
	
	
	
	

    public function goods_addOp() {		
		
		
		 $obj_validate = new Validate();
            $obj_validate->validateparam = array(
                array("input"=>$_POST["goods_name"],"require"=>"true","message"=>'商品名称不能为空'),
                array("input"=>$_POST["gc_id"],"require"=>"true","validator"=>"Number","message"=>'店铺分类不能为空'),
               
   
            );
            $error = $obj_validate->validate();
            if ($error != ''){
                 output_error($error);
            }
		
		
		
		$data = array();
		$data['goods_name'] 	= $_POST['goods_name'];
		$data['gc_id'] 			= $_POST['gc_id'];
		$data['gc_name'] 		= $_POST['gc_name'];
		$data['store_id'] 		= $this->store_info['store_id'];
		$data['store_name'] 	= $this->store_info['store_name'];
		$data['goods_body'] 	= $_POST['goods_body'];		
		$data['goods_attr'] 	= serialize($_SESSION['goods_attr']);	//获取属性
		$data['goods_spec'] 	= serialize($_SESSION['goods_spec']); //获取规格		
		
		
		$data['goods_stock'] 	= $_POST['goods_stock']; //每日库存
		
		$data['goods_addtime'] 	=  time();
		$data['goods_price'] 	= $_SESSION['goods_spec'][0]['shop_price'];
		$data['goods_image'] 	= $_POST['goods_image'];
		$data['is_recommend'] 	= $_POST['is_recommend'];
		
		$icon = $_SESSION['goods_icon'];
		$goods_icon = '';
		foreach($icon as $v){
			$goods_icon .= $v.',';
		}				
		$data['goods_icon'] = $goods_icon;		
		//print_r($data);
		
		$row = model('goods')->addGoods($data);
		
		if($row){	
			
			$_SESSION['goods_attr'] = array();
			$_SESSION['goods_spec'] = array();
			$_SESSION['goods_icon'] = array();			
			
		     output_data(array('goods_id' => $row));
		}else{
			output_error('新增失败');
		}
		
     
    }

	//格式化当前规格数据
	public function setSpecSessionOp(){
		
		$data = array();		
		
		$data[] = array(
			'type' => $_POST['type_one_value'],
			'stock_id' => $_POST['foodbox_one_value'],
			'shop_price' => ncPriceFormat($_POST['shop_price_one'])
		);		
		
		$data[] =array(			
			'type' => $_POST['type_two_value'],
			'stock_id' => $_POST['foodbox_two_value'],
			'shop_price' => ncPriceFormat($_POST['shop_price_two'])		
		
		);		
		
		$_SESSION['goods_spec'] = $data;
		output_data('ok');
		
	}
	
	public function getSpecSessionOp(){		
		$spec = $_SESSION['goods_spec'];		
		output_data($spec);		
	}
	
	
	//记录口味种类，	
	public function setAttrSessionOp(){
		
		if( is_array($_SESSION['goods_attr'])){			
				$data  = $_SESSION['goods_attr'];
		}else{
				$data = array();			
		}		
		array_push($data,$_POST['attr_name']);		
		$_SESSION['goods_attr'] = array_unique($data);		
		output_data('ok');
	}
	
	
	public function getAttrSessionOp(){		
		$attr = $_SESSION['goods_attr'];		
		output_data($attr);		
	}
	
	
	
	//记录商品ICon
	public function setGoodsIconSessionOp(){
		
		
		if( is_array($_SESSION['goods_icon'])){			
				$data  = $_SESSION['goods_icon'];
		}else{
				$data = array();			
		}
		
		if(in_array($_POST['attr_id'],$data)){
			foreach( $data as $k=>$v) {
				if($_POST['attr_id'] == $v) unset($data[$k]);
			}
		}else{
			array_push($data,$_POST['attr_id']);		
		}
	
		$_SESSION['goods_icon'] = array_unique($data);	
		
		output_data('ok');
		
		
		
	}
	
	
	public function getGoodsIconSessionOp(){
		
		
		$icon = $_SESSION['goods_icon'];		
		foreach($icon as $v){
			$icon_id .= $v.',';
		}
		$data = $this->goods_icon($icon_id);		
		output_data($data);		
		
		
	}
	
	
	
	

	
	
	 /**
     * 店铺商品
     */
    public function goodsListOp()
    {
		
	
		$model_goods = Model('goods');
        $where = array();
        $where['store_id'] = $this->store_info['store_id'];
		$where['is_delete'] = 0;
		$where['is_old'] = 0;	
		
		if($_POST['is_recommend'] > 0){
			$where['is_recommend'] = 1;
		}
		
		if($_POST['is_state_down'] > 0){
			$where['goods_state'] = 0;
		}
		
		if($_POST['is_state_up'] > 0){
			$where['goods_state'] = 1;
		}
		
		
		if (intval($_POST['class_id']) > 0) {
		  		$ext_goods = $this->extGoods($_POST['class_id']);
				$where['goods_code'] = array('in',$ext_goods);
          
	    } 
		
		//搜索关键词
		if(!empty($_POST['keyword'])){
				$ext_goods = $this->extSearchGoods($_POST['keyword']);				
				$where['goods_id'] = array('in',$ext_goods);
		}
		
		
				
					
        $goods_list = $model_goods->getGoodsList($where,'*');		
		foreach($goods_list as $k=> $v){
			$goods_data[$k] = $v;
			$goods_data[$k]['lang_name'] = $this->getLangGoods($v['goods_id']);
			$goods_data[$k]['goods_image'] = $this->goodsImageFormat($v['goods_image']);
			$goods_data[$k]['goods_ingr'] = $this->goodsIngr($v['goods_ingr']);
		}		
			
       	output_data(array('goods_list' => $goods_data));
	   
	   
    }
	
	
		
	// 读取扩展分类的商品
	private function extGoods($stc_id){		
		$ext_goods =  model('store_goods_extended')->getGoodsExtList(array('stc_id'=> $stc_id,'store_id'=> $this->store_info['store_id']),'goods_code');		
		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods .=$v['goods_code'].',';
			}
			$goods = substr($goods,0,-1);
		}else{
			$goods = '';	
		}
		return $goods;
		
	}
	
	//搜索
	private function extSearchGoods($keyword){
		
		$where = " goods_name like '%".$keyword."%' or goods_description  like '%".$keyword."%' or goods_specs like '%".$keyword."%' or goods_sizeprice  like '%".$keyword."%'";
	
		
		$ext_goods =  model('goods_language')->getGoodsLangList($where,'goods_id');		
	

		if(!empty($ext_goods)){
			foreach($ext_goods as $v){			
				$goods[$v['goods_id']] = $v['goods_id'];
			}
			$goods = implode(',',$goods);
		}else{
			$goods = '';	
		}
		return $goods;
		
	}
	
	//获取多语言数据
	private function getLangGoods($goods_id){
		$list = model('goods_language')->getGoodsLangList(array('goods_id' => $goods_id));
		
		$data = array();
		foreach($list as $k=> $v){
			$data[$k] = $v;
			$data[$k]['lang_name'] =  $v['lang_name'];	
			$data[$k]['goods_sizeprice'] = unserialize($v['goods_sizeprice']);
			if(empty($data[$k]['goods_sizeprice'])){				
				$data[$k]['goods_sizeprice'] = array(array(
					'name'=>'',
					'box_id' => '',
					'price' => '',
					'sale_price' => ''
				)) ;				
			}
			
			$data[$k]['goods_specs'] = unserialize($v['goods_specs']) ;		
			if(empty($data[$k]['goods_specs'])){	
				$data[$k]['goods_specs'] = array(array(
					'name'=>'',
					'value' => '',
					'price' => '',
				));							
			}
				
						
		}
		return $data;
	}
	
	
	
	//格式化图片
	private function goodsImageFormat($image){
		
		if(empty($image)){
			return array();			
		}
		$data = explode(',',$image);		
		foreach($data as $k => $v){
			$list[$k]['file_name'] = UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $this->store_info['store_id'] . DS .$v;
			$list[$k]['file'] = $v;
			$list[$k]['type'] ='url';
		}
		
		return $list;		
	}
		
	//商品ICON
	
	private function goodsIngr($ingr){
	
		$where = array(
			'attr_id' => array('in',$ingr)
		);
		$ingr = model('goods_attr')->getGoodsAttrList($where);
		$ingr_data = array();			
		foreach($ingr as $k=> $v){
			$ingr_data[$k]['attr_id'] =  $v['attr_id'];
		    if($v['attr_type'] == 1){
				$new = explode("|", $v['attr_content']);
				$ingr_data[$k]['attr_name'] = $new[0];
				$ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
				$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
			
			}else{
				$ingr_data[$k]['attr_name'] =  $v['attr_name'];
				$ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];	
				$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];	
				
			}	
		}
		return $ingr_data;	
	}
	
	
	
	/**
     * 店铺商品分类
     */
    private function storeGoodsClass($store_id)
    {

	   
       $store_goods_class = Model('store_goods_class')->getStoreGoodsClassPlainList($this->store_info['store_id'],$this->store_info['is_default_lang']);
	   $data = array();
	   foreach($store_goods_class as $k => $v){		   
		   $data[$k] = $v;	
		   
		   	 
		  // $data[$k]['cart_num'] =  $this->cart_count($v['class_id'],$store_id,1);		   
	  }	
      return $data;
    }
	
	
	/****************************************/
	//分类列表
	/*****************************************/
	
	
	
	public function seller_goods_classOp(){
		
		$class = Model('store_goods_class')->getStoreGoodsClassList(array('store_id' => $this->store_info['store_id']));
	
		if($type == 'list'){
			$store_goods_class[] = array(
				'stc_id' => 'all',
				'stc_name' =>'全部'
			);
		}
		
		foreach($class as $v){
			$data = $v;
			$data['stc_id'] = $v['stc_id'];
			$data['active'] = 0;
			$data['stc_lang_name'] = unserialize($v['stc_lang_name']);						
			$data['goods_count'] = $this->goodsCount($v['stc_id']);
			$store_goods_class[] = $data;
		}
	
		output_data(array('class_list' => $store_goods_class));
	
		
	}
	
	
	
	//计算分类下有多少个商品
	private function goodsCount($stc_id){
	//	$count = model('goods')->getGoodsCount(array('gc_id'=> $stc_id,'store_id'=> $_SESSION['store_id']));	
		$ext_count =  model('store_goods_extended')->getGoodsExtCount(array('stc_id'=> $stc_id,'store_id'=> $_SESSION['store_id']));
		return $ext_count;
	}
	
	
	

    /**************************************************************************************
     * 商品详细信息
     */
     public function goods_infoOp() {
		 	
			
			$where = array(
				'goods_id' => $_POST['goods_id'],
				'store_id' => $this->store_info['store_id']
			);
			// 商品详细信息
			$model_goods = Model('goods');
			$goods_detail = $model_goods->getGoodsInfo($where);		
			$goods_detail = $this->_goods_detail_extend($goods_detail);				
			if (empty($goods_detail)) {
				output_error('商品不存在');
			}
			output_data($goods_detail);
			
			
		 /*	$where = array(
				'goods_id' => $_POST['goods_id']
			);
			$goods_info = model('goods')->getGoodsInfo($where);
			$goods_info['goods_image'] = cthumb($goods_info['goods_image']);
		 	$goods_info['goods_attr'] = unserialize($goods_info['goods_attr']);
			$goods_info['goods_spec'] = unserialize($goods_info['goods_spec']);		
		 	$goods_info['goods_icon'] = $this->goods_icon($goods_info['goods_icon']);		 
			output_data($goods_info);*/
      
    }
	
	
	
	  /**
     * 商品详细信息处理
     */
    private function _goods_detail_extend($goods_detail) {
        
		      $store_id = $goods_detail['store_id'];			
		      $lang = $this->goods_language($goods_detail['goods_id']);		
				$goods_detail['goods_name'] = $lang['goods_name'];	
				$goods_detail['goods_description'] = $lang['goods_description'];	
				$goods_detail['goods_specs'] = $this->FormatData(unserialize($lang['goods_specs']),'specs',$lang['spec_attr_name']);	
				$goods_detail['goods_sizeprice'] =  unserialize($lang['goods_sizeprice']);	
			// $data[$k]['goods_optional'] = unserialize($lang['goods_optional']);	
				$goods_detail['goods_lang'] = $lang['lang_name'];				
				$goods_detail['goods_image'] = $this->goodsImageFormat($goods_detail['goods_image'],$store_id) ;		
				$goods_detail['goods_ingr'] = $this->goods_ingr($goods_detail['goods_ingr']);
				$goods_detail['goods_optional'] = $this->goodsOptional($goods_detail['goods_optional'],$store_id);
			//$goods_detail['cart_num'] = $this->cart_count($v['goods_id'],$store_id);					
		
     		   return $goods_detail;
    }
	
	
	
	
	
	//
	private function goods_icon($icon){
		
		$where = array(
			'attr_id' => array('in',$icon)
		);
		
		$list = model('goods_attr')->getGoodsAttrList($where);
		
		
		return $list;
		
	}
	
	//获取商品ICON
	public function goods_icon_listOp(){		
		$action = $_SESSION['goods_icon'];		
		$list = model('goods_attr')->getGoodsAttrList(TRUE);	
		$data =array();
		foreach($list as $k => $v){			
			$data[$k] = $v;	
			$data[$k]['action'] = @in_array($v['attr_id'],$action)  ? 1 : 0;			
		}		
		output_data($data);		
	}
	
	
	
	
	
    /**
     * 商品编辑保存
     */
    public function goods_editOp() {
       
	   
	   $where = array(
	   	'goods_id' => $_POST['goods_id'],
		'store_id' => $this->store_info['store_id']
	   );
	   
	   $data = array(
	   		'goods_state' => $_POST['goods_state'],
			'goods_stock' => $_POST['goods_stock'],
			'is_recommend' => $_POST['is_recommend'],
	   );
	   
	   $row = model('goods')->editGoodsNew($data,$where);
	   if($row){
		   	output_data('ok');		
			   
		}else{
		   	output_data('保存失败');		
		}
	   
	   
	   
    }
    
  
	


	
    /**
     * 商品删除
     */
    public function goods_dropOp() {
				
		$where = array(			
			'store_id' => $this->store_info['store_id'],
			'goods_id' => $_POST['goods_id']
		);		
		$update = array(
			'goods_state' => 2
		);		
        $result = model('goods')->editGoodsNew($update,$where);		
        if ($result) {
            output_data('ok');
        }else{	
        	output_error('删除失败');
		}
    }


	//商品ICON
	
	public function goodIconOp(){	
		$attr = model('goods_attr')->getGoodsAttrList(TRUE);		
		output_data($attr);	
	}
	
	
	
	//选中select
	public function selectIconOp(){
		
		
	}
	
	
	//获取餐盒
	public function foodboxOp(){		
		$foodbox = model('foodbox_class')->getClassList(TRUE);
		$data = array();
		foreach($foodbox as $k =>$v){
			$data[$k] = $v;
			$data[$k]['child'] = $this->foodbox_goods($v['class_id']);
		}
		
		output_data($data);
		
		
		
	}
	
	private  function foodbox_goods($class_id){
		
		$where = array(
			'class_id' => $class_id
		);
		$goods = model('foodbox_goods')->getGoodsList($where);		
		return $goods;
	}
	
	
	
	
	 public function updateFileOp()
    {
		
		$store_id = $this->store_info['store_id'];
		
		
        // 判断图片数量是否超限
        $model_album = Model('album');    

        $class_info = $model_album->getOne(array('store_id' => $store_id, 'is_default' => 1), 'album_class');
        // 上传图片
        $upload = new UploadFile();
        $upload->set('default_dir', ATTACH_GOODS . DS . $store_id . DS . $upload->getSysSetPath());
        $upload->set('max_size', C('image_max_filesize'));
        $upload->set('thumb_width', GOODS_IMAGES_WIDTH);
        $upload->set('thumb_height', GOODS_IMAGES_HEIGHT);
        $upload->set('thumb_ext', GOODS_IMAGES_EXT);
        $upload->set('fprefix', $store_id);
        $upload->set('allow_type', array('gif', 'jpg', 'jpeg', 'png'));
        $result = $upload->upfile('file');
        if (!$result) {
            return callback(false, $upload->error);
        }
        $img_path = $upload->getSysSetPath() . $upload->file_name;
        // 取得图像大小
        if (!C('oss.open')) {
            list($width, $height, $type, $attr) = getimagesize(BASE_UPLOAD_PATH . '/' . ATTACH_GOODS . '/' . $store_id . DS . $img_path);
        } else {
            list($width, $height, $type, $attr) = getimagesize(C('oss.img_url') . '/' . ATTACH_GOODS . '/' . $store_id . DS . $img_path);
        }
        // 存入相册
        $image = explode('.', $_FILES[$image_name]["name"]);
        $insert_array = array();
        $insert_array['apic_name'] = $image['0'];
        $insert_array['apic_tag'] = '';
        $insert_array['aclass_id'] = $class_info['aclass_id'];
        $insert_array['apic_cover'] = $img_path;
        $insert_array['apic_size'] = intval($_FILES[$image_name]['size']);
        $insert_array['apic_spec'] = $width . 'x' . $height;
        $insert_array['upload_time'] = TIMESTAMP;
        $insert_array['store_id'] = $store_id;
        $model_album->addPic($insert_array);

        $data = array ();
        $data ['thumb_name'] = cthumb($img_path, 240, $store_id);
        $data ['name']      = $img_path;

		output_data($data);
		
		//output_data(array('url'=> UPLOAD_SITE_URL.'/'.ATTACH_SHOP.'/','file_name'=>$upload->file_name ));                  

       
    }
		
	
	
	
	public function goods_sortOp(){
				
		$data = $_POST['goods_data'];
		
		foreach($data as $v){
			
			
			
			
			
		}
		
		
		
		
		
		
		
	
	
		
		
	}
	
	
	
	
}
