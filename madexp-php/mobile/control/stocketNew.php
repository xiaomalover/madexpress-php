<?php 
defined('InMadExpress') or exit('Access Invalid!');

class stocketNewControl extends mobileHomeControl {
	
	private $PI = 3.14159265358979324;
    private $x_pi = 0;
	
	public function __construct(){
        parent::__construct();	
		$this->x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    }



	//步骤，第一步 ，下单  <br />
	//第二步 ，抓取配送员<br />
	//第三步	，计算配送员顺序
	//第四部 ，



	
	public function indexOp(){
		
		
		
	}
	
	

	
	//订单排序条件
	
	private function orderSort(){
		
		
		
		
		
		
		
		
	}
	
	
	
	
	//配送员排序条件
	
	
	private function deliverySort($deliver_id,$order){
		
		$count = $this->deliverOrderCount($deliver_id);
		switch ($count) {
		   case 2: 
				$where = array(
					'distributor_id' => $deliver_id,		
					'store_id' => $order['store_id'],			
					'order_state' => array('in','30,40')				
				);
				$count = model('order')->getOrderCount($where);
				if($count  == 2){
					
					return  1 ;	
					
				}elseif($count == 1){
					
					return  2 ;
					
				}elseif($count == 0){
					return  4;
				}	
				
				
			 break;
		   case 1:
		   
				$where = array(
					'distributor_id' => $deliver_id,	
					'store_id' => $order['store_id'],						
					'order_state' => array('in','30,40')				
				);
				$count = model('order')->getOrderCount($where);
				
				//有一单
				if($count  == 1){
					return 3;
					
				}elseif($count == 0){
					
					return 5;
					
				}
				
			
			 break;
		   case 0:	
		   	   
				return 6;	
								
		   break;		  
		}
	}
	
	
	private function deliverOrderCount($id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
	
	
	

	
	//订单
	public function getOrderOp(){
		
		$where = array(
			'order_state' => array('in','20')
		);
		$list = model('order')->getOrderListNew($where,'*', 'daycode asc');
		$data = array();	
		foreach($list as $k => $v ){				
			$data[$k]['order_sn'] = $v['order_sn'];
			$data[$k]['order_id'] = $v['order_id'];
			$data[$k]['store_name'] = $v['store_name'];
			$store =  explode(',',$v['store_coordinate']) ;
			$data[$k]['deliver'] = $this->getOrderDeliver(array('store_lat'=> $store[1],'store_lng'=>$store[0]),$v);

			
		}		
		output_data(array('list'=>$data));
	}
	
	
	
	
	
	
	
	
	private function getDelivery(){
		
		
		
		
		
		
	}
	
	
	//后去配送员列表
	public function getDeliveryOp(){
		
			$where['distributor_status'] = 1;
			$where['distributor_online'] = 20;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			$state = array(
				'10' => '已下线',
				'20' => '接单中'
			);
			foreach($deliver_data as $k => $v){	
			
				$deliver[$k]['distributor_name'] = $v['distributor_name'];
				$deliver[$k]['order_list'] =  $this->getDeliverOrder($v['distributor_id']);
				$deliver[$k]['distributor_state'] =  $state[$v['distributor_online']];
				
				
				
					$feature[$k]['type'] = 'Feature';
					$feature[$k]['properties']['iconSize'] = array(28,28);
					$feature[$k]['properties']['name'] = $v['distributor_name'];			
					$feature[$k]['properties']['id'] = $v['distributor_id'];								
					$feature[$k]['geometry']['type'] = 'Point';
					
					$deliver_coordinate = explode(',',$v['region_coordinate']);
					
					$feature[$k]['geometry']['coordinates'][0] = $deliver_coordinate[0];			
					$feature[$k]['geometry']['coordinates'][1] = $deliver_coordinate[1];			
					
				
				
				
				
			}
		
			$deliver_list['type'] = 'FeatureCollection';
			$deliver_list['features'] = $feature;
		
		
		
			output_data(array('deliver'=>$deliver,'deliver_list'=>$deliver_list));
		
	}
	
	
	
	//获取配送员现在手里的订单
	private function getDeliverOrder($deliver_id){
		
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		$order_data = model('order')->getOrderList($where);
		$order_list = array();
		foreach($order_data as $v){
			$order_list[] = $v['store_name'].'-'.$v['order_id'];
			
		}
				
		
		return $order_list;
		
	}
	
	
	
	
	//获取订单周边范围的可用配送员
	private function getOrderDeliver($store,$order){
			$where = array();
			$where['distributor_status'] = 1;
			$where['distributor_online'] = 20;
			$where['delivery_state'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			foreach($deliver_data as $v){
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$order);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order['store_id']);
				
				//距离小于5000的可以加入
				//
				if($distance < 5000 && $superposition == 1 || $superposition == 0  ){	
					$deliver[] = array(
						'distributor_name' => $v['distributor_name'],
						'deliver_sort' => $deliver_sort,//订单排序
						'deliver_order' => '',//最早的订单
						'deliver_freetime' => ''//空闲时间
					);		
				}
				
				
			}	
			
			return $deliver;	
	
	}
	
	
	//获取配送员可接的叠加商户
	private function getOrderSuperpositionStore($deliver_id,$store_id){
		
		$count = 0;
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		$order = model('order')->getOrderInfoN($where,'*','order_id desc');
		if(!empty($order)){
			
			//直接读取当前商户是否可以叠加其他的店铺
			$store = explode(',',$order['store_superpose']);
			foreach($store as $v){
				
				if($v == $store_id){
					
					return 1; //叠加商户
					
				}else{
					
					return 2; //不是叠加商户
					
				}				
			}
		}else{
			
					return 0; //没有单子
				
		}
		
	}
	
	
	
	//获取商家周边范围的可用配送员
	private function getStoreDeliver($store){
			$where = array();
			$where['distributor_status'] = 1;
			$where['distributor_online'] = 20;
			$where['delivery_state'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			foreach($deliver_data as $v){
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$order);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order['store_id']);
				
				if($distance < 5000){	
					$deliver[] = $v['distributor_name'];							
				}
				
			}
			
			return implode(',',$deliver);	
	
	}
	
	
	
	
	//获取商家
	public function getStoreOp(){
	
		$color = array('#f40606','#0000ff','#009245','#ff00ff','#754c24','#f7931e');		
		$where = array();
		$where['store_state'] = 1;
		$store = model('store')->getStoreList($where);
		foreach($store as $k => $v){
			
			$data[$k]['store_id'] = $v['store_id'];
			$data[$k]['store_name_primary'] = $v['store_name_primary'];			
			$data[$k]['store_lat'] = $v['store_lat'];
			$data[$k]['store_lng'] = $v['store_lng'];
			$data[$k]['store_superpose'] = $this->getStoreOverlap($v['store_superpose']);		
			$data[$k]['store_yunli'] = $this->getStoreDeliver(array('store_lat'=>$v['store_lat'],'store_lng'=> $v['store_lng']));
			$data[$k]['store_color'] = $color[$k];				 
			
			
			$feature[$k]['type'] = 'Feature';
			$feature[$k]['properties']['iconSize'] = array(40,40);
			$feature[$k]['properties']['name'] = $v['store_name_primary'];			
			
			
			$feature[$k]['geometry']['type'] = 'Point';
			
			$feature[$k]['geometry']['coordinates'][0] = $v['store_lng'];			
			$feature[$k]['geometry']['coordinates'][1] = $v['store_lat'];			
			
			
		}
		
		$store_list['type'] = 'FeatureCollection';
		$store_list['features'] = $feature;
		
		
		output_data(array('store'=>$data,'store_list' => $store_list));
	//	exit(json_encode(array('store'=>$data)));
	
	}
	
	//叠加商户名称 
	private function getStoreOverlap($store_id){
		
		if(empty($store_id)){
			return '';
		}
				
		$where = array(
			'store_id' => array('in',$store_id)
		);
		
		$store = model('store')->getStoreList($where);		
		
		$store_name = array();
		
		foreach($store as $v){
			
			$store_name[] = $v['store_name_primary'];
			
		}
				
		return implode(',',$store_name);
		
	}
	
	
		
	
	public function huanyuanOp(){
			//删除排队
			model('order_polling')->delPolling(TRUE);
			//删除
			model('polling_refuse')->delRefuse(TRUE);
			$data = array(
				'store_id' => 0,
				'store_name' => '',
				'store_phone' => '',
				'store_address' => '',
				'store_region_id' => '',
				'store_region_sm_id' => '',
				'store_coordinate' => '',
				'daycode' => '',
				'distributor_id' => '',
				'distributor_name' => '',
				'distributor_mobile' => '',
				'order_state'=> 10,
				'order_polling' => 0
			);	
			model('order')->editOrder($data,TRUE);
			
			
			$data = array(
				'distributor_online' => 10
			);
			model('waiter')->editWaiter(TRUE,$data);
			
			
			model('stoket')->delStoket(TRUE);			
			output_data('ok');
		
	}
	
	
	
	//新增一笔订单
	public function addorderOp(){	
				
		$where = array(
			'store_id' => 0
		);
		$order = model('order')->getOrderInfoN($where,'*','order_id asc');
		if($order){		
		
			$store_info = model('store')->getStoreInfo(array('store_id'=> $_POST['store_id']));					
			$data= array();
			$data['store_id'] = $store_info['store_id'];
            $data['store_name'] = $store_info['store_name_primary'];
			$data['store_phone'] = $store_info['store_phone'];
			$data['store_address'] = $store_info['store_address'];
			$data['store_region_id'] = $store_info['region_id'];
			$data['store_region_sm_id'] = $store_info['region_sm_id'];			
		    $data['store_coordinate'] = $store_info['store_lng'].','.$store_info['store_lat'];	
			$data['store_superpose'] = $store_info['store_superpose'];
			$data['order_state']	 = 20;
			$data['daycode'] = time();
			
			$data['add_time'] = time(); //$this->getMillisecond();
					   
		    $where = array(
			   	'order_id' => $order['order_id']
		    );		
					   
		   $row = model('order')->editOrder($data,$where);		
		   if($row){
			   
			   $log = array(
			   		'order_sn' => $order['order_sn'],
					'log_msg' => $store_info['store_name'].'有新订单:'.$order['order_sn'],
					'log_time' => time()
			   );
			   model('stoket_log')->addSlog($log);
			  
			   //锁定运力。
			  // $this->lockWaiter($order['order_sn']);
				
			   //执行推送
				$this->sendOrder($order['order_sn']);
			   
						   
			   	output_data('ok');
			   
			}else{
				 output_error('订单添加失败');
		    }
			
		}		
		
		
	}
	
	
	
	
	//获取商家周边范围的可用配送员
	private function sendOrderDeliver($store){
			$where = array();
			$where['distributor_status'] = 1;
			$where['distributor_online'] = 20;
			$where['delivery_state'] = 0;
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			foreach($deliver_data as $v){
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
				//顶级排序
				if($distance < 5000){	
					$deliver[] = array(
						'distributor_id' => $v['distributor_id'],
						'distributor_name' => $v['distributor_name']
					);					
				}
				
			}
			
			return $deliver;	
	
	}
	
	
	
	//生成订单推送排序
	private function sendOrder($order_sn = ''){		
		$stoket =  model('stoket');			
		$order_info = model('order')->getOrderInfo(array('order_sn'=>$order_sn));
		if(empty($order_info)){
			output_error('订单不存在');
		}
		
		$delivery = $this->sendOrderDeliver(array('store_lat'=>$order_info['store_lat'],'store_lng'=> $order_info['store_lng']));		
		
		foreach($delivery as $v){
			
				
				$deliver_sort = $this->deliverySort($v['distributor_id'],$order);				
				
				
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$order_info['store_id']);
				
				//echo '单量'.$count;				
				//检查当前 配送员 是否有单			
				//没有订单，则分配个商户围栏内的配送员			
				//如果有一单，检查是否为本商家的订单，			
				//如果有一单，检查是否为叠加范围内商户，并判断他是否在叠加商户记录内。	
				//检查本单是否与上一单超过6分钟。			
				$diffTime = $this->checkDiffTime($v['distributor_id'],$order_info);
							
				
				//检查是否已拒绝过本单	
				$refuse = $this->checkRefuse($order_info['order_sn'],$v['distributor_id']);
				//echo '拒绝'.$refuse;
					
				
				//检查本单是否为列队等待单
				
			/*	echo $count;
				echo '|';
				echo $refuse;
				echo '|';						
				echo $overlying;
				echo '|';
				echo $diffTime;
				echo '|';			
				*/
				
				
				if($refuse == 0  && ($superposition == 1 && $diffTime == 0)){		
					$polling_data[] = array(
						'order_id' => $order_info['order_id'],
						'order_sn' => $order_info['order_sn'],
						'waiter_id' => $v['distributor_id'],
						'state' => 0,
						'addtime' => time(),	
						'last_time' => $this->deliver_last_time($count ,$v['distributor_id']),
						'order_sort' => $deliver_sort
					);					
					//写入lOG
					$log = array(
						'order_sn' => $order_info['order_sn'],
						'log_msg' => '轮询：'.$v['distributor_name'].'，单号：'.$order_info['order_sn'],
						'log_time' => time()
					);
					
					model('stoket_log')->addSlog($log);
					
				}			
			}
			
			$row = model('order_polling')->addPollingAll($polling_data);
			if(count($polling_data) > 0){
			//如无可用运力。直接到订单池
				$data = array(
					'order_polling' => 1
				);	
			}else{
				$data = array(
					'order_polling' => 2
				);	
			}
			$where = array(
				'order_sn' => $order_info['order_sn']
			);
			model('order')->editOrder($data,$where);			
			

			if($row){	
				//开始轮询
				$stoket->send($order_info['order_sn']);	
			}
	}
	
	
	
	
	
	
		//获取等待时间最长下单时间最久的那个
	private function deliver_last_time($count,$waiter_id){
		
		if($count > 0){		
			//获取下单时间最久的
			$where = array(
				'distributor_id' => $waiter_id,
				'order_state' => array('in','30,40')
			);
			$row = model('order')->getOrderInfoN($where,'*','add_time asc');
			return $row['add_time'];
						
		}else{
			
			//获取上线时间
			$where = array(
				'distributor_id' => $waiter_id,
				'stoket_state' => 0
			);
			$row = model('stoket')->getStoketInfo($where);
			return $row['updatetime'];
			
		}		
	}
	
	
	//检查此订单是否被该用户拒绝过
	private function checkRefuse($order_sn,$waiter_id){
		
		$where = array(
			'order_sn' => $order_sn,
			'waiter_id' => $waiter_id
		);
		$count = model('polling_refuse')->getRefuseCount($where);
		return $count;
	}
	
	
	
	
	
	
	//拖拽坐标更新配送员的新位置
	public function editWaiterPointOp(){

			$where = array(
				'distributor_id' => $_POST['waiter_id']
			);
			
			$lng = $_POST['lng'];
			$lat = $_POST['lat'];
			
			$data = array(
				'region_coordinate' => $lng.','.$lat
			);
			
			$row = model('waiter')->editWaiter($where,$data);
			
			if($row){
				output_data('ok');			
			}else{
				output_error('更新失败');
			}
		
	}
	
	
	
	
	
	private function getDistance($lat1, $lng1, $lat2, $lng2) {
        $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }


}


?>