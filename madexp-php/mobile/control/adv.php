<?php
/**
 * 新增
 * 
 **/

defined('InMadExpress') or exit('Access Invalid!');
class advControl extends mobileHomeControl{

	public function __construct() {
        parent::__construct();
    }
  
    
    
	public function adv_showOp(){
		
		$where = array();
	//	$where['adv_id'] = $_POST['adv_id'];
		$row = model('adv')->getOneById($_POST['adv_id']);
	    $text = unserialize($row['adv_text']);
	    $lang = $_POST['lang'];
	  
	    if($lang == 'ENG'){
	        $row['adv_text'] = $text['ENG']['text'];
	    }elseif($lang == 'CHN-T'){
	        $row['adv_text']  = $text['CHN-T']['text'];
	    }elseif($lang == 'CHN-S'){
	        $row['adv_text']  = $text['CHN-S']['text'];
	    }else{
	        $row['adv_text'] = $text['ENG']['text'];
	    }
	    
	    
	    $row['lang'] = model('language')->getLangList(array('language_system' => 1));
        
        
		
		output_data($row);
		
		
		
	}
  
  
  
  
	public function indexxOp(){
			
			
		$order_id = $_POST['order_id'];
		$invoice = $_POST['invoice'];
			
		$model = model('pdf');		
		$where = array(
			'order_id' => $order_id
		);
		$order = model('order')->getOrderInfo($where,array('store','order_goods','delivery'));
		
		$goods = $order['extend_order_goods'];
		$goods_list = array();
		foreach($goods as $k => $v){			
			$where = array(
				'goods_id' => $v['goods_id'],
				'lang_name' => 'ENG'
			);			
			$lang = model('goods_language')->getGoodsLangInfo($where);
			
			$v['goods_name'] = $lang['goods_name'];
			$v['unit_price'] = ncPriceFormat($v['goods_price'] * 0.9);
			$v['unit_gst'] = ncPriceFormat($v['goods_price'] * 0.1);
						
			$goods_list[$k] = $v;			
			
		}		
	
		$order['eng_date'] = date('d M Y',$order['add_time']);		
		$order['goods_list'] = $goods_list;	
		
		$row = $model->createPdf($order,$invoice);	
		
		output_data(array('url' => $row));
		
	}
  
  
  
  
  
  
  
  
  
    
}
