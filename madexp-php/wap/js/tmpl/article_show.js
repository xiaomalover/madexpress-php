$(function(){
	var adv_id = getQueryString('adv_id')	;
	var lang =  getQueryString('lang')	;
	
	if (adv_id=='') {
    	alert('信息不存在');
	}
	else {
		$.ajax({
			url:ApiUrl+"/index.php?act=adv&op=adv_show",
			type:'POST',
			data:{
			    adv_id:adv_id,
			    lang:lang
			},			
			dataType:'json',
			success:function(result){
				var data = result.datas;				
				$("#article-content").html(data.adv_text);
			}
		});
	}	
});