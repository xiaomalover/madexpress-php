<?php
/**
 * 任务计划 - 秒钟执行的任务
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class secondControl extends BaseCronControl {

	/**
	 * 默认方法
	 */
	public function indexOp() {
		
		//第一单超过6分钟后推送开始取餐	
	//	$this->_start_delivery();
		
		//商家超过120秒没接单自动退款
	//	$this->_store_refund();
		
		//客户端推送订单
		$this->_send_order();
		
		//问题单，异常单		
	//	$this->_order_problem();
		
		
		//可疑单
	//	$this->_order_attention();
		
		
		//到店异常
	//	$this->_order_delivery_store();
		
		//更新stripe 扣费
		//$this->_order_stripe_fee();
		
	}

	//处理问题单和异常单
	/*
	CA.Merchant 	到店时间异常 2分钟
	Delivery Time 	配送时间异常 2分钟

	*/
   
   //到店问题单和异常单
   private function _order_delivery_store(){
	$record 		= model('order_record');   
	$order_model 	= model('order');
	$where = array();
	$where['order_state'] = '40';
	$order_list = model('order')->getOrderListNew($where);
	$time = time();
	foreach($order_list as $v){
		$new_time =  $time - $v['distributor_store_time'];
		if($new_time > 120 && $new_time < 300){ //到店异常

			if($v['is_problem'] == 0 || $v['is_problem'] == 2){

				$data_record = array();
				$data_record['order_id'] = $v['order_id'];
				$data_record['store_taking_time'] = time();
				$data_record['store_taking_type'] = 'CA.Merchant';
				$record->addRecord('attention',$data_record);
									
				$data_order = array();
				$data_order['is_problem'] = 2;										
				$order_model->editOrder($data_order,array('order_id' => $v['order_id']));

			}
		}
	}
	
	
	
	
	
	
	
	
	
		
   }
   
   
   
   
	private function _order_attention(){
		
		
		$order_model 	= model('order');
		$delivery_model = model('waiter');
		$record 		= model('order_record');
		$store			= model('store');
	   //可疑单
	   //判断到店异常	   
	   
	   //判断配送时间异常
	   
	   
	   //配送员Socket链接异常 超时1分钟	   
	   $delivery_list = $delivery_model->getWaiterList(array('stocket_error' => 1));
	   foreach($delivery_list as $v){		   
		   $last_time = $v['stocket_last_time'];
		   $time = time();
		
		   //计算当前配送员掉线多久。大于1分钟 小于5分钟，上报可疑单。
		   $disparity_time =  $time - $last_time;





		   if($disparity_time > 60 && $disparity_time < 300){
			   
			   //获取订单列表。 
				$order_list = $order_model->getOrderListNew(array('distributor_id' => $v['distributor_id'],'order_state' =>array('in','20,30,40,50')));
				foreach($order_list as $ol){	
					
					if($ol['is_problem'] == 0 || $ol['is_problem'] == 2){					
						$data_record = array();
						$data_record['order_id'] = $ol['order_id'];
						$data_record['store_taking_time'] = time();
						$data_record['store_taking_type'] = 'Courier Socket';
						$record->addRecord('attention',$data_record);
											

						



						$data_order = array();
						$data_order['is_problem'] = 2;										
						$order_model->editOrder($data_order,array('order_id' => $ol['order_id']));
					}					
				}			  
			   
			
		   }		   
	   }
	   //商家掉线
	   //打单机Socket链接异常
	   
	   $store_list = $store->getStoreList(array('stocket_error' => 1));	   
	   foreach($store_list as $v ){		   
		    $last_time = $v['stocket_last_time'];
		    $time = time();
		    		
		    //计算当前配送员掉线多久。大于1分钟 小于5分钟，上报可疑单。
		    $disparity_time =  $time - $last_time;
		    if($disparity_time > 60 && $disparity_time < 300){				
				$order_list = $order_model->getOrderListNew(array('store_id' => $v['store_id'],'order_state' =>array('in','30,40,50')));
				foreach($order_list as $ol){					
					if($ol['is_problem'] == 0 || $ol['is_problem'] == 2){					
						$data_record = array();
						$data_record['order_id'] = $ol['order_id'];
						$data_record['store_taking_time'] = time();
						$data_record['store_taking_type'] = 'Merchant Socket';
						$record->addRecord('attention',$data_record);
																							
						$data_order = array();
						$data_order['is_problem'] = 2;										
						$order_model->editOrder($data_order,array('order_id' => $ol['order_id']));
					}					
				}
			}		   
	   }
	   	   
	
	   
	}
   
   
	private function _order_problem(){		
				
		//问题单		
		//Courier Socket T.O 配送员Socket链接断开超时 5分钟				
		//Merchant Socket T.O 打单机Socket链接断开超时 5分钟
		
		$order_model 	= model('order');
		$delivery_model = model('waiter');
		$record 		= model('order_record');
		$store			= model('store');
	
		
		
		//配送员Socket链接异常 超时1分钟	   
		$delivery_list = $delivery_model->getWaiterList(array('stocket_error' => 1));
		foreach($delivery_list as $v){		   
				   $last_time = $v['stocket_last_time'];
				   //计算当前配送员掉线多久。大于1分钟 小于5分钟，上报可疑单。
				   $time = time();
				   $disparity_time = $time - $last_time;
				   if($disparity_time >= 300){
					   
					   //获取订单列表。 
						$order_list = $order_model->getOrderListNew(array('distributor_id' => $v['distributor_id'],'order_state' =>array('in','20,30,40,50')));
						foreach($order_list as $ol){	
							
							if($ol['is_problem'] != 1 ){						
								$data_record = array();
								$data_record['order_id'] = $ol['order_id'];
								$data_record['action_required_time'] = time();
								$data_record['action_required_type'] = 'Courier Socket T.O';
								$record->addRecord('action_required',$data_record);
													
								$data_order = array();
								$data_order['is_problem'] =1;										
								$order_model->editOrder($data_order,array('order_id' => $ol['order_id']));
							}			
						}			  
					   
					   //配送员账号下线					   
					   $delivery_model->editWaiter(['distributor_id' => $v['distributor_id']],['is_problem' => 1]);
					   					   					   
					
				   }		   
		}
		//商家掉线
		//打单机Socket链接异常
		
		$store_list = $store->getStoreList(array('stocket_error' => 1));	   
		foreach($store_list as $v ){		   
				    $last_time = $v['stocket_last_time'];
				    $disparity_time = time() - $last_time;
				    if($disparity_time >= 300){				
						$order_list = $order_model->getOrderListNew(array('store_id' => $v['store_id'],'order_state' =>array('in','30,40,50')));
						foreach($order_list as $ol){					
								if($ol['is_problem'] != 1 ){									
									$data_record = array();
									$data_record['order_id'] = $ol['order_id'];
									$data_record['action_required_time'] = time();
									$data_record['action_required_type'] = 'Merchant Socket T.O';
									$record->addRecord('action_required',$data_record);
										
									$data_order = array();
									$data_order['is_problem'] = 1;										
									$order_model->editOrder($data_order,array('order_id' => $ol['order_id']));
								}			
						}
					}		   
		}
			   
		
	}


	
	//没有接满3单，第一单超过6分钟后推送开始取餐
	private function _start_delivery(){
		
			$delivery_model = model('waiter');
			$delivery_list = $delivery_model->getWaiterList(['delivery_state' => 0 , 'order_num' => array('in','1,2')]);
			foreach($delivery_list as $info){
				if (!empty($info)) {
					$old_time = $info['order_first_time'];
					$new_time = time();
					$diff = $new_time - $old_time;
					if ($diff > 360) {
						//修改配送员为取餐状态						
						$delivery_model->editWaiter(['distributor_id' => $info['distributor_id']],['delivery_state' => 1]);

					}
				} 

			}		
		
	}
	

	
	
	//商家超过120秒没接单自动退款
	private function _store_refund(){		
		$model_stoket = model('stoket');
		$model = model('order');
		
		$pdf_model = model('pdf');
		$model = model('order');
		$wallet = model('predeposit');
		
		$time = time();				
		//
		$refund_amount = 0;
		
		$order_list  = $model->getOrderListNew(array('order_state' => 30));
		foreach($order_list as $v){
			
			if($v['socket_store_send_time'] + 320 < time()){				
				//执行退款				
				$data = array(
					'order_state' => 0,
					'is_problem'  => 1					
				);				
				$where = array(
					'order_id' => $v['order_id']
				);				
				$row = $model->editOrder($data,$where);				
				if($row){					
										
					//生成退款记录单
					if($v['pd_amount'] > 0){
						//执行退款退回				
						$data = array();
						$data['amount'] = $v['pd_amount'] ;
						$data['order_sn'] = $v['order_sn'];
						$data['order_id'] = $v['order_id'];
						$data['member_id'] = $v['store_id'];
						$data['member_name'] = $v['store_name'];
						$wallet->changePd('order_refund',$data);										
					}	
					
					if($v['pay_amount'] > 0){
						
						
					}
					
										
					//生成退款记录单
					$refund_recode_data = array();
					$refund_recode_data['order_id'] = $v['order_id'];
					$refund_recode_data['refund_amount'] = $v['pay_order_amount'];
					$refund_recode_data['refund_payment_amount'] = $v['pay_amount'];
					$refund_recode_data['refund_wallet_amount'] = $v['pd_amount'];
					$voucher_re = 'No';
					if($v['store_coupon_id'] > 0 || $v['platform_coupon_id'] > 0){
						$voucher_re = 'Yes';
					}
					$refund_recode_data['voucher_re'] = $voucher_re ;		
					$refund_amount +=  $v['order_amount'];		
					$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
					
					
										
					//写入配送员接单
					$data_record = array();
					$data_record['order_id'] 			  = $v['order_id'];
					$record_data['order_cancel_time']     =  time(); //
					$record_data['order_cancel_content']  = '商家倒计时结束自动取消订单';//
					$record_data['role']    			  = 'Merchant'; //
					model('order_record')->addRecord('order_cancel',$data_record);
					
					
					//写入配送员接单
					$data_record = array();
					$data_record['order_id'] 			  = $v['order_id'];
					$record_data['action_required_type']     =  time(); //
					$record_data['action_required_time']  = 'Merchant PTO';//			
					model('order_record')->addRecord('action_required',$data_record);
					
										
			   
					//检查骑手是否还有订单。如果没有订单。就要求返回钱圈。
					$deliver_order_count = $this->deliverOrderCount($v['distributor_id']);
					if($deliver_order_count <= 0){
												
												
											
						
					}
					
					//推送已取消的信息给骑手
					$data = array();
					$data['type'] = 'order_cancel';
					$data['data'] = $v;
					//print_r($order_info);
					$data = json_encode($data);	
					$model_stoket->sendDelivery($v['distributor_id'],$data);
					
				}
				
			}			
		}
		
	}

	
	/**
	 * @param {Object} $id
	 */
	private function deliverOrderCount($id)
	{
	    $count = 0;
	    $where = array(
	        'distributor_id' =>$id,
	        'order_state' => array('in','30,40,50')
	    );
	    $count = model('order')->getOrderCount($where);
	    return $count;
	}


	
	//客户端推送订单,单次执行100单
	private function _send_order(){		
		$order_model = model('order');
		$allociton = model('order_allocation');		
		$order_list = $order_model->getOrderListNew(array(
														'order_polling'=>array('in','0,1,2'),
														'order_state'=> 20,'order_polling_show'=> 0
														),
		'order_id,order_sn,store_id','order_id asc',100);
		print_r($order_list);
		foreach($order_list as $v){			
			model('order_polling')->delPolling(array('order_sn' =>$v['order_sn']));			
			$allociton->sendDeliveryOrder($v['order_sn'],0);	
		}
	}

	
	//更新订单的stripe扣费信息
	private function _order_stripe_fee(){
		
		$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');	
		
		$order_model = model('order');
		$order_list = $order_model->getOrderListNew(['order_state'=> ['in'=> '20,30']],'order_id,order_sn,store_id','order_id asc',10);
		foreach($order_list as $v){			
			$data = $stripe->balanceTransactions->retrieve(
			  'txn_3JQa74B39zEGewFt1yjDjODk',
			  []
			);			
			$order_model->editOrder(['extxnfee_stripe' => $data['fee']],array('order_id'=>$order_info['order_id']));			
		}
		
	}
	
	



}