<?php
/**
 * 任务计划 - 分钟执行的任务
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class minutesControl extends BaseCronControl {

    /**
     * 默认方法
     */
    public function indexOp() {
 
	
		$this->_delivery_socket();
		$this->_sheduling_rest();
		//$this->_sheduling_invoice();
	//	$this->_order_delivery_timeout_cancel();
		$this->_order_store_timeout_cancel();
		
		//商家营业时间打烊计划任务
		$this->_store_online();
		
		//验证排班情况
		$this->_check_scheduling();
    }


	/*
	操作商家营业打烊每分钟执行一次	
	*/
	private function _store_online(){
		
		$store = model('store');
		$where = array();
		$where['store_state'] = 1;
		$store_list =$store->getStoreList($where);		
		foreach($store_list as $store){			
			$store_time = unserialize($store['store_shophours']);
			$week = strtolower(date('D'));	
			$time = time();
			$data = array();
			foreach($store_time as $v){
			    if($week == $v['code']){
			        if($v['t1']['show'] == 1){			        
			    		$data['t1']['stime'] = $v['t1']['stime'];
			    		$data['t1']['etime'] = $v['t1']['etime'];							
			        }else{
			    		$data['time'][] ='close';
			    	}	
			        if($v['t2']['show'] == 1){						
			    		$data['t2']['stime'] = $v['t2']['stime'];
			    		$data['t2']['etime'] = $v['t2']['etime'];	
			        }else{
			    		$data['time'][] ='close';						
			    	}			        
			    }	    
			}			
			$online = 0;
			if($data){		
				$online = $this->getCurrentTimeSection($data['t1']['stime'],$data['t1']['etime']);
				$online += $this->getCurrentTimeSection($data['t2']['stime'],$data['t2']['etime']);		
			}						
			
			
			//修改当前营业店铺的状态
			$update = array();
			$update['store_online'] = $online;
			$where = array();
			$where['store_id'] = $store['store_id'];
			model('store')->editStore($update,$where);
			
		}	
	}
	
	//查询当前时间是否在设置的营业时间段内。
	private function getCurrentTimeSection($beginTime,$endTime)
	{
	    $checkDayStr = date('Y-m-d ',time());
	    $timeBegin1 = strtotime($checkDayStr."$beginTime".":00");
	    $timeEnd1 = strtotime($checkDayStr."$endTime".":00");	    
	    $curr_time = time();	    
	    if($curr_time >= $timeBegin1 && $curr_time <= $timeEnd1)
	    {
	        return 1;
	    }	     
	    return 0;
	}

	//检查排班情况。
	private function _check_scheduling(){
		
		$where = array();
		$where['scheduling_start_time'] = array('lt',time());
		$where['scheduling_status'] = 10;
	
		$data = array();
		$data['scheduling_status'] = 50;
		
		model('scheduling')->editScheduling($data,$where);
		
	}

	
	


	
	//计算配送员socket 断线时长。
	private function _delivery_socket(){
		
		
		
	}
	
	
	/*
	执行休息记录检查
	*/	
	private function _sheduling_rest(){
		
		
	}
	
	/*
	执行定期生成票据
	*/	
	private function _invoice(){
		
		
		
		
	}
	
	
	
	
	
	/*
	* 执行配送员倒计时后不接订单的操作[弃用]
	
	private function _order_delivery_timeout_cancel(){		
		$_break = false;
		    $model_order = Model('order');
		    $logic_order = Logic('order');	 
		    $condition = array();
		    $condition['order_state'] = 20; //配送员已接		 
		    $condition['socket_send_time'] = array('lt',TIMESTAMP - 10);
		  
		//	print_r(date('Y-m-d H:i:s',TIMESTAMP - 120));
		//	print_r(date('Y-m-d H:i:s','1601547756'));	  
		//	print_r($condition);
					
			//分批，每批处理100个订单，最多处理5W个订单
		    for ($i = 0; $i < 500; $i++){			
		        if ($_break) {
					break;
				}			
		        $order_list = $model_order->getOrderList($condition, '', '*', '', 100);			
				//print_r($order_list);
				if (empty($order_list)) break;
		        foreach ($order_list as $order_info) {							
					$result = $logic_order->changeOrderStateRefuse($order_info);
				//	print_r($result);
					if (!$result['state']) {
					     $this->log('配送员未接单，系统自动分配给其他人:'.$order_info['order_sn']); $_break = true;  break;
					}			
		        }
		    }		
	}
	*/
   
   
	/**
	 * 执行商家倒计时后不接订单自动退款
	 */
	private function _order_store_timeout_cancel() {
	  
	    $_break = false;
	    $model_order = Model('order');
	    $logic_order = Logic('order');	 
	    $condition = array();
	    $condition['order_state'] = 30; //配送员已接
	    $condition['is_delay'] = 0; //未点击且慢	
	    $condition['socket_store_send_time'] = array('lt',TIMESTAMP - 120);
	  
	//	print_r(date('Y-m-d H:i:s',TIMESTAMP - 120));
	//	print_r(date('Y-m-d H:i:s','1601547756'));	  
	//	print_r($condition);		
		
		//分批，每批处理100个订单，最多处理5W个订单
	    for ($i = 0; $i < 500; $i++){			
	        if ($_break) {
				break;
			}			
	        $order_list = $model_order->getOrderList($condition, '', '*', '', 100);			
			//print_r($order_list);
			if (empty($order_list)) break;
	        foreach ($order_list as $order_info) {							
				$result = $logic_order->changeOrderStateCancel($order_info,'system','系统','商家未接单，系统自动退单',true,array('order_state'=>30));
			//	print_r($result);
				if (!$result['state']) {
				     $this->log('商家未接单，系统自动给用户退款:'.$order_info['order_sn']); $_break = true;  break;
				}			
	        }
	    }
		
	
	    
	}






	//租车不付款超时后自动过期
	/*private function vehicleLeaseNoPay(){
		
		$model = model('vehicle_lease');	
		$where = array();
		$where['lease_state'] = 0;
			
	
		$time = time();		
		$list = $model->getLeaseList($where);
		foreach($list as $v){			
			if(($v['payment_time'] + 600) < time()){				
				$data = array();
				$data['lease_state'] = 70;	
				$where = array(											
				);
				$model->editLease($data,$where);
			}
		}	
	}

	//租车付款后未确认的自动过期
	private function vehicleLeaseYesPay(){
		
		
		$model = model('vehicle_lease');	
		$where = array();
		$where['lease_state'] = 20;
		$where['payment_state'] = 1;
		
		$time = time();		
		$list = $model->getLeaseList($where);
		
		
		foreach($list as $v){
			
			if(($v['payment_time'] + 600) < time()){			
				
					if($v['payment_code'] == 'predeposit'){ //余额退款				

								$model_predeposit = Model('waiter_predeposit');
								try {
									
									$this->beginTransaction();
									$state = true;
									$order_amount = $v['lease_first_amount'];//订单金额					
									$predeposit_amount = $order_amount;
									$detail_array = array();					
									
									if ($predeposit_amount > 0) {
										
										$log_array = array();
										$log_array['waiter_id'] = $v['distributor_id'];
										$log_array['waiter_name'] = $v['distributor_name'];
										$log_array['order_sn'] = $v['order_sn'];
										$log_array['amount'] = $order_amount;								
										if ($log_array['amount'] > 0) {											
											$state = $model_predeposit->changePd('order_lease_refund', $log_array);		
										}
										
									}
									
								
										$data = array();
										$data['lease_state'] = 70;	
										
										$where = array(
											
										);
										$model->editLease($data,$where);
																
								
								
																		
									$this->commit();
									return $state;
								} catch (Exception $e) {
									$this->rollback();
									return false;
								}

				
			}
				
				
				
			}	
				
			
		}
		
				
	}

	*/




    /**
     * 发送邮件消息
     */
    private function _cron_mail_send() {
        //每次发送数量
        $_num = 50;
        $model_storemsgcron = Model('mail_cron');
        $cron_array = $model_storemsgcron->getMailCronList(array(), $_num);
        if (!empty($cron_array)) {
            $email = new Email();
            $mail_array = array();
            foreach ($cron_array as $val) {
                $return = $email->send_sys_email($val['mail'],$val['subject'],$val['contnet']);
                if ($return) {
                    // 记录需要删除的id
                    $mail_array[] = $val['mail_id'];
                }
            }
            // 删除已发送的记录
            $model_storemsgcron->delMailCron(array('mail_id' => array('in', $mail_array)));
        }
    }





    /**
     * 执行通用任务
     */
    private function _cron_common(){

        //查找待执行任务
        $model_cron = Model('cron');
        $cron = $model_cron->getCronList(array('exetime'=>array('elt',TIMESTAMP)));
        if (!is_array($cron)) return ;
        $cron_array = array(); $cronid = array();
        foreach ($cron as $v) {
            $cron_array[$v['type']][$v['exeid']] = $v;
        }
        foreach ($cron_array as $k=>$v) {
            // 如果方法不存是，直接删除id
            if (!method_exists($this,'_cron_'.$k)) {
                $tmp = current($v);
                $cronid[] = $tmp['id'];continue;
            }
            $result = call_user_func_array(array($this,'_cron_'.$k),array($v));
            if (is_array($result)){
                $cronid = array_merge($cronid,$result);
            }
        }
        //删除执行完成的cron信息
        if (!empty($cronid) && is_array($cronid)){
            $model_cron->delCron(array('id'=>array('in',$cronid)));
        }
    }

    /**
     * 上架
     *
     * @param array $cron
     */
    private function _cron_1($cron = array()){
        $condition = array('goods_commonid' => array('in',array_keys($cron)));
        $update = Model('goods')->editProducesOnline($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 根据商品id更新商品促销价格
     *
     * @param array $cron
     */
    private function _cron_2($cron = array()){
        $condition = array('goods_id' => array('in',array_keys($cron)));
        $update = Model('goods')->editGoodsPromotionPrice($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 优惠套装过期
     *
     * @param array $cron
     */
    private function _cron_3($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_bundling')->editBundlingQuotaClose($condition);
        if ($update) {
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 推荐展位过期
     *
     * @param array $cron
     */
    private function _cron_4($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_booth')->editBoothClose($condition);
        if ($update) {
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 抢购开始更新商品促销价格
     *
     * @param array $cron
     */
    private function _cron_5($cron = array()) {
        $condition = array();
        $condition['goods_commonid'] = array('in', array_keys($cron));
        $condition['start_time'] = array('lt', TIMESTAMP);
        $condition['end_time'] = array('gt', TIMESTAMP);
        $groupbuy = Model('groupbuy')->getGroupbuyList($condition);
        foreach ($groupbuy as $val) {
            Model('goods')->editGoods(array('goods_promotion_price' => $val['groupbuy_price'], 'goods_promotion_type' => 1), array('goods_commonid' => $val['goods_commonid']));
        }
        //返回执行成功的cronid
        $cronid = array();
        foreach ($cron as $v) {
            $cronid[] = $v['id'];
        }
        return $cronid;
    }

    /**
     * 抢购过期
     *
     * @param array $cron
     */
    private function _cron_6($cron = array()) {
        $condition = array('goods_commonid' => array('in', array_keys($cron)));
        //抢购活动过期
        $update = Model('groupbuy')->editExpireGroupbuy($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 限时折扣过期
     *
     * @param array $cron
     */
    private function _cron_7($cron = array()) {
        $condition = array('xianshi_id' => array('in', array_keys($cron)));
        //限时折扣过期
        $update = Model('p_xianshi')->editExpireXianshi($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }

    /**
     * 加价购过期
     *
     * @param array $cron
     */
    private function _cron_8($cron = array()) {
        $condition = array('id' => array('in', array_keys($cron)));
        // 过期
        $update = Model('p_cou')->editExpireCou($condition);
        if ($update){
            // 返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        } else {
            return false;
        }
        return $cronid;
    }

    /**
     * 更新店铺（新增）商品消费者保障服务开启状态（如果商品在店铺开启保障服务之后增加则需要执行该任务更新其服务状态）
     * @param array $cron
     */
    private function _cron_9($cron = array()) {
        //查询商品详情
        $model_goods = Model('goods');
        $where = array();
        $where['goods_commonid'] =  array('in', array_keys($cron));
        $goods_list = $model_goods->getGoodsList($where, 'goods_id,goods_commonid,store_id');
        $cronid = array();
		if (!$goods_list) {
            // 返回执行成功的cronid
            foreach ($cron as $k=>$v) {
                $cronid[] = $v['id'];
            }
            return $cronid;
        }
        $store_goods_list = array();
        foreach($goods_list as $k=>$v){
            $store_goods_list[$v['store_id']][$v['goods_id']] = $v;
        }
        //查询店铺的保障服务
        $where = array();
        $where['ct_storeid'] = array('in', array_keys($store_goods_list));
        $model_contract = Model('contract');
        $c_list = $model_contract->getContractList($where);
		if (!$c_list) {
            foreach ($cron as $k=>$v) {
				$cronid[] = $v['id'];
            }
            return $cronid;
        }
        $goods_contractstate_arr = $model_contract->getGoodsContractState();
        $c_list_tmp = array();
        foreach ($c_list as $k=>$v) {
            if ($v['ct_joinstate_key'] == 'added' && $v['ct_closestate_key'] == 'open') {
                $c_list_tmp[$v['ct_storeid']][$v['ct_itemid']] = $goods_contractstate_arr['open']['sign'];
            }else{
                $c_list_tmp[$v['ct_storeid']][$v['ct_itemid']] = $goods_contractstate_arr['close']['sign'];
            }
        }

        //整理更新数据
        $goods_commonidarr = array();
        foreach ($c_list_tmp as $s_k=>$s_v) {
            $update_arr = array();
            foreach ($s_v as $item_k=>$item_v) {
                $update_arr["contract_$item_k"] = $item_v;
            }
            $result = $model_goods->editGoodsById($update_arr, array_keys($store_goods_list[$s_k]));
            if ($result){
                foreach ($store_goods_list[$s_k] as $g_k=>$g_v) {
                    $goods_commonidarr[] = $g_v['goods_commonid'];
                }
                array_unique($goods_commonidarr);
            }
        }

        if ($goods_commonidarr){
            // 返回执行成功的cronid
            foreach ($cron as $k=>$v) {
                if (in_array($k, $goods_commonidarr)) {
                    $cronid[] = $v['id'];
                }
            }
        }
        if ($cronid){
            // 返回执行成功的cronid
            return $cronid;
        } else {
            return false;
        }
    }

    /**
     * 手机专享过期
     *
     * @param array $cron
     */
    private function _cron_10($cron = array()) {
        $condition = array('store_id' => array('in', array_keys($cron)));
        $update = Model('p_sole')->editSoleClose($condition);
        if ($update){
            //返回执行成功的cronid
            $cronid = array();
            foreach ($cron as $v) {
                $cronid[] = $v['id'];
            }
        }else{
            return false;
        }
        return $cronid;
    }
}
