<?php
/**
 * 任务计划 - 天执行的任务
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class dateControl extends BaseCronControl {

   
  
	public function indexOp() {	
		$this->_finance_stat();
	}
	

	//
	private function me_invoice_store(){


		


	}

	private function me_invoice_delivery(){




	}


	private function _finance_stat(){
				
				
		$model = model('finance_stat');
		$date = date('Ymd',time());		
		$data = array();
		$data['stripe_amount'] = $this->getStripeAmount();
		$data['store_amount'] = $this->getStoreAmount();
		$data['user_amount'] = $this->getUserAmount();
		$data['delivery_amount'] = $this->getDeliveryAmount();
		$data['addtime'] = time();
		$data['datetime'] = $date;	
		
		$info = $model->getStatInfo(['datetime' => $date]);
		if(!empty($info)){
			$model->editStat($data,['datetime' => $date]);			
		}else{
			$model->addStat($data);			
		}		
	}

	private function getStripeAmount(){		
		$stripe_url = BASE_PATH.DS.'api'.DS.'stripe-php/init.php';
		require($stripe_url);		
		$stripe = new \Stripe\StripeClient('sk_test_5o8WQAy0CXxPAafJ1DacPVsv00rDCbVkvc');				
		$data = $stripe->balance->retrieve();		
		$amount = $data['available'][0]['amount'] / 100;		
		return $amount;		
		
	}
	
	private function getStoreAmount(){
	
		$store = model('store')->getStoreInfo(TRUE,'SUM(available_predeposit) as amount');		
		$amount = $store['amount'] > 0 ?  $store['amount'] : 0;		
		return $amount ;
	
	}
	
	private function getUserAmount(){
		
		$user = model('member')->getMemberInfo(TRUE,'SUM(available_predeposit) as amount');
		$amount = $user['amount'] > 0 ?  $user['amount'] : 0;
		return $amount ;
		
	}


	private function getDeliveryAmount(){
		
		$delivery = model('waiter')->getWaiterInfo(TRUE,'SUM(available_predeposit) as amount');
		$amount = $delivery['amount'] > 0 ?  $delivery['amount'] : 0;
		return $amount ;
		
	}
	
	
    
	
	
}