<?php
/**
 * This file is part of workerman.
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the MIT-LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author walkor<walkor@workerman.net>
 * @copyright walkor<walkor@workerman.net>
 * @link http://www.workerman.net/
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
//declare(ticks=1);

use \GatewayWorker\Lib\Gateway;
use \GatewayWorker\Lib\DbConnection;
/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
   
	 public static $db = null;
	
	 /**
     * 进程启动后初始化数据库连接
     */
    public static function onWorkerStart($worker)
    {
		
		 global $db;
		
		
         $db = new DbConnection('localhost', '3306', 'madexpress', 'EBZWiJdLwdHCTCNy', 'madexpress');
    }
	
	/**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        global $db;
		
		Gateway::sendToClient($client_id,  json_encode(array(
            'type'      => 'init',
            'client_id' => $client_id
        )));
	
		$time = time();
		$data = array(
			'start_time'=>$time,
			'addtime'=> $time,
			'client_id'=> $client_id
		);
		
		$_SESSION['log_id'] = $db->insert('me_stoket')->cols($data)->query();
		

		
		var_dump($client_id.'lianjie');
		
    }
    
   /**
    * 当客户端发来消息时触发
    * @param int $client_id 连接id
    * @param mixed $message 具体消息
    */
   public static function onMessage($client_id, $message)
   {
      
      
         $message_data = json_decode($message, true);
        if(!$message_data)
        {
            return ;
        }
        
        // 根据类型执行不同的业务
        switch($message_data['type'])
        {
    		  case 'pong':
                return;
        }
      
	   // 向所有人发送 
       // Gateway::sendToAll("$client_id said $message\r\n");
 
   
   }
   
   /**
    * 当用户断开连接时触发
    * @param int $client_id 连接id
    */
   public static function onClose($client_id)
   {
	   global $db; 
	   
	   $time = time();	
	   
	   
	   //断线的时候获取当前绑定账号是谁。	   
	   $row = $db->query("SELECT * FROM `me_stoket` WHERE `log_id` = ".$_SESSION['log_id']);	 
		 
		   var_dump($row); 
	   if($row[0]){
		   
		   if($row[0]['type'] == 1){ //骑手			   
		   
			$delivery_info = $db->query("SELECT * FROM `me_distributor` WHERE `distributor_id` = ".$row[0]['distributor_id']);
		 
			if($delivery_info[0]['delivery_state'] == 0 || $delivery_info[0]['delivery_state'] == 1 || $delivery_info[0]['delivery_state'] == 2){
				
				//更新当前掉线下的骑手监听
				$db->query("UPDATE `me_distributor` SET `stocket_last_time`=". $time .", `stocket_error`='1' WHERE `distributor_id`=".$row[0]['distributor_id']);		
				
			}
		     
			   
		   }		   
		   if($row[0]['type'] == 2){ //商家
			   
			   
			   
			   
			   
			   
			   
		   }	   		   
	   }
	   
	   
	   
	   $db->query("UPDATE `me_stoket` SET `end_time` = ".$time." WHERE log_id=".$_SESSION['log_id']);
	   
	   var_dump($client_id.'duankai');
	   
	   
       // 向所有人发送 
      //   GateWay::sendToAll("$client_id logout\r\n");
	   
	   
	   
	   
	   
   }
}
