<?php
/**
 * 我的语言包
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class store_languageModel extends Model {

    public function __construct() {
        parent::__construct('store_language');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getStoreLangInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getStoreLangList($condition, $order = 'language_id asc'){
        $attr_list = $this->where($condition)->order($order)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addStoreLang($param){
        return $this->insert($param);
    }
   
   public function addStoreLangAll($param){
	   
	   return $this->insertAll($param);
	   
   }
   
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editStoreLang($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delStoreLang($condition){
        return $this->where($condition)->delete();
    }
}
