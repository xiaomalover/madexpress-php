<?php
/**
 * 当前商户的剩余的餐盒量
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_foodbox_goodsModel extends Model {
    public function __construct() {
        parent::__construct('store_foodbox');
    }

    /**
    
     * @param array $insert
     * @return boolean
     */
    public function addStoreFoodboxGoods($insert) {
        return $this->insert($insert);
    }

    /**
     
     * @param array $condition
     * @param array $update 
     * @return boolean
     */
    public function editStoreFoodboxGoods($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
    
     * @param unknown $condition
     * @return boolean
     */
    public function delStoreFoodboxGoods($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     
     * @param array $condition
     * @return array
     */
    public function getStoreFoodboxGoodsCount($condition) {
        return $this->where($condition)->count();
    }

    /**

     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getStoreFoodboxGoodsList($condition, $field = '*', $page = 0, $order = 'id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }
	
	
	
  
	public function getStoreFoodboxGoodsJoinList($condition,  $field='*', $page = null, $order='', $limit=''){
        $on = 'foodbox_goods.goods_id = store_foodbox.goods_id';
        $result = $this->table('foodbox_goods,store_foodbox')->field($field)->join('left')->on($on)->where($condition)->page($page)->order($order)->limit($limit)->select();
		
		
        return $result;
    }

   
	
	
    /**
   
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getStoreFoodboxGoodsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
