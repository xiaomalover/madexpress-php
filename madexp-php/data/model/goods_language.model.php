<?php
/**
 * 我的语言包
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class goods_languageModel extends Model {

    public function __construct() {
        parent::__construct('goods_language');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getGoodsLangInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getGoodsLangList($condition, $order = 'lang_id asc',$field = '*'){
        $attr_list = $this->where($condition)->field($field)->order($order)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addGoodsLang($param){
        return $this->insert($param);
    }
	
	  public function addGoodsLangAll($param){
        return $this->insertAll($param);
    }
	
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editGoodsLang($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delGoodsLang($condition){
        return $this->where($condition)->delete();
    }
}
