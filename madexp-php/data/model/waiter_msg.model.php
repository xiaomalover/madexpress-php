<?php
/**
 * 我的地址
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class waiter_msgModel extends Model {

    public function __construct() {
        parent::__construct('distributor_msg');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getWaiterMsgInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getWaiterMsgList($condition,$field = '*',$page =0, $order = 'msg_id desc'){
        $attr_list = $this->where($condition)->order($order)->page($page)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addWaiterMsg($param){
        return $this->insert($param);
    }
   
   public function addWaiterMsgAll($data) {
        return $this->insertAll($data);     
       
   }
   
   
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editWaiterMsg($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delWaiterMsg($condition){
        return $this->where($condition)->delete();
    }
	
	  /**
     * 查询Workhours数量
     * @param array $condition
     * @return array
     */
    public function getWaiterMsgCount($condition) {
        return $this->where($condition)->count();
    }
	
}
