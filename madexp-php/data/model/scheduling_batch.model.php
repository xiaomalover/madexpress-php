<?php
/**
 * 排班模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_batchModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_region_batch');
    }

    public function getBatchList($condition=array(), $page=null, $order='id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }



	public function getBatchInfo($condition = array()){		
		return $this->where($condition)->find();		
	}

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delBatch($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addBatch($data) {
        return $this->insert($data);
    }

    public function addBatchAll($data)
    {
        return $this->insertAll($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editBatch($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
}
