<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class stoket_lockModel extends Model {
    public function __construct() {
        parent::__construct('stoket_lock');
    }

    /**
     * 添加Lock
     * @param array $insert
     * @return boolean
     */
    public function addLock($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Lock
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editLock($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Lock
     * @param unknown $condition
     * @return boolean
     */
    public function delLock($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Lock数量
     * @param array $condition
     * @return array
     */
    public function getLockCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Lock列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getLockList($condition, $field = '*', $page =10, $order = 'lock_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Lock内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLockInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
