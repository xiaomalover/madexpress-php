<?php
/**
 * 手机端令牌模型
 */

defined('InMadExpress') or exit('Access Invalid!');

class mb_waiter_tokenModel extends Model{
    public function __construct(){
        parent::__construct('mb_waiter_token');
    }

    /**
     * 查询
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getMbWaiterTokenInfo($condition) {
        return $this->where($condition)->find();
    }

    public function getMbWaiterTokenInfoByToken($token) {
        if(empty($token)) {
            return null;
        }
        return $this->getMbWaiterTokenInfo(array('token' => $token));
    }

  
    /**
     * 新增
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addMbWaiterToken($param){
        return $this->insert($param);
    }

    /**
     * 删除
     *
     * @param int $condition 条件
     * @return bool 布尔类型的返回结果
     */
    public function delMbWaiterToken($condition){
        return $this->where($condition)->delete();
    }
}
