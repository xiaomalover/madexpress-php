<?php
/**
 * 票据模型
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class bill_typeModel extends Model {

	  public function __construct() {
		  parent::__construct('bill_type');
	  }
  

    /**
     * 列表
     * @param unknown $condition
     * @param string $fields
     * @param string $pagesize
     * @param string $order
     * @param string $limit
     */
    public function getBillTypeList($condition = array(), $fields = '*', $pagesize = null, $order = '', $limit = null) {
        return $this->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 单条
     * @param unknown $condition
     * @param string $fields
     */
    public function getBillTypeInfo($condition = array(), $fields = '*') {
        return $this->where($condition)->field($fields)->find();
    }

    /**
     * 取得数量
     * @param unknown $condition
     */
    public function getBillTypeCount($condition) {
        return $this->where($condition)->count();
    }


    public function addBillType($data) {
		
		
		
		
		
		
		
        return $this->insert($data);
    }

    public function editBillType($data, $condition = array()) {
        return $this->where($condition)->update($data);
    }
	
	/**
	 * 删除Workhours
	 * @param unknown $condition
	 * @return boolean
	 */
	public function delBillType($condition) {        
	    return $this->where($condition)->delete();
	}
	
}
