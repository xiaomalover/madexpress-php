<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class feedbackModel extends Model {
    public function __construct() {
        parent::__construct('feedback');
    }

    /**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addFeedBack($insert) {
        return $this->table('feedback')->insert($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editFeedBack($condition, $update) {
        return $this->table('feedback')->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delFeedBack($condition) {        
        return $this->table('feedback')->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getFeedBackCount($condition) {
        return $this->table('feedback')->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getFeedBackList($condition, $field = '*', $page = 0, $order = 'is_top desc,updatetime desc,id desc', $limit = 0) {
	
        return $this->table('feedback')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }
    public function getFeedBackListGroup($condition, $field = '*', $page = 0, $order = 'is_top desc,updatetime desc,id desc', $limit = 0,$group="") {
	
        return $this->table('feedback')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->group($group)->select();
    }




    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getFeedBackInfo($condition, $field = '*') {
        return $this->table('feedback')->field($field)->where($condition)->find();
    }
		
	
	public function delFeedBackLog($condition) {        
        return $this->table('feedback_log')->where($condition)->delete();
    }
	
	
	public function getFeedBackListLog($condition, $field = '*', $page = 0, $order = 'id desc', $limit = '') {
        return $this->table('feedback_log')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

	
	public function addFeedBackLog($insert){		
		return $this->table('feedback_log')->insert($insert);		
	}
	
	
	public function getFeedBackType($condition){
		
		  return $this->table('feedback_type')->where($condition)->select();
	}

	
}
