<?php
/**
 * 订单轮询模型
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class order_pollingModel extends Model{

    public function __construct(){
        parent::__construct('order_polling');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getPollingList($condition = array(), $page = '', $order = 'deliver_sort asc,last_time asc', $field = '*', $limit = '') {
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getPollingInfo($condition,$fields = '*',$order='order_sort desc,deliver_sort asc,last_time asc') {
        return $this->where($condition)->field($fields)->order($order)->find();
    }

    /*
     * 增加
     * @param array $data
     * @return bool
     */
    public function addPolling($data){
        return $this->insert($data);
    }

	public function addPollingAll($data){
		
		return $this->insertAll($data);
		
	}
	
    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPolling($data,$condition) {
        return $this->where($condition)->update($data);
    }
	
	

    public function getPollingCount($condition) {
        return $this->where($condition)->count();
    }
	
	
	public function delPolling($condition) {
        return $this->where($condition)->delete();
    }
	
	

}
