<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class adv_apModel extends Model {
    public function __construct() {
        parent::__construct('adv_position');
    }

    /**
     * 添加AdvAp
     * @param array $insert
     * @return boolean
     */
    public function addAdvAp($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑AdvAp
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editAdvAp($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除AdvAp
     * @param unknown $condition
     * @return boolean
     */
    public function delAdvAp($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询AdvAp数量
     * @param array $condition
     * @return array
     */
    public function getAdvApCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * AdvAp列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getAdvApList($condition, $field = '*', $page = 0, $order = 'ap_id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个AdvAp内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAdvApInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
	

	
}
