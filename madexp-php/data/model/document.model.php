<?php
/**
 * 系统文章

 */
defined('InMadExpress') or exit('Access Invalid!');

class documentModel extends Model {
   
	 public function __construct() {
        parent::__construct('document');
    }
	/**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addDocument($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editDocument($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delDocument($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getDocumentCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getDocumentList($condition, $field = '*', $page = 0, $order = 'doc_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getDocumentInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
