<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class goods_lang_xlsModel extends Model {
    public function __construct() {
        parent::__construct('goods_language_xls');
    }

    /**
     * 添加GoodsXlsLang
     * @param array $insert
     * @return boolean
     */
    public function addGoodsXlsLang($insert) {
        return $this->insert($insert);
    }
	
	public function addGoodsXlsLangAll($insert) {
        return $this->insertAll($insert);
    }

    /**
     * 编辑GoodsXlsLang
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editGoodsXlsLang($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除GoodsXlsLang
     * @param unknown $condition
     * @return boolean
     */
    public function delGoodsXlsLang($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询GoodsXlsLang数量
     * @param array $condition
     * @return array
     */
    public function getGoodsXlsLangCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * GoodsXlsLang列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getGoodsXlsLangList($condition, $field = '*', $page = 0, $order = 'id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个GoodsXlsLang内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsXlsLangInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
