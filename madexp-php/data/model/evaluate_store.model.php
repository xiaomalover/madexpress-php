<?php
/**
 * 店铺评分模型
 */
defined('InMadExpress') or exit('Access Invalid!');
class evaluate_storeModel extends Model {

    public function __construct(){
        parent::__construct('evaluate_store');
    }

    /**
     * 查询店铺评分列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @return array
     */
    public function getEvaluateStoreList($condition, $page=null, $order='store_evaluate_id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }

    /**
     * 获取店铺评分信息
     */
    public function getEvaluateStoreInfo($condition, $field='*') {
        $list = $this->field($field)->where($condition)->find();
        return $list;
    }

  	public function getEvaluateStoreCount($condition) {
        return $this->where($condition)->count();
    }
	
	
	//修改
	public function editEvaluateStore($where,$data){
		
		
		  return $this->where($where)->update($data);
	}


    /**
     * 添加店铺评分
     */
    public function addEvaluateStore($param) {
        return $this->insert($param);
    }

    /**
     * 删除店铺评分
     */
    public function delEvaluateStore($condition) {
        return $this->where($condition)->delete();
    }
	
	
	
		//新增log
	public function  addLog($param){		
		return $this->table('evaluate_store_log')->insert($param);
	}
	
	//log列表
	public function getLogList($condition, $page=null, $order='id desc', $field='*') {       
		$list = $this->table('evaluate_store_log')->field($field)->where($condition)->page($page)->order($order)->select();
	    return $list;		
	}
	
	
	//log详情
	public function getLogInfo($condition,$field){
		 return  $this->table('evaluate_store_log')->field($field)->where($condition)->find();
	}
	
	//删除log
	public function delLogInfo($condition){		
		return $this->table('evaluate_store_log')->where($condition)->delete();		
	}
	
	
}
