<?php
/**
 * 送餐员模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class waiterModel extends Model {

    public function __construct(){
        parent::__construct('distributor');
    }

    /**
     * 会员详细信息（查库）
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getWaiterInfo($condition, $field = '*', $master = false) {
        return $this->table('distributor')->field($field)->where($condition)->master($master)->find();
    }

    /**
     * 取得会员详细信息（优先查询缓存）
     * 如果未找到，则缓存所有字段
     * @param int $member_id
     * @param string $field 需要取得的缓存键值, 例如：'*','member_name,member_sex'
     * @return array
     */
    public function getWaiterInfoByID($waiter_id, $fields = '*') {
        $waiter_info = rcache($waiter_id, 'waiter', $fields);
        if (empty($waiter_info)) {
            $waiter_info = $this->getWaiterInfo(array('distributor_id'=>$waiter_id),'*',true);
            wcache($waiter_id, $waiter_info, 'waiter');
        }
        return $waiter_info;
    }

    /**
     * 会员列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getWaiterList($condition = array(), $field = '*', $page = null, $order = 'distributor_id asc', $limit = '') {
       return $this->table('distributor')->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

	
	/**
	 * 删除会员
	 *
	 * @param int $id 记录ID
	 * @return array $rs_row 返回数组形式的查询结果
	 */
	public function del($id){
		if (intval($id) > 0){
			$where = " distributor_id = '". intval($id) ."'";
			$result = Db::delete('distributor',$where);
			return $result;
		}else {
			return false;
		}
	}

	 public function addWaiter($param){
        return $this->insert($param);
    }

	
	//配送员真实信息表	
	public function getDeliveryCommInfo($condition,$field = '*'){
		   return $this->table('distributor_common')->field($field)->where($condition)->find();		
	}
	
	public function addDeliveryComm($param){
		  return $this->table('distributor_common')->insert($param);   
	}
	
	public function editDeliveryComm($condition,$data){
		 return $this->table('distributor_common')->where($condition)->update($data);		 
	}



	
    /**
     * 会员数量
     * @param array $condition
     * @return int
     */
    public function getWaiterCount($condition) {
        return $this->table('distributor')->where($condition)->count();
    }

    /**
     * 编辑会员
     * @param array $condition
     * @param array $data
     */
    public function editWaiter($condition, $data) {
        $update = $this->table('distributor')->where($condition)->update($data);
        if ($update && $condition['distributor_id']) {
			dcache($condition['distributor_id'], 'waiter');
        }
        return $update;
    }
	
	
	/**
     * 
     */
    public function login($login_info) {
       
		if (process::islock('login')) {
            return array('error' => '您的操作过于频繁，请稍后再试');
        }
        process::addprocess('login');
        $username = $login_info['username'];
        $password = $login_info['password'];
    
		$obj_validate = new Validate();
        $obj_validate->validateparam = array(
            array(
                "input" => $username,
                "require" => "true",
                "message" => "请填写用户名"
            ),
            array(
                "input" => $username,
                "validator" => "username",
                "message" => "请填写字母、数字、中文、_"
            ),
            array(
                "input" => $username,
                "max" => "20",
                "min" => "3",
                "validator" => "length",
                "message" => "用户名长度要在6~20个字符"
            ),
            array(
                "input" => $password,
                "require" => "true",
                "message" => "密码不能为空"
            )
        );
        $error = $obj_validate->validate();
        if ($error != ''){
            return array('error' => $error);
        }
        $condition = array();
        $condition['login_name'] = $username;
        $condition['login_password'] = md5($password);
        $waiter_info = $this->getWaiterInfo($condition);
     
		
		
        if (!empty($waiter_info)) {
            if($waiter_info['distributor_state'] == 1){
                return array('error' => '账号被停用');
            }
            process::clear('login');

            $update_info    = array(
                'login_num'=> ($waiter_info['login_num']+1),
                'login_time'=> TIMESTAMP,
                'login_last_time'=> $waiter_info['login_time'],
            
            );

            $this->editWaiter(array('distributor_id'=>$waiter_info['distributor_id']),$update_info);
            
            return $waiter_info;
        } else {
            return array('error' => '用户名或密码错误，请重新登录');
        }
    }
	
	
	
	
	
	
}
