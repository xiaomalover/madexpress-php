<?php
/**
 * 余额支付

 */
defined('InMadExpress') or exit('Access Invalid!');
class predepositModel extends Model {
    /**
     * 生成充值编号
     * @return string
     */
    public function makeSn() {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $_SESSION['member_id'] % 1000);
    }

    public function addRechargeCard($sn, array $session)
    {
        $memberId = (int) $session['member_id'];
        $memberName = $session['member_name'];

        if ($memberId < 1 || !$memberName) {
            throw new Exception("当前登录状态为未登录，不能使用充值卡");
        }

        $rechargecard_model = Model('rechargecard');

        $card = $rechargecard_model->getRechargeCardBySN($sn);

        if (empty($card) || $card['state'] != 0 || $card['member_id'] != 0) {
            throw new Exception("充值卡不存在或已被使用");
        }

        $card['member_id'] = $memberId;
        $card['member_name'] = $memberName;

        try {
            $this->beginTransaction();

            $rechargecard_model->setRechargeCardUsedById($card['id'], $memberId, $memberName);

            $card['amount'] = $card['denomination'];
            $this->changeRcb('recharge', $card);

            $this->commit();
        } catch (Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * 取得充值列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdRechargeList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('pd_recharge')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加充值记录
     * @param array $data
     */
    public function addPdRecharge($data) {
        return $this->table('pd_recharge')->insert($data);
    }

    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdRecharge($data,$condition = array()) {
        return $this->table('pd_recharge')->where($condition)->update($data);
    }

    /**
     * 取得单条充值信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdRechargeInfo($condition = array(), $fields = '*',$lock = false) {
        return $this->table('pd_recharge')->where($condition)->field($fields)->lock($lock)->find();
    }

    /**
     * 取充值信息总数
     * @param unknown $condition
     */
    public function getPdRechargeCount($condition = array()) {
        return $this->table('pd_recharge')->where($condition)->count();
    }


	/**
	 * 取得充值列表
	 * @param unknown $condition
	 * @param string $pagesize
	 * @param string $fields
	 * @param string $order
	 */
	public function getPdRechargeRefundList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
	    return $this->table('pd_recharge_refund')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
	}
	
	/**
	 * 添加充值记录
	 * @param array $data
	 */
	public function addPdRechargeRefund($data) {
	    return $this->table('pd_recharge_refund')->insert($data);
	}
	
	/**
	 * 编辑
	 * @param unknown $data
	 * @param unknown $condition
	 */
	public function editPdRechargeRefund($data,$condition = array()) {
	    return $this->table('pd_recharge_refund')->where($condition)->update($data);
	}
	
	/**
	 * 取得单条充值信息
	 * @param unknown $condition
	 * @param string $fields
	 */
	public function getPdRechargeRefundInfo($condition = array(), $fields = '*',$lock = false) {
	    return $this->table('pd_recharge_refund')->where($condition)->field($fields)->lock($lock)->find();
	}
	
	/**
	 * 取充值信息总数
	 * @param unknown $condition
	 */
	public function getPdRechargeRefundCount($condition = array()) {
	    return $this->table('pd_recharge_refund')->where($condition)->count();
	}






    /**
     * 取提现单信息总数
     * @param unknown $condition
     */
    public function getPdCashCount($condition = array()) {
        return $this->table('pd_cash')->where($condition)->count();
    }

    /**
     * 取日志总数
     * @param unknown $condition
     */
    public function getPdLogCount($condition = array()) {
        return $this->table('pd_log')->where($condition)->count();
    }

	

    /**
     * 取得余额变更日志列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdLogList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('pd_log')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 删除日志
     * @param unknown $condition
     */
    public function delPdLog($condition = array()) {
        return $this->table('pd_log')->where($condition)->delete();
    }



    /**
     * 变更充值卡余额
     *
     * @param string $type
     * @param array  $data
     *
     * @return mixed
     * @throws Exception
     */
    public function changeRcb($type, $data = array())
    {
        $amount = (float) $data['amount'];
        if ($amount < .01) {
            throw new Exception('参数错误');
        }

        $available = $freeze = 0;
        $desc = null;

        switch ($type) {
        case 'Balance Deduction':
            $available = -$amount;
            $desc = '下单，使用充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_freeze':
            $available = -$amount;
            $freeze = $amount;
            $desc = '下单，冻结充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_cancel':
            $available = $amount;
            $freeze = -$amount;
            $desc = '取消订单，解冻充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'order_comb_pay':
            $freeze = -$amount;
            $desc = '下单，扣除被冻结的充值卡余额，订单号: ' . $data['order_sn'];
            break;

        case 'recharge':
            $available = $amount;
            $desc = '平台充值卡充值，充值卡号: ' . $data['sn'];
            break;

        case 'refund':
            $available = $amount;
            $desc = '确认退款，订单号: ' . $data['order_sn'];
            break;

        case 'vr_refund':
            $available = $amount;
            $desc = '虚拟兑码退款成功，订单号: ' . $data['order_sn'];
            break;

        case 'order_book_cancel':
            $available = $amount;
            $desc = '取消预定订单，退还充值卡余额，订单号: ' . $data['order_sn'];
            break;

        default:
            throw new Exception('参数错误');
        }

        $update = array();
        if ($available) {
            $update['available_rc_balance'] = array('exp', "available_rc_balance + ({$available})");
        }
        if ($freeze) {
            $update['freeze_rc_balance'] = array('exp', "freeze_rc_balance + ({$freeze})");
        }

        if (!$update) {
            throw new Exception('参数错误');
        }

        // 更新会员
        $updateSuccess = Model('member')->editMember(array(
            'member_id' => $data['member_id'],
        ), $update);

        if (!$updateSuccess) {
            throw new Exception('操作失败');
        }

        // 添加日志
        $log = array(
            'member_id' => $data['member_id'],
            'member_name' => $data['member_name'],
            'type' => $type,
            'add_time' => TIMESTAMP,
            'available_amount' => $available,
            'freeze_amount' => $freeze,
            'description' => $desc,
        );

        $insertSuccess = $this->table('rcb_log')->insert($log);
        if (!$insertSuccess) {
            throw new Exception('操作失败');
        }

        $msg = array(
            'code' => 'recharge_card_balance_change',
            'member_id' => $data['member_id'],
            'param' => array(
                'time' => date('Y-m-d H:i:s', TIMESTAMP),
                'url' => urlMember('predeposit', 'rcb_log_list'),
                'available_amount' => ncPriceFormat($available),
                'freeze_amount' => ncPriceFormat($freeze),
                'description' => $desc,
            ),
        );

        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
                'consume_amount'=>$amount,'consume_time'=>time(),'consume_remark'=>$desc));
        // 发送买家消息
        QueueClient::push('sendMemberMsg', $msg);

        return $insertSuccess;
    }

  
	
	
	/**
     * 变更余额
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();
		
      //  $data_log['lg_invite_member_id'] = $data['invite_member_id'];
        $data_log['lg_member_id'] = $data['member_id'];
        $data_log['lg_member_name'] = $data['member_name'];
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
		$msg_code = '';
        switch ($change_type){
						
       
           
				
			case 'order_refund': //所有订单订单退款
				$msg = $data['msg'];
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_desc'] = 'Order Refund';
				$data_log['lg_name_of_activities'] = 'Order Refund';
				$data_log['lg_type_activities'] = 0;				
				$data_log['lg_order_id'] = $data['order_id'];
				$data_log['lg_order_sn'] = $data['order_sn'];								
                $data_pd['freeze_predeposit'] = 0;
                $data_msg['av_amount'] = array('exp','available_predeposit+'.$data['amount']); 
				if($data['refund_type'] == 0){ //取消订单撤销冻结 0 客户取消 1商家退款
					$data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
				}
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
				$msg_code = 'order_cancel'; //订单退款消息发送
				
                break;
				
								
			case 'balance_deduction': //钱包余额抵扣		
			    $data_log['lg_av_amount'] = -$data['amount'];
			    $data_log['lg_desc'] = 'Balance Deduction';				
				$data_log['lg_name_of_activities'] = 'Balance Deduction';
				$data_log['lg_type_activities'] = 1;				
				$data_log['lg_order_id'] = $data['order_id'];
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_pd['freeze_predeposit'] = array('exp','freeze_predeposit+'.$data['amount']);
			    $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
			    $data_msg['av_amount'] = -$data['amount'];
			    $data_msg['freeze_amount'] = 0;
			    $data_msg['desc'] = $data_log['lg_desc'];
			    break;
				
				
			case 'offline_payment': //线下钱包余额抵扣
			
			    $data_log['lg_av_amount'] = -$data['amount'];
			    $data_log['lg_desc'] = 'Offline Payment';					
				$data_log['lg_name_of_activities'] = 'Offline Payment';
				$data_log['lg_type_activities'] = 1;
				$data_log['lg_order_id'] = $data['order_id'];
				$data_log['lg_order_sn'] = $data['order_sn'];	
			    $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
			    $data_msg['av_amount'] = -$data['amount'];
			    $data_msg['freeze_amount'] = 0;
			    $data_msg['desc'] = $data_log['lg_desc'];
			    break;	
		
			
			case 'offline_payment_freeze':
			
			    $data_log['lg_av_amount'] = -$data['amount'];
			    $data_log['lg_freeze_amount'] = $data['amount'];
			    $data_log['lg_desc'] = '线下付款，冻结余额，订单号: '.$data['order_sn'];
			    $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit+'.$data['amount']);
			    $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
			
			    $data_msg['av_amount'] = -$data['amount'];
			    $data_msg['freeze_amount'] = $data['amount'];
			    $data_msg['desc'] = $data_log['lg_desc'];
			    break;
			
			case 'offline_payment_thawfreeze':
			
			    $data_log['lg_freeze_amount'] = -$data['amount'];
			    $data_log['lg_desc'] = '线下付款，扣除冻结余额，订单号: '.$data['order_sn'];
			    $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);												
				$data_msg['freeze_amount'] = -$data['amount'];							
			    $data_msg['desc'] = $data_log['lg_desc'];
			    break;
			
			
			case 'voucher_redeem':			
				$msg = $data['msg'];
				$data_log['lg_av_amount'] = $data['amount'];
				$data_log['lg_desc'] = 'Voucher Redeem';
				$data_log['lg_name_of_activities'] = 'Voucher Redeem';
				$data_log['lg_type_activities'] = 0;				
				$data_log['lg_order_id'] = $data['order_id'];
				$data_log['lg_order_sn'] = $data['order_sn'];								
				$data_pd['freeze_predeposit'] = 0;
				$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);	
				$data_msg['av_amount'] = array('exp','available_predeposit+'.$data['amount']); 
				$data_msg['desc'] = $data_log['lg_desc'];		
			break;
				
            default:
                throw new Exception('参数错误');
                break;
				
        }		
		
        $update = Model('member')->editMember(array('member_id'=>$data['member_id']),$data_pd);
        if (!$update) {
            throw new Exception('操作失败');
        }
        $insert = $this->table('pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }

      
		// 支付成功发送买家消息
       if($msg_code){
		
			$param = array();
			$param['code'] = $msg_code;
			$param['member_id'] = $data['member_id'];
		  //  $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
		  //  $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
			$param['param'] = $data_msg;
	   //     QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
	   //     'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
	  
			QueueClient::push('sendMemberMsg', $param);
		}
		
		
        return $insert;
    }

    /**
     * 删除充值记录
     * @param unknown $condition
     */
    public function delPdRecharge($condition) {
        return $this->table('pd_recharge')->where($condition)->delete();
    }

    /**
     * 取得提现列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdCashList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('pd_cash')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加提现记录
     * @param array $data
     */
    public function addPdCash($data) {
        return $this->table('pd_cash')->insert($data);
    }

    /**
     * 编辑提现记录
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdCash($data,$condition = array()) {
        return $this->table('pd_cash')->where($condition)->update($data);
    }

    /**
     * 取得单条提现信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdCashInfo($condition = array(), $fields = '*') {
        return $this->table('pd_cash')->where($condition)->field($fields)->find();
    }

    /**
     * 删除提现记录
     * @param unknown $condition
     */
    public function delPdCash($condition) {
        return $this->table('pd_cash')->where($condition)->delete();
    }
}
