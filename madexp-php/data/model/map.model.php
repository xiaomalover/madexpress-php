
<?php
/**
 * 商品属性
 *

 */
defined('InMadExpress') or exit('Access Invalid!');

class mapModel extends Model {
    public function __construct() {
        parent::__construct('map');
    }

  

    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getMapInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getMapList($condition, $order = 'region_id asc',$group=''){
        $attr_list = $this->where($condition)->order($order)->group($group)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addMap($param){
        return $this->insert($param);
    }
	
	  public function addMapAll($param){
        return $this->insertAll($param);
    }
	
	
	
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editMap($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delMap($condition){
        return $this->where($condition)->delete();
    }
}



/**
 * @name 围栏算法，判断一个坐标，是否在围栏里面.如：['113.664673,34.810146','113.681667,34.796896','113.69231,34.794711','113.702009,34.809159']
  * @author macnie <mac@lenmy.com>﻿
 * @param array $fences 围栏，是一组坐标数组 如：113.674458,34.804719
 * @param string $point
 * @return bool

function in_fences($fences, $point) {
    $nvert = count($fences);
    $vertx = [];
    $verty = [];
    list($testy, $testx) = explode(',', $point);
    foreach ($fences as $r) {
        list($lng, $lat) = explode(',', $r);
        $vertx[] = $lat;
        $verty[] = $lng;
    }
    $i = $j = $c = 0;
    for ($i = 0, $j = $nvert - 1; $i < $nvert; $j = $i++) {
        if (( ($verty[$i] > $testy) != ($verty[$j] > $testy) ) &&
            ($testx < ($vertx[$j] - $vertx[$i]) * ($testy - $verty[$i]) / ($verty[$j] - $verty[$i]) + $vertx[$i]))
            $c = !$c;
    }
    return $c;
}
 */




?>