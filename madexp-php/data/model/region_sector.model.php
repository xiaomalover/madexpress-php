<?php
/**
 * 地区模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class region_sectorModel extends Model {

    public function __construct() {
        parent::__construct('region_sector');
    }

    /**
     * 获取地址列表
     *
     * @return mixed
     */
    public function getRegionList($condition = array(), $fields = '*', $group = '', $page = 100) {
        return $this->where($condition)->field($fields)->page($page)->limit(false)->group($group)->select();
    }

	/**
	 * 从缓存获取分类 通过分类id 
	 *
	 * @param int $id 分类id
	 */
	public function getRegionInfoById($id) {
		$data = $this->getCache();
		return $data['name'][$id];
	}
	
	/**
     * 获取地址详情
     *
     * @return mixed
     */
    public function getRegionInfo($condition = array(), $fileds = '*') {
        return $this->where($condition)->field($fileds)->find();
    }

    /**
     * 获取一级大区名称数组
     *
     * @return array 键为id 值为名称字符串
     */
    public function getTopLevelRegions() {
        $data = $this->getCache();

        $arr = array();
        foreach ($data['children'][0] as $i) {
            $arr[$i] = $data['name'][$i];
        }

        return $arr;
    }

    /**
     * 获取获取市级id对应省级id的数组
     *
     * @return array 键为市级id 值为省级id
     */
    public function getCityProvince() {
        $data = $this->getCache();

        $arr = array();
        foreach ($data['parent'] as $k => $v) {
            if ($v && $data['parent'][$v] == 0) {
                $arr[$k] = $v;
            }
        }

        return $arr;
    }

    /**
     * 获取地区缓存
     *
     * @return array
     */
    public function getRegions() {
        return $this->getCache();
    }

    /**
     * 获取全部地区名称数组
     *
     * @return array 键为id 值为名称字符串
     */
    public function getRegionNames() {
        $data = $this->getCache();

        return $data['name'];
    }

    /**
     * 获取用于前端js使用的全部地址数组
     *
     * @return array
     */
    public function getRegionArrayForJson($src = 'cache') {
        if ($src == 'cache') {
            $data = $this->getCache();
        } else {
            $data = $this->_getAllRegion();
        }

        $arr = array();
        foreach ($data['children'] as $k => $v) {
            foreach ($v as $vv) {
                $arr[$k][] = array($vv, $data['name'][$vv]);
            }
        }
        return $arr;
    }

    /**
     * 获取地区数组 格式如下
     * array(
     *   'name' => array(
     *     '地区id' => '地区名称',
     *     // ..
     *   ),
     *   'parent' => array(
     *     '子地区id' => '父地区id',
     *     // ..
     *   ),
     *   'children' => array(
     *     '父地区id' => array(
     *       '子地区id 1',
     *       '子地区id 2',
     *       // ..
     *     ),
     *     // ..
     *   ),
     *   'region' => array(array(
     *     '华北区' => array(
     *       '省级id 1',
     *       '省级id 2',
     *       // ..
     *     ),
     *     // ..
     *   ),
     * )
     *
     * @return array
     */
    protected function getCache() {
        // 对象属性中有数据则返回
        if ($this->cachedData !== null)
            return $this->cachedData;

        // 缓存中有数据则返回
        if ($data = rkcache('region_sector')) {
            $this->cachedData = $data;
            return $data;
        }

        // 查库
        $data = $this->_getAllRegion();
        wkcache('region_sector', $data);
        $this->cachedData = $data;

        return $data;
    }

    protected $cachedData;

    private function _getAllRegion() {
        $data = array();
        $region_all_array = $this->limit(false)->select();
        foreach ((array) $region_all_array as $a) {
            $data['name'][$a['region_id']] = $a['region_name'];
            $data['parent'][$a['region_id']] = $a['region_parent_id'];
            $data['children'][$a['region_parent_id']][] = $a['region_id'];
        
            if ($a['area_deep'] == 1 && $a['area_region'])
                $data['region'][$a['area_region']][] = $a['region_id'];
        }
        return $data;
    }

    public function addArea($data = array()) {
        return $this->insert($data);
    }

    public function addAreaAll($data = array()) {
        return $this->insertAll($data);
    }


    public function editArea($data = array(), $condition = array()) {
        return $this->where($condition)->update($data);
    }

    public function delArea($condition = array()) {
        return $this->where($condition)->delete();
    }

	public  function countArea($condition = array()){
		
		return $this->where($condition)->count();
		
	}

    /**
     * 递归取得本地区及所有上级地区名称
     * @return string
     */
    public function getTopRegionName($region_id,$region_name = '') {
        $info_parent = $this->getAreaInfo(array('region_id'=>$region_id),'region_name,region_parent_id');
        if ($info_parent) {
            return $this->getTopRegionName($info_parent['region_parent_id'],$info_parent['region_name']).' '.$info_parent['region_name'];
        }
    }

    /**
     * 递归取得本地区所有孩子ID
     * @return array
     */
    public function getChildrenIDs($region_id) {
        $result = array();
        $list = $this->getRegionList(array('region_parent_id'=>$region_id),'*');
		
        if ($list) {
            foreach ($list as $v) {
                $result[] = $v;
                $result = array_merge($result,$this->getChildrenIDs($v['region_id']));
            }
        }
        return $result;
    }
}
