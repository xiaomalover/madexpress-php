<?php
/**
 * cms文章模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class notice_articleModel extends Model{

    public function __construct(){
        parent::__construct('notice_article');
    }

    public function getArticleListNew($condition, $field = '*', $page = 0, $order = 'article_sort asc,article_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }
  

	
	public function addArticle($insert) {
	    return $this->insert($insert);
	}
	
	public function editArticle($condition, $update) {
	    return $this->where($condition)->update($update);
	}
	
	public function delArticle($condition) {	 
	    return $this->where($condition)->delete();
	}
	
	public function getArticleInfo($condition, $field = '*') {
	    return $this->field($field)->where($condition)->find();
	}
    /**
     * 取得文章数量
     * @param unknown $condition
     */
    public function getCount($condition = array()) {
        return $this->where($condition)->count();
    }

}
