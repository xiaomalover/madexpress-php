
<?php
/**
 *
 *订单账单结算模型
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class order_billModel extends Model
{


	//订单收货后金额分配下去，写入相关的钱包里。配送员正常流程

	public function bill($order_id){		
		//print_r($order_id);
		$pdf_model = model('pdf');
		$where = array();
		$where['order_id'] = $order_id;		
	//	$where['order_bill_state'] = 0;
		$order_info = model('order')->getOrderInfoN($where);
	
		$store_wallet = model('store_wallet');
		
		//检查有没有1，2阶段退款的单子。生成退款记录单
		$refund_record = model('order_refund')->getRefundList(array('order_id' => $order_info['order_id'],'refund_stage' => array('in','0,1'),'refund_state' => 0));
		
		$refund_amount = 0;
		$refund_payment_amount = 0;	
		$refund_wallet_amount = 0;
		$refund_coupon_amount = 0;	

		if(!empty($refund_record)){
			
			foreach($refund_record as $v){
				$refund_amount += $v['refund_amount'];				
				$refund_payment_amount += $v['refund_pay_amount'];
				$refund_wallet_amount  += $v['refund_wallet_amount'];
				$refund_coupon_amount  += $v['refund_coupon_amount'];
			}


			$refund_recode_data = array();
			$refund_recode_data['order_id'] = $order_info['order_id'];
			$refund_recode_data['refund_amount'] = $refund_amount;
			$refund_recode_data['refund_payment_amount'] =  $refund_payment_amount;//;$refund_record['refund_payment_amount'];
			$refund_recode_data['refund_wallet_amount'] = $refund_wallet_amount + $refund_coupon_amount; //$refund_record['refund_wallet_amount'];
			$refund_recode_data['voucher_re'] = 'No';						
			$refund_amount +=  $refund_amount;			
			$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
			

			//写入用户退款
			$member_wallet_amount =  $refund_wallet_amount + $refund_coupon_amount; 
			if($member_wallet_amount > 0){

				//写入用户钱包
				$data = array();
				$data['amount'] = $member_wallet_amount;
				$data['order_sn'] = $order_info['order_sn'];		
				$data['order_id'] = $order_info['order_id'];		
				$data['member_id'] = $order_info['buyer_id'];
				$data['member_name'] = $order_info['buyer_name'];
				
				model('predeposit')->changePd('order_refund',$data);

			}
		}		

		
		/*
		//检查有没有2阶段退款的商品 生成退款单
		$refund_record = model('order_refund')->getRefundList(array('order_id' => $order_info['order_id'],'refund_stage' => 1,'refund_state' => 0));
		if(!empty($refund_record)){
			
			$refund_recode_data = array();
			$refund_recode_data['order_id'] = $order_info['order_id'];
			$refund_recode_data['refund_amount'] = $refund_record['refund_amount'];
			$refund_recode_data['refund_payment_amount'] = $refund_record['refund_payment_amount'];
			$refund_recode_data['refund_wallet_amount'] = $refund_record['refund_wallet_amount'];
			$refund_recode_data['voucher_re'] = 'No';		
			$refund_amount +=  $refund_record['refund_amount'];			
			$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');
			
		}	
		
		*/
	
					
		//Online Sales Income
		//线上销售收入		
		$online_sales_amount = $order_info['goods_amount']  + $order_info['pay_commission_amount'] + $order_info['foodbox_amount'] ;
	//	print_r($order_id);
		if($online_sales_amount > 0 ){
			
			//写入商家钱包
			$data = array();
			$data['amount'] = $online_sales_amount;
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			$store_wallet->changePd('online_sales_income',$data);
			
			//生成票据所需
			$invoice_data = array();
			$invoice_data['order_id'] = $order_info['order_id'];
			$invoice_data['store_id'] = $order_info['store_id'];
			$invoice_data['user_id'] = $order_info['buyer_id'];		
			
		//	print_r($invoice_data);			
			$pdf = $pdf_model->createPdf($invoice_data,'online_sales_invoice_customer');
			
		}
				

		$contract = model('store_contract')->getContractInfo(array('store_id'=> $order_info['store_id'],'is_default' => 1));        				
		//Platform Charges
		//销售抽成&附加费		
		$platform_charges_amount = ($order_info['goods_amount'] * ($contract['contract_rate'] / 100)) + $order_info['pay_commission_amount'] ;				
		if($platform_charges_amount > 0){
			
			//写入商家钱包
			$data = array();
			$data['amount'] = $platform_charges_amount;
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			$store_wallet->changePd('platform_charges',$data);
	
	
			//写入ME钱包		
			$me_wallet = model('me_wallet');
			$data = array();
			$data['amount'] = $platform_charges_amount;
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$me_wallet->changePd('platform_charges',$data);
			
			//生成平台服务集成发票
		/*	$invoice_data = array();
			$invoice_data['order_id'] = $order_info['order_id'];
			$invoice_data['store_id'] = $order_info['store_id'];
			$invoice_data['me'] = 1;
			//$invoice_data['user_id'] = $order_info['buyer_id'];			
			//$invoice_data['delivery_id'] = $order_info['distributor_id'];	
			//	print_r($invoice_data);				
			$pdf = $pdf_model->createPdf($invoice_data,'me_platform_invoice_merchant');		
		*/	
		}		
				
			
		//写入配送员钱包
		if($order_info['delivery_fee'] > 0){		

			$waiter_wallet = model('waiter_wallet');
			$data = array();
			$data['amount'] = $order_info['delivery_fee'];
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$data['distributor_id'] = $order_info['distributor_id'];
			$data['distributor_name'] = $order_info['distributor_name'];		
			$waiter_wallet->changePd('delivery_income',$data);
			
			
			//生成配送票据所需
			$invoice_data = array();
			$invoice_data['order_id'] = $order_info['order_id'];
		//	$invoice_data['store_id'] = $order_info['store_id'];
			$invoice_data['user_id'] = $order_info['buyer_id'];			
			$invoice_data['delivery_id'] = $order_info['distributor_id'];	
			//	print_r($invoice_data);				
			$pdf = $pdf_model->createPdf($invoice_data,'delivery_invoice');			
		
		}

		
		//平台优惠券补贴
		if($order_info['platform_coupon_id'] > 0){

			$me_wallet = model('me_wallet');
			$data = array();
			$data['admin_name'] = 'admin';
			$data['amount'] = $order_info['platform_coupon_price'];
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$me_wallet->changePd('voucher_redeem',$data);

		}


		//更新退款记录
		model('order_refund')->editRefund(['order_id' => $order_info['order_id'],'refund_stage' => array('in','0,1'),'refund_state' => 0],['refund_state' => 1]);

		//更新原订单价值		
		model('order')->editOrder(['old_order_amount' => $order_info['order_amount']], ['order_id' => $order_info['order_id']]);




		
	}


	//商家触发部分退款
	public function store_bill_partial($order_id,$refund_id){
		
		$pdf_model = model('pdf');
		$order_info = model('order')->getOrderInfoN(['order_id' => $order_id]);	
		
		if($order_info['order_state'] == 60 ){
			
			$refund_record = model('order_refund')->getRefundInfo(array('refund_id' => $refund_id));		
		//	print_r($refund_record);
			$store_wallet = model('store_wallet');		
			$data = array();
			$data['amount'] = $refund_record['refund_amount'];
			$data['order_sn'] = $order_info['order_sn'];
			$data['order_id'] = $order_info['order_id'];
			$data['store_id'] = $order_info['store_id'];
			$data['store_name'] = $order_info['store_name'];
			$store_wallet->changePd('order_refund',$data);   

			
			$invoice_data = array();
			$invoice_data['order_id'] = $order_info['order_id'];
			$invoice_data['store_id'] = $order_info['store_id'];
			$invoice_data['user_id'] = $order_info['buyer_id'];	
			$invoice_data['refund_id'] = $refund_id;

			//	print_r($invoice_data);			
			$pdf = $pdf_model->createPdf($invoice_data,'sales_credit_note_partial');


			//全部执行完毕后。修改退款单为已退款
			model('order_refund')->editRefund(['order_id' => $order_info['order_id'],'refund_type'=> 1,'refund_stage' => 2,'refund_state' => 0],['refund_state' => 1]);


		}




	}



	//商家触发全单退款时调用
	public function store_bill_full($order_id,$refund_id,$refund_delivery_id = 0){

				$pdf_model = model('pdf');
				$order_info = model('order')->getOrderInfoN(['order_id' => $order_id]);

				if(!empty($order_info)){

					//订单未完成退款，只生成 record；
					if($order_info['order_state'] < 60 && $order_info['refund_state'] == 2){
						


									
							$refund_recode_data = array();
							$refund_recode_data['order_id'] = $order_info['order_id'];
							$refund_recode_data['refund_amount'] = $order_info['pay_order_amount'];
							$refund_recode_data['refund_payment_amount'] = $order_info['refund_pay_amount'];
							$refund_recode_data['refund_wallet_amount'] = $order_info['refund_pd_amount'];
							$voucher_re = 'No';
							if($order_info['store_coupon_id'] > 0 || $order_info['platform_coupon_id'] > 0){
								$voucher_re = 'Yes';
							}

							$refund_recode_data['voucher_re'] = $voucher_re ;						
							$refund_amount =  $order_info['order_amount'];			
							$pdf = $pdf_model->createPdf($refund_recode_data,'refund_record');						
					
							//重制商家优惠券
							if($order_info['store_coupon_id'] > 0){




							}

							//重制平台优惠券
							if($order_info['platform_coupon_id'] > 0){

								//rpacket_state 1.

								$data_record = array();
								$data_record['order_id'] = $order_info['order_id'];
								$data_record['voucher_type'] = 'ME Platform voucher';
								$data_record['voucher_code'] = $order_info['platform_coupon_code'];            
								$data_record['voucher_price'] = $order_info['platform_coupon_price'];            
								$data_record['voucher_time'] = time();								
								model('order_record')->addRecord('voucher_issued',$data_record);
							}

							
							//执行退回到用户            
							//检查退回钱包的金额
							$pd_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
							if($pd_amount > 0){
								$member_wallet = model('predeposit');
								$data = array();
								$data['amount'] = $pd_amount;
								$data['order_sn'] = $order_info['order_sn'];
								$data['order_id'] = $order_info['order_id'];
								$data['member_id'] = $order_info['buyer_id'];
								$data['member_name'] = $order_info['buyer_name'];
								$member_wallet->changePd('order_refund',$data);
							}
							
 
							$pay_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
							if($pay_amount > 0){
								//执行stripe 退款
							}


							//违约金
							//扣除商家的违约金
							$store_wallet = model('store_wallet');		
							$data = array();
							$data['amount'] = $order_info['delivery_fee'];
							$data['order_sn'] = $order_info['order_sn'];
							$data['order_id'] = $order_info['order_id'];
							$data['store_id'] = $order_info['store_id'];
							$data['store_name'] = $order_info['store_name'];
							$store_wallet->changePd('liquidated_damage',$data);                   
	

							//生成违约金发票                    
							$invoice_data = array();
							$invoice_data['order_id'] = $order_info['order_id'];
							$invoice_data['store_id'] = $order_info['store_id'];
							$invoice_data['me'] = 1;	
							$invoice_data['refund_id'] = $refund_id;
							//	print_r($invoice_data);			
							$pdf = $pdf_model->createPdf($invoice_data,'liquidated_damage_receipt_merchant');


							//补贴给配送员
							$waiter_wallet = model('waiter_wallet');	
							$data = array();
							$data['amount'] = $order_info['delivery_fee'];
							$data['order_sn'] = $order_info['order_sn'];
							$data['order_id'] = $order_info['order_id'];
							$data['distributor_id'] = $order_info['distributor_id'];
							$data['distributor_name'] = $order_info['distributor_name'];              
							$waiter_wallet->changePd('delivery_fee_reimbursment',$data);


							//开补贴发票给平台                    
							$invoice_data = array();
							$invoice_data['order_id'] = $order_info['order_id'];
							$invoice_data['delivery_id'] = $order_info['distributor_id'];
							$invoice_data['me'] = 1;	
							$invoice_data['refund_id'] = $refund_delivery_id;
							//	print_r($invoice_data);			
							$pdf = $pdf_model->createPdf($invoice_data,'delivery_fee_reimbursement_invoice');

							//更新订单状态
							model('order')->editOrder(['order_state' => 0 ],['order_id' => $order_id]);

							//生成平台抽成



					}

					
					

					//已完成的订单全单退款
					if($order_info['order_state'] == 60 && $order_info['refund_state'] == 2){


						$refund_info = model('order_refund')->getRefundInfo(array('refund_id' => $refund_id));
						
						$refund_delivery_info = model('order_refund')->getRefundInfo(array('refund_id' => $refund_delivery_id));




						//执行商家退款
						$store_wallet = model('store_wallet');		
						$data = array();
						$data['amount'] 	= $refund_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];
						$data['store_id'] = $order_info['store_id'];
						$data['store_name'] = $order_info['store_name'];
						$store_wallet->changePd('order_refund',$data);   

							
						//生成tuikuan票据所需
						$invoice_data = array();
						$invoice_data['order_id'] = $order_info['order_id'];
						$invoice_data['store_id'] = $order_info['store_id'];
						$invoice_data['user_id'] = $order_info['buyer_id'];	
						$invoice_data['refund_id'] = $refund_id;
						//	print_r($invoice_data);			
						$pdf = $pdf_model->createPdf($invoice_data,'sales_credit_note_full');


						/**
						 * 配送费退款
						 * 
						 * 
						*/

						$waiter_wallet = model('waiter_wallet');
						$data = array();
						$data['amount'] = $refund_delivery_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];
						$data['distributor_id'] = $order_info['distributor_id'];
						$data['distributor_name'] = $order_info['distributor_name'];    
						$waiter_wallet->changePd('delivery_fee_refund',$data);
						

					
						//生成配送票据所需
						$invoice_data = array();
						$invoice_data['order_id'] = $order_info['order_id'];
					//	$invoice_data['store_id'] = $order_info['store_id'];
						$invoice_data['user_id'] = $order_info['buyer_id'];			
						$invoice_data['delivery_id'] = $order_info['distributor_id'];	
						$invoice_data['refund_id'] = $refund_delivery_id;
						//	print_r($invoice_data);				
						$pdf = $pdf_model->createPdf($invoice_data,'delivery_credit_note');	


						
						//执行退回到用户            
						//检查退回钱包的金额
						$pd_amount = $refund_info['refund_wallet_amount'] + $refund_delivery_info['refund_wallet_amount'];
						if($pd_amount > 0){				

							$member_wallet = model('predeposit');
							$data = array();
							$data['amount'] = $pd_amount;
							$data['order_sn'] = $order_info['order_sn'];
							$data['order_id'] = $order_info['order_id'];
							$data['member_id'] = $order_info['buyer_id'];
							$data['member_name'] = $order_info['buyer_name'];
							$member_wallet->changePd('order_refund',$data);

						}
						


						$pay_amount = $refund_info['refund_pay_amount'] + $refund_delivery_info['refund_pay_amount'];
						if($pay_amount > 0){
							//执行stripe 退款
						}

						
						//违约金


						//扣除商家的违约金
						$store_wallet = model('store_wallet');		
						$data = array();
						$data['amount'] = $refund_delivery_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];
						$data['store_id'] = $order_info['store_id'];
						$data['store_name'] = $order_info['store_name'];
						$store_wallet->changePd('liquidated_damage',$data);                   
		


						//生成违约金发票                    
						$invoice_data = array();
						$invoice_data['order_id'] = $order_info['order_id'];
						$invoice_data['store_id'] = $order_info['store_id'];
						$invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_delivery_id;	
						//	print_r($invoice_data);			
						$pdf = $pdf_model->createPdf($invoice_data,'liquidated_damage_receipt_merchant');
			

						//扣除违约金写入ME钱包

						$me_wallet = model('me_wallet');		
						$data = array();
						$data['amount'] = $refund_delivery_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];		
						$me_wallet->changePd('liquidated_damage',$data);                   


						//补贴给配送员
						$waiter_wallet = model('waiter_wallet');	
						$data = array();
						$data['amount'] = $refund_delivery_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];
						$data['distributor_id'] = $order_info['distributor_id'];
						$data['distributor_name'] = $order_info['distributor_name'];              
						$waiter_wallet->changePd('delivery_fee_reimbursment',$data);


						//从me钱包扣除
						$me_wallet = model('me_wallet');		
						$data = array();
						$data['amount'] = $refund_delivery_info['refund_amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];			
						$me_wallet->changePd('delivery_fee_reimbursment',$data);                   

	

						//开补贴发票给平台                    
						$invoice_data = array();
						$invoice_data['order_id'] = $order_info['order_id'];
						$invoice_data['delivery_id'] = $order_info['distributor_id'];
						$invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_delivery_id;	
						//	print_r($invoice_data);			
						$pdf = $pdf_model->createPdf($invoice_data,'delivery_fee_reimbursement_invoice');

						//全部执行完毕后。修改退款单为已退款
						model('order_refund')->editRefund(['order_id' => $order_info['order_id'],'refund_type'=> 2,'refund_stage' => 2,'refund_state' => 0],['refund_state' => 1]);


					}



				}

	}




	public function admin_order_refund($order_id,$goods_amount,$delivery_amount,$commission_amount,$refund_id = 0,$refund_delivery_id = 0){ //全单退款




			$pdf_model = model('pdf');
			$order_info = model('order')->getOrderInfoN(['order_id' => $order_id]);


            //3阶段得退款
            if($order_info['order_state'] == 60){


                //执行退款商家
                if($goods_amount['role']!='' && $goods_amount['amount'] > 0){
                
				
                    //执行商家退款
                    $store_wallet = model('store_wallet');		
                    $data = array();
                    $data['amount'] = $goods_amount['amount'];
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['store_id'] = $order_info['store_id'];
                    $data['store_name'] = $order_info['store_name'];
                    $store_wallet->changePd('order_refund',$data);                   

                    
                    //生成票据所需
                    $invoice_data = array();
                    $invoice_data['order_id'] = $order_info['order_id'];
                    $invoice_data['store_id'] = $order_info['store_id'];
                    $invoice_data['user_id']  = $order_info['buyer_id'];		
					$invoice_data['refund_id'] = $refund_id;
                        
                    //	print_r($invoice_data);			
                    $pdf = $pdf_model->createPdf($invoice_data,'sales_credit_note_full');



                    if($goods_amount['role'] == 'delivery'){ //骑手承担责任
                    
                        $waiter_wallet = model('waiter_wallet');                    
                        //扣除骑手的违约金
                        $data = array();
                        $data['amount'] = $goods_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['distributor_id'] = $order_info['distributor_id'];
                        $data['distributor_name'] = $order_info['distributor_name'];              
                        $waiter_wallet->changePd('liquidated_damage',$data);


                        //生成违约金发票                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['delivery_id'] = $order_info['distributor_id'];
						$invoice_data['refund_id'] = $refund_id;
                        $invoice_data['me'] = 1;	
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'liquidated_damage_receipt_courier');
            



                        //补贴给商家
                        $store_wallet = model('store_wallet');		
                        $data = array();
                        $data['amount'] = $goods_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['store_id'] = $order_info['store_id'];
                        $data['store_name'] = $order_info['store_name'];
                        $store_wallet->changePd('sales_reimbursement',$data);  


                        //开补贴发票给平台                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['store_id'] = $order_info['store_id'];
						$invoice_data['refund_id'] = $refund_id;
                        $invoice_data['me'] = 1;	
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'sales_reimbursement_invoice');
            
                    }



                    if($goods_amount['role'] == 'platform'){ //平台承担责任
                                    
                        //补贴给商家
                        $store_wallet = model('store_wallet');		
                        $data = array();
                        $data['amount'] = $goods_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['store_id'] = $order_info['store_id'];
                        $data['store_name'] = $order_info['store_name'];
                        $store_wallet->changePd('sales_reimbursement',$data);  


						//补贴给配送员
						$waiter_wallet = model('me_wallet');	
						$data = array();
						$data['amount'] = $goods_amount['amount'];
						$data['order_sn'] = $order_info['order_sn'];
						$data['order_id'] = $order_info['order_id'];
						$data['distributor_id'] = $order_info['distributor_id'];
						$data['distributor_name'] = $order_info['distributor_name'];              
						$waiter_wallet->changePd('sales_fee_reimbursment',$data);


                    
                        //开补贴发票给平台                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['store_id'] = $order_info['store_id'];
                        $invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_id;
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'sales_reimbursement_invoice');
            
                    }

					model('order_refund')->editRefund(['refund_id' => $refund_id],['refund_state' => 1]); //更新退款记录单

                }

            
				//退配送费

                if($delivery_amount['role'] !='' && $delivery_amount['amount'] > 0){

                    $waiter_wallet = model('waiter_wallet');
                    $data = array();
                    $data['amount'] = $delivery_amount['amount'];
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['distributor_id'] = $order_info['distributor_id'];
                    $data['distributor_name'] = $order_info['distributor_name'];
                                    
                    $waiter_wallet->changePd('delivery_fee_refund',$data);
                    
                    
                    //生成配送票据所需
                    $invoice_data = array();
                    $invoice_data['order_id'] = $order_info['order_id'];
                //	$invoice_data['store_id'] = $order_info['store_id'];
                    $invoice_data['user_id'] = $order_info['buyer_id'];			
                    $invoice_data['delivery_id'] = $order_info['distributor_id'];
					$invoice_data['refund_id']	 = $refund_delivery_id;
                    //	print_r($invoice_data);				
                    $pdf = $pdf_model->createPdf($invoice_data,'delivery_credit_note');			
            
                    if($delivery_amount['role'] == 'store'){ //商家承担责任
                                            
                        //扣除商家的违约金
                        $store_wallet = model('store_wallet');		
                        $data = array();
                        $data['amount'] = $delivery_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['store_id'] = $order_info['store_id'];
                        $data['store_name'] = $order_info['store_name'];
                        $store_wallet->changePd('liquidated_damage',$data);                   
        

                        //生成违约金发票                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['store_id'] = $order_info['store_id'];
                        $invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_delivery_id;
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'liquidated_damage_receipt_merchant');
            


                        //补贴给配送员
                        $waiter_wallet = model('waiter_wallet');	
                        $data = array();
                        $data['amount'] = $delivery_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['distributor_id'] = $order_info['distributor_id'];
                        $data['distributor_name'] = $order_info['distributor_name'];              
                        $waiter_wallet->changePd('delivery_fee_reimbursment',$data);


                        //开补贴发票给平台                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['delivery_id'] = $order_info['distributor_id'];
                        $invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_delivery_id;	
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'delivery_fee_reimbursement_invoice');
            
                    }


                    if($delivery_amount['role'] == 'platform'){ //平台承担责任
                    
                            
                        //补贴给配送员
                        $waiter_wallet = model('waiter_wallet');	
                        $data = array();
                        $data['amount'] = $delivery_amount['amount'];
                        $data['order_sn'] = $order_info['order_sn'];
                        $data['order_id'] = $order_info['order_id'];
                        $data['distributor_id'] = $order_info['distributor_id'];
                        $data['distributor_name'] = $order_info['distributor_name'];              
                        $waiter_wallet->changePd('delivery_fee_reimbursment',$data);

						 //补贴给配送员
						 $waiter_wallet = model('me_wallet');	
						 $data = array();
						 $data['amount'] = $delivery_amount['amount'];
						 $data['order_sn'] = $order_info['order_sn'];
						 $data['order_id'] = $order_info['order_id'];
						 $data['distributor_id'] = $order_info['distributor_id'];
						 $data['distributor_name'] = $order_info['distributor_name'];              
						 $waiter_wallet->changePd('delivery_fee_reimbursment',$data);
 
 


                        //开补贴发票给平台                    
                        $invoice_data = array();
                        $invoice_data['order_id'] = $order_info['order_id'];
                        $invoice_data['delivery_id'] = $order_info['distributor_id'];
                        $invoice_data['me'] = 1;	
						$invoice_data['refund_id'] = $refund_delivery_id;
                        //	print_r($invoice_data);			
                        $pdf = $pdf_model->createPdf($invoice_data,'delivery_fee_reimbursement_invoice');
            
                    }

					model('order_refund')->editRefund(['refund_id' => $refund_delivery_id],['refund_state' => 1]); //更新退款记录单

                }

				
                //执行退款，然后票据1天结束的时候生成
                if($commission_amount['role']!='' && $commission_amount['amount'] > 0){                

				//	print_r(333);

                    $me_wallet = model('me_wallet');
                    $data = array();
                    $data['amount'] = $commission_amount['amount']; 
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['distributor_id'] = $order_info['distributor_id'];
                    $data['distributor_name'] = $order_info['distributor_name'];		
                    $me_wallet->changePd('me_refund',$data);
                    

                    $store_wallet = model('store_wallet');
                    $data = array();
                    $data['amount'] = $commission_amount['amount'];
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['store_id'] = $order_info['store_id'];
                    $data['store_name'] = $order_info['store_name'];
                    $store_wallet->changePd('me_refund',$data);



					//开退款票                    
					$invoice_data = array();
					$invoice_data['store_id'] = $order_info['store_id'];
					$invoice_data['order_id'] = $order_info['order_id'];
					$invoice_data['me'] = 1;	
					//	print_r($invoice_data);			
					$pdf = $pdf_model->createPdf($invoice_data,'me_platform_credit_note_merchant');





					//print_r(333);

                }






                //执行退回到用户            
                //检查退回钱包的金额
                $pd_amount = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
                if($pd_amount > 0){

                    $member_wallet = model('predeposit');
                    $data = array();
                    $data['amount'] = $pd_amount;
                    $data['order_sn'] = $order_info['order_sn'];
                    $data['order_id'] = $order_info['order_id'];
                    $data['member_id'] = $order_info['buyer_id'];
                    $data['member_name'] = $order_info['buyer_name'];
                    $member_wallet->changePd('order_refund',$data);

                }

                $pay_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
                if($pay_amount > 0){
                    //执行stripe 退款
                    
					
                }


               // $data = array();
                //$data['refund_state'] = 1;
                

		


           
		//	 model('order_refund')->editRefund(['refund_id' => $refund_id],['refund_state' => 1]); //更新退款记录单

            }



	}




}

?>