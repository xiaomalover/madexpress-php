<?php
/**
 * 车辆维护模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_maintainModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_maintain');
    }

	
    public function vehicleMaintainList($condition, $field = '*', $page = 0, $order = 'maintain_id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

	
	
	public function vehicleMaintainInfo($where,$field='*'){		
		   return $this->field($field)->where($where)->find();
	}
	
	
	public function vehicleMaintainAdd($param)
    {
        return $this->insert($param);
    }

	
	 public function editMaintainVehicle($condition, $update) {
		 
        return $this->where($condition)->update($update);
		
    }
	
	
	
	
}
