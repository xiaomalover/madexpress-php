<?php
/**
 * 我的地址
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class order_shipping_typeModel extends Model {

    public function __construct() {
        parent::__construct('order_shipping_type');
    }



    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getTypeInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getTypeList($condition, $order = 'type_id desc'){
        $type_list = $this->where($condition)->order($order)->select();
       return $type_list;
    }

    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addType($param){
        return $this->insert($param);
    }
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editType($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delType($condition){
        return $this->where($condition)->delete();
    }
}
