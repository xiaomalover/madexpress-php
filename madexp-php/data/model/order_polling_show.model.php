<?php
/**
 * 订单轮询模型
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class order_polling_showModel extends Model{

    public function __construct(){
        parent::__construct('order_polling_show');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
	 
    public function getShowList($condition = array(), $page = '', $order = 'id asc', $field = '*', $limit = '') {
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getShowInfo($condition,$fields = '*',$order='id asc') {
        return $this->where($condition)->field($fields)->order($order)->find();
    }

    /*
     * 增加
     * @param array $data
     * @return bool
     */
    public function addShow($data){
        return $this->insert($data);
    }

    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editShow($data,$condition) {
        return $this->where($condition)->update($data);
    }
	

	

    public function getShowCount($condition) {
        return $this->where($condition)->count();
    }
	
	
	public function delShow($condition) {
        return $this->where($condition)->delete();
    }
	
	

}
