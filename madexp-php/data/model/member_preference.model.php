<?php
/**
 * 我的地址
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class member_preferenceModel extends Model {

    public function __construct() {
        parent::__construct('member_preference');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getMemberPreferenceInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getMemberPreferenceList($condition, $order = 'id desc'){
        $attr_list = $this->where($condition)->order($order)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addMemberPreference($param){
        return $this->insert($param);
    }
	
	
	/**
	 * 新增地址
	 *
	 * @param array $param 参数内容
	 * @return bool 布尔类型的返回结果
	 */
	public function addMemberPreferenceAll($param){
	    return $this->insertAll($param);
	}
	
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editMemberPreference($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delMemberPreference($condition){
        return $this->where($condition)->delete();
    }
	
	  /**
     * 取数量
     * @param unknown $condition
     */
    public function getMemberPreferenceCount($condition = array()) {
        return $this->where($condition)->count();
    }
	
}
