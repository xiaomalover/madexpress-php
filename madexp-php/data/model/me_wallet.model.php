<?php
/**
 * 平台钱包

 */
defined('InMadExpress') or exit('Access Invalid!');
class me_walletModel extends Model {
   
	
	
    /**
     * 取日志总数
     * @param unknown $condition
     */
    public function getPdLogCount($condition = array()) {
        return $this->table('me_pd_log')->where($condition)->count();
    }

	
    /**
     * 取得预存款变更日志列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdLogList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('me_pd_log')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }



	/**
	    * 删除日志
	    * @param unknown $condition
	    */
	   public function delPdLog($condition = array()) {
	       return $this->table('me_pd_log')->where($condition)->delete();
	   }
	
	

	
   
    /**
     * 变更余额
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();
		
		
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
        switch ($change_type){
           
						
			case 'platform_charges': //销售抽成&附加费
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = 'Platform Charges';
				
				$data_log['lg_name_of_activities'] = 'Platform Charges';
				$data_log['lg_type_activities'] = 0;
				
				
				$data_log['lg_order_sn'] = $data['order_sn'];	
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
				
				
			case 'incentive_cancelation': //激励金收入取消
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Incentive Cancelation";
				
				$data_log['lg_name_of_activities'] = 'Incentive Cancelation';
				$data_log['lg_type_activities'] = 0;				
				
				$data_log['lg_order_sn'] = $data['order_sn'];				
				$data_log['lg_order_id'] = $data['order_id'];				
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			
			case 'bonus_cancelation': //奖金取消
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Bonus Cancelation";
				
				$data_log['lg_name_of_activities'] = "Bonus Cancelation";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];	
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			
			case 'withdraw_fee': //取现手续费
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Withdraw Fee";
				
				$data_log['lg_name_of_activities'] = "Withdraw Fee";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			
			case 'container_delivery_fee': //餐盒配送费
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Container Delivery Fee";
				
				$data_log['lg_name_of_activities'] = "Container Delivery Fee";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 
			 
			 case 'other_platform_services_fee': //ME其他平台服务费
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Other Platform Services Fee";
				
				$data_log['lg_name_of_activities'] = "Other Platform Services Fee";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];				
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 case 'equipement_sales': //装备销售
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Equipement Sales";
				
				$data_log['lg_name_of_activities'] = "Equipement Sales";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 case 'rental_fee': //租车租金
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Rental Fee";
				
				$data_log['lg_name_of_activities'] = "Rental Fee";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];			
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 case 'me_wallet_top_up': //ME钱包充值
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "ME Wallet Top Up";
				
				$data_log['lg_name_of_activities'] = "ME Wallet Top Up";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 

			 case 'liquidated_damage': //违约金扣除
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Liquidated Damage";
				
				$data_log['lg_name_of_activities'] = "Liquidated Damage";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 



			 
			 /*******
			 扣除项
			 ******/
			 

			case 'delivery_fee_reimbursment': //发放配送费违约金
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_desc'] = "Delivery Fee Reimbursment";
				
				$data_log['lg_name_of_activities'] = "Delivery Fee Reimbursment";
				$data_log['lg_type_activities'] = 1;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 

			 case 'sales_fee_reimbursment': //发放销售违约金
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_desc'] = "Sales Reimbursment";
				
				$data_log['lg_name_of_activities'] = "Sales Reimbursment";
				$data_log['lg_type_activities'] = 1;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 




			 
			   case 'order_refund': //订单退款(全单)
					$data_log['lg_av_amount'] = -$data['amount'];					
					$data_log['lg_desc'] = 'Order Refund';
					$data_log['lg_admin_name'] = $data['admin_name'];
					
					
					$data_log['lg_name_of_activities'] = "Order Refund";
					$data_log['lg_type_activities'] = 1;
					
					
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit'.$data['amount']);
					$data_msg['av_amount'] =-$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 
			  case 'delivery_fee_refund': //推配送费(全单)
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Delivery Fee Refund';
					
					$data_log['lg_name_of_activities'] = "Delivery Fee Refund";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];	
					$data_pd['available_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0 ;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			  
			  case 'me_refund': //平台收入退款(包含全部类型的)
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'ME Refund (All Types)';
					
					$data_log['lg_name_of_activities'] = "ME Refund (All Types)";
					$data_log['lg_type_activities'] = 1;
					
			//		$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
				
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			  case 'me_store_refund': //购买装备退款
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'ME Store Refund';
					
					$data_log['lg_name_of_activities'] = "ME Store Refund";
					$data_log['lg_type_activities'] = 1;
					
			//		$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];				
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = 0;
					$data_msg['freeze_amount'] = -$data['amount'];
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			   case 'incentive_mpoig': //'MPOIG'的保底收入
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Incentive MPOIG';
					
					$data_log['lg_name_of_activities'] = "Incentive 'MPOIG'";
					$data_log['lg_type_activities'] = 1;
					
					
			//		$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 case 'incentive_schedule_urself': //奖金取消
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = "Incentive 'Schedule Urself'";
					
					$data_log['lg_name_of_activities'] = "Incentive 'Schedule Urself'";
					$data_log['lg_type_activities'] = 1;
					
				//	$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 case 'platform_bonus': //ME平台奖金
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Platform Bonus';
					
					$data_log['lg_name_of_activities'] = "Platform Bonus";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = 0;
					$data_msg['freeze_amount'] = -$data['amount'];
					$data_msg['desc'] = $data_log['lg_desc'];
              break; 

					
			  case 'voucher_redeem': //优惠券补贴
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'ME Voucher Redeem';
					
					$data_log['lg_name_of_activities'] = "ME Voucher Redeem";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
					
			 case 'withdraw': //取现金额
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Withdraw';
					
					$data_log['lg_name_of_activities'] = "Withdraw";
					$data_log['lg_type_activities'] = 1;
					
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];	
					$data_log['lg_order_id'] = $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] =  -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
							
			 case 'rental_fee_refund': //租车租金退款
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Rental Fee Refund';
					
					$data_log['lg_name_of_activities'] = "Rental Fee Refund";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];				
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
					
					  
			
            default:
                throw new Exception('参数错误');
                break;
        }

       // print_r($data_log);
        $update = Model('me')->editMe(array('me_id'=>1),$data_pd);

        if (!$update) {
            throw new Exception('操作失败');
        }
	//	print_r($data_log);
        $insert = $this->table('me_pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }
		
		
		/*
        // 支付成功发送买家消息
        $param = array();
        $param['code'] = 'predeposit_change';
        $param['member_id'] = $data['member_id'];
        $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
        $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
        $param['param'] = $data_msg;
        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
        'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
		
        QueueClient::push('sendMemberMsg', $param);
		*/
		
        return $insert;
    }

    

}
