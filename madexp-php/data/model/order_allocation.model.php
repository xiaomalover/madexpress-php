<?php
/**
 *
 *订单分配核心
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class order_allocationModel extends Model
{
   
   
   
   
   //推送给商家
    public function sendStoreOrder($order_sn= '', $is_new=0)
    {
        $where = array();
        $where['order_sn'] = $order_sn;
        $order_info = model('order')->getOrderInfo($where);
        require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
        $gateway = new \GatewayClient\Gateway();
        $gateway::$registerAddress = '47.243.54.204:1238';
                
        $data = array();
        $data['type'] = 'order_notification';
        $data['data'] = $order_info;
              
        // print_r($order_info);
        $data = json_encode($data);
        $gateway::sendToUid($order_info['store_id'], $data);
    }
   
   
   
    /*骑手接单
    deliver_id 配送员ID
    order_sn 订单sn
    */
   private  function accept($deliver_id,$order_sn){
   		
   		
   		//print_r($order_sn);
   	//	print_r($deliver_id);
   		
   		$stoket =  model('stoket');		
   		$model = model('order');				
   		$deliver_id = $deliver_id;		
   		$deliver_info = model('waiter')->getWaiterInfo(array('distributor_id'=>$deliver_id));
   		$where = array(		
   			'order_sn' =>$order_sn		
   		);
   		
   		if(empty($order_sn)){		      		    
			return array('msg' =>'没有可接的单子','msg_code' => 1 );
   		}
   		$order_info = $model->getOrderInfo($where); 	
   	//	print_r($order_info['order_state']);
   	//	print_r('|');
   	//	print_r($order_info['distributor_id']);
   	//	print_r('###');
   		if($order_info['order_state'] == 30 && $order_info['distributor_id'] > 0  ){   		   
			return array('msg' =>'异常','msg_code' => 2);
   		}else{		
   			$count = $this->deliverOrderCount($deliver_info['distributor_id']);		

            if($count == 0 ){ //本批次第一次接单后，更新批次
                $batch_code = date('dmy',time()).rand(100,999);
                model('waiter')->editWaiter(['distributor_id' =>$deliver_info['distributor_id']],['batch_code' => $batch_code]);
                $deliver_info['batch_code'] = $batch_code;   
            }

            $delivery_latlng = explode(',',$deliver_info['region_coordinate']);
   			$data = array(
   				'distributor_id'		=> $deliver_info['distributor_id'],
   				'distributor_name'		=> $deliver_info['distributor_name'],
   				'distributor_mobile'	=> $deliver_info['distributor_mobile'],	
   				'distributor_start_time'=> time(),
   				'order_state' => 30,
   				'distributor_code'      => $deliver_info['distributor_code'],
				'distributor_store_distance' => $this->getDistance($order_info['store_lat'], $order_info['store_lng'], $delivery_latlng[1], $delivery_latlng[0]), //计算当前到商家的距离。
				'distributor_store_time' => time() + 300, //暂时计算5分钟内没到店就问题单
				'socket_store_send_time' => time(),
                'batch_code' => $deliver_info['batch_code'],
                'daycode' => $count + 1
			);			
			
   			$where = array(
   				'order_sn' => $order_sn
   			);
   			$row = $model->editOrder($data,$where);	
   			if($row){	
   			 
                
                //更新配送员接单数量

                //model('waiter')->editWaiter(['distributor_id' => $delivery_info['distributor_id']])

					 




   			   	$order_info = $model->getOrderInfo($where); 	
   			 			 
				//写入短消息				
				// 发送买家消息
				$param = array();
				$param['code'] = 'order_delivery_received';
				$param['member_id'] = $order_info['buyer_id'];
				$param['param'] = array(			
					'order_sn' => $order_info['order_sn'],
					'id' => $order_info['order_id']
				);
				QueueClient::push('sendMemberMsg', $param);
					
			 
			 
				//推送给用户				
				
				$data = array();
				$data['type'] = 'order_update';
				$data['data'] = $order_info;
				$data = json_encode($data);
			//	$stoket->sendUser($order_info['buyer_id'], $data);
				    
				
   				//推送给当前商户
           		$data = array();
           		$data['type'] = 'order_notification';
           		$data['data'] = $order_info;                   // print_r($order_info);
                $data = json_encode($data);	
   			//	$stoket->sendStore($order_info['store_id'],$data);
   				
                   
				
   				//推送给骑手
   				$data = array();
           		$data['type'] = 'order_update';
           		$data['data'] = $order_info;
                   //print_r($data);
                $data = json_encode($data);	
   			//	$stoket->sendDelivery($deliver_id,$data);
   				
   				  
   				//写入配送员接单  
   		        $record_data = array();
   			    $record_data['order_id'] = $order_info['order_id'];
   			    $record_data['delivery_code']       =  $deliver_info['distributor_code']; //配送员编号

                //获取配送员当前第几单
                                
   				$record_data['order_code']          =   $this->deliverOrderCount($deliver_info['distributor_id']);//当前批次第几单
                $record_data['batch_code']          = $deliver_info['batch_code'];
   				$record_data['send_delivery_time']  = time(); //推送给配送员的时间
   			    model('order_record')->addRecord('order_confirmed_courier',$record_data);
   			
   				
   				
   				$data = array();
   				$data['order_id'] = $order_info['order_id'];
   				$data['log_role'] = 'waiter';
   				$data['log_msg'] = '送餐员已接单';
   				$data['log_user'] = $order_info['distributor_id'];
   			
   				$data['log_orderstate'] = 30;
   				$model->addOrderLog($data);
   			
                



				//(merchant)商家展示订单
				$data_record = array();
				$data_record['order_id'] = $order_info['order_id'];
				$data_record['send_store_time'] = time();		
				model('order_record')->addRecord('order_display',$data_record);

				

   			
   				//写入测试				
   			   /*  $log = array(
   			   		'order_sn' => $order_info['order_sn'],
   					'log_msg' => '已接单：'.$store_info['distributor_name'].'接单:'.$order['order_sn'],
   					'log_time' => time()
   			     );   				 
   				 model('stoket_log')->addSlog($log);*/
   				
   				 //删掉当前接的单子
   				 model('order_polling')->delPolling(array('order_sn' => $order_info['order_sn']));	
   				 $deliver_list = array();
   				
   				 //查询当前跟本配送员的所有订单
   				 $deliver_order = model('order_polling')->getPollingList(array('waiter_id'=>$deliver_id));
   					 
   				//print_r($deliver_list);
   				 //删除推送记录
   				 model('order_polling_show')->delShow(array('order_sn' => $order_info['order_sn']));
   				 
   				$deliver_ordernew = array();
   			//	print_r($deliver_order);
   				foreach($deliver_order as $v){
   				 	$deliver_order_new = model('order_polling')->getPollingList(array('order_sn'=>$v['order_sn']));
   				//	print_r($deliver_order_new);	
   					$deliver_ordernew[$v['order_sn']] = $v['order_sn'];
   				 	foreach($deliver_order_new as $n){
   				 		 $deliver_list[$n['waiter_id']] = $n['waiter_id'];
   				 	}
   				 	$is_show = model('order_polling_show')->getShowInfo(array('order_sn' => $v['order_sn']));
   				 
   				 	if(empty($is_show)){				 	
   				 	//把其他订单恢复到初始状态
   						model('order')->editOrder(array('order_polling_show'=> 0),array('order_sn' => $v['order_sn']));						
   				 	}					
   					model('order_polling')->delPolling(array('order_sn' => $v['order_sn']));
   					
   				 }   				 
   				 
   				//查询当前接了接了几单
   			 	$count = $this->deliverOrderCount($deliver_id);						
   				if($count == 3){					
   					$where = array(
   						'waiter_id' => $deliver_id
   					);
   					$order_list = model('order_polling_show')->getShowList($where);	
   					foreach($order_list as $v){
   						$order_id[] = $v['order_id'];
   					}	
   					if(is_array($order_id)){						
   						$order_ids = implode(',',$order_id);
   						$where = array(
   							'order_id' => array('in',$order_ids)
   						);						
   						model('order')->editOrder(array('order_polling_show'=> 0),$where);					
   					}
   					
   					
   					//清空本单列表
   					model('order_polling')->delPolling(array('waiter_id' => $deliver_id));					 
   					//删除推送记录
   					model('order_polling_show')->delShow(array('waiter_id' =>  $deliver_id));
   					
   					//配送员状态改成配送中
   					$where = array(
   						'distributor_id' => $deliver_id
   					);
   					$data =  array(
   						'delivery_state' => 1
   					);		
   					model('waiter')->editWaiter($where,$data);
   				
				
					//写入开始配送LOG					
					$data = array();
					$data['order_id'] = $order_info['order_id'];
					$data['log_role'] = 'waiter';
					$data['log_msg'] = '送餐员开始配送';
					$data['log_user'] = $order_info['distributor_id'];
				
					$data['log_orderstate'] = 51;
					$model->addOrderLog($data);
					   			
					
   										
   				    //接满3单后推送开始取餐
       				$data = array();
               		$data['type'] = 'start_pickup';
               		$data['data'] = array();
                        // print_r($order_info);
                    $data = json_encode($data);	
               //     $stoket->sendDelivery($order_info['distributor_id'],$data);						
   				}
   					
   				
   				if($order_info['store_capacity'] == 1){
							        //商家自动接单
				   $this->store_order_accept($order_info['order_id']);
				}
						
   					
   					
   									
   		    	return array('msg' =>'成功','msg_code' => 0);		
   			}else{					
				return array('msg' =>'接单失败','msg_code' => 3);	   				
   			}
   		}					
   				
   	}
  
	

	
	    
	    /**
	    * 商家接单
	    */
	    public function store_order_accept($order_id)
	    {
	   
	        $model_order = Model('order');
	        $stoket =  model('stoket');
	    
	        $condition = array();
	        $condition['order_id'] = $order_id;
	     //   $condition['store_id'] = $this->store_info['store_id'];
	        $order_info = $model_order->getOrderInfo($condition);
	
	        if (!empty($order_info)) {
	            $data = array(
	                'order_state' => 40
	            );
	            //推送消息。
	            $row = $model_order->editOrder($data, $condition);
	            if ($row) {
	                
					
					
					//商家接单
					$record_data = array();
					$record_data['order_id'] = $order_info['order_id'];
					$record_data['store_taking_time']       =  time(); //配送员编号
					$record_data['store_taking_type']          = 'Auto';//当前批次第几单			
					model('order_record')->addRecord('order_confirmed_merchant',$record_data);
					   			
					
	                
	                // 发送买家消息
	                $param = array();
	                $param['code'] = 'order_store_received';
	                $param['member_id'] = $order_info['buyer_id'];
	                $param['param'] = array(
	                    'order_sn' => $order_info['order_sn'],
	                    'id' => $order_info['order_id']
	                );
	                QueueClient::push('sendMemberMsg', $param);
	                    
	                
	                
	                $data = array();
	                $data['order_id'] = $order_info['order_id'];
	                $data['log_role'] = 'seller';
	                $data['log_msg'] = '商家已接单';
	                $data['log_user'] = $order_info['store_id'];
	             
	                $data['log_orderstate'] = 40;
	                $model_order->addOrderLog($data);
	
	                //推送给用户
	                $data = array();
	                $data['type'] = 'order_update';
	                $data['data'] = $order_info;
	                $data = json_encode($data);
	             //   $stoket->sendUser($order_info['buyer_id'], $data);
	
	
	                                
	                            
	                if ($order_info['order_type'] == 1) { //自提单不需要推送给骑手
	                    $data = array();
	                    $data['type'] = 'order_update';
	                    $data['data'] = $order_info;
	                    //print_r($order_info);
	                    $data = json_encode($data);
	                 //   $stoket->sendDelivery($order_info['distributor_id'], $data);
	                }
	                
					
					
	                //且慢恢复
	                //model('store')->editStore(['store_is_delay' => 0], ['store_id' => $this->store_info['store_id']]);
	                
	                //恢复标记且慢的单子
	              //  model('order')->editOrder(['is_delay' => 0 ], ['store_id' => $this->store_info['store_id'],'order_state' => 30, 'is_delay' => 1]);
	                
	                
	                return true;  // output_data('ok');
	            }
	        } else {
	                return false; // output_error('订单不存在');
	        }
	    }
	    
	    

    
    //拒绝
    public function refuse($order_sn =0, $delivery_id = 0, $batch_code = 0, $sch_id = 0)
    {
        $stoket = model('stoket');
        $model  = model('polling_refuse');
        //$this->getWaiterNew($order_sn);
                
        $count = model('order_polling')->getPollingCount(array('order_sn'=>$order_sn));
        if ($count <= 1) {
            output_error('此单必接');
        }
                
                
        //写入拒绝表
        $count = $this->deliverOrderCount($delivery_id);
        if ($count > 0) {
            $fraction = 0;
        } else {
            $fraction = 1;
        }
        //判断分值
        $data = array(
                    'waiter_id' => $delivery_id,
                    'order_sn' => $order_sn,
                    'batch_code' => $batch_code,
                    'fraction' => $fraction
                );
                
        $row = $model->addRefuse($data);
        if ($row) {
            $where = array();
            $where['batch_code'] = $batch_code;
            $where['waiter_id'] = $delivery_id;
            $total = $model->getRefuseInfo($where, 'SUM(fraction)');
            if ($total > 3) {//踢下线
                if ($sch_id > 0) {
                    $update = array();
                    $update['value'] = 2;
                    $update['content'] =  '没有订单下一直拒绝订单，被强制下线';
                    $where = array();
                    $where['sch_id'] = $sch_id;
                    $where['code'] = 'task_offline';
                    model('scheduling_task_log')->editTaskLog($update, $where);
                }
                        
                //修改配送员为
                $where = array();
                $where['distributor_id'] = $delivery_id;
                $data = array();
                $data['distributor_online'] = 10;
                model('waiter')->editWaiter($where, $data);
            } else { //标记可疑
                        
                $data = array();
                $data['distributor_id'] = $delivery_id;
                $data['desc'] ='拒单:'.$order_sn;
                $data['add_time'] = time();
                $data['state'] = 0;
                model('waiter_suspicious')->addWaiterSus($data);
            }
        }
                            
                            
        $where = array(
                    'order_sn' => $order_sn
                );
                
        model('order_polling')->delPolling($data);
        //删除推送记录
        model('order_polling_show')->delShow(array('order_sn' => $order_sn,'waiter_id' => $delivery_id));
            
        $update = array();
        $update['socket_send_delivery_id'] = 0; //重置配送员ID
                $update['order_polling_show'] = 0; //重置轮训
                $update['socket_send_time'] = 0; //重置推送时间
                $row = model('order')->editOrder($update, array('order_sn'=>$order_sn));
        if ($row) {
            $this->orderReload($delivery_id);
            return array('code' => 200);
        } else {
            return array('code' => 400);
        }
    }
            
    
    
    //取餐
    public function takeOrder($order_sn, $delivery_id)
    {
        $model = model('order');
        $stoket = model('stoket');
        $where = array(
                    'order_sn' => $order_sn
                );
        $data = array(
                    'order_state' => 50
                );
        $row =  $model->editOrder($data, $where);
        if ($row) {
            $order_info = $model->getOrderInfoN($where);
                    
			
			
			// 发送买家消息
			$param = array();
			$param['code'] = 'order_delivery_take';
			$param['member_id'] = $order_info['buyer_id'];
			$param['param'] = array(			
				'order_sn' => $order_info['order_sn'],
				'delivery_time' => date('d/m/y H:i',time()),
				'id' => $order_info['order_id']
			);
			QueueClient::push('sendMemberMsg', $param);
				
					
					
					
            //配送员已取餐
            $record_data = array();
            $record_data['pick_type'] = 'merchant';
            $record_data['pick_time'] =  TIMESTAMP;
            $record_data['order_id'] =  $order_info['order_id'];
            model('order_record')->addRecord('order_collected', $record_data);
                                                                    
                    
            $stoket = model('stoket');
            $data = array();
            $data['type'] = 'order_take';
            $data['data'] = $order_info;
            $data = json_encode($data);
           // $stoket->sendStore($order_info['store_id'], $data);
                        
            //查询是否还有可以取的餐
            $where = array(
                        'distributor_id' => $delivery_id,
                        'order_state' => 40
                    );
                        
            $count = $model->getOrderCount($where);
            if ($count == 0) {
               
				//如果没有。状态变成配送中
                $where = array();
                $where['distributor_id'] = $delivery_id;
                $data = array();
                $data['delivery_state'] = 2;
                model('waiter')->editWaiter($where, $data);
            
				//通知他开始配送了。
                        
                $data = array();
                $data['type'] = 'start_delivery'; //开始配送标记
                $data['data'] = array();
                $data = json_encode($data);
               // $stoket->sendDelivery($delivery_id, $data);
                        
				//通知用户				
				$data = array();
				$data['type'] = 'order_update';
				$data['data'] = $order_info;
				$data = json_encode($data);
			//	$stoket->sendUser($order_info['buyer_id'], $data);
				 		
						
				$order_list = model('order')->getOrderListNew(['distributor_id' =>$delivery_id,'batch_code' =>  $this->waiter_info['batch_code']]);							
                foreach($order_list as $v){
                  $batch_code = $this->waiter_info['batch_code']; //批次编号定义 用户号+天+第一单id
                    //写入log 手动接单	
                            
                    $order_num = $model->getOrderCount(array(
                                    'distributor_id' => $delivery_id,
                                    'batch_code' => $batch_code
                    ));                            
                
                    $record_data = array();
                    $record_data['order_num'] = $order_num; //当前批次有多少订单
                    $record_data['batch_code'] =  $batch_code; //本批次编号
                    $record_data['start_delivery_time'] = TIMESTAMP;
                    $record_data['order_id'] = $v['order_id'];
                    model('order_record')->addRecord('start_delivery', $record_data);
                }
            }



                            
                            
            $data = array();
            $data['order_id'] = $order_info['order_id'];
            $data['log_role'] = 'waiter';
            $data['log_msg'] = '送餐员已到店取货';
            $data['log_user'] = $order_info['distributor_id'];
            $data['log_orderstate'] = 50;
            $model->addOrderLog($data);
                                                
                            
            output_data('操作成功');
        } else {
            output_error('失败');
        }
    }
    
    
    
    public function giveOrder($order_sn, $deliver_id)
    { 
		//确认送达
                
        $stoket = model('stoket');
        $where = array(
                        'order_sn' => $order_sn,
						'distributor_id' => $deliver_id
                );
				
		$order_info = model('order')->getOrderInfoN($where);
		if(empty($order_info)){
			output_error('订单不存在');
		}  
	
        $data = array(
           'order_state' => 60,
           'order_update_time' => TIMESTAMP,
           'distributor_end_time' => TIMESTAMP //送达时间                        
        );		
        $row =  model('order')->editOrder($data, $where);
        if ($row) {
				      
				

                //更新商家已完成订单数据
                $update = array();
                $update['order_state_wancheng'] = array('exp','order_state_wancheng + 1');
                model('store')->editStore($update,['store_id' => $order_info['store_id']]);

				
				// 发送买家消息
				$param = array();
				$param['code'] = 'order_delivery_success';
				$param['member_id'] = $order_info['buyer_id'];
				$param['param'] = array(			
					'order_sn' => $order_info['order_sn'],
					'delivery_time' => date('d/m/y H:i',time()),
					'id' => $order_info['order_id']
				);
				QueueClient::push('sendMemberMsg', $param);
					
					  
            
                $data_record = array();
                $data_record['order_id'] = $order_info['order_id'];
                $data_record['delivery_time'] = time();
                model('order_record')->addRecord('order_delivered',$data_record);
            		
            					
            	$data = array();
            	$data['order_id'] = $order_info['order_id'];
            	$data['log_role'] = 'waiter';
            	$data['log_msg'] = '订单已送达';
            	$data['log_user'] = $order_info['distributor_id'];   			
            	$data['log_orderstate'] = 60;
            	model('order')->addOrderLog($data);
            					
            		
            			
            	$data = array();
            	$data['type'] = 'order_arrive';
            	$data['data'] = $order_info['order_sn']; 						
            	$data = json_encode($data);							
            //	$stoket->sendStore($order_info['store_id'],$data);
            	
            	
            	//推送给骑手
            	$data = array();
            	$data['type'] = 'order_update';
            	$data['data'] = $order_info['order_sn'];  
            	$data = json_encode($data);	
            //	$stoket->sendDelivery($order_info['distributor_id'],$data);
            		
					
            	//推送给用户						
            	$data = array();
            	$data['type'] = 'order_update';
            	$data['data'] = $order_info['order_sn'];
            	$data = json_encode($data);	
            //	$stoket->sendUser($order_info['buyer_id'],$data);
            		
								
				
                  
            //查询是否还有可以正在配送中的
            $where = array(
                            'distributor_id' => $deliver_id,
                            'order_state' => 50
              );
						
            $count = model('order')->getOrderCount($where);
            if ($count == 0) {                            
               //检查是否在钱圈内。如果在状态修改成 0 听单中 如果不在则状态是3 返回中
                $qian = $this->deliveryQian($deliver_id);
                if ($qian) {
                    $delivery_state = 0;
                } else {
                    $delivery_state = 3;
                }
                            
                //如果没有。状态变成配送中
                $where = array();
                $where['distributor_id'] = $deliver_id;
                $data = array();
                $data['delivery_state'] = $delivery_state;
                $row = model('waiter')->editWaiter($where, $data);
                if ($row && $delivery_state == 3) {                                    
                    //返回钱圈通知
                    $data = array();
                    $data['type'] = 'delivery_back';
                    $data['data'] = '';
                    $data = json_encode($data);
                //    $stoket->sendDelivery($deliver_id, $data);
                }
            }
			
			
            //生成相关记录
            model('order_bill')->bill($order_info['order_id']);            		
            //更新当前店铺所在的分类，更新销量			
            model('category')->cate_sale_num($order_info['store_id']);
                
				
				
            output_data('ok');
        } else {
            output_error('失败');
        }
    }
    
    
    //检查是否跟某个钱圈内
    
    private function deliveryQian($delivery_id)
    {
        $where = array();
        $where['distributor_id'] = $delivery_id;
        $delivery_info = model('waiter')->getWaiterInfo($where, 'region_coordinate,distributor_id');
        
        //145.003877,
        $model = model('store');
        $region =  model('region')->getRegionList(array('is_money'=> 1));
        foreach ($region as $k =>  $v) {
            $abc = $this->points_array($delivery_info['region_coordinate']);
            $row = 	$this->is_point_in_polygon($points, $abc);
            if ($row === true) {
                return true;
            }
        }
    }
    
	
	
	
    
    //模拟功能
    private function orderReload($deliver_id = 0, $is_new = 0)
    {
        $store = $this->getStore(1);
        $data = array();
        foreach ($store as $s) {
            foreach ($s['store_delivery'] as $d) {
                if ($d['distributor_id'] == $deliver_id) {
                    $data[] = $s['store_id'];
                }
            }
        }
                
                    
        $store_id  = implode(',', $data);
        //刷新这些订单
        $order = model('order')->getOrderListNew(array('order_polling'=>array('in','0,1,2'),'store_id'=>array('in',$store_id),'order_state'=> 20,'order_polling_show'=> 0), 'order_id,order_sn,store_id', 'order_id asc');
                    
        foreach ($order as $v) {
            model('order_polling')->delPolling(array('order_sn' =>$v['order_sn']));
            $this->sendDeliveryOrder($v['order_sn'], $is_new);
        }
                                            
        foreach ($order as $v) {
            $model = model('order_polling');
            $where = array(
                                'order_sn' => $v['order_sn']
                            );
            $list = $model->getPollingList($where, '*', 'order_sort desc,order_id asc');
                            
            $this->stocketShow($list[0]);
        }
    }
    
        
    
    //获取商家
    public function getStore($type = 0)
    {
        $state= array(
                0 => '未上架',
                1 => '营业中',
                2 => '已打烊',
            );
        $color = array('#f40606','#0000ff','#009245','#ff00ff','#754c24','#f7931e');
        $where = array();
        $where['store_state'] = array('in','1,2');
        $store = model('store')->getStoreList($where);
            
        foreach ($store as $k => $v) {
            $data[$k]['store_id'] = $v['store_id'];
            $data[$k]['store_name_primary'] = $v['store_name_primary'];
            $data[$k]['store_lat'] = $v['store_lat'];
            $data[$k]['store_lng'] = $v['store_lng'];
            $data[$k]['store_superpose'] = $this->getStoreOverlap($v['store_superpose']);
            $data[$k]['store_state_name'] = $state[$v['store_state']];
            $data[$k]['store_state'] = $v['store_state'];
                
            $delivery = $this->getStoreDeliver(array('store_lat'=>$v['store_lat'],'store_lng'=> $v['store_lng'],'store_id'=>$v['store_id']));
                
            $data[$k]['store_delivery'] = $delivery['deliver'];
            $data[$k]['store_delivery_num'] = $delivery['count'];
            $data[$k]['store_color'] = $color[$k];
        }
            
        return $data;
    }
    
    
    //叠加商户名称
    private function getStoreOverlap($store_id)
    {
        if (empty($store_id)) {
            return '';
        }
                    
        $where = array(
                'store_id' => array('in',$store_id)
            );
            
        $store = model('store')->getStoreList($where);
        $store_name = array();
        foreach ($store as $v) {
            $store_name[] = $v['store_name_primary'];
        }
        return implode(',', $store_name);
    }
    
	
	
    //检查订单池遗留下来的订单并推送
    private function orderPool()
    {
        $where = array(
                'order_polling' => array('in','0,1,2'),
                'order_polling_show' => 0
            );
        $order = model('order')->getOrderListNew($where, '*', 'order_id asc');
        foreach ($order as $v) {
            model('order_polling')->delPolling(array('order_sn'=>$v['order_sn']));
            $this->sendDeliveryOrder($v['order_sn'], 1);
        }
    }
       
   
   
   
   
    //推送
    public function sendDeliveryOrder($order_sn = '', $is_new = 0)
    {
        $stoket =  model('stoket');
        $order_info = model('order')->getOrderInfo(array('order_sn'=>$order_sn));
        if (empty($order_info)) {
			return array('msg' => '订单不存','msg_code' => 1);
            
        }
        $store_latlng = explode(',', $order_info['store_coordinate']);
        //print_r($store_latlng);
        $delivery = $this->sendOrderDeliver($store_latlng, $order_info);
        //	print_r($delivery);
        foreach ($delivery as $v) {
            $deliver_sort = $this->deliverySort($v['distributor_id'], $order_info);
            $order_sort = $this->deliverOrderSort($order_info, $v['distributor_id']);
            //计算当前店铺是不是骑手的叠加单店铺。
            $superposition = $this->getOrderSuperpositionStore($v['distributor_id'], $order_info['store_id']);
            //print_r($superposition);
            //echo '单量'.$count;
            //检查当前 配送员 是否有单
            //没有订单，则分配个商户围栏内的配送员
            //如果有一单，检查是否为本商家的订单，
            //如果有一单，检查是否为叠加范围内商户，并判断他是否在叠加商户记录内。
            //检查本单是否与上一单超过6分钟。
            $diffTime = $this->checkDiffTime($v['distributor_id'], $order_info);
            //检查是否已拒绝过本单
           // $refuse = $this->checkRefuse($order_info['order_sn'], $v['distributor_id']);
            //echo '拒绝'.$refuse;
            //检查本单是否为列队等待单
            /*	echo $count;
                echo '|';
                echo $refuse;
                echo '|';
                echo $overlying;
                echo '|';
                echo $diffTime;
                echo '|';
                */
            //	print_r($delivery_sort.'|');
            //	print_r($superposition.'|');
            //	print_r($refuse.'|');
            //	print_r($diffTime.'|');
                
            $count = $this->getDeliveryOrderNum($v['distributor_id']);
                
            //根据订单获取16边 可用配送员。
            $polygonCheck = $this->polygon_delivery($v['distributor_id'], $order_info);
                            
            //$refuse == 0  取消拒单这里判断就不需要了 
            if ($diffTime == 0 && ($superposition == 1 || $superposition == 0) && $polygonCheck == 0) {
					
					$polling_data[] = array(
                        'order_id' => $order_info['order_id'],
                        'order_sn' => $order_info['order_sn'],
                        'waiter_id' => $v['distributor_id'],
                        'state' => 0,
                        'addtime' => time(),
                        'last_time' => $this->deliver_last_time($count, $v['distributor_id']),
                        'deliver_sort' => $deliver_sort,
                        'order_sort' => $order_sort,
                        'store_name' => $order_info['store_name']
                    );
                                
                //print_r($polling_data);
                                    
                //写入lOG
                $log = array(
                        'order_sn' => $order_info['order_sn'],
                        'log_msg' => '轮询：'.$v['distributor_name'].'，单号：'.$order_info['order_sn'],
                        'log_time' => time()
                    );
					
                model('stoket_log')->addSlog($log);
				
            }
        }
            
        $row = model('order_polling')->addPollingAll($polling_data);            
        if (count($polling_data) > 0) {
            //如无可用运力。直接到订单池
            $data = array(
                    'order_polling' => 1
            );
        } else {
            $data = array(
                    'order_polling' => 2
            );
        }
        $where = array(
            'order_sn' => $order_info['order_sn']
        );
        model('order')->editOrder($data, $where);
        if ($row) {			
			
				print_r($order_info['order_sn'].'|'.$is_new );
				$this->stocketSend($order_info['order_sn'], 1); //给骑手推送订单
			
        }
    }
    
    

    
    
    
    //获取配送员手里的单子
    public function getDeliveryOrderNum($delivery_id)
    {
        $where = array(
            'distributor_id' => $delivery_id,
            'order_state' => array('in','30,40,50')
        );
        $order_count = model('order')->getOrderCount($where);
        return $order_count;
    }
    
    //检查此订单是否被该用户拒绝过
    private function checkRefuse($order_sn, $waiter_id)
    {
        $where = array(
                'order_sn' => $order_sn,
                'waiter_id' => $waiter_id
            );
        $count = model('polling_refuse')->getRefuseCount($where);
        return $count;
    }
    
    
    //距离上一单是否超过6分钟
    private function checkDiffTime($waiter_id, $order)
    {
        $where = array(
                        'distributor_id' => $waiter_id,
                        'daycode' => 1,
                        'order_state' => array('in','30,40')
                    );
        $info = model('order')->getOrderInfoN($where);
        if (!empty($info)) {
            $old_time = $info['add_time'];
            $new_time = $order['add_time'];
            $diff = $new_time - $old_time;
            if ($diff > 360) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
    
    
    
    //计算当前订单 当前配送员是否可接 16变形计算
    private function polygon_delivery($delivery_id, $order_info )
    {
        $model = model('order');
        //检查当前手里的订单
        $where = array();
        $where['distributor_id'] = $delivery_id;
        $where['order_state'] = array('in','30,40,50');
        $order_count = $model->getOrderCount($where);
        $delivery_order_list = $model->getOrderListNew($where, 'region_sector_id,store_bind_sector_id,region_sector_is_center');
                
               
        if ($order_count == 0) {
            return 0;
        }
		
		//判断订单商家是否绑定的同一个区域
		if($order_info['store_bind_sector_id'] != $delivery_order_list[0]['store_bind_sector_id'] ){		
			return 1;				
		}
				
		
        //有1单。在中心区域。无需匹配。其他16个区域都可接。
        //有1单。不在中心区域。可接 相关3个区域+中心区域
        
        if ($order_count == 1) {
            $delivery_order_info = $delivery_order_list[0];
            
            if ($delivery_order_info['region_sector_is_center'] == 1) {
                return 0 ;
            }
            $sector_id = $delivery_order_info['region_sector_id']; //改成code了
			
            $centre_info= model('region_sector')->getRegionInfo(array('region_is_centre'=>1,'region_money_id'=>$delivery_order_info['store_bind_sector_id']), 'region_id');
            //
            $sector_info = model('region_sector')->getRegionInfo(array('region_code'=>$delivery_order_info['region_sector_id'],'region_money_id'=> $delivery_order_info['store_bind_sector_id']));
            
            
           
            $sector_ids = array();
            $sector_ids = explode(',', $sector_info['region_relation_ids']);
            $sector_ids[] = $centre_info['region_code'];
            
           
         
            if (in_array($order_info['region_sector_id'], $sector_ids)) {
                return  0;
            } else {
                return  1;
            }
        }
        
        if ($order_count == 2) {
            //	print_r('2单');
            //有2单。任意一单在中心区域。中心区域可接，扇形3区域可接。 
            //有2单，都在中心区域，中心区域可接，其他16个区域都可接。
            //有2单，都没有在中心区域，只可接相关的两个区域+中心区域
            //有2单，2单都在同一区域，中心区域和先关3区域都可接单。
            
            $order_info_one = $delivery_order_list[0];
            $order_info_two = $delivery_order_list[1];
            if ($order_info_one['region_sector_is_center'] == 1 && $order_info_two['region_sector_is_center'] == 1) { //两单都在中心
                //	print_r('*1');
                return 0 ;
            } elseif ($order_info_one['region_sector_is_center'] == 1 || $order_info_two['region_sector_is_center'] == 1) { //有一单在中心区域
                
                //print_r('*2');
                if ($order_info_one['region_sector_is_center'] == 0) {
                    $sector_info = model('region_sector')->getRegionInfo(array('region_code'=>$order_info_one['region_sector_id'],'region_money_id'=> $order_info_one['store_bind_sector_id']));
                    $sector_ids = array();
                    $sector_ids = explode(',', $sector_info['region_relation_ids']);
                    $sector_ids[] = $order_info_two['region_sector_id'];
                }
                
                if ($order_info_two['region_sector_is_center'] == 0) {
                    $sector_info = model('region_sector')->getRegionInfo(array('region_code'=>$order_info_two['region_sector_id'],'region_money_id'=> $order_info_two['store_bind_sector_id']));
                    $sector_ids = array();
                    $sector_ids = explode(',', $sector_info['region_relation_ids']);
                    $sector_ids[] = $order_info_one['region_sector_id'];
                }
                
                //	print_r($sector_ids);
                if (in_array($order_info['region_sector_id'], $sector_ids) ) {
                    return  0;
                } else {
                    return  1;
                }
            } elseif ($order_info_one['region_sector_id'] == $order_info_two['region_sector_id']) { // 两单不在中心，但在同一个小区域
                //	print_r('*3');
               // $sector_id = $delivery_order_info['region_sector_id'];
                $centre_info= model('region_sector')->getRegionInfo(array('region_is_centre'=>1,'region_money_id'=>$order_info_one['store_bind_sector_id']), 'region_id');
                //
                $sector_info = model('region_sector')->getRegionInfo(array('region_code'=>$order_info_one['region_sector_id'],'region_money_id'=> $order_info_one['store_bind_sector_id']));
                $sector_ids = array();
                $sector_ids = explode(',', $sector_info['region_relation_ids']);
                $sector_ids[] = $centre_info['region_code'];
                if (in_array($order_info['region_sector_id'], $sector_ids)) {
                    return  0;
                } else {
                    return  1;
                }
            } else {	//两单都没在中心区域，且不在同一个小区域
                //		print_r('*4');
                $centre_info= model('region_sector')->getRegionInfo(array('region_is_centre'=>1,'region_money_id'=>$order_info_one['store_bind_sector_id']), 'region_id');
                
                $sector_ids = array();
                $sector_ids[] = $centre_info['region_code'];
                $sector_ids[] = $order_info_one['region_sector_id'];
                $sector_ids[] = $order_info_two['region_sector_id'];
                
                if (in_array($order_info['region_sector_id'], $sector_ids)) {
                    return  0;
                } else {
                    return  1;
                }
            }
        }
    }
    
    
    
    //获取商家周边范围的可用配送员
    private function sendOrderDeliver($store, $order_info)
    {
        $where = array();
        $where['distributor_status'] = 1;
        $where['distributor_online'] = 20;
        $where['delivery_state'] = 0;
        $deliver_data = model('waiter')->getWaiterList($where);
        $deliver = array();
        foreach ($deliver_data as $v) {
            $deliver_coordinate = explode(',', $v['region_coordinate']);
            $distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store[1], $store[0]);
            //$deliver_sort = $this->deliverySort($v['distributor_status']);
            //	print_r($deliver_coordinate);
            //print_r($store);
            //	print_r($distance.',');
                
            //计算当前店铺是不是骑手的叠加单店铺。
            $superposition = $this->getOrderSuperpositionStore($v['distributor_id'], $order_info['store_id']);
                    
            //顶级排序
            if (($superposition == 1)) {
                $deliver[] = array(
                            'distributor_id' => $v['distributor_id'],
                            'distributor_name' => $v['distributor_name']
                        );
            } else {
                if ($distance < 5000 &&  ($superposition == 1 || $superposition == 0)) {
                    $deliver[] = array(
                                'distributor_id' => $v['distributor_id'],
                                'distributor_name' => $v['distributor_name']
                            );
                }
            }
        }
                
                
                
                
                
        return $deliver;
    }
        
    
    
    
    //推送 新
    private function stocketSend($order_sn = '', $is_new = 0)
    {
        $stoket = model('stoket');
        
        //  require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
        //	$gateway = new \GatewayClient\Gateway();
        //	$gateway::$registerAddress = '47.243.54.204:1238';
        
        $where = array(
                    'order_sn'=>$order_sn,
                    'state' => 0,
                //	'send_state' => 0
            );
        $polling = model('order_polling')->getPollingInfo($where, '*');
        $where = array(
                'waiter_id' => $polling['waiter_id'],
                'order_sn' => $polling['order_sn']
            );
                
        $data = array(
                'send_state' =>  1
            );
        model('order_polling')->editPolling($data, $where);
        if ($is_new == 1) {               
			
		//	print_r($polling);
		               
						 
				$this->accept($polling['waiter_id'],$order_sn);
                        
           // $this->stocketShow($polling);
        }
    }
    
   
    //推送列队
    private function stocketShow($polling)
    {
                
                    //如果正在展示的订单就不写入到展示页面里去
        $count = model('order_polling_show')->getShowCount(array('waiter_id'=> $polling['waiter_id']));
        if ($count > 0) {
            $where = array(
                            'waiter_id' => $polling['waiter_id'],
                            'order_sn' => $polling['order_sn']
                        );
            $data = array(
                            'send_state' =>  1
                        );
            model('order_polling')->editPolling($data, $where);
        } else {
            $data = array();
            $data['order_id'] 	= $polling['order_id'];
            $data['order_sn'] 	= $polling['order_sn'];
            $data['waiter_id'] 	= $polling['waiter_id'];
            $data['addtime'] 	= $polling['addtime'];
            $data['store_name'] = $polling['store_name'];
            model('order_polling_show')->addShow($data);
            $where = array(
                            'waiter_id' => $polling['waiter_id'],
                            'order_sn' => $polling['order_sn']
                        );
            $data = array(
                            'send_state' =>  1
                        );
            model('order_polling')->editPolling($data, $where);
            $where = array(
                            'order_id' => $polling['order_id']
                        );
            $data = array(
                            'order_polling_show' => 1
                        );
            model('order')->editOrder($data, $where);
        }
    }
    
   
   
   
   
   
   
   
   
    //获取商家周边范围的可用配送员
    public function getStoreDeliver($store)
    {
        $where = array();
        $where['distributor_status'] = 1;
        $where['distributor_online'] = 20;
        $where['delivery_state'] = 0;
        
        $deliver_data = model('waiter')->getWaiterList($where);
        $deliver = array();
        $count = 0;
        foreach ($deliver_data as $k =>$v) {
            $deliver_coordinate = explode(',', $v['region_coordinate']);
            
            $distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
            
            
            //$deliver_sort = $this->deliverySort($v['distributor_status']);
            //顶级排序
            $deliver_sort = $this->deliverySort($v['distributor_id'], $store['store_id']);
            
            //计算当前店铺是不是骑手的叠加单店铺。
            $superposition = $this->getOrderSuperpositionStore($v['distributor_id'], $store['store_id']);
            
            
            if ($superposition == 1) {
                $deliver[$k]['name'] = $v['distributor_name'];
                $deliver[$k]['distance'] = $distance;
                $deliver[$k]['sort'] = $deliver_sort;
                $deliver[$k]['distributor_id'] = $v['distributor_id'];
                $deliver[$k]['superposition'] = $superposition;
                    
                $count += $this->getDeliveryCount($v['distributor_id']);
            } elseif ($distance < 5000 &&  $superposition == 0) {
                $deliver[$k]['name'] = $v['distributor_name'];
                $deliver[$k]['distance'] = $distance;
                $deliver[$k]['sort'] = $deliver_sort;
                $deliver[$k]['distributor_id'] = $v['distributor_id'];
                $deliver[$k]['superposition'] = $superposition;
                $count += $this->getDeliveryCount($v['distributor_id']);
            }
        }
        
        
        return array('deliver'=>$deliver,'count' => $count);
    }
   
   
    /*
    计算距离
    */
   
    public function getDistance($lat1, $lng1, $lat2, $lng2)
    {
        $earthRadius = 6367000; //approximate radius of earth in meters
   
        /*
         Convert these degrees to radians
        to work with the formula
        */
   
        $lat1 = ($lat1 * pi()) / 180;
        $lng1 = ($lng1 * pi()) / 180;
   
        $lat2 = ($lat2 * pi()) / 180;
        $lng2 = ($lng2 * pi()) / 180;
   
        /*
         Using the
        Haversine formula

        http://en.wikipedia.org/wiki/Haversine_formula

        calculate the distance
        */
   
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
   
        return round($calculatedDistance);
    }
   
   
   
   
    /*
    配送员排序条件
    配送ID
    商铺ID
    */
    private function deliverySort($deliver_id, $store_id)
    {
   
    //有两单
        $count = $this->deliverOrderCount($deliver_id);
    
        switch ($count) {
       case 2:
            $where = array(
                'distributor_id' => $deliver_id,
                'store_id' => $store_id,
                'order_state' => array('in','30,40')
            );
            $count = model('order')->getOrderCount($where);
            if ($count  == 2) {
                return  1 ;
            } elseif ($count == 1) {
                return  2 ;
            } elseif ($count == 0) {
                return  4;
            }
         break;
       case 1:
            $where = array(
                'distributor_id' => $deliver_id,
                'store_id' => $store_id,
                'order_state' => array('in','30,40')
            );
            $count = model('order')->getOrderCount($where);
            //有一单
            if ($count  == 1) {
                return 3;
            } elseif ($count == 0) {
                return 5;
            }
         break;
       case 0:
            return 6;
       break;
    }
    }
   
   
   
    /**
     * @param {Object} $deliver_id
     * @param {Object} $store_id
         获取配送员可接的叠加商户
     */
    private function getOrderSuperpositionStore($deliver_id, $store_id)
    {
        $count = 0;
        $where = array(
            'distributor_id' => $deliver_id,
            'order_state' => array('in','30,40,50')
        );
        $order = model('order')->getOrderInfoN($where, 'store_id,store_superpose', 'distributor_start_time desc');
        if ($order['store_id'] == $store_id) {
            return 1; //店铺本身.
        }
        if ($order['store_superpose'] == $store_id) {
            return 1;
        }
        //print_r($order);
        if (!empty($order)) {
            //直接读取当前商户是否可以叠加其他的店铺
            $store = explode(',', $order['store_superpose']);
            if (in_array($store_id, $store)) {
                return 1;
            } else {
                return 2;
            }
        } else {
            return 0; //没有单子
        }
    }
   
    /**
     * 计算运力
     * @param {Object} $deliver_id
     */
    public function getDeliveryCount($deliver_id)
    {
        $count = 3;
        $where = array(
            'distributor_id' => $deliver_id,
            'order_state' => array('in','30,40,50')
        );
        $order_count = model('order')->getOrderCount($where);
        return $count - $order_count;
    }
   
   
    /**
     * @param {Object} $id
     */
    private function deliverOrderCount($id)
    {
        $count = 0;
        $where = array(
            'distributor_id' =>$id,
            'order_state' => array('in','30,40,50')
        );
        $count = model('order')->getOrderCount($where);
        return $count;
    }
   
   
   
   
    //获取等待时间最长下单时间最久的那个
    private function deliver_last_time($count, $waiter_id)
    {
        if ($count > 0) {
            //获取下单时间最久的
            $where = array(
                'distributor_id' => $waiter_id,
                'order_state' => array('in','30,40')
            );
            $row = model('order')->getOrderInfoN($where, '*', 'add_time asc');
            return $row['add_time'];
        } else {
            
            //获取上线时间
            $where = array(
                'distributor_id' => $waiter_id,
                'stoket_state' => 0
            );
            $row = model('stoket')->getStoketInfo($where);
            return $row['updatetime'];
        }
    }
    
   
    //更新订单排序
    /*
    */
    private function deliverOrderSort($order, $deliver_id)
    {
        $where = array(
            'store_id' => $order['store_id'],
            'order_state' => array('in','30,40'),
            'distributor_id' => $deliver_id
        );
        
        $count = model('order')->getOrderCount($where);
        
        if ($count > 0) {
            return 1;
        } else {
            return 0 ;
        }
    }
   
   
    //抵达商家附近 现在计算为 200米
    private function orderDStore($deliver_id, $latlng)
    {
        $where = array();
        $where['distributor_id'] = $deliver_id;
        $where['order_state'] = 40;
        
        $order = model('order')->getOrderListNew($where, '*', 'order_id asc');
        
        //print_r($order);
        foreach ($order as $v) {
            $store_lng = explode(',', $v['store_coordinate']);
            //      print_r($latlng);
            //     print_r($store_lng);
           
            $delivery_info = model('waiter')->getWaiterInfo(array('distributor_id' => $deliver_id));
            $latlng = explode(',', $delivery_info['region_coordinate']);
           
            //   print_r($latlng);
            //   print_r($store_lng);
           
            $distance = $this->getDistance($latlng[1], $latlng[0], $store_lng[1], $store_lng[0]);
                
                
            //   print_r($distance);
            //	print_r($distance);
            if ($distance < 200) {
                //抵达商家附近
                $where = array();
                $where['order_id'] = $v['order_id'];
                $where['record_msg'] = '配送员到达商家附近';
                $record_info = model('order_record')->getRecordInfo($where);
                if (empty($record_info)) {
                    $data_record = array();
                    $data_record['order_id'] = $v['order_id'];
                    $data_record['record_msg'] = '配送员到达商家附近';
                    $data_record['record_time'] = time();
                    model('order_record')->addRecord($data_record);
                }
                        
                        
                $where = array();
                $where['order_id'] = $v['order_id'];
                $where['log_orderstate'] = 41;
                $log_info = model('order')->getOrderLogInfo($where);
                        
                if (empty($log_info)) {
                    $data = array();
                    $data['order_id'] = $v['order_id'];
                    $data['log_role'] = 'waiter';
                    $data['log_msg'] = '配送员到达商家附近';
                    $data['log_user'] = $v['distributor_id'];
                               
                    if ($msg) {
                        $data['log_msg'] .= ' ( '.$msg.' )';
                    }
                                   
                    $data['log_orderstate'] = 41;
                    model('order')->addOrderLog($data);
                }
            } else {
                output_error('未抵达商家附近');
            }
        }
    }
    
    private function orderDUser()
    {
        $where = array();
        $where['distributor_id'] = $deliver_id;
        $where['order_state'] = 50;
        
        $order = model('order')->getOrderListNew($where, '*', 'order_id asc');
        foreach ($order as $v) {
            $store_lng = explode(',', $v['buyer_coordinate']);
            $distance = $this->getDistance($latlng[1], $latlng[0], $store_lng[1], $store_lng[0]);
            if ($distance < 200) {
                $where = array();
                $where['order_id'] = $v['order_id'];
                $where['record_msg'] = '配送员到达顾客位置';
                $record_info = model('order_record')->getRecordInfo($where);
                if (empty($record_info)) {
                    $data_record = array();
                    $data_record['order_id'] = $order['order_id'];
                    $data_record['record_msg'] = '配送员到达顾客位置';
                    $data_record['record_time'] = time();
                    model('order_record')->addRecord($data_record);
                }
                        
                        
                $where = array();
                $where['order_id'] = $v['order_id'];
                $where['log_orderstate'] = 52;
                $log_info = model('order')->getOrderLogInfo($where);
                        
                if (empty($log_info)) {
                    $data = array();
                    $data['order_id'] = $v['order_id'];
                    $data['log_role'] = 'waiter';
                    $data['log_msg'] = '配送员到达顾客位置';
                    $data['log_user'] = $v['distributor_id'];
                               
                    if ($msg) {
                        $data['log_msg'] .= ' ( '.$msg.' )';
                    }
                                   
                    $data['log_orderstate'] = 41;
                    model('order')->addOrderLog($data);
                }
            } else {
                output_error('未抵达用户附近');
            }
        }
    }
    
   
   
   
   
    //多边形筛选
    public function polygon_order($region_id)
    {
        
        //获取多边形相关联的区域。
    }
   
   
    //下单的时候计算出订单所在16边区域。方便接单的时候筛选
    public function polygon16($region_id, $points, $order_id)
    {
        $polygon_model = model('polygon');
        $polygon_list  = model('region_sector')->getRegionList(array('region_money_id' => $region_id));
    
        //print_r($polygon_list);
    
        foreach ($polygon_list as $k =>  $v) {
            $polygon_points = $polygon_model->points_array($v['region_coordinate']);
            $row = 	$polygon_model->is_point_in_polygon($points, $polygon_points);
          //  print_r($row);
            
            
            if ($row === true) {
                $data = array();
                $data['region_sector_id'] = $v['region_code'];				
                if ($v['region_is_centre'] == 1) {
                    $data['region_sector_is_center'] = 1;
                }
              //  print_r($data);
                
                model('order')->editOrder($data, ['order_id' => $order_id]);
                break;
            }
        }
    }
   
   
    //下单的时候计算出订单所在16边区域。方便接单的时候筛选
    public function polygon16_check($region_id, $points)
    {
        $polygon_model = model('polygon');
        $polygon_list  = model('region_sector')->getRegionList(array('region_money_id' => $region_id));
    
        //print_r($polygon_list);
    
        foreach ($polygon_list as $k =>  $v) {
            $polygon_points = $polygon_model->points_array($v['region_coordinate']);
            $row = 	$polygon_model->is_point_in_polygon($points, $polygon_points);
          //  print_r($row);
            if ($row === true) {                
                return 1;
                break;
            }
        }
    }
   

   
   
   
   
   
    //计算坐标是不是在多边形内
    private function points_array($points)
    {
        $points_array = explode('|', $points);
        $data = array();
        foreach ($points_array as $k => $v) {
            $bs = explode(',', $v);
            
            $data[$k]['lng'] = $bs[0];
            $data[$k]['lat'] = $bs[1];
        }
        return $data;
    }
   
   
    //计算坐标是不是在多边形内
    private function is_point_in_polygon($point, $pts)
    {
        $N = count($pts);
        $boundOrVertex = true; //如果点位于多边形的顶点或边上，也算做点在多边形内，直接返回true
    $intersectCount = 0;//cross points count of x
    $precision = 2e-10; //浮点类型计算时候与0比较时候的容差
    $p1 = 0;//neighbour bound vertices
    $p2 = 0;
        $p = $point; //测试点
    
        $p1 = $pts[0];//left vertex
    for ($i = 1; $i <= $N; ++$i) {//check all rays
        // dump($p1);
        if ($p['lng'] == $p1['lng'] && $p['lat'] == $p1['lat']) {
            return $boundOrVertex;//p is an vertex
        }
         
        $p2 = $pts[$i % $N];//right vertex
        if ($p['lat'] < min($p1['lat'], $p2['lat']) || $p['lat'] > max($p1['lat'], $p2['lat'])) {//ray is outside of our interests
            $p1 = $p2;
            continue;//next ray left point
        }
         
        if ($p['lat'] > min($p1['lat'], $p2['lat']) && $p['lat'] < max($p1['lat'], $p2['lat'])) {//ray is crossing over by the algorithm (common part of)
            if ($p['lng'] <= max($p1['lng'], $p2['lng'])) {//x is before of ray
                if ($p1['lat'] == $p2['lat'] && $p['lng'] >= min($p1['lng'], $p2['lng'])) {//overlies on a horizontal ray
                    return $boundOrVertex;
                }
                 
                if ($p1['lng'] == $p2['lng']) {//ray is vertical
                    if ($p1['lng'] == $p['lng']) {//overlies on a vertical ray
                        return $boundOrVertex;
                    } else {//before ray
                        ++$intersectCount;
                    }
                } else {//cross point on the left side
                    $xinters = ($p['lat'] - $p1['lat']) * ($p2['lng'] - $p1['lng']) / ($p2['lat'] - $p1['lat']) + $p1['lng'];//cross point of lng
                    if (abs($p['lng'] - $xinters) < $precision) {//overlies on a ray
                        return $boundOrVertex;
                    }
                     
                    if ($p['lng'] < $xinters) {//before ray
                        ++$intersectCount;
                    }
                }
            }
        } else {//special case when ray is crossing through the vertex
            if ($p['lat'] == $p2['lat'] && $p['lng'] <= $p2['lng']) {//p crossing over p2
                $p3 = $pts[($i+1) % $N]; //next vertex
                if ($p['lat'] >= min($p1['lat'], $p3['lat']) && $p['lat'] <= max($p1['lat'], $p3['lat'])) { //p.lat lies between p1.lat & p3.lat
                    ++$intersectCount;
                } else {
                    $intersectCount += 2;
                }
            }
        }
        $p1 = $p2;//next ray left point
    }
    
        if ($intersectCount % 2 == 0) {//偶数在多边形外
            return false;
        } else { //奇数在多边形内
            return true;
        }
    }
   
    
    //下单的时候计算出订单所在16边区域。方便接单的时候筛选
    private function polygon_money($points)
    {
        $polygon_model = model('polygon');
        //	$polygon_list  = model('region_sector')->getRegionList(array('region_money_id' => $region_id));
        $region = model('region')->getRegionList(array('region_type'=> 2,'is_money_show' => 1));
        foreach ($region as $k =>  $v) {
            $polygon_points = $polygon_model->points_array($v['region_money_coordinate']);
            $row = 	$polygon_model->is_point_in_polygon($points, $polygon_points);
            if ($row === true) {
                return  1;
                break;
            }
        }
    }
}
