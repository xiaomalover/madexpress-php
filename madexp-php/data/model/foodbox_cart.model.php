<?php
/**
 * 购物车模型
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class foodbox_cartModel extends Model {

    /**
     * 购物车商品总金额
     */
    private $cart_all_price = 0;

    /**
     * 购物车商品总数
     */
    private $cart_goods_num = 0;

    public function __construct() {
       parent::__construct('foodbox_cart');
    }

    /**
     * 取属性值魔术方法
     *
     * @param string $name
     */
    public function __get($name) {
        return $this->$name;
    }

    /**
     * 检查购物车内商品是否存在
     *
     * @param
     */
    public function checkCart($condition = array()) {
        return $this->where($condition)->find();
    }


    /**
     * 取得 单条购物车信息
     * @param unknown $condition
     * @param string $field
     */
    public function getCartInfo($condition = array(), $field = '*') {
       return $this->field($field)->where($condition)->find();
    }

	
    /**
     * 将商品添加到购物车中
     *
     * @param array $data   商品数据信息
     * @param string $save_type 保存类型，可选值 db,cookie
     * @param int $quantity 购物数量
     */
    public function addCart($data = array()) {		
		return $this->insert($data);       
    }

    /**
     * 更新购物车
     *
     * @param   array   $param 商品信息
     */
    public function editCart($data,$condition) {
        return  $this->where($condition)->update($data);        
    }

	
	public function countCart($condition){
		return $this->where($condition)->count();
	}
  
	/**
     * 购物车列表
     *
     * @param string $type 存储类型 db,cookie
     * @param unknown_type $condition
     * @param int $limit
     */
    public function listCart($condition = array(),$group ='',$limit = '') {        
        return   $this->where($condition)->limit($limit)->group($group)->select();  
		
    }

    /**
     * 删除购物车商品
     *
     * @param string $type 存储类型 db,cookie
     * @param unknown_type $condition
     */
    public function delCart($condition = array()) {    		
        return  $this->where($condition)->delete();
		
    }


 
}
