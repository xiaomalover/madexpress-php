<?php
/**
 * 租车模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_leaseModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_lease');
    }

	
	public function makeSn() {
       return 'OC'.date('Ymd',time()).mt_rand(10000,99999);
    }

	
		
	
	/**
     * 添加
     * @param array $insert
     * @return boolean
     */
    public function addLease($insert) {
        return $this->table('vehicle_lease')->insert($insert);
    }

    /**
     * 编辑
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editLease($condition, $update) {
        return $this->table('vehicle_lease')->where($condition)->update($update);
    }

    /**
     * 删除
     * @param unknown $condition
     * @return boolean
     */
    public function delLease($condition) {        
        return $this->table('vehicle_lease')->where($condition)->delete();
    }

    /**
     * 查询数量
     * @param array $condition
     * @return array
     */
    public function getLeaseCount($condition) {
        return $this->table('vehicle_lease')->where($condition)->count();
    }

    /**
     * 列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getLeaseList($condition, $field = '*', $page = 0, $order = 'lease_id desc', $limit = '') {
        return $this->table('vehicle_lease')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLeaseInfo($condition, $field = '*') {
        return $this->table('vehicle_lease')->field($field)->where($condition)->find();
    }
	
	
	
    /**
     * 更改订单支付信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function editPay($data,$condition) {
        return $this->table('vehicle_lease_pay')->where($condition)->update($data);
    }

  
    public function getPayInfo($condition = array(), $master = false,$lock = false) {
        return $this->table('vehicle_lease_pay')->where($condition)->master($master)->lock($lock)->find();
    }

    /**
     * 取得支付单列表
     *
     * @param unknown_type $condition
     * @param unknown_type $pagesize
     * @param unknown_type $filed
     * @param unknown_type $order
     * @param string $key 以哪个字段作为下标,这里一般指pay_id
     * @return unknown
     */
    public function getPayList($condition, $pagesize = '', $filed = '*', $order = '', $key = '') {
        return $this->table('vehicle_lease_pay')->field($filed)->where($condition)->order($order)->page($pagesize)->key($key)->select();
    }
	
	   /**
     * 插入订单支付表信息
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrderPay($data) {
        return $this->table('vehicle_lease_pay')->insert($data);
    }

	  /**
     * 添加订单日志
     */
    public function addOrderLog($data) {
        $data['log_role'] = str_replace(array('buyer','seller','system','admin','waiter'),array('买家','商家','系统','管理员','送餐员'), $data['log_role']);
        $data['log_time'] = TIMESTAMP;
        return $this->table('vehicle_lease_log')->insert($data);
    }
	
	
}
