<?php
/**
 * 余额支付

 */
defined('InMadExpress') or exit('Access Invalid!');
class waiter_predepositModel extends Model {
    /**
     * 生成充值编号
     * @return string
     */
    public function makeSn() {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $_SESSION['member_id'] % 1000);
    }

    
	
	/**
     * 变更预存款
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();
		
        $data_log['lg_invite_waiter_id'] = $data['invite_waiter_id'];
        $data_log['lg_waiter_id'] = $data['waiter_id'];
        $data_log['lg_waiter_name'] = $data['waiter_name'];
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
     //   $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
        switch ($change_type){
				
				
            case 'order_lease_pay':
                $data_log['lg_av_amount'] = - $data['amount'];
                $data_log['lg_desc'] = '租车下单，余额支付，租车单号: '.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
				
            case 'order_lease_cancel':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_freeze_amount'] = -$data['amount'];
                $data_log['lg_desc'] = '取消订单，解冻预存款，订单号: '.$data['order_sn'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = -$data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
				
			 case 'bill_scheduling':
                $data_log['lg_av_amount'] = $data['amount'];
                $data_log['lg_freeze_amount'] = $data['amount'];
                $data_log['lg_desc'] = '排班结算，订单号: '.$data['order_sn'];
                $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit-'.$data['amount']);
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
					
				
			//订单配送费			
			case 'order_delivery':				
				$data_log['lg_av_amount'] = $data['amount'];  
                $data_log['lg_desc'] = '订单:'.$data['order_sn'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = $data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
                break;
				
			//退还配送费
			case 'order_delivery_refund':	
				$data_log['lg_av_amount'] = $data['amount'];          
                $data_log['lg_desc'] = '订单:'.$data['order_sn'];             
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
				break;
				
				
			//退还租车已支付的押金和保险
			case 'order_lease_refund':	
				$data_log['lg_av_amount'] = $data['amount'];          
                $data_log['lg_desc'] = '租车超时退款:'.$data['order_sn'];             
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
				break;
				
					
				
				
				
				
            default:
                throw new Exception('参数错误');
                break;
        }
		
        $update = Model('waiter')->editWaiter(array('distributor_id'=>$data['waiter_id']),$data_pd);

		
        if (!$update) {
            throw new Exception('操作失败');
        }
		
        $insert = $this->table('waiter_pd_log')->insert($data_log);
		
		
        if (!$insert) {
            throw new Exception('操作失败');
        }

       /* // 支付成功发送买家消息
        $param = array();
        $param['code'] = 'predeposit_change';
        $param['member_id'] = $data['member_id'];
        $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
        $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
        $param['param'] = $data_msg;
        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
        'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
        QueueClient::push('sendMemberMsg', $param);*/
		
        return $insert;
    }


    /**
     * 取得提现列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdCashList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('pd_cash')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加提现记录
     * @param array $data
     */
    public function addPdCash($data) {
        return $this->table('pd_cash')->insert($data);
    }

    /**
     * 编辑提现记录
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdCash($data,$condition = array()) {
        return $this->table('pd_cash')->where($condition)->update($data);
    }

    /**
     * 取得单条提现信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdCashInfo($condition = array(), $fields = '*') {
        return $this->table('pd_cash')->where($condition)->field($fields)->find();
    }

    /**
     * 删除提现记录
     * @param unknown $condition
     */
    public function delPdCash($condition) {
        return $this->table('pd_cash')->where($condition)->delete();
    }
}
