<?php
/**
 * 排班任务模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_taskModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_task');
    }

    public function getTaskList($condition=array(), $page=null, $order='id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delTask($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addTask($data) {
        return $this->insert($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editTask($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
	
	//详情
	public function infoTask($condition){
		return $this->where($condition)->find();
	}
	
}
