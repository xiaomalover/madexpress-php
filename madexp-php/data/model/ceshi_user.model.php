<?php
/**
 * 我的地址
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class ceshi_userModel extends Model {

    public function __construct() {
        parent::__construct('ceshi_user');
    }

    /**
     * 取得买家默认收货地址
     *
     * @param array $condition
     */
    public function getDefaultAddressInfo($condition = array(), $order = 'is_default desc,address_id desc') {
        return $this->getAddressInfo($condition, $order);
    }

    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getAddressInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getAddressList($condition, $order = 'address_id desc'){
        $address_list = $this->where($condition)->order($order)->select();
       return $address_list;
    }

    /**
     * 取数量
     * @param unknown $condition
     */
    public function getAddressCount($condition = array()) {
        return $this->where($condition)->count();
    }

    /**
     * 构造检索条件
     *
     * @param array $condition 检索条件
     * @return string 数组形式的返回结果
     */
    private function _condition($condition){
        $condition_str = '';

        if ($condition['member_id'] != ''){
            $condition_str .= " member_id = '". intval($condition['member_id']) ."'";
        }

        return $condition_str;
    }

    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addAddress($param){
        return $this->insert($param);
    }
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editAddress($update, $condition){
        return $this->where($condition)->update($update);
    }
    
	/**
     * 验证地址是否属于当前用户
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function checkAddress($member_id,$address_id) {
        /**
         * 验证地址是否属于当前用户
         */
        $check_array = self::getOneAddress($address_id);
        if ($check_array['member_id'] == $member_id){
            unset($check_array);
            return true;
        }
        unset($check_array);
        return false;
    }
    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delAddress($condition){
        return $this->where($condition)->delete();
    }
}
