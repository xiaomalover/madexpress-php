<?php
/**
 * 排班任务模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_task_logModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_task_log');
    }

    public function getTaskLogList($condition=array(), $page=null, $order='log_id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delTaskLog($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addTaskLog($data) {
        return $this->insert($data);
    }
	
	public function addTaskLogAll($data){
		return $this->insertAll($data);
	}


    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editTaskLog($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
	
	//详情
	public function infoTaskLog($condition){
		return $this->where($condition)->find();
	}
	
}
