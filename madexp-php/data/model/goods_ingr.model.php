<?php
/**
 * 商品属性
 *

 */
defined('InMadExpress') or exit('Access Invalid!');

class goods_ingrModel extends Model {
    public function __construct() {
        parent::__construct('goods_ingr');
    }

  

    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getGoodsIngrInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getGoodsIngrList($condition, $order = 'id asc',$group=''){
        $attr_list = $this->where($condition)->order($order)->group($group)->select();
        return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addGoodsIngr($param){
        return $this->insert($param);
    }
	
	  public function addGoodsIngrAll($param){
        return $this->insertAll($param);
    }
	
	
	
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editGoodsIngr($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delGoodsIngr($condition){
        return $this->where($condition)->delete();
    }
}
