<?php
/**
 * 送餐员模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class VehicleRentalRuleModel extends Model
{

    public function __construct()
    {
        parent::__construct('vehicle_rental_rule');
    }
	
	
    public function rentalInfo($condition, $field = '*', $master = false)
    {
        return $this->field($field)->where($condition)->master($master)->find();
    }

    public function rentalList($condition=[], $field = '*', $page = 0, $order = 'id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

    public function getVehicleCount($condition)
    {
        return $this->where($condition)->count();
    }

    public function AddAllRules($param)
    {
        return $this->insertAll($param);
    }
    public function delRule($condition) {        
        return $this->where($condition)->delete();
    }




}
