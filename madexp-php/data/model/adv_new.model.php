<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class adv_newModel extends Model {
    public function __construct() {
        parent::__construct('adv');
    }

    /**
     * 添加Adv
     * @param array $insert
     * @return boolean
     */
    public function addAdv($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Adv
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editAdv($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Adv
     * @param unknown $condition
     * @return boolean
     */
    public function delAdv($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Adv数量
     * @param array $condition
     * @return array
     */
    public function getAdvCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Adv列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getAdvList($condition, $field = '*', $page = 0, $order = 'slide_sort asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Adv内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getAdvInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
