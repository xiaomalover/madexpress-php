<?php
/**
 * 订单交易快照
 *
 *
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class order_snapshotModel extends Model
{
    public function __construct()
    {
        parent::__construct('orders_snapshot');
    }

    /**
     */
    public function getSnapshotInfoByRecid($rec_id, $goods_id)
    {
    }

    public function getSnaOrderCount($condition){
        
     return     $this->table('orders_snapshot')->where($condition)->count();
        
    }
    

    /*
    创建订单快照
    */
    public function createSphot($order_id)
    {
		$model_order = model('order');
        $where = array(
            'order_id' => $order_id
        );

		$order_info = $model_order->getOrderInfoN($where);
		
        if ($order_info) {
            $order_info['order_id'];
			$order_info['snapshot_time'] = time();			
            $snapshot_id = $this->addSnaOrder($order_info);
			
			if($snapshot_id){
			    		
                if($order_info['is_old_num'] == 1){
                    $where['is_edit'] = 0 ;
                }
			//获取原商品快照
            $goods_list = $model_order->getOrderGoodsList($where);            
            $goods_data = array();
            foreach ($goods_list as $v) {
                $v['snapshot_id'] = $snapshot_id;
                unset($v['is_edit']);
                $goods_datas[] = $v;
            }                                  
           $data =  $this->addSnaOrderGoods($goods_datas);
			
			
			//创建赠送的商品
			$give_goods_list = model('order_goods_give')->getGiveList(array('is_take_ffect'=> 1, 'version'=> $order_info['is_old_num'], 'order_id'=>$order_info['order_id']));
			foreach($give_goods_list as $v){				
				$v['snapshot_id'] = $snapshot_id;
                $v['buyer_id'] = $order_info['buyer_id'];
                unset($v['is_snapshot']);
                unset($v['version']);
				$give_goods_lists[] = $v;
			}			                     
		    $data =	$this->addSnaOrderGoodsGive($give_goods_lists);
            //删除is_snapshot = 1;
        // model('order_goods_give')->delGive(['is_snapshot' => 1 , 'order_id' => $order_info['order_id']]);
         //   model('order_goods_give')->editGive(array('is_take_ffect'=> 1, 'is_snapshot' => 0, 'order_id'=>$order_info['order_id']),['is_snapshot' => 1]);




            //创建订单退款
            $refund_order = model('order_refund')->getRefundList(array('order_id'=>$order_info['order_id'],'version' => $order_info['is_old_num']));
            foreach($refund_order as $v){
                $v['snapshot_id'] = $snapshot_id;
                unset($v['is_snapshot']);
                unset($v['version']);
                $refund_order_item = $v;
                model('order_snapshot_refund')->addRefund($refund_order_item);    
            }

         //   model('order_refund')->editRefund(['order_id'=>$order_info['order_id'],'is_snapshot' => 0],['is_snapshot' => 1]);


    
			//创建退款的快照
		    	$refund_goods_list = model('order_goods_refund')->getRefundList(
				array(	
					'order_id' => $order_info['order_id'],
                    'version'  => $order_info['is_old_num']
				));
				foreach($refund_goods_list as $v){				
					$v['snapshot_id'] = $snapshot_id;
                    unset($v['is_snapshot']);
                    unset($v['version']);
					$refund_goods_lists[] = $v;				
                }
            //    print_r($refund_goods_lists);
		        $data =	$this->addSnaOrderGoodsRefund($refund_goods_lists);

        //    model('order_goods_refund')->editRefund(['order_id'=>$order_info['order_id'],'is_snapshot' => 0],['is_snapshot' => 1]);


			


			//创建替换商品			
			 $replace_goods_list = model('order_goods_replace')->getGoodsList(array(
				'is_take_ffect'=> 1, 
				'order_id' => $order_info['order_id'],
                'version'=> $order_info['is_old_num']
                ));
				foreach($replace_goods_list as $v){
					$v['snapshot_id'] = $snapshot_id;
                    unset($v['version']);
					$replace_goods_lists[] = $v;					
				}
             //   print_r($replace_goods_lists);
		    $data =	$this->addSnaOrderGoodsReplace($replace_goods_lists);
			
          //  model('order_goods_replace')->editGoods(['is_take_ffect'=> 1,'order_id' => $order_info['order_id'],'is_snapshot' => 0],['is_snapshot' => 1]);

			//log_快照			
			$log_list = model('order')->getOrderLogList(array('order_id' => $order_info['order_id']));
			foreach($log_list as $v){
				$v['snapshot_id'] = $snapshot_id;
              //  unset($v['version']);
				$log_lists[] = $v;
			}
		    $data =	$this->addSnaOrderLog($log_lists);
			



			//优惠券
			$coupon_list = model('order_coupon')->getCouponList(array('order_id' => $order_info['order_id']));
			foreach($coupon_list as $v){
				$v['snapshot_id'] = $snapshot_id;
                unset($v['version']);
				$coupons[] = $v;				
			}
			 $data = $this->addSnaOrderCoupon($coupons);
			
			
			return $snapshot_id;
			
		   
			}else{
			    
			    return false;
			}
			
			
        }
    }
    
    
    public function addSnaOrder($data)
    {
        return	$this->table('orders_snapshot')->insert($data);
    }
    
    //生成商品快照
    public function addSnaOrderGoods($data)
    {
        return $this->table('order_snapshot_goods')->insertAll($data);
    }
	
	//生成退款商品快照
	public function addSnaOrderGoodsGive($data)
	{
	    return $this->table('order_snapshot_goods_give')->insertAll($data);
	}
	
    //生成商品快照
    public function addSnaOrderRefund($data)
    {
        return $this->table('order_snapshot_refund')->insertAll($data);
    }



	//生成商品快照
	public function addSnaOrderGoodsRefund($data)
	{
	    return $this->table('order_snapshot_goods_refund')->insertAll($data);
	}
	
	
	//生成商品快照
	public function addSnaOrderGoodsReplace($data)
	{
	    return $this->table('order_snapshot_goods_replace')->insertAll($data);
	}
	
	//生成LOGO快照
	public function addSnaOrderLog($data){		
		return $this->table('order_snapshot_log')->insertAll($data);		
	}
	
	
	//生成赠送优惠券快照
	public function addSnaOrderCoupon($data){		
		return $this->table('order_snapshot_coupon')->insertAll($data);		
	}
	
	
	
    
    public function getSnapshot($condition, $order = 'snapshot_id asc')
    {
        return $this->table('orders_snapshot')->where($condition)->order($order)->select();
    }
    
        
    public function delSnapshot($condition)
    {
        return $this->table('orders_snapshot')->where($condition)->delete();
    }
    
		
		
        
        
        
        
        
   
    public function getOrderInfo($condition = array(), $extend = array(), $fields = '*', $order = '',$group = '') {
      
		$order_info = $this->table('orders_snapshot')->field($fields)->where($condition)->group($group)->order($order)->find();
        if (empty($order_info)) {
            return array();
        }
        if (isset($order_info['order_state'])) {
            $order_info['state_desc'] = orderState($order_info);
        }
        if (isset($order_info['payment_code'])) {
            $order_info['payment_name'] = orderPaymentName($order_info['payment_code']);
        }

   

        //追加返回订单扩展表信息
        if (in_array('order_common',$extend)) {
            $order_info['extend_order_common'] = $this->getOrderCommonInfo(array('order_id'=>$order_info['order_id']));
            $order_info['extend_order_common']['reciver_info'] = unserialize($order_info['extend_order_common']['reciver_info']);
            $order_info['extend_order_common']['invoice_info'] = unserialize($order_info['extend_order_common']['invoice_info']);
        }

        if(in_array('delivery',$extend)){			
			$order_info['extend_delivery'] = Model('waiter')->getWaiterInfo(array('distributor_id'=>$order_info['distributor_id']));			
			unset($order_info['extend_delivery']['login_name']);
			unset($order_info['extend_delivery']['login_password']);
			unset($order_info['extend_delivery']['login_time']);
			unset($order_info['extend_delivery']['available_predeposit']);
			unset($order_info['extend_delivery']['freeze_predeposit']);
		}


        //追加返回店铺信息
        if (in_array('store',$extend)) {
            $order_info['extend_store'] = Model('store')->getStoreInfo(array('store_id'=>$order_info['store_id']));
			$order_info['extend_store']['store_avatar'] =  getStoreLogo($order_info['extend_store']['store_avatar'], 'store_avatar');   
			unset($order_info['extend_store']['store_login_name']);
			unset($order_info['extend_store']['store_login_password']);	
			unset($order_info['extend_store']['available_predeposit']);	
			unset($order_info['extend_store']['freeze_predeposit']);	
			unset($order_info['extend_store']['lbs_id']);	
			
					
        }

        //返回买家信息
        if (in_array('member',$extend)) {
            $order_info['extend_member'] = Model('member')->getMemberInfoByID($order_info['buyer_id']);
            unset($order_info['extend_member']['member_passwd']);	
            unset($order_info['extend_member']['available_predeposit']);	
            unset($order_info['extend_member']['freeze_predeposit']);	
            unset($order_info['extend_member']['available_rc_balance']);	
            unset($order_info['extend_member']['freeze_rc_balance']);	
        }
 
        //追加返回商品信息
        if (in_array('order_goods',$extend)) {
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'snapshot_id' => $order_info['snapshot_id'], 'goods_usable_num'=>array('gt',0)));			
			$goods_list = array();
			foreach($order_goods_list as $k => $v){
			    
			        $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$v['goods_ingr_new'] = $this->goodsIngrNew($v['goods_id']);
				
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();					
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();					
			
			}
			
            $order_info['extend_order_goods'] = $goods_list;
        }



        //打印单数据
		if(in_array('order_print_goods',$extend)){			
			

            //获取商家的默认语言

            $store_lang = model('store_language')->getStoreLangInfo(['store_id' => $order_info['store_id'],'is_default' => 1]);
 

			//获取打印小票商品列表
			$order_goods_list = $this->getOrderGoodsList(array( 'order_id'=>$order_info['order_id']));					
			$goods_list = array();			
			
            foreach($order_goods_list as $k => $v){				
			       
                if($v['set_meal'] == 1){                    
                    $meal_ids = explode(',',$v['set_meal_spec']);
                    //获取套餐商品
                    $meal_list = model('store_goods_meal')->get_order_meal_specs_goods($meal_ids,$store_lang['language_en']);
                    foreach($meal_list as $m){
                        
                        $goods_item = array();
                        //    $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                            
                            $goods_item['goods_name']   =  $m['meal_value'];
                            $goods_item['goods_num']    = 1;
                            $goods_item['goods_price']  = $m['meal_price'];
                            $goods_item['goods_size']   =  $m['meal_size_name'] == 'DefaultSize' ? '' : $m['meal_size_name'];
                            $goods_item['goods_spec']   =  $m['meal_specs_name'];
                            $goods_item['goods_optional'] =  array();
                            $goods_item['original_goods_optional'] = array();
                            $goods_list[] = $goods_item;

                    }                    
                    
                }else{
            
                   if($v['goods_usable_num'] > 0){
                       $lang_goods = $this->goodsLang($v,$store_lang['language_en']); 
                       // print_r($lang_goods);
                        $goods_item = array();
                    //    $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                        
                        $goods_item['goods_name']   =  $lang_goods['goods_name'];
                        $goods_item['goods_num']    = $v['goods_usable_num'];
                        $goods_item['goods_price']  = $v['goods_price'];
                        $goods_item['goods_size']   = $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
                        $goods_item['goods_spec']   =  $lang_goods['goods_spec'];
                        $goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
                        $goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
                        $goods_list[] = $goods_item;
                
                   }
               
                }
                
            }
            
            //获取替换商品
			$order_goods_list = model('order_snapshot_goods_replace')->getGoodsList(array('is_take_ffect'=> 1, 'snapshot_id' => $order_info['snapshot_id'],  'order_id' => $order_info['order_id']));	
			foreach($order_goods_list as $k => $v){				
			       
                        $lang_goods = $this->goodsLang(['goods_code' => $v['goods_code']],$store_lang['language_en']); 
    				    
    				    $goods_item = array();
    				 //   $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
    					$goods_item['goods_name']   = $lang_goods['goods_name'];
    					$goods_item['goods_num']    = $v['goods_num'];
    					$goods_item['goods_price']  = $v['goods_price'];
    					$goods_item['goods_size']   =   $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
    					$goods_item['goods_spec']   = $lang_goods['goods_spec'];
    					$goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
    					$goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			            $goods_list[] = $goods_item;
			    
			}
            
			
			
			 //获取赠送
			$order_goods_list = model('order_snapshot_goods_give')->getGiveList(array('is_take_ffect'=> 1,  'snapshot_id' => $order_info['snapshot_id'], 'order_id' => $order_info['order_id']));	
			foreach($order_goods_list as $k => $v){				
			       
                        $lang_goods = $this->goodsLang(['goods_code' => $v['goods_code']],$store_lang['language_en']); 
    				    $goods_item = array();
    				 //   $goods_item['goods_ingr']   = $this->goodsIngr($v['goods_id']);
    					$goods_item['goods_name']   =  $lang_goods['goods_name'];
    					$goods_item['goods_num']    = $v['goods_num'];
    					$goods_item['goods_price']  = $v['goods_price'];
    					$goods_item['goods_size']   =  $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
    					$goods_item['goods_spec']   = $lang_goods['goods_spec'];
    					$goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
    					$goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			            $goods_list[] = $goods_item;
			    
			}            
			//
			$order_info['extend_print_order_goods'] = $goods_list;			

		}
		
		
		
		
         //追加返回替换商品信息
        if (in_array('replace',$extend)) {

            //group 调用商品
            $replace = model('order_snapshot_goods_replace')->getGoodsList(array('is_take_ffect'=> 1,'snapshot_id'=> $order_info['snapshot_id'],  'order_id' => $order_info['order_id']),'rec_id','rec_id');
       //     print_r($replace);
            
          //  $replace =  $this->getOrderGoodsList(array('order_id'=> $order_info['order_id'],'state' => 2),'*','','','rec_id desc','replace_rec_id');
            $replace_ids = array();
    		foreach($replace as $v){			
    			$replace_ids[] = $v['rec_id'];
    		}
    		
    	
    		$replace_ids = 	implode(',',$replace_ids);
    	//	print_r($replace_ids);
           	$order_goods_list = $this->getOrderGoodsList(array('snapshot_id'=> $order_info['snapshot_id'], 'rec_id'=> array('in',$replace_ids)));
            $replace_list = array();
            $goods_num = 0;
            $goods_amount = 0;
            foreach($order_goods_list as $k => $v){	
			    $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();	
			    $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();	  
				$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
				
				
				$replace_list[$k] = $v;
				$replace_list[$k]['replace_goods'] =  $this->order_replace_goods($v['rec_id']);				
				
				$goods_num += $v['goods_num'];
				$goods_amount +=$v['goods_num'] * $v['goods_price'];
				
			}
		 
            
            $order_info['extend_order_goods_replace'] = $replace_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;
			$order_info['extend_order_goods_replace_amount'] = $goods_amount;
            
            /*
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 2 ,'is_take_ffect'=> 1));			
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] ? unserialize($v['goods_optional']) : array();					
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional'] ? unserialize($v['original_goods_optional']) : array();
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_replace'] = $goods_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;*/
			
        }
        
        
        
        //赠送商品
        if (in_array('goods_give',$extend)) {
            
            
            $order_goods_list = model('order_snapshot_goods_give')->getGiveList(array('is_take_ffect'=> 1, 'snapshot_id'=> $order_info['snapshot_id'], 'order_id'=>$order_info['order_id']));
            
            //取商品列表
          //  $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 3 ,'is_take_ffect'=> 1));			
                                    
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
                    $goods_list[$k]['goods_size']   =  $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'];
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] == null  ? array() : unserialize($v['goods_optional']) ;			
	                $goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();					
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_give'] = $goods_list;
			$order_info['extend_order_goods_give_num'] = $goods_num;
			
        }
        
        
        
        
        
		//一阶段退款
        if (in_array('one_stage_refund',$extend)) {


            //获取当前
            $refund = model('order_snapshot_refund')->getRefundInfo(['order_id' => $order_info['order_id'], 'snapshot_id' => $order_info['snapshot_id'], 'refund_stage' => 0]);

            //取商品列表
            $order_goods_list = model('order_snapshot_goods_refund')->getRefundList(
				array(
					'refund_id' => $refund['refund_id'],
                    'snapshot_id' => $order_info['snapshot_id']
				));
				
			
            $goods_list = array();
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			}		  
			
		    $order_info['extend_one_stage_refund'] = $goods_list;
						
        }
        
		
		
        //二阶段退款
        if (in_array('two_stage_refund',$extend)) {            

             //获取当前
             $refund = model('order_snapshot_refund')->getRefundInfo(['order_id' => $order_info['order_id'], 'snapshot_id' => $order_info['snapshot_id'],'refund_stage' => 1]);
            
            //取商品列表
            $order_goods_list = model('order_snapshot_goods_refund')->getRefundList(
				array(				
					'refund_id' => $refund['refund_id']
				)
			);
			$goods_list = array();
			$goods_num = 0;
            $kk = 0;
            foreach($order_goods_list as $k => $v){
                   if($v['is_replace'] == 1){
                     $v['replace_type'] = 0;  
                   }
                    $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                    $goods_list[$kk] = $v;
                    $goods_list[$kk]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                    $goods_list[$kk]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
                    $goods_list[$kk]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
                   
                   /*
                    if($v['is_replace'] == 1){
                        $kk = $kk+1;
                        
                         $replace_data = $this->get_replace_refund($v['replace_rec_id']);
                         $replace_data['replace_type'] = 1;
                         $replace_data['goods_price'] = 0;
 
 
                         $goods_list[$kk] = $replace_data;
 
                     }*/
 
                     $kk ++;
 
                    
            }			
            $order_info['extend_two_stage_refund']     = $goods_list;
        }
        
		
		
		
        //三阶段退款
        if (in_array('three_stage_refund',$extend)) {
            
            //获取当前
            $refund = model('order_snapshot_refund')->getRefundInfo(['order_id' => $order_info['order_id'],  'snapshot_id' => $order_info['snapshot_id'], 'refund_stage' =>2]);
            
            //取商品列表
            $order_goods_list = model('order_snapshot_goods_refund')->getRefundList(
                  array(				
                      'refund_id' => $refund['refund_id']
                  )
              );
        	$goods_list = array();
        	$goods_num = 0;
        	foreach($order_goods_list as $k => $v){        	        
        	       	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
        			$goods_list[$k] = $v;
        			$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
        			$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
        			$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
        	}			
            $order_info['extend_three_stage_refund']     = $goods_list;        	
        }		
				
			
        		
		
		//返回订单操作记录
		if(in_array('order_log',$extend)){			 
			 $order_log = $this->getOrderLogList(array('order_id'=>$order_info['order_id'],'snapshot_id' => $order_info['snapshot_id']),'log_id asc');
			 $data = array();
			 foreach($order_log as $k => $v){
				$data[$k] = $v;
				$data[$k]['log_time'] = date('H:i',$v['log_time']);
			 }			 
			 $order_info['extend_order_log'] = $data; 
		}
		
		
		
        return $order_info;
    }
            
            

    //获取替换退款的商品
    private function get_replace_refund($rec_id){
        $data = model('order_snapshot_goods_replace')->getGoodsInfo(['rec_id' => $rec_id]);
        return $data;
        
    }



    /**
     * 订单操作历史列表
     * @param unknown $order_id
     * @return Ambigous <multitype:, unknown>
     */
    public function getOrderLogList($condition, $order = '')
    {
        return $this->table('order_snapshot_log')->where($condition)->order($order)->select();
    }
            
            
     //商品成分记录
		private function goodsIngrNew($goods_id){
					
			$goods_info = Model('goods')->getGoodsInfo(array('goods_id' => $goods_id));
							
					
			$data = array();		
			$where = array(
				'attr_id' => array('in',$goods_info['goods_ingr'])
			);		
			$row = model('goods_attr')->getGoodsAttrList($where);
			
			foreach($row as $v){
				
				
				
				if($v['attr_type'] == 1){			
					$content = explode('|',$v['attr_content']);
					$v['attr_name'] = $content[0];
					$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[1];
					$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[2];
				}else{
					$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
					$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
					
				}
				
				
				$data[$v['attr_class_id']]['name'] = $v['attr_class_name'];
				$data[$v['attr_class_id']]['child'][] = $v;
			}		
			
			$list = array();
			foreach($data as $v){
				$list[] = $v;
			}		
			return $list;		
		}
		       
            
            
    //获取商品ICON
    private function goodsIngr($goods_id)
    {
        $goods_info = Model('goods')->getGoodsInfo(array('goods_id' => $goods_id));
        $where = array(
                        'attr_id' => array('in',$goods_info['goods_ingr'])
                    );
        $ingr = model('goods_attr')->getGoodsAttrList($where);
        $ingr_data = array();
        foreach ($ingr as $k=> $v) {
            $ingr_data[$k]['attr_id'] =  $v['attr_id'];
            if ($v['attr_type'] == 1) {
                $new = explode("|", $v['attr_content']);
                $ingr_data[$k]['attr_name'] = $new[0];
                $ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
                $ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
            } else {
                $ingr_data[$k]['attr_name'] =  $v['attr_name'];
                $ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];
                $ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];
            }
        }
        return $ingr_data;
    }
            
    private function order_replace_goods($rec_id)
    {
        $list =    model('order_snapshot_goods_replace')->getGoodsList(array('rec_id'=>$rec_id));
        $data = array();
        foreach ($list as $v) {
            $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
            $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
            $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
            $data[] = $v;
        }
        return $data;
    }
        
    public function getOrderGoodsList($condition = array(), $fields = '*', $limit = null, $page = null, $order = 'rec_id desc', $group = null, $key = null)
    {
        return $this->table('order_snapshot_goods')->field($fields)->where($condition)->limit($limit)->order($order)->group($group)->key($key)->page($page)->select();
    }
            
            
    private function goodsImageUrl($image, $store_id)
    {
        if (empty($image)) {
            return '';
        }
        return UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS . $image;
    }
        
        
    public function getOrderCommonInfo($condition = array(), $field = '*')
    {
        return $this->table('order_common')->where($condition)->find();
    }
        
    public function getOrderPayInfo($condition = array(), $master = false, $lock = false)
    {
        return $this->table('order_pay')->where($condition)->master($master)->lock($lock)->find();
    }
}
