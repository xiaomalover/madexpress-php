<?php
/**
 * 当前商户的剩余的餐盒量
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_goods_specsModel extends Model {
    public function __construct() {
        parent::__construct('goods_specs');
    }

    /**
    
     * @param array $insert
     * @return boolean
     */
    public function addSpecs($insert) {
        return $this->insert($insert);
    }

    /**
     
     * @param array $condition
     * @param array $update 
     * @return boolean
     */
    public function editSpecs($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
    
     * @param unknown $condition
     * @return boolean
     */
    public function delSpecs($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     
     * @param array $condition
     * @return array
     */
    public function getSpecsCount($condition) {
        return $this->where($condition)->count();
    }

    /**

     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getSpecsList($condition, $field = '*', $page = 0, $order = 'id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }
	
	
	

   
	
	
    /**
   
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getSpecsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
