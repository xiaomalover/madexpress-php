<?php
/**
 * 租车模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_lease_evaluateModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_lease_evaluation');
    }

	
	
	
	
	
	/**
     * 添加
     * @param array $insert
     * @return boolean
     */
    public function addLeaseEvaluate($insert) {
        return $this->insert($insert);
    }

	public function addLeaseEvaluateAll($insert){
		return $this->insertAll($insert);
		
		
	}
	
	
    /**
     * 编辑
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editLeaseEvaluate($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除
     * @param unknown $condition
     * @return boolean
     */
    public function delLeaseEvaluate($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询数量
     * @param array $condition
     * @return array
     */
    public function getLeaseEvaluateCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getLeaseEvaluateList($condition, $field = '*', $page = 0, $order = 'evaluate_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLeaseEvaluateInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
	
	
	

}
