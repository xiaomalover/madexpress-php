<?php
/**
 * 推送
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class stoketModel extends Model{
	
   
	public function __construct() {
        parent::__construct('stoket');
    }
	
	
	
	//指派
	public function sendAppoint($order_sn='',$uid=0){			
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
	
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		
		$where = array(
			'order_sn'=>$order_sn,			
			'state' => 0,
			'send_state' => 0 
			
		);		
		$polling = model('order_polling')->getPollingInfo($where);		
		
		$order_info = model('order')->getOrderInfoN(array('order_sn'=>$order_sn));
		
		$data_stock = array(
			'type'=> 'rob',
			'order_sn' => $order_sn,
			'order_info'=> $order_info
		);	
		
		$data = json_encode($data_stock);				
	    $gateway::sendToUid($polling['waiter_id'],$data);
		
		$data = array();
		$data['send_state'] = 1;
		$where = array(
			'id' => $polling['id']
		);
		$this->editPolling($data,$where);
		
		
		
	}
	
	
	
	
	
	//推送
	public function send($order_sn='',$uid=0){	
		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		
								
		$where = array(
			'order_sn'=>$order_sn,
			'state' => 0,
			'send_state' => 0 
		);			
		$polling = model('order_polling')->getPollingInfo($where);			
		$queue = $this->checkQueue($polling['waiter_id']);	
		if($queue  > 0 ){
				$data = array();
				$data['send_state'] = 2;
				$where = array(
					'id' => $polling['id']
				);
				$this->editPolling($data,$where);	
					
				$data_stock = array(
					'type'=> 'queue'
				);	
				
				$data = json_encode($data_stock);								
				$gateway::sendToUid($polling['waiter_id'],$data);				
				
		}else{
		
			
			//统计当前配送员有鸡蛋
			 
				$where = array(
					'distributor_id' =>$polling['waiter_id'],
					'order_state' => array('in','30,40')
			    );
			   $count = model('order')->getOrderCount($where);		
				if($count == 0){
					$this->sendAppoint($order_sn,0,$polling['id']);
					return '';
				}
			
				//修改
			$data = array();
			$data['send_state'] = 1;
			$where = array(
				'id' => $polling['id']
			);
			$this->editPolling($data,$where);
			
			$order_info = model('order')->getOrderInfoN(array('order_sn'=>$polling['order_sn']));
		
			$data_stock = array(
				'type'=> 'rob',
				'order_sn' => $polling['order_sn'],
				'order_info' => $order_info
			);		
			$data = json_encode($data_stock);	
			
			
			
			if($uid == 0 ){	
				//$gateway::sendToAll($data);	
				$gateway::sendToUid($polling['waiter_id'],$data);
				
			}else{			
				$gateway::sendToUid($uid,$data);
			}
			
			
		}
	}
	
	//获取当前是否正在执行的操作
	public function checkQueue($waiter_id){
		return  model('order_polling')->getPollingCount(array('waiter_id'=>$waiter_id,'send_state'=>'1','state'=> 0));		
		
	}
	
	//处理列队推送
	
	public function sendQueue($waiter_id){		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		
		$polling = model('order_polling')->getPollingInfo(array('waiter_id'=>$waiter_id,'send_state'=>'2','state' => 0));		
		$data_stock = array(
			'type'=> 'rob',
			'order_sn' => $polling['order_sn']		
		);	
		$data = json_encode($data_stock);				
	    $gateway::sendToUid($polling['waiter_id'],$data);
	}
	
	
	
	/*
	给用户发送消息
	*/
	
	public function sendUser($user_id,$data){		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';
		$data_stock = array();
	
	/*	if(!empty($data['order_sn'])){
			$data_stock['order_sn'] = $data['order_sn'];
		}
	
		if(!empty($data['type'])){
			$data_stock['type'] = $data['type'];
		}
	
		if(!empty($data['state'])){
			$data_stock['state'] = $data['state'];
		}
		*/
		
		$data_stock = $data;
	
		$data = json_encode($data_stock);
		
	//	print_r($data);
	    $gateway::sendToUid($user_id,$data);
		
		return 'ok';
		
	}
	
	//绑定用户
	
	public function bind($client_id,$uid){		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		// 设置GatewayWorker服务的Register服务ip和端口，请根据实际情况改成实际值(ip不能是0.0.0.0)		
		$gateway::$registerAddress = '47.243.54.204:1238';	
		// 假设用户已经登录，用户uid和群组id在session中
		$uid      = $uid;
		// client_id与uid绑定
		$gateway::bindUid($client_id, $uid);
		
	}
	
	
	
	
	//给用户推送
	public function sendStatusWaiter($uid,$status){
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		
		$data_stock = array(			
			'type' => 'status',
			'status' => $status	
		);	
		$data = json_encode($data_stock);				
	    $gateway::sendToUid($uid,$data);
		
	}
	


	//给商户推送
	
	public function sendStore($uid,$data){
	    
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		
		
		
		$gateway::sendToUid($uid,$data);
	}
	
	
	//给骑手推送订单变更通知
	
	public function sendDelivery($uid,$data){		
		require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
		$gateway = new \GatewayClient\Gateway();	
		$gateway::$registerAddress = '47.243.54.204:1238';	
		$gateway::sendToUid($uid,$data);
	}
	
	
	
	
	public function editPolling($data,$condition) {
        return $this->table('order_polling')->where($condition)->update($data);
    }
	
	
	public function addStoket($data){		
		return $this->insert($data);		
	}
	
	public function getStoketInfo($condition,$field = '*'){
		return $this->field($field)->where($condition)->find();
	}
	
	
	public function editStoket($where=array(),$data=array()){		
		 return $this->where($where)->update($data);
	}
	
	public function delStoket($condition){
        return $this->where($condition)->delete();
    }

	
	
	
	
}