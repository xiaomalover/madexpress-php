<?php
/**
 * 订单商品替换

 */
defined('InMadExpress') or exit('Access Invalid!');

class order_snapshot_goods_replaceModel extends Model {
    public function __construct() {
        parent::__construct('order_snapshot_goods_replace');
    }

    /**
     * 添加Workhours
     * @param array $insert
     * @return boolean
     */
    public function addGoods($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Workhours
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editGoods($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Workhours
     * @param unknown $condition
     * @return boolean
     */
    public function delGoods($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Workhours数量
     * @param array $condition
     * @return array
     */
    public function getGoodsCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Workhours列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getGoodsList($condition, $group= '', $field = '*', $page = 0, $order = 'replace_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->group($group)->select();
    }




    /**
     * 取单个Workhours内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
