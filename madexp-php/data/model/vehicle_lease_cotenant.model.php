<?php
/**
 * 租车模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_lease_cotenantModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_lease_cotenant');
    }

	
	
	
	
	
	/**
     * 添加
     * @param array $insert
     * @return boolean
     */
    public function addLeaseCotenant($insert) {
        return $this->insert($insert);
    }

	public function addLeaseCotenantAll($insert){
		return $this->insertAll($insert);
		
		
	}
	
	
    /**
     * 编辑
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editLeaseCotenant($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除
     * @param unknown $condition
     * @return boolean
     */
    public function delLeaseCotenant($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询数量
     * @param array $condition
     * @return array
     */
    public function getLeaseCotenantCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getLeaseCotenantList($condition, $field = '*', $page = 0, $order = 'id desc', $group='',$limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->group($group)->select();
    }





    /**
     * 取单个内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLeaseCotenantInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
	
	
	

}
