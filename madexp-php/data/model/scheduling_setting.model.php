<?php
/**
 * 排班模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_settingModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_setting');
    }

    /**
     * 取店铺类别列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */
  
	 public function getSchedulingList($condition=array(), $page = 0, $order='setting_id desc', $field='*',$limit=100) {
	     $field = 'scheduling_region.id as detail_id,scheduling_region.num,scheduling_region.region_id,scheduling_region.region_name,
	     scheduling_region.m_bike,scheduling_region.pa_bike,scheduling_setting.*';
	     $list = $this->table('scheduling_region,scheduling_setting')->join('inner')->on('scheduling_region.setting_id = scheduling_setting.id')
             ->where($condition)->field($field)->page($page)->order($order)->limit($limit)->select();
//        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }
	
	public function getSchedulingNew($where,$order='id asc'){
		 return  $this->where($where)->order($order)->select();
	}
	
    /**
     * 取得单条信息
     * @param unknown $condition
     */
    public function getSchedulingInfo($condition = array()) {
        return $this->where($condition)->find();
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delScheduling($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addScheduling($data) {
        return $this->insert($data);
    }

    public function addSchedulingAll($data)
    {
        return $this->insertAll($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editScheduling($condition = array(),$data = array()) {
        return $this->where($condition)->update($data);
    }
}
