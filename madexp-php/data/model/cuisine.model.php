<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class cuisineModel extends Model {
    public function __construct() {
        parent::__construct('store_cuisine');
    }

    /**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addCuisine($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editCuisine($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delCuisine($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getCuisineCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getCuisineList($condition, $field = '*', $page = 0, $order = 'cuisine_id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getCuisineInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
