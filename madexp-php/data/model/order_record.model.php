<?php
/**
 * 订单LOG记录，最新
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class order_recordModel extends Model{

    public function __construct(){
        parent::__construct('order_record');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getRecordList($condition = array(), $page = '', $order = 'record_id desc', $field = '*', $limit = '') {
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getRecordInfo($condition,$fields = '*') {
        return $this->where($condition)->field($fields)->find();
    }



 /**
  * 订单LOG记录
  *
  * @param array $type 项目类型
  * @param obj $data 数据
  * @return array 二维数组
  */
 public function addRecord($type,$data = array()){
    		
    $insert = array();
    $insert['order_id'] = $data['order_id'];
    $insert['record_time'] = time();
 	$insert['record_type'] = $type;
	$insert['record_popup'] = 0;
    $record_data = array();
	
    switch($type){
		
		
 				case 'order_placed': //订单下单	[代码已加] member_buy   member_payment  OK
									
					$record_data['order_addtime'] = $data['order_addtime']; //订单下单时间
					$insert['record_msg'] = 'Order Placed';
					
 				break; 			
 				case 'order_cancel': //订单已取消 [代码已加] member_order OK
					
					$record_data['order_cancel_time'] = $data['order_cancel_time']; //订单去取消时间
					$record_data['order_cancel_content'] = $data['order_cancel_content']; //订单取消备注
					$record_data['role'] = $data['role']; //操作方 Platform 平台 Merchant 商家 Customer 顾客					
					$insert['record_popup'] = 1; //需要点击弹出
					$insert['record_msg'] = 'Order Cancel';
					
 				break; 								
 				case 'order_confirmed_courier': // (courier) 配送员已接单  【直推的订单在member_buy】 OK
				
					$record_data['delivery_code'] = $data['delivery_code']; //配送员编号
					$record_data['order_code'] = $data['order_code'];//当前批次第几单
					$record_data['batch_code'] = $data['batch_code'];

					$record_data['send_delivery_time'] = $data['send_delivery_time']; //推送给配送员的时间
					$insert['record_popup'] = 1; //需要点击弹出
					$insert['record_msg'] = 'Order Confirmed (Courier)';
				
 				break;
 				
 				case 'order_display': // (merchant) 商家展示订单 【代码已加】member_buy				OK
					$record_data['send_store_time'] = $data['send_store_time']; //推送给商家的时间				
					$insert['record_msg'] = 'Order Display (Merchant)';
				break;
				 
 	  		    case 'order_confirmed_merchant': // (merchant)商家已接单 【代码已加】 seller_order line 402 ok
				
					$record_data['store_taking_time'] = $data['store_taking_time']; //商家接单时间
					$record_data['store_taking_type'] = $data['store_taking_type']; //商家接单类型，自动 Auto，手动 Manual
					$insert['record_msg'] = 'Order Confirmed (Merchant)';
			
				break;
								
 	  		    case 'ca_merchant': //配送员到达商家附近  [代码已加] 
					
					$record_data['delivery_store_time'] = $data['delivery_store_time']; //配送员抵达商家附近
					$insert['record_msg'] = 'CA.Merchant';
								
				break;
									
				case 'order_collected': //已取餐 [代码已加] order_allocation line 294				
				
					if($data['pick_type'] == 'courier'){						
						$record_data['delivery_code'] = $data['delivery_code']; //如果在配送员哪里取餐，这个就必须传了。						
					}				
					$record_data['pick_type'] = $data['pick_type']; //取餐类型 在商家取餐merchant，在配送员取餐delivery
					$record_data['pick_time'] = $data['pick_time']; //取餐时间
					$insert['record_msg'] = 'Order Collected';
					
					
				break;
				
 	  		    case 'picked_by_customer': //顾客已取餐、自提单 [代码已加] member_order  line 295 
					
					$record_data['pick_type'] = $data['pick_type'];	//谁操作的自提，用户 Customer，商家 Merchant								
					$record_data['pick_time'] = $data['pick_time']; //取餐时间
					$insert['record_msg'] = 'Picked by Customer';
					
				break;
				
 	  		    case 'start_delivery': //配送员开始配送 [代码已加] order_allocation line 337
					 
					$record_data['order_num'] = $data['order_num']; //本次批次的订单数
					$record_data['batch_code'] = $data['batch_code']; //批次编号  配送员编号 +140821 + 序号 001
					$record_data['start_delivery_time'] = $data['start_delivery_time']; //开始配送时间，提取最后一餐的时间。
					$insert['record_msg'] = 'Start Delivery';
					$insert['record_popup'] = 1; //需要点击弹出
				break;
				
 	  		    case 'ca_customer': //配送员到达顾客附近
					
					$record_data['delivery_customer_time'] = $data['delivery_customer_time']; //配送员抵达用户附近时间							
					$insert['record_msg'] = 'CA.Customer';
									
				
				break;
				
 	  		    case 'order_delivered': //订单已送达		[代码已加]	order_alloction line 375
					
					$record_data['delivery_time'] = $data['delivery_time']; //订单送达时间
					$insert['record_msg'] = 'Order Delivered';
						
					
				break;
				
 	  		    case 'refund_request': //顾客退款申请 【功能已剔除】
				
				
					$record_data['refund_order_content'] = $data['refund_content']; //退款申请的理由
					$record_data['refund_order_time'] = $data['refund_order_time'];
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Refund Request';
					
				
				break; 	
 	  		    case 'refund_request_granted': //顾客退款审核通过 【功能已剔除】
				
					
					$record_data['refund_order_content'] = $data['refund_content']; //退款申请的理由
					$record_data['refund_order_time'] = $data['refund_order_time'];
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Refund Request Granted';
				
				break;
				
 	  		    case 'refund_request_refused': //顾客退款审核驳回 【功能已剔除】
				
					
					$record_data['refund_refused_time'] = $data['refund_refused_time']; //驳回时间
					$record_data['refund_refused_type'] = $data['refund_refused_type']; //驳回类型  Manual Auto
					$insert['record_msg'] = 'Refund Request Refused';
									
				break;				
 	  		  
				case 'order_pending': //商家点击且慢后 【功能剔除】
					
					$record_data['order_pending_ids'] = $data['order_pending_ids']; //点击且慢的订单
					$record_data['order_pending_time'] = $data['order_pending_time']; //点击且慢的时间
					
					$insert['record_msg'] = 'Order Pending';
					$insert['record_popup'] = 1; //需要点击弹出
					
				break;
 	  		   
				case 'order_resume': //且慢状态解除后 【功能剔除】
					
					$record_data['order_resume_time'] = $data['order_resume_time']; //点击且慢的时间
					$insert['record_msg'] = 'Order Resume';
					
				break;		

				
				case 'merchant_refund': //商家操作退款 [代码已加] seller_refund line 200
					
					$record_data['merchant_refund_type'] = $data['merchant_refund_type']; //部分退款 全单退款
					$record_data['merchant_refund_time'] = $data['merchant_refund_time']; //推荐时间
					$record_data['merchant_refund_comment'] = $data['merchant_refund_comment']; //推荐时间
					
					$record_data['refund_id'] = $data['refund_id'];
					$record_data['coupon_id'] = $data['coupon_id'];					
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Merchant Refund';
									
				break;
 	  		    
				case 'me_refund': //平台操作退款 [代码已加]
					
					$record_data['me_refund_type'] = $data['me_refund_type']; //部分退款 全单退款
					$record_data['me_refund_time'] = $data['me_refund_time']; //推荐时间
					$record_data['me_refund_info'] = $data['me_refund_info']; //推荐时间
					$record_data['me_refund_comment'] = $data['me_refund_comment']; //推荐时间
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'ME Refund';
							
				break;
 	  		    
				case 'socket_disconnect': //Socket断开
					/*
					断开类型
					Courier (A) 配送员断开（自动的）
					Courier (M) 配送员断开（手动如触发下线的）
					Merchant (A) 商户断开（自动）
					Merchant (M)商户断开（手动触发下线的）											
					*/					
					$record_data['socket_disconnect_type'] = $data['socket_disconnect_type']; // 断开类型
					$record_data['socket_disconnect_time'] = $data['socket_disconnect_time']; // 断开时间
					
					$insert['record_role'] = $data['role'];
					$insert['record_msg'] = 'Socket Disconnect';
					
				break;
					
 	  		    case 'attention': //订单可疑上报 【代码已加】 waiter_order 
					/*
					
					CA.Merchant 到店时间异常 2分钟
					Delivery Time 配送时间异常 2分钟
					Courier Socket 	配送员Socket链接异常 1分钟
					Merchant Socket 打单机Socket链接异常（断开超过10秒） 
					
					*/
					$record_data['attention_type'] = $data['attention_type'];
					$record_data['attention_time'] = $data['attention_time'];
					$insert['record_msg'] = 'Attention';	
					$insert['record_popup'] = 1; //需要点击弹出			
				break; 	
 				case 'action_required': //订单变为问题单  【代码已加】
					
					/*
					
					"CA.Merchant "到店时间超时 5分钟
					Delivery T.O 配送时间超时 5分钟
					
					Courier Socket T.O 	配送员Socket链接断开超时 5分钟
					Merchant Socket T.O 打单机Socket链接断开超时 5分钟
					
					Courier M.Offline 配送员手动下线
					Merchant M.Offline 商家手动下线
					
					Courier Report 	配送员问题上报
					Customer Report 顾客订单投诉/寻求帮助
					Merchant PTO"	商家超时未接单"						

					*/
					
					$record_data['action_required_type'] = $data['action_required_type']; //类型
					$record_data['action_required_time'] = $data['action_required_time']; //时间
					$record_data['action_required_id'] = $data['action_required_time']; //如果有上报问题则关联
					$insert['record_msg'] = 'Action Required';
					$insert['record_popup'] = 1; //需要点击弹出
				break; 						
 	  		    case 'resolved': //问题单状态解除 21 [代码已加]
				
						/* "AR. CA.Merchant "到店时间超时   
							AR. Delivery Time 配送时间超时
							AR. Courier Socket T.O 配送员Socket链接断开超时
							AR. Merchant Socket T.O 打单机Socket链接断开超时
							AR. Courier M.Offline 配送员手动下线
							AR. Merchant M.Offline 商家手动下线
							AR. Courier Report 配送员问题上报
							AR. Customer Report" 顾客订单投诉/寻求帮助"
						*/
						$record_data['resolved_type'] = $data['resolved_type']; //类型
						$record_data['resolved_time'] = $data['resolved_time']; //时间
						$record_data['resolved_comment'] = $data['resolved_comment']; //时间
						$insert['record_msg'] = 'Resolved';
						$insert['record_popup'] = 1; //需要点击弹出
													
				break; 	
 	  		    case 'change_courier': //重新分配配送员
					
					$record_data['delivery_code'] = $data['delivery_code'];					
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Change Courier';
						
				
				break;
 	  		    case 'voucher_issued': //优惠券派发
					
					/*
					优惠券派发类型
					Merchant $x
					ME $x
					*/					
					$record_data['voucher_type'] = $data['voucher_type'];					
					$record_data['voucher_code'] = $data['voucher_code'];
					$record_data['voucher_price'] = $data['voucher_price'];
					$record_data['voucher_time'] = $data['voucher_time'];
				
					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Voucher Issued';
					
				break;

				case 'evaluate': //评价订单

					$record_data['evaluate_store_score'] = $data['store_score'];					
					$record_data['evaluate_delivery_score'] = $data['delivery_score'];
					$record_data['evaluate_store_content'] = $data['store_content'];
					$record_data['evaluate_delivery_content'] = $data['delivery_content'];

					$insert['record_popup'] = 1;
					$insert['record_msg'] = 'Order Evaluate';

				break;


	
    }
    
		$insert['record_datas'] = serialize($record_data);    
	//	print_r($insert);
		$row = $this->insert($insert);
	//	print_r($row);
		if (!$row) {
			throw new Exception('操作失败');
		}	
		return $row;
    
	}
	
	
	public function addRecordAll($data){		
		return $this->insertAll($data);		
	}
	

    public function editRecord($data,$condition) {
        return $this->where($condition)->update($data);
    }
	
	

    public function getRecordCount($condition) {
        return $this->where($condition)->count();
    }
	
	
	public function delRecord($condition) {
        return $this->where($condition)->delete();
    }
	
	

}
