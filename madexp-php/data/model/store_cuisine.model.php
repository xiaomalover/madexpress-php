<?php
/**
 * 店铺分类

 */
defined('InMadExpress') or exit('Access Invalid!');
class store_cuisineModel extends Model{

    public function __construct(){
        parent::__construct('store_cuisine');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getStoreCuisineList($condition,$page='',$order='',$field='*', $limit = ''){
        $result = $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
        return $result;
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getStoreCuisineInfo($condition){
        $result = $this->where($condition)->find();
        return $result;
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function addStoreCuisine($param){
        return $this->insert($param);
    }

    /*
     * 增加
     * @param array $param
     * @return bool
     */
    public function addStoreCuisineAll($param){
        return $this->insertAll($param);
    }

    /*
     * 更新
     * @param array $update
     * @param array $condition
     * @return bool
     */
    public function editStoreCuisine($update, $condition){
        return $this->where($condition)->update($update);
    }

    /*
     * 删除
     * @param array $condition
     * @return bool
     */
    public function delStoreCuisine($condition){
        return $this->where($condition)->delete();
    }

    /**
     * 总数量
     * @param unknown $condition
     */
    public function getStoreCuisineCount($condition = array()) {
        return $this->where($condition)->count();
    }




}
