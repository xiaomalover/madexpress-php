<?php
/**
 * 车辆事件模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_eventModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_event');
    }

	
    public function vehicleEventList($condition, $field = '*', $page = 0, $order = 'event_id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

	
	
	public function vehicleEventInfo($where,$field='*'){		
		   return $this->field($field)->where($where)->find();
	}
	
	
	public function vehicleEventAdd($param)
    {
        return $this->insert($param);
    }

	
	 public function editEventVehicle($condition, $update) {
		 
        return $this->where($condition)->update($update);
		
    }
	
	
	
	
}
