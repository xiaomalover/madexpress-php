<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class meModel extends Model {
    public function __construct() {
        parent::__construct('me');
    }

    /**
     * 添加Kpi
     * @param array $insert
     * @return boolean
     */
    public function addMe($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Kpi
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editMe($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Kpi
     * @param unknown $condition
     * @return boolean
     */
    public function delMe($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Kpi数量
     * @param array $condition
     * @return array
     */
    public function getMeCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Kpi列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getMeList($condition, $field = '*', $page = 0, $order = 'id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Kpi内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getMeInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
