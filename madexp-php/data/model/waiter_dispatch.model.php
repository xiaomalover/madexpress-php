<?php
/**
 * 送餐员模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class waiter_dispatchModel extends Model {

    public function __construct(){
        parent::__construct('distributor_dispatch');
    }

    /**
     * 会员详细信息（查库）
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getDispatchInfo($condition, $field = '*', $master = false) {
        return $this->table('distributor_file')->field($field)->where($condition)->master($master)->find();
    }


    /**
     * 会员列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getDispatchList($condition = array(), $field = '*', $page = null, $order = 'id desc', $limit = '') {
       return $this->table('distributor_file')->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

	
	/**
	 * 删除会员
	 *
	 * @param int $id 记录ID
	 * @return array $rs_row 返回数组形式的查询结果
	 */
	public function del($id){
		if (intval($id) > 0){
			$where = " file_id = '". intval($id) ."'";
			$result = Db::delete('distributor_file',$where);
			return $result;
		}else {
			return false;
		}
	}

	 public function addDispatch($param){
        return $this->insert($param);
    }

	
    /**
     * 会员数量
     * @param array $condition
     * @return int
     */
    public function getDispatchCount($condition) {
        return $this->table('distributor_file')->where($condition)->count();
    }

    /**
     * 编辑会员
     * @param array $condition
     * @param array $data
     */
    public function editDispatch($condition, $data) {
        $update = $this->table('distributor_file')->where($condition)->update($data);      
        return $update;
    }
}
