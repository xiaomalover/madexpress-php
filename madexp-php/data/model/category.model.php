<?php
/**
 * 商品类别模型
 *
 *
 *
 *
 */

defined('InMadExpress') or exit('Access Invalid!');

class categoryModel extends Model
{
    /**
     * 缓存数据
     */
    protected $cachedData;

    /**
     * 缓存数据 原H('goods_class')形式
     */
    protected $gcForCacheModel;

    public function __construct() {
        parent::__construct('category');
    }

    /**
     * 获取缓存数据
     *
     * @return array
     * array(
     *   'data' => array(
     *     // Id => 记录
     *   ),
     *   'parent' => array(
     *     // 子Id => 父Id
     *   ),
     *   'children' => array(
     *     // 父Id => 子Id数组
     *   ),
     *   'children2' => array(
     *     // 1级Id => 3级Id数组
     *   ),
     * )
     */
    public function getCache() {
        if ($this->cachedData) {
            return $this->cachedData;
        }
        $data = rkcache('gc_class');
        if (!$data) {
            $data = array();
            foreach ((array) $this->getGoodsClassList(array()) as $v) {
                $id = $v['cate_id'];
                $pid = $v['cate_parent_id'];
                $data['data'][$id] = $v;
                $data['parent'][$id] = $pid;
                $data['children'][$pid][] = $id;
            }
            foreach ((array) $data['children'][0] as $id) {
                foreach ((array) $data['children'][$id] as $cid) {
                    foreach ((array) $data['children'][$cid] as $ccid) {
                        $data['children2'][$id][] = $ccid;
                    }
                }
            }
            wkcache('gc_class', $data);
        }
        return $this->cachedData = $data;
    }

    /**
     * 删除缓存数据
     */
    public function dropCache() {
        $this->cachedData = null;
        $this->gcForCacheModel = null;

        dkcache('gc_class');
        dkcache('all_categories');
    }
    //获取指定分类的子分类
    public function get_child_category($id='')
    {
        $condition = "cate_parent_id = ". $id ;
        $result = $this->table('category')->field('*')->where($condition)->order('cate_id desc')->limit(false)->select();
        return $result;
    }
    /**
     * 类别列表
     *
     * @param  array   $condition  检索条件
     * @return array   返回二位数组
     */
    public function getGoodsClassList($condition, $field = '*') {
        $result = $this->table('category')->field($field)->where($condition)->order('cate_parent_id asc,cate_sort asc,cate_id asc')->limit(false)->select();
        return $result;
    }

    /**
     * 从缓存获取全部分类
     */
    public function getGoodsClassListAll() {
        $data = $this->getCache();
        return array_values((array) $data['data']);
    }

    /**
     * 从缓存获取全部分类 分类id作为数组的键
     */
    public function getGoodsClassIndexedListAll() {
        $data = $this->getCache();
        return (array) $data['data'];
    }

    /**
     * 从缓存获取分类 通过分类id数组
     *
     * @param array $ids 分类id数组
     */
    public function getGoodsClassListByIds($ids) {
        $data = $this->getCache();
        $ret = array();
        foreach ((array) $ids as $i) {
			
            if ($data['data'][$i]) {
                $ret[] = $data['data'][$i];
            }
        }
        return $ret;
    }

    /**
     * 从缓存获取分类 通过同级分类id
     *
     * @param int $id 分类id
     */
    public function getGoodsClassListBySiblingId($id) {
        if ($id < 1) {
            return;
        }
        $data = $this->getCache();
        if (!isset($data['parent'][$id])) {
            return;
        }

        $pid = $data['parent'][$id];
        return $this->getGoodsClassListByParentId($pid);
    }

    /**
     * 从缓存获取分类 通过上级分类id
     *
     * @param int $pid 上级分类id 若传0则返回1级分类
     */
    public function getGoodsClassListByParentId($pid) {
        $data = $this->getCache();
     //   print_r($data);
        $ret = array();
        foreach ((array) $data['children'][$pid] as $i) {
            if ($data['data'][$i]) {
                $ret[] = $data['data'][$i];
            }
        }
        return $ret;
    }

    /**
     * 从缓存获取分类 通过分类id
     *
     * @param int $id 分类id
     */
    public function getGoodsClassInfoById($id) {
        $data = $this->getCache();
        return $data['data'][$id];
    }
    
    /**
     * 根据一级分类id取得所有三级分类
     */
    public function getChildClassByFirstId($id) {
        $data = $this->getCache();
        $result = array();
        if (!empty($data['children2'][$id])) {
            foreach ($data['children2'][$id] as $val) {
                $child = $data['data'][$val];
                $result[$child['cate_parent_id']]['class'][$child['cate_id']] = $child['gc_name'];
                $result[$child['cate_parent_id']]['name'] = $data['data'][$child['cate_parent_id']]['gc_name'];
            }
        }
        return $result;
    }

    /**
     * 返回缓存数据 原H('goods_class')形式
     */
    public function getGoodsClassForCacheModel() {

        if ($this->gcForCacheModel)
            return $this->gcForCacheModel;

        $data = $this->getCache();

        $r = $data['data'];
        $p = $data['parent'];
        $c = $data['children'];
        $c2 = $data['children2'];

        $r = (array) $r;

        foreach ($r as $k => & $v) {
            if ((string) $p[$k] == '0') {
                $v['depth'] = 1;
                if ($data['children'][$k]) {
                    $v['child'] = implode(',', $c[$k]);
                }
                if ($data['children2'][$k]) {
                    $v['childchild'] = implode(',', $c2[$k]);
                }
            } else if ((string) $p[$p[$k]] == '0') {
                $v['depth'] = 2;
                if ($data['children'][$k]) {
                    $v['child'] = implode(',', $c[$k]);
                }
            } else if ((string) $p[$p[$p[$k]]] == '0') {
                $v['depth'] = 3;
            }
        }

        return $this->gcForCacheModel = $r;
    }

    /**
     * 更新信息
     * @param unknown $data
     * @param unknown $condition
     */
    public function editGoodsClass($data = array(), $condition = array()) {
        // 删除缓存
        $this->dropCache();
        return $this->where($condition)->update($data);
    }


        /**
     * 删除商品分类
     * @param unknown $condition
     * @return boolean
     */
    public function delGoodsClass($condition) {
        // 删除缓存
        $this->dropCache();
        return $this->where($condition)->delete();
    }

    /**
     * 删除商品分类
     *
     * @param array $gcids
     * @return boolean
     */
    public function delGoodsClassByGcIdString($gcids) {
        $gcids = explode(',', $gcids);
        if (empty($gcids)) {
            return false;
        }
        $goods_class = $this->getGoodsClassForCacheModel();
        $gcid_array = array();
        foreach ($gcids as $gc_id) {
            $child = (!empty($goods_class[$gc_id]['child'])) ? explode(',', $goods_class[$gc_id]['child']) : array();
            $childchild = (!empty($goods_class[$gc_id]['childchild'])) ? explode(',', $goods_class[$gc_id]['childchild']) : array();
            $gcid_array = array_merge($gcid_array, array($gc_id), $child, $childchild);
        }
        // 删除商品分类
        $this->delGoodsClass(array('cate_id' => array('in', $gcid_array)));
       
       
        // 删除店铺绑定分类
        Model('store_bind_class')->delStoreBindClass(array('class_1|class_2|class_3' => array('in', $gcid_array)));
    
	
        // 商品下架
        Model('goods')->editProducesLockUp(array('goods_stateremark' => '商品分类被删除，需要重新选择分类'), array('cate_id' => array('in', $gcid_array)));
        return true;
    }

    /**
     * 新增商品分类
     * @param array $insert
     * @return boolean
     */
    public function addGoodsClass($insert) {
        // 删除缓存
        $this->dropCache();
        return $this->insert($insert);
    }

    /**
     * 取分类列表，最多为三级
     *
     * @param int $show_deep 显示深度
     * @param array $condition 检索条件
     * @return array 数组类型的返回结果
     */
    public function getTreeClassList($show_deep='3',$condition=array()){
        $class_list = $this->getGoodsClassList($condition);
        $goods_class = array();//分类数组
        if(is_array($class_list) && !empty($class_list)) {
            $show_deep = intval($show_deep);
            if ($show_deep == 1){//只显示第一级时用循环给分类加上深度deep号码
                foreach ($class_list as $val) {
                    if($val['cate_parent_id'] == 0) {
                        $val['deep'] = 1;
                        $goods_class[] = $val;
                    } else {
                        break;//父类编号不为0时退出循环
                    }
                }
            } else {//显示第二和三级时用递归
                $goods_class = $this->_getTreeClassList($show_deep,$class_list);
            }
        }
        return $goods_class;
    }

    /**
     * 递归 整理分类
     *
     * @param int $show_deep 显示深度
     * @param array $class_list 类别内容集合
     * @param int $deep 深度
     * @param int $parent_id 父类编号
     * @param int $i 上次循环编号
     * @return array $show_class 返回数组形式的查询结果
     */
    private function _getTreeClassList($show_deep,$class_list,$deep=1,$parent_id=0,$i=0){
        static $show_class = array();//树状的平行数组
        if(is_array($class_list) && !empty($class_list)) {
            $size = count($class_list);
            if($i == 0) $show_class = array();//从0开始时清空数组，防止多次调用后出现重复
            for ($i;$i < $size;$i++) {//$i为上次循环到的分类编号，避免重新从第一条开始
                $val = $class_list[$i];
                $gc_id = $val['cate_id'];
                $gc_parent_id   = $val['cate_parent_id'];
                if($gc_parent_id == $parent_id) {
                    $val['deep'] = $deep;
                    $show_class[] = $val;
                    if($deep < $show_deep && $deep < 3) {//本次深度小于显示深度时执行，避免取出的数据无用
                        $this->_getTreeClassList($show_deep,$class_list,$deep+1,$gc_id,$i+1);
                    }
                }
                if($gc_parent_id > $parent_id) break;//当前分类的父编号大于本次递归的时退出循环
            }
        }
        return $show_class;
    }

    /**
     * 取指定分类ID下的所有子类
     *
     * @param int/array $parent_id 父ID 可以单一可以为数组
     * @return array $rs_row 返回数组形式的查询结果
     */
    public function getChildClass($parent_id){
        static $_cache;
        if ($_cache !== null) return $_cache;
        $all_class = $this->getGoodsClassListAll();
        if (is_array($all_class)){
            if (!is_array($parent_id)){
                $parent_id = array($parent_id);
            }
            $result = array();
            foreach ($all_class as $k => $v){
                $gc_id  = $v['cate_id'];//返回的结果包括父类
                $gc_parent_id   = $v['cate_parent_id'];
                if (in_array($gc_id,$parent_id) || in_array($gc_parent_id,$parent_id)){
                    $parent_id[] = $v['cate_id'];
                    $result[] = $v;
                }
            }
            $return = $result;
        }else {
            $return = false;
        }
        return $_cache = $return;
    }

    

    /**
     * 取指定分类ID的所有父级分类
     *
     * @param int $id 父类ID/子类ID
     * @return array $nav_link 返回数组形式类别导航连接
     */
    public function getGoodsClassLineForTag($id = 0) {
        if (intval($id)> 0) {
            $gc_line = array();
            /**
             * 取当前类别信息
             */
            $class = $this->getGoodsClassInfoById(intval($id));
            $gc_line['cate_id'] = $class['cate_id'];
            $gc_line['type_id'] = $class['type_id'];
            $gc_line['gc_virtual'] = $class['gc_virtual'];
            /**
             * 是否是子类
             */
            if ($class['cate_parent_id'] != 0) {
                $parent_1 = $this->getGoodsClassInfoById($class['cate_parent_id']);
                if ($parent_1['cate_parent_id'] != 0) {
                    $parent_2 = $this->getGoodsClassInfoById($parent_1['cate_parent_id']);
                    $gc_line['gc_id_1'] = $parent_2['cate_id'];
                    $gc_line['gc_tag_name'] = trim($parent_2['gc_name']) . ' >';
                    $gc_line['gc_tag_value'] = trim($parent_2['gc_name']) . ',';
                }
                if (!isset($gc_line['gc_id_1'])) {
                    $gc_line['gc_id_1'] = $parent_1['cate_id'];
                } else {
                    $gc_line['gc_id_2'] = $parent_1['cate_id'];
                }
                $gc_line['gc_tag_name'] .= trim($parent_1['gc_name']) . ' >';
                $gc_line['gc_tag_value'] .= trim($parent_1['gc_name']) . ',';
            }
            if (!isset($gc_line['gc_id_1'])) {
                $gc_line['gc_id_1'] = $class['cate_id'];
            } else if (!isset($gc_line['gc_id_2'])) {
                $gc_line['gc_id_2'] = $class['cate_id'];
            } else {
                $gc_line['gc_id_3'] = $class['cate_id'];
            }
            $gc_line['gc_tag_name'] .= trim($class['gc_name']) . ' >';
            $gc_line['gc_tag_value'] .= trim($class['gc_name']) . ',';
        }
        $gc_line['gc_tag_name'] = trim($gc_line['gc_tag_name'], ' >');
        $gc_line['gc_tag_value'] = trim($gc_line['gc_tag_value'], ',');
        return $gc_line;
    }

	
	//订单完成后。更新商家所在的几个分类销量。
	public function cate_sale_num($store_id){		
		
		$list = model('store_bind_class')->getStoreBindClassList(array('store_id' => $store_id));		
		foreach($list as $v){			
			$this->editGoodsClass( array('exp', "cate_sale_num + 1"),array('cate_id' => $v['class_id']));			
		}
				
	}
	



}
