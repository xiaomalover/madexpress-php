<?php
/**
 * 商户端钱包

 */
defined('InMadExpress') or exit('Access Invalid!');
class store_walletModel extends Model {
   
	/**
     * 生成充值编号
     * @return string
     */
    public function makeSn() {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $_SESSION['member_id'] % 1000);
    }
	
    /**
     * 取提现单信息总数
     * @param unknown $condition
     */
    public function getPdCashCount($condition = array()) {
        return $this->table('store_pd_cash')->where($condition)->count();
    }
	
	
    /**
     * 取得提现列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdCashList($condition = array(), $pagesize = '', $fields = '*', $order = 'lg_id desc', $limit = '') {
        return $this->table('store_pd_cash')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加提现记录
     * @param array $data
     */
    public function addPdCash($data) {
        return $this->table('store_pd_cash')->insert($data);
    }

    /**
     * 编辑提现记录
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdCash($data,$condition = array()) {
        return $this->table('store_pd_cash')->where($condition)->update($data);
    }

    /**
     * 取得单条提现信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdCashInfo($condition = array(), $fields = '*') {
        return $this->table('store_pd_cash')->where($condition)->field($fields)->find();
    }

    /**
     * 删除提现记录
     * @param unknown $condition
     */
    public function delPdCash($condition) {
        return $this->table('store_pd_cash')->where($condition)->delete();
    }
	
	
	
    /**
     * 取日志总数
     * @param unknown $condition
     */
    public function getPdLogCount($condition = array()) {
        return $this->table('store_pd_log')->where($condition)->count();
    }

	
    /**
     * 取得预存款变更日志列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdLogList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('store_pd_log')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    


	/*
	更新退款日志
	*/

 /**
     * 删除日志
     * @param unknown $condition
     */
    public function delPdLog($condition = array()) {
        return $this->table('store_pd_log')->where($condition)->delete();
    }


    public function getPdInfo($condition = array(), $fields = '*') {
        return $this->table('store_pd_log')->where($condition)->field($fields)->find();
    }


	
   
    /**
     * 变更余额
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();
		
        $data_log['lg_invite_store_id'] = $data['invite_store_id'];
        $data_log['lg_store_id'] = $data['store_id'];
        $data_log['lg_store_name'] = $data['store_name'];
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
        switch ($change_type){
           
			
			
			
				case 'online_sales_income': //线上销售收入
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'Online Sales Income';
					$data_log['lg_name_of_activities'] = 'Online Sales Income';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];		
					$data_log['lg_order_id'] =  $data['order_id'];		
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
			
				
				case 'offline_sales_income': //线下销售收入
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'Offline Sales Income';
					$data_log['lg_name_of_activities'] = 'Offline Sales Income';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];		
					$data_log['lg_order_id'] =  $data['order_id'];		
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
				
				
				case 'sales_reimbursement': //销售补贴
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'Sales Reimbursement';
					$data_log['lg_name_of_activities'] = 'Sales Reimbursment';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];		
					$data_log['lg_order_id'] =  $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
				
				
				case 'me_voucher_reimbursement': //优惠券补贴款
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'Voucher Reimbursement';
					$data_log['lg_name_of_activities'] = 'ME Voucher Reimbursement';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];		
					$data_log['lg_order_id'] =  $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
				
				
				case 'platform_bonus': //平台奖金
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'Platform Bonus';
					$data_log['lg_name_of_activities'] = 'Platform Bonus';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];	
					$data_log['lg_order_id'] =  $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
			
			
				case 'me_wallet_top_up': //ME钱包充值
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'ME Wallet Top Up';
					$data_log['lg_name_of_activities'] = 'ME Wallet Top Up';
					$data_log['lg_type_activities'] = 0;
					$data_log['lg_order_sn'] =  $data['order_sn'];				
					$data_log['lg_order_id'] =  $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
			
				case 'me_refund': //平台服务的（所有类型）退款
					$data_log['lg_av_amount'] = +$data['amount'];
					$data_log['lg_desc'] = 'ME Refund';
					$data_log['lg_name_of_activities'] = 'ME Refund';
					$data_log['lg_type_activities'] = 0;				
					$data_log['lg_order_sn'] =  $data['order_sn'];		
					$data_log['lg_order_id'] =  $data['order_id'];						
					$data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);				
					$data_msg['av_amount'] = +$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
                break;
			
			
			/**
			
			减少项
			*/
			
			 case 'platform_charges': //销售抽成&附加费
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = 0;
                $data_log['lg_desc'] = 'Platform Charges';
				$data_log['lg_name_of_activities'] = 'Platform Charges';
				$data_log['lg_type_activities'] = 1;	
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
				
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] =0;

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			case 'order_refund': //退款(包含全部类型的)
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = 0;
                $data_log['lg_desc'] = 'Order Refund';
				$data_log['lg_name_of_activities'] = 'Order Refund';
				$data_log['lg_type_activities'] = 1;					
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];				
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] = 0;
                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] =0;
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			
			case 'liquidated_damage': //违约金
			    $data_log['lg_av_amount'] = -$data['amount'];
			    $data_log['lg_freeze_amount'] = 0;
			    $data_log['lg_desc'] = 'Liquidated Damage';
				
				$data_log['lg_name_of_activities'] = 'Liquidated Damage';
				$data_log['lg_type_activities'] = 1;	
				
				
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
				
			    $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
			    $data_pd['freeze_predeposit'] = 0;
			
			    $data_msg['av_amount'] = -$data['amount'];
			    $data_msg['freeze_amount'] =0;
			    $data_msg['desc'] = $data_log['lg_desc'];
			break;
			
			
			
			
			
			case 'withdraw': //取现金额
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = $data['amount'];
                $data_log['lg_desc'] = 'Withdraw';
				
				$data_log['lg_name_of_activities'] = 'Withdraw';
				$data_log['lg_type_activities'] = 1;	
				
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
             //   $data_pd['freeze_predeposit'] = array('exp','freeze_predeposit+'.$data['amount']);

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			
			case 'withdraw_fee': //取现手续费
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = 0;
                $data_log['lg_desc'] = 'Withdraw Fee';
				
				$data_log['lg_name_of_activities'] = 'Withdraw Fee';
				$data_log['lg_type_activities'] = 1;	
				
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
				
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] = 0;

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			
			case 'other_platform_services_fee': //其他平台服务费
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = 0;			
                $data_log['lg_desc'] = 'Other Platform Services Fee';
				$data_log['lg_name_of_activities'] = 'Other Platform Services Fee';
				$data_log['lg_type_activities'] = 1;				
				
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] = 0;

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] =0;
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			
			case 'container_delivery_fee': //餐盒配送费
                $data_log['lg_av_amount'] = -$data['amount'];
                $data_log['lg_freeze_amount'] = 0;
                $data_log['lg_desc'] = 'Container Delivery Fee';
				
				$data_log['lg_name_of_activities'] = 'Container Delivery Fee';
				$data_log['lg_type_activities'] = 1;
				
				
				$data_log['lg_order_sn'] =  $data['order_sn'];
				$data_log['lg_order_id'] =  $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
                $data_pd['freeze_predeposit'] = 0;

                $data_msg['av_amount'] = -$data['amount'];
                $data_msg['freeze_amount'] = $data['amount'];
                $data_msg['desc'] = $data_log['lg_desc'];
            break;
			
			
			
				
            default:
                throw new Exception('参数错误');
                break;
        }
    
    //    print_r($data);
    //    print_r($data_pd);

        $update = Model('store')->editStore($data_pd,array('store_id'=>$data['store_id']));

        if (!$update) {
            throw new Exception('操作失败');
        }
        
     //   print_r($data_log);
        $insert = $this->table('store_pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }
		/*
        // 支付成功发送买家消息
        $param = array();
        $param['code'] = 'predeposit_change';
        $param['member_id'] = $data['member_id'];
        $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
        $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
        $param['param'] = $data_msg;
        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
        'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
		
        QueueClient::push('sendMemberMsg', $param);
		*/
		
        return $insert;
    }

    

}
