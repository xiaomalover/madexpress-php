<?php
/**
 * 店铺消息模板模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class member_msgModel extends Model{
    public function __construct() {
        parent::__construct('member_msg');
    }
    /**
     * 新增店铺消息
     * @param unknown $insert
     */
    public function addMemberMsg($insert) {
        $time = time();
        $insert['msg_addtime'] = $time;
        $sm_id = $this->insert($insert);     
        return $sm_id;
    }
	
	public function addMemberMsgAll($data) {
	     return $this->insertAll($data);     
	    
	}


    /**
     * 更新店铺消息表
     * @param unknown $condition
     * @param unknown $update
     */
    public function editMemberMsg($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 查看店铺消息详细
     * @param unknown $condition
     * @param string $field
     */
    public function getMemberMsgInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();

    }

    /**
     * 店铺消息列表
     * @param unknown $condition
     * @param string $field
     * @param string $page
     * @param string $order
     */
    public function getMemberMsgList($condition, $field = '*', $page = '0', $order = 'msg_id desc') {
        return $this->field($field)->where($condition)->order($order)->page($page)->select();
    }

    /**
     * 计算消息数量
     * @param unknown $condition
     */
    public function getMemberMsgCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 删除店铺消息
     * @param unknown $condition
     */
    public function delMemberMsg($condition) {
        $this->where($condition)->delete();
    }
}
