<?php
/**
 * 申请餐盒模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class foodbox_order_goodsModel extends Model {
    public function __construct() {
        parent::__construct('foodbox_order_goods');
    }

    /**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addOrderGoods($insert) {
        return $this->insert($insert);
    }
	
	 public function addOrderGoodsAll($insert) {
        return $this->insertAll($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editOrderGoods($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delOrderGoods($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getOrderGoodsCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getOrderGoodsList($condition, $field = '*', $page = 0, $order = 'id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

   
    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getOrderGoodsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
