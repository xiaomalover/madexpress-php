<?php
/**
 * 生成PDF
 *
 *
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');


class pdfModel extends Model {
   
   
   //销售票据
   public function createPdf($bill_data = '',$page=''){
		
		
		
				
		require_once(BASE_RESOURCE_PATH.'/tcppdf/tcpdf_include.php');
		require_once(BASE_RESOURCE_PATH.'/tcppdf/pdf_header.php');	
			
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);	
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
			
		require_once(BASE_RESOURCE_PATH.'/tcppdf/lang/eng.php');
		
		$pdf->setLanguageArray($l);
		
	// ---------------------------------------------------------

		// set font
		$pdf->SetPrintHeader(false);		
		$pdf->SetPrintFooter(true);
		
		
	//	$pdf->SetFont('stsongstdlight', '', 10);
		
		// add a page
		$pdf->AddPage();

	/* NOTE:
	 * *********************************************************
	 * You can load external XHTML using :
	 *
	 * $html = file_get_contents('/path/to/your/file.html');
	 *
	 * External CSS files will be automatically loaded.
	 * Sometimes you need to fix the path of the external CSS.
	 * *********************************************************
	 */
	
	$data = array();
	//获取生成票据的常用数据	
	if($bill_data['order_id'] > 0){		
		$order_info = model('order')->getOrderInfoN(array('order_id' => $bill_data['order_id']));		
		$data['order_sn'] = $order_info['order_sn']; 
	}
	
	if($bill_data['store_id'] > 0){
			//获取商家信息
			$store_company = model('company')->getCompanyInfo(array('store_id' => $bill_data['store_id']));
			$store = array();
			$store['store_name'] = $store_company['store_name'] ; // 'Yi Ming Chuan Restaurant'; //餐厅名称
			$store['store_company_name'] = $store_company['company_name'] ; // 'Yi Ming Chuan Pty Ltd'; //餐厅公司主体名称
			$store['store_address'] = $store_company['company_address'] ; //'5 Mad Street, Box Hill, VIC, AUS'; //餐厅地址
			$store['store_abn'] = $store_company['company_abn'] ; //'9999 99999';		
			$store['store_code'] =  $store_company['store_code'] ; //'AUS01 6666 01';		
			$data['store'] = $store; //店铺信息	
	}
	if($bill_data['me'] == 1){
			//获取平台信息
			$platform = array();
			$platform['platform_company_name'] = 'Mad Express Pty Ltd';
			$platform['platform_address_line1'] = 'U7/153-155 Rooks Road,';
			$platform['platform_address_line2'] = 'Vermont South, VIC, AUS';
			$platform['platform_abn'] = '6666 66666';
			$data['platform'] = $platform;		
	}
	
	if($bill_data['delivery_id'] > 0){
			
			
			$delivery_info = model('waiter')->getDeliveryCommInfo(array('delivery_id' => $bill_data['delivery_id']));
						
						
			//配送员信息
			$delivery = array();		
			$delivery['delivery_code'] = $delivery_info['delivery_code'];//'R6666 6666';
			$delivery['delivery_truename'] = $delivery_info['delivery_truename'];// 'San Zhang';
			$delivery['delivery_address'] =  $delivery_info['delivery_address'];//'5 Express Drive, Box Hill, VIC, AUS';
			$delivery['delivery_abn'] = $delivery_info['delivery_abn'];// '8888 88888';
			$data['delivery'] = $delivery;
				
	}
	if($bill_data['user_id'] > 0){		
			//获取用户信息

			$member = model('member')->getMemberInfo(['member_id' => $bill_data['user_id']]);

			$user = array();
			$user['user_code'] =  '78911222';
			$data['user'] = $user; //用户信息	
		
	}
	
	
	switch ($page) {
			
		
		/*
		商家发出
		Online Sales Invoice(2)
		给顾客的销售票据    online_sales_invoice_customer
		给配送员的销售票据  online_sales_invoice_courier
		
		*/
		//收票方 顾客
		case 'online_sales_invoice_customer':
		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'online_sales_invoice_customer.php';
			$role = 'S';
			$data['role'] = '(S)';
			$dir = 'online_store_invoice';
			
			//获取订单信息
			$order = array();
		
			$order['order_sn'] = $order_info['order_sn']; 					
			$order['merchant_voucher'] = array( //商家优惠券
				'voucher_code' => $order_info['store_coupon_code'],
				'voucher_amount' => $order_info['store_coupon_price']
			);	
					
			$order['platform_voucher'] = array(  //平台优惠券
				'voucher_code' => $order_info['platform_coupon_code'],
				'voucher_amount' => $order_info['platform_coupon_price']
			);
			
			$order['payment_txn_fee'] = $order_info['pay_commission_amount']; //手续费
			
		
			$order['payment_ext_fee'] = $order_info['pay_amount'];
		
		

			$order['total_amount'] = $order_info['goods_amount'] + $order_info['pay_commission_amount']; //总金额


			//判断是否有优惠券


			$voucher_redeem_fee =  $order_info['coupon_price'] - $order_info['refund_coupon_price'] ;
			$payment_wallet_fee = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			$pay_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];
		

			$sy_amount = $order['total_amount']; //本单的费用

			$new_voucher_redeem_fee = 0;
			$new_payment_wallet_fee = 0;
			$new_pay_amount = 0;
			//优先抵扣优惠券
			if($voucher_redeem_fee > 0){

				if($voucher_redeem_fee > 0 && $voucher_redeem_fee >= $sy_amount){

					$new_voucher_redeem_fee = $sy_amount;

				}elseif($voucher_redeem_fee < $sy_amount && $voucher_redeem_fee > 0 ){

					$new_voucher_redeem_fee = $voucher_redeem_fee;
				
					$sy_amount = $voucher_redeem_fee - $sy_amount; //优惠券全部抵扣了

					if($payment_wallet_fee >= $sy_amount){
					
						$new_payment_wallet_fee = $payment_wallet_fee;

					}elseif($payment_wallet_fee < $sy_amount &&  $payment_wallet_fee > 0 ){
						
						$new_payment_wallet_fee = $payment_wallet_fee;	

						$sy_amount = $sy_amount - $payment_wallet_fee;
					
						$new_pay_amount =  $sy_amount;

					}else{

						$new_pay_amount = $sy_amount;

					}

				}

			}elseif($payment_wallet_fee > 0 ){

				if($payment_wallet_fee >= $sy_amount){
					
					$new_payment_wallet_fee = $payment_wallet_fee;

				}elseif($payment_wallet_fee < $sy_amount &&  $payment_wallet_fee > 0 ){
					
					$new_payment_wallet_fee = $payment_wallet_fee;	

					$sy_amount = $sy_amount - $payment_wallet_fee;
				
					$new_pay_amount =  $sy_amount;

				}else{

					$new_pay_amount = $sy_amount;

				}
				

			}elseif($pay_amount > 0 ){ //优惠券支付的。

				if($pay_amount >= $sy_amount ){
					
					$new_pay_amount = $sy_amount;

				}

			}

	
		
			$order['voucher_redeem_fee'] = $new_voucher_redeem_fee > 0 ? $new_voucher_redeem_fee : '0.00'; //优惠券抵扣				
			$order['payment_wallet_fee'] = $new_payment_wallet_fee > 0 ? $new_payment_wallet_fee : '0.00'; //钱包抵扣
			$order['payment_method_fee'] = $new_pay_amount > 0 ? $new_pay_amount : '0.00' ;
				
			$order['subtotal'] = $order_info['goods_amount']; //商品总价
			$order['eng_date'] = date('d M Y',time());						
	
	
			//获取商品信息
			$goods = model('order')->getOrderGoodsList(array('order_id' => $order_info['order_id'],'state' => 0,'goods_usable_num' => array('gt',0)));	
		
		
			$goods_list = array();
			foreach($goods as $k => $v){				
			
				$specs = [];
				$goods_list[$k]['goods_num']  	=  	$v['goods_num'];
				$goods_list[$k]['goods_name'] 	=  $this->lang_goods($v['goods_code']);//	$v['goods_name'];
				
				if($v['set_meal'] == 0){
					$specs[] = $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'] ;
					$specs[] = $v['goods_spec'] ;
				}

				if($v['set_meal'] == 1){
					$meal_spec = model('store_goods_meal')->getMealList(array('meal_id' => array('in',$v['set_meal_spec']),'lang' => 'ENG'));
					foreach($meal_spec as $meal){
						$specs[] = $meal['meal_value'];
					}
				}

				$goods_list[$k]['goods_specs'] 	= empty($specs) ? '' :  implode('/',$specs) ;				
				$gst = round($v['goods_price'] * (10/110),2);	
				$goods_list[$k]['unit_price']  	= 	$v['goods_price'] - $gst;
				$goods_list[$k]['unit_gst']		=	$gst;
				$goods_list[$k]['goods_price'] 	= 	$v['goods_price'];
			}			

			
			$order['goods_list'] = $goods_list;		
			$data['order'] = $order;			
			
			//写入票据记录表			
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			
			$bill = array();
			$bill['form_type'] 	= 1 ;
			$bill['from_id'] 	= $order_info['store_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'online_sales_invoice' ;
			$bill['bill_type_name'] = 'Online Sales Invoice' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $order['total_amount'] ;
			
			
		break;
		
		//收票方 配送员
		case 'online_sales_invoice_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'online_sales_invoice_courier.php';
			$role = 'S';
			$data['role'] = '(S)';
			$dir = 'online_store_invoice';
			


			
			
		break;
		
		
		/*
		销售退款单
		Sales Credit Note(2)		
		全部退款 sales_credit_note_full
		部分退款 sales_credit_note_partial
		*/
	   
	   //收票放 顾客 全部退款
		case 'sales_credit_note_full': //销售退款票据		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_credit_note_full.php';
			$role = 'CN';
			$data['role'] = '(S)';
			$data['cn_role'] = 'CN';
			$dir = 'sales_credit_note_full';	
		
			$refund_info = model('order_refund')->getRefundInfo(array('refund_id' => $bill_data['refund_id']));			
		
			$order = array();
			$order['order_sn'] 			= $order_info['order_sn']; 			
			$order['total_amount'] 		= $refund_info['refund_amount']; //总金额
			$order['subtotal'] 			= $refund_info['refund_amount'] - $order_info['pay_commission_amount']; //商品总价
		
			$order['pay_commission_amount'] = $order_info['pay_commission_amount']; //手续费
					
		
			$order['refund_pay_amount'] = $refund_info['refund_pay_amount'];
			$order['refund_wallet_amount'] = $refund_info['refund_wallet_amount'];
			$order['refund_coupon_amount'] = $refund_info['refund_coupon_amount'];
			
			$order['eng_date'] = date('d M Y',time());						
		

			//获取退款商品信息			
			$goods = model('order_goods_refund')->getRefundList(array('refund_id' => $refund_info['refund_id']));			

			$goods_list = array();
			foreach($goods as $k => $v){		
				
				$specs = [];
				$goods_list[$k]['goods_num']  	=  	$v['goods_num'];
				$goods_list[$k]['goods_name'] 	=  $this->lang_goods($v['goods_code']);//	$v['goods_name'];
				
				if($v['set_meal'] == 0){
					$specs[] = $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'] ;
					$specs[] = $v['goods_spec'] ;
				}

				if($v['set_meal'] == 1){
					$meal_spec = model('store_goods_meal')->getMealList(array('meal_id' => array('in',$v['set_meal_spec']),'lang' => 'ENG'));
					foreach($meal_spec as $meal){
						$specs[] = $meal['meal_value'];
					}
				}

				$goods_list[$k]['goods_specs'] 	= empty($specs) ? '' :  implode('/',$specs) ;	
												
				$gst = round($v['goods_price'] * (10/110),2);				
				
				$goods_list[$k]['unit_price']  	= 	$v['goods_price'] - $gst;
				$goods_list[$k]['unit_gst']		=	$gst;
				$goods_list[$k]['goods_price'] 	= 	$v['goods_price'];
			}			
			$order['goods_list'] = $goods_list;		
			$data['order'] = $order;			
			
			
			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			
			//查询是否有已退款的票据
			$count = model('bill')->getBillCount(['bill_type' => 'sales_credit_note','order_id' => $order_info['order_id']]);


			$bill = array();
			$bill['form_type'] 	= 1 ;
			$bill['from_id'] 	= $order_info['store_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'sales_credit_note' ;
			$bill['bill_type_name'] = 'Sales Credit Note 0'. ($count + 1) ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $refund_info['refund_amount'] ;
			
				
		break;
		
		//收票放 顾客 部分
		case 'sales_credit_note_partial': //销售退款票据		
			
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_credit_note_partial.php';
			$role = 'CN';
			$data['role'] = '(S)';
			$data['cn_role'] = 'CN';
			$dir = 'sales_credit_note_partial';	
			
			$refund = model('order_refund')->getRefundInfo(array('refund_id' => $bill_data['refund_id']));
			$refund_amount = $refund['refund_amount'];
			$refund_payment_amount = $refund['refund_pay_amount'] ;
			$refund_wallet_amount = $refund['refund_wallet_amount'];
			$refund_coupon_amount = $refund['refund_coupon_amount'];

			/*$refund_amount = 0;
			$refund_ids = [];
			foreach($refund as $vv){
				$refund_ids[] = $vv['refund_id'];
				$refund_amount += $vv['refund_amount'];
				$refund_payment_amount += $vv['refund_pay_amount'] ;
				$refund_wallet_amount += $vv['refund_wallet_amount'];
				$refund_coupon_amount += $vv['refund_coupon_amount'];

			}*/

			$order = array();
			$order['order_sn'] = $order_info['order_sn']; 			
			$order['refund_total_amount'] = ncPriceFormat($refund_amount); //总金额	
			$order['refund_pay_amount'] = $refund_payment_amount > 0 ? ncPriceFormat($refund_payment_amount) : '0.00'; 
			$order['refund_wallet_amount'] = $refund_wallet_amount > 0 ? ncPriceFormat($refund_wallet_amount) : '0.00';
			$order['eng_date'] = date('d M Y',time());						
						
			//获取退款商品信息
			$goods = model('order_goods_refund')->getRefundList(array('refund_id' => $bill_data['refund_id']));			
		
			$goods_list = array();
			foreach($goods as $k => $v){				


				$goods_list[$k]['goods_num']  	=  	$v['goods_num'];
				$goods_list[$k]['goods_name'] 	=  	 $this->lang_goods($v['goods_code']);;
				$goods_list[$k]['goods_specs'] 	=	$v['goods_size'];
				
				$gst = round($v['goods_price'] * (10/110),2);			
				
				$goods_list[$k]['unit_price']  	= 	$v['goods_price'] - $gst;
				$goods_list[$k]['unit_gst']		=	$gst;
				$goods_list[$k]['goods_price'] 	= 	$v['goods_price'];
			}	

			$order['refund_goods_list'] = $goods_list;		
			$data['order'] = $order;

		
		
			
			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			

			//查询是否有已退款的票据
			$count = model('bill')->getBillCount(['bill_type' => 'sales_credit_note','order_id' => $order_info['order_id']]);

			$bill = array();
			$bill['form_type'] 	= 1 ;
			$bill['from_id'] 	= $order_info['store_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'sales_credit_note' ;
			$bill['bill_type_name'] = 'Sales Credit Note 0'. ($count + 1) ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $refund_amount ;
			
			//print_r($bill);

		break;
			
		
		/*
		销售费补贴发票
		Sales Reimbursement Invoice (1)
		SN:MESRI1108190001 生成的票据单号
		*/	
		//收票放 平台 ME Platform
		case 'sales_reimbursement_invoice': 	
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_reimbursement_invoice.php';
			$role = '';
			$dir = 'sales_reimbursement_invoice';
			

			$refund = model('order_refund')->getRefundInfo(array('refund_id' => $bill_data['refund_id']));
		

			$order = array();
			$order['eng_date'] = date('d M Y',time());			
			$order['order_sn'] = $order_info['order_sn']; 			
			$order['total_amount'] = $refund['refund_amount'];
			$data['order'] = $order;
			
			$data['sales_sn'] = 'MESRI'. date('dmy',time()).'0001';
			$sales_list = array();


			$gst = round($refund['refund_amount'] * (10/110),2);	
		
	
			$sales_list[0] = array(
				'item_name' => 'Sales Reimbursement',
				'item_credit_note_sn' => 'CN-'.$order_info['order_sn'].'-01',		
				'item_amount' =>  $refund['refund_amount'],
				'item_unit_price' => $refund['refund_amount'] - $gst,
				'item_unit_gst' =>  $gst,
				'item_qty' => 1
			);		
			$data['sales_list'] = $sales_list;
			



			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
			
			$bill = array();
			$bill['form_type'] 	= 1 ;
			$bill['from_id'] 	= $order_info['store_id'] ;
			$bill['to_id'] 		= 0 ;
			$bill['to_type'] 	= 0 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'sales_reimbursement_invoice' ;
			$bill['bill_type_name'] = 'Sales Reimbursement Invoice' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $refund['refund_amount'] ;
			
					
		break;
		
		/*
		优惠券使用补贴发票[弃用]
		Voucher Reimbursement Invoice (1)		
		SN:MEVR11081900000001 生成的票据单号
		
		*/
		//收票放 平台 ME Platform
		case 'voucher_reimbursement_invoice': 	
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'voucher_reimbursement_invoice.php';
			$role = 'CN';
			$dir = 'voucher_reimbursement_invoice';		
		break;



	
		/*
		配送员发出的票据		
		
		订单配送发票
		Delivery Invoice (1)	
		SN: AUS010001250221019336(D)  Invoice Number 票据单号		
		收票方：  顾客
		*/
	   
		case 'delivery_invoice': //配送票据			
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_invoice.php';				
			$role = 'D';			
			$data['role'] = '(D)';
			$dir = 'delivery_invoice';
			
			$order= array();
			$order['order_sn'] = $order_info['order_sn'];
			$order['eng_date'] = date('d M Y',time());		
			$data['order']  = $order;
			
			$gst = round($order_info['delivery_fee'] * (10/110),2);	
			//配送费发票
			$delivery_list = array();
			$delivery_list[0] = array(
				'item_name' => 'Deliery Fee',
				'item_order_sn' => 'Order #'.$order_info['order_sn'],		
				'item_amount' => $order_info['delivery_fee'],
				'item_unit_price' => $order_info['delivery_fee'] - $gst,
				'item_unit_gst' => $gst,
				'item_qty' => 1
			);		
			
			$data['delivery_list'] = $delivery_list;
			$data['delivery_amount'] = $order_info['delivery_fee'];
			




			
			$voucher_redeem_fee =  $order_info['coupon_price'] - $order_info['refund_coupon_price'] ;
			$payment_wallet_fee = $order_info['pd_amount'] - $order_info['refund_pd_amount'];
			$pay_amount = $order_info['pay_amount'] - $order_info['refund_pay_amount'];

			


			if($pay_amount > $order_info['delivery_fee'] +  $order_info['goods_amount']){ //此时的外部支付大于 运费。则运费给他。

				$delivery_pay_type = 'payment';
				
			}elseif($payment_wallet_fee > $order_info['delivery_fee']){
				
				$delivery_pay_type = 'wallet';
			
			}elseif($voucher_redeem_fee > $order_info['delivery_fee']){

				$delivery_pay_type = 'voucher';
				
			}

			$data['delivery_pay_type'] = $delivery_pay_type;




			
			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 2 ;
			$bill['from_id'] 	= $order_info['distributor_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'delivery_invoice' ;
			$bill['bill_type_name'] = 'Delivery Invoice' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $data['delivery_amount'] ;
			
			
		break;	
		
		/*
		订单配送退款单
		Delivery Credit Note(1)
		SN:CN-AUS010001250221019336-02 退配送费票据单号
		of SN：AUS010001250221019336(D) 配送发票
		收票方：Customer 顾客
		*/				
		case 'delivery_credit_note': //销售退款票据		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_credit_note.php';
			$role = 'CN'; //前缀\
			$data['role'] = '(D)';
			$data['cn_role'] = 'CN';
			$dir = 'delivery_credit_note';		
			

			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $bill_data['refund_id']]);


			
			$order= array();
			$order['order_sn'] = $order_info['order_sn'];
			$order['eng_date'] = date('d M Y',time());		

			$order['refund_pay_amount'] = $refund_info['refund_pay_amount'];
			$order['refund_wallet_amount'] = $refund_info['refund_wallet_amount'];

			$data['order']  = $order;
			
			$delivery_list = array();			
		
			$gst = round($refund_info['refund_amount'] * (10/110),2);	
			$delivery_list[0] = array(
				'item_name' => 'Deliery Fee',
				'item_order_sn' => 'Order #'.$order_info['order_sn'],		
				'item_amount' => $refund_info['refund_amount'],
				'item_unit_price' => $refund_info['refund_amount'] - $gst,
				'item_unit_gst' => $gst,
				'item_qty' => 1
			);		
			$data['delivery_list'] = $delivery_list;
			$data['delivery_amount'] = $refund_info['refund_amount'];



			
			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 2 ;
			$bill['from_id'] 	= $order_info['distributor_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'delivery_credit_note' ;
			$bill['bill_type_name'] = 'Delivery Credit Note' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $refund_info['refund_amount'] ;
			
		break;
		
		
		/*
		配送费补贴发票
		Delivery Fee Reimbursement Invoice (1)
		SN:MEDRI11081900001 票号
		收票方：ME Platform 平台
		*/		
		case 'delivery_fee_reimbursement_invoice':		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_fee_reimbursement_invoice.php';
			$role = '';
			$dir = 'delivery_fee_reimbursement_invoice';
			
			$data['medr_sn'] = 'MEDR'. date('dmy',time()).'0001';
			$medr_list = array();


			$refund_info = model('order_refund')->getRefundInfo(['refund_id' => $bill_data['refund_id']]);



			$gst = round($refund_info['refund_amount'] * (10/110),2);	


			$medr_list[0] = array(
				'item_name' => 'Delivery Fee Reimbursement',
				'item_credit_note_sn' => 'CN-'.$order_info['order_sn'].'-02',		
				'item_amount' => $refund_info['refund_amount'],
				'item_unit_price' => $refund_info['refund_amount'] - $gst,
				'item_unit_gst' => $gst,
				'item_qty' => 1
			);		
			$data['medr_list'] = $medr_list;
			$data['medr_amount'] = $refund_info['refund_amount'];			
			$order['eng_date'] = date('d M Y',time());		
			$data['order']  = $order;
			
					//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 2 ;
			$bill['from_id'] 	= $order_info['distributor_id'] ;
			$bill['to_id'] 		= 0;
			$bill['to_type'] 	= 0 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'delivery_fee_reimbursement_invoice' ;
			$bill['bill_type_name'] = 'Delivery Fee Reimbursement Invoice' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $refund_info['refund_amount'] ;
			
			
			
			
			
		break;
		
		/*
		激励金发票
		Incentive Invoice(1)
		SN:MECIR66666666120819 票号 MECI + 配送员编号 + 日期		
		收票方：ME Platform 平台
		*/		
		case 'incentive_invoice':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'incentive_invoice.php';
			$role = '';
			$dir = 'incentive_invoice';
		break;
		
		/*
		激励金退款单
		Incentive Credit Note
		SN:CN-MECIR66666666120819-01 票号 CN + MECI + 配送员编号 + 日期	
		of SN:MECIR66666666120819 票号 MECI + 配送员编号 + 日期	
		收票方 ：ME Platform 平台
		*/
		case 'incentive_credit_note':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'incentive_credit_note.php';
			$role = '';
			$data['role'] = 'CN';
			$dir = 'incentive_credit_note';
		break;
		
		
		
		/*
		平台发出的票据
		
		平台服务集成发票  1天1生成
		ME Platform Invoice（2）
	
		ME_Platform_Invoice_Merchant 
		SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Merchant
		*/
		case 'me_platform_invoice_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_invoice_merchant.php';
			$role = '';

			  
			$data['mepimel_sn'] = 'MEPIMEL'. date('dmy',time()).'0001';
			$data['order_sn'] = 'MEPIMEL'. date('dmy',time()).'0001';
			$order['eng_date'] = date('d M Y',time());		
			$data['order']  = $order;

			$mepimel_list = array();

			$stime = date('Y-m-d',time()).' '.'00:00:00';
			$etime = date('Y-m-d',time()).' '.'23:59:59';

			$where = array();
			$where['lg_store_id'] = $bill_data['store_id'];
			$where['lg_type'] = array('in','platform charges');
			$where['lg_add_time'] = array('between',array(strtotime($stime),strtotime($etime)));
			$list = model('store_wallet')->getPdLogList($where);
			$amount = 0;
			foreach($list as $k => $v){
				
				$gst = round(abs($v['lg_av_amount']) * (10/110),2);	

				$mepimel_list[$k] = array(
						'item_name' => $v['lg_name_of_activities'],
						'item_desc' => $v['lg_order_sn'],		
						'item_amount' => abs($v['lg_av_amount']),
						'item_unit_price' => abs($v['lg_av_amount']) - $gst,
						'item_unit_gst' => $gst,
						'item_qty' => 1
				);		

				$amount += abs($v['lg_av_amount']);

			}
	
			$data['mepimel_list'] = $mepimel_list;
			$data['mepimel_amount'] = $amount;
			
			

			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 0 ;
			$bill['from_id'] 	= 0 ;
			$bill['to_id'] 		= $bill_data['store_id'];
			$bill['to_type'] 	= 1 ;			
			$bill['order_sn']   = $data['order_sn'] ;
			$bill['bill_type'] 	= 'me_platform_invoice' ;
			$bill['bill_type_name'] = 'ME Platform Invoice' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $amount ;
			
			


			$dir = 'me_platform_invoice_merchant';
									
			
			
		break;
				
		/*
		ME_Platform_Invoice_Courier
		SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Courier
		*/
		case 'me_platform_invoice_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_invoice_courier.php';
			$role = '';

			
			 
			$mepimel_courier_list = array();
			$mepimel_courier_list[0] = array(
				'item_name' => "Withdraw Fee",
				'item_desc' => 'of $100.00',		
				'item_amount' => '0.90',
				'item_unit_price' => '0.81',
				'item_unit_gst' => '0.09',
				'item_qty' => 1
			);		
			
			 $data['mepimel_courier_list'] = $mepimel_courier_list;
			 $data['mepimel_courier_amount'] = '0.90';
			 $data['mepimel_courier_peirod'] = date('d M y',time() - (24*60*60 *8)) .' - '. date('d M y',time() - (24*60*60));
			 
			 


			

			

			
			
			$dir = 'me_platform_invoice_courier';

		break;
		
		
		
		
		/*
		平台服务集成退款单
		ME Platform Credit Note (2)
		
		ME_Platform_Credit_Note_Merchant
		SN:CN-MEPIMEL1108190123-01 票号 CN + (MEPIMEL + 日期 + 随机号4位) + 01
		of SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Merchant 商户
		*/
		case 'me_platform_credit_note_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_credit_note_merchant.php';
			$role = '';
			$data['role'] ='CN';

			$data['mepimel_sn'] = 'MEPIMEL'. date('dmy',time()).'0001';
			$data['order_sn'] = 'MEPIMEL'. date('dmy',time()).'0001';
			$order['eng_date'] = date('d M Y',time());		
			$data['order']  = $order;

			
			//平台服务集成发票退款 商家
			$mepimel_credit_merchant_list = array();
			$mepimel_credit_merchant_list[0] = array(
				'item_name' => "platform charges",
				'item_desc' => $order_info['order_sn'] ,		
				'item_amount' =>  $order_info['commission_amount']
			);		
		

			$data['mepimel_credit_merchant_list'] = $mepimel_credit_merchant_list;
			$data['mepimel_credit_merchant_amount'] = $order_info['commission_amount'];

			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 0 ;
			$bill['from_id'] 	= 0 ;
			$bill['to_id'] 		= $order_info['store_id'] ;
			$bill['to_type'] 	= 1 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'me_platform_credit_note' ;
			$bill['bill_type_name'] = 'ME Platform Credit Note' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $order_info['commission_amount'] ;

			$dir = 'me_platform_credit_note_merchant';



		break;
		
		/*
		ME_Platform_Credit_Note_Courier
		SN:CN-MEPIMEL1108190123-01 票号 CN + (MEPIMEL + 日期 + 随机号4位) + 01
		of SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Courier 配送员
		*/
		case 'me_platform_credit_note_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_credit_note_courier.php';
			$role = '';
			$data['role'] ='CN';







			$dir = 'me_platform_credit_note_courier';
		break;
		
		
		/*
		退款记录单（订单完成前）
		Refund Record		
		收票方：Customer 顾客
		*/		
		case 'refund_record':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'refund_record.php';
			$role = '';
			$dir = 'refund_record';
			
			
			$data['refund_record_sn'] = 'MERR'.date('dmy',time()).rand(100000,999999);	
			$data['refund_amount'] = $bill_data['refund_amount'];
			$data['refund_payment_amount'] = $bill_data['refund_payment_amount'];
			$data['refund_wallet_amount'] = $bill_data['refund_wallet_amount'];
			$data['voucher_re'] = $bill_data['voucher_re'];
			
						
			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 0 ;
			$bill['from_id'] 	= $order_info['store_id'] ;
			$bill['to_id'] 		= $order_info['buyer_id'] ;
			$bill['to_type'] 	= 3 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'refund_record' ;
			$bill['bill_type_name'] = 'Refund Record' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	= $bill_data['refund_amount'] ;
			
			
			
		break;
		
		
		/*
		违约金收据
		Liquidated Damage Receipt
		
		
		Liquidated Damage Receipt Merchant
		SN:LDR-AUS010001250221019336-01 票号 LDR + 单号 + 01
		收票方：Merchant 商户
		*/
		case 'liquidated_damage_receipt_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'liquidated_damage_receipt_merchant.php';
			$role = '';
			$data['role'] = 'LDR';

			$order['eng_date'] = date('d M Y',time());		
			$data['order'] = $order;

			$data['receipt_sn'] = $order_info['order_sn'];
			$receipt_merchant_list = array();	   

			$refund = model('order_refund')->getRefundInfo(array('refund_id' => $bill_data['refund_id']));
		

			$gst = round($refund['refund_amount'] * (10/110),2);	
		
			$receipt_merchant_list[0] = array(
				'item_name' => "Delivery Fee Reimbursement",
				'item_desc' => 'CN-'.$order_info['order_sn'].'-02',		
				'item_amount' => $refund['refund_amount'],
				'item_unit_price' => $refund['refund_amount'] - $gst,
				'item_unit_gst' => $gst,
				'item_qty' => 1
				
			);
	
	/*		$receipt_merchant_list[1] = array(
				'item_name' => "Voucher Re-Issued",
				'item_desc' => 'MEVR110819000001',		
				'item_amount' => '5.00',
				'item_unit_price' => '4.50',
				'item_unit_gst' => '0.50',
				'item_qty' => 1
			);		*/
				
			$data['receipt_merchant_list'] = $receipt_merchant_list;
			$data['receipt_merchant_amount'] = $refund['refund_amount'];


			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 0 ;
			$bill['from_id'] 	= 0 ;
			$bill['to_id'] 		= $order_info['store_id'] ;
			$bill['to_type'] 	= 1 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'liquidated_damage_receipt' ;
			$bill['bill_type_name'] = 'Liquidated Damage Receipt' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	=  $refund['refund_amount'] ;

			$dir = 'liquidated_damage_receipt_merchant';
		break;
		
		
		/*
		Liquidated Damage Receipt Courier
		SN:LDR-AUS010001250221019336-02 票号 LDR + 单号 + 02
		收票方：Courier 配送员
		*/
		case 'liquidated_damage_receipt_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'liquidated_damage_receipt_courier.php';
			$role = '';
			$data['role'] = 'LDR';
			$order['eng_date'] = date('d M Y',time());		
			$data['order'] = $order;
			$data['receipt_sn'] = $order_info['order_sn'];
			$receipt_courier_list = array();	

			$refund = model('order_refund')->getRefundInfo(array('refund_id' => $bill_data['refund_id']));
		

			$gst = round($refund['refund_amount'] * (10/110),2);	
		
			$receipt_courier_list[0] = array(
				'item_name' => "Sales Reimbursement",
				'item_desc' => 'CN-'.$order_info['order_sn'].'-01',		
				'item_amount' => $refund['refund_amount'],
				'item_unit_price' => $refund['refund_amount'] - $gst,
				'item_unit_gst' =>  $gst,
				'item_qty' => 1
			);

			/*$receipt_courier_list[1] = array(
				'item_name' => "Voucher Re-Issued",
				'item_desc' => 'AUS010001250221019336(S)',		
				'item_amount' => '8.00',
				'item_unit_price' => '7.20',
				'item_unit_gst' => '0.80',
				'item_qty' => 1
			);			
			*/
			$data['receipt_courier_list'] = $receipt_courier_list;
			$data['receipt_courier_amount'] = $refund['refund_amount'];	


			//写入票据记录表
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  			
			$bill = array();
			$bill['form_type'] 	= 0 ;
			$bill['from_id'] 	= 0 ;
			$bill['to_id'] 		= $order_info['distributor_id'] ;
			$bill['to_type'] 	= 2 ;
			$bill['order_id'] 	= $order_info['order_id'] ;
			$bill['order_sn'] 	= $order_info['order_sn'] ;
			$bill['bill_type'] 	= 'liquidated_damage_receipt' ;
			$bill['bill_type_name'] = 'Liquidated Damage Receipt' ;		
			$bill['bill_addtime'] = time() ;
			$bill['bill_price'] 	=  $refund['refund_amount'] ;
			
			
			
			$dir = 'liquidated_damage_receipt_courier';
		break;
		
		
		/*
		票据集成表
		Tax Summary (2)
		
		
		Tax Summary Merchant （3）年度，季度，月度
		收票方 商户
		*/
		case 'tax_summary_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'tax_summary_merchant.php';
			$role = '';
			$dir = 'tax_summary_merchant';
		break;   
				
				
		/*
		Tax Summary Courier （3）年度，季度，月度
		收票方 配送员
		*/
	   
	   case 'tax_summary_courier':
	   	$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'tax_summary_courier.php';
	   	$role = '';
	   	$dir = 'tax_summary_courier';
		
		
		
	   break;   
	   
		
		
		default:
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'default.php';
		break;
	 }
	 
	 
	if (file_exists($file)) {
        ob_start();
        include $file;
        $html = ob_get_contents();
        ob_end_clean();
    }		
	

	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// reset pointer to the last page
	$pdf->lastPage();
	// ---------------------------------------------------------
	ob_end_clean();
		
	$pdf_url = BASE_UPLOAD_PATH.'/'.ATTACH_PDF.'/'.$dir.'/'.$data['order_sn'].$role.'.pdf';
	
	//写入票据记录表
	$bill['bill_pdf'] 	= ATTACH_PDF.'/'.$dir.'/'.$data['order_sn'].$role.'.pdf' ;
	$bill['date_of_issue'] = time();
	$bill['bill_day'] = date('d',time());
	$bill['bill_month'] = date('m',time());
	$bill['bill_year'] = date('Y',time());
	model('bill')->addBill($bill);
	
	
	
	
	if(file_exists($pdf_url)) unlink($pdf_url); 		
	//Close and output PDF document
	$pdf->Output($pdf_url, 'F');
	
	return $pdf_url;
	
	exit;
	

	//============================================================+
	// END OF FILE
	//============================================================+
		
	 }
		   
		   
	private function lang_goods($goods_code){
		
		$where = array(
			'goods_code' => $goods_code,
			'lang_name' => 'ENG'
		);
		
		$lang_goods = model('goods_language')->getGoodsLangInfo($where);
		
		return $lang_goods['goods_name'];
		
		
	}
}
