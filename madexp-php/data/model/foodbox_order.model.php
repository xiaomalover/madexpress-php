<?php
/**
 * 申请餐盒模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class foodbox_orderModel extends Model {
    public function __construct() {
        parent::__construct('foodbox_order');
    }

	public function makeSn($store_id) {
       return 'E'.date('YmdHis');
    }

	
    /**
     * 添加Order
     * @param array $insert
     * @return boolean
     */
    public function addOrder($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Order
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editOrder($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Order
     * @param unknown $condition
     * @return boolean
     */
    public function delOrder($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     * 查询Order数量
     * @param array $condition
     * @return array
     */
    public function getOrderCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Order列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getOrderList($condition, $field = '*', $page = 0, $order = 'order_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

   
    /**
     * 取单个Order内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getOrderInfo($condition,$field = '*') {
		$order_info = $this->field($field)->where($condition)->find();
		return $order_info;
		
    }
}
