<?php
/**
 * 问题单类型
 */
defined('InMadExpress') or exit('Access Invalid!');
class problem_typeModel extends Model {
    
    public function __construct() {
        parent::__construct('order_problem_type');
    }

    /**
     * 添加Type
     * @param array $insert
     * @return boolean
     */
    public function addType($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Type
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editType($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Type
     * @param unknown $condition
     * @return boolean
     */
    public function delType($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Type数量
     * @param array $condition
     * @return array
     */
    public function getTypeCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Type列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
     
    public function getTypeList($condition, $field = '*', $order = 'type_id desc') {
        return $this->where($condition)->field($field)->order($order)->select();
    }



    /**
     * 取单个Type内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getTypeInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
    
}
