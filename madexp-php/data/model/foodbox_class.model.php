<?php
/**
 * 申请餐盒模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class foodbox_classModel extends Model {
    public function __construct() {
        parent::__construct('foodbox_class');
    }

    /**
     * 添加
     * @param array $insert
     * @return boolean
     */
    public function addClass($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editClass($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除
     * @param unknown $condition
     * @return boolean
     */
    public function delClass($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     * 查询数量
     * @param array $condition
     * @return array
     */
    public function getClassCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getClassList($condition, $field = '*', $page = 0, $order = 'class_sort asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

   
    /**
     * 取单个内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getClassInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
