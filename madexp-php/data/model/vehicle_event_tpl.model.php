<?php
/**
 * 车辆事件模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_event_tplModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_event_tpl');
    }

	
    public function vehicleEventTplList($condition, $field = '*', $page = 0, $order = 'tpl_id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

	
	
	public function vehicleEventTplInfo($where,$field='*'){		
		   return $this->field($field)->where($where)->find();
	}
	
	
	public function vehicleEventTplAdd($param)
    {
        return $this->insert($param);
    }

	
	 public function vehicleEventTplEdit($condition, $update) {
		 
        return $this->where($condition)->update($update);
		
    }
	
	 public function vehicleEventTplDel($condition) {        
        return $this->where($condition)->delete();
    }
	
	
}
