<?php
/**
 * 车辆事件模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_event_logModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_event_log');
    }

	
    public function vehicleEventLogList($condition, $field = '*', $page = 0, $order = 'log_id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

	
	
	public function vehicleEventLogInfo($where,$field='*'){		
		   return $this->field($field)->where($where)->find();
	}
	
	
	public function vehicleEventLogAdd($param)
    {
        return $this->insert($param);
    }

	
	 public function vehicleEventLogEdit($condition, $update) {
		 
        return $this->where($condition)->update($update);
		
    }
	
	
	
	
}
