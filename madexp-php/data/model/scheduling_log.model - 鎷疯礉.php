<?php
/**
 * 排班模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_logModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_log');
    }

    /**
     * 取店铺类别列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */
  
	 public function getSchedulingLogList($condition, $page=null, $order='log_id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }
    /**
     * 取得单条信息
     * @param unknown $condition
     */
    public function getSchedulingLogInfo($condition = array()) {
        return $this->where($condition)->find();
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delSchedulingLog($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addSchedulingLog($data) {
        return $this->insert($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editSchedulingLog($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
}
