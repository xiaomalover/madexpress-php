<?php
/**
 * 排班模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class polling_refuseModel extends Model {

    public function __construct(){
        parent::__construct('order_polling_refuse');
    }

    /**
     * 取店铺类别列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */
  
	 public function getRefuseList($condition, $page=null, $order='id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }
    /**
     * 取得单条信息
     * @param unknown $condition
     */
    public function getRefuseInfo($condition = array()) {
        return $this->where($condition)->find();
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delRefuse($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addRefuse($data) {
        return $this->insert($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editRefuse($condition = array(),$data = array()) {
        return $this->where($condition)->update($data);
    }
	
	
	 public function getRefuseCount($condition = array()) {
        return $this->where($condition)->count();
    }
	
}
