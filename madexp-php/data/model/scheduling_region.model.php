<?php
/**
 * 排班模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class scheduling_regionModel extends Model {

    public function __construct(){
        parent::__construct('scheduling_region');
    }

    public function getSchedulingList($condition=array(), $page=null, $order='id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }



	public function getSchedulingInfo($condition = array()){		
		return $this->where($condition)->find();		
	}

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delScheduling($condition = array()) {
        return $this->where($condition)->delete();
    }

    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addScheduling($data) {
        return $this->insert($data);
    }

    public function addSchedulingAll($data)
    {
        return $this->insertAll($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editScheduling($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
}
