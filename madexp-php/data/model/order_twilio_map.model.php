<?php
/**
 * 订单与twilio关系表管理
 */
defined('InMadExpress') or exit('Access Invalid!');
class order_twilio_mapModel extends Model {

	public function __construct() {
        parent::__construct('order_twilio_map');
    }


	/**
     * 根据条件获取数据
     * @param num $gc_id
     * @return array
     */
    public function getordertwiliomap($where) {
        return $this->where($where)->find();
    }


	/**
     * 新增映射
     *
     * @param array $insert
     * @param boolean $replace
     * @return boolean
     */
    public function add_order_twilio_map($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑映射
     *
     * @param unknown $update
     * @param unknown $gc_id
     * @return boolean
     */
    public function editorder_twilio_map($update, $id) {
        return $this->where(array('id' => $id))->update($update);
    }

}