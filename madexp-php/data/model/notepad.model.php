<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class notepadModel extends Model {
    public function __construct() {
        parent::__construct('notepad');
    }

    /**
     * 添加Notepad
     * @param array $insert
     * @return boolean
     */
    public function addNotepad($insert) {
        return $this->table('notepad')->insert($insert);
    }

    /**
     * 编辑Notepad
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editNotepad($condition, $update) {
        return $this->table('notepad')->where($condition)->update($update);
    }

    /**
     * 删除Notepad
     * @param unknown $condition
     * @return boolean
     */
    public function delNotepad($condition) {        
        return $this->table('notepad')->where($condition)->delete();
    }

    /**
     * 查询Notepad数量
     * @param array $condition
     * @return array
     */
    public function getNotepadCount($condition) {
        return $this->table('notepad')->where($condition)->count();
    }

    /**
     * Notepad列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getNotepadList($condition, $field = '*', $page = 0, $order = 'notepad_id desc', $limit = '') {
        return $this->table('notepad')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Notepad内容
     * @param array $condition
     * @param string $field
     * @return array
     */
	
	
    public function getNotepadInfo($condition, $field = '*') {
        return $this->table('notepad')->field($field)->where($condition)->find();
    }
	
	
	
	//
    
    
	
	 public function addNotepadClass($insert) {
        return $this->table('notepad_class')->insert($insert);
     }
	
	
	 public function getNotepadClassList($condition, $field = '*', $page = 0, $order = 'class_id desc', $limit = '') {
        return $this->table('notepad_class')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
     }
	
	
	
    /**
     * 删除Notepad
     * @param unknown $condition
     * @return boolean
     */
    public function delNotepadClass($condition) {        
        return $this->table('notepad_class')->where($condition)->delete();
    }

    
    /**
     * 编辑Notepad
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editNotepadClass($condition, $update) {
        return $this->table('notepad_class')->where($condition)->update($update);
    }

	
	
	
}
