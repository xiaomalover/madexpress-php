<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class activityModel extends Model {
    public function __construct() {
        parent::__construct('activity');
    }

    /**
     * 添加Activity
     * @param array $insert
     * @return boolean
     */
    public function addActivity($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Activity
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editActivity($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Activity
     * @param unknown $condition
     * @return boolean
     */
    public function delActivity($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Activity数量
     * @param array $condition
     * @return array
     */
    public function getActivityCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Activity列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getActivityList($condition, $field = '*', $page = 0, $order = 'activity_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Activity内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getActivityInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
