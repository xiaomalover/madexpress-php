<?php
/**
 * 生成PDF
 *
 *
 *
 *
 */
defined('InMadExpress') or exit('Access Invalid!');


class pdf2Model extends Model {
   
   
   //销售票据
   public function createPdf($data = '',$page=''){
		
		
		
				
		require_once(BASE_RESOURCE_PATH.'/tcppdf/tcpdf_include.php');
		require_once(BASE_RESOURCE_PATH.'/tcppdf/pdf_header.php');	
			
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);	
		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
			
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set some language-dependent strings (optional)
			
		require_once(BASE_RESOURCE_PATH.'/tcppdf/lang/eng.php');
		
		$pdf->setLanguageArray($l);
		
	// ---------------------------------------------------------

		// set font
		$pdf->SetPrintHeader(false);		
		$pdf->SetPrintFooter(true);
		
		
	//	$pdf->SetFont('stsongstdlight', '', 10);
		
		// add a page
		$pdf->AddPage();

	/* NOTE:
	 * *********************************************************
	 * You can load external XHTML using :
	 *
	 * $html = file_get_contents('/path/to/your/file.html');
	 *
	 * External CSS files will be automatically loaded.
	 * Sometimes you need to fix the path of the external CSS.
	 * *********************************************************
	 */
	/*
	$data = array();
	//获取生成票据的常用数据	
	if($bill_data['order_id'] > 0){		
		$order_info = model('order')->getOrderInfoN(array('order_id' => $bill_data['order_id']));		
		$data['order_sn'] = $order_info['order_sn']; 
	}
	
	if($bill_data['store_id'] > 0){
			//获取商家信息
			$store_company = model('company')->getCompanyInfo(array('store_id' => $bill_data['store_id']));
			$store = array();
			$store['store_name'] = $store_company['store_name'] ; // 'Yi Ming Chuan Restaurant'; //餐厅名称
			$store['store_company_name'] = $store_company['company_name'] ; // 'Yi Ming Chuan Pty Ltd'; //餐厅公司主体名称
			$store['store_address'] = $store_company['company_address'] ; //'5 Mad Street, Box Hill, VIC, AUS'; //餐厅地址
			$store['store_abn'] = $store_company['company_abn'] ; //'9999 99999';		
			$store['store_code'] =  $store_company['store_code'] ; //'AUS01 6666 01';		
			$data['store'] = $store; //店铺信息	
	}
	if($bill_data['me'] == 1){
			//获取平台信息
			$platform = array();
			$platform['platform_company_name'] = 'Mad Express Pty Ltd';
			$platform['platform_address_line1'] = 'U7/153-155 Rooks Road,';
			$platform['platform_address_line2'] = 'Vermont South, VIC, AUS';
			$platform['platform_abn'] = '6666 66666';
			$data['platform'] = $platform;		
	}
	
	if($bill_data['delivery_id'] > 0){
			
			
			$delivery = model('waiter')->getDeliveryCommInfo(array('delivery_id' => $bill_data['delivery_id']));	
						
						
			//配送员信息
			$delivery = array();		
			$delivery['delivery_code'] = $delivery['delivery_code'];//'R6666 6666';
			$delivery['delivery_truename'] = $delivery['delivery_truename'];// 'San Zhang';
			$delivery['delivery_address'] =  $delivery['delivery_address'];//'5 Express Drive, Box Hill, VIC, AUS';
			$delivery['delivery_abn'] = $delivery['delivery_abn'];// '8888 88888';
			$data['delivery'] = $delivery;
				
	}
	if($bill_data['user_id'] > 0){		
			//获取用户信息
			$user = array();
			$user['user_code'] =  '78911222';
			$data['user'] = $user; //用户信息	
		
	}
	
	*/
	switch ($page) {
			
		
		/*
		商家发出
		Online Sales Invoice(2)
		给顾客的销售票据    online_sales_invoice_customer
		给配送员的销售票据  online_sales_invoice_courier
		
		*/
		//收票方 顾客
		case 'online_sales_invoice_customer':
		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'online_sales_invoice_customer.php';
			$role = 'S';
			$data['role'] = '(S)';
			$dir = 'online_store_invoice';
			
		
			//获取订单信息
			$order = array();
		
				
			$data['order'] = $data['order'];			
			
			//写入票据记录表			
			//发送方和收票方类型  0平台 1商家 2配送员 3顾客 4专员 5汽修厂  
		
			
		break;
		
		//收票方 配送员
		case 'online_sales_invoice_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'online_sales_invoice_courier.php';
			$role = 'S';
			$data['role'] = '(S)';
			$dir = 'online_store_invoice';
			
			
			
		break;
		
		
		/*
		销售退款单
		Sales Credit Note(2)		
		全部退款 sales_credit_note_full
		部分退款 sales_credit_note_partial
		*/
	   
	   //收票放 顾客 全部退款
		case 'sales_credit_note_full': //销售退款票据		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_credit_note_full.php';
			$role = 'CN';
			$data['role'] = '(S)';
			$data['cn_role'] = 'CN';
			$dir = 'sales_credit_note_full';	
		
			//获取订单信息
			$order = array();
					
				
			$data['order'] = $data['order'];			
			
				
				
		break;
		
		//收票放 顾客 部分
		case 'sales_credit_note_partial': //销售退款票据		
			
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_credit_note_partial.php';
			$role = 'CN';
			$data['role'] = '(S)';
			$data['cn_role'] = 'CN';
			$dir = 'sales_credit_note_partial';	
			
			$data['order'] = $data['order'];
			
			
			
			
		break;
			
		
		/*
		销售费补贴发票
		Sales Reimbursement Invoice (1)
		SN:MESRI1108190001 生成的票据单号
		*/	
		//收票放 平台 ME Platform
		case 'sales_reimbursement_invoice': 	
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'sales_reimbursement_invoice.php';
			$role = '';
			$dir = 'sales_reimbursement_invoice';
			
			$data['order'] = $data['order'];
			
			
					
		break;
		
		/*
		优惠券使用补贴发票[弃用]
		Voucher Reimbursement Invoice (1)		
		SN:MEVR11081900000001 生成的票据单号
		
		*/
		//收票放 平台 ME Platform
		case 'voucher_reimbursement_invoice': 	
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'customer'.DS.'voucher_reimbursement_invoice.php';
			$role = 'CN';
			$dir = 'voucher_reimbursement_invoice';		
		break;



	
		/*
		配送员发出的票据		
		
		订单配送发票
		Delivery Invoice (1)	
		SN: AUS010001250221019336(D)  Invoice Number 票据单号		
		收票方：  顾客
		*/
	   
		case 'delivery_invoice': //配送票据			
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_invoice.php';				
			$role = 'D';			
			$data['role'] = '(D)';
			$dir = 'delivery_invoice';
			
			
			
			
		break;	
		
		/*
		订单配送退款单
		Delivery Credit Note(1)
		SN:CN-AUS010001250221019336-02 退配送费票据单号
		of SN：AUS010001250221019336(D) 配送发票
		收票方：Customer 顾客
		*/				
		case 'delivery_credit_note': //销售退款票据		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_credit_note.php';
			$role = 'CN'; //前缀\
			$data['role'] = '(D)';
			$data['cn_role'] = 'CN';
			$dir = 'delivery_credit_note';		
			
			
			
		break;
		
		
		/*
		配送费补贴发票
		Delivery Fee Reimbursement Invoice (1)
		SN:MEDRI11081900001 票号
		收票方：ME Platform 平台
		*/		
		case 'delivery_fee_reimbursement_invoice':		
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'delivery_fee_reimbursement_invoice.php';
			$role = '';
			$dir = 'delivery_fee_reimbursement_invoice';
			
			
			
			
			
			
			
			
		break;
		
		/*
		激励金发票
		Incentive Invoice(1)
		SN:MECIR66666666120819 票号 MECI + 配送员编号 + 日期		
		收票方：ME Platform 平台
		*/		
		case 'incentive_invoice':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'incentive_invoice.php';
			$role = '';
			$dir = 'incentive_invoice';
		break;
		
		/*
		激励金退款单
		Incentive Credit Note
		SN:CN-MECIR66666666120819-01 票号 CN + MECI + 配送员编号 + 日期	
		of SN:MECIR66666666120819 票号 MECI + 配送员编号 + 日期	
		收票方 ：ME Platform 平台
		*/
		case 'incentive_credit_note':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'courier'.DS.'incentive_credit_note.php';
			$role = '';
			$data['role'] = 'CN';
			$dir = 'incentive_credit_note';
		break;
		
		
		
		/*
		平台发出的票据
		
		平台服务集成发票
		ME Platform Invoice（2）
	
		ME_Platform_Invoice_Merchant 
		SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Merchant
		*/
		case 'me_platform_invoice_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_invoice_merchant.php';
			$role = '';
			$dir = 'me_platform_invoice_merchant';
			
			
			
			
			
		break;
				
		/*
		ME_Platform_Invoice_Courier
		SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Courier
		*/
		case 'me_platform_invoice_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_invoice_courier.php';
			$role = '';
			$dir = 'me_platform_invoice_courier';
		break;
		
		
		
		
		/*
		平台服务集成退款单
		ME Platform Credit Note (2)
		
		ME_Platform_Credit_Note_Merchant
		SN:CN-MEPIMEL1108190123-01 票号 CN + (MEPIMEL + 日期 + 随机号4位) + 01
		of SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Merchant 商户
		*/
		case 'me_platform_credit_note_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_credit_note_merchant.php';
			$role = '';
			$data['role'] ='CN';
			
			$dir = 'me_platform_credit_note_merchant';
		break;
		
		/*
		ME_Platform_Credit_Note_Courier
		SN:CN-MEPIMEL1108190123-01 票号 CN + (MEPIMEL + 日期 + 随机号4位) + 01
		of SN:MEPIMEL1108190123 票号 MEPIMEL + 日期 + 随机号4位
		收票方：Courier 配送员
		*/
		case 'me_platform_credit_note_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'me_platform_credit_note_courier.php';
			$role = '';
			$data['role'] ='CN';
			$dir = 'me_platform_credit_note_courier';
		break;
		
		
		/*
		退款记录单（订单完成前）
		Refund Record		
		收票方：Customer 顾客
		*/		
		case 'refund_record':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'refund_record.php';
			$role = '';
			$dir = 'refund_record';
			
			
			$data['refund_record_sn'] = 'MERR'.date('dmy',time()).rand(100000,999999);	
			$data['refund_amount'] = '66.00';
			$data['refund_payment_amount'] = '42.00';
			$data['refund_wallet_amount'] = '24.00';
			$data['voucher_re'] = 'No';
			
		
			
			
			
		break;
		
		
		/*
		违约金收据
		Liquidated Damage Receipt
		
		
		Liquidated Damage Receipt Merchant
		SN:LDR-AUS010001250221019336-01 票号 LDR + 单号 + 01
		收票方：Merchant 商户
		*/
		case 'liquidated_damage_receipt_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'liquidated_damage_receipt_merchant.php';
			$role = '';
			$data['role'] = 'LDR';
			$dir = 'liquidated_damage_receipt_merchant';
		break;
		
		
		/*
		Liquidated Damage Receipt Courier
		SN:LDR-AUS010001250221019336-02 票号 LDR + 单号 + 02
		收票方：Courier 配送员
		*/
		case 'liquidated_damage_receipt_courier':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'liquidated_damage_receipt_courier.php';
			$role = '';
			$data['role'] = 'LDR';
			$dir = 'liquidated_damage_receipt_courier';
		break;
		
		
		/*
		票据集成表
		Tax Summary (2)
		
		
		Tax Summary Merchant （3）年度，季度，月度
		收票方 商户
		*/
		case 'tax_summary_merchant':
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'tax_summary_merchant.php';
			$role = '';
			$dir = 'tax_summary_merchant';
		break;   
				
				
		/*
		Tax Summary Courier （3）年度，季度，月度
		收票方 配送员
		*/
	   
	   case 'tax_summary_courier':
	   	$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'me_platform'.DS.'tax_summary_courier.php';
	   	$role = '';
	   	$dir = 'tax_summary_courier';
		
		
		
	   break;   
	   
		
		
		default:
			$file = BASE_DATA_PATH.DS.'resource'.DS.'pdf_temp'.DS.'default.php';
		break;
	 }
	 
	 
	if (file_exists($file)) {
        ob_start();
        include $file;
        $html = ob_get_contents();
        ob_end_clean();
    }		
	

	// output the HTML content
	$pdf->writeHTML($html, true, false, true, false, '');

	// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	// reset pointer to the last page
	$pdf->lastPage();
	// ---------------------------------------------------------
	ob_end_clean();
		
	$pdf_url = BASE_UPLOAD_PATH.'/'.ATTACH_PDF.'/'.$dir.'/'.$data['order_sn'].$role.'.pdf';
	
	//写入票据记录表
	$bill['bill_pdf'] 	= ATTACH_PDF.'/'.$dir.'/'.$data['order_sn'].$role.'.pdf' ;
	model('bill')->addBill($bill);
	
	
	
	
	if(file_exists($pdf_url)) unlink($pdf_url); 		
	//Close and output PDF document
	$pdf->Output($pdf_url, 'I');
	
	return $pdf_url;
	
	exit;
	

	//============================================================+
	// END OF FILE
	//============================================================+
		
	 }
		   
		   

}
