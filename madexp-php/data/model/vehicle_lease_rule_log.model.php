<?php
/**
 * 租赁市场的规则完成度

 */
defined('InMadExpress') or exit('Access Invalid!');

class vehicle_lease_rule_logModel extends Model {
    public function __construct() {
        parent::__construct('vehicle_lease_rule_log');
    }

    /**
     * 添加LeaseRule
     * @param array $insert
     * @return boolean
     */
    public function addLeaseRule($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑LeaseRule
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editLeaseRule($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除LeaseRule
     * @param unknown $condition
     * @return boolean
     */
    public function delLeaseRule($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询LeaseRule数量
     * @param array $condition
     * @return array
     */
    public function getLeaseRuleCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * LeaseRule列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getLeaseRuleList($condition, $field = '*', $page = 0, $order = 'id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个LeaseRule内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getLeaseRuleInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
