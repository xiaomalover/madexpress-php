<?php
/**
 * 送餐员模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class VehicleRentalModel extends Model
{

    public function __construct()
    {
        parent::__construct('vehicle_rental');
    }

	 public function delRental($condition) {        
        return $this->where($condition)->delete();
    }


    public function rentalInfo($condition, $field = '*', $master = false)
    {
        return $this->field($field)->where($condition)->master($master)->find();
    }

    public function rentalList($condition=array(), $field = '*', $page = 0, $order = 'id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

    public function getVehicleCount($condition)
    {
        return $this->where($condition)->count();
    }

    public function vehicleAdd($param)
    {
        return $this->insert($param);
    }
	   public function rentalEdit($condition, $update) {
        return $this->where($condition)->update($update);
    }

}
