<?php
/**
 * 财务统计
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class finance_statModel extends Model {

    public function __construct() {
        parent::__construct('finance_stat');
    }

   
    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getStatInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getStatList($condition, $order = 'id desc'){
        $list = $this->where($condition)->order($order)->select();
       return $list;
    }

    /**
     * 取数量
     * @param unknown $condition
     */
    public function getStatCount($condition = array()) {
        return $this->where($condition)->count();
    }

   
    /**
     * 新增
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addStat($param){
        return $this->insert($param);
    }
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editStat($update, $condition){
        return $this->where($condition)->update($update);
    }
    
    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delStat($condition){
        return $this->where($condition)->delete();
    }
}
