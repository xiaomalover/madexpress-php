<?php
/**
 * 收货人模型
 *

 */
defined('InMadExpress') or exit('Access Invalid!');

class consigneeModel extends Model {
    public function __construct() {
        parent::__construct('consignee');
    }

    /**
     * 添加收货人
     * @param array $insert
     * @return boolean
     */
    public function addConsignee($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑收货人
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editConsignee($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除收货人
     * @param unknown $condition
     * @return boolean
     */
    public function delConsignee($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     * 查询收货人数量
     * @param array $condition
     * @return array
     */
    public function getConsigneeCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 收货人列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getConsigneeList($condition, $field = '*', $page = 0, $order = 'consignee_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

   

    /**
     * 取单个收货人内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getConsigneeInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
