<?php
/**
 * 反馈得问题类型

 */
defined('InMadExpress') or exit('Access Invalid!');

class order_problemModel extends Model {
    public function __construct() {
        parent::__construct('order_problem');
    }

    /**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addProblem($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editProblem($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delProblem($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getProblemCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getProblemList($condition, $field = '*', $page = 0, $order = 'problem_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getProblemInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
	
	
	
	
	
		//新增log
	public function  addProblemLog($param){		
		return $this->table('order_problem_log')->insert($param);
	}
	
	//log列表
	public function getProblemLogList($condition, $page=null, $order='id desc', $field='*') {       
		$list = $this->table('order_problem_log')->field($field)->where($condition)->page($page)->order($order)->select();
	    return $list;		
	}
	
	
	//log详情
	public function getProblemLogInfo($condition,$field){
		 return  $this->table('order_problem_log')->field($field)->where($condition)->find();
	}
	
	//删除log
	public function delProblemLogInfo($condition){		
		return $this->table('order_problem_log')->where($condition)->delete();		
	}
	
	
	
	
	
	
	
}
