<?php
/**
 * 排班模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class schedulingModel extends Model
{

    public function __construct()
    {
        parent::__construct('scheduling');
    }

    /**
     * 取店铺类别列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */

    public function getSchedulingList($condition = array(), $page = null, $order = 'scheduling_id desc', $field = '*',$group = '')
    {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->group($group)->select();
        return $list;
    }

    /**
     * 取得单条信息
     * @param unknown $condition
     */
    public function getSchedulingInfo($condition = array())
    {
        return $this->where($condition)->find();
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delScheduling($condition = array())
    {
        return $this->where($condition)->delete();
    }

	
	//保存
	public function countScheduling($where){
		
		return $this->where($where)->count();
		
		
		
		
	}
	
    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addScheduling($data)
    {
        return $this->insert($data);
    }

    public function addSchedulingAll($data)
    {
        return $this->insertAll($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editScheduling($data = array(), $condition = array())
    {
        return $this->where($condition)->update($data);
    }
}
