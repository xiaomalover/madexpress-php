<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class stoket_logModel extends Model {
    public function __construct() {
        parent::__construct('stoket_log');
    }

    /**
     * 添加Slog
     * @param array $insert
     * @return boolean
     */
    public function addSlog($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Slog
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editSlog($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Slog
     * @param unknown $condition
     * @return boolean
     */
    public function delSlog($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Slog数量
     * @param array $condition
     * @return array
     */
    public function getSlogCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Slog列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getSlogList($condition, $field = '*', $page =10, $order = 'log_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Slog内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getSlogInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
