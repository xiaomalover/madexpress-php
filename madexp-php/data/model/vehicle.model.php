<?php
/**
 * 送餐员模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class vehicleModel extends Model
{

    public function __construct()
    {
        parent::__construct('vehicle');
    }

    public function vehicleInfo($condition, $field = '*', $master = false)
    {
        return $this->table('vehicle')->field($field)->where($condition)->master($master)->find();
    }

    public function vehicleList($condition, $field = '*', $page = 0, $order = 'vehicle_id desc', $limit = '')
    {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

    public function getVehicleCount($condition)
    {
        return $this->where($condition)->count();
    }

    public function vehicleAdd($param)
    {
        return $this->insert($param);
    }

	   public function editVehicle($condition, $update) {
        return $this->where($condition)->update($update);
    }
	
	
}
