<?php
/**
 *搜索关键词
 */
defined('InMadExpress') or exit('Access Invalid!');
class keywordModel extends Model{
    public function __construct() {
        parent::__construct('search_keyword');
    }

    /**
     * 取得列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */
    public function getKeywordList($condition = array(),  $order = 'id desc') {
        return $this->where($condition)->order($order)->select();
    }

    /**
     * 增加新记录
     * @param unknown $data
     * @return
     */
    public function addKeyword($data) {
        return $this->insert($data);
    }

    /**
     * 取单条信息
     * @param unknown $condition
     */
    public function getKeywordInfo($condition) {
        return $this->where($condition)->find();
    }

    /**
     * 更新记录
     * @param unknown $condition
     * @param unknown $data
     */
    public function editKeyword($data,$condition) {
        return $this->where($condition)->update($data);
    }

    /**
     * 取得数量
     * @param unknown $condition
     */
    public function getKeywordCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 删除记录
     * @param unknown $condition
     */
    public function delKeyword($condition) {
        return $this->where($condition)->delete();
    }
}