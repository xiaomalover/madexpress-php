<?php
/**
 * 审批模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class store_examineModel extends Model {

    public function __construct(){
        parent::__construct('store_examine');
    }

    /**
     * 取店铺类别列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $order
     */
  
	 public function getStoreExamineList($condition, $page=null, $order='examine_id desc', $field='*') {
        $list = $this->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }
    /**
     * 取得单条信息
     * @param unknown $condition
     */
    public function getStoreExamineInfo($condition = array()) {
        return $this->where($condition)->find();
    }

    /**
     * 删除类别
     * @param unknown $condition
     */
    public function delStoreExamine($condition = array()) {
        return $this->where($condition)->delete();
    }
	
	 public function getStoreExamineCount($condition = array()) {
        return $this->where($condition)->count();
    }
	
	
    /**
     * 增加店铺分类
     * @param unknown $data
     * @return boolean
     */
    public function addStoreExamine($data) {
        return $this->insert($data);
    }

    /**
     * 更新分类
     * @param unknown $data
     * @param unknown $condition
     */
    public function editStoreExamine($data = array(),$condition = array()) {
        return $this->where($condition)->update($data);
    }
	
	//审核
	public function storeExamine($id){
		
		$where = array(
			'examine_id' => $id
		);
		
		$row = $this->getStoreExamineInfo($where);
		
		$update = array();
		switch($row['examine_type']){
				
			 case 'mobile': //修改公告
			
				$update['store_phone'] = $row['examine_new_data'];
				
				
			 break;  	
				
			 case 'shophours': //修改公告				
				$update['store_shophours'] = $row['examine_new_data'];
				
			 break;  	
				
			 case 'class': //分类
			
				$update['store_shophours'] = $row['examine_new_data'];
				
				
			 break; 
			 case 'address': //修改公告			
				$update['store_address'] = $row['examine_new_data'];
				
				
			 break; 	
			 case 'about': //修改公告			
				$update['store_about'] = $row['examine_new_data'];
				
				
			 break; 
			 case 'notice': //修改公告			
				$update['store_notice'] = $row['examine_new_data'];
								
			 break; 
				
		}
		
		$this->editStoreExamine(array('examine_state'=> 1),$where);
		$result = model('store')->editStore($update,array('store_id'=> $row['store_id']));		
		return $result;
		
		
	}
	
	
	
	
}
