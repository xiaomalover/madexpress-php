<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class waiter_workhoursModel extends Model {
    public function __construct() {
        parent::__construct('distributor_workhours');
    }

    /**
     * 添加Workhours
     * @param array $insert
     * @return boolean
     */
    public function addWorkhours($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Workhours
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editWorkhours($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Workhours
     * @param unknown $condition
     * @return boolean
     */
    public function delWorkhours($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Workhours数量
     * @param array $condition
     * @return array
     */
    public function getWorkhoursCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Workhours列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getWorkhoursList($condition, $field = '*', $page = 0, $order = 'work_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Workhours内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getWorkhoursInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
