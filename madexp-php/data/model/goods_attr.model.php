<?php
/**
 * 商品属性
 *

 */
defined('InMadExpress') or exit('Access Invalid!');

class goods_attrModel extends Model {
    public function __construct() {
        parent::__construct('goods_attr');
    }

    /**
     * 对应列表
     *
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsAttrList($condition, $field = '*',$order='attr_sort asc') {
        return $this->table('goods_attr')->where($condition)->field($field)->order($order)->select();
    }
	
	/*
	详情
	*/
	public function getGoodsAttrInfo($condition,$field){
		return $this->table('goods_attr')->field($field)->where($condition)->find();
	}
	
	/*
	添加属性
	*/
	public function addGoodsAttr($data){
		return $this->table('goods_attr')->insert($data);
	}
	
	/*
	编辑属性
	*/
	public function editGoodsAttr($condition,$data){
		return $this->table('goods_attr')->where($condition)->update($update);			
	}
	
	
	/**
	 * 删除
	 * @param unknown $condition
	 * @return boolean
	 */
	public function delGoodsAttr($condition) {       
	    return $this->table('goods_attr')->where($condition)->delete();
	}	
	
	
	
	
	
	/**
	 * 元素ICON管理开始
	 * 添加
	 * @param array $insert
	 * @return boolean
	 */
	public function addClass($insert) {
	    return $this->table('goods_attr_class')->insert($insert);
	}
	
	/**
	 * 编辑
	 * @param array $condition
	 * @param array $update
	 * @return boolean
	 */
	public function editClass($condition, $update) {
	    return $this->table('goods_attr_class')->where($condition)->update($update);
	}
	
	/**
	 * 删除
	 * @param unknown $condition
	 * @return boolean
	 */
	public function delClass($condition) {       
	    return $this->table('goods_attr_class')->where($condition)->delete();
	}
	
	/**
	 * 查询数量
	 * @param array $condition
	 * @return array
	 */
	public function getClassCount($condition) {
	    return $this->table('goods_attr_class')->where($condition)->count();
	}
	
	/**
	 * 列表
	 * @param array $condition
	 * @param string $field
	 * @param string $order
	 * @param number $page
	 * @param string $limit
	 * @return array
	 */
	public function getClassList($condition, $field = '*', $page = 0, $order = 'gc_sort asc', $limit = '') {
	    return $this->table('goods_attr_class')->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
	}
	
	   
	/**
	 * 取单个内容
	 * @param array $condition
	 * @param string $field
	 * @return array
	 */
	public function getClassInfo($condition, $field = '*') {
	    return $this->table('goods_attr_class')->field($field)->where($condition)->find();
	}
	
	
	
	
}
