<?php
/**
 * 手机端令牌模型
 *
 */

defined('InMadExpress') or exit('Access Invalid!');

class mb_admin_tokenModel extends Model{
    public function __construct(){
        parent::__construct('mb_admin_token');
    }

    /**
     * 查询
     *
     * @param array $condition 查询条件
     * @return array
     */
    public function getMbAdminTokenInfo($condition) {
        return $this->where($condition)->find();
    }

    public function getMbAdminTokenInfoByToken($token) {
        if(empty($token)) {
            return null;
        }
        return $this->getMbAdminTokenInfo(array('token' => $token));
    }

    public function updateMemberOpenId($token, $openId)
    {
        return $this->where(array(
            'token' => $token,
        ))->update(array(
            'openid' => $openId,
        ));
    }

    /**
     * 新增
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addMbAdminToken($param){
        return $this->insert($param);
    }

    /**
     * 删除
     *
     * @param int $condition 条件
     * @return bool 布尔类型的返回结果
     */
    public function delMbAdminToken($condition){
        return $this->where($condition)->delete();
    }
}
