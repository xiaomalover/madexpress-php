<?php
/**
 * 骑手端钱包

 */
defined('InMadExpress') or exit('Access Invalid!');
class waiter_walletModel extends Model {
   
	/**
     * 生成充值编号
     * @return string
     */
    public function makeSn() {
       return mt_rand(10,99)
              . sprintf('%010d',time() - 946656000)
              . sprintf('%03d', (float) microtime() * 1000)
              . sprintf('%03d', (int) $_SESSION['member_id'] % 1000);
    }
	
    /**
     * 取提现单信息总数
     * @param unknown $condition
     */
    public function getPdCashCount($condition = array()) {
        return $this->table('distributor_pd_cash')->where($condition)->count();
    }
	
	
    /**
     * 取得提现列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdCashList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('distributor_pd_cash')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 添加提现记录
     * @param array $data
     */
    public function addPdCash($data) {
        return $this->table('distributor_pd_cash')->insert($data);
    }

    /**
     * 编辑提现记录
     * @param unknown $data
     * @param unknown $condition
     */
    public function editPdCash($data,$condition = array()) {
        return $this->table('distributor_pd_cash')->where($condition)->update($data);
    }

    /**
     * 取得单条提现信息
     * @param unknown $condition
     * @param string $fields
     */
    public function getPdCashInfo($condition = array(), $fields = '*') {
        return $this->table('distributor_pd_cash')->where($condition)->field($fields)->find();
    }

    /**
     * 删除提现记录
     * @param unknown $condition
     */
    public function delPdCash($condition) {
        return $this->table('distributor_pd_cash')->where($condition)->delete();
    }
	
	
	
    /**
     * 取日志总数
     * @param unknown $condition
     */
    public function getPdLogCount($condition = array()) {
        return $this->table('distributor_pd_log')->where($condition)->count();
    }

	
    /**
     * 取得预存款变更日志列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getPdLogList($condition = array(), $pagesize = '', $fields = '*', $order = '', $limit = '') {
        return $this->table('distributor_pd_log')->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

	/**
	    * 删除日志
	    * @param unknown $condition
	    */
	   public function delPdLog($condition = array()) {
	       return $this->table('distributor_pd_log')->where($condition)->delete();
	   }
	
	
   
   
   
   
    /**
     * 变更余额
     * @param unknown $change_type
     * @param unknown $data
     * @throws Exception
     * @return unknown
     */
    public function changePd($change_type,$data = array()) {
        $data_log = array();
        $data_pd = array();
        $data_msg = array();
		
		
		
        //$data_log['lg_invite_distributor_id'] = $data['invite_distributor_id'];
        $data_log['lg_distributor_id'] = $data['distributor_id'];
        $data_log['lg_distributor_name'] = $data['distributor_name'];
        $data_log['lg_add_time'] = TIMESTAMP;
        $data_log['lg_type'] = $change_type;

        $data_msg['time'] = date('Y-m-d H:i:s');
        $data_msg['pd_url'] = urlMember('predeposit', 'pd_log_list');
        switch ($change_type){
           
		   /*
		   Delivery Income
		   配送收入
		   
		   Sales Order Number
		   订单编号
		   
		   Delivery Invoice
		   配送Invoice
		   */
						
			case 'delivery_income': //配送收入
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = 'Delivery Income';
				
				$data_log['lg_name_of_activities'] = 'Delivery Income';
				$data_log['lg_type_activities'] = 0;				
				
				$data_log['lg_order_sn'] = $data['order_sn'];	
				$data_log['lg_order_id'] = $data['order_id'];	
             
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
				
				
				
				
			case 'mpoig_incentive': //'MPOIG'的保底收入
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "'MPOIG' Incentive";				
				$data_log['lg_name_of_activities'] = "'MPOIG' Incentive";
				$data_log['lg_type_activities'] = 0;
				$data_log['lg_order_sn'] = $data['order_sn'];				
				$data_log['lg_order_id'] = $data['order_id'];				
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			
			
			/*
			'Schedule Urself' Incentive
			排班的保底收入
			
			'Schedule Urself' Ref. Number
			排班订单编号
			
			Incentive Invoice
			相关激励金发票
			*/
			case 'schedule_urself_incentive': //排班的保底收入
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "'Schedule Urself' Incentive";
				
				$data_log['lg_name_of_activities'] = "'Schedule Urself' Incentive";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];	
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 
			 /*
			 Delivery Fee Reimbursment
			 配送费补贴
			 
			 Relevant Delivery Credit Note Number
			 相关的配送Credit Note编号
			 
			 Delivery Fee Reimbursement Invoice
			 配送费补贴发票
			 */
			 case 'delivery_fee_reimbursment': //配送费补贴
			     $data_log['lg_av_amount'] = +$data['amount'];
			     $data_log['lg_desc'] = "Delivery Fee Reimbursment";
			 	
			 	$data_log['lg_name_of_activities'] = "Delivery Fee Reimbursment";
			 	$data_log['lg_type_activities'] = 0;
			 	
			 	$data_log['lg_order_sn'] = $data['order_sn'];
			 	$data_log['lg_order_id'] = $data['order_id'];	
			     $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
			     $data_msg['av_amount'] = +$data['amount'];
			     $data_msg['freeze_amount'] = 0;
			     $data_msg['desc'] = $data_log['lg_desc'];
			  break;
			 
			 /*
			 ME Wallet Top Up
			 ME钱包充值
			 
			 Number of Uploaded Evidence
			 后台生成的凭证序列号
			 
			 Uploaded Evidence by ME
			 后台选择生成的凭证
			 */
			 
			 case 'me_wallet_top_up': //ME钱包充值
			     $data_log['lg_av_amount'] = +$data['amount'];
			     $data_log['lg_desc'] = "ME Wallet Top Up";
			 	
			 	$data_log['lg_name_of_activities'] = "ME Wallet Top Up";
			 	$data_log['lg_type_activities'] = 0;
			 	
			 	$data_log['lg_order_sn'] = $data['order_sn'];
			 	$data_log['lg_order_id'] = $data['order_id'];	
			     $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
			     $data_msg['av_amount'] = +$data['amount'];
			     $data_msg['freeze_amount'] = 0;
			     $data_msg['desc'] = $data_log['lg_desc'];
			  break;
			 
			 /*
			 ME Refund
			 平台服务的（所有类型）退款
			 
			 ME Platform Credit Note Number
			 平台服务集成退款单号码
			 
			 */ 
			 
			 case 'me_refund': //其他平台服务费退款
			    $data_log['lg_av_amount'] = +$data['amount'];
			    $data_log['lg_desc'] = "ME Refund";
			 				
			 	$data_log['lg_name_of_activities'] = "ME Refund";
			 	$data_log['lg_type_activities'] = 0;
			 				
			 	$data_log['lg_order_sn'] = $data['order_sn'];			
			 	$data_log['lg_order_id'] = $data['order_id'];	
			    $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
			    $data_msg['av_amount'] = +$data['amount'];
			    $data_msg['freeze_amount'] = 0;
			    $data_msg['desc'] = $data_log['lg_desc'];
			 break;
			 
			 
			 
			 /*
			 Platform Bonus
			 平台奖金
			 
			 Bonus Type and Number
			 奖金类型与编号
			 */
			
			case 'platform_bonus': //平台奖金
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Platform Bonus";
				
				$data_log['lg_name_of_activities'] = "Platform Bonus";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];	
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			
			
			 
			 case 'from_internal_wallet_txn': //ME钱包内部转账
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Internal Wallet TXN";
				
				$data_log['lg_name_of_activities'] = "Internal Wallet TXN";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];				
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 
			 case 'bond_release': //押金退还
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "Bond Release";
				
				$data_log['lg_name_of_activities'] = "Bond Release";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 
			 		
			 
			 case 'me_store_refund': //购买装备退款
                $data_log['lg_av_amount'] = +$data['amount'];
                $data_log['lg_desc'] = "ME Store Refund (Equipement Purchase)";
				
				$data_log['lg_name_of_activities'] = "ME Store Refund";
				$data_log['lg_type_activities'] = 0;
				
				$data_log['lg_order_sn'] = $data['order_sn'];
				$data_log['lg_order_id'] = $data['order_id'];
                $data_pd['available_predeposit'] = array('exp','available_predeposit+'.$data['amount']);
                $data_msg['av_amount'] = +$data['amount'];
                $data_msg['freeze_amount'] = 0;
                $data_msg['desc'] = $data_log['lg_desc'];
             break;
			 

			 
			 /*******
			 扣除项
			 
			 ******/
			 
			 /*
			 Delivery Fee Refund
			 配送费退款
			 
			 Sales Order Number
			 订单编号
			 
			 Relevant Delivery Credit Note
			 相关的配送Credit Note
			 		 
			 
			 */
			 
			   case 'delivery_fee_refund': //订单退款(全单)
					$data_log['lg_av_amount'] = -$data['amount'];					
					$data_log['lg_desc'] = 'Delivery Fee Refund';
					$data_log['lg_admin_name'] = $data['admin_name'];					
					
					$data_log['lg_name_of_activities'] = "Delivery Fee Refund";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] =-$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 /*
			 Liquidated Damage
			 违约金
			 
			 Sales Order Number
			 订单编号
			 
			 */
			 
			  case 'liquidated_damage': //推配送费(全单)
					$data_log['lg_freeze_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Liquidated Damage';					
					$data_log['lg_name_of_activities'] = "Liquidated Damage";
					$data_log['lg_type_activities'] = 1;					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];	
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0 ;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			  
			  
			  
			  
			  case 'withdraw': //取现金额
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Withdraw';
					
					$data_log['lg_name_of_activities'] = "Withdraw";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
				    $data_log['lg_order_sn'] = $data['order_sn'];
				
					
					$data_pd['available_predeposit'] = array('exp','available_predeposit - '.$data['amount']);
					//$data_pd['freeze_predeposit'] = array('exp','freeze_predeposit + '.$data['amount']);
					
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
					
               break;
			 
			 
			 
			  case 'withdraw_fee': //取现手续费
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Withdraw Fee';
					
					$data_log['lg_name_of_activities'] = "Withdraw Fee";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
				    
				    $data_log['lg_order_sn'] = $data['order_sn'];
				
				    
					
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
               
               
			 
			   case 'incentive_cancelation': //激励金收入取消
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Incentive Cancelation';
					
					$data_log['lg_name_of_activities'] = "Incentive Cancelation";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];					
					
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 
			 
			 case 'bonus_cancelation': //奖金取消
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Bonus Cancelation';
					
					$data_log['lg_name_of_activities'] = "Bonus Cancelation";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			 
			 
			 case 'to_internal_wallet_txn': //ME钱包内部转账
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Internal Wallet TXN';
					
					$data_log['lg_name_of_activities'] = "Internal Wallet TXN";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = 0;
					$data_msg['freeze_amount'] = -$data['amount'];
					$data_msg['desc'] = $data_log['lg_desc'];
              break; 

					
			  case 'rental_bond': //租车押金
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Rental Bond';
					
					$data_log['lg_name_of_activities'] = "Rental Bond";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
					
			 case 'rental_fee': //租车押金
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Rental Fee';
					
					$data_log['lg_name_of_activities'] = "Rental Fee";
					$data_log['lg_type_activities'] = 1;
					
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];	
					$data_log['lg_order_id'] = $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] =  -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
							
			 case 'other_platform_services_fee': //其他平台服务费
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Other Platform Services Fee';
					
					$data_log['lg_name_of_activities'] = "Other Platform Services Fee";
					$data_log['lg_type_activities'] = 1;
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];
					$data_log['lg_order_id'] = $data['order_id'];				
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break; 			
					
					
				
				case 'offline_payment': //线下支付
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Offline Payment';
					
					$data_log['lg_name_of_activities'] = "Offline Payment";
					$data_log['lg_type_activities'] = 1;
					
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];	
					$data_log['lg_order_id'] = $data['order_id'];		
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] = -$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			   
			   
			   case 'equipement_purchase': //购买装备
					$data_log['lg_av_amount'] = -$data['amount'];
					$data_log['lg_desc'] = 'Equipement Purchase';
					
					$data_log['lg_name_of_activities'] = "Equipement Purchase";
					$data_log['lg_type_activities'] = 1;
					
					
					$data_log['lg_admin_name'] = $data['admin_name'];
					$data_log['lg_order_sn'] = $data['order_sn'];		
					$data_log['lg_order_id'] = $data['order_id'];			
					$data_pd['available_predeposit'] = array('exp','available_predeposit-'.$data['amount']);
					$data_msg['av_amount'] =-$data['amount'];
					$data_msg['freeze_amount'] = 0;
					$data_msg['desc'] = $data_log['lg_desc'];
               break;
			   
							  
			
            default:
                throw new Exception('参数错误');
                break;
        }
        
	
        $update = Model('waiter')->editWaiter(array('distributor_id'=>$data['distributor_id']),$data_pd);

        if (!$update) {
            throw new Exception('操作失败');
        }
		
		$waiter_info = model('waiter')->getWaiterInfo(array('distributor_id'=>$data['distributor_id']));
		
        $data_log['lg_balance'] = $waiter_info['available_predeposit'];   		
		
        $insert = $this->table('distributor_pd_log')->insert($data_log);
        if (!$insert) {
            throw new Exception('操作失败');
        }
		
		
		/*
        // 支付成功发送买家消息
        $param = array();
        $param['code'] = 'predeposit_change';
        $param['member_id'] = $data['member_id'];
        $data_msg['av_amount'] = ncPriceFormat($data_msg['av_amount']);
        $data_msg['freeze_amount'] = ncPriceFormat($data_msg['freeze_amount']);
        $param['param'] = $data_msg;
        QueueClient::push('addConsume', array('member_id'=>$data['member_id'],'member_name'=>$data['member_name'],
        'consume_amount'=>$data['amount'],'consume_time'=>time(),'consume_remark'=>$data_log['lg_desc']));
		
        QueueClient::push('sendMemberMsg', $param);
		*/
		
        return $insert;
    }

    

}
