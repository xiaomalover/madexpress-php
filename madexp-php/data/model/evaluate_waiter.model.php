<?php
/**
 * 骑手评价
 */
defined('InMadExpress') or exit('Access Invalid!');
class evaluate_waiterModel extends Model {

    public function __construct(){
        parent::__construct('evaluate_waiter');
    }

    /**
     * 查询店铺评分列表
     *
     * @param array $condition 查询条件
     * @param int $page 分页数
     * @param string $order 排序
     * @param string $field 字段
     * @return array
     */
    public function getEvaluateWaiterList($condition, $page=null, $order='waiter_evaluate_id desc', $field='*') {
        $list = $this->table('evaluate_waiter')->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;
    }

    /**
     * 获取店铺评分信息
     */
    public function getEvaluateWaiterInfo($condition, $field='*') {
        $list = $this->table('evaluate_waiter')->field($field)->where($condition)->find();
        return $list;
    }

  	public function getEvaluateWaiterCount($condition) {
        return $this->table('evaluate_waiter')->where($condition)->count();
    }

	
	
	//修改
	public function editEvaluateWaiter($where,$data){
		
		
		  return $this->table('evaluate_waiter')->where($where)->update($data);
	}


    /**
     * 添加店铺评分
     */
    public function addEvaluateWaiter($param) {
        return $this->table('evaluate_waiter')->insert($param);
    }

    /**
     * 删除店铺评分
     */
    public function delEvaluateWaiter($condition) {
        return $this->table('evaluate_waiter')->where($condition)->delete();
    }
    
    
    
    	//新增log
	public function  addLog($param){		
		return $this->table('evaluate_waiter_log')->insert($param);
	}
	
	//log列表
	public function getLogList($condition, $page=null, $order='id desc', $field='*') {       
		$list = $this->table('evaluate_waiter_log')->field($field)->where($condition)->page($page)->order($order)->select();
        return $list;		
	}
	
	
	//log详情
	public function getLogInfo($condition,$field){
		 return  $this->table('evaluate_waiter_log')->field($field)->where($condition)->find();
	}
	
	//删除log
	public function delLogInfo($condition){		
		return $this->table('evaluate_waiter_log')->where($condition)->delete();		
	}
    
    
}
