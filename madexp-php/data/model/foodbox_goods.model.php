<?php
/**
 * 申请餐盒模型
 */
defined('InMadExpress') or exit('Access Invalid!');

class foodbox_goodsModel extends Model {
    public function __construct() {
        parent::__construct('foodbox_goods');
    }

    /**
     * 添加
     * @param array $insert
     * @return boolean
     */
    public function addGoods($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑
     * @param array $condition
     * @param array $update
     * @return boolean
     */
	
    public function editGoods($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除
     * @param unknown $condition
     * @return boolean
     */
    public function delGoods($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     * 查询数量
     * @param array $condition
     * @return array
     */
    public function getGoodsCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getGoodsList($condition, $field = '*', $page = 0, $order = 'goods_id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }

   
    /**
     * 取单个内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
