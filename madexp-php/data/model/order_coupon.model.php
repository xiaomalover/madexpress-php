<?php
/**
 * 赠送优惠券记录
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class order_couponModel extends Model{

    public function __construct(){
        parent::__construct('order_coupon');
    }

    /**
     * 读取列表
     * @param array $condition
     *
     */
    public function getCouponList($condition = array(), $page = '', $order = '', $field = '*', $limit = '') {
        return $this->field($field)->where($condition)->page($page)->order($order)->limit($limit)->select();
    }

    /**
     * 读取单条记录
     * @param array $condition
     *
     */
    public function getCouponInfo($condition,$fields = '*',$order='order_sort desc,deliver_sort asc,last_time asc') {
        return $this->where($condition)->field($fields)->order($order)->find();
    }

    /*
     * 增加
     * @param array $data
     * @return bool
     */
    public function addCoupon($data){
        return $this->insert($data);
    }

	public function addCouponAll($data){		
		return $this->insertAll($data);		
	}
	
    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editCoupon($data,$condition) {
        return $this->where($condition)->update($data);
    }
	
	

    public function getCouponCount($condition) {
        return $this->where($condition)->count();
    }
	
	
	public function delCoupon($condition) {
        return $this->where($condition)->delete();
    }
	
	

}
