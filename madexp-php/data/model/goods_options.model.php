<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class goods_optionsModel extends Model {
    public function __construct() {
        parent::__construct('goods_options');
    }

    /**
     * 添加Options
     * @param array $insert
     * @return boolean
     */
    public function addOptions($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Options
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editOptions($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Options
     * @param unknown $condition
     * @return boolean
     */
    public function delOptions($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Options数量
     * @param array $condition
     * @return array
     */
    public function getOptionsCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Options列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getOptionsList($condition, $field = '*', $page = 0, $order = 'options_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Options内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getOptionsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
