<?php
/**
 * 订单管理

 */
defined('InMadExpress') or exit('Access Invalid!');
class orderModel extends Model {

    /**
     * 取单条订单信息
     *
     * @param unknown_type $condition
     * @param array $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return unknown
     */
	 
	 
	public function getOrderInfoN($condition = array(), $fields = '*', $order = '',$group = ''){		
		   return  $this->table('orders')->field($fields)->where($condition)->group($group)->order($order)->find();
	}
	 
    public function getOrderInfo($condition = array(), $extend = array(), $fields = '*', $order = '',$group = '') {
      
		$order_info = $this->table('orders')->field($fields)->where($condition)->group($group)->order($order)->find();
        if (empty($order_info)) {
            return array();
        }
        if (isset($order_info['order_state'])) {
            $order_info['state_desc'] = orderState($order_info);
        }
        if (isset($order_info['payment_code'])) {
            $order_info['payment_name'] = orderPaymentName($order_info['payment_code']);
        }

        //追加返回订单扩展表信息
        if (in_array('order_common',$extend)) {
            $order_info['extend_order_common'] = $this->getOrderCommonInfo(array('order_id'=>$order_info['order_id']));
            $order_info['extend_order_common']['reciver_info'] = unserialize($order_info['extend_order_common']['reciver_info']);
            $order_info['extend_order_common']['invoice_info'] = unserialize($order_info['extend_order_common']['invoice_info']);
        }

        if(in_array('delivery',$extend)){			
			$order_info['extend_delivery'] = Model('waiter')->getWaiterInfo(array('distributor_id'=>$order_info['distributor_id']));			
			unset($order_info['extend_delivery']['login_name']);
			unset($order_info['extend_delivery']['login_password']);
			unset($order_info['extend_delivery']['login_time']);
			unset($order_info['extend_delivery']['available_predeposit']);
			unset($order_info['extend_delivery']['freeze_predeposit']);
		}


        //追加返回店铺信息
        if (in_array('store',$extend)) {
            $order_info['extend_store'] = Model('store')->getStoreInfo(array('store_id'=>$order_info['store_id']));
			$order_info['extend_store']['store_avatar'] =  getStoreLogo($order_info['extend_store']['store_avatar'], 'store_avatar');   
			unset($order_info['extend_store']['store_login_name']);
			unset($order_info['extend_store']['store_login_password']);	
			unset($order_info['extend_store']['available_predeposit']);	
			unset($order_info['extend_store']['freeze_predeposit']);	
			unset($order_info['extend_store']['lbs_id']);	
			
					
        }

        //返回买家信息
        if (in_array('member',$extend)) {
            $order_info['extend_member'] = Model('member')->getMemberInfoByID($order_info['buyer_id']);
            unset($order_info['extend_member']['member_passwd']);	
            unset($order_info['extend_member']['available_predeposit']);	
            unset($order_info['extend_member']['freeze_predeposit']);	
            unset($order_info['extend_member']['available_rc_balance']);	
            unset($order_info['extend_member']['freeze_rc_balance']);	
        }

        //追加返回商品信息
        if (in_array('order_goods',$extend)) {
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'], 'state'=> 0, 'goods_usable_num'=>array('gt',0)));			
			$goods_list = array();
			foreach($order_goods_list as $k => $v){			    
			        $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$v['goods_ingr_new'] = $this->goodsIngrNew($v['goods_id']);	
                   // $v['goods_num']			 = $v['goods_usable_num'];
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();					
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();								
			}			
            $order_info['extend_order_goods'] = $goods_list;
        }





        //打印单数据
		if(in_array('order_print_goods',$extend)){			
			

            //获取商家的默认语言

            $store_lang = model('store_language')->getStoreLangInfo(['store_id' => $order_info['store_id'],'is_default' => 1]);
 

			//获取打印小票商品列表
			$order_goods_list = $this->getOrderGoodsList(array( 'order_id'=>$order_info['order_id']));					
			$goods_list = array();			
			
            foreach($order_goods_list as $k => $v){				
			       
                if($v['set_meal'] == 1){                    
                    $meal_ids = explode(',',$v['set_meal_spec']);
                    //获取套餐商品
                    $meal_list = $this->get_order_meal_specs_goods($meal_ids,$store_lang['language_en']);
                    foreach($meal_list as $m){
                        
                        $goods_item = array();
                        //    $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                            
                            $goods_item['goods_name']   =  $m['meal_value'];
                            $goods_item['goods_code']    = $v['goods_code'];
                            $goods_item['goods_num']    = 1;
                            $goods_item['goods_price']  = $m['meal_price'];
                            $goods_item['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                            $goods_item['goods_size']   =  $m['meal_size_name'] == 'DefaultSize' ? '' : $m['meal_size_name'];
                            $goods_item['goods_spec']   =  $m['meal_specs_name'];
                            $goods_item['goods_optional'] =  array();
                            $goods_item['original_goods_optional'] = array();
                            $goods_list[] = $goods_item;

                    }                    
                    
                }else{
            
                   if($v['goods_usable_num'] > 0){
                       $lang_goods = $this->goodsLang($v,$store_lang['language_en']); 
                       // print_r($lang_goods);
                        $goods_item = array();
                    //    $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                        
                        $goods_item['goods_name']   =  $lang_goods['goods_name'];
                        $goods_item['goods_num']    = $v['goods_usable_num'];
                        $goods_item['goods_code']    = $v['goods_code'];
                        $goods_item['goods_price']  = $v['goods_price'];
                        $goods_item['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                        $goods_item['goods_size']   = $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
                        $goods_item['goods_spec']   =  $lang_goods['goods_spec'];
                        $goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
                        $goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
                        $goods_list[] = $goods_item;
                
                   }
               
                }
                
            }
            
            //获取替换商品
			$order_goods_list = model('order_goods_replace')->getGoodsList(array('is_take_ffect'=> 1, 'order_id' => $order_info['order_id']));	
			foreach($order_goods_list as $k => $v){				
			       
                        $lang_goods = $this->goodsLang(['goods_code' => $v['goods_code']],$store_lang['language_en']); 
    				    
    				    $goods_item = array();
    				 //   $goods_item['goods_ingr'] = $this->goodsIngr($v['goods_id']);
    					$goods_item['goods_name']   = $lang_goods['goods_name'];
                        $goods_item['goods_code']    = $v['goods_code'];
    					$goods_item['goods_num']    = $v['goods_num'];
    					$goods_item['goods_price']  = $v['goods_price'];
                        $goods_item['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
    					$goods_item['goods_size']   =   $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
    					$goods_item['goods_spec']   = $lang_goods['goods_spec'];
    					$goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
    					$goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			            $goods_list[] = $goods_item;
			    
			     
					
			}
            
			
			
			
			 //获取赠送
			$order_goods_list = model('order_goods_give')->getGiveList(array('is_take_ffect'=> 1, 'order_id' => $order_info['order_id']));	
			foreach($order_goods_list as $k => $v){				
			       
                        $lang_goods = $this->goodsLang(['goods_code' => $v['goods_code']],$store_lang['language_en']); 
    				    
    				    
    				    $goods_item = array();
    				 //   $goods_item['goods_ingr']   = $this->goodsIngr($v['goods_id']);
    					$goods_item['goods_name']   =  $lang_goods['goods_name'];
    					$goods_item['goods_code']    = $v['goods_code'];
                        $goods_item['goods_num']    = $v['goods_num'];
    					$goods_item['goods_price']  = $v['goods_price'];
                        $goods_item['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
    					$goods_item['goods_size']   =  $lang_goods['goods_size'] == 'DefaultSize' ? '' : $lang_goods['goods_size'];
    					$goods_item['goods_spec']   = $lang_goods['goods_spec'];
    					$goods_item['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
    					$goods_item['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			            $goods_list[] = $goods_item;
			    
			     					
			}
      
			$order_info['extend_print_order_goods'] = $goods_list;
			
		}
	
        



		
		
		
		
		
         //追加返回替换商品信息
        if (in_array('replace',$extend)) {

            //group 调用商品
            $replace = model('order_goods_replace')->getGoodsList(array('is_take_ffect'=> 1, 'order_id' => $order_info['order_id']),'rec_id','rec_id');
       //     print_r($replace);
            
          //  $replace =  $this->getOrderGoodsList(array('order_id'=> $order_info['order_id'],'state' => 2),'*','','','rec_id desc','replace_rec_id');
            $replace_ids = array();
    		foreach($replace as $v){			
    			$replace_ids[] = $v['rec_id'];
    		}
    		
    	
    		$replace_ids = 	implode(',',$replace_ids);
    	//	print_r($replace_ids);
           	$order_goods_list = $this->getOrderGoodsList(array('rec_id'=> array('in',$replace_ids)));
            $replace_list = array();
            $goods_num = 0;
            $goods_amount = 0;
            foreach($order_goods_list as $k => $v){	
			    $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();	
			    $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();	  
				$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
				
				
				$replace_list[$k] = $v;
				$replace_list[$k]['replace_goods'] =  $this->order_replace_goods($v['rec_id']);				
				
				$goods_num += $v['goods_num'];
				$goods_amount +=$v['goods_num'] * $v['goods_price'];
				
			}
		 
            
            $order_info['extend_order_goods_replace'] = $replace_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;
			$order_info['extend_order_goods_replace_amount'] = $goods_amount;
            
            /*
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 2 ,'is_take_ffect'=> 1));			
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] ? unserialize($v['goods_optional']) : array();					
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional'] ? unserialize($v['original_goods_optional']) : array();
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_replace'] = $goods_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;*/
			
        }
        
        
         //追加返回替换商品信息
         if (in_array('replace_pc',$extend)) {

            //group 调用商品
            $replace = model('order_goods_replace')->getGoodsList(array('is_take_ffect'=> 1, 'version' => $order_info['is_old_num'],  'order_id' => $order_info['order_id']),'rec_id','rec_id');
       //     print_r($replace);
            
          //  $replace =  $this->getOrderGoodsList(array('order_id'=> $order_info['order_id'],'state' => 2),'*','','','rec_id desc','replace_rec_id');
            $replace_ids = array();
    		foreach($replace as $v){			
    			$replace_ids[] = $v['rec_id'];
    		}
    		
    	
    		$replace_ids = 	implode(',',$replace_ids);
    	//	print_r($replace_ids);
           	$order_goods_list = $this->getOrderGoodsList(array('rec_id'=> array('in',$replace_ids)));
            $replace_list = array();
            $goods_num = 0;
            $goods_amount = 0;
            foreach($order_goods_list as $k => $v){	

			    $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();	
			    $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();	  
				$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
				
				
				$replace_list[$k] = $v;
				$replace_list[$k]['replace_goods'] =  $this->order_replace_goods($v['rec_id']);				
				
				$goods_num += $v['goods_num'];
				$goods_amount +=$v['goods_num'] * $v['goods_price'];
				
			}
		 
            
            $order_info['extend_order_goods_replace'] = $replace_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;
			$order_info['extend_order_goods_replace_amount'] = $goods_amount;
            
            /*
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 2 ,'is_take_ffect'=> 1));			
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] ? unserialize($v['goods_optional']) : array();					
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional'] ? unserialize($v['original_goods_optional']) : array();
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_replace'] = $goods_list;
			$order_info['extend_order_goods_replace_num'] = $goods_num;*/
			
        }
        
        
        //赠送商品后端，打单机
        if (in_array('goods_give_pc',$extend)) {
            
            
            $order_goods_list = model('order_goods_give')->getGiveList(array('is_take_ffect'=> 1, 'version' => $order_info['is_old_num'], 'order_id'=>$order_info['order_id']));
            
            //取商品列表
          //  $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 3 ,'is_take_ffect'=> 1));			
                                    
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
                    $goods_list[$k]['goods_size']   =  $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'];
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] == null  ? array() : unserialize($v['goods_optional']) ;			
	                $goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();					
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_give'] = $goods_list;
			$order_info['extend_order_goods_give_num'] = $goods_num;
			
        }
        
        
         //赠送商品前段
         if (in_array('goods_give',$extend)) {
            
            
            $order_goods_list = model('order_goods_give')->getGiveList(array('is_take_ffect'=> 1,   'order_id'=>$order_info['order_id']));
            
            //取商品列表
          //  $order_goods_list = $this->getOrderGoodsList(array('order_id'=>$order_info['order_id'],'state'=> 3 ,'is_take_ffect'=> 1));			
                                    
			$goods_list = array();
			$goods_num = 0;
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
                    $goods_list[$k]['goods_size']   =  $v['goods_size'] == 'DefaultSize' ? '' : $v['goods_size'];
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional'] == null  ? array() : unserialize($v['goods_optional']) ;			
	                $goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();					
					$goods_num += $v['goods_num'];
			}
			
            $order_info['extend_order_goods_give'] = $goods_list;
			$order_info['extend_order_goods_give_num'] = $goods_num;
			
        }
        




        	//售前退款
            if (in_array('presale_refund',$extend)) {

                 //获取当前
              $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'refund_stage' => array('in','0,1')]);
                //取商品列表
                 $order_goods_list = model('order_goods_refund')->getRefundList(
				array(
					'refund_id' => $refund['refund_id']
				));
				
			
                $goods_list = array();
                foreach($order_goods_list as $k => $v){
                        $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                        $goods_list[$k] = $v;
                        $goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                        $goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
                        $goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
                }		  
			
                 $order_info['extend_presale_refund'] = $goods_list;
						

            }

            //售后退款
            if(in_array('aftersale_refund',$extend)){

                   //获取当前
              $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'refund_stage' => 2]);
              //取商品列表
               $order_goods_list = model('order_goods_refund')->getRefundList(
              array(
                  'refund_id' => $refund['refund_id']
              ));
              
          
              $goods_list = array();
              foreach($order_goods_list as $k => $v){
                      $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                      $goods_list[$k] = $v;
                      $goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                      $goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
                      $goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
              }		  
          
               $order_info['extend_aftersale_refund'] = $goods_list;



            }
        
        
		//一阶段退款
        if (in_array('one_stage_refund',$extend)) {


            //获取当前
            $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'refund_stage' => 0]);
            

            //取商品列表
            $order_goods_list = model('order_goods_refund')->getRefundList(
				array(
					'refund_id' => $refund['refund_id']
				));
				
			
            $goods_list = array();
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			}		  
			
		    $order_info['extend_one_stage_refund'] = $goods_list;
						
        }
        

        //一阶段退款
        if (in_array('one_stage_refund_pc',$extend)) {


            //获取当前
            $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'], 'version' => $order_info['is_old_num'], 'refund_stage' => 0]);
            

            //取商品列表
            $order_goods_list = model('order_goods_refund')->getRefundList(
				array(
					'refund_id' => $refund['refund_id']
				));
				
			
            $goods_list = array();
			foreach($order_goods_list as $k => $v){
			    	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
					$goods_list[$k] = $v;
					$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
					$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
			}		  
			
		    $order_info['extend_one_stage_refund'] = $goods_list;
						
        }
        

		
		
        //二阶段退款
        if (in_array('two_stage_refund',$extend)) {            

             //获取当前
             $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'refund_stage' => 1]);
            
            //取商品列表
            $order_goods_list = model('order_goods_refund')->getRefundList(
				array(				
					'refund_id' => $refund['refund_id']
				)
			);
			$goods_list = array();
			$goods_num = 0;
            $kk = 0;
			foreach($order_goods_list as $k => $v){
			        
			       	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                    $v['replace_type'] = 0;   
					$goods_list[$kk] = $v;
					$goods_list[$kk]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
					$goods_list[$kk]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
					$goods_list[$kk]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
              
              /*      if($v['is_replace'] == 1){
                        $kk = $kk+1;
                        $replace_data = $this->get_replace_refund($v['replace_rec_id']);
                        $replace_data['replace_type'] = 1;
                        $goods_list[$kk] = $replace_data;

                    }*/

                    $kk ++;



			}			
            $order_info['extend_two_stage_refund']     = $goods_list;
        }
        
		
		
        //二阶段退款
        if (in_array('two_stage_refund_pc',$extend)) {            

            //获取当前
            $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'], 'version' => $order_info['is_old_num'],'refund_stage' => 1]);
          
           //取商品列表
           $order_goods_list = model('order_goods_refund')->getRefundList(
               array(				
                   'refund_id' => $refund['refund_id'],
                   'version' => $order_info['is_old_num']
               )
           );
          
           $goods_list = array();
           $goods_num = 0;
           $kk = 0;
           foreach($order_goods_list as $k => $v){
                  if($v['is_replace'] == 1){
                    $v['replace_type'] = 0;  
                  }
                   $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
                   $goods_list[$kk] = $v;
                   $goods_list[$kk]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
                   $goods_list[$kk]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
                   $goods_list[$kk]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
                  
                  
                  /* if($v['is_replace'] == 1){
                       $kk = $kk+1;
                       
                        $replace_data = $this->get_replace_refund($v['replace_rec_id']);
                        $replace_data['replace_type'] = 1;
                        $replace_data['goods_price'] = 0;


                        $goods_list[$kk] = $replace_data;

                    }
*/
                    $kk ++;

                   
           }			
           $order_info['extend_two_stage_refund']     = $goods_list;
       }
       
		
        //三阶段退款
        if (in_array('three_stage_refund',$extend)) {
            
            //获取当前
            $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'refund_stage' =>2]);
            
            //取商品列表
            $order_goods_list = model('order_goods_refund')->getRefundList(
                  array(				
                      'refund_id' => $refund['refund_id']
                  )
              );
        	$goods_list = array();
        	$goods_num = 0;
        	foreach($order_goods_list as $k => $v){        	        
        	       	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
        			$goods_list[$k] = $v;
        			$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
        			$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
        			$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
        	}			
            $order_info['extend_three_stage_refund']     = $goods_list;        	
        }		
		

        	
        //三阶段退款
        if (in_array('three_stage_refund_pc',$extend)) {
            
            //获取当前
            $refund = model('order_refund')->getRefundInfo(['order_id' => $order_info['order_id'],'is_delivery' => 0,'version' => $order_info['is_old_num'], 'refund_stage' =>2]);
            
          
            //取商品列表
            $order_goods_list = model('order_goods_refund')->getRefundList(
                  array(				
                      'refund_id' => $refund['refund_id']
                  )
              );
        	$goods_list = array();
        	$goods_num = 0;
        	foreach($order_goods_list as $k => $v){        	        
        	       	$v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
        			$goods_list[$k] = $v;
        			$goods_list[$k]['goods_image'] = $this->goodsImageUrl($v['goods_image'],$v['store_id']);
        			$goods_list[$k]['goods_optional'] = $v['goods_optional']  ? unserialize($v['goods_optional']) : array();		
        			$goods_list[$k]['original_goods_optional'] = $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
        	}			
            $order_info['extend_three_stage_refund']     = $goods_list;        	
        }		
				
			
			
        		
		
		//返回订单操作记录
		if(in_array('order_log',$extend)){
			 
			 $order_log = $this->getOrderLogList(array('order_id'=>$order_info['order_id']),'log_id asc');
			 $data = array();
			 foreach($order_log as $k => $v){
				$data[$k] = $v;
                $data[$k]['log_times'] = $v['log_time'];
				$data[$k]['log_time'] = date('H:i',$v['log_time']);
               
			 }
			 
			 $order_info['extend_order_log'] = $data; 
			
			
		}
		
		
		
        return $order_info;
    }


    //获取替换退款的商品
    private function get_replace_refund($rec_id){

        $data = model('order_goods_replace')->getGoodsInfo(['rec_id' => $rec_id]);
        return $data;
        
    }

    	//套餐子数据集
	private function get_order_meal_specs_goods($meal_id,$lang="CHN-S"){
		
		$where = array();
		$where['meal_id'] = array('in',$meal_id);		
		$where['lang'] = $lang;
		$where['is_old'] = 0;
		$data = model('store_goods_meal')->getMealList($where);		
	
		return $data;		
		
	}
    

 


	
	//格式化当前数据根据现有值匹配出英文值	
	private function goodsLang($data,$lang){		
			
        //根据现有数据获得规格的key
       $where = array(
           'goods_code' => $data['goods_code'],
           'lang_name' => $lang
       );		
       $lang_goods = model('goods_language')->getGoodsLangInfo($where);	
       $cart['goods_name'] = $lang_goods['goods_name'];
       //size			
       $size_row = 	model('store_goods_size')->getSizeInfo(array('size_id' => $data['goods_size_id'],'lang' => $lang,'is_old' => 0));
                       
       $cart['goods_size'] = $size_row['size_value'];
       $specs_row =  model('store_goods_specs')->getSpecsList(array('specs_id'=>array('in',$data['goods_specs_id']),'lang' => $lang,'is_old' =>0));
       $specs_name = [];
       foreach($specs_row as $v){
           $specs_name[] = $v['specs_value'];
       }
       $specs_name = implode('/',$specs_name);			
       $cart['goods_spec'] = $specs_name;			
       $cart['goods_lang']	 = $lang;
       return $cart;
    }

    private function goodsOptional($ids,$lang ='CHN-S'){
		   
        $where = array();
        $where['options_id'] = array('in',$ids);
        $where['is_old'] = 0;		   
        $options_list = model('store_goods_options')->getOptionsList($where);
        $data = array();	   
        $size_model = model('store_goods_size');
        $specs_model = model('store_goods_specs');
        $goods_lang_model = model('goods_language');
        foreach($options_list as $k =>$v){			  
            $size = $size_model->getSizeInfo(array('size_id' => $v['goods_size_id'],'lang' => $lang,'is_old' => 0));
            $data[$k]['size'] = $size['size_value'];		   
            $specs = $specs_model->getSpecsList(array('specs_id'=> array('in',$v['goods_specs_id']),'lang' => $lang));
            $specs_name = array();
            foreach($specs as $vs){
                $specs_name[] = $vs['specs_value'];
            }
            $specs_name = implode(',',$specs_name);			   
            
            $data[$k]['specs'] = $specs_name;		   
            $data[$k]['price'] = $v['options_price'];		   
            $goods_name = $goods_lang_model->getGoodsLangInfo(array('goods_code' => $v['options_goods_code'],'lang_name' => $lang,'is_old' => 0 ));		   
            $data[$k]['name'] = $goods_name['goods_name'];
     //	   $data[$k]['goods_code'] = $v['options_goods_code'];
            $data[$k]['id'] = $v['options_id'];
        }
        
        return $data;
        
        
 }



    //获取商品ICON
    private function goodsIngr($goods_id){
		
			$goods_info = Model('goods')->getGoodsInfo(array('goods_id' => $goods_id));
		
			$where = array(
				'attr_id' => array('in',$goods_info['goods_ingr'])
			);
			
			
			$ingr = model('goods_attr')->getGoodsAttrList($where);
			$ingr_data = array();			
			foreach($ingr as $k=> $v){
				$ingr_data[$k]['attr_id'] =  $v['attr_id'];
			    if($v['attr_type'] == 1){
					$new = explode("|", $v['attr_content']);
					$ingr_data[$k]['attr_name'] = $new[0];
					$ingr_data[$k]['attr_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[2];
					$ingr_data[$k]['attr_active_icon'] = UPLOAD_SITE_URL.'/shop/icon/'.$new[1];
				
				}else{
				    
					$ingr_data[$k]['attr_name'] =  $v['attr_name'];
					$ingr_data[$k]['attr_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_icon'];	
					$ingr_data[$k]['attr_active_icon'] =   UPLOAD_SITE_URL.'/shop/icon/'.$v['attr_active_icon'];	
					
				}	
			}
			return $ingr_data;	
		}
		
		
		//商品成分记录
		private function goodsIngrNew($goods_id){
					
			$goods_info = Model('goods')->getGoodsInfo(array('goods_id' => $goods_id));
							
					
			$data = array();		
			$where = array(
				'attr_id' => array('in',$goods_info['goods_ingr'])
			);		
			$row = model('goods_attr')->getGoodsAttrList($where);
			
			foreach($row as $v){
				
				
				
				if($v['attr_type'] == 1){			
					$content = explode('|',$v['attr_content']);
					$v['attr_name'] = $content[0];
					$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[1];
					$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $content[2];
				}else{
					$v['attr_icon'] =		 UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_icon'];
					$v['attr_active_icon']	= UPLOAD_SITE_URL.DS. ATTACH_INGR . DS . $v['attr_active_icon'];
					
				}
				
				
				$data[$v['attr_class_id']]['name'] = $v['attr_class_name'];
				$data[$v['attr_class_id']]['child'][] = $v;
			}		
			
			$list = array();
			foreach($data as $v){
				$list[] = $v;
			}		
			return $list;		
		}
		
		
    
    private function order_replace_goods($rec_id){
        
    
        $list =    model('order_goods_replace')->getGoodsList(array('rec_id'=>$rec_id));
        $data = array();
        foreach($list as $v){
            $v['goods_optional'] =  $v['goods_optional']  ? unserialize($v['goods_optional']) : array();
            $v['original_goods_optional'] =  $v['original_goods_optional']  ? unserialize($v['original_goods_optional']) : array();
            $v['goods_ingr'] = $this->goodsIngr($v['goods_id']);
				
            $data[] = $v;
        }
        
        return $data;
        
        
    }

    
	
	private function goodsImageUrl($image,$store_id){
		
		if(empty($image)){
			return '';
		}		
		return UPLOAD_SITE_URL.DS. ATTACH_GOODS . DS . $store_id . DS . $image;
		
	}


    public function getOrderCommonInfo($condition = array(), $field = '*') {
        return $this->table('order_common')->where($condition)->find();
    }

    public function getOrderPayInfo($condition = array(), $master = false,$lock = false) {
        return $this->table('order_pay')->where($condition)->master($master)->lock($lock)->find();
    }

    /**
     * 取得支付单列表
     *
     * @param unknown_type $condition
     * @param unknown_type $pagesize
     * @param unknown_type $filed
     * @param unknown_type $order
     * @param string $key 以哪个字段作为下标,这里一般指pay_id
     * @return unknown
     */
    public function getOrderPayList($condition, $pagesize = '', $filed = '*', $order = '', $key = '') {
        return $this->table('order_pay')->field($filed)->where($condition)->order($order)->page($pagesize)->key($key)->select();
    }

    /**
     * 取得店铺订单列表
     *
     * @param int $store_id 店铺编号
     * @param string $order_sn 订单sn
     * @param string $buyer_name 买家名称
     * @param string $state_type 订单状态
     * @param string $query_start_date 搜索订单起始时间
     * @param string $query_end_date 搜索订单结束时间
     * @param string $skip_off 跳过已关闭订单
     * @return array $order_list
     */
     public function getStoreOrderList($store_id, $order_sn, $buyer_name, $state_type, $query_start_date, $query_end_date, $skip_off, $fields = '*', $extend = array(),$is_problem = 0) {
        $condition = array();
        $condition['store_id'] = $store_id;
        if (preg_match('/^\d{10,20}$/',$order_sn)) {
            $condition['order_sn'] = $order_sn;
        }
        
        if ($buyer_name != '') {
            $condition['buyer_name'] = $buyer_name;
        }
        
		if($state_type){
			$condition['order_state'] = $state_type;			
		}
		 
		
		
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_start_date);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_end_date);
        $start_unixtime = $if_start_date ? strtotime($query_start_date) : null;
        $end_unixtime = $if_end_date ? strtotime($query_end_date): null;
        if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }

		if($is_problem > 0 ){			
			$condition['is_problem'] = $is_problem;					
			$conditon['order_state'] = array('in','30,40,50,60');				
		}


		//$condition['is_formal'] =  1; //加载当前显示的订单			
     
		$order_list = $this->getOrderList($condition, 20, $fields, 'order_id desc','', $extend);
		
		
		$order = array();
        //页面中显示那些操作
        foreach ($order_list as $key => $order_info) {                         
            
            $order_info['countdown'] = 120;
            $order[] = $order_info;
            
            
        }
        return $order;
    }



	
	
	
	
	
	
	

    /**
     * 取得订单列表(未被删除)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getNormalOrderList($condition, $pagesize = '', $field = '*', $order = 'order_id desc', $limit = '', $extend = array()){
        $condition['delete_state'] = 0;
        return $this->getOrderList($condition, $pagesize, $field, $order, $limit, $extend);
    }

	
	
	public function getOrderListNew($condition,  $field = '*', $order = 'order_id desc', $limit = ''){
		
		  return  $this->table('orders')->field($field)->where($condition)->order($order)->limit($limit)->select();
	}
	
    /**
     * 取得订单列表(所有)
     * @param unknown $condition
     * @param string $pagesize
     * @param string $field
     * @param string $order
     * @param string $limit
     * @param unknown $extend 追加返回那些表的信息,如array('order_common','order_goods','store')
     * @return Ambigous <multitype:boolean Ambigous <string, mixed> , unknown>
     */
    public function getOrderList($condition, $pagesize = '', $field = '*', $order = 'order_id desc', $limit = '', $extend = array(), $master = false){
        $list = $this->table('orders')->field($field)->where($condition)->page($pagesize)->order($order)->limit($limit)->master($master)->select();
        if (empty($list)) return array();
        $order_list = array();
        foreach ($list as $order) {
            if (isset($order['order_state'])) {
                $order['state_desc'] = orderState($order);
            }
            if (isset($order['payment_code'])) {
                $order['payment_name'] = orderPaymentName($order['payment_code']);
            }
            if (!empty($extend)) $order_list[$order['order_id']] = $order;
        }
        if (empty($order_list)) $order_list = $list;

       
        //追加返回店铺信息
        if (in_array('store',$extend)) {
            $store_id_array = array();
            foreach ($order_list as $value) {
                if (!in_array($value['store_id'],$store_id_array)) $store_id_array[] = $value['store_id'];
            }
            $store_list = Model('store')->getStoreList(array('store_id'=>array('in',$store_id_array)));
            $store_new_list = array();
            foreach ($store_list as $store) {
                $store_new_list[$store['store_id']] = $store;
            }
            foreach ($order_list as $order_id => $order) {
                $order_list[$order_id]['extend_store'] = $store_new_list[$order['store_id']];
            }
        }

        //追加返回买家信息
        if (in_array('member',$extend)) {
            foreach ($order_list as $order_id => $order) {
                $order_list[$order_id]['extend_member'] = Model('member')->getMemberInfoByID($order['buyer_id']);
            }
        }

		
        //追加返回商品信息
        if (in_array('order_goods',$extend)) {
            //取商品列表
            $order_goods_list = $this->getOrderGoodsList(array('order_id'=>array('in',array_keys($order_list))));			
            if (!empty($order_goods_list)) {
                foreach ($order_goods_list as $value) {
                    $order_list[$value['order_id']]['extend_order_goods'][] = $value;
                }
            } else {
                $order_list[$value['order_id']]['extend_order_goods'] = array();
            }
        }

        return $order_list;
    }

    /**
     * 取得(买/卖家)订单某个数量缓存
     * @param string $type 买/卖家标志，允许传入 buyer、store
     * @param int $id   买家ID、店铺ID
     * @param string $key 允许传入  NewCount、PayCount、SendCount、EvalCount、TakesCount，分别取相应数量缓存，只许传入一个
     * @return array
     */
    public function getOrderCountCache($type, $id, $key) {
        if (!C('cache_open')) return array();
        $type = 'ordercount'.$type;
        $ins = Cache::getInstance('cacheredis');
        $order_info = $ins->hget($id,$type,$key);
        return !is_array($order_info) ? array($key => $order_info) : $order_info;
    }


    /**
     * 设置(买/卖家)订单某个数量缓存
     * @param string $type 买/卖家标志，允许传入 buyer、store
     * @param int $id 买家ID、店铺ID
     * @param array $data
     */
    public function editOrderCountCache($type, $id, $data) {
        if (!C('cache_open') || empty($type) || !intval($id) || !is_array($data)) return ;
        $ins = Cache::getInstance('cacheredis');
        $type = 'ordercount'.$type;
        $ins->hset($id,$type,$data);
    }

    /**
     * 取得买卖家订单数量某个缓存
     * @param string $type $type 买/卖家标志，允许传入 buyer、store
     * @param int $id 买家ID、店铺ID
     * @param string $key 允许传入  NewCount、PayCount、SendCount、EvalCount、TakesCount，分别取相应数量缓存，只许传入一个
     * @return int
     */
    public function getOrderCountByID($type, $id, $key) {
        $cache_info = $this->getOrderCountCache($type, $id, $key);

        if (is_string($cache_info[$key])) {
            //从缓存中取得
            $count = $cache_info[$key];
        } else {
            //从数据库中取得
            $field = $type == 'buyer' ? 'buyer_id' : 'store_id';
            $condition = array($field => $id);
            $func = 'getOrderState'.$key;
            $count = $this->$func($condition);
            $this->editOrderCountCache($type,$id,array($key => $count));
        }
        return $count;
    }

    /**
     * 删除(买/卖家)订单全部数量缓存
     * @param string $type 买/卖家标志，允许传入 buyer、store
     * @param int $id   买家ID、店铺ID
     * @return bool
     */
    public function delOrderCountCache($type, $id) {
        if (!C('cache_open')) return true;
        $ins = Cache::getInstance('cacheredis');
        $type = 'ordercount'.$type;
        return $ins->hdel($id,$type);
    }

    /**
     * 待付款订单数量
     * @param unknown $condition
     */
    public function getOrderStateNewCount($condition = array()) {
        $condition['order_state'] = ORDER_STATE_NEW;
        $condition['chain_code'] = 0;
        return $this->getOrderCount($condition);
    }

    /**
     * 待发货订单数量
     * @param unknown $condition
     */
    public function getOrderStatePayCount($condition = array()) {
        $condition['order_state'] = ORDER_STATE_PAY;
        return $this->getOrderCount($condition);
    }

    /**
     * 待收货订单数量
     * @param unknown $condition
     */
    public function getOrderStateSendCount($condition = array()) {
        $condition['order_state'] = ORDER_STATE_SEND;
        return $this->getOrderCount($condition);
    }

    /**
     * 待评价订单数量
     * @param unknown $condition
     */
    public function getOrderStateEvalCount($condition = array()) {
        $condition['order_state'] = ORDER_STATE_SUCCESS;
        $condition['delete_state'] = 0;
        $condition['evaluation_state'] = 0;
        return $this->getOrderCount($condition);
    }

    /**
     * 待自提订单数量
     * @param unknown $condition
     */
    public function getOrderStateTakesCount($condition = array()) {
        $condition['order_state'] = array('in',array(ORDER_STATE_NEW,ORDER_STATE_PAY));
        $condition['chain_code'] = array('gt',0);
        return $this->getOrderCount($condition);
    }

    /**
     * 交易中的订单数量
     * @param unknown $condition
     */
    public function getOrderStateTradeCount($condition = array()) {
        $condition['order_state'] = array(array('neq',ORDER_STATE_CANCEL),array('neq',ORDER_STATE_SUCCESS),'and');
        return $this->getOrderCount($condition);
    }

    /**
     * 取得订单数量
     * @param unknown $condition
     */
    public function getOrderCount($condition = array()) {
        return $this->table('orders')->where($condition)->count();
    }

    /**
     * 取得订单商品表详细信息
     * @param unknown $condition
     * @param string $fields
     * @param string $order
     */
    public function getOrderGoodsInfo($condition = array(), $fields = '*', $order = '') {
        return $this->table('order_goods')->where($condition)->field($fields)->order($order)->find();
    }

    /**
     * 取得订单商品表列表
     * @param unknown $condition
     * @param string $fields
     * @param string $limit
     * @param string $page
     * @param string $order
     * @param string $group
     * @param string $key
     */
    public function getOrderGoodsList($condition = array(), $fields = '*', $limit = null, $page = null, $order = 'rec_id desc', $group = null, $key = null) {
        return $this->table('order_goods')->field($fields)->where($condition)->limit($limit)->order($order)->group($group)->key($key)->page($page)->select();
    }

    /**
     * 取得订单扩展表列表
     * @param unknown $condition
     * @param string $fields
     * @param string $limit
     */
    public function getOrderCommonList($condition = array(), $fields = '*', $order = '', $limit = null) {
        return $this->table('order_common')->field($fields)->where($condition)->order($order)->limit($limit)->select();
    }

    /**
     * 插入订单支付表信息
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrderPay($data) {
        return $this->table('order_pay')->insert($data);
    }

    /**
     * 插入订单表信息
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrder($data) {
        $insert = $this->table('orders')->insert($data);
        if ($insert) {
            //更新缓存
            if (C('cache_open')) {
                QueueClient::push('delOrderCountCache',array('buyer_id'=>$data['buyer_id'],'store_id'=>$data['store_id']));
            }
        }
        return $insert;
    }

    /**
     * 插入订单扩展表信息
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrderCommon($data) {
        return $this->table('order_common')->insert($data);
    }

    /**
     * 插入订单扩展表信息
     * @param array $data
     * @return int 返回 insert_id
     */
    public function addOrderGoods($data) {
        return $this->table('order_goods')->insertAll($data);
    }
	
	
    public function addOrderGood($data) {
        return $this->table('order_goods')->insert($data);
    }

    /**
     * 添加订单日志
     */
    public function addOrderLog($data) {
        $data['log_role'] = str_replace(array('buyer','seller','system','admin','waiter'),array('买家','商家','系统','管理员','送餐员'), $data['log_role']);
        $data['log_time'] = TIMESTAMP;
        return $this->table('order_log')->insert($data);
    }
	
	public function delOrderLog($where){
	    
	        return $this->table('order_log')->where($where)->delete();
	}
	
	
	

    /**
     * 更改订单信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function editOrder($data,$condition,$limit = '') {
        $update = $this->table('orders')->where($condition)->limit($limit)->update($data);
        if ($update) {
            //更新缓存
            if (C('cache_open')) {
                QueueClient::push('delOrderCountCache',$condition);
            }
        }
        return $update;
    }

    /**
     * 更改订单信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function editOrderCommon($data,$condition) {
        return $this->table('order_common')->where($condition)->update($data);
    }

    /**
     * 更改订单信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function editOrderGoods($data,$condition) {
        return $this->table('order_goods')->where($condition)->update($data);
    }


	/**
     * 删除订单信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function delOrderGoods($condition) {
        return $this->table('order_goods')->where($condition)->delete();
    }





    /**
     * 更改订单支付信息
     *
     * @param unknown_type $data
     * @param unknown_type $condition
     */
    public function editOrderPay($data,$condition) {
        return $this->table('order_pay')->where($condition)->update($data);
    }

    /**
     * 订单操作历史列表
     * @param unknown $order_id
     * @return Ambigous <multitype:, unknown>
     */
    public function getOrderLogList($condition,$order = '') {
        return $this->table('order_log')->where($condition)->order($order)->select();
    }

    /**
     * 取得单条订单操作记录
     * @param unknown $condition
     * @param string $order
     */
    public function getOrderLogInfo($condition = array(), $order = '') {
        return $this->table('order_log')->where($condition)->order($order)->find();
    }

  
	
	
	/**
     * 返回是否允许某些操作
     * @param unknown $operate
     * @param unknown $order_info
     */
    public function getOrderOperateState($operate,$order_info){
        if (!is_array($order_info) || empty($order_info)) {
            return false;
        }

        if (isset($order_info['if_'.$operate])) {
            return $order_info['if_'.$operate];
        }

        switch ($operate) {

            //买家取消订单
           case 'buyer_cancel':
               $state = ($order_info['order_state'] == 30) ||
                   ($order_info['payment_code'] == 'offline' && $order_info['order_state'] == 30);
               break;

           //申请退款
           case 'refund_cancel':
               $state = $order_info['refund'] == 1 && !intval($order_info['lock_state']);
               break;

           //商家取消订单
           case 'store_cancel':
               $state = ($order_info['order_state'] == ORDER_STATE_NEW && $order_info['payment_code'] != 'chain') ||
               ($order_info['payment_code'] == 'offline' &&
               in_array($order_info['order_state'],array(ORDER_STATE_PAY,ORDER_STATE_SEND)));
               break;

           //平台取消订单
           case 'system_cancel':
               $state = ($order_info['order_state'] == ORDER_STATE_NEW) ||
               ($order_info['payment_code'] == 'offline' && $order_info['order_state'] == ORDER_STATE_PAY);
               break;

           //平台收款
           case 'system_receive_pay':
               $state = $order_info['order_state'] == ORDER_STATE_NEW;
               $state = $state && $order_info['payment_code'] == 'online' && $order_info['api_pay_time'];
               break;

           //买家投诉
           case 'complain':
               $state = in_array($order_info['order_state'],array(ORDER_STATE_PAY,ORDER_STATE_SEND)) ||
                   intval($order_info['finnshed_time']) > (TIMESTAMP - C('complain_time_limit'));
               break;

           case 'payment':
               $state = $order_info['order_state'] == ORDER_STATE_NEW && $order_info['payment_code'] == 'online';
               break;

            //调整运费
            case 'modify_price':
                $state = ($order_info['order_state'] == ORDER_STATE_NEW) ||
                   ($order_info['payment_code'] == 'offline' && $order_info['order_state'] == ORDER_STATE_PAY);
                $state = floatval($order_info['shipping_fee']) > 0 && $state;
               break;
			   
			//调整商品费用
        	case 'spay_price':
        	    $state = ($order_info['order_state'] == ORDER_STATE_NEW) ||
        	       ($order_info['payment_code'] == 'offline' && $order_info['order_state'] == ORDER_STATE_PAY);
				   $state = floatval($order_info['goods_amount']) > 0 && $state;
        	   break;

            //发货
            case 'store_send':
                $state = !$order_info['lock_state'] && $order_info['order_state'] == ORDER_STATE_PAY && !$order_info['chain_id'];
                break;

            //收货
            case 'receive':
                $state = !$order_info['lock_state'] && $order_info['order_state'] == ORDER_STATE_SEND;
                break;

            //门店自提完成
            case 'chain_receive':
                $state = !$order_info['lock_state'] && in_array($order_info['order_state'],array(ORDER_STATE_NEW,ORDER_STATE_PAY)) && 
                $order_info['chain_code'];
                break;

            //评价
            case 'evaluation':
                $state = !$order_info['lock_state'] && !$order_info['evaluation_state'] && $order_info['order_state'] == ORDER_STATE_SUCCESS;
                break;

            case 'evaluation_again':
                $state = !$order_info['lock_state'] && $order_info['evaluation_state'] && !$order_info['evaluation_again_state'] && $order_info['order_state'] == ORDER_STATE_SUCCESS;
                break;

            //锁定
            case 'lock':
                $state = intval($order_info['lock_state']) ? true : false;
                break;

            //快递跟踪
            case 'deliver':
                $state = !empty($order_info['shipping_code']) && in_array($order_info['order_state'],array(ORDER_STATE_SEND,ORDER_STATE_SUCCESS));
                break;

            //放入回收站
            case 'delete':
                $state = in_array($order_info['order_state'], array(ORDER_STATE_CANCEL,ORDER_STATE_SUCCESS)) && $order_info['delete_state'] == 0;
                break;

            //永久删除、从回收站还原
            case 'drop':
            case 'restore':
                $state = in_array($order_info['order_state'], array(ORDER_STATE_CANCEL,ORDER_STATE_SUCCESS)) && $order_info['delete_state'] == 1;
                break;

            //分享
            case 'share':
                $state = true;
                break;

        }
        return $state;

    }

    /**
     * 联查订单表订单商品表
     *
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     * @return array
     */
    public function getOrderAndOrderGoodsList($condition, $field = '*', $page = 0, $order = 'rec_id desc') {
        return $this->table('order_goods,orders')->join('inner')->on('order_goods.order_id=orders.order_id')->where($condition)->field($field)->page($page)->order($order)->select();
    }

    /**
     * 订单销售记录 订单状态为20、30、40时
     * @param unknown $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getOrderAndOrderGoodsSalesRecordList($condition, $field="*", $page = 0, $order = 'rec_id desc') {
        $condition['order_state'] = array('in', array(ORDER_STATE_PAY, ORDER_STATE_SEND, ORDER_STATE_SUCCESS));
        return $this->getOrderAndOrderGoodsList($condition, $field, $page, $order);
    }

    /**
     * 取得其它订单类型的信息
     * @param unknown $order_info
     */
    public function getOrderExtendInfo(& $order_info) {
        //取得预定订单数据
        if ($order_info['order_type'] == 2) {
            $result = Logic('order_book')->getOrderBookInfo($order_info);
            //如果是未支付尾款
            if ($result['data']['if_buyer_repay']) {
                $result['data']['order_pay_state'] = false;
            }
            $order_info = $result['data'];
        }
    }

    /**
     * 更改订单状态
     * @param $orderInfo
     * @param $updateData
     * @return mixed
     */
    public function updateOrderState($orderInfo,$updateData)
    {
        try{
            $update = $this->table('orders')->where(array('order_id'=>$orderInfo['order_id']))->update($updateData);
            if ($update){
                return ['success'=>true,'data'=>$update,'msg'=>'修改成功'];
            }
        }catch (Exception $e){
            return ['success'=>true,'data'=>'','msg'=>$e->getMessage()];
        }
    }
	
	
	/**
     * 取得骑手订单列表
     *
     * @param int $waiter_id 骑手编号
     * @param string $order_sn 订单sn
     * @param string $buyer_name 买家名称
     * @param string $state_type 订单状态
     * @param string $query_start_date 搜索订单起始时间
     * @param string $query_end_date 搜索订单结束时间
     * @param string $skip_off 跳过已关闭订单
     * @return array $order_list
     */
    public function getWaiterOrderList($waiter_id, $order_sn, $buyer_name, $state_type, $query_start_date, $query_end_date, $skip_off, $fields = '*',$start_income,$end_income,$extend = array()) {
        $condition = array();
        $condition['distributor_id'] = $waiter_id;
        if (preg_match('/^\d{10,20}$/',$order_sn)) {
            $condition['order_sn'] = $order_sn;
        }
        if ($buyer_name != '') {
            $condition['buyer_name|store_name|store_phone|store_address|buyer_name|buyer_phone|buyer_address|order_sn|order_comment'] = array('like','%'.$buyer_name.'%');
        }
        
      	if($state_type == 1){
			$condition['order_state'] =  array('in','20,30,40,50');
		}
		if($state_type == 2){
			
			$condition['order_state'] =  array('in','60');
		}
		
		if($state_type == 3){
			
			$condition['order_state'] =  array('in','0,70');
		}
		
		
	
        $if_start_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_start_date);
        $if_end_date = preg_match('/^20\d{2}-\d{2}-\d{2}$/',$query_end_date);
        $start_unixtime = $if_start_date ? strtotime($query_start_date) : null;
        $end_unixtime = $if_end_date ? strtotime($query_end_date): null;
      
        if ($start_unixtime || $end_unixtime) {
            $condition['add_time'] = array('time',array($start_unixtime,$end_unixtime));
        }
		

	    $price_from = preg_match('/^[\d.]{1,20}$/',$start_income) ?$start_income : null;
        $price_to = preg_match('/^[\d.]{1,20}$/',$end_income) ? $end_income : null;
		
        if ($price_from && $price_to) {
            $condition['delivery_fee'] = array('between',"{$price_from},{$price_to}");
        } elseif ($price_from) {
            $condition['delivery_fee'] = array('egt',$price_from);
        } elseif ($price_to) {
            $condition['delivery_fee'] = array('elt',$price_to);
        }

//	print_r($condition);
		
        $order_list = $this->getOrderList($condition, 20, $fields, 'order_id desc','', $extend);		
		$region = $this->region();
        //页面中显示那些操作
        $order = array();
        foreach ($order_list as $key => $order_info) {			         
            
			$order_info['finnshed_time'] = date('d/m H:i:s',$order_info['finnshed_time']);
			$order_info['add_time'] = date('d/m H:i',$order_info['add_time']);	
		    if($order_info['order_state'] == 0){
			    $order_info['cancel_reason'] = '客户取消';
		    }
			
			$estimate_time =  $order_info['order_estimate_time'] - time();
			if($estimate_time > 0 ){
			    $order_estimate_time = gmstrftime("%M'%S\"", $estimate_time);
			}else{
			    $order_estimate_time = '已超时';
			}
			
			$order_info['order_estimate_time'] = $order_estimate_time;
			
			
			
			$order[] = $order_info;
			
			
        }

	
        return $order;
    }

   private function region(){		
		$list = model('region')->getRegionList(True);
		$data= array();
		foreach($list as $v){
			$data[$v['region_id']] = $v;
		}		
		return $data;
		
	}
	
	
	
	
	
	

}
