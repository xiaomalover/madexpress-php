<?php
/**
 * 用户消息模板模型
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class delivery_msg_tplModel extends Model{
    public function __construct() {
        parent::__construct('delivery_msg_tpl');
    }

    /**
     * 用户消息模板列表
     * @param array $condition
     * @param string $field
     * @param number $page
     * @param string $order
     */
    public function getDeliveryMsgTplList($condition, $field = '*', $page = 0, $order = 'dmt_code asc') {
        return $this->field($field)->where($condition)->order($order)->page($page)->select();
    }

    /**
     * 用户消息模板详细信息
     * @param array $condition
     * @param string $field
     */
    public function getDeliveryMsgTplInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }

    /**
     * 编辑用户消息模板
     * @param array $condition
     * @param unknown $update
     */
    public function editDeliveryMsgTpl($condition, $update) {
        return $this->where($condition)->update($update);
    }
}
