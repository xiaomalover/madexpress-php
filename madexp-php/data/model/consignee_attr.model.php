<?php
/**
 * 收货人属性
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class consignee_attrModel extends Model {

    public function __construct() {
        parent::__construct('consignee_attr');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getConsigneeAttrInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getConsigneeAttrList($condition, $order = 'attr_id asc'){
        $attr_list = $this->where($condition)->order($order)->select();
       return $attr_list;
    }



    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addConsigneeAttr($param){
        return $this->insert($param);
    }
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editConsigneeAttr($update, $condition){
        return $this->where($condition)->update($update);
    }
    

    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delConsigneeAttr($condition){
        return $this->where($condition)->delete();
    }
}
