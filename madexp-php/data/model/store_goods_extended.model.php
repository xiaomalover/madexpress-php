<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class store_goods_extendedModel extends Model {
    public function __construct() {
        parent::__construct('store_goods_extended');
    }

    /**
     * 添加GoodsExt
     * @param array $insert
     * @return boolean
     */
    public function addGoodsExt($insert) {
        return $this->insert($insert);
    }
    public function addGoodsExtAll($insert) {
        return $this->insertAll($insert);
    }


    /**
     * 编辑GoodsExt
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editGoodsExt($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除GoodsExt
     * @param unknown $condition
     * @return boolean
     */
    public function delGoodsExt($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询GoodsExt数量
     * @param array $condition
     * @return array
     */
    public function getGoodsExtCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * GoodsExt列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getGoodsExtList($condition, $field = '*', $page = 0, $order = 'id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个GoodsExt内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsExtInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
