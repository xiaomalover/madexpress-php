<?php
/**
 * cms文章分类模型
*
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class notice_article_classModel extends Model{

    public function __construct(){
        parent::__construct('notice_article_class');
    }

     /**
     * 添加品牌
     * @param array $insert
     * @return boolean
     */
    public function addArticleClass($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑品牌
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editArticleClass($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除品牌
     * @param unknown $condition
     * @return boolean
     */
    public function delArticleClass($condition) {
 
	    return $this->where($condition)->delete();
    }

    /**
     * 查询品牌数量
     * @param array $condition
     * @return array
     */
    public function getArticleClassCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * 品牌列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getArticleClassList($condition, $field = '*', $page = 0, $order = 'class_sort asc, class_id desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }



    /**
     * 取单个品牌内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getArticleClassInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
