<?php
/**
 * 合同模型

 */
defined('InMadExpress') or exit('Access Invalid!');

class store_goods_xlsModel extends Model {
    public function __construct() {
        parent::__construct('store_goods_xls');
    }

    /**
     * 添加GoodsXls
     * @param array $insert
     * @return boolean
     */
    public function addGoodsXls($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑GoodsXls
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editGoodsXls($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除GoodsXls
     * @param unknown $condition
     * @return boolean
     */
    public function delGoodsXls($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询GoodsXls数量
     * @param array $condition
     * @return array
     */
    public function getGoodsXlsCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * GoodsXls列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getGoodsXlsList($condition, $field = '*', $page = 0, $order = 'fid desc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个GoodsXls内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getGoodsXlsInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
