<?php
/**
 * 当前商户的剩余的餐盒量
 */
defined('InMadExpress') or exit('Access Invalid!');

class store_goods_sizeModel extends Model {
    public function __construct() {
        parent::__construct('goods_size');
    }

    /**
    
     * @param array $insert
     * @return boolean
     */
    public function addSize($insert) {
        return $this->insert($insert);
    }

    /**
     
     * @param array $condition
     * @param array $update 
     * @return boolean
     */
    public function editSize($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
    
     * @param unknown $condition
     * @return boolean
     */
    public function delSize($condition) {       
        return $this->where($condition)->delete();
    }

    /**
     
     * @param array $condition
     * @return array
     */
    public function getSizeCount($condition) {
        return $this->where($condition)->count();
    }

    /**

     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getSizeList($condition, $field = '*', $page = 0, $order = 'id asc',  $group='' ,$limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->group($group)->limit($limit)->select();
    }
	
	
    /**
   
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getSizeInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
	
	
}
