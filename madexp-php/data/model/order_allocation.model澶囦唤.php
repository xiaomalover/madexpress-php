<?php
/**
 * 
 *订单分配核心
 *
 */
defined('InMadExpress') or exit('Access Invalid!');
class order_allocationModel extends Model{
   
   
   
   
   //获取商家周边范围的可用配送员
   public function getStoreDeliver($store){
   	
		$where = array();
   		$where['distributor_status'] = 1;			
   		$where['distributor_online'] = 20;			
   		$where['delivery_state'] = 0;
   		
   		$deliver_data = model('waiter')->getWaiterList($where);
   		$deliver = array();
   		$count = 0;
   		foreach($deliver_data as $k =>$v){
   		
   			$deliver_coordinate = explode(',',$v['region_coordinate']);
   			
   			$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
   			
   			
   			//$deliver_sort = $this->deliverySort($v['distributor_status']);
   			//顶级排序
   			$deliver_sort = $this->deliverySort($v['distributor_id'],$store['store_id']);
   			
   			//计算当前店铺是不是骑手的叠加单店铺。
   			$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$store['store_id']);
   			
   			
   			if($superposition == 1 ){						
   					
   					$deliver[$k]['name'] = $v['distributor_name'];	
   					$deliver[$k]['distance'] = $distance;
   					$deliver[$k]['sort'] = $deliver_sort;
   					$deliver[$k]['distributor_id'] = $v['distributor_id'];
   					$deliver[$k]['superposition'] = $superposition;
   					
   					$count += $this->getDeliveryCount($v['distributor_id']);
   					
   					
   			}else if($distance < 5000 &&  $superposition == 0 ){
   			
   					$deliver[$k]['name'] = $v['distributor_name'];	
   					$deliver[$k]['distance'] = $distance;
   					$deliver[$k]['sort'] = $deliver_sort;
   					$deliver[$k]['distributor_id'] = $v['distributor_id'];	
   					$deliver[$k]['superposition'] = $superposition;
   					$count += $this->getDeliveryCount($v['distributor_id']);
   					
   			}
   			
   		}
   		
   		
   		return array('deliver'=>$deliver,'count' => $count);	
   
   }
   
   
   /*
   计算距离
   */
   
   public function getDistance($lat1, $lng1, $lat2, $lng2) {
     
       $earthRadius = 6367000; //approximate radius of earth in meters
   
       /*
        Convert these degrees to radians
       to work with the formula
       */
   
       $lat1 = ($lat1 * pi() ) / 180;
       $lng1 = ($lng1 * pi() ) / 180;
   
       $lat2 = ($lat2 * pi() ) / 180;
       $lng2 = ($lng2 * pi() ) / 180;
   
       /*
        Using the
       Haversine formula
   
       http://en.wikipedia.org/wiki/Haversine_formula
   
       calculate the distance
       */
   
       $calcLongitude = $lng2 - $lng1;
       $calcLatitude = $lat2 - $lat1;
       $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
       $calculatedDistance = $earthRadius * $stepTwo;
   
       return round($calculatedDistance);
   }
   
   
   
   
   /*
   配送员排序条件
   配送ID
   商铺ID
   */   
   private function deliverySort($deliver_id,$store_id){		
   
	//有两单
   	$count = $this->deliverOrderCount($deliver_id);
   	
   	switch ($count) {		
   	   case 2:    			
   			$where = array(
   				'distributor_id' => $deliver_id,		
   				'store_id' => $store_id,			
   				'order_state' => array('in','30,40')				
   			);   			
   			$count = model('order')->getOrderCount($where);   			
   			if($count  == 2){   			
   				return  1 ;	   			
   			}elseif($count == 1){   			
   				return  2 ;   			
   			}elseif($count == 0){   			
   				return  4;
   			}					
   		 break;
   	   case 1:		   
   			$where = array(
   				'distributor_id' => $deliver_id,	
   				'store_id' => $store_id,						
   				'order_state' => array('in','30,40')				
   			);
   			$count = model('order')->getOrderCount($where);
   			//有一单
   			if($count  == 1){
   				return 3;					
   			}elseif($count == 0){					
   				return 5;
   			}			
   		 break;			 
   	   case 0:			   	   
   			return 6;									
   	   break;		  
   	}
   }
   
   
   
   /**
	* @param {Object} $deliver_id
	* @param {Object} $store_id
		获取配送员可接的叠加商户
	*/
   private function getOrderSuperpositionStore($deliver_id,$store_id){   	
		$count = 0;   	
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);   	
		$order = model('order')->getOrderInfoN($where,'store_id,store_superpose','distributor_start_time desc');
		if($order['store_id'] == $store_id){					
			return 1; //店铺本身.
		}   	
		if($order['store_superpose'] == $store_id){
			return 1;
		}
		//print_r($order);
		if(!empty($order)){		
			//直接读取当前商户是否可以叠加其他的店铺
			$store = explode(',',$order['store_superpose']);
			if(in_array($store_id,$store)){  			
				return 1;				
			}else{			
				return 2;				
			}						
		}else{		
			return 0; //没有单子
		}
   	
   }
   
   /**
	* 计算运力
	* @param {Object} $deliver_id
	*/
   public function getDeliveryCount($deliver_id){
		$count = 3;				
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40,50')
		);		
		$order_count = model('order')->getOrderCount($where);
		return $count - $order_count;	
   }
   
   
   /**
	* @param {Object} $id
	*/
   private function deliverOrderCount($id){
       $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40,50')
		);
   	$count = model('order')->getOrderCount($where);
   	return $count;
   }
   
   
   
   
	//推送订单
	private function stocketSend($order_sn = '',$is_new = 0){	
		
		    require_once(BASE_CORE_PATH.'/framework/libraries/gateway.php');
    		$gateway = new \GatewayClient\Gateway();
    		$gateway::$registerAddress = '150.158.195.68:1238';	
    		
    
				$where = array(
					'order_sn'=>$order_sn,
					'state' => 0,
				//	'send_state' => 0 
				);			
				
				$polling = model('order_polling')->getPollingInfo($where,'*');
				
				$where = array(
						'waiter_id' => $polling['waiter_id'],
						'order_sn' => $polling['order_sn']
				);	
				
				$data = array(
						'send_state' =>  1
				);	
				
				model('order_polling')->editPolling($data,$where);	
				
				if($is_new == 1){	
					
					
				
					$orders = model('order')->getOrderInfoN(array('order_sn' => $order_sn));
					$store = $this->getStore($orders['store_id']);
					$data = array();
                	$data['order_sn'] = $order_sn;
                	$data['type'] = 'rob';
                
					    
					
						$buyer_info = array();
                		$buyer_info['buyer_id'] = $orders['buyer_id'];
                		$buyer_info['buyer_code'] =$orders['buyer_code'];
                		$buyer_info['buyer_name'] = $orders['buyer_name'];
                		$buyer_info['buyer_phone'] = $orders['buyer_phone'];
                		$buyer_info['buyer_address'] = $orders['buyer_address'];
                		$buyer_info['buyer_region_id'] = $orders['buyer_region_id'];
                		$buyer_info['buyer_is_new'] = $orders['buyer_is_new'];
                		$buyer_info['buyer_coordinate'] = $orders['buyer_coordinate'];
                		$buyer_info['buyer_region'] =$orders['buyer_region'];		
                		$buyer_info['buyer_region_color'] = $orders['buyer_region_color'];
                		$buyer_info['buyer_status_code'] = $orders['buyer_status_code'];
    		 
		 
		                $order_info['buyer'] = $buyer_info;
		 
	    
            	      	$store_info = array();
                		$store_info['store_id'] = $store['store_id'];
                		$store_info['store_name_primary'] = $store['store_name_primary'];
                		$store_info['store_name_secondary'] = $store['store_name_secondary'];
                		$store_info['store_code'] = $store['store_code'];
                		$store_info['store_address'] = $store['store_address'];
                		$store_info['store_lat'] = $store['store_lat'];
                		$store_info['store_lng'] = $store['store_lng'];
                		$store_info['region_name'] = $store['region_name'];
                		$store_info['region_color'] = $store['region_color'];
                		$store_info['store_phone'] = $store['store_phone'];
                		$store_info['store_status_code'] = $orders['store_status_code'];
		    
		                $order_info['store'] =$store_info;
		
		    
            		    $order = array();
            		    $order['order_sn'] = $orders['order_sn'];
                		$order['delivery_time'] = $orders['delivery_time'];
                		$order['delivery_type'] = $orders['delivery_type'];
                		$order['order_state'] = $orders['order_state'];
                		$order['delivery_fee'] = $orders['delivery_fee'];
                		$order['delivery_comment'] = $orders['deliver_comment'];
                		
                		$time = time();
                		if($time > $orders['order_estimate_time']){
                		    $estimate_time = 0;
                		}else{
                		    $estimate_time = $orders['order_estimate_time'] - time();
                		}
                		
                		
                		$order['order_estimate_time'] = $estimate_time;
                	 //   $order['delivery_state'] = $this->waiter_info['delivery_state'];
            		    $order['order_id'] = $orders['order_id'];
            		    $order_info['order'] = $order;
		
				    	$data['order_info'] = $order_info;
					
					    
					    $where = array(
							'order_id' => $polling['order_id']
						);
					
						model('order')->editOrder(array('socket_send_time' => time()),$where);
					
					
						$data['closetime'] =  (time() + 30) - time();
                	
    					$data = json_encode($data);	
    					$gateway::sendToUid($polling['waiter_id'],$data);
        		
    					
    					$this->stocketShow($polling);
					
				}
	}
   
   
   //商家
	private function getStore($store_id){
		return  model('store')->getStoreInfo(array('store_id' => $store_id));
	}
	
	
	//展示中的订单
    private function stocketShow($polling){
	
				
					//如果正在展示的订单就不写入到展示页面里去
					$count = model('order_polling_show')->getShowCount(array('waiter_id'=> $polling['waiter_id']));
				
					if($count > 0){
						
						$where = array(
							'waiter_id' => $polling['waiter_id'],
							'order_sn' => $polling['order_sn']
						);	
						$data = array(
							'send_state' =>  1
						);									
						model('order_polling')->editPolling($data,$where);	
						
						
					}else{		
					
					
						$data = array();
						$data['order_id'] 	= $polling['order_id'];
						$data['order_sn'] 	= $polling['order_sn'];
						$data['waiter_id'] 	= $polling['waiter_id'];
						$data['addtime'] 	= $polling['addtime'];
						$data['store_name'] = $polling['store_name'];								
						model('order_polling_show')->addShow($data);
						
						$where = array(
							'waiter_id' => $polling['waiter_id'],
							'order_sn' => $polling['order_sn']
						);	
						
														
						$data = array(
							'send_state' =>  1
						);			
						
						model('order_polling')->editPolling($data,$where);	
						
						
						$where = array(
							'order_id' => $polling['order_id']
						);
						$data = array(
							'order_polling_show' => 1
						);
						model('order')->editOrder($data,$where);
					}
	}
	
	
   
}