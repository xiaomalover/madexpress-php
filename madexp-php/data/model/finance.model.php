<?php
/**
 * 账单项
 *

 */
defined('InMadExpress') or exit('Access Invalid!');
class financeModel extends Model {

    public function __construct() {
        parent::__construct('finance');
    }


    /**
     * 取得单条地址信息
     * @param array $condition
     * @param string $order
     */
    public function getFinanceInfo($condition, $order = '') {
        $addr_info = $this->where($condition)->order($order)->find();        
        return $addr_info;
    }

    /**
     * 读取地址列表
     *
     * @param
     * @return array 数组格式的返回结果
     */
    public function getFinanceList($condition, $order = 'finance_id asc'){
        $address_list = $this->where($condition)->order($order)->select();
       return $address_list;
    }

    /**
     * 取数量
     * @param unknown $condition
     */
    public function getFinanceCount($condition = array()) {
        return $this->where($condition)->count();
    }

   
    /**
     * 新增地址
     *
     * @param array $param 参数内容
     * @return bool 布尔类型的返回结果
     */
    public function addFinance($param){
        return $this->insert($param);
    }
   
    /**
     * 更新地址信息
     *
     * @param array $param 更新数据
     * @return bool 布尔类型的返回结果
     */
    public function editFinance($update, $condition){
        return $this->where($condition)->update($update);
    }
    
    /**
     * 删除地址
     *
     * @param int $id 记录ID
     * @return bool 布尔类型的返回结果
     */
    public function delFinance($condition){
        return $this->where($condition)->delete();
    }
}
