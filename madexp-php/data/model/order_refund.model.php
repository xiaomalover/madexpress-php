<?php
/**
 * 订单退款详单

 */
defined('InMadExpress') or exit('Access Invalid!');

class order_refundModel extends Model {
    public function __construct() {
        parent::__construct('order_refund');
    }

    /**
     * 添加Refund
     * @param array $insert
     * @return boolean
     */
    public function addRefund($insert) {
        return $this->insert($insert);
    }

    /**
     * 编辑Refund
     * @param array $condition
     * @param array $update
     * @return boolean
     */
    public function editRefund($condition, $update) {
        return $this->where($condition)->update($update);
    }

    /**
     * 删除Refund
     * @param unknown $condition
     * @return boolean
     */
    public function delRefund($condition) {        
        return $this->where($condition)->delete();
    }

    /**
     * 查询Refund数量
     * @param array $condition
     * @return array
     */
    public function getRefundCount($condition) {
        return $this->where($condition)->count();
    }

    /**
     * Refund列表
     * @param array $condition
     * @param string $field
     * @param string $order
     * @param number $page
     * @param string $limit
     * @return array
     */
    public function getRefundList($condition, $field = '*', $page = 0, $order = 'refund_id asc', $limit = '') {
        return $this->where($condition)->field($field)->order($order)->page($page)->limit($limit)->select();
    }




    /**
     * 取单个Refund内容
     * @param array $condition
     * @param string $field
     * @return array
     */
    public function getRefundInfo($condition, $field = '*') {
        return $this->field($field)->where($condition)->find();
    }
}
