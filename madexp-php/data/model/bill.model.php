<?php
/**
 * 票据模型
 *
 */
defined('InMadExpress') or exit('Access Invalid!');

class billModel extends Model {

	  public function __construct() {
		  parent::__construct('bill');
	  }
  

    /**
     * 列表
     * @param unknown $condition
     * @param string $fields
     * @param string $pagesize
     * @param string $order
     * @param string $limit
     */
    public function getBillList($condition = array(), $fields = '*', $pagesize = null, $order = 'bill_id desc', $limit = null) {
        return $this->where($condition)->field($fields)->order($order)->limit($limit)->page($pagesize)->select();
    }

    /**
     * 单条
     * @param unknown $condition
     * @param string $fields
     */
    public function getBillInfo($condition = array(), $fields = '*') {
        return $this->where($condition)->field($fields)->find();
    }

    /**
     * 取得数量
     * @param unknown $condition
     */
    public function getBillCount($condition) {
        return $this->where($condition)->count();
    }


    public function addBill($data) {
		
		
		
		
		
		
		
        return $this->insert($data);
    }

    public function editBill($data, $condition = array()) {
        return $this->where($condition)->update($data);
    }
	
	/**
	 * 删除Workhours
	 * @param unknown $condition
	 * @return boolean
	 */
	public function delBill($condition) {        
	    return $this->where($condition)->delete();
	}
	
}
