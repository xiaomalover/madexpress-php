<?php
/**
 * 送餐员模型

 */
defined('InMadExpress') or exit('Access Invalid!');
class vehicle_classModel extends Model {

    public function __construct(){
        parent::__construct('vehicle_class');
    }

    public function vehicleClassList($condition=array(), $field = '*', $master = false) {
        return $this->field($field)->where($condition)->master($master)->select();
    }

	
	
	public function vehicleClassInfo($where,$field='*'){		
		return $this->field($field)->where($where)->find();
	}
	
}
