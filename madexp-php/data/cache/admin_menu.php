<?php defined('InMadExpress') or exit('Access Invalid!'); return array (
  'shop' => 
  array (
    'name' => '商户',
    'child' => 
    array (
      0 => 
      array (
        'name' => '设置',
        'child' => 
        array (
          'stat_general' => '运营数据',
          'order' => '订单管理',
          'sorting' => '分单管理',
          'store' => '商家管理',
          'waiter' => '送餐员管理',
          'member' => '用户管理',
          'depot' => '库存管理',
          'vehicle' => '车辆管理',
          'repair' => '汽修厂管理',
          'repair_vehicle' => '车辆管理',
          'repair_event' => '事件管理',
          'sales' => '钱葬场',
          'memo' => '备忘录',
          'interface' => '界面管理',
          'scheduling' => '排班管理',
          'feedback' => '客服',
          'merchant' => '商户专员',
          'finance' => '财务管理',
          'message' => '消息管理',
          'websystem' => '系统设置',
          'logout' => '登出',
        ),
      ),
    ),
  ),
);