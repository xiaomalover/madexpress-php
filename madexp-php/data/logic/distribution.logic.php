<?php 
//分单核心
defined('InMadExpress') or exit('Access Invalid!');
class distributionLogic {

    public function __construct() {
        Language::read('member_store_goods_index');
    }

	
	//获取商家周边范围的可用配送员
	public function getStoreDeliver($store){
			$where = array();
			$where['distributor_status'] = 1;			
			$where['distributor_online'] = 20;			
			$where['delivery_state'] = 0;
			
			$deliver_data = model('waiter')->getWaiterList($where);
			$deliver = array();
			$count = 0;
			foreach($deliver_data as $k =>$v){
			
				$deliver_coordinate = explode(',',$v['region_coordinate']);
				
				$distance = $this->getDistance($deliver_coordinate[1], $deliver_coordinate[0], $store['store_lat'], $store['store_lng']);
				
				
				//$deliver_sort = $this->deliverySort($v['distributor_status']);
				//顶级排序
				$deliver_sort = $this->deliverySort($v['distributor_id'],$store['store_id']);
				
				//计算当前店铺是不是骑手的叠加单店铺。
				$superposition = $this->getOrderSuperpositionStore($v['distributor_id'],$store['store_id']);
				
				
				if($superposition == 1 ){						
						
						$deliver[$k]['name'] = $v['distributor_name'];	
						$deliver[$k]['distance'] = $distance;
						$deliver[$k]['sort'] = $deliver_sort;
						$deliver[$k]['distributor_id'] = $v['distributor_id'];
						$count += $this->getDeliveryCount($v['distributor_id']);
						
						
				}else if($distance < 5000 &&  $superposition == 0 ){
				
						$deliver[$k]['name'] = $v['distributor_name'];	
						$deliver[$k]['distance'] = $distance;
						$deliver[$k]['sort'] = $deliver_sort;
						$deliver[$k]['distributor_id'] = $v['distributor_id'];	
						$count += $this->getDeliveryCount($v['distributor_id']);
				}
			}
			return array('deliver'=>$deliver,'count' => $count);	
	
	}
	
	
	
	
	private function getDistance($lat1, $lng1, $lat2, $lng2) {
      
	    $earthRadius = 6367000; //approximate radius of earth in meters
    
        /*
         Convert these degrees to radians
        to work with the formula
        */
    
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
    
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
    
        /*
         Using the
        Haversine formula
    
        http://en.wikipedia.org/wiki/Haversine_formula
    
        calculate the distance
        */
    
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);  $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
    
        return round($calculatedDistance);
    }

	
		//配送员排序条件
	
	
	private function deliverySort($deliver_id,$store_id){		
		//有两单
		$count = $this->deliverOrderCount($deliver_id);
		
		switch ($count) {		
		   case 2: 
				
				$where = array(
					'distributor_id' => $deliver_id,		
					'store_id' => $store_id,			
					'order_state' => array('in','30,40')				
				);
				
				$count = model('order')->getOrderCount($where);
				
				if($count  == 2){
				
					return  1 ;	
				
				}elseif($count == 1){
				
					return  2 ;
				
				}elseif($count == 0){
				
					return  4;
				
				}					
			 break;
		   case 1:		   
				$where = array(
					'distributor_id' => $deliver_id,	
					'store_id' => $store_id,						
					'order_state' => array('in','30,40')				
				);
				$count = model('order')->getOrderCount($where);
				//有一单
				if($count  == 1){
					return 3;					
				}elseif($count == 0){					
					return 5;
				}			
			 break;			 
		   case 0:			   	   
				return 6;									
		   break;		  
		}
	}
	
	
	
	//获取配送员可接的叠加商户
	private function getOrderSuperpositionStore($deliver_id,$store_id){
		
		$count = 0;
		
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);
		
		$order = model('order')->getOrderInfoN($where,'store_id,store_superpose','distributor_start_time desc');		

		if($order['store_id'] == $store_id){					
			return 1; //店铺本身.
		}
		
		if($order['store_superpose'] == $store_id){
			return 1;
		}
		
		//print_r($order);
		if(!empty($order)){		
			//直接读取当前商户是否可以叠加其他的店铺
			$store = explode(',',$order['store_superpose']);
			if(in_array($store_id,$store)){ 
				return 1;
			}else{			
				return 2;
				
			}
		}else{
		
			return 0; //没有单子
		}
		
	}
	

		//计算运力
	public function getDeliveryCount($deliver_id){

		$count = 3;				
		$where = array(
			'distributor_id' => $deliver_id,
			'order_state' => array('in','30,40')
		);		
		$order_count = model('order')->getOrderCount($where);
		
		
		return $count - $order_count;
	
	}
	
	private function deliverOrderCount($id){		
	    $count = 0;
		$where = array(
			'distributor_id' =>$id,
			'order_state' => array('in','30,40')
		);
		$count = model('order')->getOrderCount($where);
		return $count;
	}
	
		


}

?>