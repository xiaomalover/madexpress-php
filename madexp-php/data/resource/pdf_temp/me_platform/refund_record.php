<?php defined('InMadExpress') or exit('Access Invalid!');?>
<style>
	.pager {
		background: #fff;
		margin: 0 auto;
		font-size: 12px;
		
	}

	.font-10{
		font-size: 10px;
	}
	.font-11{
		font-size: 11px;
	}
	
	.font-16{
		font-size: 16px;
	}
	
	.total_amount{
		font-size: 60px;
		font-weight: bold;
	}
</style>

  <div class="pager"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr>
       <td align="left" valign="top" >
		 <img src="http://test-api.mad-express.com/data/resource/tcppdf/images/refund_record_01.png" alt="test alt attribute" width="500" border="0" />
	   </td>		
      </tr>	 
	   
	   <tr>
	   		  <td class="font-14">
	   			
	   		  </td>
	   </tr>	  
	  <tr>
		  <td class="font-16">
			#<?php echo $data['refund_record_sn'] ?>
		  </td>
	  </tr>	  
	  <tr>
	  		  <td class="font-14">
	  			
	  		  </td>
	  </tr>	  
	  <tr>
	  		  <td class="font-14">
	  		
	  		  </td>
	  </tr>	  
     </tbody>
    </table>  
 <table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tbody>
   <tr>
	<td>
		<table>
			<tr>
				<td width="80%">
					<div class="total_amount">$<?php echo $data['refund_amount']; ?></div>
				</td>
				<td>
					
				</td>
			</tr>
			<tr class="font-10">
				<td >
					Refund back to external payment method
				</td>
				<td>
					$<?php echo $data['refund_payment_amount']; ?>
				</td>
			</tr>
			<tr class="font-10">
				<td >
					Refund back to ME wallet credit
				</td>
				<td>
					$<?php echo $data['refund_wallet_amount']; ?>
				</td>
			</tr>
			<tr class="font-10">
				<td >
					Voucher Re-Issused					
				</td>
				<td>
					<?php echo $data['voucher_re']; ?>
				</td>
			</tr>
		</table>		
		<table>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td >* Full refund is transfered back to your original payment method(s)</td>
			</tr>
			<tr>
				<td >* Used voucher(s) will be restore/re-issue to your account</td>
			</tr>
		</table>
	</td>
    <td align="right" valign="top" >
 	 <img src="http://test-api.mad-express.com/data/resource/tcppdf/images/refund_record_02.png" alt="test alt attribute" width="250" border="0" />
    </td>
 	
   </tr>
   
  </tbody>
 </table>
   
  
    
   
   

   




  </div>
