<?php defined('InMadExpress') or exit('Access Invalid!');?>
<style>
	.pager {
		background: #fff;
		margin: 0 auto;
		font-size: 13px;
		
	}

	.text-bold {
		font-weight: bold
	}

	.text-center {
		text-align: center
	}

	.text-left {
		text-align: left
	}

	.text-right {
		text-align: right
	}

	.header {
		padding: 0 50px
	}

	.title {
		
		font-size: 33px;
		color: #000;
		
	}

	.user {
		
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		padding: 0 50px
	}

	.user .customer {
		margin-top: 60px
	}

	.user .date {
		margin-top: 30px
	}

	.goods {
	
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		line-height: 30px;
		padding: 0 50px;
		margin-top: 60px;
		border-bottom: 1px solid #000;
	}

	.goods-header {
		margin-top: 50px
		
	}

	.goods-header td {
		border-bottom: 1px solid #000;
		line-height:30px;
		
	}
	.goods-list td{
		line-height: 40px;
	
	
	}
	.small {
		font-family: ArialMT;
		font-size: 10px;
		color: #000;
		line-height: 20px;
	}

	.subtotal {
		
		font-family: ArialMT;
		font-size: 13px;
		color: #000;		
		margin: 0 50px;
		line-height: 20px;
		height: 20px;
		border-top: 1px solid #000;
	}

	.param {
		margin: 0 50px;
		padding: 0px;
		font-size: 13px;
		color: #000;
		
		border-bottom: 1px solid #000;
	}
	.total-x{
		border-top: 1px solid #000;
	}
	.total {
		margin: 0 50px;
		font-family: Arial-BoldMT;
		font-size: 16px;
		color: #000
	}

	.total-tap {
		font-family: Arial-ItalicMT;
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		line-height: 20px
	}

	.footer {
		font-family: ArialMT;
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
	}
	.hr{
		
	}
	
	.total_amount{
		font-size: 16px;;
	}
</style>

  <div class="pager"> 

    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr>
       <td align="left" valign="top" class="title text-bold">Tax Invoice</td>
       <td valign="top" align="right"><img src="http://test-api.mad-express.com/data/resource/tcppdf/images/logo1.png" alt="test alt attribute" width="150" border="0" /></td>
      </tr>
     </tbody>
    </table>
  
 
    
   
   <table  width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
		   <td>&nbsp;</td>
		   <td>&nbsp;</td>
	   </tr>
	   <tr>
	   		   <td>&nbsp;</td>
	   		   <td>&nbsp;</td>
	   </tr>
  <tr>
    <td align="left" valign="top"><table  width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td><?php echo  $data['store']['store_name'];?></td>
        </tr>
      <tr>
        <td><?php echo  $data['store']['store_company_name'];?></td>
      </tr>
      <tr>
        <td><?php echo  $data['store']['store_address'];?></td>
      </tr>
      <tr>
        <td>ABN: <?php echo  $data['store']['store_abn'];?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>To</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>ME Courier ID: <?php echo $data['delivery']['delivery_code']; ?></td>
      </tr>
      <tr>
        <td><?php echo $data['delivery']['delivery_truename']; ?></td>
      </tr>
      <tr>
        <td><?php echo $data['delivery']['delivery_address'] ;?></td>
      </tr>
      <tr>
        <td>ABN: <?php echo $data['delivery']['delivery_abn'] ;?></td>
      </tr>
    </table></td>
    <td align="right" valign="top"><table  width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td align="right">Invoice Number</td>
      </tr>
      <tr>
        <td align="right"><?php echo $data['order']['order_sn'].$data['role']; ?></td>
      </tr>
      <tr>
        <td align="right">&nbsp;</td>
      </tr>
      <tr>
        <td align="right">Date: <?php echo $data['order']['eng_date']; ?></td>
      </tr>
    </table></td>
  </tr>
</table>

  

    <table  border="0" cellpadding="0" cellspacing="0">
     <tbody>
		<tr>
			<td></td>
			<td></td>
			<td></td>
		 <td></td>
		 <td align="right"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
		 <td></td>
		 <td align="right"></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
		 <td></td>
		 <td align="right"></td>
		</tr>
      <tr class="goods-header">
       <td width="4%" align="center" >Qty</td>
       <td width="60%" align="center">Items</td>	 
       <td width="12%" align="center">Unit Price</td>
       <td width="12%" align="center">Unit GST</td>
       <td width="12%" align="center">Amount</td>
      </tr> 

	  <?php foreach($data['order']['goods_list'] as $k => $v){  ?>
      <tr class="goods-list">
       <td width="4%" height="55"  align="center" valign="middle" ><?php echo $v['goods_num']; ?></td>
 		<td width="60%" align="center" valign="middle">
			<?php echo $v['goods_name']; ?>
			<div class="small"><?php echo $v['goods_specs']; ?></div>
		</td>	   
       <td width="12%" height="60" align="center" valign="middle">$<?php echo $v['unit_price']; ?></td>
       <td width="12%" align="center" valign="middle">$<?php echo $v['unit_gst']; ?></td>
       <td width="12%" align="center" valign="middle">$<?php echo $v['goods_price']; ?></td>
      </tr> 
	  <?php }  ?>
     </tbody>
    </table>
   

    <table border="0" cellpadding="0" cellspacing="0" class="subtotal"> 
     <tbody>
		 <tr>
		  <td width="70%"></td>
		  <td align="right" width="27%"></td>
		  <td width="3%"></td>
		 </tr>
      <tr>
       <td></td>
       <td align="right">Subtotal &nbsp;&nbsp;$<?php echo $data['order']['subtotal']; ?></td>
	   <td ></td>
      </tr> 
      <tr>
       <td></td>
       <td align="right"></td>
	    <td ></td>
      </tr>
      
	 
      <tr>
       <td>Payment TXN Fee</td>
       <td align="right">$<?php echo $data['order']['payment_txn_fee']; ?></td>
	      <td ></td>
      </tr> 
      
   
	  <tr>
	   <td></td>
	   <td align="right"></td>
	  </tr>
     </tbody>
    </table>
 
	
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="total-x">
     <tbody>
		 <tr class="">
			<td width="50%">&nbsp;</td>
			<td width="47%" align="right"></td>
		   <td width="3%"></td>
		 </tr>
      <tr>
       <td class="text-bold total_amount">Total Amount</td>
       <td align="right" class="text-bold total_amount">AUD $<?php 	 echo $data['order']['subtotal'] + $data['order']['payment_txn_fee']; ?></td>
		 <td ></td>
      </tr>
      <tr>
       <td style="font-size:14px"><i>*Total amount include GST</i></td>
       <td align="right"></td>
      </tr>
     </tbody>
    </table>

   <div style=""></div>
   <div class="footer">
	   <table>
	   	   <tr>
	   		   <td width="1%">*</td>
	   		   <td width="99%">Invoice Issued by <?php echo $data['store']['store_name']; ?>  (ABN <?php echo $data['store']['store_abn']; ?>) via the E-Invoice Generator developed by Mad Express Pty Ltd </td>
	   		   
	   	   </tr>
	   </table>
   
   </div>
  </div> 

							
			