<?php defined('InMadExpress') or exit('Access Invalid!'); ?>
<style>
	.pager {
		background: #fff;
		margin: 0 auto;
		font-size: 13px;

	}

	.text-bold {
		font-weight: bold
	}

	.text-center {
		text-align: center
	}

	.text-left {
		text-align: left
	}

	.text-right {
		text-align: right
	}

	.header {
		padding: 0 50px
	}

	.title {

		font-size: 33px;
		color: #000;

	}

	.user {

		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		padding: 0 50px
	}

	.user .customer {
		margin-top: 60px
	}

	.user .date {
		margin-top: 30px
	}

	.goods {

		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		line-height: 30px;
		padding: 0 50px;
		margin-top: 60px;
		border-bottom: 1px solid #000;
	}

	.goods-header {
		margin-top: 50px
	}

	.goods-header td {
		border-bottom: 1px solid #000;
		line-height: 30px;

	}

	.goods-list td {
		line-height: 40px;
	}

	.goods-name{
		line-height: 18px;
		margin: 0px;
		padding: 0px;
		
	}

	.small {
		font-family: ArialMT;
		font-size: 10px;
		color: #000;
		line-height: 20px;
	}

	.subtotal {

		font-family: ArialMT;
		font-size: 13px;
		color: #000;
		margin: 0 50px;
		line-height: 20px;
		height: 20px;
		border-top: 1px solid #000;
	}

	.param {
		margin: 0 50px;
		padding: 0px;
		font-size: 13px;
		color: #000;

		border-bottom: 1px solid #000;
	}

	.total-x {
		border-top: 1px solid #000;
	}

	.total {
		margin: 0 50px;
		font-family: Arial-BoldMT;
		font-size: 16px;
		color: #000
	}

	.total-tap {
		font-family: Arial-ItalicMT;
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
		line-height: 20px
	}

	.footer {
		font-family: ArialMT;
		font-size: 13px;
		color: #000;
		letter-spacing: 0;
	}

	.hr {}

	.total_amount {
		font-size: 16px;
		;
	}
</style>

<div class="pager">

	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td align="left" valign="top" class="title text-bold">Tax Invoice</td>
				<td valign="top" align="right"><img src="http://test-api.mad-express.com/data/resource/tcppdf/images/logo1.png" alt="test alt attribute" width="150" border="0" /></td>
			</tr>
		</tbody>
	</table>




	<table width="100%" border="0" cellpadding="0" cellspacing="0">

		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="left" valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><?php echo  $data['store']['store_name']; ?></td>
					</tr>
					<tr>
						<td><?php echo  $data['store']['store_company_name']; ?></td>
					</tr>
					<tr>
						<td><?php echo  $data['store']['store_address']; ?></td>
					</tr>
					<tr>
						<td>ABN: <?php echo  $data['store']['store_abn']; ?></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>To</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Mad Express Customer #<?php echo $data['user']['user_code'] ?></td>
					</tr>

				</table>
			</td>
			<td align="right" valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right">Invoice Number</td>
					</tr>
					<tr>
						<td align="right"><?php echo $data['order']['order_sn'] . $data['role']; ?></td>
					</tr>
					<tr>
						<td align="right">&nbsp;</td>
					</tr>
					<tr>
						<td align="right">Date: <?php echo $data['order']['eng_date']; ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>



	<table border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="right"></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td align="right"></td>
			</tr>

			<tr class="goods-header">
				<td width="4%" align="center">Qty</td>
				<td width="60%" align="center">Items</td>
				<td width="12%" align="center">Unit Price</td>
				<td width="12%" align="center">Unit GST</td>
				<td width="12%" align="center">Amount</td>
			</tr>

			<?php foreach ($data['order']['goods_list'] as $k => $v) {  ?>
				<tr class="goods-list">
					<td width="4%" align="center" valign="middle">
					<?php echo $v['goods_num']; ?>
					</td>
					<td width="60%" align="center" valign="middle">
						<div style="line-height: 18px;">
							<div class="goods-name"><?php echo $v['goods_name']; ?></div>
							<?php if (!empty($v['goods_specs'])) { ?>
							<div class="small"><?php echo $v['goods_specs']; ?></div>
							<?php } ?>
						</div>
					</td>
					<td width="12%" align="center" valign="middle">$<?php echo $v['unit_price']; ?></td>
					<td width="12%" align="center" valign="middle">$<?php echo $v['unit_gst']; ?></td>
					<td width="12%" align="center" valign="middle">$<?php echo $v['goods_price']; ?></td>
				</tr>
			<?php }  ?>
		</tbody>
	</table>


	<table border="0" cellpadding="0" cellspacing="0" class="subtotal">
		<tbody>
			<tr>
				<td width="70%"></td>
				<td align="right" width="27%"></td>
				<td width="3%"></td>
			</tr>
			<tr>
				<td></td>
				<td align="right">Subtotal &nbsp;&nbsp;$<?php echo $data['order']['subtotal']; ?></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"></td>
				<td></td>
			</tr>




			<?php if ($data['order']['platform_voucher']['voucher_amount'] > 0) { ?>
				<tr>
					<td>ME Platform Voucher $<?php echo $data['order']['platform_voucher']['voucher_amount'];  ?>
						<div style=" font-size: 10px;"><i>#<?php echo $data['order']['platform_voucher']['voucher_code']; ?></i></div>
					</td>
					<td align="right"></td>
					<td></td>
				</tr>

			<?php }  ?>
			<!--
	  <tr>
	   <td>Delivery Fee</td>
	   <td align="right">$< ?php echo $data['order']['payment_delivery_fee']; ?></td>
	      <td ></td>
	  </tr> 
	 -->
			<tr>
				<td>Payment TXN Fee</td>
				<td align="right">$<?php echo $data['order']['payment_txn_fee']; ?></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td align="right"></td>
				<td></td>
			</tr>



		</tbody>
	</table>


	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="total-x">
		<tbody>
			<tr class="">
				<td width="50%">&nbsp;</td>
				<td width="47%" align="right"></td>
				<td width="3%"></td>
			</tr>

			<tr class="">
				<td width="50%">

					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="text-bold total_amount">Total Amount</td>
						</tr>
						<tr>
							<td style="font-size:14px"><i>*Total amount include GST</i></td>
						</tr>
					</table>

				</td>
				<td width="47%" align="right">


					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td align="right" class="text-bold total_amount">AUD $<?php echo $data['order']['total_amount']; ?></td>
						</tr>
						<tr>
							<td style="font-size:12px">
								<i>Voucher Redeem $<?php echo $data['order']['voucher_redeem_fee']; ?></i>
							</td>
						</tr>

						<tr>
							<td style="font-size:12px">
								<i>External Payment Method $<?php echo $data['order']['payment_method_fee']; ?></i>

							</td>
						</tr>

						<tr>
							<td style="font-size:12px">
								<i>ME Wallet Credit $<?php echo $data['order']['payment_wallet_fee']; ?></i>
							</td>
						</tr>

					</table>


				</td>
				<td width="3%"></td>
			</tr>
		</tbody>
	</table>

	<div></div>
	<div class="footer">
		<table>
			<tr>
				<td width="1%">*</td>
				<td width="99%">Invoice Issued by Mad Express (ABN <?php echo $data['platform']['platform_abn']; ?>) via the E-Invoice Generator developed by Mad Express Pty Ltd </td>

			</tr>
			<tr>
				<td width="1%">*</td>
				<td width="99%">Mad Express may purchase goods from third party directly if short in stock; order will be fulfilled within 48 hours of order </td>

			</tr>
		</table>


	</div>
</div>