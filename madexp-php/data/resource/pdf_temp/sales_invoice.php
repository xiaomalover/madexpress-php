<?php defined('InMadExpress') or exit('Access Invalid!');?>
<style>.pager{width:642px;background:#fff;margin:0 auto}.flex{display:flex}.flex-sub{flex:1}.flex-twice{flex:2}.flex-treble{flex:3}.text-bold{font-weight:bold}.text-center{text-align:center}.text-left{text-align:left}.text-right{text-align:right}.header{padding:0 50px}.title{font-family:Arial-BoldMT;font-size:33px;color:#000;letter-spacing:0}.user{font-family:ArialMT;font-size:13px;color:#000;letter-spacing:0;padding:0 50px}.user .customer{margin-top:60px}.user .date{margin-top:30px}.goods{font-family:ArialMT;font-size:13px;color:#000;letter-spacing:0;line-height:30px;padding:0 50px;margin-top:60px;border-bottom:1px solid #000;}.goods-header{margin-top:50px}.goods-header td{border-bottom:1px solid #000}.small{font-family:ArialMT;font-size:10px;color:#000;line-height:12px}.subtotal{width:100%;font-family:ArialMT;font-size:13px;color:#000;text-align:right;margin:0 50px;border-top:1px solid #000}.param{margin:0 50px;font-family:ArialMT;font-size:13px;color:#000;letter-spacing:0;border-bottom:1px solid #000}.total{margin:0 50px;font-family:Arial-BoldMT;font-size:16px;color:#000}.total-tap{font-family:Arial-ItalicMT;font-size:13px;color:#000;letter-spacing:0;line-height:20px}.footer{font-family:ArialMT;font-size:13px;color:#000;letter-spacing:0}</style>
  <div class="pager"> 
   <div class=" header">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr>
       <td height="80" valign="top">
        <div class="title">
         Tax Invoice 
        </div></td>
       <td valign="top" align="right">
        <div>
         <img src="http://www.mad-express.com/data/resource/tcppdf/images/logo1.png" alt="test alt attribute" width="150" border="0" />
        </div></td>
      </tr>
     </tbody>
    </table>
   </div> 
   <div class="user">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr>
       <td align="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tbody>
          <tr>
           <td><?php echo  $order['extend_store']['store_name_primary'];?> Restaurant</td>
          </tr>
          <tr>
           <td><?php echo  $order['extend_store']['store_name_secondary'];?> Restaurant</td>
          </tr>
          <tr>
           <td><?php echo  $order['extend_store']['store_address'];?></td>
          </tr>
          <tr>
           <td>ABN:<?php echo  $order['extend_store']['store_abn'];?></td>
          </tr>
          <tr>
           <td>
            <div style="height:100px"></div>
            <div>
             To:Mad Express Customer #<?php echo  $order['buyer_code'];?>
            </div></td>
          </tr>
         </tbody>
        </table></td>
       <td valign="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tbody>
          <tr>
           <td align="right">Invoice Number</td>
          </tr>
          <tr>
           <td align="right"><?php echo $order['order_sn'].'S'; ?></td>
          </tr>
          <tr>
           <td align="right">
            <div style="height:100px"></div>Date:<?php echo $order['eng_date'] ?> </td>
          </tr>
         </tbody>
        </table></td>
      </tr>
     </tbody>
    </table>
   </div> 
   <div class="goods">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr class="goods-header">
       <td width="10%" align="center">Qty</td>
       <td width="45%" align="center">Items</td>
       <td width="15%" align="center">Unit Price</td>
       <td width="15%" align="center">Unit GST</td>
       <td width="15%" align="center">Amount</td>
      </tr> 
	  <?php foreach($order['goods_list'] as $k => $v){  ?>
      <tr class="goods-list">
       <td width="10%" align="center"><?php echo $v['goods_num']; ?></td>
       <td width="45%" align="center"><?php echo $v['goods_name']; ?><p class="small"><?php echo $v['goods_size']; ?></p></td>
       <td width="15%" align="center">$<?php echo $v['unit_price']; ?></td>
       <td width="15%" align="center">$<?php echo $v['unit_gst']; ?></td>
       <td width="15%" align="center">$<?php echo $v['goods_price']; ?></td>
      </tr> 
	  <?php }  ?>
     </tbody>
    </table>
   </div>
   <div class="param">
    <table width="100%" border="0" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td></td>
       <td align="right">Subtotal $<?php echo $order['goods_amount']; ?></td>
      </tr> 
      <tr>
       <td></td>
       <td align="right"></td>
      </tr>
	   <?php if($order['platform_coupon_id'] > 0){ ?>
      <tr>
       <td>Platform Voucher # <?php echo $order['platform_coupon_code']; ?></td>
       <td align="right">($<?php echo $order['platform_coupon_price']; ?>)</td>
      </tr> 
	  <?php }  ?>
	  
	  <?php if($order['store_coupon_id'] > 0){  ?>
      <tr>
       <td>Store Voucher # <?php echo $order['store_coupon_code']; ?></td>
       <td align="right">($<?php echo $order['store_coupon_price']; ?>)</td>
      </tr> 
	  <?php } ?> 
      <tr>
       <td>Payment TXN Fee</td>
       <td align="right">$<?php echo $order['extxnfee']; ?></td>
      </tr> 
      <tr>
       <td>Container Fee</td>
       <td align="right">$<?php echo $order['foodbox_amount']; ?></td>
      </tr> 
     </tbody>
    </table>
   </div> 
   <div class="total">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
     <tbody>
      <tr>
       <td>Total Amount</td>
       <td align="right">AUD $<?php 	   
	   $amount = $order['goods_amount'] - $order['platform_coupon_price'] - $order['store_coupon_price'] + $order['extxnfee'] + $order['foodbox_amount'];
	    if($amount < 0){
			echo '0.00';
		}else{
			echo $amount;
		}
		
		?></td>
      </tr>
      <tr>
       <td style="font-size:14px"><i>*Total amount include GST</i></td>
       <td align="right"></td>
      </tr>
     </tbody>
    </table>
   </div>
   <div style=""></div>
   <div class="footer">
    * Invoice Issued by <?php echo $order['extend_store']['store_name_secondary']; ?> Restaurant(ABN <?php echo $order['extend_store']['store_abn']; ?>) via the E-Invoice Generator developed by Mad Express Pty Ltd 
   </div>
  </div> 

							
			