<?php 
class Lbs
{
	const API_URL_PREFIX = 'http://api.map.baidu.com';	
	const GEOCONV_URL   = '/geoconv/v1/';	
	const CREATEPOI_URL = '/geodata/v3/poi/create';	 //创建poi链接 POST
	const UPDATEPOI_URL = '/geodata/v3/poi/update';	 //修改poi数据
	const DELETEPOI_URL = '/geodata/v3/poi/delete'; //删除poi		
	const SEARCHPOI_URL = '/geodata/v3/poi/list';//查询poi列表 GET
	const SEARCHPOIDETAIL_RUL = '/geosearch/v3/detail';//查询poi详情 get	
	
	const GEOSEARCH_NEARBY_URL = '/geosearch/v3/nearby';
	
	const LOCATION_URL = '/location/ip';
	const GEOCODING_URL = '/geocoder/v2';	
	const ABROAD_URL = '/place_abroad/v1/suggestion';
	
	const DIRECTION_ABROAD  = '/direction_abroad/v1/driving'; //境外路线规划
	const DIRECTION = 'direction/v2/driving'; //国内路线规划
	
	
	
	
	
	
	
	
    const ASCEND = 1;
    const DESCEND = -1;
	
	
  	protected $params_ = array();
	
	public function __construct()
	{		
		$this->ak = 'DDf3a96db5c46a43d953cc10d6e1d96d' ;//;isset($options['ak'])?$options['ak']:''; //ak秘钥
		
		$this->waiter_id = '200671' ;//isset($options['geotable_id'])?$options['geotable_id']:''; //远程数据表ID
		
		$this->store_id = '200670';
	}
	
	public function Geocoding($address='',$location=''){
		$this->Url = self::API_URL_PREFIX.self::GEOCODING_URL;		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 
		$this->params_['output'] = 'json'; 		
		$this->params_['address'] = $address; 		
		$this->params_['location'] = $location; 
	    $content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'/?'.$content;		
		
		$result = $this->httpGet($url);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
		
		
	
	
		
	}



	  /**
     * create 
     * 
     * @param $title    poi名称 string(256) 可选 。
     * @param address  地址    string(256) 可选 。
     * @param tags tags    string(256) 可选 。
     * @param latitude 用户上传的纬度  double  必选 。
     * @param longitude    用户上传的经度  double  必选 。
     * @param coord_type   用户上传的坐标的类型    uint32  1：未加密的GPS坐标 2：国测局加密 3：百度加密 必选
     * @access public
     * @return void
     */
	public function createPoi($type, $uid, $latitude, $longitude, $coord_type = 3, $options=array()){
		
		$this->Url = self::API_URL_PREFIX.self::CREATEPOI_URL;		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 
		
		if($type == 'waiter'){
			$this->params_['geotable_id'] = $this->waiter_id; 
			$this->params_['waiter_id'] = $uid; 
		}
		
		if($type == 'store'){
			$this->params_['geotable_id'] = $this->store_id; 	
			$this->params_['store_id'] = $uid; 
		}
		
		//$this->params_['title'] = $title; 
       
		
     //   $this->params_['tags'] = $tags; 
        $this->params_['latitude'] = $latitude; 
        $this->params_['longitude'] = $longitude; 
        $this->params_['coord_type'] = $coord_type;
        foreach($options as $k=>$v){
            if (is_null($v)) continue;
            $this->params_[$k] = $v;
        }
	
		$result = $this->httpPost($this->Url,$this->params_,true);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
	}
	
	//修改数据
	public function updatePoi($type, $id ,  $latitude, $longitude, $coord_type = 3, $options=array()){
       	$this->Url = self::API_URL_PREFIX.self::UPDATEPOI_URL;		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak;
		
		if($type == 'waiter'){
			$this->params_['geotable_id'] = $this->waiter_id; 
		}
		
		if($type == 'store'){
			$this->params_['geotable_id'] = $this->store_id; 
		}
     
      
        $this->params_['latitude'] = $latitude; 
        $this->params_['longitude'] = $longitude; 
        $this->params_['coord_type'] = $coord_type;
     	$this->params_['id']    = $id;  
	    foreach($options as $k=>$v){
            if (is_null($v)) continue;
            $this->params_[$k] = $v;
        }
		$result = $this->httpPost($this->Url,$this->params_,true);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
		
	}	
	
	
	//删除POI
	public function deletePoi($id){
        $this->params_['id']    = $id;
		
	}
	
	
	
	//查询poi数据详情
	public function detailPoi($id){
        $this->Url = self::API_URL_PREFIX.self::SEARCHPOIDETAIL_RUL;		
	    $this->params_ = array();
	  //  $this->params_['id']    = $id;
		$this->params_['ak'] = $this->ak; 
		$this->params_['geotable_id'] = $this->geotable_id; 		
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'/'.$id.'?'.$content;		
		
		$result = $this->httpGet($url);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
		
		
		
	}
	
	//查询商户周边叠加商户
	public function storePoi($type,$location,$sortby ='distance:1',$page = 0,$page_size =50,$coord_type = 3 , $radius=3000){
		
		$this->Url = self::API_URL_PREFIX.self::GEOSEARCH_NEARBY_URL;		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 		
		
		
		if($type == 'waiter'){
			$this->params_['geotable_id'] = $this->waiter_id; 		
		}
				
		if($type == 'store'){
			$this->params_['geotable_id'] = $this->store_id; 	
		}
		
		
		$this->params_['location'] = $location; 			
		$this->params_['coord_type'] = $coord_type; 		
		$this->params_['radius'] = $radius; 	
	    $this->params_['sortby'] = $sortby;
	
	  	$this->params_['page'] = $page;
	 	$this->params_['page_size'] = $page_size; 		
	  	
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
		
		
		$result = $this->httpGet($url);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
		
		
	}
		
	
	//查询poi数据
	public function searchPoi($type,$location,$sortby ='distance:1',$page = 0,$page_size =50,$coord_type = 3 , $radius=5000){
		
		$this->Url = self::API_URL_PREFIX.self::GEOSEARCH_NEARBY_URL;		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 		
		
		
		if($type == 'waiter'){
			$this->params_['geotable_id'] = $this->waiter_id; 		
		}
				
		if($type == 'store'){
			$this->params_['geotable_id'] = $this->store_id; 	
		}
		
		
		$this->params_['location'] = $location; 			
		$this->params_['coord_type'] = $coord_type; 		
		$this->params_['radius'] = $radius; 	
	    $this->params_['sortby'] = $sortby;
	
	  	$this->params_['page'] = $page;
	 	$this->params_['page_size'] = $page_size; 		
	  	
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
		
		
		$result = $this->httpGet($url);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
				
	}
	
	
	
	
	
	
	
	public function geoconv($coords,$from = 3,$to = 5) {
		$this->Url = self::API_URL_PREFIX.self::GEOCONV_URL;		
		
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 		
		$this->params_['coords'] = $coords;
		$this->params_['from'] = $from;
		$this->params_['to'] = $to;	
		$result = $this->httpPost($this->Url,$this->params_,true);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
	}
	
	public function location(){
		$this->Url = self::API_URL_PREFIX.self::LOCATION_URL;	
		$this->params_ = array();
		$this->params_['ak'] = $this->ak; 		
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
		
		
		$result = $this->httpGet($url);
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;
		
		
	}
	
	//境外路线规划,距离。时间等
	public function directionAbroad($origin,$destination){
		
		$this->Url = self::API_URL_PREFIX.self::DIRECTION_ABROAD;	
	
		$this->params_ = array();
		$this->params_['origin'] = $origin;
		$this->params_['destination'] = $destination;	
		
		$this->params_['ak'] = $this->ak; 	
		$this->params_['output'] ='json'; 	
		
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
	
		$result = $this->httpGet($url);
	
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $this->abroad_format($json);
		}
		return false;		
		
		
	}
	
	//返回推荐路线格式化
	private function abroad_format($data){
		
		if($data['status'] == 0 && $data['result']['total'] > 0 ){
			
			//只去第一条数据
			$row = $data['result']['routes'][0];			
			$result['distance'] = round($row['distance'] / 1000,2);
			$result['duration'] = round($row['duration'] / 60,0);	
			
			
		}else{
			
			return $data;
			
		}
		
		return $result;
			
	}
	
	
	//路线规划,距离。时间等
	public function direction($origin,$destination){
		
		$this->Url = self::API_URL_PREFIX.self::DIRECTION;	
	
		$this->params_ = array();
		$this->params_['origin'] = $origin;
		$this->params_['destination'] = $destination;	
		
		$this->params_['ak'] = $this->ak; 	
		$this->params_['output'] ='json'; 	
		
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
	
		$result = $this->httpGet($url);
	
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $this->abroad_format($json);
		}
		return false;		
		
		
	}
	
	
	
	
	//境外地点输入提示	
	public function place_abroad($query){
		
		$this->Url = self::API_URL_PREFIX.self::ABROAD_URL;	
		$this->params_ = array();
		$this->params_['query'] = $query;
		$this->params_['region'] = '墨尔本';
		$this->params_['ak'] = $this->ak; 	
		$this->params_['output'] ='json'; 	
		
		$content = '';
		foreach ($this->params_ as $k => &$v) 
        {
            if (is_null($v)) continue;
            $v = urlencode($v);
            $content .= $k . '=' . $v . '&';
        }
        $content = substr($content, 0, strlen($content) - 1);			
		$url = $this->Url .'?'.$content;	
	
		$result = $this->httpGet($url);
	
		if ($result)
		{
			$json = json_decode($result,true);
			if (!$json || !empty($json['errcode'])) {
				$this->errCode = $json['errcode'];
				$this->errMsg = $json['errmsg'];
				return false;
			}
			return $json;
		}
		return false;		
		
		
	}
	
	
	
	
	/**
	 * GET 请求
	 * @param string $url
	 */
	private function httpGet($url){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}

	/**
	 * POST 请求
	 * @param string $url
	 * @param array $param
	 * @param boolean $post_file 是否文件上传
	 * @return string content
	 */
	private function httpPost($url,$param,$post_file=false){
		$oCurl = curl_init();
		if(stripos($url,"https://")!==FALSE){
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($oCurl, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($oCurl, CURLOPT_SSLVERSION, 1); //CURL_SSLVERSION_TLSv1
		}
		if (is_string($param) || $post_file) {
			$strPOST = $param;
		} else {
			$aPOST = array();
			foreach($param as $key=>$val){
				$aPOST[] = $key."=".urlencode($val);
			}
			$strPOST =  join("&", $aPOST);
		}
		curl_setopt($oCurl, CURLOPT_URL, $url);
		curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($oCurl, CURLOPT_POST,true);
		curl_setopt($oCurl, CURLOPT_POSTFIELDS,$strPOST);
		$sContent = curl_exec($oCurl);
		$aStatus = curl_getinfo($oCurl);
		curl_close($oCurl);
		if(intval($aStatus["http_code"])==200){
			return $sContent;
		}else{
			return false;
		}
	}
	
	
	
	
	/**
	 * @name 围栏算法，判断一个坐标，是否在围栏里面.如：['113.664673,34.810146','113.681667,34.796896','113.69231,34.794711','113.702009,34.809159']
	 * @author macnie <mac@lenmy.com>
	 * @param array $fences 围栏，是一组坐标数组 如：113.674458,34.804719
	 * @param string $point
	 * @return bool
	 */
	
	
	
	
	public function in_fences($fences, $point) {
		$nvert = count($fences);
		$vertx = [];
		$verty = [];
		list($testy, $testx) = explode(',', $point);
		foreach ($fences as $r) {
			list($lng, $lat) = explode(',', $r);
			$vertx[] = $lat;
			$verty[] = $lng;
		}
		$i = $j = $c = 0;
		for ($i = 0, $j = $nvert - 1; $i < $nvert; $j = $i++) {
			if (( ($verty[$i] > $testy) != ($verty[$j] > $testy) ) &&
				($testx < ($vertx[$j] - $vertx[$i]) * ($testy - $verty[$i]) / ($verty[$j] - $verty[$i]) + $vertx[$i]))
				$c = !$c;
		}
		return $c;
	}
	
	
	
	
	
	/*
		
		
		
		
		
	*/
	
	
	
	
	
	
	
}



?>