<?php 
defined('InMadExpress') or exit('Access Invalid!');

class dir{
    public $path; //路径
    public $opendirr; //打开路径
    public $readdirr;//读取路径
    public $filee; //文件数组
    public $doc_root;//站点根目录
    public $zsPath;        //用于拼接目录
    public $fileStar;  //文件属性
    public $strip;    //字符最后一次出现的位置
    public $strlen;        //字符总长度
    public $sub;       //截取的文件后缀名
    public $fileEvenImg; //每个不同的文件属性的图标
    public $alert = "文件暂时不能读取，新功能开发中！";
    public $fileOutChar;   //原编码
    public $pathChar;  //路径字符编码
    public $pathGbk;   //路径gbk编码

    public function __construct(){
        $this->doc_root = BASE_UPLOAD_PATH;
    }

    //析构函数打开文件
    public function odir($path){
        $this->path = $this->doc_root.'/'.$path;
        if(!$path){
            die("error 没有定义路径请定义路径！");
        }
        //开始获取路径编码并强行转为gbk
        //然后用gbk的方式来获取文件后强行转为utf-8显示
        $this->pathChar = mb_detect_encoding($this->path,array('UTF-8','GBK','LATIN1','BIG5'));
        $this->pathChar = strtolower($this->pathChar);
        if($this->pathChar != 'gbk'){
            $this->pathGbk = iconv($this->pathChar, 'gbk',$this->path );
        }
        if(!is_dir($this->pathGbk)){
            die("error 不是一个正确的路径！");
        }
        $this->opendirr = opendir($this->pathGbk);
        if(!$this->opendirr){
            die('error 打开文件夹失败！');
        }
		$dir_list = array();
        while($this->filee = readdir($this->opendirr)){
				
		
            /*获取编码值不是则转换为utf-8 */
            $charset = mb_detect_encoding($this->filee,array('UTF-8','GBK','LATIN1','BIG5'));
            $charset = strtolower($charset);
            /* 转换为utf-8 */
            if($charset != 'utf-8' || $charset != 'utf8'){
                //$char = iconv($charset, 'cp960', $this->filee);
                $this->filee = iconv($charset, 'utf-8', $this->filee);
            }
            /*附一个值为原编码 * 方便浏览访问+判断 */
            $this->fileOutChar = iconv('utf-8', 'gbk', $this->filee);
            $this->pathGbk = iconv('utf-8', 'gbk', $this->path);
            /* 开始switch判断把默认的 . 与 ..改成返回上一页与反回首页 */
		
			if($this->filee == '.' || $this->filee == '..'){
				continue;
			}
			
            switch ($this->filee) {
                case '.':
                  //  $dir_list[] = "<li><a href='javascrip::valid(0)' onclick='window.history.back(-1);return false;'><img src='img/back.png' width='25' height='25'>返回上级</a></li>";
                    break;
                case '..':
                   // $dir_list[] = "<li><a href='index.php?url={$this->doc_root}'><img src='img/index.png' width='25' height='25'>站点首页</a></li>";
                    break;
                default :
                 
					/* $this->zsPath  拼接的gbk路径  用于判断是否为文件夹 */
                 
			
					$this->zsPath = $this->pathGbk.'/'.$this->fileOutChar;
					
				//	print_r($this->zsPath);
					
					$dir_list['name'] = $this->filee;
					
                    if(is_dir($this->zsPath)){                       
					   $dir_list['dir'] = true;
					   
					//	$dir_list[] = "<li><img src='img/files.png' width='30' height='30'><a href='index.php?path={$this->path}/{$this->filee}'>".$this->filee."</a></li>";
						
                    }else {
						$dir_list['contentType'] = '';
						$dir_list['dir'] = false;	 //是否是文件夹	
						$dir_list['formattedSize'] = filesize($this->path.'/'.$this->filee);
						$dir_list['path'] = $this->path;
						$dir_list['size'] = filesize($this->path.'/'.$this->filee);
						$dir_list['url'] = UPLOAD_SITE_URL.'/'.$path.'/'.$this->filee;
						
                        /* 开始获取文件后缀名 */
                        $this->strip = strripos($this->filee,'.');
						
						
						$this->strlen = strlen($this->filee);                     
						$this->sub = substr($this->filee,$this->strip+1,$this->strlen - $this->strip);
						$dir_list['suffix'] = $this->sub;
                                           
						//截取完毕开始判断 * 如果没有后缀名则使用no.png
                        if(!$this->strip){
                            $this->sub = 'no';
                        }
						
                        //如果后缀名数字多余两位 使用nums.png
                        if(preg_match('/[0-9]{2,}/',$this->sub)){
                            $this->sub = 'nums';
                        }
						
                        //如果后缀名中带‘ - ’则使用no.png
                        if(preg_match('-', $this->sub)){
                            $this->sub = 'no';
                        }
						
                        //如果后缀名大于等于6位则使用no.png
                        if(strlen($this->sub) >= 6){
                            $this->sub = 'no';
                        }
						
                        //否则使用img目录对于的后缀名
                      //  $dir_list[] = "<li><img src='img/{$this->sub}.png' width='30' height='30'><a href='edit.php?f={$this->path}/{$this->filee}'>".$this->filee."</a></li>";
						
                    }
                    break;
            }
			
			
			
			$list[] = $dir_list;
			
			
			
        }
		
		
		
        closedir($this->opendirr);
		
		return $list;
		
    }
}
?>