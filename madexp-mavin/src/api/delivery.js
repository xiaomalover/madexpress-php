import request from '@/utils/request'

export function fetchDeliveryList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_list',
    method: 'post',
    data
  })
}

export function fetchDeliveryDetails(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_info',
    method: 'post',
 data
  })
}


//订单列表
export function fetchDeliveryOrderList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_order',
    method: 'post',
  data
  })
}


//租赁记录
export function fetchDeliveryLeaseList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_lease',
    method: 'post',
    data
  })
}

//反馈记录
export function fetchDeliveryFeedbackList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_feedback',
    method: 'post',
    data
  })
}

//排班记录
export function fetchDeliveryShedulingList(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_sheduling',
    method: 'post',
    data
  })
}


//新增新的配送员
export function fetchDeliverySave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_save',
    method: 'post',
    data
  })
}

//文件编辑
export function fetchFileSave(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_base_save',
    method: 'post',
    data
  })
}



//基础信息
export function fetchDeliveryBase(data) {
  return request({
    url: 'index.php?act=delivery&op=delivery_basic',
    method: 'post',
    data
  })
}

