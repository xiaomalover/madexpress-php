import request from '@/utils/request'

export function getProRegionList() {
  return request({
    url: 'index.php?act=region_sector&op=get_area_region',
    method: 'post'
  })
}


export function getMoneyRegionList() {
  return request({
    url: 'index.php?act=region_sector&op=get_sector_point_list',
    method: 'post'
  })
}



export function getSectorRegionList() {
  return request({
    url: 'index.php?act=region_sector&op=get_sector_list',
    method: 'post'
  })
}

export function  getOldRegionList(data) {
  return request({
    url: 'index.php?act=region_sector&op=get_old_region',
    method: 'post'
  })
}
//新增大区
export function  addBigRegion(data) {
  return request({
    url: 'index.php?act=region_sector&op=add_big_name',
    method: 'post'	,
	data
  })
}

//新增大区
export function  saveRegionVersion(data) {
  return request({
    url: 'index.php?act=region_sector&op=save_region',
    method: 'post'	,
	data
  })
}

//删除
export function  delBigRegion(data) {
  return request({
    url: 'index.php?act=region_sector&op=del_sector',
    method: 'post'	,
	data
  })
}

//批量编辑大区名字
export function  editBigRegion(data) {
  return request({
    url: 'index.php?act=region_sector&op=edit_big',
    method: 'post'	,
	data
  })
}



//
export function  editSwitchPointShow(data) {
  return request({
    url: 'index.php?act=region_sector&op=switch_point_show',
    method: 'post'	,
	data
  })
}

//
export function  editSwitchSectorShow(data) {
  return request({
    url: 'index.php?act=region_sector&op=switch_sector_show',
    method: 'post'	,
	data
  })
}

//
export function  editSwitchAreaShow(data) {
  return request({
    url: 'index.php?act=region_sector&op=switch_area_show',
    method: 'post'	,
	data
  })
}
