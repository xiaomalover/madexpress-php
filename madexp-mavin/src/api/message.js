import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=message&op=index',
    method: 'post',
    data
  })
}

export function fetchMesInfo(data) {
  return request({
    url: 'index.php?act=message&op=get_msg_info',
    method: 'post',
    data
  })
}

export function fetchMesSave(data) {
  return request({
    url: 'index.php?act=message&op=save_msg',
    method: 'post',
    data
  })
}
