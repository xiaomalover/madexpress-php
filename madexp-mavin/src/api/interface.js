import request from '@/utils/request'

export function fetchInterfaceList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=interface&op=interface_list',
    method: 'post',
    data
  })
}
