import request from '@/utils/request'

export function fetchClassList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_goods&op=get_goods_class',
    method: 'post',
    data
  })
}

export function fetchClassInfo(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_goods&op=get_goods_class_info',
    method: 'post',
    data
  })
}

export function fetchClassSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_goods&op=save_class',
    method: 'post',
    data
  })
}

//删除
export function fetchClassDel(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_goods&op=drop_class',
    method: 'post',
    data
  })
}



//批量修改
export function fetchBatchClassSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_goods&op=batch_class_save',
    method: 'post',
    data
  })
}



export function fetchGoodsList(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_goods_list',
    method: 'post',
    data
  })
}


//删除商品
export function fetchDropGoods(data) {
  return request({
    url: 'index.php?act=store_goods&op=drop_goods',
    method: 'post',
    data
  })
}

//获取点铺分类


//获取配件商品

export function fetchOptionsGoods(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_options_list',
    method: 'post',
    data
  })
}

//新增配件init

export function fetchOptionsGoodsInt(data) {
  return request({
    url: 'index.php?act=store_goods&op=goods_options',
    method: 'post',
    data
  })
}

//保存配件

export function fetchOptionsGoodsSave(data) {
  return request({
    url: 'index.php?act=store_goods&op=save_options',
    method: 'post',
    data
  })
}

//详情
export function fetchOptionsGoodsInfo(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_options_info',
    method: 'post',
    data
  })
}



//商品INIT

export function fetchGoodsInit(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_goods_init',
    method: 'post',
    data
  })
}
//删除
export function fetchGoodsDel(data) {
  return request({
    url: 'index.php?act=store_goods&op=goods_del',
    method: 'post',
    data
  })
}

//编辑
export function fetchGoodsInfo(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_goods_info',
    method: 'post',
    data
  })
}



//保存
export function fetchGoodsSave(data) {
  return request({
    url: 'index.php?act=store_goods&op=save_goods',
    method: 'post',
    data
  })
}

//推荐
export function fetchGoodsStateSwitch(data) {
  return request({
    url: 'index.php?act=store_goods&op=state_switch',
    method: 'post',
    data
  })
}

//上下架
export function fetchGoodsStateGoodsSwitch(data) {
  return request({
    url: 'index.php?act=store_goods&op=state_goods_switch',
    method: 'post',
    data
  })
}



//商品
export function fetchXlsExcelClassList(data) {
  return request({
    url: 'index.php?act=store_goods&op=goods_excel_class',
    method: 'post',
    data
  })
}



//xls商品
export function fetchXlsGoodsList(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_xls_goods_list',
    method: 'post',
    data
  })
}


//xls详情
export function fetchXlsGoodsInfo(data) {
  return request({
    url: 'index.php?act=store_goods&op=get_xls_goods_info',
    method: 'post',
    data
  })
}


//xls 删除商品
export function fetchXlsGoodsDel(data) {
  return request({
    url: 'index.php?act=store_goods&op=xls_goods_del',
    method: 'post',
    data
  })
}

//xls 保存商品
export function fetchXlsGoodsSave(data) {
  return request({
    url: 'index.php?act=store_goods&op=xls_goods_save',
    method: 'post',
    data
  })
}


//xls 清空商品
export function fetchXlsEmpty(data) {
  return request({
    url: 'index.php?act=store_goods&op=excel_empty',
    method: 'post',
    data
  })
}

//xls 导入商品
export function fetchXlsImport(data) {
  return request({
    url: 'index.php?act=store_goods&op=excel_import',
    method: 'post',
    data
  })
}


