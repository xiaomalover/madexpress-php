import request from '@/utils/request'

//获取设置
export function fetchSetting(data) {
  return request({
    url: 'index.php?act=system&op=index',
    method: 'post',
    data
  })
}

//获取语言配置
export function fetchLangList(data) {
  return request({
    url: 'index.php?act=system&op=system_lang',
    method: 'post',
    data
  })
}

export function logout() {
  return request({
    url: 'index.php?act=admin&op=logout',
    method: 'post'
  })
}
