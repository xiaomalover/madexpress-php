import request from '@/utils/request'

export function info(query) {
  return request({
    url: 'index.php?act=mavin_store&op=info',
    method: 'get',
    params: query
  })
}

export function fetchList(data) {
  return request({
    url: 'index.php?act=mavin_store&op=store_list',
    method: 'post',
   data
  })
}


export function fetchClassList(data) {
  return request({
    url: 'index.php?act=mavin_store&op=store_class',
    method: 'post',
    data
  })
}





//保存商家
export function fetchStoreSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=store_save',
    method: 'post',
    data
  })
}

//保存STIPE商家
export function fetchStoreStripeSave(data) {
  return request({
    url: 'index.php?act=stripe&op=create_store',
    method: 'post',
    data
  })
}





//保存商家大类
export function fetchCateBigSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=save_big_class',
    method: 'post',
    data
  })
}

//获取商家大类初始化
export function fetchCateBigInit(data) {
  return request({
    url: 'index.php?act=mavin_store&op=get_big_class_init',
    method: 'post',
    data
  })
}


//获取商家大类详情

export function fetchCateBigInfo(data) {
  return request({
    url: 'index.php?act=mavin_store&op=get_big_class_info',
    method: 'post',
    data
  })
}


//保存小类
export function fetchCateSmallSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=save_small_class',
    method: 'post',
    data
  })
}

//获取小类初始化
export function fetchCateSmallInit(data) {
  return request({
    url: 'index.php?act=mavin_store&op=get_small_class_init',
    method: 'post',
    data
  })
}


//获取小类详情

export function fetchCateSmallInfo(data) {
  return request({
    url: 'index.php?act=mavin_store&op=get_small_class_info',
    method: 'post',
    data
  })
}




//INGR分类列表
export function fetchIngrClassList(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_list',
    method: 'post',
    data
  })
}

//INGR分类列表
export function fetchIngrClassSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_save',
    method: 'post',
    data
  })
}

//INGR分类列表
export function fetchIngrClassDel(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_del',
    method: 'post',
    data
  })
}




//INGR列表
export function fetchIngrList(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_list',
    method: 'post',
    data
  })
}

//INGR保存
export function fetchIngrSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_save',
    method: 'post',
    data
  })
}

//INGR删除
export function fetchIngrDel(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_del',
    method: 'post',
    data
  })
}

//INGRINIT
export function fetchIngrInit(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_init',
    method: 'post',
    data
  })
}
//INGRINIT
export function fetchIngrInfo(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_info',
    method: 'post',
    data
  })
}






//保存商家大类
export function fetchIngrBigSave(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_save',
    method: 'post',
    data
  })
}

//获取商家大类初始化
export function fetchIngrBigInit(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_init',
    method: 'post',
    data
  })
}


//获取商家大类详情

export function fetchIngrBigInfo(data) {
  return request({
    url: 'index.php?act=mavin_store&op=ingr_class_info',
    method: 'post',
    data
  })
}



//获取商家大类详情

export function fetchStoreToken(data) {
  return request({
    url: 'index.php?act=mavin_store&op=store_token',
    method: 'post',
    data
  })
}
