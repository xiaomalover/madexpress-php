import request from '@/utils/request'


//鑾峰彇鍖哄煙
export function getRegionList() {
  return request({
    url: 'index.php?act=region&op=get_region',
    method: 'post'   
  })
}

export function  getOldRegionList(data) {
  return request({
    url: 'index.php?act=region&op=get_old_region',
    method: 'post'	
  })
}
//新增大区
export function  addBigRegion(data) {
  return request({
    url: 'index.php?act=region&op=add_big_name',
    method: 'post'	,
	data
  })
}

//新增大区
export function  saveRegionVersion(data) {
  return request({
    url: 'index.php?act=region&op=save_region',
    method: 'post'	,
	data
  })
}

//删除大区小区
export function  delBigRegion(data) {
  return request({
    url: 'index.php?act=region&op=del_big',
    method: 'post'	,
	data
  })
}

//批量编辑大区名字
export function  editBigRegion(data) {
  return request({
    url: 'index.php?act=region&op=edit_big',
    method: 'post'	,
	data
  })
}


//地区版本启用
export function  editVersionEnable(data) {
  return request({
    url: 'index.php?act=region&op=region_enable',
    method: 'post'	,
	data
  })
}