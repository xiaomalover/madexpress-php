import request from '@/utils/request'

export function info(query) {
  return request({
    url: 'index.php?act=mavin_operate&op=info',
    method: 'get',
    params: query
  })
}