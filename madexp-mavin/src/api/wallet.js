import request from '@/utils/request'

export function fetchCouponOrderList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=wallet&op=coupon_order_list',
    method: 'post',
    data
  })
}
export function fetchCouponList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=wallet&op=coupon_list',
    method: 'post',
    data
  })
}

//
export function fetchCouponSave(data) {
	console.log(data);
  return request({
    url: 'index.php?act=wallet&op=coupon_save',
    method: 'post',
    data
  })
}

export function fetchCouponInfo(data) {
	console.log(data);
  return request({
    url: 'index.php?act=wallet&op=coupon_info',
    method: 'post',
    data
  })
}

export function fetchCouponInfoList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=wallet&op=coupon_info_list',
    method: 'post',
    data
  })
}
export function fetchCouponMemberList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=member&op=member_list',
    method: 'post',
    data
  })
}


