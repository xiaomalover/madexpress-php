import request from '@/utils/request'

export function fetchEvaList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=store_evaluate&op=evaluate_list',
    method: 'post',
    data
  })
}

export function getEvaInfo(data) {
  return request({
    url: 'index.php?act=store_evaluate&op=evaluate_info',
    method: 'post',
    data
  })
}
