import request from '@/utils/request'

export function fetchClassList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=customer_class',
    method: 'post',
    data
  })
}

export function fetchList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=get_customer_list',
    method: 'post',
    data
  })
}

export function fetchFeedbackLogAdd(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=feedback_add_log',
    method: 'post',
    data
  })
}


export function fetchConfirmFeedback(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=confirmFeedback',
    method: 'post',
    data
  })
}



export function fetchHelpClassList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=help_class',
    method: 'post',
    data
  })
}

export function fetchHelpList(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=help_list',
    method: 'post',
    data
  })
}

export function fetchHelpSave(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=help_save',
    method: 'post',
    data
  })
}

export function fetchHelpInfo(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=help_info',
    method: 'post',
    data
  })
}

export function fetchHelpDel(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=help_del',
    method: 'post',
    data
  })
}

export function getInfo(data) {
  console.log(data)
  return request({
    url: 'index.php?act=mavin_customer&op=feedback_info',
    method: 'post',
    data
  })
}
