import request from '@/utils/request'

//状态
export function fetchClassList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_depot&op=depot_class',
    method: 'post',
    data
  })
}

//餐盒类别
export function fetchGoodsList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_depot&op=depot_goods',
    method: 'post',
    data
  })
}

//仓库地区
export function fetchRegionList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_depot&op=deopt_region',
    method: 'post',
    data
  })
}



//餐盒订单
export function fetchOrderList(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_depot&op=depot_order_list',
    method: 'post',
    data
  })
}
