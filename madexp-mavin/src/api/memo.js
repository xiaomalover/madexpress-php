import request from '@/utils/request'

export function fetchClassList(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=get_class',
    method: 'post',
    data
  })
}

export function fetchClassSave(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=class_save',
    method: 'post',
    data
  })
}

export function fetchInfo(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=memo_info',
    method: 'post',
    data
  })
}

export function fetchSave(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=memo_save',
    method: 'post',
    data
  })
}

export function fetchDel(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=memo_del',
    method: 'post',
    data
  })
}

export function fetchClassDel(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=memo_class_del',
    method: 'post',
    data
  })
}

export function uploadFile(data) {
  return request({
    url: 'index.php?act=mavin_memo&op=upload_file',
    method: 'post',
    data
  })
}