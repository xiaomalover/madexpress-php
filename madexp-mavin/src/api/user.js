import request from '@/utils/request'

export function login(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_login&op=login',
    method: 'post',
    data
  })
}

export function autologin(data) {
	console.log(data);
  return request({
    url: 'index.php?act=mavin_login&op=autologin',
    method: 'post',
    data
  })
}


export function getInfo(token) {
  return request({
    url: 'index.php?act=mavin_admin&op=index',
    method: 'post',
    token
  })
}

export function info(token) {
  return request({
    url: 'index.php?act=mavin_admin&op=info',
    method: 'post',
    token
  })
}

export function logout() {
  return request({
    url: 'index.php?act=mavin_admin&op=logout',
    method: 'post'
  })
}
