import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_scheduling',
    method: 'post',
    data
  })
}

//配置排班数据
export function fetchRegionList(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_region',
    method: 'post',
    data
  })
}




export function fetchInfo(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_info',
    method: 'post',
    data
  })
}

//车辆排班
export function fetchVehicleList(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_vehicle_lease',
    method: 'post',
    data
  })
}


//
export function fetchSchSettingList(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_setting',
    method: 'post',
    data
  })
}


//
export function fetchSchSaveSetting(data) {
  return request({
    url: 'index.php?act=scheduling&op=save_setting',
    method: 'post',
    data
  })
}



//保存排班配置
export function fetchSchSaveRegion(data) {
  return request({
    url: 'index.php?act=scheduling&op=save_sch_region',
    method: 'post',
    data
  })
}




//日期
export function fetchSchWork(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_work_list',
    method: 'post',
    data
  })
}



//任务
export function fetchSchTaskList(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_task_list',
    method: 'post',
    data
  })
}

//任务保存
export function fetchSchTaskInfo(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_task_info',
    method: 'post',
    data
  })
}

//任务保存
export function fetchSchTaskSave(data) {
  return request({
    url: 'index.php?act=scheduling&op=get_task_save',
    method: 'post',
    data
  })
}




//批量的那个玩意，名字都不知道咋的起了。
export function fetchSchBatchList(data) {
  return request({
    url: 'index.php?act=scheduling&op=scheduling_batch_list',
    method: 'post',
    data
  })
}

//批量的那个玩意，保存名字都不知道咋的起了。
export function fetchSchBatchSave(data) {
  return request({
    url: 'index.php?act=scheduling&op=scheduling_batch_save',
    method: 'post',
    data
  })
}



//更新未来排班的设定
export function fetchSchCronTab(data) {
  return request({
    url: 'index.php?act=scheduling&op=update_region',
    method: 'post',
    data
  })
}


//更新未来排班的设定
export function fetchSchItemSave(data) {
  return request({
    url: 'index.php?act=scheduling&op=scheduling_item_save',
    method: 'post',
    data
  })
}

