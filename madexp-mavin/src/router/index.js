import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */


/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)',
      component: () => import('@/views/redirect/index')
    }]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
{
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      component: () => import('@/views/operate/index'),
      name: 'Dashboard',
      meta: {
        title: '首页',
        icon: 'icon_yunyingshuju',
        affix: true
      }
    }]
  }

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  //商户管理
 {

   path: '/store',
   component: Layout,
   redirect: '/store/index',
   name: 'store',
   meta: {
     title: '商户管理',
     icon: 'icon_shangjiaguanli'
   },
   children: [{
       path: 'index',
       component: () => import('@/views/store/index'),
       name: 'index',
       meta: {
         title: '商户管理',
         icon: 'icon_shangjiaguanli',
         affix: false
       }
     },
     {
       path: 'cate',
       component: () => import('@/views/store/cate'),
       name: 'cate',
       meta: {
         title: '菜品分类',
         noCache: true,
         activeMenu: '/store/index'
       },
       hidden: true
     },
     {
       path: 'ingr',
       component: () => import('@/views/store/ingr'),
       name: 'ingr',
       meta: {
         title: '菜品元素',
         noCache: true,
         activeMenu: '/store/index'
       },
       hidden: true
     },

     {
       path: 'stripe',
       component: () => import('@/views/store/stripe'),
       name: 'stripe',
       meta: {
         title: 'stripe管理',
         noCache: true,
         activeMenu: '/store/index'
       },
       hidden: true
     },


   ]},
  //订单管理
  {

    path: '/order',
    component: Layout,
    redirect: '/order/index',
    name: 'order',
    meta: {
      title: '订单管理',
      icon: 'el-icon-s-help'
    },
    children: [{
        path: 'index',
        component: () => import('@/views/order/index'),
        name: 'orderindex',
        meta: {
          title: '订单管理',
          icon: 'icon_dingdanguanli',
          affix: false
        }
      },
      {
        path: 'details/:id(\\d+)',
        component: () => import('@/views/order/details'),
        name: 'orderdetails',
        meta: {
          title: '订单详情',
          activeMenu: '/order/index'
        },
        hidden: true
      },
    ]},




  //客服
  {


    path: '/customer',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/customer/index'),
      name: 'customer',
      meta: {
        title: '客服管理',
        icon: 'icon_kefu',
        affix: false
      }
    },
    {
      path: 'info/:id(\\d+)',
      component: () => import('@/views/customer/info'),
      name: 'info',
      meta: {
        title: '详情',
        noCache: true,
        activeMenu: '/customer/index'
      },
      hidden: true
    }]

  },

  //库存
  {

    path: '/depot',
    component: Layout,
    redirect: '/depot/index',
    name: 'depot',
    meta: {
      title: '库存管理',
      icon: 'el-icon-s-help'
    },
    children: [{
        path: 'index',
        component: () => import('@/views/depot/index'),
        name: 'depot',
        meta: {
          title: '库存管理',
          icon: 'icon_canheguanli',
          affix: false
        }
      },
      {
        path: 'info/:id(\\d+)',
        component: () => import('@/views/depot/info'),
        name: 'info',
        meta: {
          title: '订单详情',
          noCache: true,
          activeMenu: '/depot/index'
        },
        hidden: true
      },
      {
        path: 'log',
        component: () => import('@/views/depot/log'),
        name: 'log',
        meta: {
          title: '仓库日志',
          noCache: true,
          activeMenu: '/depot/index'
        },
        hidden: true
      }
    ]},


  //财务管理
  {

    path: '/finance',
    component: Layout,
    redirect: '/finance/index',
    name: 'mavin_finance',
    meta: {
      title: '财务管理',
      icon: 'el-icon-s-help'
    },
    children: [{
      path: 'index',
      component: () => import('@/views/finance/index'),
      name: 'mavin_finance',
      meta: {
        title: '财务管理',
        icon: 'icon_paibanguanli',
        affix: false
      }
    } ]



  },

  //备忘
  {
    path: '/memo',
    component: Layout,
    children: [{
      path: 'index',
      component: () => import('@/views/memo/index'),
      name: 'memo',
      meta: {
        title: '备忘录',
        icon: 'icon_beiwanglu',
        affix: false
      }
    }]
  },

	// 404 page must be placed at the end !!!
	{
		path: '*',
		redirect: '/404',
		hidden: true
	}
]


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
